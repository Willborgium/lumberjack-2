// Will's guide to cool code...

// Comments should start with a space, like this one, and not like the one directly below this:
//this is a terrible comment.

//
// Multi-line comments should begin and end with a blank comment,
// to denote their multi-linedness. It makes it easier to identify
// a block of related comments as opposed to a bunch of semi-unrelated
// comments near each other.
//

// This is a typical function: it starts with a comment, like this one!
// void MakeMeASandwich( int piecesOfBread, bool hasBologne )
// {
//		if( piecesOfBread > 0 )
//		{
//			cout << "[BREAD]\n";
// 
//			for( int index = 0; index < piecesOfBread; index++ )
//			{
//				cout << ( hasBologne ? "[BOLOGNE]\n" : "[AIR]\n" ) << "[BREAD]\n";
//			}
//		}
// }

//
// Some subtlties:
// - Each line that is not seperated by a curly brace is seperated by blank line
// - Function names are PascalCase, and function parameters are camelCase
// - Parenthesis never 'eat' other symbols, meaning there is always a space
//   on the side in which the parenthesis is bending.
// - Multi-line comments that are part of a bulleted list should line up with each
//   other, like this one and the one before this.
// - for loops should be iterated with index, as opposed to ndx or i. That is, unless you
//   have a more descriptive name like row, column, u, or v. Those make sense. Just prefer
//   index over i or ndx.
//

//
// Now for a cool class!
// - Template classes should use the following basic declaration
//   template <typename Type>
//   class GenericClass
// - The things to note there are the words typename, which is better than saying class,
//   and Type, which makes it easy, internally, to know you're talking about the template type.
//   That is, of course, unless you can say something more descriptive like
//   BufferType, ValueType, KeyType, etc...
// - Setup structures should:
//
//		- Be structs
//		- Be marked final ( so there's no accidental trickle-down if the parent changes )
//		- Only have private members ( they can have methods, too. Just avoid private members and methods
//		  because that would imply you should probably make it a class )
//
// - Storage specifiers should be grouped together and ordered:
//
//		- private
//		- protected
//		- public
//
// - Avoid using private storage unless its in a sealed class, or you really want to
// - Avoid using friend classes, as it makes for difficult-to-track nuances
// - Members should have end-line comments that are aligned with each other, like so
//
//		int _bob;				// this is a bob
//		bool _jerry;			// this is a jerry
//		Candle* _bigCandle;		// this is a big candle
//
// - Functions should have comments that indicate what the parameters are and what the return
//   value is.
// - Function comments should only be in the header, as the CPP's will be compiled away later anyway.
// - Function names should be PascalCase, whereas function parameter names should be camelCase
// - Constructors should be listed in order of complexity ( basically number of parameters )
//   starting with a default constructor
// - If there is more than one constructor, consider wrapping it in a region called Constructors
// - Use right-hand reference for copy constructors ( && )
// - Setters and Getters should be named SetXXX and GetXXX, and should be written inline.
// - Setters and Getters should NOT be performing complex activity
// - Getters should be marked with a trailing const, like:
//   int GetAge( ) const
// - Use #ifndef, #define, #endif as an include guard on all headers
// - When using #ifndef, use one of the following formats: 
//
//		#ifndef CLASSNAME_H
//		#ifndef NAMESPACE_CLASSNAME_H
//		#ifndef INTERFACENAME_INTERFACE_H
//		#ifndef NAMESPACE_INTERFACENAME_INTERFACE_H
// 
// - Use initializer lists on constructors, and initialize all pointers to nullptr
// - Use virtual destructors
//

//
// - Curly braces shouldn't be on the same line as a function name. Always drop down to the next line.
// - Use nullptr instead of NULL
// - Use trailing types on numbers: 0.0f, 1.25d, 123456789L
// - When using trailing types on longs ( L ), be sure to use uppercase L, as lowercase L looks like a 1.
// - Members should start with an underscore and be camelCase: _myNumber.
// - Be aware of the following optimization, as it happens a lot:
//
//			Object* obj = nullptr;
//			
//			if( someBool )
//			{
//				obj = new Object( 10 );
//			}
//			else
//			{
//				obj = new Object( 5 );
//			}
//
//   This statement takes 4 logical operations, but is the same as:
//
//		Object* obj = someBool ? new Object( 10 ) : new Object( 5 );
//
//   which takes 2...
//   applause ensues
//

// I know I have more, but those will come on a "oh, hey, by the way" basis...
// I'm not a tyrant, these are just things I do. If you don't like em, let me know! We can make
// a new style that works for both of us. Nothing here is set in stone. Don't think I'm trying to
// be a jerk and say THIS IS THE ONLY WAY TO CODE! ( Although it's the only way to code if you
// wanna be a cool guy, like me )