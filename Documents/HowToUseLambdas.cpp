#include <iostream>
#include <functional>
using namespace std;

// How To Use Lambdas Effectively!

// Definition:
// Lambdas are dynamic functions that exist as objects instantiated at run-time
// The most noticable feature of lambdas is that they are "nameless"
// Their syntax may seem strange at first glance, but it all makes sense after an explanation
// So lets begin!

void main()
{
	// This is a lambda function that does nothing
	[]( )
	{
	};

	// Looking at the syntax, the square brackets take the place of a name.
	// Thinking of it like this will make it seem much more like a function.
	// The parenthesis serve the same purpose in a lambda as they do in a function,
	// for passing parameters.
	// And the curly braces indicate scope.
	// The semicolon at the end is necessary because lambdas are technically
	// objects, not functions, and therefor represent a statement. We essentially just
	// called the constructor of a function object (more on that later).

	// Lets write a lambda that actually does something is time...
	[]( char* name )
	{
		cout << name << " just called a lambda!\n";
	};

	system( "pause" );

	// So running the application will show you that, up to this point, nothing has happened.
	// Thats because, again, we've instantiated a function object. And instantiation doesn't call it.
	// So how do we call it? Assign it to a variable! Right now, lets use the auto keyword and
	// let the compiler figure out the type.

	auto myFunc = []( char* name )
	{
		cout << name << " just called a lambda!\n";
	};

	// Then just use the variable like a function (weird)...

	myFunc( "Will" );

	system( "pause" );

	// Pretty cool?! But what's the point of writing a one-time function in another function?
	// If I left you a voicemail, what am I expecting you to do?

	// ...
	// ...
	// ...
	// ...
	// ...

	// CALLBACK!

	// Yup, these doodads can be super awesome for callbacks. But there's a problem... Lets say we had a
	// class that had a need for a callback at some point. Lets make an object in that class
	// that can hold a lambda. What type would we use? auto is not a type, it is a preprocessor keyword and
	// can only be used in declarations that are immediately initialized or [ other situation that needn't be
	// explained here ].

	// In C++0x, there's only one way to hold these nameless, arbitrarily-typed objects. With function objects
	// from the <functional> header.

	function<void(char*)> myFunc = []( char* name )
		{
			cout << name << " just called a lambda!\n";
		};

	myFunc( "Will, once again," );

	system( "pause" );

	// Common convention is to indent the body by one from the declaration line (to imply scope)

	// It is said that in C++11 they added support for assigning lambdas to function pointers, but I haven't tried
	// this; and i think the function syntax is easier than function pointers.

	// So lets talk about CAPTURE LISTS

	int number = 0;

	auto mutator = []( )
		{
			// number += 5;      << This is totally illegal because the lambda can be called from anywhere, and number
			//						may not exist at the time this is called.
		};

	auto actualMutator = [&]( )
		{
			number += 5;	// This is legal because of the magic & we jammed into the square brackets
			cout << number << endl;
		};

	// The square brackets aren't actually the imaginary name of the function, they represent
	// what's called the capture list: the list of objects this method takes a snapshot of when it is called.

	actualMutator( );
	actualMutator( );
	actualMutator( );

	system( "pause" );

	// Notice how it manipulated the actual variable? Pretty cool! But what if we wanted it to
	// only capture by value?

	auto captureByValue = [=]( )
		{			
			cout << number + 5 << endl;
		};

	captureByValue( );
	captureByValue( );
	captureByValue( );
	
	system("pause");

	// The ampersand and the equals sign capture all variable in scope at the time
	// the lambda is instantiated. The difference is:
	// & captures by reference, making the values modifiable
	// = captures by value, making everything a const object (essentially)

	// So now for the last piece of syntax surrounding lambdas:
	// return values!

	// It's syntactically funky, but conceptually super-cool.

	//								   | The arrow is a symbol that represents the word "becomes"
	auto multiply = []( int a, int b ) -> int
		{
			return a * b;
		};

	int result = multiply( 10, 5 );

	cout << result << endl;

	system( "pause" );

	// So multiply is a function object taking in two ints that "becomes" an int

	// And thats it for syntax! Kind of a lot, but not really too bad if you understand
	// the syntax of a function. Some real-world applications of lambdas include:
	
	// 
	// for_each uses lambdas to iterate objects in stl collections
	//
	
	// 
	// For our collections, I've expanded on this:
	// - we use lambdas in the ForEach method of collections
	// - we us lambdas for querying against our collections when using the Where method
	//         so, in pseudo code:
	// 
	//  ICollection<int> numbers;
	//  numbers.Where( [](int num) { return number > 5; } );
	// 
	// this would return all numbers in the collection that are greater than 5
	// 

	// 
	// the <ppl.h> (Parallel Patterns Library) which we will be using uses lambdas
	//    ppl is for super-stupidly easily parallelism (async code)

	// 
	// task_group myTask;
	// myTask.run( []( )
	//		{
	//			// do some crazy stuff
	//		} );
	// 

	// Callbacks are an awesome use for this

	// So enjoy!

	system("pause");
}