#include <iostream>
using namespace std;

typedef char byte;

struct Footprint sealed
{
	static const unsigned int SizeOfFootprint;
	unsigned short _size;
	unsigned short _reserved;
};

const unsigned int Footprint::SizeOfFootprint = sizeof( Footprint );

__declspec( align( 32 ) ) struct AlignedBlock32 sealed
{
	byte _bytes[32];
};

struct FreeBlock sealed
{
	byte* _start;
	unsigned int _size;

	FreeBlock( byte* start, unsigned int size )
		: _start( start ), _size( size )
	{
	}
};

struct MemorySegmentInformation sealed
{
	MemorySegmentInformation( void* start )
		: StartAddress( start )
	{
	}
	const void* const StartAddress;
	unsigned int Size;
	unsigned int FreeBytes;
	unsigned int Footprints;
	unsigned int FragmentedBytes;
	unsigned int TrailingBytes;
};

class MemorySegment sealed
{
protected:
	byte* _start;
	unsigned int _byteCount;
public:
	MemorySegment( )
	{
	}

	inline void Initialize( byte* start, unsigned int byteCount )
	{
		_start = start;
		_byteCount = byteCount;
		Clear( );
	}

	inline void Clear( )
	{
		if( _start != nullptr && _byteCount > 0 )
		{
			memset( _start, 0, _byteCount );
		}
	}

	inline FreeBlock GetFreeBlock( unsigned int minSize )
	{
#pragma warning( disable:4018 )

		if( _start == nullptr || _byteCount == 0 || minSize >= _byteCount )
		{
			return FreeBlock( nullptr, 0 );
		}
		else
		{
			byte* sptr = _start;
			byte* eptr = sptr;
			byte* end = _start + _byteCount;

			while( sptr >= _start && sptr <= end &&
				   eptr >= _start && eptr <= end &&
				   ( end - sptr ) > minSize )
			{
				if( *sptr == 0 )
				{
					if( eptr < sptr )
					{
						eptr = sptr;
					}
					else
					{
						if( *eptr == 0 )
						{
							eptr++;

							if( ( eptr - sptr ) >= minSize )
							{
								break;
							}
						}
						else
						{
							sptr = eptr;
						}
					}
				}
				else
				{
					sptr += ( ( Footprint* )( sptr ) )->_size + Footprint::SizeOfFootprint;
				}
			}

			if( ( eptr - sptr ) >= minSize )
			{
				return FreeBlock( sptr, minSize );
			}
			else
			{
				return FreeBlock( nullptr, 0 );
			}
		}
#pragma warning( default:4018 )
	}
	
	inline unsigned int FreeBytes( )
	{
#pragma warning( disable:4018 )

		if( _start == nullptr || _byteCount == 0 )
		{
			return 0;
		}
		else
		{
			byte* sptr = _start;
			byte* eptr = sptr;
			byte* end = _start + _byteCount;

			unsigned int freebytes = 0;

			while( sptr >= _start && sptr <= end &&
				   eptr >= _start && eptr <= end )
			{
				if( *sptr == 0 )
				{
					if( eptr < sptr )
					{
						eptr = sptr;
					}
					else
					{
						if( *eptr == 0 )
						{
							eptr++;
						}
						else
						{
							freebytes += eptr - sptr;
							sptr = eptr;
						}
					}
				}
				else
				{
					sptr += ( ( Footprint* )( sptr ) )->_size + Footprint::SizeOfFootprint;
				}
			}

			return freebytes;
		}
#pragma warning( default:4018 )
	}

	inline MemorySegmentInformation GetInfo( )
	{		
#pragma warning( disable:4018 )

		MemorySegmentInformation msi( _start );
		memset( &msi, 0, sizeof( MemorySegmentInformation ) );
		msi.Size = _byteCount;

		if( _start != nullptr && _byteCount != 0 )
		{
			byte* sptr = _start;
			byte* eptr = sptr;
			byte* end = _start + _byteCount;

			while( sptr >= _start && sptr <= end &&
				   eptr >= _start && eptr <= end )
			{
				if( *sptr == 0 )
				{
					if( eptr < sptr )
					{
						eptr = sptr;
					}
					else
					{
						if( *eptr == 0 )
						{
							eptr++;
						}
						else
						{
							// Free bytes
							unsigned int bytes = eptr - sptr;
							msi.FreeBytes += bytes;

							// Fragmented bytes ( segments less than 4 bytes )
							if( bytes < 4 )
							{
								msi.FragmentedBytes += bytes;
							}

							sptr = eptr;
						}
					}
				}
				else
				{
					sptr += ( ( Footprint* )( sptr ) )->_size + Footprint::SizeOfFootprint;
					msi.Footprints++;
				}
			}

			// Trailing Bytes
			if( eptr > sptr )
			{
				msi.TrailingBytes = eptr - sptr;
			}
		}

		return msi;
#pragma warning( default:4018 )
	}
};

class Allocator
{
protected:
	byte* _startAddress;
	byte* _endAddress;
public:
	Allocator( ) { }

	void SetStartAddress( byte* start ) { _startAddress = start; }
	void SetEndAddress( byte* end ) { _endAddress = end; }
};

void PrintMemInfo( MemorySegmentInformation msi )
{
	cout << "MSI for address " << msi.StartAddress << endl;
	cout << "Size  : " << msi.Size << endl;
	cout << "Free  : " << msi.FreeBytes << endl;
	cout << "Objs  : " << msi.Footprints << endl;
	cout << "Frags : " << msi.FragmentedBytes << endl;
	cout << "Trail : " << msi.TrailingBytes << endl;
}

void main()
{
	MemorySegment segment;

	AlignedBlock32* blocks = new AlignedBlock32[16];

	segment.Initialize( ( byte* )blocks, 512 );

	cout << segment.FreeBytes( ) << endl;

	FreeBlock fb = segment.GetFreeBlock( 32 );
	Footprint* header = new ( fb._start ) Footprint;
	header->_size = 32 - Footprint::SizeOfFootprint;
	
	cout << segment.FreeBytes( ) << endl;

	fb = segment.GetFreeBlock( 18 );
	Footprint* header2 = new ( fb._start ) Footprint;
	header2->_size = 18 - Footprint::SizeOfFootprint;
	
	cout << segment.FreeBytes( ) << endl;

	PrintMemInfo( segment.GetInfo( ) );

	system( "pause" );
}