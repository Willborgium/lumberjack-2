﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.IO.MemoryMappedFiles;
using System.Windows;
using System.Windows.Threading;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.InteropServices;

namespace MemoryViewer
{
    public class MainViewModel : NotifyOnPropertyChanged
    {
        #region Fields

        private ObservableCollection<ByteInfo> _bytes;
        private string _mmfileName;
        private MemoryMappedFile _mmfile;
        private MemoryMappedViewStream _mmview;
        private long _startIndex;
        private long _length;
        private bool _isReadingFile;
        private bool _isFileOpen;
        private int _pollingRate;
        private DateTime _lastPoll;
        public int _allocatorKey;
        private string _nameSpaceFileName;
        private int _nameSpaceLength;
        private MemoryMappedFile _nsmmfile;
        private MemoryMappedViewStream _nsmmview;
        private bool _isNameSpaceAvailable;
        private string _rootHeapFileName;
        private int _rootHeapLength;
        private MemoryMappedFile _rootHeapMMFile;
        private MemoryMappedViewStream _rootHeapMMView;
        private bool _isRootHeapAvailable;

        #endregion

        #region Properties

        public ObservableCollection<ByteInfo> Bytes
        {
            get
            {
                return _bytes;
            }
            set
            {
                if (_bytes != value)
                {
                    _bytes = value;
                    OnPropertyChanged("Bytes");
                }
            }
        }

        public string MMFileName
        {
            get
            {
                return _mmfileName;
            }
            set
            {
                if (_mmfileName != value)
                {
                    _mmfileName = value;
                    OnPropertyChanged("MMFileName");
                }
            }
        }
        public MemoryMappedFile MMFile
        {
            get
            {
                return _mmfile;
            }
            set
            {
                if (_mmfile != value)
                {
                    _mmfile = value;
                    OnPropertyChanged("MMFile");
                }
            }
        }
        public MemoryMappedViewStream MMView
        {
            get
            {
                return _mmview;
            }
            set
            {
                if (_mmview != value)
                {
                    _mmview = value;
                    OnPropertyChanged("MMView");
                }
            }
        }

        public long StartIndex
        {
            get
            {
                return _startIndex;
            }
            set
            {
                if (_startIndex != value)
                {
                    _startIndex = value;
                    OnPropertyChanged("StartIndex");
                }
            }
        }
        public long Length
        {
            get
            {
                return _length;
            }
            set
            {
                if (_length != value)
                {
                    _length = value;
                    OnPropertyChanged("Length");
                }
            }
        }

        public bool IsReadingFile
        {
            get
            {
                return _isReadingFile;
            }
            set
            {
                if (_isReadingFile != value)
                {
                    _isReadingFile = value;
                    OnPropertyChanged("IsReadingFile");
                }
            }
        }
        public bool IsFileOpen
        {
            get
            {
                return _isFileOpen;
            }
            set
            {
                if (_isFileOpen != value)
                {
                    _isFileOpen = value;
                    OnPropertyChanged("IsFileOpen");
                }
            }
        }
        public int PollingRate
        {
            get
            {
                return _pollingRate;
            }
            set
            {
                if (_pollingRate != value)
                {
                    _pollingRate = value;
                    OnPropertyChanged("PollingRate");
                }
            }
        }
        public DateTime LastPoll
        {
            get
            {
                return _lastPoll;
            }
            set
            {
                if (_lastPoll != value)
                {
                    _lastPoll = value;
                    OnPropertyChanged("LastPoll");
                }
            }
        }

        public int AllocatorKey
        {
            get
            {
                return _allocatorKey;
            }
            set
            {
                if (_allocatorKey != value)
                {
                    _allocatorKey = value;
                    OnPropertyChanged("AllocatorKey");
                }
            }
        }

        public string NameSpaceFileName
        {
            get
            {
                return _nameSpaceFileName;
            }
            set
            {
                if (_nameSpaceFileName != value)
                {
                    _nameSpaceFileName = value;
                    OnPropertyChanged("NameSpaceFileName");
                }
            }
        }
        public int NameSpaceLength
        {
            get
            {
                return _nameSpaceLength;
            }
            set
            {
                if (_nameSpaceLength != value)
                {
                    _nameSpaceLength = value;
                    OnPropertyChanged("NameSpaceLength");
                }
            }
        }
        public MemoryMappedFile NSMMFile
        {
            get
            {
                return _nsmmfile;
            }
            set
            {
                if (_nsmmfile != value)
                {
                    _nsmmfile = value;
                    OnPropertyChanged("NSMMFile");
                }
            }
        }
        public MemoryMappedViewStream NSMMView
        {
            get
            {
                return _nsmmview;
            }
            set
            {
                if (_nsmmview != value)
                {
                    _nsmmview = value;
                    OnPropertyChanged("NSMMView");
                }
            }
        }

        public string RootHeapFileName
        {
            get
            {
                return _rootHeapFileName;
            }
            set
            {
                if (_rootHeapFileName == value)
                {
                    return;
                }
                _rootHeapFileName = value;
                OnPropertyChanged();
            }
        }

        public int RootHeapLength
        {
            get
            {
                return _rootHeapLength;
            }
            set
            {
                if (_rootHeapLength == value)
                {
                    return;
                }
                _rootHeapLength = value;
                OnPropertyChanged();
            }
        }

        public MemoryMappedFile RootHeapMMFile
        {
            get
            {
                return _rootHeapMMFile;
            }
            set
            {
                if (_rootHeapMMFile == value)
                {
                    return;
                }
                _rootHeapMMFile = value;
                OnPropertyChanged();
            }
        }

        public MemoryMappedViewStream RootHeapMMView
        {
            get
            {
                return _rootHeapMMView;
            }
            set
            {
                if (_rootHeapMMView == value)
                {
                    return;
                }
                _rootHeapMMView = value;
                OnPropertyChanged();
            }
        }

        public bool IsRootHeapAvailable
        {
            get
            {
                return _isRootHeapAvailable;
            }
            set
            {
                if (_isRootHeapAvailable == value)
                {
                    return;
                }
                _isRootHeapAvailable = value;
                OnPropertyChanged();
            }
        }
        
        #endregion

        #region Constructor

        public MainViewModel()
        {
            MMFileName = "HyJynx Heap";
            StartIndex = 0;
            Length = 8192;
            AllocatorKey = 7;
            PollingRate = 1000;
            NameSpaceFileName = "HyJynx Namespace Heap";
            NameSpaceLength = 8192;
            _isNameSpaceAvailable = false;
            RootHeapFileName = "HyJynx Root Heap";
            RootHeapLength = 1200;
            IsRootHeapAvailable = false;
        }

        #endregion

        #region Methods

        #region Publics

        public void OpenFile()
        {
            try
            {
                if (MMFileName != null)
                {
                    MMFile = MemoryMappedFile.OpenExisting(MMFileName);
                }
                else
                {
                    MessageBox.Show("You must provide a file name first!");
                    return;
                }

                if (Length == 0)
                {
                    MessageBox.Show("You must provide a length greater than zero!");
                    return;
                }

                MMView = MMFile.CreateViewStream(StartIndex, Length);
                
                IsFileOpen = true;

                if (NameSpaceFileName != null && NameSpaceFileName.Length > 0)
                {
                    try
                    {
                        NSMMFile = MemoryMappedFile.OpenExisting(NameSpaceFileName);
                        NSMMView = NSMMFile.CreateViewStream(0, NameSpaceLength);
                        _isNameSpaceAvailable = true;
                    }
                    catch (Exception err)
                    {
                        MessageBox.Show("Unable to read name space:\n" + err.Message);
                        _isNameSpaceAvailable = false;
                    }
                }     
           
                if(!string.IsNullOrWhiteSpace(RootHeapFileName))
                {
                    try
                    {
                        RootHeapMMFile = MemoryMappedFile.OpenExisting(RootHeapFileName);
                        RootHeapMMView = RootHeapMMFile.CreateViewStream(0, RootHeapLength);
                        IsRootHeapAvailable = true;
                    }
                    catch (Exception err)
                    {
                        MessageBox.Show("Unable to read root heap:\n" + err.Message);
                        IsRootHeapAvailable = false;
                    }
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "Unhandled Exception in OpenFile");
                IsFileOpen = false;
            }
        }

        public void CloseFile()
        {
            if (MMFile != null && IsFileOpen)
            {
                EndReadFile();
                IsFileOpen = false;
                MMView.Close();
                MMView.Dispose();
                MMView = null;
                MMFile.Dispose();
                MMFile = null;
                if (_isNameSpaceAvailable)
                {
                    NSMMView.Close();
                    NSMMView.Dispose();
                    NSMMView = null;
                    NSMMFile.Dispose();
                    NSMMFile = null;
                    _isNameSpaceAvailable = false;
                }
            }
        }

        public void BeginReadFileAsync()
        {
            if (!IsReadingFile && MMView != null && IsFileOpen)
            {
                IsReadingFile = true;

                Task.Factory.StartNew(() =>
                    {
                        while (IsReadingFile)
                        {
                            if (DateTime.Now > LastPoll.AddMilliseconds(PollingRate))
                            {
                                ReadFile();
                                _lastPoll = DateTime.Now;
                            }
                        }
                    });
            }
            else
            {
                MessageBox.Show("You must first open a file for reading.", "Unable to begin polling");
            }
        }

        public void EndReadFile()
        {
            IsReadingFile = false;
        }

        #endregion

        #region Privates
        
        private string GetTypeName(byte[] bytes, uint offset)
        {
            string output = string.Empty;

            while (offset < bytes.Length && bytes[offset] != 0)
            {
                output += Convert.ToChar(bytes[offset]);
                offset++;
            }

            return output;
        }

        private void ReadFile()
        {
            List<ByteInfo> measuredBytes = new List<ByteInfo>((int)Length);

            byte[] bytes = new byte[Length];

            MMView.Position = 0;
            MMView.Read(bytes, 0, (int)Length);

            byte[] nsBytes = null;

            if(_isNameSpaceAvailable)
            {
                nsBytes = new byte[NameSpaceLength];
                NSMMView.Position = 0;
                NSMMView.Read(nsBytes, 0, NameSpaceLength);
            }

            for (int index = 0; index < Length; )
            {
                int value = (int)bytes[index];

                if (value != 0 && value == AllocatorKey)
                {
                    measuredBytes.Add(new ByteInfo() { Value = (int)bytes[index++], ByteType = ByteDescription.HeaderByte | ByteDescription.AllocatorKey, TypeName = "char" });
                    measuredBytes.Add(new ByteInfo() { Value = (int)bytes[index++], ByteType = ByteDescription.HeaderByte | ByteDescription.Marking, TypeName = "char" });
                    measuredBytes.Add(new ByteInfo() { Value = (int)bytes[index++], ByteType = ByteDescription.HeaderByte | ByteDescription.Usage, TypeName = "char" });
                    measuredBytes.Add(new ByteInfo() { Value = (int)bytes[index++], ByteType = ByteDescription.HeaderByte | ByteDescription.Unused, TypeName = "char" });

                    for (int dmIndex = 0; dmIndex < 4; dmIndex++)
                    {
                        measuredBytes.Add(new ByteInfo() { Value = (int)bytes[index++], ByteType = ByteDescription.HeaderByte | ByteDescription.DestructorMapping, TypeName = "void*" });
                    }
                    
                    uint objSize = BitConverter.ToUInt32(bytes, index);

                    for (uint objIndex = 0; objIndex < 4; objIndex++)
                    {
                        measuredBytes.Add(new ByteInfo() { Value = (int)bytes[index++], ByteType = ByteDescription.HeaderByte | ByteDescription.ObjectSize, TypeName = "uint" });
                    }

                    string typeName = "Unidentified Type";

                    uint nameOffset = BitConverter.ToUInt32(bytes, index);
                    for (uint nameIndex = 0; nameIndex < 4; nameIndex++)
                    {
                        measuredBytes.Add(new ByteInfo() { Value = (int)bytes[index++], ByteType = ByteDescription.HeaderByte | ByteDescription.NameIndex, TypeName = "uint" });
                    }

                    if (_isNameSpaceAvailable)
                    {
                        typeName = GetTypeName(nsBytes, nameOffset);
                    }

                    for (int objIndex = 0; objIndex < objSize; objIndex++)
                    {
                        measuredBytes.Add(new ByteInfo() { Value = (int)bytes[index++], ByteType = ByteDescription.DataByte, TypeName = typeName });
                    }
                }
                else
                {
                    if (value == 0)
                    {
                        measuredBytes.Add(new ByteInfo() { Value = (int)bytes[index++], ByteType = ByteDescription.FreeByte, TypeName = "No type" });
                    }
                    else
                    {
                        measuredBytes.Add(new ByteInfo() { Value = (int)bytes[index++], ByteType = ByteDescription.Unknown, TypeName = "Unknown" });
                    }
                }
            }

            if (Bytes == null)
            {
                for (int index = 0; index < Length; index++)
                {
                    measuredBytes[index].Previous = index == 0 ? null : measuredBytes[index - 1];
                    measuredBytes[index].Next = index == Length - 1 ? null : measuredBytes[index + 1];
                }

                UIDispatcher.BeginInvoke(new Action(() =>
                {
                    Bytes = new ObservableCollection<ByteInfo>(measuredBytes);
                }));
            }
            else
            {
                for (int index = 0; index < Length; index++)
                {
                    Bytes[index].ByteType = measuredBytes[index].ByteType;
                    Bytes[index].IsHovered = measuredBytes[index].IsHovered;
                    Bytes[index].TypeName = measuredBytes[index].TypeName;
                    Bytes[index].Value = measuredBytes[index].Value;
                    Bytes[index].OnPropertyChanged("Self");
                }
            }

            byte[] rootBytes = null;
            
            if (IsRootHeapAvailable)
            {
                rootBytes = new byte[RootHeapLength];
                RootHeapMMView.Position = 0;
                RootHeapMMView.Read(rootBytes, 0, RootHeapLength);


            }
        }

        #endregion

        #endregion
    }
}