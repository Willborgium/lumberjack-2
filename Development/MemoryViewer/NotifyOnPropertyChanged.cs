﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Threading;
using System;

namespace MemoryViewer
{
    public class NotifyOnPropertyChanged : INotifyPropertyChanged
    {
        private IDictionary<string, PropertyChangedEventArgs> _args;
        private static Dispatcher _uiDispatcher;

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        public NotifyOnPropertyChanged()
        {
            _args = new Dictionary<string, PropertyChangedEventArgs>();
        }

        public static Dispatcher UIDispatcher
        {
            get
            {
                return _uiDispatcher;
            }
            set
            {
                _uiDispatcher = value;
            }
        }

        public void OnPropertyChanged(string property = "")
        {
            PropertyChangedEventArgs args = null;

            if (!_args.ContainsKey(property))
            {
                args = new PropertyChangedEventArgs(property);
                _args.Add(property, args);
            }
            else
            {
                args = _args[property];
            }

            if (Dispatcher.CurrentDispatcher == UIDispatcher)
            {
                PropertyChanged(this, args);
            }
            else
            {
                UIDispatcher.Invoke(new Action(() =>
                {
                    PropertyChanged(this, _args[property]);
                }));
            }
        }
    }
}
