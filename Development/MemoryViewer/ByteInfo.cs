﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace MemoryViewer
{
    [Flags]
    public enum ByteDescription
    {
        FreeByte = 0x1,
        DataByte = 0x2,
        HeaderByte = 0x4,
        AllocatorKey = 0x8,
        Marking = 0x10,
        Usage = 0x20,
        Unused = 0x40,
        DestructorMapping = 0x80,
        ObjectSize = 0x100,
        NameIndex = 0x200,
        Unknown = 0x400
    }

    public enum MarkingValue
    {
        Untouched = 0,
        Touched
    }

    public enum UsageValue
    {
        Default = 0,
        Constant,
        Fixed,
        Permanent
    }

    public class ByteInfo : NotifyOnPropertyChanged
    {
        private int _value;
        private ByteDescription _byteType;
        private ByteInfo _self;
        private string _typeName;        

        public int Value
        {
            get
            {
                return _value;
            }
            set
            {
                if (_value != value)
                {
                    _value = value;
                    OnPropertyChanged("Value");
                }
            }
        }

        public ByteDescription ByteType
        {
            get
            {
                return _byteType;
            }
            set
            {
                if (_byteType != value)
                {
                    _byteType = value;
                    OnPropertyChanged("ByteType");
                }
            }
        }

        public ByteInfo Self
        {
            get
            {
                return _self;
            }
            set
            {
                if (_self != value)
                {
                    _self = value;
                    OnPropertyChanged("Self");
                }
            }
        }

        public string TypeName
        {
            get
            {
                return _typeName;
            }
            set
            {
                if (_typeName != value)
                {
                    _typeName = value;
                    OnPropertyChanged("TypeName");
                }
            }
        }

        public bool IsHovered { get; set; }

        public ByteInfo Previous { get; set; }
        public ByteInfo Next { get; set; }

        public ByteInfo()
        {
            Self = this;
        }
    }
}
