﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MemoryViewer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainViewModel ViewModel
        {
            get
            {
                return this.DataContext as MainViewModel;
            }
        }

        public MainWindow()
        {
            InitializeComponent();

            NotifyOnPropertyChanged.UIDispatcher = Dispatcher;

            this.DataContext = new MainViewModel();
        }

        public void OnOpenFile(object sender, EventArgs args)
        {
            if (ViewModel != null)
            {
                ViewModel.OpenFile();
            }
        }

        public void OnCloseFile(object sender, EventArgs args)
        {
            if (ViewModel != null)
            {
                ViewModel.CloseFile();
            }
        }

        public void OnStartReading(object sender, EventArgs args)
        {
            if (ViewModel != null)
            {
                ViewModel.BeginReadFileAsync();
            }
        }

        public void OnStopReading(object sender, EventArgs args)
        {
            if (ViewModel != null)
            {
                ViewModel.EndReadFile();
            }
        }

        private void SetHovered(ByteInfo info, bool hovered)
        {
            if (info.ByteType.HasFlag(ByteDescription.HeaderByte))
            {
                var markBytes = ByteUtils.GetBytesOfType(info, info.ByteType ^ ByteDescription.HeaderByte);
                foreach (var b in markBytes)
                {
                    b.IsHovered = hovered;
                    b.OnPropertyChanged();
                }
            }
            else
            {
                var markBytes = ByteUtils.GetBytesOfType(info, info.ByteType);
                foreach (var b in markBytes)
                {
                    b.IsHovered = hovered;
                    b.OnPropertyChanged();
                }
            }
        }

        private void OnByteHovered(object sender, MouseEventArgs e)
        {
            var element = sender as FrameworkElement;

            if (element != null)
            {
                var info = element.Tag as ByteInfo;

                if (info != null)
                {
                    SetHovered(info, true);
                }
            }
        }

        private void OnByteUnhovered(object sender, MouseEventArgs e)
        {
            var element = sender as FrameworkElement;

            if (element != null)
            {
                var info = element.Tag as ByteInfo;

                if (info != null)
                {
                    SetHovered(info, false);
                }
            }
        }

        private void OnTitleBarMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                if (e.ClickCount == 2)
                {
                    switch (this.WindowState)
                    {
                        case WindowState.Maximized:
                            this.WindowState = WindowState.Normal;
                            break;
                        default:
                            this.WindowState = WindowState.Maximized;
                            break;
                    }
                }
                else
                {
                    Application.Current.MainWindow.DragMove();
                }
            }
        }

        private void OnClose(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
