﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows;

namespace MemoryViewer
{
    public class ByteUtils
    {
        private IDictionary<ByteDescription, SolidColorBrush> _borderColors;
        private IDictionary<ByteDescription, SolidColorBrush> _backgroundColors;

        private static ByteUtils _instance;
        private static ByteUtils Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ByteUtils();
                }
                return _instance;
            }
        }

        private ByteUtils()
        {
            InitializeBorderColors();
            InitializeBackgroundColors();
        }

        private void InitializeBorderColors()
        {
            _borderColors = new Dictionary<ByteDescription, SolidColorBrush>();

            _borderColors.Add(ByteDescription.HeaderByte, new SolidColorBrush(Colors.Maroon));
            _borderColors.Add(ByteDescription.AllocatorKey, new SolidColorBrush(Colors.Maroon));
            _borderColors.Add(ByteDescription.Marking, new SolidColorBrush(Colors.Maroon));
            _borderColors.Add(ByteDescription.Usage, new SolidColorBrush(Colors.Maroon));
            _borderColors.Add(ByteDescription.Unused, new SolidColorBrush(Colors.Maroon));
            _borderColors.Add(ByteDescription.DestructorMapping, new SolidColorBrush(Colors.Maroon));
            _borderColors.Add(ByteDescription.ObjectSize, new SolidColorBrush(Colors.Maroon));
            _borderColors.Add(ByteDescription.NameIndex, new SolidColorBrush(Colors.Maroon));

            _borderColors.Add(ByteDescription.DataByte, new SolidColorBrush(Colors.Black));
            _borderColors.Add(ByteDescription.FreeByte, new SolidColorBrush(Colors.White));
            _borderColors.Add(ByteDescription.Unknown, new SolidColorBrush(Colors.Red));
        }

        private void InitializeBackgroundColors()
        {
            _backgroundColors = new Dictionary<ByteDescription, SolidColorBrush>();

            _backgroundColors.Add(ByteDescription.HeaderByte, new SolidColorBrush(Color.FromArgb(255, 150, 128, 128)));
            _backgroundColors.Add(ByteDescription.AllocatorKey, new SolidColorBrush(Color.FromArgb(255, 160, 128, 128)));
            _backgroundColors.Add(ByteDescription.Marking, new SolidColorBrush(Color.FromArgb(255, 170, 128, 128)));
            _backgroundColors.Add(ByteDescription.Usage, new SolidColorBrush(Color.FromArgb(255, 180, 128, 128)));
            _backgroundColors.Add(ByteDescription.Unused, new SolidColorBrush(Color.FromArgb(255, 190, 128, 128)));
            _backgroundColors.Add(ByteDescription.DestructorMapping, new SolidColorBrush(Color.FromArgb(255, 208, 128, 128)));
            _backgroundColors.Add(ByteDescription.ObjectSize, new SolidColorBrush(Color.FromArgb(255, 224, 128, 128)));
            _backgroundColors.Add(ByteDescription.NameIndex, new SolidColorBrush(Color.FromArgb(255, 255, 128, 128)));

            _backgroundColors.Add(ByteDescription.DataByte, new SolidColorBrush(Colors.LightGray));
            _backgroundColors.Add(ByteDescription.FreeByte, new SolidColorBrush(Colors.White));
            _backgroundColors.Add(ByteDescription.Unknown, new SolidColorBrush(Colors.Red));
        }

        public static SolidColorBrush GetBorderColor(ByteDescription desc)
        {
            ByteDescription flagVal = ByteDescription.Unknown;

            if (desc.HasFlag(ByteDescription.HeaderByte))
            {
                flagVal = desc ^ ByteDescription.HeaderByte;
            }
            else
            {
                flagVal = desc;
            }

            return Instance._borderColors[flagVal];
        }

        public static SolidColorBrush GetBackgroundColor(ByteDescription desc)
        {
            ByteDescription flagVal = ByteDescription.Unknown;

            if (desc.HasFlag(ByteDescription.HeaderByte))
            {
                flagVal = desc ^ ByteDescription.HeaderByte;
            }
            else
            {
                flagVal = desc;
            }

            return Instance._backgroundColors[flagVal];
        }

        public static ByteInfo[] GetBytesOfType(ByteInfo search, ByteDescription type)
        {
            List<ByteInfo> output = new List<ByteInfo>();

            ByteInfo ptr = search;

            while (ptr.Previous != null && ptr.Previous.ByteType.HasFlag(type))
            {
                ptr = ptr.Previous;
            }

            while (ptr != null && ptr.ByteType.HasFlag(type))
            {
                output.Add(ptr);
                ptr = ptr.Next;
            }

            return output.ToArray();
        }
    }

    public class ByteBorderBrushConverter : IValueConverter
    {
        private SolidColorBrush _hovered;

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            ByteInfo info = value as ByteInfo;

            if (info != null)
            {
                if (info.IsHovered)
                {
                    if(_hovered == null)
                    {
                        _hovered = new SolidColorBrush(Colors.Gold);
                    }
                    return _hovered;
                }

                return ByteUtils.GetBorderColor(info.ByteType);
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ByteBackgroundConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            ByteInfo info = value as ByteInfo;

            if (info != null)
            {
                return ByteUtils.GetBackgroundColor(info.ByteType);
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ByteBorderThicknessConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            ByteInfo info = value as ByteInfo;

            if (info != null)
            {
                var output = new Thickness(2.0);

                return output;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ByteTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var info = value as ByteInfo;

            var output = string.Empty;

            if (info != null)
            {
                if (info.ByteType.HasFlag(ByteDescription.HeaderByte))
                {
                    output += string.Format("Header [{0}]", (info.ByteType ^ ByteDescription.HeaderByte).ToString().Replace("Byte", ""));
                }
                else
                {
                    output = info.ByteType.ToString().Replace("Byte", "");
                }
            }

            return output;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ByteValueConverter : IValueConverter
    {
        private string GetDestructorPtrValue(ByteInfo b)
        {
            var dBytes = ByteUtils.GetBytesOfType(b, ByteDescription.DestructorMapping).Select(e => (byte)e.Value).ToArray();

            return string.Format("0x{0}", BitConverter.ToInt32(dBytes, 0).ToString("x8"));
        }

        private string GetObjectSize(ByteInfo b)
        {
            var sBytes = ByteUtils.GetBytesOfType(b, ByteDescription.ObjectSize).Select(e => (byte)e.Value).ToArray();

            return string.Format("{0} bytes", BitConverter.ToUInt32(sBytes, 0));
        }

        private string GetNameOffset(ByteInfo b)
        {
            var oBytes = ByteUtils.GetBytesOfType(b, ByteDescription.NameIndex).Select(e => (byte)e.Value).ToArray();

            return BitConverter.ToUInt32(oBytes, 0).ToString();
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var info = value as ByteInfo;

            var output = string.Empty;

            if (info != null)
            {
                if (info.ByteType.HasFlag(ByteDescription.HeaderByte))
                {
                    switch (info.ByteType ^ ByteDescription.HeaderByte)
                    {
                        case ByteDescription.DestructorMapping:
                            output = GetDestructorPtrValue(info);
                            break;
                        case ByteDescription.ObjectSize:
                            output = GetObjectSize(info);
                            break;
                        case ByteDescription.AllocatorKey:
                        case ByteDescription.Unused:
                            output = info.Value.ToString();
                            break;
                        case ByteDescription.Marking:
                            output = ((MarkingValue)info.Value).ToString();
                            break;
                        case ByteDescription.Usage:
                            output = ((UsageValue)info.Value).ToString();
                            break;
                        case ByteDescription.NameIndex:
                            output = GetNameOffset(info);
                            break;
                    }
                }
                else if (info.ByteType.HasFlag(ByteDescription.FreeByte))
                {
                    return string.Format("{0} free bytes", ByteUtils.GetBytesOfType(info, ByteDescription.FreeByte).Count());
                }
                else
                {
                    output = info.Value.ToString();
                }
            }

            return output;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
