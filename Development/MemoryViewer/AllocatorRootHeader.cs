﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MemoryViewer
{
    public class AllocatorRootHeader : NotifyOnPropertyChanged
    {
        private byte _allocatorKey;
        private byte _marking;
        private byte _usage;
        private byte _reserved;
        private IntPtr _destructorMapping;
        private uint _size;
        private uint _nameIndex;
        private string _namespaceName;
        private IntPtr _address;
        private IntPtr _namePtr;

        public byte AllocatorKey
        {
            get
            {
                return _allocatorKey;
            }
            set
            {
                if (_allocatorKey == value)
                {
                    return;
                }
                _allocatorKey = value;
                OnPropertyChanged();
            }
        }

        public byte Marking
        {
            get
            {
                return _marking;
            }
            set
            {
                if (_marking == value)
                {
                    return;
                }
                _marking = value;
                OnPropertyChanged();
            }
        }

        public byte Usage
        {
            get
            {
                return _usage;
            }
            set
            {
                if (_usage == value)
                {
                    return;
                }
                _usage = value;
                OnPropertyChanged();
            }
        }

        public byte Reserved
        {
            get
            {
                return _reserved;
            }
            set
            {
                if (_reserved == value)
                {
                    return;
                }
                _reserved = value;
                OnPropertyChanged();
            }
        }

        public IntPtr DestructorMapping
        {
            get
            {
                return _destructorMapping;
            }
            set
            {
                if (_destructorMapping == value)
                {
                    return;
                }
                _destructorMapping = value;
                OnPropertyChanged();
            }
        }

        public uint Size
        {
            get
            {
                return _size;
            }
            set
            {
                if (_size == value)
                {
                    return;
                }
                _size = value;
                OnPropertyChanged();
            }
        }

        public uint NameIndex
        {
            get
            {
                return _nameIndex;
            }
            set
            {
                if (_nameIndex == value)
                {
                    return;
                }
                _nameIndex = value;
                OnPropertyChanged();
            }
        }

        public string NamespaceName
        {
            get
            {
                return _namespaceName;
            }
            set
            {
                if (_namespaceName == value)
                {
                    return;
                }
                _namespaceName = value;
                OnPropertyChanged();
            }
        }

        public IntPtr Address
        {
            get
            {
                return _address;
            }
            set
            {
                if (_address == value)
                {
                    return;
                }
                _address = value;
                OnPropertyChanged();
            }
        }

        public IntPtr NamePtr
        {
            get
            {
                return _namePtr;
            }
            set
            {
                if (_namePtr == value)
                {
                    return;
                }
                _namePtr = value;
                OnPropertyChanged();
            }
        }

        AllocatorRootHeader()
        {
        }
    }
}
