﻿using System.Windows;
using CDV3.ViewModels;

namespace CDV3
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            DataContext = new MainWindowViewModel(this);
            MemoryViewerControl.ViewModel = new MemoryViewerViewModel(null, DataContext as MainWindowViewModel);
        }
    }
}
