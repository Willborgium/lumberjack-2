﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CDV3.ViewModels;
using System.Collections.ObjectModel;

namespace CDV3.DataContracts
{
    public class HeaderedStructure : NotifyPropertyChanged
    {
        private byte[] _bytes;

        public byte[] Bytes
        {
            get
            {
                return _bytes;
            }
            set
            {
                _bytes = value;
            }
        }
        public int Size
        {
            get
            {
                return Bytes != null ? Bytes.Length : 0;
            }
        }

        public HeaderedStructure(byte[] bytes)
        {
            Bytes = bytes;
        }

        public void NotifyUI()
        {
            OnPropertyChanged("Bytes");
            OnPropertyChanged("Size");
        }
    }

    public class AllocationGroup : NotifyPropertyChanged
    {
        private int _groupID;
        private List<HeaderedStructure> _structures;

        public int GroupID
        {
            get
            {
                return _groupID;
            }
            set
            {
                _groupID = value;
            }
        }
        public List<HeaderedStructure> Structures
        {
            get
            {
                return _structures;
            }
            set
            {
                _structures = value;
            }
        }
        public int StructureCount
        {
            get
            {
                return Structures != null ? Structures.Count : 0;
            }
        }
        public int StructureBytes
        {
            get
            {
                if (Structures != null)
                {
                    int number = 0;
                    foreach (HeaderedStructure structure in Structures)
                    {
                        number += structure.Size;
                    }
                    return number;
                }
                else
                {
                    return 0;
                }
            }
        }
        public int SmallestStructureSize
        {
            get
            {
                if (Structures != null && Structures.Count > 0)
                {
                    return Structures.OrderBy(e => e.Size).ToArray()[0].Size;
                }
                else
                {
                    return 0;
                }
            }
        }
        public int LargestStructureSize
        {
            get
            {
                if (Structures != null && Structures.Count > 0)
                {
                    return Structures.OrderBy(e => -e.Size).ToArray()[0].Size;
                }
                else
                {
                    return 0;
                }
            }
        }

        public AllocationGroup()
        {
            Structures = new List<HeaderedStructure>();
        }

        public void NotifyUI()
        {
            OnPropertyChanged("GroupID");
            OnPropertyChanged("Structures");
            OnPropertyChanged("StructureCount");
            OnPropertyChanged("StructureBytes");
            OnPropertyChanged("SmallestStructureSize");
            OnPropertyChanged("LargestStructureSize");
            foreach (HeaderedStructure hs in Structures)
            {
                hs.NotifyUI();
            }
        }
    }

    public class MemoryStatistics : NotifyPropertyChanged
    {
        private List<AllocationGroup> _allocationGroups;
        public List<AllocationGroup> AllocationGroups
        {
            get
            {
                return _allocationGroups;
            }
            set
            {
                _allocationGroups = value;
            }
        }

        public MemoryStatistics()
        {
            AllocationGroups = new List<AllocationGroup>();
        }

        public void AddStructure(int groupid, byte[] bytes)
        {
            AllocationGroup ag = (from g in AllocationGroups where g.GroupID == groupid select g).FirstOrDefault();

            if (ag == null)
            {
                ag = new AllocationGroup();
                ag.GroupID = groupid;
                AllocationGroups.Add(ag);
                AllocationGroups = (List<AllocationGroup>)AllocationGroups.OrderBy(e => e.GroupID).ToList();
            }

            ag.Structures.Add(new HeaderedStructure(bytes));
        }

        public void NotifyUI()
        {
            OnPropertyChanged("AllocationGroups");
            foreach (AllocationGroup ag in AllocationGroups)
            {
                ag.NotifyUI();
            }
        }
    }
}
