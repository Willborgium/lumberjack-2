﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows.Controls;

namespace CDV3.ViewModels
{
    /// <summary>
    /// Provides a base for all view models with an OnPropertyChanged method as well as
    /// Parent and Owner properties.
    /// </summary>
    public class ViewModelBase : NotifyPropertyChanged
    {
        #region Members

        private UserControl _owner;
        private ViewModelBase _parent;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the UserControl for which this view model is the data context.
        /// </summary>
        public UserControl Owner
        {
            get
            {
                return _owner;
            }
            set
            {
                _owner = value;
                OnPropertyChanged("Owner");
            }
        }

        /// <summary>
        /// Gets or sets the view model that created this view model.
        /// </summary>
        public ViewModelBase Parent
        {
            get
            {
                return _parent;
            }
            set
            {
                _parent = value;
                OnPropertyChanged("Parent");
            }
        }

        #endregion

        #region Constructor

        public ViewModelBase(UserControl owner, ViewModelBase parent)
        {
            Owner = owner;
            Parent = parent;
        }

        #endregion
    }
}
