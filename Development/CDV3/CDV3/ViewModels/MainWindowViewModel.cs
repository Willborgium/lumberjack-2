﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace CDV3.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        #region Members

        private MainWindow _window;

        #endregion

        #region Properties

        public MainWindow Window
        {
            get
            {
                return _window;
            }
            set
            {
                _window = value;
                OnPropertyChanged("Window");
            }
        }

        #endregion

        #region Constructor

        public MainWindowViewModel(MainWindow window)
            : base(null, null)
        {
        }

        #endregion
    }
}
