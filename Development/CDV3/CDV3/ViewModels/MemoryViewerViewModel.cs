﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Threading;
using System.Windows;
using System.IO.MemoryMappedFiles;
using System.IO;
using CDV3.DataContracts;
using System.Collections.ObjectModel;

namespace CDV3.ViewModels
{
    public class MemoryViewerViewModel : ViewModelBase
    {
        #region Members

        private string _mMFName;
        private bool _shouldPoll;
        private int _headerSize;
        private int _structureSizeOffset;
        private int _allocationGroupOffset;
        private MemoryStatistics _statistics;
        private int _heapSize;
        private int _pollingInterval;
        private bool _shouldRecord;
        private List<MemoryStatistics> _records;

        #endregion

        #region Properties

        public string MMFName
        {
            get
            {
                return _mMFName;
            }
            set
            {
                _mMFName = value;
                OnPropertyChanged("MMFName");
            }
        }
        public bool ShouldPoll
        {
            get
            {
                return _shouldPoll;
            }
            set
            {
                _shouldPoll = value;
                OnPropertyChanged("ShouldPoll");
                OnPropertyChanged("CanUserEditNumbers");
            }
        }
        public bool CanUserEditNumbers
        {
            get
            {
                return !ShouldPoll;
            }
        }
        public int HeapSize
        {
            get
            {
                return _heapSize;
            }
            set
            {
                _heapSize = value;
                OnPropertyChanged("HeapSize");
            }
        }
        public int PollingInterval
        {
            get
            {
                return _pollingInterval;
            }
            set
            {
                _pollingInterval = value;
                OnPropertyChanged("PollingInterval");
            }
        }
        public bool ShouldRecord
        {
            get
            {
                return _shouldRecord;
            }
            set
            {
                _shouldRecord = value;
                OnPropertyChanged("ShouldRecord");
            }
        }
        public List<MemoryStatistics> Records
        {
            get
            {
                return _records;
            }
            set
            {
                _records = value;
                OnPropertyChanged("Records");
            }
        }

        public MemoryStatistics Statistics
        {
            get
            {
                return _statistics;
            }
            set
            {
                _statistics = value;
                OnPropertyChanged("Statistics");
            }
        }

        #region Header Information Properties

        public int HeaderSize
        {
            get
            {
                return _headerSize;
            }
            set
            {
                _headerSize = value;
                OnPropertyChanged("HeaderSize");
            }
        }
        public int StructureSizeOffset
        {
            get
            {
                return _structureSizeOffset;
            }
            set
            {
                _structureSizeOffset = value;
                OnPropertyChanged("StructureSizeOffset");
            }
        }
        public int AllocationGroupOffset
        {
            get
            {
                return _allocationGroupOffset;
            }
            set
            {
                _allocationGroupOffset = value;
                OnPropertyChanged("AllocationGroupOffset");
            }
        }

        #endregion

        #endregion

        #region Constructor

        public MemoryViewerViewModel(UserControl owner, ViewModelBase parent)
            : base(owner, parent)
        {
            ShouldPoll = false;
            HeapSize = 100000;
            PollingInterval = 250;
            MMFName = "MemoryManager";
            ShouldRecord = false;

            Statistics = new MemoryStatistics();

            if (File.Exists(Properties.Settings.Default.DefaultHeaderInformationFilePath))
            {
                LoadHeaderInformation();
            }
        }

        #endregion

        #region Methods

        #region Private Methods

        private void PollMemory(ref byte[] bytes, ref MemoryStatistics statistics)
        {
            int index = 0;

            while (index < bytes.Length)
            {
                int byteValue = Convert.ToInt32(bytes[index]);

                if (byteValue != 0)
                {
                    statistics.AddStructure(Convert.ToInt32(bytes[index + AllocationGroupOffset]), bytes.Skip(index).Take(Convert.ToInt32(bytes[index + StructureSizeOffset])).ToArray());
                    index += Convert.ToInt32(bytes[index + StructureSizeOffset]);
                }
                else
                {
                    index++;
                }
            }
        }

        private void UpdateUI(ref MemoryStatistics statistics)
        {
            Statistics = statistics;

            if (ShouldRecord)
            {
                Records.Add(Statistics);
            }

            Statistics.NotifyUI();
            OnPropertyChanged("Statistics");
        }

        #endregion

        #region Public Methods

        public void StartPolling()
        {
            if (HeapSize <= 0)
            {
                MessageBox.Show("Heap Size must be greater than zero.", "Heap Size Too Small...", MessageBoxButton.OK, MessageBoxImage.Stop);
                return;
            }

            if (!ShouldPoll)
            {
                Dispatcher uithread = Dispatcher.CurrentDispatcher;
                ShouldPoll = true;

                if (ShouldRecord)
                {
                    Records = new List<MemoryStatistics>();
                }

                Task.Factory.StartNew(() =>
                    {
                        try
                        {
                            MemoryMappedFile file = MemoryMappedFile.OpenExisting(MMFName);
                            MemoryMappedViewAccessor view = file.CreateViewAccessor();

                            byte[] vals = new byte[HeapSize];

                            while (ShouldPoll)
                            {
                                if (PollingInterval > 0)
                                {
                                    Thread.Sleep(PollingInterval);
                                }
                                view.ReadArray<byte>(0, vals, 0, HeapSize);

                                MemoryStatistics stats = new MemoryStatistics();

                                PollMemory(ref vals, ref stats);

                                uithread.Invoke(new Action(() =>
                                    {
                                        UpdateUI(ref stats);
                                    }));
                            }
                        }
                        catch (Exception error)
                        {
                            uithread.BeginInvoke(new Action(() =>
                                {
                                    MessageBox.Show(error.Message, "Unable to start polling...", MessageBoxButton.OK, MessageBoxImage.Stop);
                                    ShouldPoll = false;
                                }));
                        }
                    });
            }
        }

        public void StopPolling()
        {
            ShouldPoll = false;
        }

        public void LoadHeaderInformation()
        {
            StreamReader sr = new StreamReader(Properties.Settings.Default.DefaultHeaderInformationFilePath);

            while (!sr.EndOfStream)
            {
                string[] parts = sr.ReadLine().Split('=');

                switch (parts[0])
                {
                    case "HeaderSize":
                        HeaderSize = Convert.ToInt32(parts[1]);
                        break;
                    case "StructureSizeOffset":
                        StructureSizeOffset = Convert.ToInt32(parts[1]);
                        break;
                    case "AllocationGroupOffset":
                        AllocationGroupOffset = Convert.ToInt32(parts[1]);
                        break;
                }
            }

            sr.Close();
        }

        public void SaveHeaderInformation()
        {
            string output = "HeaderSize={0}\nStructureSizeOffset={1}\nAllocationGroupOffset={2}";

            StreamWriter sw = new StreamWriter(Properties.Settings.Default.DefaultHeaderInformationFilePath);
            sw.Write(string.Format(output, new object[] { HeaderSize, StructureSizeOffset, AllocationGroupOffset }));
            sw.Close();
        }

        #endregion

        #endregion

    }
}
