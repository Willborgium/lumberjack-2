﻿using System.Windows.Controls;
using System;
using CDV3.ViewModels;

namespace CDV3.Views
{
    public partial class MemoryViewerView : UserControl
    {
        public MemoryViewerViewModel ViewModel
        {
            get
            {
                return DataContext as MemoryViewerViewModel;
            }
            set
            {
                DataContext = value;
            }
        }

        public MemoryViewerView()
        {
            InitializeComponent();
        }

        public void StartPolling(object sender, EventArgs e)
        {
            if (ViewModel != null)
            {
                ViewModel.StartPolling();
            }
        }

        public void StopPolling(object sender, EventArgs e)
        {
            if (ViewModel != null)
            {
                ViewModel.StopPolling();
            }
        }

        public void SaveHeaderInformation(object sender, EventArgs e)
        {
            if (ViewModel != null)
            {
                ViewModel.SaveHeaderInformation();
            }
        }

        public void LoadHeaderInformation(object sender, EventArgs e)
        {
            if (ViewModel != null)
            {
                ViewModel.LoadHeaderInformation();
            }
        }
    }
}
