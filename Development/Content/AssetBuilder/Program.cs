﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using AssetBuilder.Properties;

namespace AssetBuilder
{
    public class AssetEntry
    {
        public string FilePath;
        public int SizeInBytes;
    }

    public class AssetBuilder
    {
        private int _outputVerbosity = 0;
        private string _headerOutputPath = @"..\Engine\Headers\Assets.h";
        private string _cppOutputPath = @"..\Engine\Source\Assets.cpp";
        private const string _logFile = "assets.log";
        private List<Asset> _recordedAssets;
        private string _inputDirectory;
        private string _outputDirectory;
        private string _basePath;
        private ManagedInterop.Cryptography _crypto = new ManagedInterop.Cryptography();
        private IEnumerable<string> _filePaths;
        private Dictionary<string, AssetEntry> _fileMapping = new Dictionary<string, AssetEntry>();
        private bool _mustProduce;
        private bool _mustEncrypt;

        public AssetBuilder(string inputDirectory, string outputDirectory)
        {
            _basePath = Directory.GetCurrentDirectory();
            _inputDirectory = inputDirectory;
            _outputDirectory = outputDirectory;

            Log(1, "Base directory is {0}.", _basePath);
            Log(1, "Input directory is {0}.", _inputDirectory);
            Log(1, "Output directory is {0}.", _outputDirectory);
        }

        private void Log(int verbosity, string format, params object[] options)
        {
            if(_outputVerbosity <= verbosity)
            {
                Console.WriteLine(format, options);
            }
        }
        
        private string ReducePath(string basePath, string path)
        {
            var bpParts = basePath.Split('\\');
            var pParts = path.Split('\\');

            string output = string.Empty;

            for (var index = bpParts.Length; index < pParts.Length; index++)
            {
                output += pParts[index];

                if (index != pParts.Length - 1)
                {
                    output += "\\\\";
                }
            }

            return output;
        }

        private void LoadRecordedAssets()
        {
            var xml = new XmlSerializer(typeof(List<Asset>));

            if (!File.Exists(_logFile))
            {
                Log(0, "No assets found. Creating new assets.");
                _recordedAssets = new List<Asset>();
            }
            else
            {
                using (var reader = new StreamReader(_logFile))
                {
                    _recordedAssets = xml.Deserialize(reader.BaseStream) as List<Asset>;
                }
                Log(0, "Assets log loaded. {0} assets already known.", _recordedAssets.Count);
            }
        }
        
        private void CheckRecordedAssets()
        {
            _mustProduce = _recordedAssets.Count != _filePaths.Count();

            foreach (var filePath in _filePaths)
            {
                var record = _recordedAssets.Where(a => a.FileName == filePath).FirstOrDefault();
                var lastModified = File.GetLastWriteTime(filePath);

                if (record != null)
                {
                    if (record.LastModified == lastModified)
                    {
                        continue;
                    }
                    else
                    {
                        record.LastModified = lastModified;
                        _mustEncrypt = true;
                    }
                }
                else
                {
                    _mustProduce = _mustEncrypt = true;
                    _recordedAssets.Add(new Asset()
                    {
                        FileName = filePath,
                        LastModified = lastModified
                    });
                }
            }
        }

        private void UpdateLogFile()
        {
            Log(0, "Updating log file.");
            var xml = new XmlSerializer(typeof(List<Asset>));
            using (var writer = new StreamWriter(_logFile))
            {
                xml.Serialize(writer.BaseStream, _recordedAssets);
            }
        }

        private void EncryptFiles()
        {
            foreach (var filePath in _filePaths)
            {
                var name = filePath.Split('\\').Last();
                var nameParts = name.Split('.');
                var subDirParts = filePath.Replace(_basePath, string.Empty).Replace(name, string.Empty).Split(new[] { '\\' }, StringSplitOptions.RemoveEmptyEntries);


                var targetPath = _outputDirectory;
                string subDirectory = string.Empty;
                if (subDirParts.Length > 1)
                {
                    for (int index = 1; index < subDirParts.Length; index++)
                    {
                        subDirectory += subDirParts[index] + "\\";
                    }
                    Log(2, "Subdirectory is {0}.", subDirectory);
                    targetPath += subDirectory;
                }

                if (!Directory.Exists(targetPath))
                {
                    Log(2, "Creating directory {0}.", targetPath);
                    Directory.CreateDirectory(targetPath);
                }

                foreach (var part in nameParts)
                {
                    if (part != nameParts.Last())
                    {
                        targetPath += part + ".";
                    }
                    else
                    {
                        targetPath += "enc." + part;
                    }
                }

                Log(1, "Encrypting {0} at {1}.", name, targetPath);
                var asset = new AssetEntry()
                {
                    FilePath = targetPath.Replace(_basePath, string.Empty)
                };
                asset.SizeInBytes = _crypto.Encrypt(filePath, targetPath);

                _fileMapping.Add(name, asset);
            }
        }

        private void OutputFiles()
        {
            var headerSb = new StringBuilder();
            var cppSb = new StringBuilder();

            foreach (var file in _fileMapping)
            {
                var name = file.Key.Replace(".", "");

                Log(1, "Writing entry for {0}.", name);
                headerSb.Append("\n\t\t");
                headerSb.Append(string.Format(Resources.AssetHeaderGetFormat, name));
                if (file.Key != _fileMapping.Last().Key)
                {
                    headerSb.Append("\n");
                }

                var reducedPath = ReducePath(_basePath, file.Value.FilePath);
                Log(1, "File path: {0}", file.Value);
                Log(2, "Reduced path: {0}", reducedPath);
                cppSb.Append("\n\n");
                cppSb.Append(string.Format(Resources.AssetSourceGetFormat, name, reducedPath, file.Value.SizeInBytes));
            }

            string headerOutput = string.Format(Resources.AssetHeaderFormat, headerSb.ToString());
            string cppOutput = string.Format(Resources.AssetSourceFormat, cppSb.ToString());
            
            Log(0, "Updating Assets header.");
            using (var writer = new StreamWriter(_headerOutputPath))
            {
                writer.Write(headerOutput);
            }

            Log(0, "Updating Assets source.");
            using (var writer = new StreamWriter(_cppOutputPath))
            {
                writer.Write(cppOutput);
            }
        }

        public void CreateAssets()
        {
            try
            {
                LoadRecordedAssets();

                _filePaths = Directory.EnumerateFiles(_inputDirectory, "*.*", SearchOption.AllDirectories);

                Log(0, "{0} assets found.", _filePaths.Count());

                if (!Directory.Exists(_outputDirectory))
                {
                    Log(0, "Output directory does not exist. Creating output directory.");
                    Directory.CreateDirectory(_outputDirectory);
                }

                CheckRecordedAssets();

                UpdateLogFile();

                if (_mustEncrypt)
                {
                    Log(0, "Assets are out of date. Producing new assets.");
                }
                else
                {
                    Log(0, "All assets are up to date.");
                    return;
                }

                EncryptFiles();

                if (_mustProduce)
                {
                    Log(0, "Asset count has changed. Producing new header.");
                }
                else
                {
                    Log(0, "No assets have been added or removed.");
                    return;
                }

                OutputFiles();
            }
            catch(Exception err)
            {
                Log(0, "Unabled to successfully complete encryption process. Details:\n{0}", err);
            }
        }
    }

	class Program
	{
        static string ReducePath(string basePath, string path)
        {
            var bpParts = basePath.Split('\\');
            var pParts = path.Split('\\');

            string output = string.Empty;

            for (var index = bpParts.Length; index < pParts.Length; index++)
            {
                output += pParts[index];

                if(index != pParts.Length - 1)
                {
                    output += "\\\\";
                }
            }

            return output;
        }

        static void Test()
        {
            ManagedInterop.Cryptography c = new ManagedInterop.Cryptography();

            c.Encrypt(@"E:\Win7\Documents\Source Control\Lumberjack 2\Development\HyJynx3\Assets\Assets\Raw\Config.json", @"E:\Win7\Documents\Source Control\Lumberjack 2\Development\HyJynx3\Assets\Assets\Raw\ConfigENCRYPTED.json");
        }

		static void Main(string[] args)
		{
            AssetBuilder ab = new AssetBuilder(args[0], args[1]);
            ab.CreateAssets();
            /*try
            {
                //Test();
                string logFile = "assets.log";
                var xml = new XmlSerializer(typeof(List<Asset>));

                List<Asset> recordedAssets = null;

                if (!File.Exists(logFile))
                {
                    recordedAssets = new List<Asset>();
                }
                else
                {
                    using (var reader = new StreamReader(logFile))
                    {
                        recordedAssets = xml.Deserialize(reader.BaseStream) as List<Asset>;
                    }
                }

                Console.WriteLine("Input location is {0}\nOutput location is {1}", args[0], args[1]);
                ManagedInterop.Cryptography crypto = new ManagedInterop.Cryptography();

                var basePath = Directory.GetCurrentDirectory();
                var rootDirectory = args[0];
                var outputDirectory = args[1];

                var filePaths = Directory.EnumerateFiles(rootDirectory, "*.*", SearchOption.AllDirectories);

                Console.WriteLine("{0} files found...\n{1} files known...", filePaths.Count(), recordedAssets.Count);


                if (!Directory.Exists(outputDirectory))
                {
                    Console.WriteLine("Creating output directory...");
                    Directory.CreateDirectory(outputDirectory);
                }
								
                var fileMapping = new Dictionary<string, string>();

                var mustProduce = recordedAssets.Count != filePaths.Count();
                bool mustEncrypt = false;

                if (filePaths.Count() > 0)
                {
                    foreach (var filePath in filePaths)
                    {
                        var record = recordedAssets.Where(a => a.FileName == filePath).FirstOrDefault();
                        var lastModified = File.GetLastWriteTime(filePath);

                        if (record != null)
                        {
                            if (record.LastModified == lastModified)
                            {
                                continue;
                            }
                            else
                            {
                                record.LastModified = lastModified;
                                mustEncrypt = true;
                            }
                        }
                        else
                        {
                            mustProduce = mustEncrypt = true;
                            recordedAssets.Add(new Asset()
                            {
                                FileName = filePath,
                                LastModified = lastModified
                            });
                        }
                    }

                    Console.WriteLine("Updating log file....");
                    using (var writer = new StreamWriter(logFile))
                    {
                        xml.Serialize(writer.BaseStream, recordedAssets);
                    }

                    if (!mustEncrypt)
                    {
                        Console.WriteLine("All assets are up to date....");
                        return;
                    }
                    else
                    {
                        Console.WriteLine("Assets are out of date. Producing new assets...");
                    }

                    foreach (var filePath in filePaths)
                    {
                        var name = filePath.Split('\\').Last();
                        var nameParts = name.Split('.');
                        var subDirParts = filePath.Replace(basePath, string.Empty).Replace(name, string.Empty).Split(new [] { '\\' }, StringSplitOptions.RemoveEmptyEntries);


                        var targetPath = outputDirectory;
                        string subDirectory = string.Empty;
                        if(subDirParts.Length > 1)
                        {
                            for(int index = 1; index < subDirParts.Length; index++)
                            {
                                subDirectory += subDirParts[index] + "\\";
                            }
                            Console.WriteLine("Subdirectory is {0}.", subDirectory);
                            targetPath += subDirectory;
                        }

                        if(!Directory.Exists(targetPath))
                        {
                            Console.WriteLine("Creating directory {0}.", targetPath);
                            Directory.CreateDirectory(targetPath);
                        }

                        foreach (var part in nameParts)
                        {
                            if (part != nameParts.Last())
                            {
                                targetPath += part + ".";
                            }
                            else
                            {
                                targetPath += "enc." + part;
                            }
                        }
                        
                        Console.WriteLine(string.Format("Encrypting {0} at {1}.", name, targetPath));
                        crypto.Encrypt(filePath, targetPath);

                        fileMapping.Add(name, targetPath.Replace(basePath, string.Empty));
                    }

                    if (!mustProduce)
                    {
                        Console.WriteLine("No assets have been added or removed...");
                        return;
                    }
                    else
                    {
                        Console.WriteLine("Asset count has changed. Producing new header...");
                    }

                    string headerOutputFormat =
    @"#ifndef HYJYNX_CORE_ASSETS_H
#define HYJYNX_CORE_ASSETS_H

namespace HyJynxCore
{{
    class Assets
    {{
    public:
        static char* LoadAsset( char const* filePath, unsigned int* sizeInBytes );
{0}
    }};
}}

#endif
";

                    string cppOutputFormat =
                        "#include \"..\\Headers\\Core.h\"" +
                        "\nusing namespace HyJynxCore;" +
                        "{0}";

                    string headerGetter = "\r\n\t\tstatic AssetBase& Get{0}( );";
                    string cppGetter =
                        @"

const char* Assets::Get{0}( )
{{
    const char* output = nullptr;

    File target( " + "\"{1}\" );" +
@"

    target.Open( File::OpenMode::Read, true );

    char* encryptedData = target.ReadToEnd( );
    LongLong encryptedLength = target.GetFileLength( );
    output = Decrypt( encryptedData, (unsigned int)encryptedLength );

    return output;
}}";


                    StringBuilder headerSb = new StringBuilder();
                    StringBuilder cppSb = new StringBuilder();

                    foreach (var file in fileMapping)
                    {
                        var name = file.Key.Replace(".", "");
                        Console.WriteLine("Writing entry for {0}.", name);
                        headerSb.Append(string.Format(headerGetter, name));
                        Console.WriteLine("File path: " + file.Value);
                        Console.WriteLine("Reduced path: " + ReducePath(basePath, file.Value));
                        cppSb.Append(string.Format(cppGetter, name, ReducePath(basePath, file.Value)));
                    }

                    string headerOutput = string.Format(headerOutputFormat, headerSb.ToString());
                    string cppOutput = string.Format(cppOutputFormat, cppSb.ToString());


                    string headerOutputPath = @"..\Engine\Headers\Assets.h";
                    string cppOutputPath = @"..\Engine\Source\Assets.cpp";

                    Console.WriteLine("Updating Assets.h.");
                    using (var writer = new StreamWriter(headerOutputPath))
                    {
                        writer.Write(headerOutput);
                    }

                    Console.WriteLine("Updating Assets.cpp.");
                    using (var writer = new StreamWriter(cppOutputPath))
                    {
                        writer.Write(cppOutput);
                    }
                }
            }
            catch (Exception err)
            {
                Console.WriteLine("Unable to successfully complete the encryption process.\n{0}", err);
            }*/
		}
	}
}