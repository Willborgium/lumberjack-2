﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetBuilder
{
    public class Asset
    {
        public string FileName { get; set; }
        public DateTime LastModified { get; set; }
    }
}
