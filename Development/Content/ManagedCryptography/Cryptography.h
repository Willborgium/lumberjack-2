#ifndef MANAGED_CRYPTOGRAPHY_H
#define MANAGED_CRYPTOGRAPHY_H

#include <msclr\marshal.h>

using namespace System;

namespace ManagedInterop
{
	public ref class Cryptography
	{
	public:
		int Encrypt( String^, String^ );
	};
}

#endif