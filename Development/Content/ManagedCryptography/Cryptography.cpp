#include "Cryptography.h"
using namespace ManagedInterop;
using namespace System::Runtime::InteropServices;

#include "..\..\HyJynx3\Cryptography\Cryptography.h"

#include <fstream>
#include <iostream>

int Cryptography::Encrypt( String^ inputFilePath, String^ outputFilePath )
{
	char* nativeInputFilePath = ( char* ) ( void* ) Marshal::StringToHGlobalAnsi( inputFilePath );
	char* nativeOutputFilePath = ( char* ) ( void* ) Marshal::StringToHGlobalAnsi( outputFilePath );
	int inputLength = 0;
	char* inputData = nullptr;
	char* outputData = nullptr;

	std::ifstream inputFile;

	inputFile.open( nativeInputFilePath, std::ios::in | std::ios::binary );
	
	auto inBuffer = inputFile.rdbuf( );
	inBuffer->pubseekpos( 0, inputFile.in );
	inputLength = inBuffer->pubseekoff( 0, inputFile.end, inputFile.in );
	inBuffer->pubseekpos( 0, inputFile.in );

	inputData = new char [ inputLength ];
	memset( inputData, 0, inputLength );
	inputFile.read( inputData, inputLength );

	inputFile.close( );

	char* result = HyJynxCore::Encrypt( inputData, inputLength );

	std::ofstream file;

	file.open( nativeOutputFilePath, std::ios::out | std::ios::binary );

	std::cout << "Output length: " << inputLength + sizeof( int ) << std::endl;
	file.write( result, inputLength + sizeof( int ) );

	file.close( );

	delete[ ] inputData;
	delete[ ] result;

	Marshal::FreeHGlobal( ( IntPtr ) nativeOutputFilePath );
	Marshal::FreeHGlobal( ( IntPtr ) nativeInputFilePath );

	return inputLength;
}