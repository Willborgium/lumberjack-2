#include "Cryptography.h"

#include <cstring>
#include <random>

void ManagedEncrypt( char* data, unsigned int length, char* output )
{
	char* temp = Encrypt( data, length );

	memcpy( output, temp, length + sizeof( unsigned int ) );
}


void ManagedDecrypt( char* data, unsigned int length, char* output )
{
	char* temp = Decrypt( data, length );

	memcpy( output, temp, length - sizeof( unsigned int ) );
}

char* Encrypt( char* data, unsigned int length )
{
	char* output = nullptr;
	unsigned int uiSize = sizeof(unsigned int);

	if (data != nullptr && length > 0)
	{
		unsigned int key = rand( );


		// Create the output buffer to be the size of the given data
		// plus the size of the key (in bytes)
		unsigned int outputSize = length + uiSize;
		output = new char [ outputSize ];
		memset( output, 0, outputSize );

		char* keyParts = (char*)&key;

		// Key byte locations will be the first, last, and middle of the data.
		int keyLocations[4] = { 0, outputSize / 2 - 1, outputSize / 2, outputSize - 1 };

		unsigned int currentKeyOffset = 0;
		for (unsigned int index = 0; index < length + uiSize; index++)
		{
			bool isKeyLocation = false;

			for (unsigned int keyIndex = 0; keyIndex < 4; keyIndex++)
			{
				if (index == keyLocations[keyIndex])
				{
					output[index] = (keyParts[keyIndex] + index) % 256;
					currentKeyOffset++;
					isKeyLocation = true;
					break;
				}
			}

			if (!isKeyLocation)
			{
				unsigned int dataIndex = index - currentKeyOffset;
				output[index] = data[dataIndex] + ((dataIndex + key) % 256);
			}
		}
	}

	return output;
}

char* Decrypt( char* data, unsigned int length )
{
	char* output = nullptr;
	unsigned int uiSize = sizeof(unsigned int);

	if (data != nullptr && length > uiSize)
	{
		output = new char[length - uiSize];
		memset(output, 0, length - uiSize);

		unsigned int key = 0;
		char* keyMask = (char*)&key;
		int keyLocations[4] = { 0, length / 2 - 1, length / 2, length - 1 };

		for (unsigned int keyIndex = 0; keyIndex < uiSize; keyIndex++)
		{
			keyMask[keyIndex] = data[keyLocations[keyIndex]] - keyLocations[keyIndex];
		}

		unsigned int currentKeyOffset = 0;
		for (unsigned int index = 0; index < length; index++)
		{
			bool isKeyLocation = false;

			for (unsigned int keyIndex = 0; keyIndex < 4; keyIndex++)
			{
				if (index == keyLocations[keyIndex])
				{
					isKeyLocation = true;
					currentKeyOffset++;
					break;
				}
			}

			if (!isKeyLocation)
			{
				unsigned int dataIndex = index - currentKeyOffset;
				output[dataIndex] = data[index] - ((dataIndex + key) % 256);
			}
		}
	}

	return output;
}