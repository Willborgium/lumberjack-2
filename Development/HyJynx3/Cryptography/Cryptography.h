#ifndef HYJYNX_CORE_CRYPTOGRAPHY_H
#define HYJYNX_CORE_CRYPTOGRAPHY_H

namespace HyJynxCore
{
	//
	// Encrypts the given data using a jam and roll algorithm.
	// - data:	 The data to be encrypted.
	// - length:	The length, in bytes, of the data to be encrypted.
	// - returns:	The encrypted data. The length, in bytes, of the result will be
	//				the length of the input data plus the size of an unsigned int.
	//
	char* Encrypt( char* data, unsigned int length );

	//
	// Decrypts the given jam and roll encrypted data.
	// - data:	 The encrypted data to be decrypted.
	// - length:	The length, in bytes, of the encrypted data.
	// - returns:	The decrypted data. The length, in bytes, of the result will be
	//				the length of the input data minus the size of an unsigned int.
	//
	char* Decrypt( char* data, unsigned int length );
}

#endif