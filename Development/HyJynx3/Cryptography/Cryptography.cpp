#include "Cryptography.h"

#include <cstring>
#include <random>
//#include <iostream>

char* HyJynxCore::Encrypt( char* data, unsigned int length )
{
	char* output = nullptr;

	if ( data != nullptr && length > 0 )
	{
		int key = rand( );
		//std::cout << "Key: " << key << std::endl;
		char* keyMask = new( &key ) char [ sizeof( key ) ];
		unsigned int newLength = length + sizeof( key );
		int keyLocations [ 4 ] = { 0, newLength / 2 - 1, newLength / 2, newLength - 1 };
		
		output = new char [ newLength ];
		memset( output, 0, newLength );

		int offset = 0;
		bool isKeyLocation = false;
		for ( int index = 0; index < newLength; index++ )
		{
			for ( int keyIndex = 0; keyIndex < sizeof( int ); keyIndex++ )
			{
				if ( index == keyLocations [ keyIndex ] )
				{
					//std::cout << "Key part " << keyIndex << " at index " << index << " is " <<  (int)keyMask [ keyIndex ] << std::endl;
					output [ index ] = keyMask [ keyIndex ];
					offset++;
					isKeyLocation = true;
					break;
				}
			}

			if ( !isKeyLocation )
			{
				output [ index ] = data [ index - offset ] + key + index;
			}

			isKeyLocation = false;
		}
	}

	return output;
}

char* HyJynxCore::Decrypt( char* data, unsigned int length )
{
	char* output = nullptr;

	if ( data != nullptr && length > sizeof( int ) )
	{
		int key = 0;
		char* keyMask = new ( &key ) char [ sizeof( int ) ];
		unsigned int outputLength = length - sizeof( int );
		int keyLocations [ 4 ] = { 0, length / 2 - 1, length / 2, length - 1 };

		for ( int keyIndex = 0; keyIndex < sizeof( int ); keyIndex++ )
		{
			keyMask [ keyIndex ] = data[ keyLocations [ keyIndex ] ];
		}

		output = new char [ outputLength ];
		memset( output, 0, outputLength );

		int offset = 0;
		bool isKeyLocation = false;
		for ( int index = 0; index < length; index++ )
		{
			for ( int keyIndex = 0; keyIndex < sizeof( int ); keyIndex++ )
			{
				if ( index == keyLocations [ keyIndex ] )
				{
					isKeyLocation = true;
					offset++;
					break;
				}
			}

			if ( !isKeyLocation )
			{
				output [ index - offset ] = data [ index ] - key - index;
			}

			isKeyLocation = false;
		}
	}

	return output;
}