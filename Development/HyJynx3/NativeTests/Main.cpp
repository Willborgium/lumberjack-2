#include "..\Engine\Headers\DynamicCollection.h"
using namespace HyJynxCollections;

#include <iostream>
using namespace std;

template <typename NumericType>
int Ascending ( NumericType lhs, NumericType rhs )
{
	if ( lhs < rhs )
	{
		return -1;
	}
	else if ( lhs > rhs )
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

template <typename NumericType>
int Descending ( NumericType lhs, NumericType rhs )
{
	if ( lhs > rhs )
	{
		return -1;
	}
	else if ( lhs < rhs )
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

void main ( )
{
	DynamicCollection<int> a;

	a.Append ( 11 );
	a.Append ( 8 );
	a.Append ( 14 );
	a.Append ( 15 );
	a.Append ( 4 );
	a.Append ( 1 );
	a.Append ( 9 );
	a.Append ( 12 );

	cout << "Unsorted\n";
	a.ForEach ( [] ( int x )
	{
		cout << x << endl;
	} );

	if ( 1 );

	auto result = a.Sort ( Descending<int> );

	if ( result != nullptr )
	{
		cout << "Sorted\n";
		result->ForEach ( [] ( int x )
		{
			cout << x << endl;
		} );
	}

	system ( "pause" );
}