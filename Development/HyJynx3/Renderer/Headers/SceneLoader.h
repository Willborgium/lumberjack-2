/**
 * SceneLoader.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_SCENELOADER_H
#define HYJYNXRENDERER_SCENELOADER_H

#include <Engine/Headers/Text.h>
#include <Engine/Headers/Function.h>
#include <Renderer/Headers/ISceneIO.h>
#include <sal.h>

namespace HyJynxRenderer
{

	//
	// Implements and manages all file IO for 3D scenes.
	//
	class SceneLoader sealed
	{
	protected:

		ISceneIO* _operations;

	public:

		//
		// null ctor
		//
		SceneLoader( );

		//
		// default ctor
		// - ISceneIO*: file operations behavior
		//
		SceneLoader( _In_ ISceneIO* const );

		//
		// copy ctor
		//
		SceneLoader( _In_ const SceneLoader& );

		//
		// move ctor
		//
		SceneLoader( _In_ const SceneLoader&& );

		//
		// dtor
		//
		~SceneLoader( );

		// ================================================

		//
		// open and load a scene from file
		// - Text: filepath of the scene
		// - function<void, Window*>: onSuccess callback with the loaded window
		// - function<void, Text>: onFail callback with argued reason
		//
		void LoadFromFile( _In_ HyJynxCore::Text,
			_In_ HyJynxCore::Function< void, Window* >,
			_In_ HyJynxCore::Function< void, HyJynxCore::Text > );

		//
		// load a scene from data
		// - char*: data loaded from file, or from decrypted data from file
		// - UInt: size of the data in bytes
		// - function<void, Window*>: onSuccess callback with the loaded window
		// - function<void, Text>: onFail callback with argued reason
		//
		void LoadFromData( _In_ char* const, 
			_In_ const UInt,
			_In_ HyJynxCore::Function< void, Window* >,
			_In_ HyJynxCore::Function< void, HyJynxCore::Text > );

		//
		// Save the current scene to file
		// - Text: filepath to save to (will overrite)
		// - Window*: window containg the scene you wish to serialize
		// - function<void>: onSuccess callback
		// - function<void, Text>: onFail callback with argued reason
		//
		void SaveToFile( _In_ HyJynxCore::Text,
			_In_ Window* const,
			_In_ HyJynxCore::Function< void >,
			_In_ HyJynxCore::Function< void, HyJynxCore::Text > );

	};
};

#endif // HYJYNXRENDERER_SCENELOADER_H 