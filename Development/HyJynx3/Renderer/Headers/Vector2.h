/**
* Vector2.h
* (c) 2014 All Rights Reserved
*/

#ifndef HYJYNXRENDERER_VECTOR2_H
#define HYJYNXRENDERER_VECTOR2_H

#include <Renderer\Headers\Vector3.h>
#include <Engine\Headers\TypeDefinitions.h>
#include <DirectXMath.h>
#include <sal.h>

namespace HyJynxRenderer
{
	//
	// This extends the DirectX XMFLOAT2 math primitive,
	// so we can add functionality and overloads as we please
	//
	class Vector2 : public DirectX::XMFLOAT2
	{

	public:

#pragma region init

		//
		// null ctor - values default to 0
		//
		Vector2( );

		//
		// single defined ctor - both x/y components set to argued value
		// - float: value for both X and Y
		//
		Vector2( _In_ const float );

		//
		// fully defined ctor
		// - float: X component value
		// - float: Y component value
		//
		Vector2( _In_ const float, _In_ const float );

		//
		// vector3 trim to vector2 ctor
		// - Vector3: retrieves x and y from the argued vector
		//
		Vector2( _In_ const Vector3& );

		//
		// copy ctor
		//
		Vector2( _In_ const Vector2& );

		//
		// move ctor
		//
		Vector2( _In_ const Vector2&& );

		//
		// get a vector2 with all components set to 1
		// - returns Vector2: (1, 1)
		//
		static Vector2 One( );

		//
		// get a vector2 with all components set to 0
		// - returns Vector2: (0, 0)
		//
		static Vector2 Zero( );

#pragma endregion

#pragma region operators

		// Arithmetic 
		Vector2 operator = ( _In_ const Vector2& );
		Vector2 operator * ( _In_ const Vector2& ) const;
		Vector2 operator / ( _In_ const Vector2& ) const;
		Vector2 operator / ( _In_ const float ) const;
		Vector2 operator - ( _In_ const Vector2& ) const;
		Vector2 operator + ( _In_ const Vector2& ) const;
		Vector2& operator ++ ( );
		Vector2& operator -- ( );

		// Comparison
		bool operator == ( _In_ const Vector2& ) const;
		bool operator != ( _In_ const Vector2& ) const;
		bool operator > ( _In_ const Vector2& ) const;
		bool operator >= ( _In_ const Vector2& ) const;
		bool operator < ( _In_ const Vector2& ) const;
		bool operator <= ( _In_ const Vector2& ) const;

		// Compound
		Vector2& operator += ( _In_ const Vector2& );
		Vector2& operator -= ( _In_ const Vector2& );
		Vector2& operator *= ( _In_ const Vector2& );
		Vector2& operator /= ( _In_ const Vector2& );

		// Member
		float operator [] ( _In_ const UInt ) const;

#pragma endregion
	};
}

#endif // HYJYNXRENDERER_VECTOR2_H