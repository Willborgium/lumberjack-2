/**
 * AnimationController.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_ANIMATIONCONTROLLER_H
#define HYJYNXRENDERER_ANIMATIONCONTROLLER_H

#include <sal.h>

namespace HyJynxRenderer
{
	class IAnimation;

	//
	// After beginning an Animation, Animator will return this object back to you
	// so you may further control the active animation.
	//
	class AnimationController
	{
	protected:

		IAnimation* _animation;

	public:

		//
		// null ctor
		//
		AnimationController( );

		//
		// default ctor
		// - IAnimation: animation interface
		//
		AnimationController( _In_ IAnimation* const );

		//
		// copy ctor
		//
		AnimationController( _In_ const AnimationController& );

		//
		// move ctor
		//
		AnimationController( _In_ const AnimationController&& );

		//
		// dtor
		//
		virtual ~AnimationController( );

		//
		// temporarily pause the animation this controller represents
		//
		void Pause( );

		//
		// resume a previously paused animation, calling this on an active animation has no effect
		//
		void Resume( );

		//
		// Set the animation to completion values (onComplete callback will occur)
		//
		void Finish( );

		//
		// Set the animation to beginning values
		//
		void Reset( );

		//
		// Stop the animation completely, removing it from Animator, and making your
		// reference to this AnimationController useless. Use Pause/Reset/Resume if you
		// wish to avoid this.
		//
		void Stop( );

		//
		// set the speed of the animation in FPS
		// - double: frames per second
		//
		void SetSpeed( _In_ const double );

		//
		// get the FPS of this animation
		// - returns double: frames per second
		//
		double GetSpeed( );

		//
		// determine if this AnimationController is valid or not
		// - returns bool: true - the controller works,  
		//				false - Animator has completed this, or you have called .Stop()
		//
		bool IsValid( );
	};
};

#endif // HYJYNXRENDERER_ANIMATIONCONTROLLER_H 