/**
 * TransformState.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_TRANSFORMSTATE_H
#define HYJYNXRENDERER_TRANSFORMSTATE_H

#include <Renderer\Headers\Vector3.h>
#include <sal.h>

namespace HyJynxRenderer
{

	//
	// Contains a recorded state of the Transform object
	//
	class TransformState
	{
	public:

		Vector3 Position = Vector3( 0.0f ), 
				Scale = Vector3( 1.0f ), 
				Rotation = Vector3( 0.0f );

#pragma region Init

		//
		// null ctor
		//
		TransformState( );

		//
		// copy ctor
		//
		TransformState( _In_ const TransformState& );

		//
		// move ctor
		//
		TransformState( _In_ const TransformState&& );

		//
		// dtor
		//
		~TransformState( );

#pragma endregion

#pragma region API



#pragma endregion

#pragma region Operators

		TransformState& operator = ( _In_ const TransformState& );
		bool operator == ( _In_ const TransformState& ) const;
		bool operator != ( _In_ const TransformState& ) const;

		TransformState operator / ( _In_ const TransformState& ) const;
		TransformState operator / ( _In_ const float ) const;
		TransformState operator / ( _In_ const double ) const;
		TransformState operator / ( _In_ const long ) const;
		TransformState operator / ( _In_ const LongLong ) const;
		TransformState operator * ( _In_ const TransformState& ) const;
		TransformState operator * ( _In_ const float ) const;
		TransformState operator * ( _In_ const double ) const;
		TransformState operator * ( _In_ const long ) const;
		TransformState operator * ( _In_ const LongLong ) const;

		TransformState& operator /= ( _In_ const TransformState& );
		TransformState& operator /= ( _In_ const float );
		TransformState& operator /= ( _In_ const double );
		TransformState& operator /= ( _In_ const long );
		TransformState& operator /= ( _In_ const LongLong );
		TransformState& operator *= ( _In_ const TransformState& );
		TransformState& operator *= ( _In_ const float );
		TransformState& operator *= ( _In_ const double );
		TransformState& operator *= ( _In_ const long );
		TransformState& operator *= ( _In_ const LongLong );

#pragma endregion
	};
};

#endif // HYJYNXRENDERER_TRANSFORMSTATE_H 