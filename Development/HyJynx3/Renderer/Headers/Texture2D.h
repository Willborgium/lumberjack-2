/**
 * Texture2D.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_TEXTURE2D_H
#define HYJYNXRENDERER_TEXTURE2D_H

#include <Renderer/Headers/Texture.h>
#include <sal.h>

namespace HyJynxRenderer
{
	//
	// Extends Texture with 2D Implementations
	//
	class Texture2D : public Texture
	{
	protected:

		//
		// Internally called to read pixel data from the CPU stored memory
		//
		virtual void ReadFromCpuData( _In_ LoadedTexture*, _Out_ Color**, _In_ Point3[ ], _In_ UInt ) override;

		//
		// Internally called to read pixel data from the GPU stored memory
		//
		virtual void ReadFromGpuData( _In_opt_ Context*, _In_ LoadedTexture*, _Out_ Color**, _In_ Point3[ ], _In_ UInt ) override;

	public:

		//
		// null ctor
		//
		Texture2D( );

		//
		// default ctor used by TextureManager
		//
		Texture2D( _In_ LoadedTexture* );

		//
		// copy ctor
		//
		Texture2D( _In_ const Texture2D& );

		//
		// move ctor
		//
		Texture2D( _In_ const Texture2D&& );

		//
		// dtor
		//
		virtual ~Texture2D( );
	};
}

#endif // HYJYNXRENDERER_TEXTURE2D_H