/**
 * ITween.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_ITWEEN_H
#define HYJYNXRENDERER_ITWEEN_H

#include <sal.h>

namespace HyJynxRenderer
{

	//
	// Interface all Tween operations inherit from
	//
	class ITween abstract
	{
	public:

		//
		// per-cycle update
		// - double: total time since the start of the tween
		// - double: delta time since the last update call
		// - double: maximum duration
		//
		virtual void Update( _In_ const double, _In_ const double, _In_ const double ) = 0;

	};
};

#endif // HYJYNXRENDERER_ITWEEN_H 