/**
 * Vector4.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_VECTOR4_H
#define HYJYNXRENDERER_VECTOR4_H

#include <Renderer\Headers\Vector3.h>
#include <Engine\Headers\TypeDefinitions.h>
#include <DirectXMath.h>
#include <sal.h>

namespace HyJynxRenderer
{
	//
	// this extends the DirectX::XMFLOAT4 math primitive,
	// so we can add functionality as we please
	//
	class Vector4 : public DirectX::XMFLOAT4
	{
		
	public:

#pragma region init

		//
		// null ctor - all values default to 0
		//
		Vector4( );

		//
		// single defined ctor - all values set to argumented
		// - float: value for XYZW components
		//
		Vector4( _In_ const float );

		//
		// V3 defined ctor - XYZ values are set to arguments, W is set to 1
		// - float: X component value
		// - float: Y component value
		// - float: Z component value
		//
		Vector4( _In_ const float, _In_ const float, _In_ const float );

		//
		// fully defined ctor - all components defined
		// - float: X component value
		// - float: Y component value
		// - float: Z component value
		// - float: W component value
		//
		Vector4( _In_ const float, _In_ const float, _In_ const float, _In_ const float );

		//
		// fully defined ctor, V3-xyz, float-w
		// - Vector3: xyz component values
		// - float: w component value
		//
		Vector4( _In_ const Vector3, _In_ const float );

		//
		// copy ctor
		//
		Vector4( _In_ const Vector4& );

		//
		// move ctor
		//
		Vector4( _In_ const Vector4&& );

		//
		// get a vector3 with all components set to 1
		// - returns Vector4: (1, 1, 1, 1)
		//
		static Vector4 One( );

		//
		// get a vector3 with all components set to 0
		// - returns Vector4: (0, 0, 0, 0)
		//
		static Vector4 Zero( );

		//
		// copy and normalize the argued vector
		// - Vector4: vector to normalize
		// - returns Vector4: copy of the argued and normalized vector
		//
		static Vector4 Normalize( _In_ const Vector4& );

		//
		// normalize the vector as it stands
		//
		void Normalize( );

		//
		// copy and normalize the argued vector
		// - XMFLOAT4: vector to normalize
		// - returns XMFLOAT4: copy of the argued and normalized vector
		//
		static DirectX::XMFLOAT4 Normalize( _In_ const DirectX::XMFLOAT4& );

#pragma endregion

#pragma region operators

		// Arithmetic 
		Vector4 operator = ( _In_ const Vector4& );
		Vector4 operator * ( _In_ const Vector4& ) const;
		Vector4 operator / ( _In_ const Vector4& ) const;
		Vector4& operator ++ ( );
		Vector4& operator -- ( );

		// Comparison
		bool operator == ( _In_ const Vector4& ) const;
		bool operator != ( _In_ const Vector4& ) const;
		bool operator > ( _In_ const Vector4& ) const;
		bool operator >= ( _In_ const Vector4& ) const;
		bool operator < ( _In_ const Vector4& ) const;
		bool operator <= ( _In_ const Vector4& ) const;

		// Compound
		Vector4& operator += ( _In_ const Vector4& );
		Vector4& operator -= ( _In_ const Vector4& );
		Vector4& operator *= ( _In_ const Vector4& );
		Vector4& operator /= ( _In_ const Vector4& );

		// Member
		float operator [] ( _In_ const UInt ) const;

#pragma endregion
	};
}

#endif // HYJYNXRENDERER_VECTOR4_H