/**
 * FBXSceneIO.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_FBXSCENEIO_H
#define HYJYNXRENDERER_FBXSCENEIO_H

#include <Renderer/Headers/ISceneIO.h>
#include <sal.h>

namespace HyJynxRenderer
{

	//
	// Implements ISceneIO with FBX file IO logic
	//
	class FBXSceneIO sealed : public ISceneIO
	{
	protected:

	public:

		//
		// null ctor
		//
		FBXSceneIO( );

		//
		// copy ctor
		//
		FBXSceneIO( _In_ const FBXSceneIO& );

		//
		// move ctor
		//
		FBXSceneIO( _In_ const FBXSceneIO&& );

		//
		// dtor
		//
		~FBXSceneIO( );

		// ===================================================

		//
		// Initialize the scene from a file
		// - Text: filepath to the scene file
		// - function<void, Window>: onSuccess callback with the loaded Window argued
		// - function<void, Text>: onFail callback with argued text reason
		//
		virtual void InitializeFromFile( _In_ HyJynxCore::Text,
			_In_ HyJynxCore::Function< void, Window* >,
			_In_ HyJynxCore::Function< void, HyJynxCore::Text > ) override;

		//
		// Initialize the scene from loaded file data
		// - char*: data loaded from file, or decrypted data
		// - UInt: size of the data
		// - function<void, Window*>: onSuccess callback with the loaded Window argued
		// - function<void, Text>: onFail callback with a text reason
		//
		virtual void InitializeFromData( _In_ char* const,
			_In_ const UInt,
			_In_ HyJynxCore::Function< void, Window* >,
			_In_ HyJynxCore::Function< void, HyJynxCore::Text > ) override;

		//
		// Saves the scene to file
		// - Text: filepath where the scene will save to (will overwrite)
		// - Window*: pointer to a window containing the scene you wish to serialize
		// - function<void>: onSuccess callback
		// - function<void, Text>: onFail callback with argued text reason
		//
		virtual void SaveToFile( _In_ HyJynxCore::Text,
			_In_ Window* const,
			_In_ HyJynxCore::Function< void >,
			_In_ HyJynxCore::Function< void, HyJynxCore::Text > ) override;

	};
};

#endif // HYJYNXRENDERER_FBXSCENEIO_H 