/**
 * Drawable.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_DRAWABLE_H
#define HYJYNXRENDERER_DRAWABLE_H

#include <Renderer\Headers\IDrawable.h>
#include <Renderer\Headers\CollisionDescription.h>
#include <Renderer\Headers\Model.h>
#include <Engine\Headers\Function.h>
#include <sal.h>

namespace HyJynxRenderer
{

	//
	// Implements IDrawable, and provides a base concrete class with members for IDrawable
	//
	class Drawable abstract : public IDrawable
	{
	protected:

		CollisionDescription			_collision = CollisionDescription( );
		Model*							_model = nullptr;
		int								_drawableId;
		bool							_active = true;
		DrawableRenderMode				_rasterizerMode = DrawableRenderMode::DRAWABLE_RENDER_SOLID;

		//
		// null ctor
		//
		Drawable( );

		//
		// copy ctor
		//
		Drawable( _In_ const Drawable& );

		//
		// move ctor
		//
		Drawable( _In_ const Drawable&& );

		//
		// dtor
		//
		virtual ~Drawable( );

#pragma region IDrawable

	public:

		//
		// drawables are constructed with the data they need to load,
		// this kicks off the sequence to load and and register the mesh buffers
		// - function< void(void) >: lambda to call when loading has completed
		//					(this is here for async loading down the road)
		//
		virtual void LoadModel( _In_ HyJynxCore::Function< void > ) override;

		//
		// this should be called after a drawable is constructed, and LoadModel() called
		//
		virtual void GenerateBounding( ) override;

		//
		// per-cycle update call
		// Context*: dx API container
		// const RenderTime*: reference to Renderer's per cycle run timer
		//
		virtual void Update( _In_ Context*, _In_ const RenderTime ) override;

		//
		// when we're in development, this API will help us draw debugging information to the screen
		// -Context*: reference to the context we're drawing to
		//
		virtual void RenderDebug( _In_ Context* ) override;

		//
		// Get the model containing all of the meshes in this drawable
		// Derived children wil be able to override this if they so choose
		//
		virtual Model* GetModel( ) override;

		//
		// get the container holding all collision descriptions
		// - returns CollisionDescription: container with collidable sphere, abb, obb1, obb2
		//
		virtual CollisionDescription* GetCollisionDescription( ) override;

		//
		// determine if this drawable is active or not
		// return bool: true - will draw, false - renderer will skip
		//
		virtual bool GetActive( ) const override;

		//
		// set whether this drawable will be active in Renderer's magical eyes
		// -bool: true - will draw, false - renderer will skip
		//
		virtual void SetActive( _In_ bool ) override;

		//
		// Get a transform reference used for 3D space manipulation
		// -return: Transform*
		//
		virtual Transform* GetTransform( ) override;

		//
		// get the ID of this drawable (defaults to infinity when no ID is set)
		// - returns int: drawable identifier
		//
		virtual int GetDrawableID( ) const override;

		//
		// set an identifier to this drawable
		// - int: drawable identifier
		//
		virtual void SetDrawableID( _In_ const int ) override;

		//
		// sets how the contained meshes will rasterize
		// - DrawableRenderMode: rasterizer modes
		//
		virtual void SetRenderMode( _In_ const DrawableRenderMode ) override;

		//
		// gets how the contained meshes will rasterize
		// returns DrawableRenderMode: rasterizer modes
		//
		virtual DrawableRenderMode GetRenderMode( ) const override;

#pragma endregion

	};
};

#endif // HYJYNXRENDERER_DRAWABLE_H 