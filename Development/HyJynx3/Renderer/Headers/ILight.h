/**
 * ILight.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_ILIGHT_H
#define HYJYNXRENDERER_ILIGHT_H

#include <Renderer/Headers/IDrawable.h>
#include <sal.h>

namespace HyJynxRenderer
{

	class Color;
	class Vector3;
	class Transform;

	//
	// Interface all lights will proxy through for Renderer
	//
	class ILight abstract : public IDrawable
	{
	protected:

		ILight( ) : IDrawable( ) { }
		ILight( _In_ const ILight& other ) : IDrawable( other ) { }
		ILight( _In_ const ILight&& other ) : IDrawable( other ) { }

	public:

		virtual ~ILight( ) { }

		//
		// Get the main color of the light source
		// - return Color: Color object reference
		//
		virtual const Color& GetDiffuseColor( ) const = 0;

		//
		// Set the main color of the light source
		// Color: diffuse color of the light source
		//
		virtual void SetDiffuseColor( _In_ const Color& ) = 0;

		//
		// Get the Dimensions of the light source, used for Solf-Light calculations
		// - return Vector3: Dimensions of the Light Source
		//
		virtual const Vector3& GetDimensions( ) const = 0;

		//
		// Set the dimensions of the light source, used for Solf-Light calculations
		// - Vector3: dimensions of the light source
		//
		virtual void SetDimensions( _In_ const Vector3& ) = 0;

		//
		// Get the general direction of the light
		// - return Vector3: Direction of the Light Source
		//
		virtual const Vector3& GetNormal( ) const = 0;

		//
		// Set the general direction of the light
		// - Vector3: direction of the light source
		//
		virtual void SetNormal( _In_ const Vector3& ) = 0;

		//
		// Get the general intensity of the light
		// - return float: Intensity, default is 1
		//
		virtual const float GetIntensity( ) const = 0;

		//
		// Get the general intensity of the light
		// - float: intensity where 1 is default
		//
		virtual void SetIntensity( _In_ const float ) = 0;

		//
		// Get the total distance this light can cover
		// - return float: distance this light reaches
		//
		virtual const float GetDistance( ) const = 0;

		//
		// Set the total distance this light can cover
		// - float: distance in world units this light reaches
		//
		virtual void SetDistance( _In_ const float ) = 0;

		//
		// Get where the light begins to falloff within it's distance (0-1 scale)
		// - return float: 0-1 scale where this light begins to falloff within its distance
		//
		virtual const float GetFalloff( ) const = 0;

		//
		// Set where the light begins to falloff within it's distance (0-1 scale)
		// - float: 0-1 scale where this light begins to falloff within its distance
		//
		virtual void SetFalloff( _In_ const float ) = 0;

		//
		// Determine if this light casts shadows on any receiving drawables
		// - return bool: true - this light will cast shadows, false - will not
		//
		virtual bool CastShadows( ) = 0;

		//
		// Set this light to cast shadows on any receiving drawables
		// - bool: true - this light will cast shadows, false - will not
		//
		virtual void SetCastShadows( _In_ const bool ) = 0;

		//
		// Get the field of view of each axis 
		// - return Vector3: local field of view's of each axis
		//
		virtual const Vector3& GetFieldOfView( ) const = 0;

		//
		// Set the field of view of each local axis
		// - Vector3: field of view of each local axis
		//
		virtual void SetFieldOfView( _In_ const Vector3 ) = 0;
	};
};

#endif // HYJYNXRENDERER_ILIGHT_H 