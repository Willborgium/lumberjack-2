/**
 * RendererConfiguration.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_RENDERERCONFIGURATION_H
#define HYJYNXRENDERER_RENDERERCONFIGURATION_H

#include <Renderer\Headers\CollisionDescription.h>
#include <Engine\Headers\Dirty.h>

namespace HyJynxRenderer
{

	//
	// This configuration deals with only Renderer specific logic, not to be confused
	// with GraphicsConfiguration, which deals with DirectX configuration
	//
	class RendererConfiguration sealed : public HyJynxUtilities::Dirty
	{
	private:

		CollisionMethod _frustumCullMethod = CollisionMethod::COLLISION_AABB;

	public:

		//
		// null ctor
		//
		RendererConfiguration( );

		//
		// dtor
		//
		~RendererConfiguration( );

		//
		// copy ctor
		//
		RendererConfiguration( _In_ const RendererConfiguration& );

		//
		// move ctor
		//
		RendererConfiguration( _In_ const RendererConfiguration&& );

		//
		// assignment operator (per move constructor)
		//
		RendererConfiguration& operator = ( _In_ const RendererConfiguration& );

		//
		// get what method we're using to cull objects off screen
		// - return CollisionMethod: description of culling method
		//
		CollisionMethod GetFrustumCullMethod( ) const;

		//
		// set how we're culling object off screen
		// - CollisionMethod: enumeration describing how we're culling objects
		//
		void SetFrustumCullMethod( _In_ const CollisionMethod );
	};
}

#endif // HYJYNXRENDERER_RENDERERCONFIGURATION_H