/**
 * LinkageController.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_LINKAGECONTROLLER_H
#define HYJYNXRENDERER_LINKAGECONTROLLER_H

#include <Engine\Headers\Text.h>
#include <Engine\Headers\Dictionary.h>
#include <sal.h>
#pragma warning(disable: 4005)
#include <D3D11.h>
#include <D3Dcompiler.h>
#pragma warning(default: 4005)

namespace HyJynxRenderer
{
	class Context;

	//
	// Shader stages with multiple intefaces found (Dynamic Shader Linkage)
	// will be created with this controller as the IO for the consuming Material to use
	//
	class LinkageController
	{
	protected:

		HyJynxCollections::Dictionary< HyJynxCore::Text, UInt > _variables;
		ID3D11ClassInstance**									_linkageArray = nullptr;
		HyJynxCore::Text										_owningShader = "";
		UInt													_interfaceSlotCount = 0;

	public:

		//
		// null ctor
		//
		LinkageController( );

		//
		// default ctor
		// Text: the filepath to the shader this controls
		//
		LinkageController( _In_ const HyJynxCore::Text );

		//
		// copy ctor
		//
		LinkageController( _In_ const LinkageController& );

		//
		// move ctor
		//
		LinkageController( _In_ const LinkageController&& );

		//
		// dtor
		//
		virtual ~LinkageController( );

		//
		// initialize the class instance array 
		// - UInt: number of interface slots
		//
		void InitializeLinkageArray( _In_ const UInt );

		//
		// register the base abstract type variables here, which will be permutated
		// with dx11 linkage (I really hope all of this crap is worth it.)
		// - ID3D11ShaderReflector*: reflector 
		// - Text: name of the abstract shader variable (the one you will be calling in the shader itself)
		// - returns bool: true for success, false for failure
		//
		bool AddAbstractVariable( _In_ ID3D11ShaderReflection* const, _In_ const HyJynxCore::Text );

		//
		// get the array of class linkage permutations
		// - returns ID3D11ClassInstance**: array of class instances
		//
		ID3D11ClassInstance** GetClassLinkageArray( );

		//
		// get the number of interface slots in this linkage
		// - returns UInt: number of interface slots
		//
		UInt GetInterfaceCount( ) const;
	};
}

#endif // HYJYNXRENDERER_LINKAGECONTROLLER_H