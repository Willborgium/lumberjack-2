/**
 * CollisionDescription.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_COLLISIONDESCRIPTION_H
#define HYJYNXRENDERER_COLLISIONDESCRIPTION_H

#include <Renderer\Headers\OrientedBoundingBox.h>
#include <Renderer\Headers\ViewFrustum.h>
#include <DirectXCollision.h>
#include <sal.h>

namespace HyJynxRenderer
{
	//
	// describes what method of collision detection used,
	// ordered by accuracy and speed
	//
	enum class CollisionMethod : short
	{
		NONE					= -1,
		COLLISION_SPHERE		= ( NONE +1 ),
		COLLISION_AABB			= ( COLLISION_SPHERE +1 ),
		COLLISION_OBB_LEVEL1	= ( COLLISION_AABB +1 ),
		COLLISION_OBB_LEVEL2	= ( COLLISION_OBB_LEVEL1 +1 )
	};

	//
	// container providing all collidable objects 
	//
	class CollisionDescription sealed
	{
	private:

		DirectX::BoundingSphere*		_sphere = nullptr;
		DirectX::BoundingBox*			_aabb = nullptr;
		DirectX::BoundingOrientedBox*	_dxobb = nullptr;
		OrientedBoundingBox*			_obb = nullptr;

		//
		// recursive helper function, tests this collisiondescription to the given maximum complexity,
		// starting at the fastest/easiest first
		// - ViewFrustum: frustum we're testing ourselves against
		// - CollisionMethod: the maximum complexity we're testing up to
		// - CollisionMethod: the current complexity we're testing
		// - returns bool: true for collision
		//
		bool testFrustum( _In_ ViewFrustum&, _In_ const CollisionMethod, _In_ const CollisionMethod );

	public:

		//
		// null ctor
		//
		CollisionDescription( );

		//
		// copy ctor
		//
		CollisionDescription( _In_ const CollisionDescription& );

		//
		// move ctor
		//
		CollisionDescription( _In_ const CollisionDescription&& );

		//
		// dtor
		//
		~CollisionDescription( );

		//
		// during collision generation, drawables can optionally set whatever collidables they choose to use
		// - BoundingSphere*: sphere collision object
		// - BoundingBox*: aabb collision object
		// - BoundingOrientedBox*: obb collision object (DirectX)
		// - OrientedBoundingBox*: obb collision object (Dan's)
		//
		void SetCollidables( _In_opt_ DirectX::BoundingSphere* = nullptr,
			_In_opt_ DirectX::BoundingBox* = nullptr,
			_In_opt_ DirectX::BoundingOrientedBox* = nullptr,
			_In_opt_ OrientedBoundingBox* = nullptr );

		//
		// gets the Sphere collision object
		// - returns BoundingSphere*: pointer to the sphere collision object
		//
		DirectX::BoundingSphere* GetBoundingSphere( );

		//
		// gets the axis-aligned bounding box collision object
		// - returns BoundingBox*: pointer to the AABB collision object
		//
		DirectX::BoundingBox* GetBoundingBox( );

		//
		// gets the DIRECT X oriented bounding box collision object.
		// - note: the Level 1 OBB only accounts for rotation, and not scale
		// - returns BoundingOrientedBox*: pointer to the DX OBB collision object
		//
		DirectX::BoundingOrientedBox* GetOBBLevel1( );

		//
		// gets our oriented bounding box collision object.
		// - note: the Level 2 OBB accounts for all transformation properties
		// - returns OrientedBoundingBox*: pointer to the OBB collision object.
		//
		OrientedBoundingBox* GetOBBLevel2( );

		//
		// determine if this collision description is inside of the argued viewfrustum.
		// we will start at the cheapest detection (sphere) and work up to the aruged maximum.
		// defaults to [true] when we are unable to test.
		// - ViewFrustum*: pointer to the frustum in question
		// - CollisionMethod: the maximum collision test we wish to perform
		// - return bool: true - this collisionDescription is inside of the frustum, false - it is not
		//
		bool IsInFrustum( _In_ ViewFrustum&, _In_ const CollisionMethod );

		//
		// get the oriented dimensions of this description
		// - returns Vector3: dimensions of the representing object
		//
		Vector3 GetDimensions( ) const;

	};
}

#endif // HYJYNXRENDERER_COLLISIONDESCRIPTION_H