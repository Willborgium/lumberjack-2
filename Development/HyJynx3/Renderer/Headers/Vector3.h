/**
 * Vector3.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_VECTOR3_H
#define HYJYNXRENDERER_VECTOR3_H


#include <Engine\Headers\TypeDefinitions.h>
#include <DirectXMath.h>
#include <sal.h>

namespace HyJynxRenderer
{
	class Vector2;

	//
	// this extends the directX XMFLOAT3 math primitive,
	// so we can add functionality as we please
	//
	class Vector3 : public DirectX::XMFLOAT3
	{

	public:

#pragma region init

		//
		// null ctor - values default to 0
		//
		Vector3( );

		//
		// single defined ctor - all components set to argued value
		// - float: value for all components to be set to
		//
		Vector3( _In_ const float );

		//
		// xy defined ctor, other components set to 0
		// - float: x component
		// - float: y component
		//
		Vector3( _In_ const float, _In_ const float );

		//
		// fully defined ctor - all components defined
		// - float: X component value
		// - float: Y component value
		// - float: Z component value
		//
		Vector3( _In_ const float, _In_ const float, _In_ const float );

		//
		// base-type ctor
		// - XMFLOAT3: values to initialize with
		//
		Vector3( _In_ const XMFLOAT3& );

		//
		// copy ctor
		//
		Vector3( _In_ const Vector3& );

		//
		// move ctor
		//
		Vector3( _In_ const Vector3&& );

		//
		// get a vector3 with all components set to 1
		// - returns Vector3: (1, 1, 1)
		//
		static Vector3 One( );

		//
		// get a vector3 with all components set to 0
		// - returns Vector3: (0, 0, 0)
		//
		static Vector3 Zero( );

		//
		// normalize the vector so x+y+z equals 1
		//
		void Normalize( );

#pragma endregion

#pragma region operators

		// Arithmetic
		Vector3 operator = ( _In_ const Vector3& );
		Vector3 operator * ( _In_ const Vector3& ) const;
		Vector3 operator * ( _In_ const float ) const;
		Vector3 operator * ( _In_ const double ) const;
		Vector3 operator * ( _In_ const long ) const;
		Vector3 operator * ( _In_ const LongLong ) const;
		Vector3 operator / ( _In_ const Vector3& ) const;
		Vector3 operator / ( _In_ const float ) const;
		Vector3 operator / ( _In_ const double ) const;
		Vector3 operator / ( _In_ const long ) const;
		Vector3 operator / ( _In_ const LongLong ) const;
		Vector3& operator ++ ( );
		Vector3& operator -- ( );

		// Comparison
		bool operator == ( _In_ const Vector3& ) const;
		bool operator != ( _In_ const Vector3& ) const;
		bool operator > ( _In_ const Vector3& ) const;
		bool operator >= ( _In_ const Vector3& ) const;
		bool operator < ( _In_ const Vector3& ) const;
		bool operator <= ( _In_ const Vector3& ) const;

		// Compound
		Vector3& operator += ( _In_ const Vector3& );
		Vector3& operator -= ( _In_ const Vector3& );
		Vector3& operator *= ( _In_ const Vector3& );
		Vector3& operator *= ( _In_ const float );
		Vector3& operator *= ( _In_ const double );
		Vector3& operator *= ( _In_ const long );
		Vector3& operator *= ( _In_ const LongLong );
		Vector3& operator /= ( _In_ const Vector3& );
		Vector3& operator /= ( _In_ const Vector2& );
		Vector3& operator /= ( _In_ const float );
		Vector3& operator /= ( _In_ const double );
		Vector3& operator /= ( _In_ const long );
		Vector3& operator /= ( _In_ const LongLong );

#pragma endregion
	};
}

#endif // HYJYNXRENDERER_VECTOR3_H