/**
 * Materials.h
 * (c) 2014 All Rights Reserved
 */

// Helper include header to get all HyJynx Renderer Materials

#ifndef HYJYNXRENDERER_MATERIALS_H
#define HYJYNXRENDERER_MATERIALS_H

#include <Renderer/Headers/ShaderFactory.h>
#include <Renderer/Headers/MaterialLibrary.h>
#include <Renderer/Headers/Material2D.h>
#include <Renderer/Headers/Material3D.h>

#endif // HYJYNXRENDERER_MATERIALS_H