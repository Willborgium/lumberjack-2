/**
 * LODLevel.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_LODLEVEL_H
#define HYJYNXRENDERER_LODLEVEL_H

#include <sal.h>

namespace HyJynxRenderer
{
	//
	// An LOD level describes a minimum and maximum distance from camera,
	// including any opacity changes for automated LOD fades.
	// Renderer will utilize these to dynamically switch buffers to render
	//
	class LODLevel
	{
	protected:

		float _minimum = LODLevel::LODMinimum;
		float _minFadeStop = LODLevel::LODMinimum;
		float _maximum = LODLevel::LODMaximum;
		float _maxFadeStop = LODLevel::LODMaximum;

	public:

		static float LODMinimum;
		static float LODMaximum;
		static LODLevel LOD_Null;

		//
		// null ctor
		// values default to min and max
		//
		LODLevel( );

		//
		// ctor to set min and max values
		// -float minimum
		// -float maximum
		//
		LODLevel( _In_ const float, _In_ const float );

		//
		// ctor to set min/max, and min/max fade stops
		// eg: {camera} ----minimum--(alpha fade)--fadeStop---(full alpha)--fadeStop--(alpha fade)--maximum---
		// - float: minimum
		// - float: minimum fade stop
		// - float: maximum
		// - float: maximum fade stop
		//
		LODLevel( _In_ const float, _In_ const float, _In_ const float, _In_ const float );

		//
		// copy ctor
		//
		LODLevel( _In_ const LODLevel& );

		//
		// move ctor
		//
		LODLevel( _In_ const LODLevel&& );

		//
		// dtor
		//
		~LODLevel( );

		//
		// if auto-fade LOD is toggled, get the suggested opacity (0-1)
		// - float: disance from camera
		// - returns float: 0-1 opacity 
		//
		float GetSuggestedOpacity( _In_ const float );
	};
}

#endif // HYJYNXRENDERER_LODLEVEL_H