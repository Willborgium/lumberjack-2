/**
 * Material2D.h
 * (c) 2014 All Rights Reserved.
 */

#ifndef HYJYNXRENDERER_MATERIAL2D_H
#define HYJYNXRENDERER_MATERIAL2D_H

#include <Renderer/Headers/Material.h>
#include <sal.h>

namespace HyJynxRenderer
{
	class IDrawable;
	class Mesh;

	//
	// Controls the shader linkage options for drawables rendering into the 2D layer
	//
	class Material2D : public Material
	{
	protected:


	public:

		//
		// null ctor
		//
		Material2D( );

		//
		// copy ctor
		//
		Material2D( _In_ const Material2D& );

		//
		// move ctor
		//
		Material2D( _In_ const Material2D&& );

		//
		// dtor
		//
		virtual ~Material2D( );

		//
		// update and set buffers to the shader which all following objects will use
		// - Context*: dx11 command API container
		//
		virtual void BatchBind( _In_ Context* const ) override;

		//
		// bind the argued drawable containers to the argued constant buffer
		// - IDrawable*: pointer to the drawable in need of binding
		// - Mesh*: pointer to the current mesh to be rendered 
		// - CBObject*: per-object constant buffer who's data needs to be updated
		//
		virtual void ObjectBind( _In_ IDrawable* const, _In_ Mesh* const, _In_ CBObject* const ) override;

	};
}

#endif // HYJYNXRENDERER_MATERIAL2D_H