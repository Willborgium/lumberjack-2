/**
 * Context.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_CONTEXT_H
#define HYJYNXRENDERER_CONTEXT_H


#include <Renderer/Headers/Color.h>
#include <Renderer/Headers/Buffer.h>
#include <Renderer/Headers/DepthStencil.h>
#include <Logger/Headers/ILog.h>

#pragma warning(disable: 4005)
#include <D3D11.h>
#pragma warning(default: 4005)

namespace HyJynxRenderer
{
	class VertexBuffer;
	class IndexBuffer;
	class Texture2D;
	class Texture3D;

	//
	// as objects make their set calls, we will filter what calls will actually do something
	// and what calls are repeats, reducing dx API calls as much as possible.
	// this structure represents all of the current values during runtime for arguments to
	// validate against.
	//
	struct ContextCurrentSettings sealed
	{
		D3D_PRIMITIVE_TOPOLOGY VertexTopology = D3D_PRIMITIVE_TOPOLOGY::D3D10_PRIMITIVE_TOPOLOGY_UNDEFINED;
		ID3D11InputLayout* Layout = nullptr;
		HyJynxCore::Text Rasterizer = "";
		Texture2D* DiffuseTexture = nullptr;
		Texture2D* NormalTexture = nullptr;
		Texture3D* EnvironmentTexture = nullptr;
	};

	//
	// This is the API which will be passed throughout all of the drawables
	// for rendering, or render command recording
	//
	class Context : HyJynxLogger::ILog
	{
	protected:

		ContextCurrentSettings	_current;
		ID3D11DeviceContext*	_deviceContext;

	public:

		//
		// null ctor
		//
		Context( );

		//
		// default used ctor
		//
		Context( _In_ ID3D11DeviceContext* context );

		//
		// copy ctor - not implemented
		//
		Context( _In_ const Context& );

		//
		// dtor
		//
		virtual ~Context( );

		//
		// Get direct access to the DX11 Device Context object
		// Warning: Use at your risk - your skipping all automation, 
		//			observe for unwanted behavior
		// - returns ID3D11DeviceContext*: DX11 context API
		//
		ID3D11DeviceContext* GetDeviceContext( );

#pragma region PreRender

		//
		// set the current render target and depth target
		// - ID3D11RenderTargetView*: active render target all following render calls are drawn to
		// - DepthStencil*: active depth stencil all following render calls are drawn to
		//
		void BindTargets( _In_ ID3D11RenderTargetView*, _In_opt_ DepthStencil* = nullptr );

		//
		// Bind a viewport to the context
		// -D3D11_ViewPort*: vieport to bind to the immediate/deffered context
		//
		void BindViewPort( _In_ UInt, _In_ D3D11_VIEWPORT* );

		//
		// Clear the screen using the given color
		// -Color*: color object you want to sclear the screen with
		//
		void ClearScreen( _In_ ID3D11RenderTargetView*, _In_opt_ Color* = nullptr );

		//
		// Set the active rasterizer, these are pre-loaded inside of Renderer.GetRasterizer( RasterizerState::DefaultSolid );
		// - ID3D11RasterizerState*: the rasterizer state to set active until set otherwise
		//
		void SetRasterizer( _In_ const HyJynxCore::Text& );

		//
		// get the name of the active rasterizer
		// - returns Text: name of the current rasterizer
		//
		HyJynxCore::Text GetCurrentRasterizerState( ) const;

#pragma endregion

#pragma region ConstantBuffers

		//
		// map the projection constant buffer for cpu use
		// returns CBCameraProjection*: camera projection constant buffer
		//
		CBCameraProjection* GetProjectionConstantBuffer( );

		//
		// updates the projection matrix on the gpu, when updates are complete call Context::UpdateProjectionConstantBuffer
		// - CBCameraProjection*: constant buffer containing camera projection matrix, which doesn't update often
		//
		void UpdateProjectionConstantBuffer( );

		//
		// map the cycle constant buffer for cpu use, when updates are complete call Context::UpdateCycleConstantBuffer
		// returns CBCycle*: cycle constant buffer
		//
		CBCycle* GetCycleConstantBuffer( );

		//
		// Renderer will update the timer cycle buffers before a render sequence begins
		// - CBCycle*: constant buffer containing per-cycle values, such as time
		//
		void UpdateCycleConstantBuffer( );

		//
		// map the camera constant buffer for cpu use, when updates are complete, call Context::UpdateCameraConstantBuffer
		// - returns CBCamera*: camera values constant buffer
		//
		CBCamera* GetCameraConstantBuffer( );

		//
		// Update the necessary camera buffers when changes occur, Camera object will manage this, and fire this function
		// - CBCamera*: constant buffer containing Camera values, which updates only when the camera changes
		//
		void UpdateCameraConstantBuffer( );

		//
		// map the per-object constant buffer for cpu use, when updates are complete call Context::UpdateObjectConstantBuffer
		// - returns CBObject*: per-object constant buffer
		//
		CBObject* GetObjectConstantBuffer( );

		//
		// Each object will call this to update their associated constant buffer
		// The buffer passed in here should contain the object's world matrix and material properties
		// - Please remember to have these values 16-byte aligned for a near 30x performance boost (damn!)
		// - CBObject*: reference to the per-object Constant Buffer container
		//
		void UpdateObjectConstantBuffer( );

		//
		// Set the Constant Buffers as active on the GPU
		// Shader Stages are using a shared constant repository for optimal performace
		// - UInt: constant slot index
		// - ID3D11Buffer*: buffer to bind
		//
		void SetVertexConstantBuffer( _In_ const UInt, _In_ ID3D11Buffer* );

		//
		// bind a list of sequential buffers at once
		// - UInt: starting index of the first buffer
		// - ID3D11Buffer*: array of ID3D11Buffer objects
		// - UInt: total amount of buffers your binding
		//
		void SetVertexConstantBuffer( _In_ const UInt, _In_ ID3D11Buffer*, _In_ const UInt );

#pragma endregion

#pragma region BindShaders

		//
		// Set the argued shader component as the active shader on the GPU
		// - ID3D11VertexShader*: DX11 Shader Inferface
		//
		void BindVertexShader( _In_ ID3D11VertexShader* );

		//
		// Set the argued shader component as the active shader on the GPU
		// - ID3D11VertexShader*: DX11 Shader Inferface
		// - ID3D11ClassInstance**: array of class instances acting as active shader linkages
		//
		void BindPixelShader( _In_ ID3D11PixelShader*, _In_opt_ ID3D11ClassInstance** = nullptr, _In_opt_ UInt = 0 );

		//
		// Set the argued shader component as the active shader on the GPU
		// - ID3D11VertexShader*: DX11 Shader Inferface
		//
		void BindHullShader( _In_ ID3D11HullShader*, _In_opt_ ID3D11ClassInstance** = nullptr, _In_opt_ UInt = 0 );

		//
		// Set the argued shader component as the active shader on the GPU
		// - ID3D11VertexShader*: DX11 Shader Inferface
		//
		void BindDomainShader( _In_ ID3D11DomainShader*, _In_opt_ ID3D11ClassInstance** = nullptr, _In_opt_ UInt = 0 );

		//
		// Set the argued shader component as the active shader on the GPU
		// - ID3D11VertexShader*: DX11 Shader Inferface
		//
		void BindGeometryShader( _In_ ID3D11GeometryShader*, _In_opt_ ID3D11ClassInstance** = nullptr, _In_opt_ UInt = 0 );
	
#pragma endregion

#pragma region Mapping

		//
		// maps the given buffer pointer for CPU usage, we will consume the cast so
		// you can template what your return object will be.
		// When your done fiddling with your data, be sure to call .UnMapData( buffer )
		// - ID3D11Buffer*: subResource buffer pointer
		// - UInt: SubResource index (idk what this is for yet, just pass 0)
		// - D3D11_Map: mapping method
		// - UInt: mapping falgs
		//
		template< typename T >
		T MapData( _In_ ID3D11Buffer* buffer, _In_ UInt subResource, 
			_In_ D3D11_MAP mapMethod, _In_ UInt flags )
		{
			if ( buffer == nullptr )
			{
				return nullptr;
			}

			D3D11_MAPPED_SUBRESOURCE resource;
			ZeroMemory( &resource, sizeof( D3D11_MAPPED_SUBRESOURCE ) );
			HRESULT result = _deviceContext->Map( buffer,
				subResource, mapMethod, 0, &resource );

			if ( SUCCEEDED( result ) )
			{
				return reinterpret_cast< T >( resource.pData );
			}
			else
			{
				return nullptr;
			}
		}

		//
		// UnMap a recently mapped resource data, this unlocks it for GPU usage again
		// - ID3D11Buffer*: the buffer to un-map back to GPU usage
		//
		void UnMapData( _In_ ID3D11Buffer* );

#pragma endregion

#pragma region Drawing

		//
		// bind the vertex topology
		// - ID3D11InputLayout*: dx input layout object
		//
		void BindVertexTopology( _In_ ID3D11InputLayout* );

		//
		// set a bound vertex buffer as active
		// - VertexBuffer: verticies to draw
		// - IndexBuffer: vertex drawing order
		//
		void SetActiveBuffers( _In_ VertexBuffer*, _In_ IndexBuffer* );

		//
		// set the topology of the primitive, use values from D3D_PRIMITIVE_TOPOLOGY enumeration
		// - D3D_PRIMITIVE_TOPOLOGY: topology method of the primitive
		//
		void SetPrimitiveTopology( _In_ const D3D_PRIMITIVE_TOPOLOGY );

		//
		// draw the active set buffers
		// - UInt: number of indicies
		// - UInt: location of the first index
		// - int: offet of the indicies
		//
		void DrawIndexed( _In_ const UInt, _In_ const UInt, _In_ const int );

#pragma endregion
	};
}

#endif // HYJYNXRENDERER_CONTEXT_H