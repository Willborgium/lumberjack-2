/**
 * Primitive.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_PRIMITIVE_H
#define HYJYNXRENDERER_PRIMITIVE_H

#include <Renderer/Headers/IDrawable.h>
#include <Renderer/Headers/PrimitiveConfiguration.h>

namespace HyJynxRenderer
{
	//
	// This provides some automation and base-functionality for all
	// primitive drawables to derive
	//
	class Primitive abstract
	{
	protected:

		PrimitiveConfiguration* _configuration;

		//
		// during contruction, Primitive base class will call this on the derived class
		// to generate the buffer data
		//
		virtual void GeneratePrimitiveModel( ) = 0;

	public:

		//
		// null ctor - should not be used.
		//
		Primitive( );

		//
		// default ctor - config defined
		// -PrimitiveConfiguration*: pointer to a derived configuration object
		//
		Primitive( _In_ PrimitiveConfiguration* );

		//
		// copy ctor
		//
		Primitive( _In_ const Primitive& );

		//
		// dtor
		//
		virtual ~Primitive( );

		//
		// configuration retrieval
		// -returns: PrimitiveConfiguration*
		//
		PrimitiveConfiguration* GetPrimitiveConfiguration( ) const;
	};
}

#endif // HYJYNXRENDERER_PRIMITIVE_H