/**
 * Buffer.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_BUFFER_H
#define HYJYNXRENDERER_BUFFER_H

#pragma warning(disable: 4005)
#include <D3D11.h>
#pragma warning(default: 4005)

namespace HyJynxRenderer
{
	//
	// this enumeration describes at what constant buffer registries the
	// associated buffers are bound to
	//
	enum class ConstantBufferIndicies : int
	{
		CONSTANT_BUFFER_NULL = -1,
		CONSTANT_BUFFER_CAMERA_PROJECTION = 0,
		CONSTANT_BUFFER_CAMERA = 1,
		CONSTANT_BUFFER_CYCLE = 2,
		CONSTANT_BUFFER_OBJECT = 3
	};

	//
	// This Constant Buffer contains the rarely changing projection matrix and screen information
	// (See HyJynxConstantBuffer.hlsli for GPU-structure)
	//
	__declspec( align( 16 ) ) struct CBCameraProjection sealed
	{
		DirectX::XMFLOAT4X4 ProjectionMatrix;
		DirectX::XMFLOAT4X4 OrthographicMatrix;
		DirectX::XMFLOAT4 ScreenSize_HalfPixel;
	};

	//
	// This Constant Buffer contains per-frame Camera values
	// (See HyJynxConstantBuffer.hlsli for GPU-structure)
	//
	__declspec( align( 16 ) ) struct CBCamera sealed
	{
		DirectX::XMFLOAT4X4 ViewMatrix;
		DirectX::XMFLOAT4 CameraPosition;
		DirectX::XMFLOAT4 CameraVector;
	};

	//
	// This Constant Buffer contains per-frame cycle values
	// (See HyJynxConstantBuffer.hlsli for GPU-structure)
	//
	__declspec( align( 16 ) ) struct CBCycle sealed
	{
		DirectX::XMFLOAT4 Time;
		DirectX::XMFLOAT4 SkyColor;
		DirectX::XMFLOAT4 GroundColor;
	};

	//
	// This Constant Buffer contains values every object will contain
	// (See HyJynxConstantBuffer.hlsli for GPU-structure)
	//
	__declspec( align( 16 ) ) struct CBObject sealed
	{
		DirectX::XMFLOAT4X4 WorldMatrix;
	};

}

#endif // HYJYNXRENDERER_BUFFER_H