/**
 * RendererCore.h
 * (c) 2014 All Rights Reserved
 */

//
// Helper File to include all of Renderer Components in one file
//

#ifndef HYJYNXRENDERER_RENDERERCORE_H
#define HYJYNXRENDERER_RENDERERCORE_H

#include <Renderer/Headers/Renderer.h>
#include <Renderer/Headers/DiffuseRenderSequence.h>
#include <Renderer/Headers/RenderSequenceNull.h>
#include <Renderer/Headers/RenderMathInclude.h>
#include <Renderer/Headers/Drawables.h>
#include <Renderer/Headers/Materials.h>
#include <Renderer/Headers/SceneLoader.h>
#include <Renderer/Headers/FBXSceneIO.h>

#endif // HYJYNXRENDERER_RENDERERCORE_H