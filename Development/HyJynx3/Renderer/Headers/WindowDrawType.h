/**
 * WindowDrawType.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_WINDOWDRAWTYPE_H
#define HYJYNXRENDERER_WINDOWDRAWTYPE_H

namespace HyJynxRenderer
{
	//
	// This enum will describe to renderer what type of stuff its drawing,
	// Renderer in turn will utilize this to optimize the window draw order
	//
	enum class WindowDrawType : char
	{
		WINDOW_DRAW_2D,
		WINDOW_DRAW_3D
	};
}

#endif // HYJYNXRENDERER_WINDOWDRAWTYPE_H