/**
 * Mesh.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_MESH_H
#define HYJYNXRENDERER_MESH_H

#include <Renderer\Headers\VertexBuffer.h>
#include <Renderer\Headers\IndexBuffer.h>
#include <Renderer\Headers\Material.h>
#include <Renderer\Headers\LODLevel.h>
#include <Renderer\Headers\Context.h>
#include <sal.h>

namespace HyJynxRenderer
{
	class IDrawable;

	//
	// The is the primary datatype containing everything needed to put a single 
	// 'thing' to the screen. 
	// This contains:
	//		- the vertex/index buffers
	//		- the material (Shader API)
	//		- the LOD specifier, (Where, in terms of distance from camera, you want this to draw, it'll default visible don't worry)
	//		- the name (Main Identifier) of the mesh
	//
	class Mesh
	{
	protected:

		VertexBuffer*		_vertexBuffer = nullptr;
		IndexBuffer*		_indexBuffer = nullptr;
		Material*			_material = nullptr;
		LODLevel*			_lodLevel = &LODLevel::LOD_Null;
		HyJynxCore::Text	_name = "";

	public:

		//
		// null ctor
		//
		Mesh( );

		//
		// drawable ctor
		// -VertexBuffer*
		// -IndexBuffer*
		//
		Mesh( _In_ VertexBuffer*, _In_ IndexBuffer* );

		//
		// drawable ctor with material
		// -VertexBuffer*
		// -IndexBuffer*
		// -Material*
		//
		Mesh( _In_ VertexBuffer*, _In_ IndexBuffer*, _In_ Material* );

		//
		// fully defined drawable ctor
		// -VertexBuffer*
		// -IndexBuffer*
		// -Material*
		// -LODLevel*
		//
		Mesh( _In_ VertexBuffer*, _In_ IndexBuffer*, _In_ Material*, _In_ LODLevel* );

		//
		// copy ctor
		//
		Mesh( _In_ const Mesh& );

		//
		// move ctor
		//
		Mesh( _In_ const Mesh&& );

		//
		// dtor
		//
		virtual ~Mesh( );

		//
		// Render our buffers to the screen
		// - IDrawable*: pointer to the drawable who owns this Mesh
		// - Context*: API container for interacting with the back buffer
		//
		void Render( _In_ IDrawable* const, _In_ Context* const );

		//
		// get the vertex buffer used in this mesh
		// -returns: VertexBuffer*
		//
		VertexBuffer* const GetVertexBuffer( ) const;

		//
		// get the index buffer used in this mesh
		// -returns: IndexBuffer*
		//
		IndexBuffer* const GetIndexBuffer( ) const;

		//
		// get the material used for this mesh
		// -returns: Material*
		//
		Material* const GetMaterial( ) const;

		//
		// apply a new material to this mesh
		// - Mesh*: pointer to the material you wish to apply
		//
		void SetMaterial( _In_ Material* );

		//
		// get the LOD object describing 'where in distance' this mesh will draw
		// -returns: LODLevel*
		//
		const LODLevel* const GetLOD( ) const;

		//
		// get the name idnetifier of this mesh
		// - returns Text: name of the mesh
		//
		const HyJynxCore::Text& GetName( ) const;

		//
		// override the loaded name identifer of this mesh with another
		// - Text: new name of the mesh
		//
		void SetName( _In_ const HyJynxCore::Text& );
	};
}

#endif // HYJYNXRENDERER_MESH_H