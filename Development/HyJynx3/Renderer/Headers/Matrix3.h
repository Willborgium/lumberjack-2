/**
 * Matrix3.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_MATRIX3_H
#define HYJYNXRENDERER_MATRIX3_H

#include <DirectXMath.h>
#include <sal.h>
#include <Engine\Headers\TypeDefinitions.h>

namespace HyJynxRenderer
{
	//
	// Matrix3 represents the orientation values from a 4x4 matrix.
	// This class is designed to be STACK ONLY - DO NOT HEAP ALLOCATE
	// Matrix3 is a helper object for HyJynxRenderer::OrientedBoundingBox
	// don't try and use yourself.
	//
	class Matrix3
	{
	protected:

		DirectX::XMVECTOR _elements[ 3 ];

	public:

		//
		// null ctor - defaults to identity
		//
		Matrix3( );

		//
		// creates a new rotational 3x3 matrix
		// -XMMATRIX: 4x4 matrix this will construct from
		//
		Matrix3( _In_ DirectX::XMFLOAT4X4 );

		//
		// sets matrix elements to an absolute value
		//
		void Absolute( );

		//
		// get a row of values from the given index
		// -UINT: index of the requested row
		//
		DirectX::XMVECTOR GetRow( _In_ UInt );

		//
		// get a column of vlaues from the given index
		// -UINT: index of the requested column
		//
		DirectX::XMVECTOR GetColumn( _In_ UInt );

		//
		// get a value of the matrix
		// -UInt: row index
		// -UInt: column index
		//
		float GetValue( _In_ UInt, _In_ UInt );
	};
}

#endif // HYJYNXRENDERER_MATRIX3_H