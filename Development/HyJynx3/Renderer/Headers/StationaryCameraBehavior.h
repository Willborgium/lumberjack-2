/**
 * StationaryCameraBehavior.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_STATIONARYCAMERABEHAVIOR_H
#define HYJYNXRENDERER_STATIONARYCAMERABEHAVIOR_H

#include <Renderer/Headers/ICameraBehavior.h>
#include <sal.h>

namespace HyJynxRenderer
{
	//
	// Implements ICamera with stationary behavior
	//
	class StationaryCameraBehavior : public ICameraBehavior
	{
	protected:

	public:

		//
		// null ctor
		//
		StationaryCameraBehavior();

		//
		// copy ctor
		//
		StationaryCameraBehavior( _In_ const StationaryCameraBehavior& );

		//
		// move ctor
		//
		StationaryCameraBehavior( _In_ const StationaryCameraBehavior&& );

		// 
		// dtor
		//
		~StationaryCameraBehavior();

		//
		// per-cycle update call
		//
		virtual void Update() override;

	};
}

#endif // HYJYNXRENDERER_STATIONARYCAMERABEHAVIOR_H