/**
 * ActiveAnimation.h
 * (c) 2014 All Rights Reserved
 */

#include <Engine\Headers\Function.h>
#include <sal.h>

namespace HyJynxRenderer
{
	//
	// internal wrapper of a current animation
	//
	template< typename AnimType >
	class ActiveAnimation sealed : public IAnimation
	{
	public:

		AnimType								StartValue;
		AnimType								EndValue;
		HyJynxCore::Function< void, AnimType* >	SetProxy = nullptr;
		HyJynxCore::Function< void >			OnCompleteCallback = nullptr;
		double									Duration = 0.0;
		double									Progress = 0.0;
		double									Speed = 60.0;
		bool									Active = true;
		bool									Valid = true;

		//
		// null ctor
		//
		ActiveAnimation( )
		{ }

		//
		// default ctor
		// - AnimType: start value
		// - AnimType: end value
		//
		ActiveAnimation( _In_ AnimType start, _In_ AnimType end )
			: StartValue( start ),
			EndValue( end )
		{ }

		//
		// Per-cycle update
		// - double: delta milliseconds
		//
		virtual void Update( _In_ const double delta ) override
		{
			// TODO: proxy this math to the Tween interface
		}

		//
		// determine the active state of the animation
		// - returns bool: true - the animation is updating, false - tis not
		//
		virtual bool IsActive( ) override
		{
			return Active;
		}

		//
		// set the animation active state
		// bool: true - the animation is updating, false - tis not
		//
		virtual void SetActive( _In_ const bool val )
		{
			Active = val;
		}

		//
		// determine the animation's progress on a 0 to 1 scale
		// - returns double: 0-1 progress value
		//
		virtual double GetProgress( ) override
		{
			return Progress / Duration;
		}

		//
		// set the animation's progress on a 0 to 1 scale
		// - double: zero to one scale of progress
		//
		virtual void SetProgress( _In_ const double val ) override
		{
			Progress = RenderMath::Clamp< double >( val, 0, 1 );
		}

		//
		// get FPS
		// - return double: frames per second
		//
		virtual double GetSpeed( ) override
		{
			return Speed;
		}

		//
		// set FPS
		// - double: frames per second
		//
		virtual void SetSpeed( _In_ const double value ) override
		{
			Speed = value >= 0 ? value : 0.0;
		}

		//
		// release all references for disposal
		//
		virtual void Release( ) override
		{
			Remove( );
			SetProxy.ClearCapture();
			OnCompleteCallback.ClearCapture();
			Active = false;
			Valid = false;
		}

		//
		// determine if this animation is active with Animator
		//
		virtual bool IsValid( )
		{
			return Valid;
		}
	};
}