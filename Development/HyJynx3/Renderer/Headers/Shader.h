/**
 * Shader.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_SHADER_H
#define HYJYNXRENDERER_SHADER_H

#include <Renderer\Headers\LinkageController.h>
#include <Engine\Headers\Text.h>
#include <Logger\Headers\ILog.h>

#include <sal.h>
#pragma warning(disable: 4005)
#include <D3D11.h>
#pragma warning(default: 4005)

namespace HyJynxRenderer
{
	class Context;

	//
	// Base class for all shader objects to derive from.
	// All shader objects composed here are optional, but at least vertex and pixel
	// shaders are needed in order to render.
	//
	class Shader : public HyJynxLogger::ILog
	{
		friend class ShaderFactory;

	public:

		//
		// provides static paths to the general vertex shaders used with different materials.
		// You can always create and specify your own of course.
		//
		static class VertexShader abstract sealed
		{
		public:
			static const HyJynxCore::Text UnProjected;
			static const HyJynxCore::Text Projected;
			static const HyJynxCore::Text SkeletonUnProjected;
			static const HyJynxCore::Text SkeletonProjected;
		};

	protected:

		ID3D11InputLayout*		_inputLayout = nullptr;

		HyJynxCore::Text		_vertexPath = NoShaderPath;
		ID3D11VertexShader*		_vertexShader = nullptr;

		HyJynxCore::Text		_pixelPath = NoShaderPath;
		ID3D11PixelShader*		_pixelShader = nullptr;
		LinkageController*		_linkageController = nullptr;

		HyJynxCore::Text		_hullPath = NoShaderPath;
		ID3D11HullShader*		_hullShader = nullptr;

		HyJynxCore::Text		_domainPath = NoShaderPath;
		ID3D11DomainShader*		_domainShader = nullptr;

		HyJynxCore::Text		_geometryPath = NoShaderPath;
		ID3D11GeometryShader*	_geometryShader = nullptr;

	public:

		static const HyJynxCore::Text		NoShaderPath;

#pragma region init

		//
		// null ctor - should not be used
		//
		Shader( );

		//
		// copy ctor
		//
		Shader( _In_ const Shader& );

		//
		// move ctor
		//
		Shader( _In_ const Shader&& );

		//
		// dtor
		//
		virtual ~Shader( );

		//
		// release run-time object pointers
		// will not release directX shader objects, as other materials may be referencing them.
		//
		void Release( );

		//
		// assignment operator
		//
		Shader& operator = ( _In_ const Shader& );

		//
		// comparison operator
		//
		bool operator == ( _In_ const Shader& );

#pragma endregion

#pragma region runtime

		//
		// bind this shader as the active GPU shader
		// - Context: device context API container
		//
		void Bind( _In_ Context* const );

#pragma endregion

#pragma region Accessors

		//
		// get the vertex input layout
		// - returns ID3D11InputLayout*: dx11 vertex layout
		//
		ID3D11InputLayout* const GetInputLayout( );

#pragma endregion

	};
}

#endif // HYJYNXRENDERER_SHADER_H