/**
 * Color.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_COLOR_H
#define HYJYNXRENDERER_COLOR_H

#include <Renderer/Headers/Vector3.h>
#include <Renderer/Headers/Vector4.h>
#include <Engine\Headers\Dirty.h>
#include <DirectXMath.h>
#include <sal.h>

namespace HyJynxRenderer
{
	//
	// This is a wrapper for all sorts of API to manipulate color values
	// -we store both the rgba color, and the multiplied color
	// as a developer you interact with RGBA color as its much more readable
	//
	class Color : HyJynxUtilities::Dirty
	{
	protected:

		Vector4 _multipliedColor = Vector4( 1.0f, 1.0f, 1.0f, 1.0f );
		Vector4 _rgbaColor = Vector4( 255.0f, 255.0f, 255.0f, 255.0f );

		// update multiplied color from rgba color
		void multiplyColor( );

		// perform all updates
		virtual void Clean( );

	public:

		//
		// a repository of pre-defined mutiplied colors in Vector4 format
		//
		static class PreDefined abstract sealed
		{
		public:
			static const Vector4 White;
			static const Vector4 Black;
		};

		//
		// null ctor - defaults to full opacity white
		//
		Color( );

		//
		// gray ctor - defaults RGB to specifiied value, full opacity
		// -float: 0-255 gray scale
		//
		Color( _In_ float );

		//
		// color defined ctor - RGB values are set
		// -float: 0-255 RED
		// -float: 0-255 GREEN
		// -float: 0-255 BLUE
		//
		Color( _In_ float, _In_ float, _In_ float );

		//
		// color and alpha defined ctor - RGBA values are set
		// -float 0-255 RED
		// -float 0-255 GREEN
		// -float 0-255 BLUE
		// -float 0-255 ALPHA
		//
		Color( _In_ float, _In_ float, _In_ float, _In_ float );

		//
		// color defined ctor - RGB values set
		// -XMFLOAT3: container for all RGB values
		//
		Color( _In_ Vector3 );

		//
		// color/alpha defined ctor - RGBA calues set
		// -XMFLOAT4: container for all RGBA values
		//
		Color( _In_ Vector4 );

		//
		// copy ctor
		//
		Color( _In_ const Color& );

		//
		// move ctor
		//
		Color( _In_ const Color&& );

		//
		// dtor
		//
		virtual ~Color( );

		//
		// set this color by the values of another
		// -Color*: pointer to the color being duplicated
		//
		void SetColor( _In_ Color* color );

		//
		// Set the color to a full opaque grey scale
		// -float: RGB grey scale
		//
		void SetColor( _In_ float );

		//
		// Set the RGB color, alpha remains unchanged
		// -float: red component 0-255
		// -float: green component 0-255
		// -float: blue component 0-255
		//
		void SetColor( _In_ float, _In_ float, _In_ float );

		//
		// Set the RGBA color
		// -float: red component 0-255
		// -float: green component 0-255
		// -float: blue component 0-255
		// -float: alpha component 0-255
		//
		void SetColor( _In_ float, _In_ float, _In_ float, _In_ float );

		//
		// set the 0-1 multiplied RGB color 
		// - Vector3: RGB color values between 0 and 1
		//
		void SetMultipliedColor( _In_ Vector3& );

		//
		// set the 0-1 multiplied RGBA color
		// - Vector4: RGBA color values between 0 and 1
		//
		void SetMultipliedColor( _In_ Vector4& );

		//
		// Set the color to a full opaque grey scale
		// -float: RGB grey scale (0-1)
		//
		void SetMultipliedColor( _In_ float );

		//
		// Set the RGB color, alpha remains unchanged
		// -float: red component 0-1
		// -float: green component 0-1
		// -float: blue component 0-1
		//
		void SetMultipliedColor( _In_ float, _In_ float, _In_ float );

		//
		// Set the RGBA color
		// -float: red component 0-1
		// -float: green component 0-1
		// -float: blue component 0-1
		// -float: alpha component 0-1
		//
		void SetMultipliedColor( _In_ float, _In_ float, _In_ float, _In_ float );

		//
		// get the RGBA (0-255) color value
		// returns: XMFLOAT4
		//
		const Vector4& GetRGBAColor( );

		//
		// get the multiplied rgba (0-1) color value, loaded into a SIMD register
		// returns XMVECTOR
		//
		DirectX::XMVECTOR GetMultipliedColorXM( );

		//
		// Get the multiplied RGBA (0-1) color value
		// returns: XMFLOAT4
		//
		Vector4* const GetMultipliedColor( );

		//
		// Get the Red Component (0-255)
		//
		float GetRed( ) const;

		//
		// Get the Blue Component (0-255)
		//
		float GetGreen( ) const;

		//
		// Get the Green Component (0-255)
		//
		float GetBlue( ) const;

		//
		// Get the Alpha Component (0-255)
		//
		float GetAlpha( ) const;

		//
		// Get the Red Component (0-1)
		//
		float GetMultipliedRed( ) const;

		//
		// Get the Green Component (0-1)
		//
		float GetMultipliedGreen( ) const;

		//
		// Get the Blue Component (0-1)
		//
		float GetMultipliedBlue( ) const;

		//
		// Get the Alpha Component (0-1)
		//
		float GetMultipliedAlpha( ) const;


		// arithmetic
		Color operator = ( _In_ const Color& );
		Color operator + ( _In_ const Color& ) const;
		Color operator - ( _In_ const Color& ) const;
		Color& operator - ( );
		Color operator * ( _In_ Color& );
		Color operator / ( _In_ Color& );
		Color& operator ++ ( );
		Color& operator -- ( );

		// comparison
		bool operator == ( _In_ const Color& ) const;
		bool operator != ( _In_ const Color& ) const;
		bool operator < ( _In_ const Color& ) const;
		bool operator <= ( _In_ const Color& ) const;
		bool operator > ( _In_ const Color& ) const;
		bool operator >= ( _In_ const Color& ) const;

		// compound
		Color& operator += ( _In_ const Color& );
		Color& operator -= ( _In_ const Color& );
		Color& operator *= ( _In_ Color& );
		Color& operator /= ( _In_ Color& );

		// index directly into the RGBA color values
		Color operator [] ( const int );
	};
}

#endif // HYJYNXRENDERER_COLOR_H