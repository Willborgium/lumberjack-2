/**
 * Animator.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_ANIMATOR_H
#define HYJYNXRENDERER_ANIMATOR_H

#include <Renderer/Headers/IAnimation.h>
#include <Renderer/Headers/ActiveAnimation.h>
#include <Renderer/Headers/AnimationController.h>
#include <Renderer/Headers/Transform.h>
#include <Renderer/Headers/RenderMath.h>
#include <Engine\Headers\Singleton.h>
#include <Engine\Headers\DynamicCollection.h>
#include <Engine\Headers\Function.h>
#include <Logger\Headers\ILog.h>
#include <sal.h>

namespace HyJynxRenderer
{
	//
	// Animator is the meca for managing and automating all sorts of animations
	// Front-End will use Animator to begin animations, but all further control will be
	// found within the returned AnimationController object.
	//
	class Animator : public HyJynxDesign::Singleton< Animator >, public HyJynxLogger::ILog
	{
		friend HyJynxDesign::Singleton < Animator >;
		friend class IAnimation;

	private:

		HyJynxCollections::DynamicCollection< IAnimation* > _animationList;

		// Unimplemented
		Animator( );
		Animator( _In_ const Animator& ) = delete;
		Animator( _In_ const Animator&& ) = delete;
		Animator operator = ( _In_ const Animator& ) = delete;

		//
		// stops and removes an active animation, called from IAnimation interface
		// via an AnimationController, or by Animator when the animation completes.
		// - IAnimation*: active animation
		//
		void Remove( _In_ IAnimation* );

	public:

		//
		// dtor
		//
		~Animator( );

		//
		// Process active animations
		//
		void Update( );

		//
		// Begin an animation on the given type
		// T*: address of an object which can be interpolated
		// T*: final values the interpolation will end
		// float: millisecond duration
		// function<void(void)>: callback made when the animation completes
		// - returns AnimationController*: API container to further control the current animation
		//
		template< typename T >
		AnimationController* Animate( _In_ T* object, 
			_In_ T* endVal, 
			_In_ double duration, 
			_In_opt_ HyJynxCore::Function< void > onComplete,
			_In_opt_ HyJynxCore::Function< void, T* > setProxy )
		{
			if ( object == nullptr )
			{
				LogError( "Animate(nullptr) - First three arguments must not be nullptr or 0." );
				return nullptr;
			}
			
			ActiveAnimation< T >* animation = anew( ActiveAnimation< T > )  ( *object, *endVal );
			
			if ( setProxy.HasValue() == true )
			{
				animation->SetProxy = setProxy;
			}
			else
			{
				HyJynxCore::Function< void, T* > proxy = [ ] ( _In_ HyJynxCore::CaptureData& capture, _In_ T* value )
				{
					T* object = capture[ "animObject" ].As< T* >( );
					*object = *value;
				};

				proxy.Capture( "animObject", object );

				animation->SetProxy = proxy;
			}

			if ( onComplete.HasValue() == true )
			{
				animation->OnCompleteCallback = onComplete;
			}

			animation->Duration = duration;

			_animationList.Append( animation );

			return anew( AnimationController )( animation );
		}

	};
}

#endif // HYJYNXRENDERER_ANIMATOR_H