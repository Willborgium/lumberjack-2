/**
 * RenderReport.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_RENDERREPORT_H
#define HYJYNXRENDERER_RENDERREPORT_H

#include <Engine\Headers\HighPrecisionTimer.h>
#include <Engine\Headers\TimeSpan.h>
#include <Engine\Headers\Function.h>
#include <sal.h>

namespace HyJynxRenderer
{

#define RENDER_STATUS_FAIL	0x0
#define RENDER_STATUS_OK	0x1
#define RENDER_STATUS_SLOW	0x01


	//
	// This object tracks the performance of renderer
	// down to the freakin microsecond (Thanks Will.)
	// In turn when performance drops, a seperate component
	// can optionally attempt to optimize settings to maintain framerate
	//
	class RenderReport sealed
	{
	private:

		HyJynxCore::HighPrecisionTimer _cycleCounter;
		HyJynxCore::HighPrecisionTimer _stepCounter;
		HyJynxCore::HighPrecisionTimer _avgCounter;
		HyJynxCore::Function< void > AvgIntervalCallback;

		HyJynxCore::TimeSpan _updateTime;
		HyJynxCore::TimeSpan _avgUpdateTime;
		
		HyJynxCore::TimeSpan _renderTime;
		HyJynxCore::TimeSpan _avgRenderTime;

		HyJynxCore::TimeSpan _cycleTime;
		HyJynxCore::TimeSpan _avgCycleTime;

		double _avgInterval;

		UINT _targetFrameRate;

		bool _targetFrameRateMet;
		
		bool _avgActive;
		
		char _renderStatus;

	
		//
		// logic to update the average values
		//
		void UpdateAverages();

	public:

		//
		// default ctor
		//
		RenderReport();

		//
		// reset cycle counters, sequence is about to begin
		//
		void MarkSequenceStart();

		//
		// mark cycle drawable update begin time
		//
		void MarkUpdateStart();

		//
		// mark cycle drawable update end time
		//
		void MarkUpdateEnd();

		//
		// mark cycle drawable render time
		//
		void MarkRenderStart();

		//
		// mark cycle drawable render end time
		//
		void MarkRenderEnd();

		//
		// calculate averages and determine if we're running OK
		//
		void Finalize();

		//
		// Set Averaging Active (good for loading times when we don't want to calculate)
		// -bool: true - will calculate averages, false - will not
		//
		void SetAveragingActive(_In_ bool);

		//
		// determine if we are currently calculating averages
		//
		bool GetAveragingActive() const;

		//
		// determine the status of the current render cycle
		// returns char: definitions RENDER_STATUS_OK or RENDER_STATUS_FAIL
		//
		char GetRenderStatus() const;

		//
		// set by Renderer indicating a successfull or failing render cycle
		// -char: definitions RENDER_STATUS_OK or RENDER_STATUS_FAIL
		//
		void SetRenderStatus(_In_ char);
	};
}

#endif // HYJYNXRENDERER_RENDERREPORT_H