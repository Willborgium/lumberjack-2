/**
 * Window.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_WINDOW_H
#define HYJYNXRENDERER_WINDOW_H

#include <Renderer/Headers/Transform.h>
#include <Renderer/Headers/IDrawable.h>
#include <Renderer/Headers/CollisionDescription.h>
#include <Renderer/Headers/ViewFrustum.h>
#include <Renderer/Headers/BatchContainer.h>
#include <Renderer/Headers/WindowDrawType.h>
#include <Renderer/Headers/DrawableContainer.h>
#include <Engine/Headers/TypeDefinitions.h>
#include <Engine/Headers/TypeDefinitions.h>
#include <Engine/Headers/Text.h>
#include <Logger/Headers/ILog.h>
#include <sal.h>

namespace HyJynxRenderer
{
	//
	// This represents an on-screen rectangle which Renderer will draw things into.
	// These objects are created and put inside of the Screen object, which is where the HWND resides.
	// Just to reiterate: This is NOT the actual window which the user sees on his monitor.
	//
	class Window : public HyJynxLogger::ILog
	{
	protected:

		WindowDrawType			_drawType = WindowDrawType::WINDOW_DRAW_2D;
		Transform				_transform = Transform( );
		HyJynxCore::Text		_name = "no-name";
		CollisionDescription	_collisionDescription = CollisionDescription( );
		UINT					_width = 512;
		UINT					_height = 256;
		bool					_lockedZ = false;
		bool					_active = true;

		HyJynxCollections::DynamicCollection<IDrawable*> _drawableList = HyJynxCollections::DynamicCollection< IDrawable* >( );
		HyJynxCollections::DynamicCollection<DrawableContainer*> _containerList = HyJynxCollections::DynamicCollection< DrawableContainer* >( );

	public:

		//
		// Null Ctor - should never be used
		//
		Window( );

		//
		// Default used ctor #1 (width and height default to 512/256)
		// -WindowDrawType: whether this draws in 2D or 3D
		//
		Window( _In_ WindowDrawType, _In_opt_ HyJynxCore::Text = "" );

		//
		// default used ctor #2 (draw type, width and height defined)
		// -WindowDrawType: whether this draws in 2D or 3D
		// -UINT: width of the window, anything outside is clipped
		// -UINT: height of the window, anything outside is clipped
		//
		Window( _In_ WindowDrawType, _In_ UInt, _In_ UInt );

		//
		// Copy Ctor
		//
		Window( _In_ const Window& );

		//
		// move ctor
		//
		Window( _In_ const Window&& );

		//
		// Dtor
		//
		~Window( );

		//
		// release all drawables
		//
		void Release( );

		//
		// overriden children logger retrieval
		//
		virtual void GetChildrenLoggers( _In_ HyJynxCollections::DynamicCollection< ILog* >* const ) override;

#pragma region API

		//
		// Add an on-screen object to be rendered.
		// IDrawable*: object to be considered for on-screen rendering
		//
		void AddDrawable( _In_ IDrawable* );

		//
		// Remove a registered drawable object using the reference of the original drawable to added
		// IDrawable* pointer to the drawable object your looking to remove
		//
		void RemoveDrawable( _In_ IDrawable* );

		//
		// Add a container of drawables
		// - DrawableContainer*: group of drawables
		//
		void AddContainer( _In_ DrawableContainer* const );

		//
		// Remove a container of drawables
		// DrawableContainer*: group of drawables to remove
		//
		void RemoveContainer( _In_ DrawableContainer* const );

		//
		// get a pointer to a drawable with the given ID
		// - int: ID of a drawable in question
		// - returns IDrawable*: pointer to the added drawable
		//
		IDrawable* GetDrawableById( _In_ const int ) const;

		//
		// main update call - in turn will update all registered drawables
		// - Context*: dx11 API container
		// -RenderTime: high-precision-timer used for renderer's cycle time
		//
		void Update( _In_ Context*, _In_ const RenderTime );

		//
		// cull everything inside of this window. All drawables will know 
		// can manage themselves for better flexibility.
		// - ViewFrustum* const: what drawables collision test again
		// - CollisionMethod const: the maximum collision method to test against
		// - DynamicCollection< IDrawable* >*: the ongoing list of valid drawable to render
		//
		void FrustumCull( _In_ ViewFrustum&, _In_ const CollisionMethod, _In_ BatchContainer& );

#pragma endregion

#pragma region Accessors

		//
		// determine if this window is z-locked or not
		// if a window is z-locked, its Z-location cannot be changed, nor anything else be on
		// the same Z-location
		// (think of the 3D game window, if its clicked, it won't be placed on-top, because UI is on top duh)
		//
		bool IsZLocked( ) const;

		//
		// set whether this window is z-locked or not
		// bool: true - z index won't change, false - z index can change
		//
		void SetZLock( _In_ const bool );

		//
		// get a reference to the transform used for 3D space manipulation
		// return Transform*: reference to the transform used for window placement etc...
		//
		Transform* GetTransform( );

		//
		// determine if this window is currently active or not
		// return bool: true - the window will draw, false - the window will be skipped
		//
		bool GetActive( ) const;

		//
		// set this window to an active/inactive state
		// -bool: true - the window will draw, false - the window will be skipped
		//
		void SetActive( _In_ const bool );

		//
		// get the width of this window
		// return Uint: width in pixels of the window
		//
		UInt GetWidth( ) const;

		//
		// get the height of this window
		// return Uint: height in pixels of the window
		//
		UInt GetHeight( ) const;

		//
		// set the width in pixels of this window
		// -UINT: width in pixels
		//
		void SetWidth( _In_ const UInt );

		//
		// set the height in pixels of this window
		// -UINT: height in pixels
		//
		void SetHeight( _In_ const UInt );

		//
		// determine if this window wants to draw in 2D or 3D
		// -return: WindowDrawType
		//
		WindowDrawType GetDrawingType( ) const;

		//
		// get the name used for logging amongst other things
		// -return: Text
		//
		HyJynxCore::Text GetName( ) const;

#pragma endregion

#pragma region Operators

		//
		// add a drawable to the window, if its not nullptr
		// - IDrawable*: pointer to the drawable you wish to add
		//
		Window& operator += ( _In_ IDrawable* );

		//
		// add a container to the window, if its not nullptr
		// - IDrawable*: pointer to the drawable you wish to add
		//
		Window& operator += ( _In_ DrawableContainer* );

		//
		// remove a drawable from the window, if present and not null
		// - IDrawable*: pointer to the drawable you wish to remove
		//
		Window& operator -= ( _In_ IDrawable* );

		//
		// add a container to the window, if its not nullptr
		// - IDrawable*: pointer to the drawable you wish to add
		//
		Window& operator -= ( _In_ DrawableContainer* );

#pragma endregion

	};

}

#endif // HYJYNXRENDERER_WINDOW_H