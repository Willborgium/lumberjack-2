/**
 * IndexBuffer.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_INDEXBUFFER_H
#define HYJYNXRENDERER_INDEXBUFFER_H

#include <sal.h>

#pragma warning( disable: 4005 )
#include <D3D11.h>
#pragma warning( default: 4005 )

namespace HyJynxRenderer
{
	//
	// The index buffer contains a list of indicies into a vertex buffer, 
	// used for vertex drawing order (programmatically created, or loaded from file)
	//
	class IndexBuffer sealed
	{
	protected:

		ID3D11Buffer*			_buffer = nullptr;
		UInt					_count = 0;

	public:

		//
		// null ctor
		//
		IndexBuffer( );

		//
		// defined ctor
		//
		IndexBuffer( _In_ ID3D11Buffer*, _In_ const UInt );

		//
		// copy ctor
		//
		IndexBuffer( _In_ const IndexBuffer& );

		//
		// move ctor
		//
		IndexBuffer( _In_ const IndexBuffer&& );

		//
		// your good ol' dtor
		//
		~IndexBuffer( );

		//
		// Get the ID3D11Buffer
		// - returns ID3D11Buffer*: dx11 gpu buffer container
		//
		ID3D11Buffer* GetBuffer( );

		//
		// get the amount of indicies the buffer contains
		// - returns UInt: number of indicies contained
		//
		UInt GetCount( ) const;
	};
}

#endif // HYJYNXRENDERER_INDEXBUFFER_H