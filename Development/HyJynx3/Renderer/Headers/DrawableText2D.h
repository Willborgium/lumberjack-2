/**
 * DrawableText2D.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_DRAWABLETEXT2D_H
#define HYJYNXRENDERER_DRAWABLETEXT2D_H

#include <Renderer\Headers\Drawable.h>
#include <Engine\Headers\Text.h>
#include <Engine\Headers\Function.h>
#include <sal.h>

namespace HyJynxRenderer
{
	//
	// implements drawable text which draws onto the 2D layer
	//
	class DrawableText2D : public Drawable
	{
	protected:

	public:

		//
		// null ctor
		//
		DrawableText2D( );

		//
		// copy ctor
		//
		DrawableText2D( _In_ const DrawableText2D& );

		//
		// move ctor
		//
		DrawableText2D( _In_ const DrawableText2D&& );

		//
		// dtor
		//
		virtual ~DrawableText2D( );

		//
		// drawables are constructed with the data they need to load,
		// this kicks off the sequence to load and and register the mesh buffers
		// - function< void(void) >: lambda to call when loading has completed
		//					(this is here for async loading down the road)
		//
		virtual void LoadModel( _In_ HyJynxCore::Function< void > ) override;


		//
		// this should be called after a drawable is constructed, and LoadModel() called
		//
		virtual void GenerateBounding( ) override;

		//
		// per-cycle update call
		// Context*: dx API container
		// const RenderTime*: reference to Renderer's per cycle run timer
		//
		virtual void Update( _In_ Context*, _In_ const RenderTime ) override;

		//
		// when we're in development, this API will help us draw debugging information to the screen
		// -Context*: reference to the context we're drawing to
		//
		virtual void RenderDebug( _In_ Context* ) override;


	};
}

#endif // HYJYNXRENDERER_DRAWABLETEXT2D_H