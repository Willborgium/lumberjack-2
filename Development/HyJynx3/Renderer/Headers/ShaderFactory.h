/**
 * ShaderLibrary.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_SHADERFACTORY_H
#define HYJYNXRENDERER_SHADERFACTORY_H

#include <Renderer/Headers/Shader.h>
#include <Renderer/Headers/LinkageController.h>
#include <Engine/Headers/Singleton.h>
#include <Engine/Headers/Dictionary.h>
#include <Engine/Headers/Text.h>
#include <Engine/Headers/Function.h>
#include <Logger/Headers/ILog.h>
#include <sal.h>

#pragma warning(disable: 4005)
#include <D3D11.h>
#include <D3Dcompiler.h>
#pragma warning(default: 4005)

namespace HyJynxRenderer
{

	typedef HyJynxCore::Function< void, ID3D11ShaderReflection* const, LinkageController* const > LinkageCallback;

	//
	// describes the different stages of the shader process
	//
	enum class ShaderStage : short
	{
		SHADER_STAGE_UNKNOWN = -1,
		SHADER_STAGE_VERTEX = 0,
		SHADER_STAGE_PIXEL = 1,
		SHADER_STAGE_HULL = 2,
		SHADER_STAGE_DOMAIN = 3,
		SHADER_STAGE_GEOMETRY = 4
	};

	//
	// This wrapper is for a single Vertex Description, these containers are found in the
	// pre-declared static VertexElementDescription within ShaderFactory
	//
	class VertexDescription sealed
	{
	public:

		HyJynxCore::Text			Name = "";
		D3D11_INPUT_ELEMENT_DESC*	Description = nullptr;
		UInt						Size = 0;
		UInt						ElementCount = 0;

		// null ctor
		VertexDescription( );

		// defined ctor
		VertexDescription( _In_ HyJynxCore::Text, D3D11_INPUT_ELEMENT_DESC*, _In_ UInt, _In_ UInt );

		// copy ctor
		VertexDescription( _In_ const VertexDescription& );

		// move ctor
		VertexDescription( _In_ const VertexDescription&& );

		// assignment
		VertexDescription& operator = ( _In_ const VertexDescription& );
	};

	//
	// A single created Vertex Declaration, containing the description it was created
	// with, and the Dx11 input layout pointer
	//
	class VertexDeclaration sealed
	{
	public:

		VertexDescription			Description;
		ID3D11InputLayout*			Layout = nullptr;

		// nullptr
		VertexDeclaration( );

		// defined ctor
		VertexDeclaration( _In_ VertexDescription, _In_ ID3D11InputLayout* );

		// copy ctor
		VertexDeclaration( _In_ const VertexDeclaration& );

		// move ctor
		VertexDeclaration( _In_ const VertexDeclaration&& );

		// assignment
		VertexDeclaration& operator = ( _In_ const VertexDeclaration& );
	};

	//
	// After a shader is successfully loaded, it is wrapped and stored in here.
	// This is so we can automate the creation of InputLayouts, and manage them behind the scenes.
	// (note: This needs to be observed and revised)
	//
	class LoadedShader sealed
	{
	public:

		HyJynxCore::Text	ID = "";
		ID3D11DeviceChild*	ShaderChild = nullptr;

		VertexDeclaration*	VDeclaration = nullptr;
		LinkageController*	Linkage = nullptr;


		//
		// null ctor - never used
		//
		LoadedShader( );

		//
		// basic ctor - for shaders which have no additional members
		//
		LoadedShader( _In_ HyJynxCore::Text, _In_ ID3D11DeviceChild* );

		//
		// vertex shader ctor - extra vertex declaration member
		// - Text: file path to the shader
		// - ID3D11DeviceChild*: the dx11 parent shader object
		// - VertexDeclaration: the verticies the shader expects
		//
		LoadedShader( _In_ HyJynxCore::Text, _In_ ID3D11DeviceChild*, _In_ VertexDeclaration* );

		//
		// pixel shader ctor, with dynamic shader linkage if present
		// - Text: file path to the shader
		// - ID3D11DeviceChild*: the dx11 parent shader object
		// - LinkageController*: the linkage controller used for this pixel shader
		//
		LoadedShader( _In_ HyJynxCore::Text, _In_ ID3D11DeviceChild*, _In_ LinkageController* );

		//
		// copy ctor
		//
		LoadedShader( _In_ const LoadedShader& );

		//
		// move ctor
		//
		LoadedShader( _In_ const LoadedShader&& );

		//
		// assignment
		//
		LoadedShader& operator = ( _In_ const LoadedShader& );

		//
		// dtor
		//
		~LoadedShader( );
	};

	//
	// wrapper for all parameters needed to dynamically create a shader
	//
	struct ShaderInit sealed
	{
		// depicts what type of verticies the vertex shader will be expecting
		VertexDescription ExpectedVertexDescription;

		// filepath to the pre-compiled vertex shader
		HyJynxCore::Text VertexPath = "";

		// filepath to the pre-compiled pixel shader
		HyJynxCore::Text PixelPath = "";

		// filepath to the pre-compiled hull shader
		HyJynxCore::Text HullPath = "";

		// filepath to the pre-compiled domain shader
		HyJynxCore::Text DomainPath = "";

		// filepath to the pre-compiled geometry shader
		HyJynxCore::Text GeometryPath = "";

		// callback made when dynamic shader linkage is found, this is so custom shaders/materials
		// can specify the variable names and build the LinkageController the material will consume
		LinkageCallback OnLinkageFound;
	};

	typedef HyJynxCollections::Dictionary< HyJynxCollections::KeyValuePair< ShaderStage, HyJynxCore::Text >, LoadedShader > ShaderDictionary;

	//
	// ShaderFactory is the manager dealing with loading and releasing shaders. Many
	// materials can reference the same loaded shader.
	//
	class ShaderFactory sealed : public HyJynxDesign::Singleton< ShaderFactory >, public HyJynxLogger::ILog
	{
		friend class HyJynxDesign::Singleton< ShaderFactory >;

	private:

		ID3D11ClassLinkage*		_classLinkage = nullptr;

		ID3D11Buffer*			_projectionBuffer = nullptr;
		ID3D11Buffer*			_cameraBuffer = nullptr;
		ID3D11Buffer*			_cycleBuffer = nullptr;
		ID3D11Buffer*			_objectBuffer = nullptr;

		ShaderDictionary		_shaderList;

		//
		// default ctor
		//
		ShaderFactory( );

		// unimplemented
		ShaderFactory( _In_ const ShaderFactory& ) = delete;
		ShaderFactory( _In_ const ShaderFactory&& ) = delete;
		ShaderFactory& operator = ( _In_ const ShaderFactory& ) = delete;

		//
		// initializes the pointers to our shader constant buffers
		//
		bool initializeConstants( );

		//
		// loads or retrieves the given vertex shader filepath
		// - Text: filepath to the shader, eg: "Shaders\VS_Color.cso"
		// - returns ID3D11VertexShader*: dx vertex shader interface
		//
		void BuildVertexShader( _In_ HyJynxCore::Text, _In_ VertexDescription&, _Out_ Shader* const );

		//
		// loads or retrieves the given pixel shader filepath
		// - Text: filepath to the shader, eg: "Shaders\PS_Color.cso"
		// - returns ID3D11PixelShader*: dx pixel shader interface
		//
		void BuildPixelShader( _In_ HyJynxCore::Text, _In_ LinkageCallback, _Out_ Shader* const );

		//
		// loads or retrieves the given hull shader filepath
		// - Text: filepath to the shader, eg: "Shaders\HS_Color.cso"
		// - returns ID3D11HullShader*: dx hull shader interface
		//
		void BuildHullShader( _In_ HyJynxCore::Text, _Out_ Shader* const );

		//
		// loads or retrieves the given domain shader filepath
		// - Text: filepath to the shader, eg: "Shaders\DS_Color.cso"
		// - returns ID3D11DomainShader*: dx domain shader interface
		//
		void BuildDomainShader( _In_ HyJynxCore::Text, _Out_ Shader* const );

		//
		// loads or retrieves the given geometry shader filepath
		// - Text: filepath to the shader, eg: "Shaders\GS_Color.cso"
		// - returns ID3D11GeometryShader*: dx geometry shader interface
		//
		void BuildGeometryShader( _In_ HyJynxCore::Text, _Out_ Shader* const );

		//
		// get a buffer by the gpu bound constant buffer index
		// - UInt: index of the gpu constant buffer
		// - ID3D11Buffer*: Constant Buffer bound to the given index
		//
		ID3D11Buffer* GetBufferByConstantIndex( _In_ const UInt );

		//
		// Get or Create a Vertex Layout, for your convienence you can find some pre-declared descriptions
		// statically in this class.
		// - VertexDescription: description with all info needed to create a layout
		// - void*: shader byte code
		// - UInt: size in bytes of the shader code
		// - ID3D11InputLayout*: output created input layout, or nullptr for failure
		//
		VertexDeclaration* CreateVertexDeclaration( _In_ VertexDescription&, _In_ void*, _In_ UInt );

		//
		// get a shader if it was already loaded, or nullptr if it hasn't been loaded
		// - ShaderStage: the type of shader in question
		// - Text: the filepath to the shader in question
		// - ID3D11DeviceChild*: dx11 shader parent, or nullptr
		//
		LoadedShader* const GetLoadedShader( _In_ const ShaderStage, _In_ const HyJynxCore::Text& ) const;

	public:

		//
		// provides pre-declared vertex element descriptions - but you can always create your own 
		//
		static class VertexElementDescription abstract sealed
		{
		public:

			static const VertexDescription Position;
			static const VertexDescription PositionColor;
			static const VertexDescription PositionColorTexture;
			static const VertexDescription PositionNormalColor;
			static const VertexDescription PositionTexture;
			static const VertexDescription PositionNormalTexture;
			static const VertexDescription PositionNormalBinormalTangentTexture;
		};

		//
		// A Horse is a Horse, of course of course
		//
		~ShaderFactory( );

		//
		// releases all loaded shaders. use with caution.
		//
		void ReleaseAll( );

		//
		// initializes the dynamic shader linkage interface, and the shader which 
		// will act as constant buffer update entry point for automation
		// - returns bool: true for success
		//
		bool Initialize( );

		//
		// Creates a new shader, vertex and pixel shader is required,
		// hull, domain, and geometry shaders are optional
		//
		Shader* CreateShader( _In_ ShaderInit& );
		
		//
		// retrieve the buffer pointer of the Camera Projection constant
		// - ID3D11Buffer*: constant buffer
		//
		ID3D11Buffer* GetProjectionConstant( );

		//
		// retrieve the buffer pointer of the Camera constant
		// - ID3D11Buffer*: constant buffer
		//
		ID3D11Buffer* GetCameraConstant( );

		//
		// retrieve the buffer pointer of the Camera cycle constant
		// - ID3D11Buffer*: constant buffer
		//
		ID3D11Buffer* GetCycleConstant( );

		//
		// retrieve the buffer pointer of the Camera object constant
		// - ID3D11Buffer*: constant buffer
		//
		ID3D11Buffer* GetObjectConstant( );

		//
		// get a class instance through the shader linkage object
		// - Text: name of the class you wish to create
		// - returns ID3D11ClassInstance*: dx11 shader class object
		//
		ID3D11ClassInstance* GetClassInstance( _In_ const HyJynxCore::Text );
	};
}

#endif // HYJYNXRENDERER_SHADERFACTORY_H