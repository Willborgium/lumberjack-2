/**
 * DrawableQuad2D.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_DRAWABLEQUAD2D_H
#define HYJYNXRENDERER_DRAWABLEQUAD2D_H

#include <Renderer/Headers/Drawable.h>
#include <Renderer/Headers/Primitive.h>
#include <Renderer/Headers/PrimitiveConfiguration.h>
#include <Renderer/Headers/ViewFrustum.h>
#include <Renderer/Headers/Color.h>
#include <sal.h>

namespace HyJynxRenderer
{
	//
	// provides all modifiable values quad2D will use
	//
	class DrawableQuad2DConfiguration : public PrimitiveConfiguration
	{
	protected:

		float _width = 512.0f;
		float _height = 256.0f;
		Color _topLeft = Color( 255.0f );
		Color _topRight = Color( 255.0f );
		Color _botLeft = Color( 255.0f );
		Color _botRight = Color( 255.0f );

	public:

		// null ctor
		DrawableQuad2DConfiguration( );

		// copy ctor
		DrawableQuad2DConfiguration( _In_ const DrawableQuad2DConfiguration& );

		// move ctor
		DrawableQuad2DConfiguration( _In_ const DrawableQuad2DConfiguration&& );

		// defined ctor
		DrawableQuad2DConfiguration( _In_ float, _In_ float );

		// get width
		const float GetWidth( ) const;

		// get height
		const float GetHeight( ) const;

		// set width
		void SetWidth( _In_ const float );

		// set height
		void SetHeight( _In_ const float );

		// set width & height
		void SetDimensions( _In_ const float, _In_ const float );

		// get a container with both width and height
		Vector2 GetDimensions( ) const;

		// set all vertex colors
		void SetVertexColors( _In_ const Color&, _In_ const Color&, _In_ const Color&, _In_ const Color& );

		// get vertex top-left color
		Color& GetTopLeftColor( );
		void SetTopLeftColor( _In_ const Color& );

		// get vertex top-right color
		Color& GetTopRightColor( );
		void SetTopRightColor( _In_ const Color& );

		// get bottom-left color
		Color& GetBottomLeftColor( );
		void SetBottomLeftColor( _In_ const Color& );
		
		// get bottom-right color
		Color& GetBottomRightColor( );
		void SetBottomRightColor( _In_ const Color& );
	};

	//
	// This drawing object represents a unprojected quad.
	// Primarily used to draw items into 2D
	//
	class DrawableQuad2D : public Drawable, public Primitive
	{
	protected:

		Transform	_transform;

		void MatrixCalculationDefaultOverride( _In_ Transform& transform, _Out_ Matrix* matrix );

		//
		// initialize the matrix calculation for 2D rendering
		//
		void initializeMatrixOverride( );

		//
		// called to create the vertex/index/model objects
		//
		virtual void GeneratePrimitiveModel( ) override;

		//
		// called to update resource buffers with current configuration settings
		//
		void updateResourceData( _In_ Context* );

	public:

		//
		// null ctor
		//
		DrawableQuad2D( );

		//
		// configured ctor
		//
		DrawableQuad2D( _In_ DrawableQuad2DConfiguration* );

		//
		// copy ctor
		//
		DrawableQuad2D( _In_ const DrawableQuad2D& );

		//
		// move ctor
		//
		DrawableQuad2D( _In_ const DrawableQuad2D&& );

		//
		// dtor
		//
		virtual ~DrawableQuad2D( );

		//
		// overriden call to load model with current values
		// - Function< void, void >: callback to execute when loading has completed
		//
		virtual void LoadModel( _In_ HyJynxCore::Function< void > ) override;

		//
		// overriden call to generate the collision description
		//
		virtual void GenerateBounding( ) override;

		//
		// per-cycle and per-drawable rendering logic
		// - Context*: dx11 API container
		// -const RenderTime: high-precision-timer
		//
		virtual void Update( _In_ Context*, _In_ const RenderTime ) override;

		//
		// render any debugging visuals to the screen
		// -Context*: pointer to renderer's context API container
		//
		void RenderDebug( _In_ Context* ) override;

		//
		// override what transform to return
		// -Transform*: returns the actual transform to interact with
		//
		Transform* GetTransform( ) override;

		//
		// get the un-transformed width in pixels
		// -returns: float width
		//
		float GetWidth( );

		//
		// get the un-transformed height in pixels
		// returns: float height
		//
		float GetHeight( );

		//
		// set the un-transformed width in pixels, where possible, prefer
		// tranformation scale and not width change, as this will trigger a gpu resource update
		// - UINT: width
		//
		void SetWidth( _In_ const float );

		//
		// set the un-transformed height in pixels, where possible prefer transformation
		// scale and not height change, as this will trigger a gpu resource update
		// - UINT: height
		//
		void SetHeight( _In_ const float );
	};
}

#endif // HYJYNXRENDERER_DRAWABLEQUAD2D_H