/**
* Point2.h
* (c) 2014 All Rights Reserved
*/

#ifndef HYJYNXRENDERER_POINT2_H
#define HYJYNXRENDERER_POINT2_H

#include <sal.h>

namespace HyJynxRenderer
{

	//
	// wraps xyz integral values, handy for dimensional indicies
	//
	class Point2
	{
	public:

		int X = 0,
			Y = 0;

#pragma region init

		//
		// null ctor
		//
		Point2( );

		//
		// initialize all components to the argued value
		//
		Point2( _In_ int );

		//
		// initialize all components seperately
		//
		Point2( _In_ int, _In_ int );

		//
		// copy ctor
		//
		Point2( _In_ const Point2& );

		//
		// move ctor
		//
		Point2( _In_ const Point2&& );

		//
		// dtor
		//
		virtual ~Point2( );

#pragma endregion

#pragma region API



#pragma endregion

#pragma region Operators

		bool operator == ( _In_ const Point2& other ) const;
		bool operator != ( _In_ const Point2& other ) const;

		Point2& operator += ( _In_ const Point2& other );
		Point2& operator -= ( _In_ const Point2& other );
		Point2& operator *= ( _In_ const Point2& other );
		Point2& operator /= ( _In_ const Point2& other );

		Point2 operator + ( _In_ const Point2& other ) const;
		Point2 operator - ( _In_ const Point2& other ) const;
		Point2 operator * ( _In_ const Point2& other ) const;
		Point2 operator / ( _In_ const Point2& other ) const;

#pragma endregion
	};
};

#endif // HYJYNXRENDERER_POINT2_H 