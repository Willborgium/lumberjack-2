/**
 * Material3D.hs
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJENXRENDERER_MATERIAL3D_H
#define HYJENXRENDERER_MATERIAL3D_H

#include <Renderer/Headers/Material.h>
#include <sal.h>

namespace HyJynxRenderer
{
	class IDrawable;
	class Mesh;

	//
	// Controls the shader linkage options for drawables rendering into the 3D layer
	//
	class Material3D : public Material
	{
	protected:

		

	public:

		//
		// null ctor
		//
		Material3D( );

		//
		// copy ctor
		//
		Material3D( _In_ const Material3D& );

		//
		// move ctor
		//
		Material3D( _In_ const Material3D&& );

		//
		// dtor
		//
		virtual ~Material3D( );

		//
		// update and set buffers to the shader which all following objects will use
		// - Context*: dx11 command API container
		//
		virtual void BatchBind( _In_ Context* const ) override;

		//
		// bind the argued drawable containers to the argued constant buffer
		// - IDrawable*: pointer to the drawable in need of binding
		// - Mesh*: pointer to the current mesh to be rendered 
		// - CBObject*: per-object constant buffer who's data needs to be updated
		//
		virtual void ObjectBind( _In_ IDrawable* const, _In_ Mesh* const, _In_ CBObject* const ) override;

	};
}

#endif // HYJENXRENDERER_MATERIAL3D_H