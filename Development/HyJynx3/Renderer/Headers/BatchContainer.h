/**
 * BatchContainer.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_BATCHCONTAINER_H	
#define HYJYNXRENDERER_BATCHCONTAINER_H

#include <Renderer\Headers\IDrawable.h>
#include <Renderer\Headers\Mesh.h>
#include <Renderer\Headers\WindowDrawType.h>
#include <Renderer\Headers\Context.h>
#include <Engine\Headers\Dictionary.h>
#include <Engine\Headers\DynamicCollection.h>
#include <Engine\Headers\TypeDefinitions.h>
#include <sal.h>
#include <DirectXMath.h>

namespace HyJynxRenderer
{
	//
	// this wrapper contains a mesh which needs to be drawn, and the drawable
	// which it belongs to. This is needed because each Mesh of a drawable can have
	// its own material, but its transormation comes from the Drawable who owns it.
	//
	struct BatchItem sealed
	{
		IDrawable* Drawable = nullptr;
		Mesh* DrawableMesh = nullptr;

		//
		// null ctor
		//
		BatchItem( );

		//
		// default used ctor
		// - IDrawable*: pointer to the parent drawable
		// - Mesh*: pointer to the mesh to be drawn
		//
		BatchItem( _In_ IDrawable* drawable, _In_ Mesh* mesh );

		//
		// dtor
		//
		~BatchItem( );

		//
		// comparison operator
		//
		bool operator == ( _In_ const BatchItem& other ) const;
	};

	//
	// container for a material to render, and all associated meshes which use it
	//
	struct MaterialBatch sealed
	{
		int MaterialID = ~0;
		HyJynxCollections::DynamicCollection< BatchItem* > MeshList;

		//
		// null ctor
		//
		MaterialBatch( );

		//
		// default used ctor
		// - id: ID of the material associated with this Batch
		//
		MaterialBatch( _In_ int id );

		//
		// comparison operator needed for dynamicCollection
		//
		bool operator == ( _In_ const MaterialBatch& other ) const;

		//
		// safely retrieve the material representing this batch
		// - returns Material*: pointer to the material batch of meshes use
		//
		Material* const GetMaterial( );
	};

	//
	// This stack object (used inside of Renderer.Render()) is a helper container
	// and exposes API for Render Sequences to utilize. In here we keep track of
	// what shaders need to draw for the current render cycle, and their respective
	// drawables. As Drawables are culled and checked, active and visible drawables will
	// be added in here for Sequence to perform custom logic, before actually drawing 
	// them to the back-buffer.
	//
	class BatchContainer sealed
	{
	private:

		HyJynxCollections::DynamicCollection< MaterialBatch* > _2DLayer;
		HyJynxCollections::DynamicCollection< MaterialBatch* > _3DLayer;

		//
		// privately called to add a mesh to a 2D material batch, or create one if it doesn't exist yet
		// - Mesh*: pointer to the mesh being added
		//
		void AddMesh2D( _In_ Mesh*, _In_ IDrawable* );

		//
		// privately called to add a mesh to a 3D material batch, or create one if it doesn't exist yet
		// - Mesh*: pointer to the mesh being added
		//
		void AddMesh3D( _In_ Mesh*, _In_ IDrawable* );

	public:

		//
		// null ctor
		//
		BatchContainer( );

		//
		// copy ctor
		//
		BatchContainer( _In_ const BatchContainer& );

		//
		// move ctor
		//
		BatchContainer( _In_ const BatchContainer&& );
		
		//
		// dtor
		//
		~BatchContainer( );

		//
		// comparison operator
		//
		bool operator == ( _In_ const BatchContainer& ) const;

		//
		// Register a drawable for rendering
		// - WindowDrawType: the registering window's draw type, 2D or 3D
		// - IDrawable*: pointer to the drawable itself
		//
		void RegisterDrawable( _In_ WindowDrawType, _In_ IDrawable*, _In_ const DirectX::XMFLOAT3& );

		//
		// get a pointer to the MaterialBatch list holding all 2D rendered items,
		// this is sequence's chance to perform custom ordering and manipulations
		// - DynamicCollection< MaterialBatch* >*: all 2D meshes to render
		//
		HyJynxCollections::DynamicCollection< MaterialBatch* >* Get2DLayer( );

		//
		// get a pointer to the MaterialBatch list holding all 3D rendered items
		// this is sequence's chance to perform custom ordering and manipulations
		// - DynamicCollection< MaterialBatch* >*: all 3D meshes to render
		//
		HyJynxCollections::DynamicCollection< MaterialBatch* >* Get3DLayer( );

		//
		// Render the 2D Layer batches as it stands to the context
		// - Context: dx11 context commands
		//
		void Render2DLayer( _In_ Context* const );

		//
		// Render the 3D Layer batches as it stands to the context
		// - Context: dx11 context commands
		//
		void Render3DLayer( _In_ Context* const );
	};
}

#endif // HYJYNXRENDERER_BATCHCONTAINER_H