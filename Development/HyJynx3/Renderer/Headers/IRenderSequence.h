/**
 * IRenderSequence.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_IRENDERSEQUENCE_H
#define HYJYNXRENDERER_IRENDERSEQUENCE_H

#include <Renderer/Headers/Context.h>
#include <Renderer/Headers/Graphics.h>
#include <Renderer/Headers/BatchContainer.h>
#include <Logger/Headers/ILog.h>
#include <Engine/Headers/Dictionary.h>
#include <Engine/Headers/Text.h>

#pragma warning(disable: 4005)
#include <D3D11.h>
#pragma warning(default: 4005)

namespace HyJynxRenderer
{
	//
	// This interface is what Renderer will use to kick off a render sequence.
	// I'm using strategy pattern here so the main render process can mitigate
	// the logic, and swap on-the-fly how rendering occurrs
	// eg: {diffuse}, {cartoon}, {normals}, {wireframe}, {depth buffer}, etc...
	//
	class IRenderSequence abstract : public HyJynxLogger::ILog
	{
	protected:

		HyJynxCore::Text _name;

	public:

		//
		// null ctor
		//
		IRenderSequence( );

		//
		// ctor with name defined
		// -Text name of the Sequence
		//
		IRenderSequence( _In_ HyJynxCore::Text );

		//
		// ensure the render targets we need are created and ready
		//
		virtual bool ValidateTargets( _In_ HyJynxCollections::Dictionary<HyJynxCore::Text, ID3D11RenderTargetView*>* const,
			_In_ HyJynxCollections::Dictionary<HyJynxCore::Text, DepthStencil*>* const ) = 0;

		//
		// if we couldn't validate our render targets, initialize them
		// -Graphics*: provides the API to create render targets
		//
		virtual bool InitializeTargets( _In_ Graphics*, _In_ HyJynxCollections::Dictionary<HyJynxCore::Text, ID3D11RenderTargetView*>*,
			_In_ HyJynxCollections::Dictionary<HyJynxCore::Text, DepthStencil*>* ) = 0;

		//
		// clear the targets we're using to a blank slate
		// -Context*: provides the API to clear a render target
		//
		virtual bool ClearTargets( _In_ Context*,
			_In_ HyJynxCollections::Dictionary<HyJynxCore::Text, ID3D11RenderTargetView*>*,
			_In_ HyJynxCollections::Dictionary<HyJynxCore::Text, DepthStencil*>*,
			_In_opt_ Color* ) = 0;

		//
		// using the filled BatchContainer, perform any custom operations and draw
		// - Context*: API container drawables render to
		// - Dictionary<Text, ID3D11RenerTargetView*>*: intialized render targets
		// - Dictionary<Text, DepthStencil*>*: initialized depth stencils
		// - BatchContainer*: the filled container of everything that will draw this cycle
		//
		virtual bool Render( _In_ Context*, 
			_In_ HyJynxCollections::Dictionary<HyJynxCore::Text, ID3D11RenderTargetView*>*,
			_In_ HyJynxCollections::Dictionary<HyJynxCore::Text, DepthStencil*>*,
			_In_ BatchContainer* ) = 0;

		//
		// Get the name of this render sequence
		//
		virtual HyJynxCore::Text GetName( ) const;
	};
}

#endif // HYJYNXRENDERER_IRENDERSEQUENCE_H