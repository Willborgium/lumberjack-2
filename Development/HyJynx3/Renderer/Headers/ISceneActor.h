/**
 * ISceneActor.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_ISCENEACTOR_H
#define HYJYNXRENDERER_ISCENEACTOR_H

#include <Engine\Headers\Text.h>
//#include <Engine\Headers\JsonParser.h> // TODO: BREAK?
#include <fbxsdk.h>
#include <sal.h>

namespace HyJynxRenderer
{

	//
	// All actors of a scene will implement this, so all file operations are uniform
	// across any scene file types we choose to implement
	//
	class ISceneActor abstract
	{
	protected:

		HyJynxCore::Text _id = "";

	public:

		//
		// null ctor
		//
		ISceneActor( );

		//
		// copy ctor
		//
		ISceneActor( _In_ const ISceneActor& );

		//
		// move ctor
		//
		ISceneActor( _In_ const ISceneActor&& );

		//
		// dtor
		//
		virtual ~ISceneActor( );

		// ===============================================

		//
		// get the ID of the actor
		// - return int: ID of this actor
		//
		const HyJynxCore::Text GetActorID( ) const;

		//
		// set the ID of the actor
		// - int: ID of the actor
		//
		void SetActorID( _In_ const HyJynxCore::Text );

		//
		// get the node used for FBX serialization
		// - return FbxNode*: FBX sdk node object
		//
		virtual FbxNode* GetFbxNode( ) = 0;

		//
		// create this object from a de-serialized fbx node
		// - FbxNode*: FBX node from file
		//
		virtual void FromFbxNode( _In_ const FbxNode* const ) = 0;

		//
		// get the node used for JSON serialization
		// - return Text: Text representing a JSON object
		//
		//virtual HyJynxCore::JsonObject GetJsonNode( ) = 0;

		//
		// create this object from a de-serialized JSON node
		// - Text: Text representing a JSON object
		//
		//virtual void FromJsonNode( _In_ const HyJynxCore::JsonObject ) = 0;

		//
		// get the node used for XML serialization
		// - return Text: Text representing a XML object
		//
		virtual HyJynxCore::Text GetXmlNode( ) = 0;

		//
		// create this object from a de-serialized XML node
		// - Text: Text representing an XML node
		//
		virtual void FromXmlNode( _In_ const HyJynxCore::Text ) = 0;
	};
};

#endif // HYJYNXRENDERER_ISCENEACTOR_H 