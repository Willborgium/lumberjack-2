/**
 * Renderer.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_RENDERER_H
#define HYJYNXRENDERER_RENDERER_H

#include <Renderer/Headers/Screen.h>
#include <Renderer/Headers/Window.h>
#include <Renderer/Headers/Graphics.h>
#include <Renderer/Headers/Color.h>
#include <Renderer/Headers/IRenderSequence.h>
#include <Renderer/Headers/RenderReport.h>
#include <Renderer/Headers/DepthStencil.h>
#include <Renderer/Headers/RendererConfiguration.h>
#include <Engine/Headers/DynamicCollection.h>
#include <Engine/Headers/Dictionary.h>
#include <Engine/Headers/Text.h>
#include <Engine/Headers/HighPrecisionTimer.h>
#include <Engine/Headers/Singleton.h>
#include <Logger/Headers/ILog.h>

#include <sal.h>

namespace HyJynxRenderer
{
	typedef HyJynxCollections::DynamicCollection<Window*> WindowList;
	typedef HyJynxCollections::Dictionary<HyJynxCore::Text, ID3D11RenderTargetView*> RTDictionary;
	typedef HyJynxCollections::Dictionary<HyJynxCore::Text, DepthStencil*> DSDictionary;
	typedef HyJynxCollections::Dictionary<HyJynxCore::Text, ID3D11RasterizerState*> RasterizerStateList;

	//
	// This is the Meca of drawing 0's and 1's into beautiful 3D images. Neat.
	// Renderer will utilize all of our drawing objects, and automate some topics such as:
	//		- LOD transitions in models with multiple buffer sets
	//		- Ordering of transparent objects
	//		- toggling debugging rendering modes such as wire-frame
	//		- occluding objects off of the active camera's frustum
	//		- ordering the window-draw order based on it's z-index
	//		- giving out lollipops to friendly orcs
	//		- material batching, minimizing render state changes
	//		- drawing 2D first into depth buffer, then 3D
	//		- auto performance adjustments
	//
	class Renderer sealed : public HyJynxDesign::Singleton<Renderer>, public HyJynxLogger::ILog
	{
		friend class HyJynxDesign::Singleton<Renderer>;

	private:

		// components
		Graphics						_graphics;
		Screen							_screen = Screen( );
		WindowList						_windowList = WindowList( );
		RTDictionary					_renderTargets = RTDictionary( );
		DSDictionary					_depthTargets = DSDictionary( );
		RenderReport					_renderReport = RenderReport( );
		Color							_clearColor = Color( 0.0f, 0.0f, 0.0f );
		HyJynxCore::HighPrecisionTimer	_runTimer = HyJynxCore::HighPrecisionTimer( );
		RasterizerStateList				_rasterizerStates = RasterizerStateList( );

		// custom run-time
		RendererConfiguration			_config = RendererConfiguration( );
		Camera*							_activeCamera = nullptr;
		IRenderSequence*				_renderSequence = nullptr;

		//
		// this is called before every render sequence to validate if we're ready to render or not.
		// If not, we'll try and recover by releasing DX components and re-initializing them
		// note: this may be removed or refactored once we see its magic happen
		// returns: true - ready to render, false - unable to render
		//
		bool PreRenderCheck();

		//
		// null and default ctor
		//
		Renderer();

		// unimplemented
		Renderer& operator = ( _In_ const Renderer& ) = delete;
		Renderer( _In_ const Renderer& ) = delete;
		Renderer( _In_ const Renderer&& ) = delete;

	public:

		//
		// Static API class, contains the names used for default loaded rasterizer states
		//
		static class RasterizerState abstract sealed
		{
		private:

			// this class provides static memory only
			RasterizerState( );
			RasterizerState( _In_ const RasterizerState& );
			RasterizerState( _In_ const RasterizerState&& );
			RasterizerState& operator = ( _In_ const RasterizerState& );

		public:

			static const HyJynxCore::Text FrontFaceSolid;
			static const HyJynxCore::Text WireFrame;
			static const HyJynxCore::Text WireFrameManualDepth;
			static const HyJynxCore::Text Solid2DManualDepth;
		};

		//
		// dtor
		//
		~Renderer( );

		//
		// retrieves all singleton loggers relevant to the rendering system 
		//
		virtual void GetChildrenLoggers( _In_ HyJynxCollections::DynamicCollection< HyJynxLogger::ILog* >* const ) override;

		//
		// Release all run-time data. Rendering Renderer Unrenderable.
		// This should only be called during application shutdown.
		//
		void Shutdown( );

		//
		// Initialize the renderer to a ready-to-draw state. with defined configurations
		// - return: true for success, false if we need to abadon ship
		//
		bool Initialize( _In_ HWND*, _In_ GraphicsConfiguration*, _In_ RendererConfiguration* );

		//
		// ReInitialize Renderer with current Graphics Settings. This call is automated
		// whenever a Graphics undergoes a change, but hey go ahead and call it yourself
		// if you like, its all good.
		//
		bool ReInitialize( );

		//
		// Register a window for rendering
		// - Window* pointer to the window used to contain a list of drawables
		//
		void AddWindow( _In_ Window* );

		//
		// Remove a window from renderer's reign
		// - Window& pointer to the Window you wish to remove
		//
		void RemoveWindow( _In_ Window* );

		//
		// Begins the game draw sequence
		//
		const RenderReport& Render( );

		//
		// when graphic settings change, renderer will release the current render targets,
		// so the current RenderSequence can reinitialize what it needs
		//
		void ReleaseRenderTargets( );


		// ==========================================================================

		//
		// Set the color object used to clear the screen
		// -Color*: pointer to the color object you will use to clear the screen
		//
		void SetClearColor( _In_opt_ Color* color = nullptr );

		//
		// Get a reference to the renderSequence currently set
		// returns IRenderSequence*
		//
		IRenderSequence* GetRenderSequence( ) const;

		//
		// set a new render sequence. During this process the sequence will have a chance
		// to ensure Renderer is able to perform the tasks.
		// -IRenderSequence*: reference to your new rendering sequence, if errors occur, Renderer
		//						will default to RenderSequeneceNull (whatever clear screen color is),
		//						or white (Renderer is F.U.B.A.R. and not drawing at all) - Have Fun!
		//
		void SetRenderSequence( _In_opt_ IRenderSequence* = nullptr );

		//
		// get the object containing all of the graphics library API
		// Graphics*: pointer to the graphics library API
		//
		Graphics* GetGraphicsLibrary();

		//
		// get a pre-loaded rasterizer (note: every rasterizer state change will inflict performance)
		// - Text: ID Name of the loaded rasterizer, eg: RasterizerState::WireFrame
		// - ID3D11RasterizerState*: dx11 rasterizer state
		//
		ID3D11RasterizerState* GetRasterizer( _In_ HyJynxCore::Text );

		//
		// get the currently set active camera
		// - returns Camera*: pointer to the active camera
		//
		Camera* GetActiveCamera( ) const;

		//
		// set an active camera to act as Renderer's eyes
		// - Camera*: pointer to a camera considered active
		//
		void SetActiveCamera( _In_ Camera* );

		//
		// get the size of the drawable screen
		// - returns Vector2: xy dimensions of the drawable screen
		//
		Vector2 GetScreenSize( ) const;

		//
		// Determine the maximum directX the hardware supports
		// - returns D3D_FEATURE_LEVEL: directX version
		//
		D3D_FEATURE_LEVEL GetDeviceFeatureLevel( );

		//
		// determine if the argued DXGI format is supported by the hardware
		// - DXGI_FORMAT: format in question
		// - returns UInt: support bit-wise flags
		//
		UInt GetFormatSupport( _In_ DXGI_FORMAT );

		//
		// change how Renderer will cull objects
		// - CollisionMethod: maximum complexity to test
		//
		void SetCullMethod( _In_ const CollisionMethod );

	};
}

#endif // HYJYNXRENDERER_RENDERER_H