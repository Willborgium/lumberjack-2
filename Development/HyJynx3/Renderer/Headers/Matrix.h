/**
 * Matrix.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_MATRIX_H
#define HYJYNXRENDERER_MATRIX_H

#include <DirectXMath.h>
#include <sal.h>

namespace HyJynxRenderer
{
	//
	// this math primitive extends DirectX:XMFLOAT4X4 with built-in functionality
	//
	class Matrix : public DirectX::XMFLOAT4X4
	{
	public:

		//
		// null ctor - defaults to identity
		//
		Matrix( );

		//
		// copy ctor
		//
		Matrix( _In_ const Matrix& );

		//
		// move ctor
		//
		Matrix( _In_ const Matrix&& );

		//
		// get a created matrix set to identity
		// - return Matrix: identity matrix
		//
		static Matrix Identity( );

#pragma region operators

		bool operator == ( _In_ const Matrix& ) const;

		bool operator != ( _In_ const Matrix& ) const;

		Matrix& operator = ( _In_ const Matrix& );

		Matrix operator * ( _In_ const Matrix& );

		Matrix& operator *= ( _In_ const Matrix& );

#pragma endregion
	};
}

#endif // HYJYNXRENDERER_MATRIX_H