/**
 * ICameraBehavior.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_ICAMERABEHAVIOR_H
#define HYJYNXRENDERER_ICAMERABEHAVIOR_H

#include <Logger\Headers\ILog.h>

namespace HyJynxRenderer
{
	class ICameraBehavior abstract : public HyJynxLogger::ILog
	{
		friend class Camera;

	public:

		virtual void Update() = 0;

	};
}

#endif // HYJYNXRENDERER_ICAMERABEHAVIOR_H