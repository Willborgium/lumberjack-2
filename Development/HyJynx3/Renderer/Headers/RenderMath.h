/**
 * RenderMath.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_RENDERMATH_H
#define HYJYNXRENDERER_RENDERMATH_H

#include <Renderer\Headers\Transform.h>
#include <Renderer\Headers\ViewFrustum.h>
#include <Renderer\Headers\CollisionDescription.h>
#include <sal.h>
#include <DirectXMath.h>

namespace HyJynxRenderer
{
	class RenderMath abstract sealed
	{
	private:

		RenderMath( );
		RenderMath( const RenderMath& );
		RenderMath( const RenderMath&& );
		RenderMath& operator = ( const RenderMath& );

		//
		// multiply a value by 'power' times - recursive
		// -<typename> value to multiply
		// -<typename> how many times to power it
		//
		template<typename T, typename P> static T GetPower( _In_ const T value, _In_ const P power )
		{
			if ( power > 0 )
			{
				return RenderMath::GetPower<T, P>( value * value, power - 1 );
			}
			else return value;
		}

	public:

		//
		// Linear Interpolation between start and end
		// -<type 1> beginning value of interpolation
		// -<type 1> ending value of interpolation
		// -<type 2> scalar to multiply difference (eg: 0-1)
		//
		template<typename T, typename C> static T LinearInterpolate( _In_ const T start, _In_ const T end, _In_ const C scalar )
		{
			return start + ( ( start - end ) * scalar );
		}

		//
		// Bi-linear Interpolation between start and end
		// -<type 1> beginning value of interpolation
		// -<type 1> ending value of interpolation
		// -<type 2> scalar to multiply^2 difference (eg: 0-1 * 0-1)
		//
		template<typename T, typename C> static T BiLinearInterpolate( _In_ const T start, _In_ const T end, _In_ const C scalar )
		{
			return start + ( ( end - start ) * ( scalar * scalar ) );
		}

		//
		// linear Interpolation between start and end, with a specified multiplier power
		// -<type 1> beginning value of interpolation
		// -<type 1> ending value of interpolation
		// -<type 2> scalar to multiply difference (eg: 0-1)
		// -<type 2> the power of the scalar
		//
		template<typename T, typename C> static T Interpolate( _In_ const T start, _In_ const T end, _In_ const C scalar, _In_ const C multiplier )
		{
			return start + ( end - start ) * RenderMath::GetPower<T, C>( scalar, multiplier );
		}

		//
		// clamp a value between min and max
		// -typename: value you want to clamp
		// -typename: minimum possible value allowed
		// -typename: maximum possible value allowed
		// -returns typename: clamped value between min and max
		//
		template<typename T> static T Clamp( _In_ const T val, _In_ const T min, _In_ const T max )
		{
			T output = val;

			if ( val < min )
			{
				output = min;
			}

			if ( val > max )
			{
				output = max;
			}

			return output;
		}

		//
		// get the distance between two Transform objects
		// - Transform*: first transformation
		// - Transform*: second transformation, get distance from one
		// - returns float: distance between the two transforms
		//
		static float Distance( _In_ const Transform&, _In_ const Transform& );

		//
		// get the distance between two positions
		// - XMFLOAT3: position1
		// - XMFLOAT3: position2
		// - returns float: distance between two points
		//
		static float Distance( _In_ const DirectX::XMFLOAT3&, _In_ const DirectX::XMFLOAT3& );

		//
		// get absolute value of the given argument of any type
		// - typename T: value in question
		//
		template< typename T > static T Abs( _In_ const T val )
		{
			return val < 0 ? val * -1 : val;
		}

		//
		// convert degrees to radians
		// - float: degrees
		// - returns float: radians
		//
		static float ToRadians( _In_ const float );

		//
		// convert radians to degrees
		// - float: radians
		// - returns float: degrees
		//
		static float ToDegrees( _In_ const float );
	};
}

#endif // HYJYNXRENDERER_RENDERMATH_H