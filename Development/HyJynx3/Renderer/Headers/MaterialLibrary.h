/**
 * MaterialLibrary.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_MATERIALLIBRARY_H
#define HYJYNXRENDERER_MATERIALLIBRARY_H

#include <sal.h>

namespace HyJynxRenderer
{
	//
	// MaterialLibrary is a static class containing API and static memory,
	// Althought front-end can define their own Materials, all default supplied material ID's 
	// can be found here.
	//
	class MaterialLibrary abstract sealed
	{
	private:


		// this class provides static memory and API only
		MaterialLibrary( ) = delete;
		MaterialLibrary( _In_ const MaterialLibrary& ) = delete;
		MaterialLibrary( _In_ const MaterialLibrary&& ) = delete;
		MaterialLibrary& operator = ( _In_ const MaterialLibrary& ) = delete;

	public:

		~MaterialLibrary( );

		static const int Material_Unknown = -1;
		static const int ShaderLinkage2DLayer = 1;
		static const int ShaderLinkage3DLayer = 2;

	};
}

#endif // HYJYNXRENDERER_MATERIALLIBRARY_H