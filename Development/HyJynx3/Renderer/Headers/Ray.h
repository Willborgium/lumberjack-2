/**
 * Ray.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_RAY_H
#define HYJYNXRENDERER_RAY_H

#include <Renderer/Headers/Vector3.h>
#include <DirectXMath.h>
#include <sal.h>

namespace HyJynxRenderer
{
	//
	// This nifty little oh-so-typical class contains a... a... a RAY!
	// contains position, vector, and a maximum distance accounted
	//
	class Ray
	{
	protected:

		Vector3	_origin = Vector3( 0.0f, 0.0f, 0.0f );
		Vector3 _vector = Vector3( 0.0f, 1.0f, 0.0f );
		float _maxDistance;

	public:

		//
		// null ctor
		//
		Ray( );

		//
		// default ctor
		// -XMFLOAT3: origin, starting location of this ray
		// -XMFLOAT3: vector, direction of this ray, normalized or not
		//
		Ray( _In_ Vector3, _In_ Vector3 );

		//
		// fully defined ctor
		// -XMFLOAT3: origin, starting location of this ray
		// -XMFLOAT3: vector, direction of this ray, normalized or not
		// - float: maximum distance this ray represents, absolute value used
		//
		Ray( _In_ Vector3, _In_ Vector3, _In_ float );

		//
		// copy ctor
		//
		Ray( _In_ const Ray& );

		//
		// move ctor
		//
		Ray( _In_ const Ray&& );

		//
		// destructs this shit - as if theres shit to destruct mwahaha
		//
		~Ray( );

		//
		// normalize the vector to a length of 1
		//
		void Normalize( );

		//
		// static method to normalize the argued ray
		// - Ray*: the ray to normalize
		//
		static void Normalize( _In_ Ray* );

		//
		// get the origin of this ray
		// - returns XMFLOAT3*: origin
		//
		Vector3* GetOrigin( );

		//
		// get the vector of this ray
		// - returns XMFLOAT3*: vector
		//
		Vector3* GetVector( );

		//
		// get the maximum distance this Ray represents (defaults to maximum float)
		// - returns float: maximum this distance the Ray represents
		//
		float GetDistance( ) const;

		//
		// set the maximum distance this ray represents, 
		// cannot be negative, will use absolute value
		// - float: maximum distance the ray will account for
		//
		void SetDistance( _In_ const float );
	};
}

#endif // HYJYNXRENDERER_RAY_H