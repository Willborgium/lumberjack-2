/**
 * GraphicsConfiguration.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_GRAPHICSCONFIGURATION_H
#define HYJYNXRENDERER_GRAPHICSCONFIGURATION_H

#include <Engine/Headers/Dirty.h>
#include <Engine/Headers/TypeDefinitions.h>
#include <sal.h>

#pragma warning(disable: 4005)
#include <D3D11.h>
#pragma warning(default: 4005)

namespace HyJynxRenderer
{
	//
	// Describes Multisample Anti-Aliasing quality setting
	//
	enum class MsaaQuality : short
	{
		MSAA_NONE = 0,
		MSAA_2X = 2,
		MSAA_4X = 4,
		MSAA_8X = 8,
		MSAA_16X = 16,
		MSAA_32X = 32 // hah, you wish sucka
	};

	//
	// all configuration settings are found here
	//
	class GraphicsConfiguration sealed : public HyJynxUtilities::Dirty
	{

	private:

		bool					_directXDebug = false;

		UInt					_width = 512;
		UInt					_height = 256;
		D3D_DRIVER_TYPE			_driverType = D3D_DRIVER_TYPE_HARDWARE;
		UInt					_refreshRate = 60;
		UInt					_bufferCount = 1;
		MsaaQuality				_msaa = MsaaQuality::MSAA_8X;
		bool					_fullscreen = false;

	public:

		//
		// default value ctor
		//
		GraphicsConfiguration( );

		//
		// copy ctor
		//
		GraphicsConfiguration( _In_ const GraphicsConfiguration& );

		//
		// move ctor
		//
		GraphicsConfiguration( _In_ const GraphicsConfiguration&& );

		//
		// dtor
		//
		virtual ~GraphicsConfiguration( );

		//
		// assignment operator
		//
		GraphicsConfiguration operator = ( _In_ const GraphicsConfiguration& );

#pragma region setters/getters

		//
		// is directX in debug mode
		// - returns bool: true: directX was created with the debug flag
		//
		bool GetDirectXDebug( ) const;

		//
		// get the width of the screen
		// returns UInt: width in pixels
		//
		UInt GetWidth( ) const;

		//
		// get the height of the screen
		// returns UINT: height in pixels
		//
		UInt GetHeight( ) const;

		//
		// set whether directX is in debug mode.
		// - bool: true: directX created in debug mode, false it is not
		//
		void SetDirectXDebug( _In_ const bool );

		//
		// set the width of the screen
		// -UInt: width in pixels
		//
		void SetWidth( _In_ UInt );

		//
		// set the height of the screen
		// -UInt: height in pixels
		//
		void SetHeight( _In_ UInt );

		//
		// determine if configuration is set to fullscreen
		// returns bool: true - fullscreen, false - not.
		//
		bool GetFullscreen( ) const;

		//
		// set the fullscreen config
		// -bool: true - fullscreen, false - not
		//
		void SetFullscreen( _In_ bool );

		//
		// get the DX11 preffered refresh rate
		// returns UInt: refresh rate per second
		//
		UInt GetRefreshRate( ) const;

		//
		// set the DX11 preffered refresh rate
		// -UInt: rate per second
		//
		void SetRefreshRate( _In_ UInt );

		//
		// get the currently set MSAA quality
		// retuns MsaaQuality: it does, I swear
		//
		MsaaQuality GetMSAAQuality( ) const;

		//
		// set a multi-sample-anti-aliasing quality
		// -MsaaQuality: sampling quality of anti-aliasing
		//
		void SetMSAAQuality( _In_ MsaaQuality );

		//
		// get how many back-buffers Renderer will utilize
		// -returns UINT: number of back-buffers
		//
		UINT GetBufferCount( ) const;

		//
		// set how many back-buffers Renderer will utilize
		// -UINT: amount of backbuffers
		// note: I suggest you leave the default, Renderer will decide what's best
		//
		void SetBufferCount( _In_ UInt );

		//
		// get the current driver mode. 
		// returns D3D_DRIVER_TYPE: what type of graphics driver we're drawing with
		//
		D3D_DRIVER_TYPE GetDriverType( ) const;

		//
		// set the graphics driver mode. If your argued driver mode is incorrect, renderer will correct you
		// -D3D_DRIVER_TYPE: your options are: [TODO]
		//
		void SetDriverType( _In_ D3D_DRIVER_TYPE );

#pragma endregion
	};
}

#endif // HYJYNXRENDERER_GRAPHICSCONFIGURATION_H