/**
* Texture3D.h
* (c) 2014 All Rights Reserved
*/

#ifndef HYJYNXRENDERER_TEXTURE3D_H
#define HYJYNXRENDERER_TEXTURE3D_H

#include <Renderer/Headers/Texture.h>
#include <sal.h>

namespace HyJynxRenderer
{
	struct LoadedTexture;

	//
	// Extends Texture with 3D Implementations
	//
	class Texture3D : public Texture
	{
	protected:

		//
		// Internally called to read pixel data from the CPU stored memory
		//
		virtual void ReadFromCpuData( _In_ LoadedTexture*, _Out_ Color**, _In_ Point3[ ], _In_ UInt ) override;

		//
		// Internally called to read pixel data from the GPU stored memory
		//
		virtual void ReadFromGpuData( _In_opt_ Context*, _In_ LoadedTexture*, _Out_ Color**, _In_ Point3[ ], _In_ UInt ) override;

	public:

		//
		// null ctor
		//
		Texture3D( );

		//
		// default ctor
		//
		Texture3D( _In_ LoadedTexture* );

		//
		// copy ctor
		//
		Texture3D( _In_ const Texture3D& );

		//
		// move ctor
		//
		Texture3D( _In_ const Texture3D&& );

		//
		// dtor
		//
		virtual ~Texture3D( );
	};
}

#endif // HYJYNXRENDERER_TEXTURE3D_H