/**
 * RenderSequenceNull.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_RENDERSEQUENCENULL_H
#define HYJYNXRENDERER_RENDERSEQUENCENULL_H

#include <Renderer/Headers/IRenderSequence.h>

namespace HyJynxRenderer
{
	class RenderSequenceNull sealed : public IRenderSequence
	{
	public:

		//
		// default ctor
		//
		RenderSequenceNull();

		//
		// ensure the render targets we need are created and ready
		//
		virtual bool ValidateTargets( _In_ HyJynxCollections::Dictionary<HyJynxCore::Text, ID3D11RenderTargetView*>* const,
			_In_ HyJynxCollections::Dictionary<HyJynxCore::Text, DepthStencil*>* const ) override;

		//
		// if we couldn't validate our render targets, initialize them
		//
		virtual bool InitializeTargets( _In_ Graphics*, _In_ HyJynxCollections::Dictionary<HyJynxCore::Text, ID3D11RenderTargetView*>*,
			_In_ HyJynxCollections::Dictionary<HyJynxCore::Text, DepthStencil*>* ) override;

		//
		// clear the targets we're using to a blank slate
		//
		virtual bool ClearTargets( _In_ Context*, 
			_In_ HyJynxCollections::Dictionary<HyJynxCore::Text, ID3D11RenderTargetView*>*,
			_In_ HyJynxCollections::Dictionary< HyJynxCore::Text, DepthStencil*>*,
			_In_opt_ Color* = nullptr ) override;

		//
		// loop through windows/drawables and render them to the screen
		//
		virtual bool Render( _In_ Context*,
			_In_ HyJynxCollections::Dictionary<HyJynxCore::Text, ID3D11RenderTargetView*>*,
			_In_ HyJynxCollections::Dictionary<HyJynxCore::Text, DepthStencil*>*,
			_In_ BatchContainer* ) override;
	};
}

#endif // HYJYNXRENDERER_RENDERSEQUENCENULL_H