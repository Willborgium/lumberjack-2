/**
 * PrimitiveConfiguration.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_PRIMITIVECONFIGURATION_H
#define HYJYNXRENDERER_PRIMITIVECONFIGURATION_H

#include <Engine\Headers\Dirty.h>

namespace HyJynxRenderer
{
	//
	// All primitives have specific configurations, 
	// eg: a sphere has a radius, and segments...
	// this class will be the parent container for these configurations
	//
	class PrimitiveConfiguration abstract : public HyJynxUtilities::Dirty
	{
	public:

		//
		// null ctor
		//
		PrimitiveConfiguration( );

		//
		// copy ctor
		//
		PrimitiveConfiguration( _In_ const PrimitiveConfiguration& );

		//
		// copy ctor
		//
		PrimitiveConfiguration( _In_ const PrimitiveConfiguration&& );

		//
		// dtor
		//
		virtual ~PrimitiveConfiguration();

	};
}

#endif // HYJYNXRENDERER_PRIMITIVECONFIGURATION_H