/**
 * Texture.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_TEXTURE_H
#define HYJYNXRENDERER_TEXTURE_H

#include <Renderer\Headers\Color.h>
#include <Renderer\Headers\Context.h>
#include <Renderer\Headers\Point3.h>
#include <Engine\Headers\TypeDefinitions.h>
#include <Logger\Headers\ILog.h>
#include <sal.h>

#pragma warning(disable: 4005)
#include <D3D11.h>
#pragma warning(default: 4005)

namespace HyJynxRenderer
{
	struct LoadedTexture;

	//
	// Implements base functionality and API all Texture types will inherit from
	// All data point to TextureManager's data, allowing TextureManager to automate
	// the resize and updates of loaded textures.
	//
	class Texture abstract : public HyJynxLogger::ILog
	{
		friend class TextureManager;

	protected:

		LoadedTexture*	_texture;

		//
		// Internally called to read pixel data from the CPU stored memory
		//
		virtual void ReadFromCpuData( _In_ LoadedTexture*, _Out_ Color**, _In_ Point3[ ], _In_ UInt ) = 0;

		//
		// Internally called to read pixel data from the GPU stored memory
		//
		virtual void ReadFromGpuData( _In_opt_ Context*, _In_ LoadedTexture*, _Out_ Color**, _In_ Point3[ ], _In_ UInt ) = 0;

	public:

		//
		// null ctor
		//
		Texture( );

		//
		// default ctor TextureManager uses
		//
		Texture( _In_ LoadedTexture* );

		//
		// copy ctor
		//
		Texture( _In_ const Texture& );

		//
		// move ctor
		//
		Texture( _In_ const Texture&& );

		//
		// dtor
		//
		virtual ~Texture( );

		//
		// get the resource view used in shader binding.
		// note: may return nullptr if the texture was initialized to not load on the GPU
		// - returns ID3D11ShaderResrouceView*: dx11 resource view
		//
		virtual ID3D11ShaderResourceView* GetResourceView( );

		//
		// get the texture resource used in shader mapping
		// note: may return nullptr if the texture was initialized to not load on the GPU
		// - returns ID3D11Resource*: parent to Dx11 Texture interfaces
		//
		virtual ID3D11Resource* GetTexture( );

		//
		// Get the pixel colors of the given indicies, which point3 arguments are being used will determined
		// by which Texture child this is being called from
		// - Context*: if the texture is loaded to GPU only, context is needed to map the region for cpu read
		//			   if you are positive this texture is on the CPU, you can pass nullptr
		// - Point3*: array of indices you wish to recieve pixels for
		// - returns Color*: RGBA array of the argued indicies, 
		//					 if invalid indices were argued, white will be returned
		//
		Color* GetPixel( _In_opt_ Context*, _In_ Point3[ ] );
	};
};

#endif // HYJYNXRENDERER_TEXTURE_H 