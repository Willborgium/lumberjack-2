/**
 * DrawableCube3D.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_DRAWABLECUBE3D_H
#define HYJYNXRENDERER_DRAWABLECUBE3D_H

#include <Renderer/Headers/Drawable.h>
#include <Renderer/Headers/Primitive.h>
#include <Renderer/Headers/PrimitiveConfiguration.h>
#include <sal.h>

namespace HyJynxRenderer
{
	// 
	// contains all subresource configurations
	//
	class DrawableCube3DConfiguration : public PrimitiveConfiguration
	{
	protected:

		Vector3		_dimensions = Vector3::One( );
		Color		_topColor = Color( 255.0f );
		Color		_bottomColor = Color( 255.0f );

	public:

		//
		// null ctor
		//
		DrawableCube3DConfiguration( );

		//
		// dimensions defined ctor
		//
		DrawableCube3DConfiguration( _In_ const Vector3 );

		//
		// dimensions, solid color defined ctor
		//
		DrawableCube3DConfiguration( _In_ const Vector3, _In_ const Color );

		//
		// dimensions, top-to-bottom gradient color defined ctor
		//
		DrawableCube3DConfiguration( _In_ const Vector3, _In_ const Color, _In_ const Color );

		//
		// copy ctor
		//
		DrawableCube3DConfiguration( _In_ const DrawableCube3DConfiguration& );

		//
		// move ctor
		//
		DrawableCube3DConfiguration( _In_ const DrawableCube3DConfiguration&& );

		//
		// dtor
		//
		virtual ~DrawableCube3DConfiguration( );

		//
		// Get dimensions of the cube
		// - return Vector3: dimensions
		//
		Vector3 GetDimensions( ) const;

		//
		// set the dimensions of the cube
		// - Vector3: dimensions
		//
		void SetDimensions( _In_ const Vector3 );

		//
		// set the dimensions of the cube
		// - float: width
		// - float: height
		// - float: depth
		//
		void SetDimensions( _In_opt_ const float = NULL, _In_opt_ const float = NULL, _In_opt_ const float = NULL );

		//
		// get the color applied to the top of the cube
		// - returns Color: top color component
		//
		Color GetTopColor( ) const;

		//
		// set the color applied to the top of the ube
		// - Color: top color component
		//
		void SetTopColor( _In_ const Color );

		//
		// set the color applied to the top of the cube
		// - float: R component
		// - float: G component
		// - float: B component
		// - float: A component
		//
		void SetTopColor( _In_ const float, _In_ const float, _In_ const float, _In_ const float );

		//
		// get the color applied to the bottom of the cube
		// - returns Color: top color component
		//
		Color GetBottomColor( ) const;

		//
		// set the color applied to the bottom of the cube
		// - Color: top color component
		//
		void SetBottomColor( _In_ const Color );

		//
		// set the color applied to the bottom of the cube
		// - float: R component
		// - float: G component
		// - float: B component
		// - float: A component
		//
		void SetBottomColor( _In_ const float, _In_ const float, _In_ const float, _In_ const float );
	};

	// =================================================================

	//
	// implements a basic cube to be drawn in 3D space
	//
	class DrawableCube3D : public Drawable, public Primitive
	{
	protected:

		Transform _transform;

		//
		// update the data we have loaded on the GPU
		//
		void updateSubresourceData( );

	public:

		//
		// null ctor
		//
		DrawableCube3D( );

		//
		// configured CTOR
		// - DrawableCube3DConfiguration: cube configurations container
		//
		DrawableCube3D( _In_ DrawableCube3DConfiguration* );

		//
		// copy ctor
		//
		DrawableCube3D( _In_ const DrawableCube3D& );

		//
		// move ctor
		//
		DrawableCube3D( _In_ const DrawableCube3D&& );

		//
		// good ol' dtor
		//
		virtual ~DrawableCube3D( );

		//
		// generate the renderable model based from the current configurations
		//
		virtual void GeneratePrimitiveModel( ) override;

		//
		// load the cube, ready for rendering
		// - function< void(void) >: lambda to call when loading has completed
		//
		virtual void LoadModel( _In_ HyJynxCore::Function< void > ) override;

		//
		// generate collision description
		//
		virtual void GenerateBounding( ) override;

		//
		// per-cycle update call
		// Context*: dx API container
		// const RenderTime*: reference to Renderer's per cycle run timer
		//
		virtual void Update( _In_ Context*, _In_ const RenderTime ) override;

		//
		// when we're in development, this API will help us draw debugging information to the screen
		// -Context*: reference to the context we're drawing to
		//
		virtual void RenderDebug( _In_ Context* ) override;

		//
		// get the transform which represents this cube
		// - returns Transform*: on-screen transformation container
		//
		virtual Transform* GetTransform( ) override;
	};
}

#endif // HYJYNXRENDERER_DRAWABLECUBE3D_H