/**
 * Vertex.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_VERTEX_H
#define HYJYNXRENDERER_VERTEX_H

#include <Renderer/Headers/Vector2.h>
#include <Renderer/Headers/Vector3.h>
#include <Renderer/Headers/Vector4.h>
#include <sal.h>

#pragma warning(disable: 4005)
#include <d3d11.h>
#pragma warning(default: 4005)

namespace HyJynxRenderer
{

#pragma region VertexPosition

	//
	// A null-type vertex who only defines position
	//
	struct VertexPosition
	{
		Vector3 _position;

		//
		// null ctor - should never be used
		//
		VertexPosition();

		//
		// default ctor
		// -Vector3: position
		//
		VertexPosition( _In_ Vector3 );

		//
		// Aggregate friendly ctor
		// -float: x component
		// -float: y component
		// -float: z component
		//
		VertexPosition( _In_ float, _In_ float, _In_ float );
	};
	
#pragma endregion

#pragma region VertexPositionColor

	//
	// This vertex describes a vert with positition and color
	//
	struct VertexPositionColor
	{
		Vector3 _position;
		Vector4 _color;


		//
		// null ctor
		//
		VertexPositionColor( );

		//
		// default ctor
		// -Vector3: position component
		// -Vector4: color value
		//
		VertexPositionColor( _In_ Vector3, _In_ Vector4 );

	};

#pragma endregion

#pragma region VertexPositionColorTexture

	//
	// This vertex describes a vert with positition and color
	//
	struct VertexPositionColorTexture
	{

		Vector3 _position;
		Vector4 _color;
		Vector2 _texCoord;

		//
		// null ctor
		//
		VertexPositionColorTexture( );

		//
		// default ctor
		// -Vector3: position component
		// -Vector4: color value
		// -Vector2: tex coordinates
		//
		VertexPositionColorTexture( _In_ Vector3, _In_ Vector4, _In_ Vector2 );

	};

#pragma endregion

#pragma region VertexPositionNormalColor

	struct VertexPositionNormalColor
	{
		Vector3 _position;
		Vector3 _normal;
		Vector4 _color;

		//
		// null ctor
		//
		VertexPositionNormalColor( );

		//
		// default ctor
		// -Vector3: position
		// -Vector3: normal
		// -Vector4: color
		//
		VertexPositionNormalColor( _In_ Vector3, _In_ Vector3, _In_ Vector4 );
	};

#pragma endregion

#pragma region VertexPositionTexture

	//
	// a vertex which defines position and texture coordinate
	//
	struct VertexPositionTexture
	{
		Vector3 _position;
		Vector2 _texCoord;

		//
		// null ctor - should never be used
		//
		VertexPositionTexture( );

		//
		// default ctor
		// -Vector3: position of the vertex
		// -Vector2: texture coordinate of the vertex
		//
		VertexPositionTexture( _In_ Vector3, _In_ Vector2 );
	};

#pragma endregion 

#pragma region VertexPositionTextureNormal

	//
	// this vertex defines position, normal, and texture coordinate
	//
	struct VertexPositionNormalTexture
	{
		Vector3 _position;
		Vector3 _normal;
		Vector2 _texCoord;

		//
		// null ctor - should never be used
		//
		VertexPositionNormalTexture();

		//
		// default used ctor - all values defined
		//
		VertexPositionNormalTexture( _In_ Vector3, _In_ Vector3, _In_ Vector2 );

	};

#pragma endregion 

}

#endif // HYJYNXRENDERER_VERTEX_H