/**
 * Texture1D.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_TEXTURE1D_H
#define HYJYNXRENDERER_TEXTURE1D_H

#include <Renderer/Headers/Texture.h>
#include <sal.h>

namespace HyJynxRenderer
{
	struct LoadedTexture;

	//
	// Extends Texture with 1D Implementations
	//
	class Texture1D : public Texture
	{
	protected:

		//
		// Internally called to read pixel data from the CPU stored memory
		//
		virtual void ReadFromCpuData( _In_ LoadedTexture*, _Out_ Color**, _In_ Point3[ ], _In_ UInt ) override;

		//
		// Internally called to read pixel data from the GPU stored memory
		//
		virtual void ReadFromGpuData( _In_opt_ Context*, _In_ LoadedTexture*, _Out_ Color**, _In_ Point3[ ], _In_ UInt ) override;

	public:

		//
		// null ctor
		//
		Texture1D( );

		//
		// default ctor
		//
		Texture1D( _In_ LoadedTexture* );

		//
		// copy ctor
		//
		Texture1D( _In_ const Texture1D& );

		//
		// move ctor
		//
		Texture1D( _In_ const Texture1D&& );

		//
		// dtor
		//
		virtual ~Texture1D( );

		//
		// equality operator
		//
		bool operator == ( _In_ const Texture1D& ) const;

		//
		// inequality operator
		//
		bool operator != ( _In_ const Texture1D& ) const;

	};
}

#endif // HYJYNXRENDERER_TEXTURE1D_H