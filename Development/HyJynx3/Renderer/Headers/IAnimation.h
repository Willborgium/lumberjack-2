/**
 * IAnimation.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_IANIMATION_H
#define HYJYNXRENDERER_IANIMATION_H

#include <sal.h>

namespace HyJynxRenderer
{
	//
	// Animation interface, proxy to ActiveAnimation template container
	//
	class IAnimation abstract
	{
	protected:

		//
		// Remove this animation from Animator
		//
		void Remove( );

	public:

		//
		// Per-cycle update
		// -double: delta milliseconds
		//
		virtual void Update( _In_ const double ) = 0;

		//
		// determine the active state of the animation
		// - returns bool: true - the animation is updating, false - tis not
		//
		virtual bool IsActive( ) = 0;

		//
		// set the animation active state
		// bool: true - the animation is updating, false - tis not
		//
		virtual void SetActive( _In_ const bool ) = 0;

		//
		// determine the animation's progress on a 0 to 1 scale
		// - returns double: 0-1 progress value
		//
		virtual double GetProgress( ) = 0;

		//
		// set the animation's progress on a 0 to 1 scale
		// - double: zero to one scale of progress
		//
		virtual void SetProgress( _In_ const double ) = 0;

		//
		// get the speed in the animation per second
		// - return double: frames per second
		//
		virtual double GetSpeed( ) = 0;

		//
		// set the speed of the animation per second
		// - double: frames per second
		//
		virtual void SetSpeed( _In_ const double ) = 0;

		//
		// release all references for disposal
		//
		virtual void Release( ) = 0;

		//
		// determine if this animation is within Animator's list
		//
		virtual bool IsValid( ) = 0;
	};

}

#endif // HYJYNXRENDERER_IANIMATION_H