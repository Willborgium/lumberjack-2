/**
 * Point3.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_POINT3_H
#define HYJYNXRENDERER_POINT3_H

#include <sal.h>

namespace HyJynxRenderer
{

	//
	// wraps xyz integral values, handy for dimensional indicies
	//
	class Point3
	{
	public:

		int X = 0,
			Y = 0,
			Z = 0;

#pragma region init

		//
		// null ctor
		//
		Point3( );

		//
		// initialize all components to the argued value
		//
		Point3( _In_ int );

		//
		// initialize all components seperately
		//
		Point3( _In_ int, _In_ int, _In_ int );

		//
		// copy ctor
		//
		Point3( _In_ const Point3& );

		//
		// move ctor
		//
		Point3( _In_ const Point3&& );

		//
		// dtor
		//
		virtual ~Point3( );

#pragma endregion

#pragma region API



#pragma endregion

#pragma region Operators

		bool operator == ( _In_ const Point3& other ) const;
		bool operator != ( _In_ const Point3& other ) const;

		Point3& operator += ( _In_ const Point3& other );
		Point3& operator -= ( _In_ const Point3& other );
		Point3& operator *= ( _In_ const Point3& other );
		// will check rho for 0, and default to 1
		Point3& operator /= ( _In_ const Point3& other );

		Point3 operator + ( _In_ const Point3& other ) const;
		Point3 operator - ( _In_ const Point3& other ) const;
		Point3 operator * ( _In_ const Point3& other ) const;
		// will check rho for 0, and default to 1
		Point3 operator / ( _In_ const Point3& other ) const;

#pragma endregion
	};
};

#endif // HYJYNXRENDERER_POINT3_H 