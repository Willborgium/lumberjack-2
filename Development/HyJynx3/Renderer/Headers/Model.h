/**
 * Model.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_MODEL_H
#define HYJYNXRENDERER_MODEL_H

#include <Renderer/Headers/Mesh.h>
#include <Renderer/Headers/Transform.h>
#include <Engine/Headers/Text.h>
#include <Engine/Headers/DynamicCollection.h>

namespace HyJynxRenderer
{
	class Texture2D;

	//
	// This is a the main drawable which all Drawing objects will interact with.
	// In here we contain a list of Meshs, all of which are either programmatically created (eg: billboards)
	// or loaded from a model file (eg: .obj/.fbx/etc...)
	//
	class Model
	{
	protected:

		HyJynxCollections::DynamicCollection<Mesh*> _meshList;

	public:

		//
		// Null Ctor
		//
		Model( );

		//
		// Copy ctor
		//
		Model( _In_ const Model& );

		//
		// move ctor
		//
		Model( _In_ const Model&& );

		//
		// go go dtor
		//
		virtual ~Model( );

		//
		// Add a mesh to this model
		// -Mesh*: pointer to the mesh being added
		//
		void AddMesh( _In_ Mesh* );

		//
		// Remove a mesh out of this model
		// -Mesh*: pointer to the mesh being removed
		//
		void RemoveMesh( _In_ Mesh* );

		//
		// Get a pointer to the list of meshes contained in this model
		// returns: DynamicCollection<Mesh*>*
		//
		HyJynxCollections::DynamicCollection<Mesh*>* GetMeshList( );

		//
		// get a list of mesh objects valid for drawing at the given distance 
		// -DynamicCollection<Mesh*>*: the list to append to
		// -float: distance from camera
		//
		void GetMeshAtLOD( _In_ HyJynxCollections::DynamicCollection<Mesh*>*, _In_ float ) const;

		//
		// apply a material to a contained mesh, optionally filtering by the name of the mesh
		// - Material*: Material being applied to the named mesh
		// - Text: (optional) name of the mesh(es) the material will be applied to
		//
		void ApplyMaterial( _In_ Material*, _In_opt_ HyJynxCore::Text = "" );

		//
		// apply a diffuse texture to a contained mesh, optionally filtering by the name of the mesh
		// - Texture2D*: texture to apply
		// - Text: (optional) name of the mesh(es) the texture will be applied to
		//
		void ApplyDiffuseTexture( _In_ Texture2D*, _In_opt_ HyJynxCore::Text = "" );

		//
		// determines how many mesh objects this model contains
		// - returns UInt: number of meshes in the model
		//
		UInt GetMeshCount( ) const;

		//
		// get the vertex buffer at the given index
		// - UInt: index of the mesh you want the vertex buffer from (defaults to 0)
		// - returns VertexBuffer*: vertexBuffer wrapper object from the given mesh index
		//
		VertexBuffer* GetVertexBufferAtIndex( _In_opt_ UInt = 0 );
	};
}

#endif // HYJYNXRENDERER_MODEL_H