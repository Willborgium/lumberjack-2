/**
 * AnimConfig.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_ANIMCONFIG_H
#define HYJYNXRENDERER_ANIMCONFIG_H

#include <Renderer/Headers/ITween.h>
#include <Engine\Headers\Function.h>
#include <sal.h>

namespace HyJynxRenderer
{

	//
	// Animation Parameters, to be sent into Animator::Animate()
	//
	class AnimConfig sealed
	{
	public:

		HyJynxCore::Function< void, void > OnComplete;
		double Duration = 0;

		//
		// null ctor
		//
		AnimConfig( );

		//
		// copy ctor
		//
		AnimConfig( _In_ const AnimConfig& );

		//
		// move ctor
		//
		AnimConfig( _In_ const AnimConfig&& );

		//
		// dtor
		//
		virtual ~AnimConfig( );

	};
};

#endif // HYJYNXRENDERER_ANIMCONFIG_H 