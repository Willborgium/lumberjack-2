/**
 * Graphics.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_GRAPHICS_H
#define HYJYNXRENDERER_GRAPHICS_H

#include <Renderer/Headers/Context.h>
#include <Renderer/Headers/Vertex.h>
#include <Renderer/Headers/VertexBuffer.h>
#include <Renderer/Headers/IndexBuffer.h>
#include <Renderer/Headers/DepthStencil.h>
#include <Renderer/Headers/GraphicsConfiguration.h>
#include <Renderer/Headers/ShaderFactory.h>
#include <Renderer/Headers/Context.h>
#include <Renderer/Headers/Vertex.h>
#include <Renderer/Headers/VertexBuffer.h>
#include <Renderer/Headers/IndexBuffer.h>
#include <Renderer/Headers/DepthStencil.h>
#include <Renderer/Headers/GraphicsConfiguration.h>
#include <Renderer/Headers/ShaderFactory.h>
#include <Logger/Headers/ILog.h>
#include <Engine/Headers/TypeDefinitions.h>

#pragma warning(disable: 4005)
#include <d3d11.h>
#pragma warning(default: 4005)

namespace HyJynxRenderer
{
	//
	// Graphics contains our API set to interact with DirectX 11.
	//
	class Graphics sealed : public HyJynxLogger::ILog
	{
	private:

		GraphicsConfiguration	_configuration;
		ID3D11Device*			_d3dDevice;
		IDXGISwapChain*			_swapChain;
		Context*				_context;

		// Unimplemented - Renderer, and Renderer only may contain this object
		Graphics( _In_ const Graphics& );
		Graphics( _In_ const Graphics&& );
		Graphics& operator = ( _In_ const Graphics& );

	public:

		//
		// null ctor - graphics will be unusable with this
		//
		Graphics( );

		//
		// Configured ctor - default used
		//
		Graphics( _In_ GraphicsConfiguration );

		//
		// dtor
		//
		~Graphics( );

		//
		// Releases all run-time DirectX objects
		//
		void Shutdown( );

		//
		// get a reference to the graphics configuration object
		// -GraphicsConfiguration*: pointer to the graphics configuration
		//
		GraphicsConfiguration* GetConfiguration( );

		//
		// set a new graphical configuration, or pass null to revert to default settings
		// -GraphicsConfiguration: the config object containing all graphics settings
		//
		void SetConfiguration( _In_opt_ GraphicsConfiguration* config );

		//
		// Initialize the Graphics library with current configuration settings
		//
		bool Initialize( _In_ HWND* );

		//
		// When changes occur, Renderer will try and re-init graphics before drawing again
		//
		bool ReInitialize( );

		//
		// Creates a render target to draw into
		// - int: optional width, defaults to screen width
		// - int: optional height, defaults to screen height
		// - returns: ID3D11RenderTargetView*
		//
		ID3D11RenderTargetView* CreateRenderTarget( _In_opt_ int = -1, _In_opt_ int = -1 );

		//
		// Create a depth stencil to draw into
		// - int: optional width, defaults to screen width
		// - int: optional height, defaults to screen height
		// - returns: ID3D11DepthStencilView*
		//
		DepthStencil* CreateDepthStencil( _In_opt_ int = -1, _In_opt_ int = -1 );

		//
		// Create a vertex input layout on the gpu
		// - VertexDescription: description of a vertex
		// - void*: shader byte code
		// - UInt: size in bytes of the shader code
		// - ID3D11InputLayout**: dx input layout object output
		//
		void CreateInputLayout( _In_ VertexDescription&, _In_ void*,_In_ UInt, _Out_ ID3D11InputLayout** );

		//
		// Creates a class linkage object for dynamic shader linkage
		// - ID3D11ClassLinkage*: output pointer to the created class linkage object
		//
		void CreateClassLinkage( _Out_ ID3D11ClassLinkage** );

		//
		// create a vertex buffer (note: I'm making the caller fill the DX descriptions for better flexibility)
		// - VertexType: what type of verticies to build vertexBuffer with
		// - D3D11_BUFFER_DESC*: pre-filled buffer description
		// - D3D11_SUBRESOURCE_DATA*: pre-filled subresource data
		// - UInt: number of verticies in the buffer
		//
		VertexBuffer* CreateVertexBuffer( _In_ D3D11_BUFFER_DESC*, 
			_In_ D3D11_SUBRESOURCE_DATA*, _In_ VertexDescription, _In_ const UInt );

		//
		// create an index buffer
		// - D3D11_BUFFER_DESC*: pre-filled buffer description
		// - D3D11_SUBRESOURCE_DATA*: pre-filled subresource data
		// - UInt: numver of indicies in the buffer
		//
		IndexBuffer* CreateIndexBuffer( _In_ D3D11_BUFFER_DESC*, _In_ D3D11_SUBRESOURCE_DATA*, _In_ const UInt );

		//
		// present whatever is in the swapchain's buffer
		// -UInt: sync interval
		// -UInt: flags
		//
		void Preset( _In_opt_ UInt = 0, _In_opt_ UInt = 0 );


		/*************************************************************
		*			Testing API
		*************************************************************/

		//
		// Determine if the hardware can use deffered rendering
		// return: bool - true if device can support multithreaded rendering
		//
		bool CanDeferredRender( );

		//
		// Determine the maximum msaa quality of the current device
		// returns: MsaaQuality enumeration value
		//
		MsaaQuality GetMaximumMSAAQuality( );

		/*************************************************************
		*			Setters/Getters
		*************************************************************/

		//
		// Get the default used context (for immediate mode)
		// -return Context*: reference to the context used to render into
		//
		Context* GetDefaultContext( );

		//
		// get the default used directX11 device
		// - returns ID3D11Device*:
		//
		ID3D11Device* GetDevice( );

		//
		// get the width of the drawable screen
		// - returns UInt: pixels wide
		//
		UInt GetWidth( ) const;

		//
		// get the height of the drawable screen
		// - returns UInt: pixels high
		//
		UInt GetHeight( ) const;

	};
}

#endif // HYJYNXRENDERER_GRAPHICS_H