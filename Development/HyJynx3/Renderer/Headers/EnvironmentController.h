/**
 * EnvironmentController.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_ENVIRONMENTCONTROLLER_H
#define HYJYNXRENDERER_ENVIRONMENTCONTROLLER_H

#include <sal.h>

namespace HyJynxRenderer
{

	//
	// Controls the visual world
	//
	class EnvironmentController
	{
	protected:

		// sky color
		// ground color
		// sun color
		// wind map
		// cloud map
		// etc...

	public:

		//
		// null ctor
		//
		EnvironmentController( );

		//
		// copy ctor
		//
		EnvironmentController( _In_ const EnvironmentController& );

		//
		// move ctor
		//
		EnvironmentController( _In_ const EnvironmentController&& );

		//
		// dtor
		//
		~EnvironmentController( );

	};
};

#endif // HYJYNXRENDERER_ENVIRONMENTCONTROLLER_H 