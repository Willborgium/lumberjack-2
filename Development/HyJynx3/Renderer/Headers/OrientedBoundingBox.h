/**
 * OrientedBoundingBox.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_ORIENTEDBOUNDINGBOX_H
#define HYJYNXRENDERER_ORIENTEDBOUNDINGBOX_H

#include <Renderer/Headers/Vector3.h>
#include <Renderer/Headers/Matrix.h>
#include <DirectXMath.h>
#include <sal.h>

namespace HyJynxRenderer
{
	class OrientedBoundingBox
	{
	protected:

		Vector3 _minimum = Vector3( -1.0f, -1.0f, -1.0f );
		Vector3 _maximum = Vector3::One( );
		Vector3 _center = Vector3::Zero( );
		Vector3 _extents = Vector3::Zero( );
		Matrix	_world;

		//
		// update center/extents from current min/max values
		// 
		void updateFromMinMax( );

	public:

		//
		// null ctor
		//
		OrientedBoundingBox( );

		//
		// default ctor
		// -XMFLOAT3: minimum
		// -XMFLOAT3: maximum
		//
		OrientedBoundingBox( _In_ const Vector3, _In_ const Vector3 );

		//
		// copy ctor
		//
		OrientedBoundingBox( _In_ const OrientedBoundingBox& );

		//
		// move ctor
		//
		OrientedBoundingBox( _In_ const OrientedBoundingBox&& );

		//
		// dtor
		//
		~OrientedBoundingBox( );

		//
		// get the minimum positional value of this OBB
		// returns Vector3*
		//
		Vector3* GetMinimum( );

		//
		// get the maximum positional value of this OBB
		// returns Vector3*
		//
		Vector3* GetMaximum( );

		//
		// get the center of this obb
		// returns Vector3*
		//
		Vector3* GetCenter( );

		//
		// get the extents of this obb
		// returns Vector3*
		//
		Vector3* GetExtents( );

		//
		// determine if THIS OBB intersects the argued obb
		//
		bool Intersects( _In_ OrientedBoundingBox* );

		//
		// equality operator
		//
		bool operator == ( _In_ const OrientedBoundingBox& other ) const;

		//
		// inequality operator
		//
		bool operator != ( _In_ const OrientedBoundingBox& other ) const;
	};
}

#endif // HYJYNXRENDERER_ORIENTEDBOUNDINGBOX_H