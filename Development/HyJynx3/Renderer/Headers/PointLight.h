/**
 * PointLight.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_POINTLIGHT_H
#define HYJYNXRENDERER_POINTLIGHT_H

#include <Renderer\Headers\ILight.h>
#include <Renderer\Headers\Vector3.h>
#include <Renderer\Headers\Color.h>
#include <Renderer\Headers\Transform.h>
#include <sal.h>

namespace HyJynxRenderer
{

	//
	// Implements ILight as a single omni-directional light, dimensions 
	// will be calculated as a sphere, where it's largest axis determines radius
	//
	class PointLight : public ILight
	{
	protected:

		Transform	_transform = Transform( );
		Color		_diffuseColor = Color( 255.0f, 255.0f, 255.0f );
		Vector3		_dimensions = Vector3::Zero( );
		Vector3		_fov = Vector3( 360.0f, 360.0f, 360.0f );
		float		_intensity = 1.0f;
		float		_distance = 100.0f;
		float		_falloff = 1.0f;
		bool		_castShadows = false;

	public:

#pragma region init

		//
		// null ctor
		//
		PointLight( );

		//
		// copy ctor
		//
		PointLight( _In_ const PointLight& );

		//
		// move ctor
		//
		PointLight( _In_ const PointLight&& );

		//
		// dtor
		//
		virtual ~PointLight( );

#pragma endregion

#pragma region ILight

		//
		// Get the main color of the light source
		// - return Color: Color object reference
		//
		virtual const Color& GetDiffuseColor( ) const override;

		//
		// Set the main color of the light source
		// Color: diffuse color of the light source
		//
		virtual void SetDiffuseColor( _In_ const Color& ) override;

		//
		// Get the general intensity of the light
		// - return float: Intensity, default is 1
		//
		virtual const float GetIntensity( ) const override;

		//
		// Get the general intensity of the light
		// - float: intensity where 1 is default
		//
		virtual void SetIntensity( _In_ const float ) override;

		//
		// Get the Dimensions of the light source, used for Solf-Light calculations
		// - return Vector3: Dimensions of the Light Source
		//
		virtual const Vector3& GetDimensions( ) const override;

		//
		// Set the dimensions of the light source, used for Solf-Light calculations
		// - Vector3: dimensions of the light source
		//
		virtual void SetDimensions( _In_ const Vector3& ) override;

		//
		// Get the general direction of the light
		// - return Vector3: Direction of the Light Source
		//
		virtual const Vector3& GetNormal( ) const override;

		//
		// Set the general direction of the light
		// - Vector3: direction of the light source
		//
		virtual void SetNormal( _In_ const Vector3& ) override;

		//
		// Get the positional Transform of the light
		// - return Transform&: world-decription container
		//
		virtual Transform* GetTransform( ) override;

		//
		// Get the total distance this light can cover
		// - return float: distance this light reaches
		//
		virtual const float GetDistance( ) const override;

		//
		// Set the total distance this light can cover
		// - float: distance in world units this light reaches
		//
		virtual void SetDistance( _In_ const float ) override;

		//
		// Get where the light begins to falloff within it's distance (0-1 scale)
		// - return float: 0-1 scale where this light begins to falloff within its distance
		//
		virtual const float GetFalloff( ) const override;

		//
		// Set where the light begins to falloff within it's distance (0-1 scale)
		// - float: 0-1 scale where this light begins to falloff within its distance
		//
		virtual void SetFalloff( _In_ const float ) override;

		//
		// Determine if this light casts shadows on any receiving drawables
		// - return bool: true - this light will cast shadows, false - will not
		//
		virtual bool CastShadows( ) override;

		//
		// Set this light to cast shadows on any receiving drawables
		// - bool: true - this light will cast shadows, false - will not
		//
		virtual void SetCastShadows( _In_ const bool ) override;

		//
		// Get the field of view of each axis 
		// - return Vector3: local field of view's of each axis
		//
		virtual const Vector3& GetFieldOfView( ) const override;

		//
		// Set the field of view of each local axis
		// - Vector3: field of view of each local axis
		//
		virtual void SetFieldOfView( _In_ const Vector3 ) override;

#pragma endregion

	};
};

#endif // HYJYNXRENDERER_POINTLIGHT_H 