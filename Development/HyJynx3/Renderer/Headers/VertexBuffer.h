/**
 * VertexBuffer.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_VERTEXBUFFER_H
#define HYJYNXRENDERER_VERTEXBUFFER_H

#include <Renderer/Headers/ShaderFactory.h>
#include <Engine\Headers\TypeDefinitions.h>

#include <sal.h>

#pragma warning(disable: 4005)
#include <D3D11.h>
#pragma warning(default: 4005)

namespace HyJynxRenderer
{
	//
	// The VertexBuffer stores a list of points, used for drawing stuff into 3D space.
	//
	class VertexBuffer sealed
	{
	protected:

		VertexDescription		_description;
		ID3D11Buffer*			_buffer = nullptr;
		UInt					_count = 0;

	public:

		//
		// null ctor
		//
		VertexBuffer( );

		//
		// default ctor
		// - VertexDescription: type of verticies this buffer contains
		// - ID3D11Buffer*: pointer to the loaded directX buffer
		// - UInt: Vertex Count
		//
		VertexBuffer( _In_ const VertexDescription&, _In_ ID3D11Buffer*, _In_ const UInt );

		//
		// copy ctor
		//
		VertexBuffer( _In_ const VertexBuffer& );

		//
		// move ctor
		//
		VertexBuffer( _In_ const VertexBuffer&& );

		//
		// dtor
		//
		~VertexBuffer( );
	
		//
		// get the container holding a description of the verticies contained in this buffer
		// - VertexDescription: infomation about what type of verticies we contain
		//
		VertexDescription& GetDescription( );

		//
		// get the dx11 buffer object
		// returns: ID3D11Buffer*
		//
		ID3D11Buffer* GetBuffer( ) const;

		//
		// get the amount of verticies in this buffer
		// - returns UInt: number of verticies contained
		//
		UInt GetCount( ) const;

		//
		// get memory size
		// - returns UInt: size in bytes
		//
		UInt GetSizeInBytes( ) const;

	};
}

#endif // HYJYNXRENDERER_VERTEXBUFFER_H