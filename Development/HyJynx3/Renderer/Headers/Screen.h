/**
 * Screen.h 
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_SCREEN_H
#define HYJYNXRENDERER_SCREEN_H

#include <Windows.h>

namespace HyJynxRenderer
{
	//
	// This class represents the actual monitor screen. We contain the HWND here
	// along with all "Psuedo" Windows which we create inside of it
	//
	class Screen
	{
	protected:

		HWND* _hwnd;

	public:

		//
		// null ctor
		//
		Screen();

		//
		// default ctor with window handle
		//
		Screen(_In_ HWND*);

		//
		// copy ctor - NOT IMPLEMENTED
		//
		Screen(const Screen&);

		//
		// dtor
		//
		~Screen();

		//
		// Get the PC Window we're drawing into
		// return: HWND* pointer to the HWND we're using to draw into
		//
		inline HWND* GetWindow()
		{
			return _hwnd; 
		}

	};
}

#endif // HYJYNXRENDERER_SCREEN_H