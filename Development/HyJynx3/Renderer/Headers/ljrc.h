/**
 * ljrc.h
 * (c) 2014 All Rights Reserved
 */

#ifndef LJRC_H
#define LJRC_H

#include <Engine\Headers\Text.h>
#include <Engine\Headers\DynamicCollection.h>
#include <Engine\Headers\Dictionary.h>
#include <Engine\Headers\TypeDefinitions.h>
#include <Engine\Headers\Allocator.h>
#include <Engine\Headers\Function.h>
#include <sal.h>
#include <DirectXMath.h>

#ifndef COUNT
#define COUNT( arr ) ( sizeof( arr ) / sizeof( arr[0] ) )
#endif // COUNT

#ifndef SAFE_RELEASE
#define SAFE_RELEASE( x ) { if( x != nullptr ) x->Release( ); }
#endif // SAFE_RELEASE

#ifndef SAFE_DELETE
#define SAFE_DELETE(x) { if( x != nullptr ) delete x; }
#endif // SAFE_DELETE

#endif // LJRC_H