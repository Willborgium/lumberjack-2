/**
 * Transform.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_TRANSFORM_H
#define HYJYNXRENDERER_TRANSFORM_H

#include <Renderer\Headers\TransformState.h>
#include <Renderer\Headers\Vector3.h>
#include <Renderer\Headers\Vector4.h>
#include <Renderer\Headers\Matrix.h>
#include <Engine\Headers\Text.h>
#include <Engine\Headers\Dirty.h>
#include <Engine\Headers\Delegate.h>
#include <Engine\Headers\Function.h>
#include <Logger\Headers\ILog.h>
#include <DirectXMath.h>
#include <sal.h>

namespace HyJynxRenderer
{
	//
	// Contains Scale, Position, and Rotation.
	// Fires events for when changes occur.
	// Optional override methods for Matrix calculations.
	//
	class Transform : public HyJynxUtilities::Dirty, public HyJynxLogger::ILog
	{
	protected:

		Matrix			_matrix;
		Vector4			_rotationQuat = Vector4( 0.0f, 0.0f, 0.0f, 1.0f );
		Vector3			_rotation = Vector3( 0.0f, 0.0f, 0.0f );
		Vector3			_position = Vector3( 0.0f, 0.0f, 0.0f );
		Vector3			_scale = Vector3( 1.0f, 1.0f, 1.0f );
		Vector3			_origin = Vector3( 0.0f, 0.0f, 0.0f );
		
		Transform*		_parent = nullptr;

		HyJynxCore::Delegate< Transform& >							_onChange;
		HyJynxCore::Function<void, Transform&, Matrix*>				_matrixOverride;

		bool _eventsActive;

		//
		// calculate the world-matrix with current values
		//
		void CalculateMatrix( );

	public:

		//
		// null ctor
		//
		Transform( );

		//
		// copy ctor
		//
		Transform( const Transform& );

		//
		// move ctor
		//
		Transform( const Transform&& );

		//
		// dtor
		//
		~Transform( );

		//
		// assignment operator
		//
		Transform& operator = ( _In_ const Transform& );

		//
		// comparison operator
		//
		bool operator == ( _In_ const Transform& );

		//
		// get the delegate managing on-change events
		// - Delegate< Transform& >*: on-change delegate
		//
		HyJynxCore::Delegate< Transform& >* GetOnChangeDelegate( );

		//
		// override the matrix calculation - pass nullptr to remove an override
		// - Function<void, Transform&, Matrix*>: callback overriding the default matrix calculation behavior
		//
		void SetMatrixCalculationOverride( _In_opt_ HyJynxCore::Function< void, Transform&, Matrix* > );

		//
		// reset the transform, position, rotation = 0, scale = 1
		//
		void Reset( );

		//
		// get whether events are currently active or not
		//
		bool EventsActive( ) const;

		//
		// toggle events from firing on/off
		// - bool: true - events will fire, false - events will not fire
		//
		void SetEventsActive( _In_ const bool );

		//
		// get the matrix this transform computes to
		//
		Matrix& GetMatrix( );

		//
		// get the position in 3D space
		//
		const Vector3& GetPosition( ) const;

		//
		// get the EULER rotation angles
		//
		const Vector3& GetRotation( ) const;

		//
		// get the quaternion orientation
		//
		const Vector4& GetOrientation( ) const;

		// 
		// get the scaling
		//
		const Vector3& GetScale( ) const;

		//
		// get the origin of what this transform is being applied to
		//
		const Vector3& GetOrigin( ) const;

		//
		// set all position values
		// - float: x position component
		// - float: y position component
		// - float: z position component
		//
		void SetPosition( _In_ const float, _In_ const float, _In_ const float );

		//
		// add translation 
		// - float: x position translation from current
		// - float: y position translation from current
		// - float: z position translation from current
		//
		void AddPosition( _In_ const float, _In_ const float, _In_ const float );

		//
		// set all position values, copied from another Vector3
		// - Vector3*: pointer to the vector you wish to set position to
		//
		void SetPosition( _In_ const Vector3& );

		//
		// set all rotation values (in EULER angles)
		// - float: x rotation component
		// - float: y rotation component
		// - float: z rotation component
		//
		void SetRotation( _In_ const float, _In_ const float, _In_ const float );

		//
		// set all rotation values, copied from another Vector3
		// - Vector3*: pointer to the vector you wish to set rotation to
		//
		void SetRotation( _In_ const Vector3& );

		//
		// add rotation to components
		// - float: x relative rotation
		// - float: y relative rotation
		// - float: z relative rotation
		//
		void AddRotation( _In_ const float, _In_ const float, _In_ const float );

		//
		// set all scaling values
		// - float: x scaling component
		// - float: y scaling component
		// - float: z scaling component
		//
		void SetScale( _In_ const float, _In_ const float, _In_ const float );

		//
		// set all scale values, copied from another Vector3
		// - Vector3*: pointer to the vector you wish to set scale to
		//
		void SetScale( _In_ const Vector3& );

		//
		// set a new origin point to affect whoever is listening to this transform
		//		note: its up to the listener to decide what to do with origin
		//		note: these values should be between 0-1, but you can go legally go past
		// -float: x origin component
		// -float: y origin component
		// -float: z origin component
		//
		void SetOrigin( _In_ const float, _In_ const float, _In_ const float );

		//
		// set all origin values, copied from another Vector3
		// - Vector3*: pointer to the vector you wish to set origin to
		//
		void SetOrigin( _In_ const Vector3& );

		//
		// get the actual left position, based from the given transform's X, origin, and given width
		// -Transform*: transform in question
		// -float: width of the transform which it represents
		//
		static float GetLeft( _In_ const Transform&, _In_ const float );

		//
		// get the actual top position, based from the given transform's Y, origin, and given height
		// -Transform*: transform in question
		// -float: height of the transform which it represents
		//
		static float GetTop( _In_ const Transform&, _In_ const float );

		//
		// get the actual right position, basied from the given transform's X, origin, and given width
		// - Transform*: transform in question
		// - float: width of the transform which it represents
		//
		static float GetRight( _In_ const Transform&, _In_ const float );

		//
		// get the actual bottom position, based from the given transform's Y, origin, and given height
		// - Transform*: transform in question
		// - float: height of the transform which it represents
		//
		static float GetBottom( _In_ const Transform&, _In_ const float );

		//
		// get the actual center position, based from the given Transform, origin, and width/height
		// - Transform* transform in question
		// - float: width of the transform which it represents
		// - float: height of the transform which it represents
		// - float: length of the transform which it represents
		//
		static Vector3 GetCenter( _In_ const Transform&, _In_ const float, _In_ const float, _In_ const float );

		//
		// get the actual front position, based from the given Transform, origin, and given length
		// - Transform*: transform in question
		// - float: length of the transform which it represents
		//
		static float GetFront( _In_ const Transform&, _In_ const float );

		//
		// get the actual back position, based from the given Transform, origin, and given length
		// - Transform*: transform in question
		// - float: length of the transform which it represents
		//
		static float GetBack( _In_ const Transform&, _In_ const float );

		//
		// overloaded index operator, valid inputs are:
		// (x, y, z, rotationX, rotationY, rotationZ, scaleX, scaleY, scaleZ, originX, originY, originZ)
		// - Text: name of the component you want
		//
		float operator [] ( _In_ const HyJynxCore::Text );

		//
		// get the current state of the Transform
		// - returns TransformState:
		//
		TransformState GetState( ) const;

		//
		// set the transform to a previous state, or state of another transform why not
		// - TransformState: the state of a transform
		//
		void ToState( _In_ const TransformState& );

		//
		// set the parent of this transform
		// - Transform*: parent Transformation
		//
		void SetParent( _In_opt_ Transform* = nullptr );

		//
		// get a pointer to the parent of this transform
		// - returns Transform*: parent transformation object
		//
		Transform* GetParent( );

#pragma region ParentalGetters

		//
		// helper method to get the combined scale of all parents
		// - returns Vector3: combined scaling of all parent scales
		//
		Vector3 GetCombinedScale( ) const;

		//
		// helper method to get the combined position of all parents
		// - returns Vector3: combined position of all parents
		//
		Vector3 GetCombinedPosition( ) const;

#pragma endregion
	};
}

#endif // HYJYNXRENDERER_TRANSFORM_H