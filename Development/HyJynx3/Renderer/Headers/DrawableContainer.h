/**
 * DrawableContainer.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_DRAWABLECONTAINER_H
#define HYJYNXRENDERER_DRAWABLECONTAINER_H

#include <Renderer/Headers/Transform.h>
#include <Engine/Headers/DynamicCollection.h>
#include <sal.h>
#include <functional>

namespace HyJynxRenderer
{
	class IDrawable;

	//
	// Helper object which simply wraps a list of drawables, and provides some
	// simple API to add/remove/sort/etc... the contained drawables
	//
	class DrawableContainer
	{
	protected:

		Transform		_transform;
		HyJynxCollections::DynamicCollection< IDrawable* > _drawableList;

	public:


#pragma region Init

		//
		// null ctor
		//
		DrawableContainer( );

		//
		// copy ctor
		//
		DrawableContainer( _In_ const DrawableContainer& );

		//
		// move ctor
		//
		DrawableContainer( _In_ const DrawableContainer&& );

		//
		// dtor
		//
		~DrawableContainer( );

#pragma endregion

#pragma region API

		//
		// Add a drawable to this container
		// - IDrawable*: pointer to the drawable object
		//
		void AddChild( _In_ IDrawable* const );

		//
		// Add all drawables from another container to this one
		// - DrawbleContainer*: container of drawables
		//
		void AddChild( _In_ DrawableContainer* const );

		//
		// Remove a child from the container
		// - Drawable*: pointer to the drawable
		//
		void RemoveChild( _In_ IDrawable* const );

		//
		// iterate the list of drawables
		// - function<void(IDrawable*)>: function to iterate drawable
		//
		void ForEach( std::function< void( _In_ IDrawable* ) > );

		//
		// get the on-screen transformation object
		// - returns Transform*: Transformation container
		//
		Transform* const GetTransform( );

#pragma endregion

#pragma region Operators

		DrawableContainer& operator += ( _In_ IDrawable* );
		DrawableContainer& operator += ( _In_ DrawableContainer* );
		DrawableContainer& operator -= ( _In_ IDrawable* );

#pragma endregion
	};
};

#endif // HYJYNXRENDERER_DRAWABLECONTAINER_H 