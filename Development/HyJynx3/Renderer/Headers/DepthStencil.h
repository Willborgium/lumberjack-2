/**
 * DepthStencil.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_DEPTHSTENCIL_H
#define HYJYNXRENDERER_DEPTHSTENCIL_H

#pragma warning(disable: 4005)
#include <D3D11.h>
#pragma warning(default: 4005)

namespace HyJynxRenderer
{
	//
	// we wrap the depth stencil target in this object, with some extra
	// features so we can render it to screen with ease.
	//
	class DepthStencil sealed
	{
	private:

		ID3D11DepthStencilView* _view = nullptr;
		ID3D11Texture2D* _texture = nullptr;
		ID3D11DepthStencilState * _state = nullptr;

	public:

		//
		// null ctor
		//
		DepthStencil( );

		//
		// default ctor
		//
		DepthStencil( _In_ ID3D11DepthStencilView* const, 
			_In_ ID3D11Texture2D* const, _In_ ID3D11DepthStencilState* const );

		//
		// copy ctor
		//
		DepthStencil( _In_ const DepthStencil& );

		//
		// move ctor
		//
		DepthStencil( _In_ const DepthStencil&& );

		//
		// dtor
		//
		~DepthStencil( );

		//
		// release directX components
		//
		void Release( );

		//
		// gets the directX stencil view object (what we draw into)
		// - returns ID3D11DepthStencilView*: pointer to the depth buffer
		//
		ID3D11DepthStencilView* const GetView( );

		//
		// gets the directX depth stencil texture (what we can phyically look at)
		// - returns ID3D11Texture2D*: pointer to the texture used to view the depth stencil
		//
		ID3D11Texture2D* const GetTexture( );

		//
		// get the output merger's depth stencil state
		// - returns ID3D11DepthStencilState*: dx11 depth stencil state
		//
		ID3D11DepthStencilState* const GetState( );
	};
}

#endif // HYJYNXRENDERER_DEPTHSTENCIL_H