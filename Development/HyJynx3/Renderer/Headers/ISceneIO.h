/**
 * ISceneIO.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_ISCENEIO_H
#define HYJYNXRENDERER_ISCENEIO_H

#include <Renderer/Headers/ISceneActor.h>
#include <Renderer/Headers/Window.h>
#include <Engine\Headers\Text.h>
#include <Engine\Headers\Function.h>
#include <sal.h>

namespace HyJynxRenderer
{
	//
	// API all types of scene file IO must implement
	//
	class ISceneIO abstract
	{
	public:

		//
		// Initialize the scene from a file
		// - Text: filepath to the scene file
		// - function<void, Window*>: onSuccess callback
		// - function<void, Text>: onFail callback with argued text reason
		//
		virtual void InitializeFromFile( _In_ HyJynxCore::Text,
			_In_ HyJynxCore::Function< void, Window* >,
			_In_ HyJynxCore::Function< void, HyJynxCore::Text > ) = 0;

		//
		// Initialize the scene from data
		// - char*: loaded data file
		// - function<void, Window*)>: onSuccess callback with the loaded window argued
		// - function<void, Text)>: onFail callback with argued text reason
		//
		virtual void InitializeFromData( _In_ char* const,
			_In_ const UInt,
			_In_ HyJynxCore::Function< void, Window* >,
			_In_ HyJynxCore::Function< void, HyJynxCore::Text > ) = 0;

		//
		// Saves the scene to file
		// - Text: filepath where the scene will save to (will overwrite)
		// - Window*: pointer to a window you wish to serialize into a scene
		// - function<void>: onSuccess callback
		// - function<void, Text)>: onFail callback with argued text reason
		//
		virtual void SaveToFile( _In_ HyJynxCore::Text,
			_In_ Window* const,
			_In_ HyJynxCore::Function< void >,
			_In_ HyJynxCore::Function< void, HyJynxCore::Text > ) = 0;
	};
}

#endif // HYJYNXRENDERER_ISCENEIO_H