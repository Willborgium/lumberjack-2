/**
 * DrawableInclude.h
 * (c) 2014 All Rights Reserved
 */

//
// This is a ehlper file to include all drawables for Front-End gameplay
//

#ifndef HYJYNXRENDERER_DRAWABLES_H
#define HYJYNXRENDERER_DRAWABLES_H

#include <Renderer/Headers/TextureManager.h>
#include <Renderer/Headers/DrawableQuad2D.h>
#include <Renderer/Headers/DrawableCube3D.h>
#include <Renderer/Headers/DrawableText2D.h>
#include <Renderer/Headers/EnvironmentController.h>

#endif // HYJYNXRENDERER_DRAWABLES_H