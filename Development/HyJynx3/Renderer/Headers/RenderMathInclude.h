/**
 * RenderMathInclude.h
 * (c) 2014 All Rights Reserved
 */

//
// This is a helper file to include all of HyJynxRenderer Math Primitives
//

#ifndef HYJYNXRENDERER_RENDERMATHINCLUDE_H
#define HYJYNXRENDERER_RENDERMATHINCLUDE_H

#include <Renderer/Headers/Point2.h>
#include <Renderer/Headers/Point3.h>
#include <Renderer/Headers/Vector2.h>
#include <Renderer/Headers/Vector3.h>
#include <Renderer/Headers/Vector4.h>
#include <Renderer/Headers/Matrix.h>
#include <Renderer/Headers/Ray.h>
#include <Renderer/Headers/RenderMath.h>
#include <Renderer/Headers/ITween.h>
#include <Renderer/Headers/TweenLinear.h>
#include <Renderer/Headers/TweenEase.h>
#include <Renderer/Headers/Animator.h>

#endif // HYJYNXRENDERER_RENDERMATHINCLUDE_H