/**
 * IDrawable.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_IDRAWABLE_H
#define HYJYNXRENDERER_IDRAWABLE_H

#include <Renderer\Headers\Camera.h>
#include <Renderer\Headers\Context.h>
#include <Renderer\Headers\Transform.h>
#include <Renderer\Headers\ViewFrustum.h>
#include <Renderer\Headers\Model.h>
#include <Renderer\Headers\CollisionDescription.h>
#include <Engine\Headers\HighPrecisionTimer.h>
#include <Engine\Headers\Function.h>
#include <Logger\Headers\ILog.h>

namespace HyJynxRenderer
{
	typedef HyJynxCore::HighPrecisionTimer RenderTime;

	//
	// Describes what rasterizer(s) a drawable will render with.
	// 
	enum class DrawableRenderMode : char
	{
		DRAWABLE_RENDER_SOLID,
		DRAWABLE_RENDER_WIREFRAME,
		DRAWABLE_RENDER_SOLID_WIREFRAME
	};

	//
	// This Interface is the API Renderer will use to automate it's procedures
	// We contain the main model and material which all derived drawables will populate
	//
	class IDrawable abstract : public HyJynxLogger::ILog
	{

	protected:

		//
		// default ctor - active = true, id = infinity
		//
		IDrawable( ) { }

		//
		// copy ctor
		//
		IDrawable( _In_ const IDrawable& ) { }

		//
		// move ctor
		//
		IDrawable( _In_ const IDrawable&& ) { }

	public:

		//
		// good ol' dtor
		//
		virtual ~IDrawable( ) { }


		//
		// drawables are constructed with the data they need to load,
		// this kicks off the sequence to load and and register the mesh buffers
		// - function< void(void) >: lambda to call when loading has completed
		//					(this is here for async loading down the road)
		//
		virtual void LoadModel( _In_ HyJynxCore::Function< void > ) = 0;

		//
		// this should be called after a drawable is constructed, and LoadModel() called
		//
		virtual void GenerateBounding( ) = 0;

		//
		// per-cycle update call
		// Context*: dx API container
		// const RenderTime*: reference to Renderer's per cycle run timer
		//
		virtual void Update( _In_ Context*, _In_ const RenderTime ) = 0;

		//
		// when we're in development, this API will help us draw debugging information to the screen
		// -Context*: reference to the context we're drawing to
		//
		virtual void RenderDebug( _In_ Context* ) = 0;

		//
		// Get the model containing all of the meshes in this drawable
		// Derived children wil be able to override this if they so choose
		//
		virtual Model* GetModel( ) = 0;

		//
		// get the container holding all collision descriptions
		// - returns CollisionDescription: container with collidable sphere, abb, obb1, obb2
		//
		virtual CollisionDescription* GetCollisionDescription( ) = 0;

		//
		// determine if this drawable is active or not
		// return bool: true - will draw, false - renderer will skip
		//
		virtual bool GetActive( ) const = 0;

		//
		// set whether this drawable will be active in Renderer's magical eyes
		// -bool: true - will draw, false - renderer will skip
		//
		virtual void SetActive( _In_ bool ) = 0;

		//
		// Get a transform reference used for 3D space manipulation
		// -return: Transform*
		//
		virtual Transform* GetTransform( ) = 0;

		//
		// get the ID of this drawable (defaults to infinity when no ID is set)
		// - returns int: drawable identifier
		//
		virtual int GetDrawableID( ) const = 0;

		//
		// set an identifier to this drawable
		// - int: drawable identifier
		//
		virtual void SetDrawableID( _In_ const int ) = 0;

		//
		// sets how the contained meshes will rasterize
		// - DrawableRenderMode: rasterizer modes
		//
		virtual void SetRenderMode( _In_ const DrawableRenderMode ) = 0;

		//
		// gets how the contained meshes will rasterize
		// returns DrawableRenderMode: rasterizer modes
		//
		virtual DrawableRenderMode GetRenderMode( ) const = 0;
	};

}

#endif // HYJYNXRENDERER_IDRAWABLE_H