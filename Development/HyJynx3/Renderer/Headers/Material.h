/**
 * Material.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_MATERIAL_H
#define HYJYNXRENDERER_MATERIAL_H

#include <Renderer/Headers/Shader.h>
#include <Renderer/Headers/MaterialLibrary.h>
#include <Renderer/Headers/Context.h>
#include <Logger/Headers/ILog.h>
#include <sal.h>

namespace HyJynxRenderer
{
	class IDrawable;
	class Mesh;
	class Texture2D;
	class Texture3D;
	struct CBObject;

	//
	// A material is an object containing all characteristics 
	// of how a drawable is put to screen.
	// Diffuse, Normals, and Sky-Box maps are global to all shaders, hence
	// why these textures are found here in the parent Material
	//
	class Material abstract : public HyJynxLogger::ILog
	{
	protected:

		Shader*					_shader = nullptr;

		Texture2D*				_diffuseTexture = nullptr;
		Texture2D*				_normalTexture = nullptr;
		Texture3D*				_environmentTexture = nullptr;

		int						_materialID = MaterialLibrary::Material_Unknown;

	public:

		//
		// null ctor
		//
		Material( );

		//
		// default ctor - ID defined
		// - int: ID of the derived Material
		// - VertexDescription: description of the verticies the material expects
		//
		Material( _In_ int );


		//
		// copy ctor
		//
		Material( _In_ const Material& );

		//
		// move ctor
		//
		Material( _In_ const Material&& );

		//
		// mr. dtor
		//
		virtual ~Material( );

		//
		// get any loggers this material composes
		//
		virtual void GetChildrenLoggers( _In_ HyJynxCollections::DynamicCollection< HyJynxLogger::ILog* >* const ) override;

		//
		// determine if the shader is in a renderable state
		// - returns bool: true - ready to draw, false - 'tis not.
		//
		virtual bool IsValid( ) const;

		//
		// get the identifier representing this material
		// - returns int: identifier uniqute to this material type
		//
		int GetID( ) const;

		//
		// get the run-time shader this material is using
		// - returns Shader: shader API container
		//
		Shader* const GetShader( );

		//
		// update and set buffers to the shader which all following objects will use
		// - Context*: dx11 command API container
		//
		virtual void BatchBind( _In_ Context* const ) = 0;

		//
		// bind the argued drawable containers to the argued constant buffer
		// - IDrawable*: pointer to the drawable in need of binding
		// - Mesh*: pointer to the current mesh to be rendered 
		// - CBObject*: per-object constant buffer who's data needs to be updated
		//
		virtual void ObjectBind( _In_ IDrawable* const, _In_ Mesh* const, _In_ CBObject* const ) = 0;

		//
		// set the texture used to display diffuse colors,
		// argue nullptr/nothing to remove
		// - Texture2D*: diffuse texture
		//
		virtual void SetDiffuseTexture( _In_opt_ Texture2D* = nullptr );

		//
		// get the texture used to display diffuse
		// - returns Texture2D*: texture used for diffuse
		//
		virtual Texture2D* GetDiffuseTexture( );

		//
		// set the texture used for normal mapping,
		// argue nullptr/nothing to remove
		// - Texture2D*: normal map texture
		//
		virtual void SetNormalTexture( _In_opt_ Texture2D* = nullptr );

		//
		// get the texture used for normal mapping
		// returns Texture2D*: normal map texture
		//
		virtual Texture2D* GetNormalTexture( );
		
		//
		// set the texture used for environment mapping
		// argue nullptr/nothing to remove
		// - Texture3D*: environment map texture
		//
		virtual void SetEnvironmentTexture( _In_opt_ Texture3D* = nullptr );

		//
		// get the texture used for the environment cube-map
		// - returns Texture3D*: environment cube map
		//
		virtual Texture3D* GetEnvironmentTexture( );

	};
}

#endif // HYJYNXRENDERER_MATERIAL_H