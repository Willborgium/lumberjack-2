/**
 * TweenEase.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_TWEENEASE_H
#define HYJYNXRENDERER_TWEENEASE_H

#include <Renderer/Headers/ITween.h>
#include <sal.h>

namespace HyJynxRenderer
{

	//
	// Easing interpolation, In/Out Easing based on the sign of the power
	// eg: TweenEase( 4 ) - eases in, TweenEase( -4 ) - eases out
	//
	class TweenEase : public ITween
	{
	protected:

		double _power = 2;

	public:

		//
		// null ctor
		//
		TweenEase( );

		//
		// default ctor
		// - double: power of the easing
		//
		TweenEase( _In_ const double );

		//
		// copy ctor
		//
		TweenEase( _In_ const TweenEase& );

		//
		// move ctor
		//
		TweenEase( _In_ const TweenEase&& );

		//
		// dtor
		//
		~TweenEase( );

		//
		// per-cycle update
		// - double: total time since the start of the tween
		// - double: delta time since the last update call
		// - double: maximum duration
		//
		virtual void Update( _In_ const double, _In_ const double, _In_ const double ) override;

	};
};

#endif // HYJYNXRENDERER_TWEENEASE_H 