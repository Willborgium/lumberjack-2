/**
 * TweenLinear.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_TWEENLINEAR_H
#define HYJYNXRENDERER_TWEENLINEAR_H

#include <Renderer/Headers/ITween.h>
#include <sal.h>

namespace HyJynxRenderer
{

	//
	// basic linear interpolation between start and end
	//
	class TweenLinear : public ITween
	{
	protected:

	public:

		//
		// null ctor
		//
		TweenLinear( );

		//
		// copy ctor
		//
		TweenLinear( _In_ const TweenLinear& );

		//
		// move ctor
		//
		TweenLinear( _In_ const TweenLinear&& );

		//
		// dtor
		//
		~TweenLinear( );

		//
		// per-cycle update
		// - double: total time since the start of the tween
		// - double: delta time since the last update call
		// - double: maximum duration
		//
		virtual void Update( _In_ const double, _In_ const double, _In_ const double ) override;

	};
};

#endif // HYJYNXRENDERER_TWEENLINEAR_H 