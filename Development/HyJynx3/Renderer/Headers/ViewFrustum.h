/**
 * ViewFrustum.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_VIEWFRUSTUM_H
#define HYJYNXRENDERER_VIEWFRUSTUM_H

#include <Renderer\Headers\Ray.h>
#include <Renderer\Headers\Matrix.h>
#include <Renderer\Headers\Vector3.h>
#include <Renderer\Headers\Vector4.h>
#include <Renderer\Headers\OrientedBoundingBox.h>
#include <Logger\Headers\ILog.h>
#include <Engine\Headers\Dirty.h>
#include <DirectXCollision.h>
#include <sal.h>

#pragma warning(disable: 4005)
#include <D3D11.h>
#pragma warning(default: 4005)

namespace HyJynxRenderer
{
	//
	// Implements all API needed to test collidable objects to a camera's frustum
	//
	class ViewFrustum sealed : public HyJynxLogger::ILog, public HyJynxUtilities::Dirty
	{
	private:

		Matrix _projectionMatrix;
		DirectX::BoundingFrustum _frustum;

		float _fieldOfView = 0.4f;
		float _nearClip = 1.0f;
		float _farClip = 1000.0f;
		float _aspectRatio = DirectX::XM_PIDIV4;

	public:

		//
		// called when changes to projection are made,
		// to clean the frustum and projection matrix
		//
		void CleanFrustum( );

		//
		// null ctor
		//
		ViewFrustum();

		//
		// copy ctor
		//
		ViewFrustum( _In_ const ViewFrustum& );

		//
		// move ctor
		//
		ViewFrustum( _In_ const ViewFrustum&& );

		//
		// dtor
		//
		~ViewFrustum();

		//
		// update (if neccessary) and get the projection matrix
		// - Matrix: updated projection matrix
		//
		Matrix& GetProjectionMatrix( );

		//
		// update (if neccessary) and set the argued pointer to the value
		// - XMMATRIX**: pointer to the location of where the matrix will be set
		//
		DirectX::XMMATRIX GetProjectionMatrixXM( );

		//
		// update the frustum with orientation and translation
		// - Vector4: quaternion orientation
		// - Vector3: positional translation
		//
		void Update( _In_ const Vector4&, _In_ const Vector3& );

		//
		// determine if the given bounding box is inside the frustum
		// - BoundingBox: the directX bounding box object
		// - ContainmentType: optionally get what type of containment the intersection was.
		//						(disjoint: nothing, intersects: it collides, contains: completely inside)
		// - return bool: true: the object is inside of the frustum, false: it is culled
		//
		bool IsInside( _In_ DirectX::BoundingBox&, _Out_opt_ DirectX::ContainmentType* = nullptr );

		//
		// determine if the given oriented bounding box tis inside of the frustum
		// - BoundingOrientedBox: the directX OBB object
		// - ContainmentType: optionally get what type of containment the intersection was.
		//						(disjoint: nothing, intersects: it collides, contains: completely inside)
		// - return bool: true: the object is inside of the frustum, false: it is culled
		//
		bool IsInside( _In_ DirectX::BoundingOrientedBox&, _Out_opt_ DirectX::ContainmentType* = nullptr );

		//
		// determine if the given bounding sphere tis inside of the frustum
		// - BoundingSphere: the directX sphere object
		// - ContainmentType: optionally get what type of containment the intersection was.
		//						(disjoint: nothing, intersects: it collides, contains: completely inside)
		// - return bool: true: the object is inside of the frustum, false: it is culled
		//
		bool IsInside( _In_ DirectX::BoundingSphere&, _Out_opt_ DirectX::ContainmentType* = nullptr );

		//
		// determine if the argued frustum collides with this frustum
		// - ViewFrustm*: the frustum to test against this
		// - ContainmentType: optionally get what type of containment the intersection was.
		//						(disjoint: nothing, intersects: it collides, contains: completely inside)
		// - return bool: true: the object is inside of the frustum, false: it is culled
		//
		bool IsInside( _In_ ViewFrustum&, _Out_opt_ DirectX::ContainmentType* = nullptr );

		//
		// determine if the given ray intersects the frustum
		// - Ray: the subject ray we're testing
		// - float*: if collision occured, this will equal the distance from origin to intersection
		// - return bool: true: the ray intersects the frustum, false: it does not
		//
		bool IsInside( _In_ Ray&, _Out_opt_ float* = nullptr );

		//
		// determine if the given OBB intersects the frustum
		// - OrientedBoundingBox*: the subject OBB we're testing
		// - return bool: true: the OBB intersects the frustum, false: it does not
		//
		bool IsInside( _In_ OrientedBoundingBox& );

		//
		// get the pointer to the dx frustum
		// - returns BoundingFrustum*: the DirectX BoundingFrustum object
		//
		DirectX::BoundingFrustum& GetFrustum( );

		//
		// get the near-clipping range (default is 1)
		// - return float: clipping range closest to camera
		//
		float GetNearClipRange( ) const;

		//
		// set the near- clipping range, this cannot be negative
		// - float: near-clipping range
		//
		void SetNearClipRange( _In_ const float );

		//
		// get the far-clipping range (Default is 1000)
		// - return float: clipping range farthest from camera
		//
		float GetFarClipRange( ) const;

		//
		// set the far-clipping range, this cannot be negative
		// -float: far-clipping range
		//
		void SetFarClipRange( _In_ const float );

		//
		// get the aspect ratio of this camera, defaults to PI/4
		// -return float: aspect ratio
		//
		float GetAspectRatio( ) const;

		//
		// set the aspect ratio of this camera, will trigger projection matrix re-calculation
		// - float: aspect ratio
		//
		void SetAspectRatio( _In_ const float );

		//
		// get the field of view, defaults to 0.4 (converts to radians during projection calculation)
		// - return float: field of view
		//
		float GetFieldOfView( ) const;

		//
		// set the field of view, will convert to radians during projection calculation
		// - float: field of view
		//
		void SetFieldOfView( _In_ const float );
	};
}

#endif // HYJYNXRENDERER_VIEWFRUSTUM_H