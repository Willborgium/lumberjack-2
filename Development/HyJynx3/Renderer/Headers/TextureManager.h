/**
 * TextureManager.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_TEXTUREMANAGER_H
#define HYJYNXRENDERER_TEXTUREMANAGER_H

#include <Renderer/Headers/Texture.h>
#include <Renderer/Headers/Texture1D.h>
#include <Renderer/Headers/Texture2D.h>
#include <Renderer/Headers/Texture3D.h>
#include <Engine/Headers/Singleton.h>
#include <Engine/Headers/Text.h>
#include <Engine/Headers/Dictionary.h>
#include <Engine/Headers/DynamicCollection.h>
#include <Engine/Headers/Function.h>
#include <Logger/Headers/ILog.h>

#include <sal.h>
#include <stdint.h>

#pragma warning(disable: 4005)
#include <D3D11.h>
#include <D2D1.h>
#include <wincodec.h>
#include <wincodecsdk.h>
#pragma warning(default: 4005)


namespace HyJynxRenderer
{
	//
	// describes where a texture can load to, cpu, gpu, or both
	//
	enum class TextureLoadTarget : char
	{
		TEXTURE_LOAD_GPU,
		TEXTURE_LOAD_CPU,
		TEXTURE_LOAD_GPU_CPU
	};

	//
	// describes the types of textures we can load
	//
	enum class TextureType : char
	{
		TEXTURE_TYPE_UNKNOWN,
		TEXTURE_TYPE_1D,
		TEXTURE_TYPE_2D,
		TEXTURE_TYPE_3D
	};
	
	//
	// argued with the requestTexture error callback
	//
	enum class TextureRequestError : char
	{
		TEXTURE_ERROR_NONE,
		TEXTURE_ERROR_UNKNOWN,
		TEXTURE_ERROR_BAD_FILEPATH,
		TEXTURE_ERROR_FACTORY_UNINITIALIZED,
		TEXTURE_ERROR_FRAME_FAIL,
		TEXTURE_ERROR_UNSUPPORTED_PIXEL_TYPE,
		TEXTURE_ERROR_UNSOPPORTED_FORMAT,
		TEXTURE_ERROR_DIMENSION_INQUIRE_FAILED,
		TEXTURE_ERROR_ZERO_DIMENSIONS,
		TEXTURE_ERROR_COPY_PIXEL_FAIL,
		TEXTURE_ERROR_RESIZE_FAIL,
		TEXTURE_ERROR_CONVERSION_FAIL,
		TEXTURE_ERROR_PALLETE_FAIL,
		TEXTURE_ERROR_GPU_LOAD_FAIL
	};

	// ==================================================================
	//			Init Structures
	// ==================================================================

	//
	// contains all possible Texture initialize values
	// - Filepath is always required.
	//
	class TextureInit
	{
	public:

		// Path to the texture
		HyJynxCore::Text Filepath = "";

		// Decrypted image data
		char* ImageData = nullptr;
		size_t ImageDataSizeInBytes = 0;
		// ---------------------

		// Load the texture n pixels wide, leave -1 for native size
		int Width = -1;

		// Load the texture n pixels high, leave -1 for native size
		int Height = -1;

		// Toggle where you want the texture loaded to, (defaults to GPU)
		TextureLoadTarget TextureLoadTarget = TextureLoadTarget::TEXTURE_LOAD_GPU;

		// Toggle this to load the texture async or blocking (Defaults async)
		bool LoadAsync = true;

		// Auto-generate MipMaps (only if hardware supports it)
		bool AutoMipMap = true;

		// null/default ctor
		TextureInit( )
		{ }

		// optional quick-use ctor (from filepath)
		TextureInit( _In_ HyJynxCore::Text path )
			: Filepath( path )
		{ }

		// optional quick-use ctor (from memory)
		TextureInit( _In_ HyJynxCore::Text path, _In_ const char* const data, _In_ const size_t size )
			: Filepath( path ),
			ImageData( const_cast< char* const >( data ) ),
			ImageDataSizeInBytes( size )
		{ }

		// copy ctor
		TextureInit( _In_ const TextureInit& other )
			: Filepath( other.Filepath ),
			Width( other.Width ),
			Height( other.Height ),
			TextureLoadTarget( other.TextureLoadTarget ),
			LoadAsync( other.LoadAsync ),
			AutoMipMap( other.AutoMipMap )
		{ }

		// move ctor
		TextureInit( _In_ const TextureInit&& other )
			: Filepath( other.Filepath ),
			Width( other.Width ),
			Height( other.Height ),
			TextureLoadTarget( other.TextureLoadTarget ),
			LoadAsync( other.LoadAsync ),
			AutoMipMap( other.AutoMipMap )
		{ }

		// dtor
		~TextureInit( )
		{
			ImageData = nullptr;
			Filepath = "";
		}

		// equality comparison operator
		bool operator == ( _In_ const TextureInit& other )
		{
			return other.Filepath == Filepath
				&& other.AutoMipMap == AutoMipMap
				&& other.Width == Width
				&& other.Height == Height
				&& other.TextureLoadTarget == TextureLoadTarget;
		}
	};

	//
	// all texture types will begin the load process the same way,
	// this structure wraps all data generated from that process
	//
	struct PreInitOutput sealed
	{
		TextureRequestError Status = TextureRequestError::TEXTURE_ERROR_NONE;

		UInt NativeWidth = 0;
		UInt NativeHeight = 0;
		UInt LoadWidth = 0;
		UInt LoadHeight = 0;

		IWICBitmapFrameDecode* Frame = nullptr;
		DXGI_FORMAT DxgiFormat = DXGI_FORMAT::DXGI_FORMAT_UNKNOWN;
		WICPixelFormatGUID WICFormat/* = ??? */;

		size_t PixelSizeInBytes = 0;
		size_t RowPitch = 0;
		size_t ImageSizeInBytes = 0;

		// dtor
		~PreInitOutput( )
		{
			if ( Frame != nullptr )
			{
				Frame->Release( );
			}
		}
	};
	// ==================================================================



	//
	// wrapper for callers requesting a texture load
	// this is used to queue simutaneous requesters of the same texture
	//
	template <typename TexType>
	class TextureLoader sealed
	{
	public:

		TextureInit* InitValues = nullptr;
		HyJynxCore::Function< void, TexType* > OnSuccessCallback;
		HyJynxCore::Function< void, TextureRequestError > OnFailCallback;

		// null ctor
		TextureLoader( )
		{ }

		// default ctor
		TextureLoader( _In_ TextureInit* init,
			_In_ HyJynxCore::Function< void, TexType* > success,
			_In_ HyJynxCore::Function< void, TextureRequestError > fail )
			: InitValues( init ),
			OnSuccessCallback( success ),
			OnFailCallback( fail )
		{ }

		// dtor
		~TextureLoader( )
		{
			InitValues = nullptr;
			OnSuccessCallback.ClearCapture( );
			OnFailCallback.ClearCapture( );
		}

		// comparison
		bool operator == ( _In_ const TextureLoader& other )
		{
			return InitValues == other.InitValues;
		}
	};
	
	// ==================================================================


	//
	// wraps a single loaded texture, all texture-types will wrap a pointer to one of these
	//
	struct LoadedTexture sealed
	{
		HyJynxCore::Text				Path;
		ID3D11ShaderResourceView*		ResourceView;
		ID3D11Resource*					Texture;
		uint8_t*						CpuMemory;
		UInt							SizeInBytes;
		UInt							NativeWidth;
		UInt							NativeHeight;
		UInt							NativeDepth;
		UInt							LoadedWidth;
		UInt							LoadedHeight;
		UInt							LoadedDepth;
		TextureType						Type;
	};

	typedef HyJynxCollections::KeyValuePair< HyJynxCollections::KeyValuePair< TextureType, TextureInit* >, LoadedTexture > TexturePair;
	typedef HyJynxCollections::Dictionary < HyJynxCollections::KeyValuePair< TextureType, TextureInit* >, LoadedTexture > TextureDictionary;
	typedef HyJynxCollections::Dictionary < TextureInit*, HyJynxCollections::DynamicCollection < TextureLoader< Texture1D >* > > Texture1DLoadQueue;
	typedef HyJynxCollections::Dictionary < TextureInit*, HyJynxCollections::DynamicCollection < TextureLoader< Texture2D >* > > Texture2DLoadQueue;
	typedef HyJynxCollections::Dictionary < TextureInit*, HyJynxCollections::DynamicCollection < TextureLoader< Texture3D >* > > Texture3DLoadQueue;

	//
	// Implements the API for loading and retrieving all texture types.
	// This uses the Windows Imaging Compoent (WIC) - A complete refactor will be needed for additional platforms.
	// Some additional features include:
	//	- asnyc or blocking togglable
	//	- target cpu or gpu memory togglable
	//  - queue simutaneous requests of the same texture
	//	- automatic conversion of unsupported texture types
	//  - automatic and togglable resize of textures
	//	- automatic and togglable mip-map generation (if hardware supports)
	//
	class TextureManager sealed : public HyJynxDesign::Singleton< TextureManager >, public HyJynxLogger::ILog
	{
		friend class HyJynxDesign::Singleton < TextureManager >;

	private:

		//
		// Provides static memory and API to TextureManager - Conversion tables/API
		// to and from WIC factory pixel formats, and DXGI formats
		//
		static class PixelTypeConversionTable abstract sealed
		{
			friend class TextureManager;

		private:

			// un-implemented
			PixelTypeConversionTable( ) = delete;
			PixelTypeConversionTable( _In_ const PixelTypeConversionTable& ) = delete;
			PixelTypeConversionTable( _In_ const PixelTypeConversionTable&& ) = delete;
			PixelTypeConversionTable& operator = ( _In_ const PixelTypeConversionTable& ) = delete;

			//
			// DX format type, and its associated WIC format type
			//
			struct WICTranslate
			{	
				GUID			WIC;
				DXGI_FORMAT     Format;
			};
			static WICTranslate _WICFormats[ ];

			//
			// WIC formats to WIC formats which WE support
			//
			struct WICConvert
			{
				GUID	Source;
				GUID	Target;
			};
			static WICConvert _WICConvertTable[ ];

		public:

			//
			// Convert from WIC decoded pixel type, to DXGI format
			// - GUID: image pixel type from WIC decoder factory
			//
			static DXGI_FORMAT WICToDXGI( _In_ const GUID& guid );

			//
			// Determine how many bits per pixel the decoder factory determines for the given GUID
			// - IWICImagingFactory*: pointer to the TextureManager's Imaging Factory
			// - GUID: decoder pixel type
			// - returns size_t: size in bits of an individual pixel
			//
			static size_t WICBitsPerPixel( _In_ IWICImagingFactory* const factory, _In_ const GUID& targetGuid );
			
		};

		// Dictionary of all loaded and retained textures
		TextureDictionary	_textureList;

		// Queue of simutaneous requests of 1D textures
		Texture1DLoadQueue	_1DQueue;

		// Queue of simutaneous requests of 2D textures
		Texture2DLoadQueue	_2DQueue;

		// Queue of simutaneous requests of 3D textures
		Texture3DLoadQueue	_3DQueue;

		// WIC Imaging Factory
		IWICImagingFactory*	_factory = nullptr;

		// Maximum texture size supported dynamic from directX feature level
		UInt				_maxTextureSize = 0;

		// default ctor
		TextureManager( );

		// un-implmented
		TextureManager( _In_ const TextureManager& ) = delete;
		TextureManager( _In_ const TextureManager&& ) = delete;
		TextureManager& operator = ( _In_ const TextureManager& ) = delete;

		//
		// release all textures from memory
		//
		void ReleaseAll( );

		//
		// Add a requester to the 1D queue
		// - TextureInit: intialization and key comparison parameters
		// - function<void(Texture1D)>: onSuccess callback
		// - function<void(TextureRequestError)>: onFail callback
		//
		void AddTo1DQueue( _In_ TextureInit* const,
			HyJynxCore::Function< void, Texture1D* >,
			HyJynxCore::Function< void, TextureRequestError > );

		//
		// Add a requester to the 2D queue
		// - TextureInit: intialization and key comparison parameters
		// - function<void(Texture2D)>: onSuccess callback
		// - function<void(TextureRequestError)>: onFail callback
		//
		void AddTo2DQueue( _In_ TextureInit* const,
			HyJynxCore::Function< void, Texture2D* >,
			HyJynxCore::Function< void, TextureRequestError > );

		//
		// Add a requester to the 3D queue
		// - TextureInit: intialization and key comparison parameters
		// - function<void(Texture3D)>: onSuccess callback
		// - function<void(TextureRequestError)>: onFail callback
		//
		void AddTo3DQueue( _In_ TextureInit* const,
			HyJynxCore::Function< void, Texture3D* >,
			HyJynxCore::Function< void, TextureRequestError > );
		
		//
		// retrieve a 1D texture, if it exists
		// - Text: filepath to the 1D texture
		//
		Texture1D* GetTexture1D( _In_ const TextureInit* const );

		//
		// retrieve a 2D texture, if it exists
		// - Text: filepath to the 2D texture
		//
		Texture2D* GetTexture2D( _In_ const TextureInit* const );

		//
		// retrieve a 3D texture, if it exists
		// - Text: filepath to the 3D texture
		//
		Texture3D* GetTexture3D( _In_ const TextureInit* const );

		//
		// Get the bitmap frame decoder of a texture file
		// - Text: filepath to the texture in question
		// - IWICBitmapFrameDecode*: Frame decoder, or nullptr
		//
		IWICBitmapFrameDecode* GetTextureFrameFromFile( _In_ const HyJynxCore::Text& );

		//
		// Get the bitmap frame decoder of the argued decrypted image data
		// - char*: image data, decrypted from Assets
		// - size_t: size of the image image data in bytes
		// - IWICBitmapFrameDecode*: Frame decoder, or nullptr
		//
		IWICBitmapFrameDecode* GetTextureFrameFromData( _In_bytecount_( size ) char* const, _In_ size_t size );

		//
		// determine the DXGI pixel format of the argued frame decode
		// - IWICBitmapFrameDecode*: WIC frame decode
		// - size_t*: output size of bit depth
		// - WICPixelFormatGUID*: output WIC pixel format
		// - returns DXGI_FORMAT: pixel format of the file
		//
		DXGI_FORMAT GetPixelFormat( _In_ IWICBitmapFrameDecode* const, _Out_ size_t*, _Out_ WICPixelFormatGUID* );

		//
		// All textures run a similar process before actually loading to memory, this is that
		// - TextureInit: wrapper with all initialization params
		// - PreInitOutput: initialized values common for all textures
		//
		PreInitOutput PreInitTexture( _In_ const TextureInit* const );

		//
		// copies frame pixels to cpu memory, accounts for auto resize, and format conversions
		// - TextureInit: initialization parameters
		// - PreInitOutput: structure with all texture pre-init values
		// - uint8_t**: pointer to the cpu buffer used to store the pixels
		//
		void LoadCPUData( _In_ PreInitOutput*, _Out_ uint8_t** );

		//
		// load a 1D texture from file
		// - Text: filepath to the 1D texture
		// - function< void( Texture1D ) >: onSuccess callback
		// - function< void( TextureRequestError ) >: onFail callback
		//
		void LoadTexture1D( _In_ TextureInit* const );

		//
		// load a 2D texture from file
		// - Text: filepath to the 2D texture
		// - function< void( Texture2D ) >: onSuccess callback
		// - function< void( TextureRequestError ) >: onFail callback
		//
		void LoadTexture2D( _In_ TextureInit* const );

		//
		// load a 3D texture from file
		// - Text: filepath to the 3D texture
		// - function< void( Texture3D ) >: onSuccess callback
		// - function< void( TextureRequestError ) >: onFail callback
		//
		void LoadTexture3D( _In_ TextureInit* const );

		//
		// load the argued 1D texture data to the gpu
		// - TextureInit: user-defined initialization parameters
		// - PreInitOutput: initialized image parameters
		// - D3D11_TEXTURE1D_DESC: texture description paramaters
		// - D3D11_SUBRESOURCE_DATA: texture data parameters
		// - ID3D11Texture1D**: output DX texture handle
		// - ID3D11ShaderResourceView**: output DX shader resource handle
		//
		void LoadGPUResource1D(
			_In_ const TextureInit* const,
			_In_ const PreInitOutput&,
			_In_ D3D11_TEXTURE1D_DESC&,
			_In_ D3D11_SUBRESOURCE_DATA&,
			_Out_ ID3D11Texture1D**,
			_Out_ ID3D11ShaderResourceView** );

		//
		// load the argued 2D texture data to the gpu
		// - TextureInit: user-defined initialization parameters
		// - PreInitOutput: initialized image parameters
		// - D3D11_TEXTURE2D_DESC: texture description paramaters
		// - D3D11_SUBRESOURCE_DATA: texture data parameters
		// - ID3D11Texture2D**: output DX texture handle
		// - ID3D11ShaderResourceView**: output DX shader resource handle
		//
		void LoadGPUResource2D(
			_In_ const TextureInit* const,
			_In_ const PreInitOutput&,
			_In_ D3D11_TEXTURE2D_DESC&,
			_In_ D3D11_SUBRESOURCE_DATA&,
			_Out_ ID3D11Texture2D**,
			_Out_ ID3D11ShaderResourceView** );
		
		//
		// load the argued 3D texture data to the gpu
		// - TextureInit: user-defined initialization parameters
		// - PreInitOutput: initialized image parameters
		// - D3D11_TEXTURE3D_DESC: texture description paramaters
		// - D3D11_SUBRESOURCE_DATA: texture data parameters
		// - ID3D11Texture3D**: output DX texture handle
		// - ID3D11ShaderResourceView**: output DX shader resource handle
		//
		void LoadGPUResource3D(
			_In_ const TextureInit* const,
			_In_ const PreInitOutput&,
			_In_ D3D11_TEXTURE3D_DESC&,
			_In_ D3D11_SUBRESOURCE_DATA&,
			_Out_ ID3D11Texture3D**,
			_Out_ ID3D11ShaderResourceView** );

		//
		// callback to those in the 1D queue waiting for the argued texture
		// - TextureInit: initialization values passed with RequestTexture1D()
		// - LoadedTexture: the loaded texture, as requested :)
		//
		void Process1DQueue( _In_ const TextureInit* const, _In_ LoadedTexture& );

		//
		// callback to those in the 1D informing the given texture has failed
		// - TextureInit: initialization values passed with RequestTexture1D()
		// - TextureRequestError: reason why the texture failed to load :(
		//
		void Process1DQueue( _In_ const TextureInit* const, _In_ TextureRequestError );

		//
		// callback to those in the 2D queue waiting for the argued texture
		// - TextureInit: initialization values passed with RequestTexture2D()
		// - LoadedTexture: the loaded texture, as requested :)
		//
		void Process2DQueue( _In_ const TextureInit* const, _In_ LoadedTexture& );

		//
		// callback to those in the 2D informing the given texture has failed
		// - TextureInit: initialization values passed with RequestTexture2D()
		// - TextureRequestError: reason why the texture failed to load :(
		//
		void Process2DQueue( _In_ const TextureInit* const, _In_ TextureRequestError );

		//
		// callback to those in the 3D queue waiting for the argued texture
		// - TextureInit: initialization values passed with RequestTexture3D()
		// - LoadedTexture: the loaded texture, as requested :)
		//
		void Process3DQueue( _In_ const TextureInit* const, _In_ LoadedTexture& );

		//
		// callback to those in the 3D queue informing the given texture has failed
		// - TextureInit: initialization values passed with RequestTexture3D()
		// - TextureRequestError: reason why the texture failed to load :(
		//
		void Process3DQueue( _In_ const TextureInit* const, _In_ TextureRequestError );

	public:

		//
		// dtor
		//
		~TextureManager( );

		//
		// initializes the WIC decoder factory
		//
		bool Initialize( );

		//
		// request for a 1D texture resource
		// - TextureInit: initializer structure
		// - function< void(Texture1D) >: callback with the loaded ready to use texture
		// - function< void(TextureRequestError) >: callback with an enumerated reason
		//
		void RequestTexture1D( _In_ TextureInit*,
			_In_ HyJynxCore::Function< void, Texture1D* >,
			_In_opt_ HyJynxCore::Function< void, TextureRequestError > );

		//
		// request for a 2D texture resource
		// - TextureInit: initializer structure
		// - function< void(Texture2D) >: callback with the loaded ready to use texture
		// - function< void(TextureRequestError) >: callback with an enumerated reason
		//
		void RequestTexture2D( _In_ TextureInit*,
			_In_ HyJynxCore::Function< void, Texture2D* >,
			_In_opt_ HyJynxCore::Function< void, TextureRequestError > );

		//
		// request for a 3D texture resource
		// - TextureInit: initializer structure
		// - function< void(Texture3D) >: callback with the loaded ready to use texture
		// - function< void(TextureRequestError) >: callback with an enumerated reason
		//
		void RequestTexture3D( _In_ TextureInit*,
			_In_ HyJynxCore::Function< void, Texture3D* >,
			_In_opt_ HyJynxCore::Function< void, TextureRequestError > );

		//
		// get how many bytes of texture data is loaded on the GPU
		// - UInt: optionally retrieve the total amount of textures loaded on GPU
		// - returns UInt: bytes loaded on the GPU
		//
		UInt TotalGPUBytesLoaded( _Out_opt_ UInt* const ) const;

		//
		// get how many bytes of texture data is loaded on the CPU
		// - UInt: optionally retrieve the total amount of textures loaded on CPU
		// - returns UInt: bytes loaded on the CPU
		//
		UInt TotalCPUBytesLoaded( _Out_opt_ UInt* const ) const;

		//
		// get how many bytes of teture data is currently loaded
		// - UInt: optionally retrieve the total amount of textures loaded
		// - returns UInt: bytes loaded on CPU and GPU
		//
		UInt TotalBytesLoaded( _Out_opt_ UInt* const ) const;

		//
		// output useful information to a human-readable string
		// - returns Text: useful readable information
		//
		HyJynxCore::Text GetLog( ) const;

		//
		// translate the argued texture error to human readable text
		// - TextureRequestError: error returned with failing callback
		// - returns Text: pathetic human text
		//
		static HyJynxCore::Text TranslateError( _In_ const TextureRequestError );
	};
}

#endif // HYJYNXRENDERER_TEXTUREMANAGER_H