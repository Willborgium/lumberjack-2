/**
 * Camera.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXRENDERER_CAMERA_H
#define HYJYNXRENDERER_CAMERA_H

#include <Renderer/Headers/ICameraBehavior.h>
#include <Renderer/Headers/Transform.h>
#include <Renderer/Headers/ViewFrustum.h>
#include <Renderer/Headers/CollisionDescription.h>
#include <Renderer/Headers/Matrix.h>
#include <Renderer/Headers/Vector3.h>
#include <Renderer/Headers/Context.h>
#include <Engine\Headers\Dirty.h>
#include <Logger\Headers\ILog.h>
#include <DirectXMath.h>
#include <DirectXCollision.h>

namespace HyJynxRenderer
{
	//
	// Your API to look into our world.
	//
	class Camera : public HyJynxUtilities::Dirty, public HyJynxLogger::ILog
	{
	protected:

		Transform					_position;
		Transform					_target;
		ViewFrustum					_frustum;
		Matrix						_viewMatrix;

		ICameraBehavior*			_behavior = nullptr;

		bool						_projectionNeedsUpdate = true;
		bool						_viewNeedsUpdate = true;

		//
		// when changes occur to the camera's view, this will update the View matrix
		//
		void updateViewMatrix( );

	public:

		static const Vector3		UpDirection;

		//
		// Null ctor
		//
		Camera( );

		//
		// copy ctor
		//
		Camera( _In_ const Camera& );

		//
		// move ctor
		//
		Camera( _In_ const Camera&& );

		//
		// dtor
		//
		virtual ~Camera( );

		//
		// overriden ILog method to get contained loggers
		//
		void GetChildrenLoggers( _In_ HyJynxCollections::DynamicCollection< HyJynxLogger::ILog* >* const ) override;

		//
		// Get a const reference to the ViewFrustum of this camera
		// -returns: const ViewFrustum*
		//
		ViewFrustum* const GetViewFrustum( );

		//
		// get a pointer to the transform handling the camera's position
		// - return Transform*: position transform
		//
		Transform& GetPosition( );

		//
		// get a pointer to the position of the target this camera is looking at
		// - return Transform*: position of the look at target
		//
		Transform& GetLookAtTarget( );

		//
		// get the view matrix of this camera
		// - returns Matrix: matrix representing this camera's view space
		//
		Matrix& GetViewMatrix( );

		//
		// get a normalized vector representing its direction in 3D space
		// - returns Vector3: direction the camera is viewing
		//
		Vector3 GetViewingVector( ) const;

		//
		// get the view matrix of this camera, loaded into a SIMD proxy
		// - returns XMMATRIX: matrix representing this camera's view space
		//
		DirectX::XMMATRIX GetViewMatrixXM( );

		//
		// need to know if something is visible or not? no problem buddy,
		// consider this your clean hook to hack! - DO NOT ABUSE this function.
		// Renderer will frustum cull drawables by default, this is a hook for gameplay logic.
		// - CollisionDescription*: collidable interface 
		// - CollisionMethod: the maximum cull complexity to test
		// - returns bool: true - the object is visible by this camera, false - tis not
		//
		bool CanSee( _In_ CollisionDescription*, _In_ CollisionMethod );

		//
		// Update the constant buffers appropriately.
		// Camera will manage what buffers need updating for optimal performance.
		// - Context: DirectX context API container
		//
		void UpdateConstants( _In_ Context* );
	};
}

#endif // HYJYNXRENDERER_CAMERA_H