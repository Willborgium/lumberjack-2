/**
 * Vector3.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\Vector3.h>
#include <Renderer\Headers\Vector2.h>


using namespace DirectX;

namespace HyJynxRenderer
{
	// null ctor
	Vector3::Vector3( )
		: XMFLOAT3( 0.0f, 0.0f, 0.0f )
	{ }

	// single value ctor
	Vector3::Vector3( _In_ const float val )
		: XMFLOAT3( val, val, val )
	{ }

	// xy value ctor
	Vector3::Vector3( _In_ const float x, _In_ const float y )
		: XMFLOAT3( x, y, 0.0f )
	{ }

	// full ctor
	Vector3::Vector3( _In_ const float x, _In_ const float y, _In_ const float z )
		: XMFLOAT3( x, y, z )
	{ }

	// base-type ctor
	Vector3::Vector3( _In_ const XMFLOAT3& input )
		: XMFLOAT3( input.x, input.y, input.z )
	{ }

	// copy ctor
	Vector3::Vector3( _In_ const Vector3& other )
		: XMFLOAT3( other )
	{ }

	// move ctor
	Vector3::Vector3( _In_ const Vector3&& other )
		: XMFLOAT3( other )
	{ }

	// get a vector3 set to 1
	Vector3 Vector3::One( )
	{
		return Vector3( 1.0f, 1.0f, 1.0f );
	}

	// get a vector3 set to 0
	Vector3 Vector3::Zero( )
	{
		return Vector3( );
	}

	// normalize this to a length of 1
	void Vector3::Normalize( )
	{
		float length = std::sqrt( ( x * x ) + ( y * y ) + ( z * z ) );
		x /= length;
		y /= length;
		z /= length;
	}

	// Arithmetic 
	Vector3 Vector3::operator = ( _In_ const Vector3& other )
	{
		x = other.x;
		y = other.y;
		z = other.z;
		return *this;
	}

	Vector3 Vector3::operator * ( _In_ const Vector3& other ) const
	{
		return Vector3( x * other.x, y * other.y, z * other.z );
	}

	Vector3 Vector3::operator * ( _In_ const float value ) const
	{
		return Vector3( x * value, y * value, z * value );
	}

	Vector3 Vector3::operator * ( _In_ const double value ) const
	{
		float val = static_cast< float >( value );
		return Vector3( x * val, y * val, z * val );
	}

	Vector3 Vector3::operator * ( _In_ const long value ) const
	{
		float val = static_cast< float >( value );
		return Vector3( x * val, y * val, z * val );
	}

	Vector3 Vector3::operator * ( _In_ const LongLong value ) const
	{
		float val = static_cast< float >( value );
		return Vector3( x * val, y * val, z * val );
	}


	Vector3 Vector3::operator / ( _In_ const Vector3& other ) const
	{
		return Vector3( x / other.x, y / other.y, z / other.z );
	}

	Vector3 Vector3::operator / ( _In_ const float value ) const
	{
		return Vector3( x / value, y / value, z / value );
	}

	Vector3 Vector3::operator / ( _In_ const double value ) const
	{
		float val = static_cast< float >( value );
		return Vector3( x / val, y / val, z / val );
	}

	Vector3 Vector3::operator / ( _In_ const long value ) const
	{
		float val = static_cast< float >( value );
		return Vector3( x / val, y / val, z / val );
	}

	Vector3 Vector3::operator / ( _In_ const LongLong value ) const
	{
		float val = static_cast< float >( value );
		return Vector3( x / val, y / val, z / val );
	}


	Vector3& Vector3::operator ++ ( )
	{
		x++;
		y++;
		z++;
		return *this;
	}

	Vector3& Vector3::operator -- ( )
	{
		x--;
		y--;
		z--;
		return *this;
	}


	// Comparison
	bool Vector3::operator == ( _In_ const Vector3& other ) const
	{
		return x == other.x && y == other.y && z == other.z;
	}

	bool Vector3::operator != ( _In_ const Vector3& other ) const
	{
		return x != other.x && y != other.y && z != other.z;
	}

	bool Vector3::operator > ( _In_ const Vector3& other ) const
	{
		return ( x + y + z ) > ( other.x + other.y + other.z );
	}

	bool Vector3::operator >= ( _In_ const Vector3& other ) const
	{
		return ( x + y + z ) >= ( other.x + other.y + other.z );
	}

	bool Vector3::operator < ( _In_ const Vector3& other ) const
	{
		return ( x + y + z ) < ( other.x + other.y + other.z );
	}

	bool Vector3::operator <= ( _In_ const Vector3& other ) const
	{
		return ( x + y + z ) <= ( other.x + other.y + other.z );
	}


	// Compound
	Vector3& Vector3::operator += ( _In_ const Vector3& other )
	{
		x += other.x;
		y += other.y;
		z += other.z;
		return *this;
	}

	Vector3& Vector3::operator -= ( _In_ const Vector3& other )
	{
		x -= other.x;
		y -= other.y;
		z -= other.z;
		return *this;
	}

	Vector3& Vector3::operator *= ( _In_ const Vector3& other )
	{
		x *= other.x;
		y *= other.y;
		z *= other.z;
		return *this;
	}

	Vector3& Vector3::operator *= ( _In_ const float value )
	{
		float val = static_cast< float >( value );
		x *= val;
		y *= val;
		z *= val;
		return *this;
	}

	Vector3& Vector3::operator *= ( _In_ const double value )
	{
		float val = static_cast< float >( value );
		x *= val;
		y *= val;
		z *= val;
		return *this;
	}

	Vector3& Vector3::operator *= ( _In_ const long value )
	{
		float val = static_cast< float >( value );
		x *= val;
		y *= val;
		z *= val;
		return *this;
	}

	Vector3& Vector3::operator *= ( _In_ const LongLong value )
	{
		float val = static_cast< float >( value );
		x *= val;
		y *= val;
		z *= val;
		return *this;
	}


	Vector3& Vector3::operator /= ( _In_ const Vector3& other )
	{
		x /= other.x;
		y /= other.y;
		z /= other.z;
		return *this;
	}

	Vector3& Vector3::operator /= ( _In_ const Vector2& other )
	{
		x /= other.x;
		y /= other.y;
		return *this;
	}

	Vector3& Vector3::operator /= ( _In_ const float value )
	{
		float val = static_cast< float >( value );
		x /= val;
		y /= val;
		z /= val;
		return *this;
	}

	Vector3& Vector3::operator /= ( _In_ const double value )
	{
		float val = static_cast< float >( value );
		x /= val;
		y /= val;
		z /= val;
		return *this;
	}

	Vector3& Vector3::operator /= ( _In_ const long value )
	{
		float val = static_cast< float >( value );
		x /= val;
		y /= val;
		z /= val;
		return *this;
	}

	Vector3& Vector3::operator /= ( _In_ const LongLong value )
	{
		float val = static_cast< float >( value );
		x /= val;
		y /= val;
		z /= val;
		return *this;
	}

}