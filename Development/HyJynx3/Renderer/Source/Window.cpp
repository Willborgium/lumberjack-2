/**
 * Window.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\Window.h>

using namespace HyJynxCore;
using namespace HyJynxCollections;

namespace HyJynxRenderer
{
	// null ctor
	Window::Window( )
	{ }

	// default ctor #1
	Window::Window( _In_ WindowDrawType type, _In_opt_ Text name )
		: _drawType( type ),
		_name( name )
	{ }

	// default ctor #2
	Window::Window( _In_ WindowDrawType type, _In_ UInt width, _In_ UInt height )
		: _drawType( type ),
		_width( width ),
		_height( height )
	{ }

	// copy ctor
	Window::Window( _In_ const Window& other )
		: _drawType( other._drawType ),
		_name( other._name ),
		_transform( other._transform ),
		_width( other._width ),
		_height( other._height ),
		_active( other._active ),
		_lockedZ( other._lockedZ ),
		_drawableList( other._drawableList )
	{ }

	// move ctor
	Window::Window( _In_ const Window&& other )
		: _drawType( other._drawType ),
		_name( other._name ),
		_transform( other._transform ),
		_width( other._width ),
		_height( other._height ),
		_active( other._active ),
		_lockedZ( other._lockedZ ),
		_drawableList( other._drawableList )
	{ }

	// dtor
	Window::~Window( )
	{
		Release( );
		_name = "disposed";
		_active = false;
	}

	// release drawables from the window
	void Window::Release( )
	{
		_drawableList.Clear( );
	}

	// retrieve composed loggers
	void Window::GetChildrenLoggers( _In_ DynamicCollection< ILog* >* const children )
	{
		_drawableList.ForEach( [ &children ] ( _In_ IDrawable* drawable )
		{
			children->Append( drawable );
		} );
	}

	// add a drawable to be rendered on screen
	void Window::AddDrawable( _In_ IDrawable* drawable )
	{
		if ( drawable != nullptr )
		{
			if ( _drawableList.Contains( drawable ) == false )
			{
				_drawableList.Append( drawable );
			}
			else
			{
				LogWarning( "AddDrawable() - argument is already registered in this window, will not add." );
			}
		}
		else
		{
			LogWarning( "AddDrawable( nullptr ) - argument is null, will not add." );
		}
	}

	// remove a container
	void Window::AddContainer( _In_ DrawableContainer* const container )
	{
		if ( container != nullptr )
		{
			_containerList.Append( container );
		}
		else
		{
			LogWarning( "AddContainer(nullptr) - invalid argument." );
		}
	}

	// remove a drawable by original reference
	void Window::RemoveDrawable( _In_ IDrawable* drawable )
	{
		if ( drawable == nullptr )
		{
			LogWarning( "RemoveDrawable( nullptr ) - argument is null, cannot remove." );
			return;
		}

		_drawableList.Remove( drawable );
	}

	// remove a container
	void Window::RemoveContainer( _In_ DrawableContainer* container )
	{
		if ( container != nullptr )
		{
			_containerList.Remove( container );
		}
		else
		{
			LogWarning( "RemoveContainer(nullptr) - invalid argument." );
		}
	}

	// get a drawable pointer by ID
	IDrawable* Window::GetDrawableById( _In_ const int id ) const
	{
		for ( UInt index = 0; index < _drawableList.Count( ); index++ )
		{
			if ( _drawableList[ index ]->GetDrawableID( ) == id )
			{
				return _drawableList[ index ];
			}
		}

		return nullptr;
	}

	// main update call
	void Window::Update( _In_ Context* context, _In_ const RenderTime time )
	{
		_drawableList.Where( [ ] ( _In_ IDrawable* drawable )
		{
			return drawable->GetActive( );

		} )->ForEach( [&] ( IDrawable* drawable )
		{
			drawable->Update( context, time );
		} );
	}

	// cull the window and contents
	void Window::FrustumCull( _In_ ViewFrustum& frustum, _In_ const CollisionMethod maxMethod,
		_In_ BatchContainer& batchContainer )
	{
		// not active, don't bother
		if ( _active == false )
		{
			return;
		}

		WindowDrawType type = _drawType;

		// test the window itself
		if ( _collisionDescription.GetBoundingBox( ) != nullptr )
		{
			if ( frustum.IsInside( *_collisionDescription.GetBoundingBox( ) ) == false )
			{
				return;
			}
		}

		// test contents
		_drawableList.ForEach( [&frustum, &maxMethod, &batchContainer, type] ( _In_ IDrawable* drawable )
		{
			if ( drawable->GetActive( ) == true )
			{
				if ( drawable->GetCollisionDescription( )->IsInFrustum( frustum, maxMethod ) )
				{
					batchContainer.RegisterDrawable( type, drawable, frustum.GetFrustum( ).Origin );
				}
			}
		} );
	}

#pragma region accessors

	// return clip-region width
	UInt Window::GetWidth( ) const
	{
		return _width;
	}

	// return clip-region height
	UInt Window::GetHeight( ) const
	{
		return _height;
	}

	// set clip-region width
	void Window::SetWidth( _In_ const UInt w )
	{
		_width = w;
	}

	// set clip-region height
	void Window::SetHeight( _In_ const UInt h )
	{
		_height = h;
	}

	// 2d? 3d?
	WindowDrawType Window::GetDrawingType( ) const
	{
		return _drawType;
	}

	// what do you call yourself?
	Text Window::GetName( ) const
	{
		return _name;
	}

	// do not draw yourself at all.
	void Window::SetActive( _In_ const bool val )
	{
		_active = val;
	}

	// are you drawing?
	bool Window::GetActive( ) const
	{
		return _active;
	}

	// get the pointer to this window's physical location obj
	Transform* Window::GetTransform( )
	{
		return &_transform;
	}

	// can this window's Z position ever be changed?
	void Window::SetZLock( _In_ const bool val )
	{
		_lockedZ = val;
	}

	// well are ya!?
	bool Window::IsZLocked( ) const
	{
		return _lockedZ;
	}

#pragma endregion

#pragma region operators

	// add a drawable
	Window& Window::operator += ( _In_ IDrawable* drawable )
	{
		AddDrawable( drawable );
		return *this;
	}

	// add a container
	Window& Window::operator += ( _In_ DrawableContainer* container )
	{
		AddContainer( container );
		return *this;
	}

	// remove a drawable from the window
	Window& Window::operator -= ( _In_ IDrawable* drawable )
	{
		RemoveDrawable( drawable );
		return *this;
	}

	// remove a container
	Window& Window::operator -= ( _In_ DrawableContainer* container )
	{
		RemoveContainer( container );
		return *this;
	}

#pragma endregion
}