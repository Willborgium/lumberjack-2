/**
 * DepthStencil.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\DepthStencil.h>

namespace HyJynxRenderer
{
	// null ctor
	DepthStencil::DepthStencil( )
	{ }

	// default ctor
	DepthStencil::DepthStencil( _In_ ID3D11DepthStencilView* const view, 
		_In_ ID3D11Texture2D* const tex, ID3D11DepthStencilState* state )
		: _view( view ),
		_texture( tex ),
		_state( state )
	{ }

	// copy ctor
	DepthStencil::DepthStencil( _In_ const DepthStencil& other )
		: _view( other._view ),
		_texture( other._texture ),
		_state( other._state )
	{ }

	// move ctor
	DepthStencil::DepthStencil( _In_ const DepthStencil&& other )
		: _view( other._view ),
		_texture( other._texture ),
		_state( other._state )
	{ }

	// dtor
	DepthStencil::~DepthStencil( )
	{
		Release( );
	}

	// release dx objects
	void DepthStencil::Release( )
	{
		if ( _view != nullptr )
		{
			_view->Release( );
		}

		if ( _texture != nullptr )
		{
			_texture->Release( );
		}

		if ( _state != nullptr )
		{
			_state->Release( );
		}
	}

	// gets the depth stencil view
	ID3D11DepthStencilView* const DepthStencil::GetView( )
	{
		return _view;
	}

	// gets the viewable texture
	ID3D11Texture2D* const DepthStencil::GetTexture( )
	{
		return _texture;
	}

	// gets the depth stencil state
	ID3D11DepthStencilState* const DepthStencil::GetState( )
	{
		return _state;
	}
}