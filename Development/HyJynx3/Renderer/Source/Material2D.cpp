/**
* Material2D.cpp
* (c) 2014 All Rights Reserved.
*/

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\Material2D.h>
#include <Renderer\Headers\IDrawable.h>
#include <Renderer\Headers\MaterialLibrary.h>
#include <Renderer\Headers\ShaderFactory.h>
#include <Renderer\Headers\Renderer.h>
#include <Engine\Headers\ResourceController.h>

using namespace HyJynxCore;
using namespace DirectX;

namespace HyJynxRenderer
{
	// null ctor
	Material2D::Material2D( )
		: Material( MaterialLibrary::ShaderLinkage2DLayer )
	{
		ShaderInit initData;
		initData.ExpectedVertexDescription = ShaderFactory::VertexElementDescription::PositionColorTexture;
		initData.VertexPath = ResourceController::GetPath( Shader::VertexShader::UnProjected );
		initData.PixelPath = ResourceController::GetPath( "PSTest.cso" );

		_shader = ShaderFactory::Instance( )->CreateShader( initData );
	}

	// copy ctor
	Material2D::Material2D( _In_ const Material2D& other )
		: Material( other )
	{ }

	// move ctor
	Material2D::Material2D( _In_ const Material2D&& other )
		: Material( other )
	{ }

	// dtor
	Material2D::~Material2D( )
	{ }

	// update and set buffers to the shader which all following objects will use
	void Material2D::BatchBind( _In_ Context* const context )
	{
		_shader->Bind( context );

		// TODO: make the API to set all of these at once

		context->SetVertexConstantBuffer( static_cast<UInt>( ConstantBufferIndicies::CONSTANT_BUFFER_CAMERA_PROJECTION ), ShaderFactory::Instance( )->GetProjectionConstant( ) );
		context->SetVertexConstantBuffer( static_cast<UInt>( ConstantBufferIndicies::CONSTANT_BUFFER_CAMERA ), ShaderFactory::Instance( )->GetCameraConstant( ) );
		context->SetVertexConstantBuffer( static_cast<UInt>( ConstantBufferIndicies::CONSTANT_BUFFER_CYCLE ), ShaderFactory::Instance( )->GetCycleConstant( ) );
		context->SetVertexConstantBuffer( static_cast<UInt>( ConstantBufferIndicies::CONSTANT_BUFFER_OBJECT ), ShaderFactory::Instance( )->GetObjectConstant( ) );
	
		context->SetRasterizer( Renderer::RasterizerState::Solid2DManualDepth );
	}

	// bind the argued drawable containers to the argued constant buffer
	void Material2D::ObjectBind( _In_ IDrawable* const drawable, _In_ Mesh* const mesh, _In_ CBObject* const objConstant )
	{
		XMMATRIX mat = XMLoadFloat4x4( &drawable->GetTransform( )->GetMatrix( ) );
		XMStoreFloat4x4( &objConstant->WorldMatrix, XMMatrixTranspose( mat ) );
	}
}