/**
 * Camera.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\Camera.h>

#include <Renderer\Headers\Renderer.h>
#include <Renderer\Headers\StationaryCameraBehavior.h>

using namespace HyJynxUtilities;
using namespace HyJynxCollections;
using namespace DirectX;

namespace HyJynxRenderer
{

	const Vector3 Camera::UpDirection = Vector3( 0.0f, 1.0f, 0.0f );

	// null ctor
	Camera::Camera( )
		: Dirty( true ),
		ILog( "Camera" )
	{
		_behavior = anew (StationaryCameraBehavior)( );
		_target.SetPosition( 0.0f, 1.0f, 0.0f );

		updateViewMatrix( );

		_frustum.GetOnDirtyDelegate( )->Append( "CameraProjectionListener", [ this ] ( _In_ void* )
		{
			_projectionNeedsUpdate = true;
		} );

		_position.GetOnDirtyDelegate( )->Append( "CameraViewListener", [this] ( _In_ void* )
		{
			_viewNeedsUpdate = true;
		} );

		_target.GetOnDirtyDelegate( )->Append( "CameraViewListener", [this] ( _In_ void* )
		{
			_viewNeedsUpdate = true;
		} );
	}

	// copy ctor
	Camera::Camera( _In_ const Camera& other )
		: Dirty( true ),
		ILog( "Camera" ),
		_behavior( other._behavior )
	{
		_position.SetPosition( other._position.GetPosition( ) );
		_position.SetRotation( other._position.GetRotation( ) );
		_target.SetPosition( other._position.GetPosition( ) );

		_frustum.GetOnDirtyDelegate( )->Append( "CameraProjectionListener", [this] ( _In_ void* )
		{
			_projectionNeedsUpdate = true;
		} );

		_position.GetOnDirtyDelegate( )->Append( "CameraViewListener-Position", [this] ( _In_ void* )
		{
			_viewNeedsUpdate = true;
		} );

		_target.GetOnDirtyDelegate( )->Append( "CameraViewListener-Target", [this] ( _In_ void* )
		{
			_viewNeedsUpdate = true;
		} );
	}

	// move ctor
	Camera::Camera( _In_ const Camera&& other )
		: Dirty( true ),
		ILog( "Camera" ),
		_behavior( other._behavior )
	{
		_position.SetPosition( other._position.GetPosition( ) );
		_position.SetRotation( other._position.GetRotation( ) );
		_target.SetPosition( other._position.GetPosition( ) );

		_frustum.GetOnDirtyDelegate( )->Append( "CameraProjectionListener", [this] ( _In_ void* )
		{
			_projectionNeedsUpdate = true;
		} );

		_position.GetOnDirtyDelegate( )->Append( "CameraViewListener-Position", [this] ( _In_ void* )
		{
			_viewNeedsUpdate = true;
		} );

		_target.GetOnDirtyDelegate( )->Append( "CameraViewListener-Target", [this] ( _In_ void* )
		{
			_viewNeedsUpdate = true;
		} );
	}

	// dtor
	Camera::~Camera( )
	{
		_behavior = nullptr;
	}

	// overriden ILog method to get contained loggers
	//
	void Camera::GetChildrenLoggers( _In_ HyJynxCollections::DynamicCollection< ILog* >* const loggers )
	{
		loggers->Append( &_frustum );
		if ( _behavior != nullptr )
		{
			loggers->Append( _behavior );
		}
	}

	// calculate the view matrix
	void Camera::updateViewMatrix( )
	{
		if ( _position.GetPosition( ) == _target.GetPosition( ) )
		{
			LogWarning( "updateViewMatrix() - position and target cannot be the same, we will add 1 to target.z in order to avoid a SIMD crash." );
			_target.SetPosition( NULL, NULL, _target.GetPosition( ).z + 1.0f );
		}

		XMVECTOR pos = XMLoadFloat3( &_position.GetPosition( ) );
		XMVECTOR target = XMLoadFloat3( &_target.GetPosition( ) );
		XMVECTOR up = XMLoadFloat3( &UpDirection );
		
		XMMATRIX mat = XMMatrixLookAtLH( pos, target, up );

		XMStoreFloat4x4( &_viewMatrix, mat );

		ToggleClean( );
	}

	// clean and get the view frustum
	ViewFrustum* const Camera::GetViewFrustum( )
	{
		if ( _frustum.IsDirty( ) == true )
		{
			_frustum.CleanFrustum( );
		}

		_frustum.Update( _position.GetOrientation( ), _position.GetPosition( ) );

		return &_frustum;
	}

	// get the position transform pointer - modify as you please
	Transform& Camera::GetPosition( )
	{
		return _position;
	}

	// get the look-at position pointer - modify as you please
	Transform& Camera::GetLookAtTarget( )
	{
		return _target;
	}

	// get view matrix
	Matrix& Camera::GetViewMatrix( )
	{
		if ( IsDirty( ) == true )
		{
			updateViewMatrix( );
		}

		return _viewMatrix;
	}

	// get the viewing direction into the world
	Vector3 Camera::GetViewingVector( ) const
	{
		Vector3 output = {
			_position.GetPosition( ).x - _target.GetPosition( ).x,
			_position.GetPosition( ).y - _target.GetPosition( ).y,
			_position.GetPosition( ).z - _target.GetPosition( ).z
		};

		output.Normalize( );

		return output;
	}

	// get view matrix, in simd proxy
	XMMATRIX Camera::GetViewMatrixXM( )
	{
		if ( IsDirty( ) == true )
		{
			updateViewMatrix( );
		}

		return XMLoadFloat4x4( &_viewMatrix );
	}

	// determine if this camera can see the given collidable
	bool Camera::CanSee( _In_ CollisionDescription* description, _In_ CollisionMethod maximum )
	{
		if ( description == nullptr )
		{
			LogWarning( "CanSee( nullptr ) - CollisionDescription is null, unable to process." );
			return false;
		}

		if ( _frustum.IsDirty( ) == true )
		{
			_frustum.CleanFrustum( );
			_frustum.Update( _position.GetOrientation( ), _position.GetPosition( ) );
		}

		return description->IsInFrustum( _frustum, maximum );
	}

	// update constant buffers which need updating
	void Camera::UpdateConstants( _In_ Context* context )
	{
		if ( _projectionNeedsUpdate == true )
		{
			CBCameraProjection* projBound = context->GetProjectionConstantBuffer( );

			// projection
			XMMATRIX multTemp = XMMatrixIdentity( );
			XMMATRIX valMat = XMLoadFloat4x4( &_frustum.GetProjectionMatrix( ) );
			XMMATRIX output = XMMatrixMultiply( multTemp, valMat );
			XMStoreFloat4x4( &projBound->ProjectionMatrix, XMMatrixTranspose( output ) );

			// orthographic, top-left is 0,0
			Vector2 screen = Renderer::Instance( )->GetScreenSize( );
			valMat = XMMatrixOrthographicOffCenterLH( -1, 1, -1, 1, 0.0f, 1.0f );
			output = XMMatrixMultiply( multTemp, valMat );
			multTemp = XMMatrixTranslation( -1.0f, 1.0f, 0.0f );
			output = XMMatrixMultiply( output, multTemp );
			XMStoreFloat4x4( &projBound->OrthographicMatrix, XMMatrixTranspose( output ) );

			// screen size and texel adjustment
			projBound->ScreenSize_HalfPixel.x = screen.x;
			projBound->ScreenSize_HalfPixel.y = screen.y;
			projBound->ScreenSize_HalfPixel.z = 1.0f / screen.x;
			projBound->ScreenSize_HalfPixel.w = 1.0f / screen.y;

			context->UpdateProjectionConstantBuffer( );

			_projectionNeedsUpdate = false;
		}

		if ( _viewNeedsUpdate == true )
		{
			CBCamera* cameraBound = context->GetCameraConstantBuffer( );

			cameraBound->CameraPosition = Vector4( _position.GetPosition( ), 1.0f );
			cameraBound->CameraVector = Vector4( GetViewingVector( ), 1.0f );
			XMMATRIX mat = XMLoadFloat4x4( &GetViewMatrix( ) );
			XMStoreFloat4x4( &cameraBound->ViewMatrix, XMMatrixTranspose( mat ) );

			context->UpdateCameraConstantBuffer( );

			_viewNeedsUpdate = false;
		}
	}
}