/**
 * IAnimation.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\IAnimation.h>
#include <Renderer\Headers\Animator.h>

namespace HyJynxRenderer
{
	// remove the animation out of Animator
	void IAnimation::Remove( )
	{
		Animator::Instance( )->Remove( this );
	}
}