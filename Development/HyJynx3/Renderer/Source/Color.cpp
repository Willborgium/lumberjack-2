/**
 * Color.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\Color.h>

using namespace DirectX;

#define CLAMP_COLOR(v){if(v<0)v=0;else if(v>255)v=255;}

namespace HyJynxRenderer
{
#pragma region PreDefined

	const Vector4 Color::PreDefined::White = Vector4( 1.0f );
	const Vector4 Color::PreDefined::Black = Vector4( 0.0f );

#pragma endregion

#pragma region ctor/dtor

	// default ctor - white - not dirty
	Color::Color( )
		:Dirty( false ),
		_multipliedColor( 1, 1, 1, 1 ),
		_rgbaColor( 255, 255, 255, 255 )
	{ }

	// set RGB to value ctor - not dirty
	Color::Color( _In_ float value )
		:Dirty( false ),
		_multipliedColor( value / 255, value / 255, value / 255, 1 ),
		_rgbaColor( value, value, value, 255 )
	{ }

	// rgb - not dirty
	Color::Color( _In_ float r, _In_ float g, _In_ float b )
		:Dirty( false ),
		_multipliedColor( r / 255, g / 255, b / 255, 1 ),
		_rgbaColor( r, g, b, 255 )
	{ }

	// rgba - dirty
	Color::Color( _In_ float r, _In_ float g, _In_ float b, _In_ float a )
		:Dirty( true ),
		_multipliedColor( ),
		_rgbaColor( r, g, b, a )
	{ }

	// rgb xm - dirty
	Color::Color( _In_ Vector3 rgbColor )
		:Dirty( true ),
		_multipliedColor( ),
		_rgbaColor( rgbColor.x, rgbColor.y, rgbColor.z, 255 )
	{ }

	// rgba xm - dirty
	Color::Color( _In_ Vector4 rgbaColor )
		:Dirty( true ),
		_multipliedColor( ),
		_rgbaColor( rgbaColor.x, rgbaColor.y, rgbaColor.z, rgbaColor.w )
	{ }

	// copy ctor
	Color::Color( _In_ const Color& other )
		: Dirty( true ),
		_rgbaColor( other._rgbaColor )
	{ }

	// move ctor
	Color::Color( _In_ const Color&& other )
		: Dirty( true ),
		_rgbaColor( other._rgbaColor )
	{ }

	// dtor
	Color::~Color( )
	{ }

#pragma endregion ctor/dtor

#pragma region api

	// multiplie color values
	void Color::multiplyColor( )
	{
		_multipliedColor.w = _rgbaColor.w / 255;
		_multipliedColor.x = ( _rgbaColor.x / 255 ) * _multipliedColor.w;
		_multipliedColor.y = ( _rgbaColor.y / 255 ) * _multipliedColor.w;
		_multipliedColor.z = ( _rgbaColor.z / 255 ) * _multipliedColor.w;
	}

	// update multiplied color values
	void Color::Clean( )
	{
		multiplyColor( );
		ToggleClean( );
	}

	// set color to the values of another;
	void Color::SetColor( _In_ Color* color )
	{
		if ( color != nullptr )
		{
			_rgbaColor.x = color->GetRed( );
			_rgbaColor.y = color->GetGreen( );
			_rgbaColor.z = color->GetBlue( );
			_rgbaColor.w = color->GetAlpha( );

			SetDirty( true );
		}
	}

	// Set the color to a full opaque grey scale
	void Color::SetColor( _In_ float grey )
	{
		_rgbaColor.x = _rgbaColor.y = _rgbaColor.z = grey;

		_rgbaColor.w = 255;

		SetDirty( true );
	}

	// Set the RGB color, alpha remains unchanged
	void Color::SetColor( _In_ float r, _In_ float g, _In_ float b )
	{
		_rgbaColor.x = r;
		_rgbaColor.y = g;
		_rgbaColor.z = b;

		SetDirty( true );
	}

	// Set the RGBA color
	void Color::SetColor( _In_ float r, _In_ float g, _In_ float b, _In_ float a )
	{
		_rgbaColor.x = r;
		_rgbaColor.y = g;
		_rgbaColor.z = b;
		_rgbaColor.w = a;

		SetDirty( true );
	}

	// set RGB multplied colors from another
	void Color::SetMultipliedColor( _In_ Vector3& rgb )
	{
		_rgbaColor.x = rgb.x * 255.0f;
		_rgbaColor.y = rgb.y * 255.0f;
		_rgbaColor.z = rgb.z * 255.0f;

		SetDirty( true );
	}

	// set RGBA multiplied colors from another
	void Color::SetMultipliedColor( _In_ Vector4& rgba )
	{
		_rgbaColor.x = rgba.x * 255.0f;
		_rgbaColor.y = rgba.y * 255.0f;
		_rgbaColor.z = rgba.z * 255.0f;
		_rgbaColor.w = rgba.w * 255.0f;

		SetDirty( true );
	}

	// Set the color to a full opaque grey scale
	void Color::SetMultipliedColor( _In_ float grey )
	{
		_rgbaColor.x = _rgbaColor.y = _rgbaColor.z = grey * 255.0f;

		_rgbaColor.w = 255;

		SetDirty( true );
	}

	// Set the RGB color, alpha remains unchanged
	void Color::SetMultipliedColor( _In_ float r, _In_ float g, _In_ float b )
	{
		_rgbaColor.x = r * 255.0f;
		_rgbaColor.y = g * 255.0f;
		_rgbaColor.z = b * 255.0f;

		SetDirty( true );
	}

	// Set the RGBA color
	void Color::SetMultipliedColor( _In_ float r, _In_ float g, _In_ float b, _In_ float a )
	{
		_rgbaColor.x = r * 255.0f;
		_rgbaColor.y = g * 255.0f;
		_rgbaColor.z = b * 255.0f;
		_rgbaColor.w = a * 255.0f;

		SetDirty( true );
	}

	// clean and get multiplied value
	Vector4* const Color::GetMultipliedColor( )
	{
		if ( IsDirty( ) == true )
		{
			multiplyColor( );
		}
		return &_multipliedColor;
	}

	// Get the RGBA color (0-255)
	const Vector4& Color::GetRGBAColor( )
	{
		return _rgbaColor;
	}

	// Get the Red Component (0-255)
	float Color::GetRed( ) const
	{
		return _rgbaColor.w;
	}

	// Get the Blue Component (0-255)
	float Color::GetGreen( ) const
	{
		return _rgbaColor.y;
	}

	// Get the Green Component (0-255)
	float Color::GetBlue( ) const
	{
		return _rgbaColor.z;
	}

	// Get the Alpha Component (0-255)
	float Color::GetAlpha( ) const
	{
		return _rgbaColor.w;
	}

	// Get the Red Component (0-1)
	float Color::GetMultipliedRed( ) const
	{
		return _multipliedColor.w;
	}

	// Get the Green Component (0-1)
	float Color::GetMultipliedGreen( ) const
	{
		return _multipliedColor.y;
	}

	// Get the Blue Component (0-1)
	float Color::GetMultipliedBlue( ) const
	{
		return _multipliedColor.z;
	}

	// Get the Alpha Component (0-1)
	float Color::GetMultipliedAlpha( ) const
	{
		return _multipliedColor.w;
	}

#pragma endregion api

#pragma region operators

	// arithmetic
	Color Color::operator = ( _In_ const Color& other )
	{
		_rgbaColor.x = other._rgbaColor.x;
		_rgbaColor.y = other._rgbaColor.y;
		_rgbaColor.z = other._rgbaColor.z;
		_rgbaColor.w = other._rgbaColor.w;
		ToggleDirty( );
		return *this;
	}

	Color Color::operator + ( _In_ const Color& other ) const
	{
		return Color(
			_rgbaColor.x + other._rgbaColor.x,
			_rgbaColor.y + other._rgbaColor.y,
			_rgbaColor.z + other._rgbaColor.z,
			_rgbaColor.w + other._rgbaColor.w
		);
	}

	Color Color::operator - ( _In_ const Color& other ) const
	{
		return Color(
			_rgbaColor.x - other._rgbaColor.x,
			_rgbaColor.y - other._rgbaColor.y,
			_rgbaColor.z - other._rgbaColor.z,
			_rgbaColor.w - other._rgbaColor.w
		);
	}

	Color& Color::operator - ( )
	{
		_rgbaColor.x = 255 - _rgbaColor.x;
		_rgbaColor.y = 255 - _rgbaColor.y;
		_rgbaColor.z = 255 - _rgbaColor.z;
		_rgbaColor.w = 255 - _rgbaColor.w;
		ToggleDirty( );
		return *this;
	}

	Color Color::operator * ( _In_ Color& other )
	{
		if ( IsDirty( ) == true )
			multiplyColor( );

		Vector4* col = other.GetMultipliedColor( );

		return Color(
			( _multipliedColor.x * col->x ) * 255,
			( _multipliedColor.y * col->y ) * 255,
			( _multipliedColor.z * col->z ) * 255,
			( _multipliedColor.w * col->w ) * 255
		);
	}

	Color Color::operator / ( _In_ Color& other )
	{
		if ( IsDirty( ) == true )
			multiplyColor( );

		Vector4* col = other.GetMultipliedColor( );

		return Color(
			( _multipliedColor.x / col->x ) * 255,
			( _multipliedColor.y / col->y ) * 255,
			( _multipliedColor.z / col->z ) * 255,
			( _multipliedColor.w / col->w ) * 255
		);
	}

	Color& Color::operator ++ ( )
	{
		_rgbaColor.x++;
		_rgbaColor.y++;
		_rgbaColor.z++;
		_rgbaColor.w++;

		ToggleDirty( );

		return *this;
	}

	Color& Color::operator -- ( )
	{
		_rgbaColor.x--;
		_rgbaColor.y--;
		_rgbaColor.z--;
		_rgbaColor.w--;

		ToggleDirty( );

		return *this;
	}


	// comparison
	bool Color::operator == ( _In_ const Color& other ) const
	{
		return _rgbaColor.x == other._rgbaColor.x
			&& _rgbaColor.y == other._rgbaColor.y
			&& _rgbaColor.z == other._rgbaColor.z
			&& _rgbaColor.w == other._rgbaColor.w;
	}

	bool Color::operator != ( _In_ const Color& other ) const
	{
		return _rgbaColor.x != other._rgbaColor.x
			&& _rgbaColor.y != other._rgbaColor.y
			&& _rgbaColor.z != other._rgbaColor.z
			&& _rgbaColor.w != other._rgbaColor.w;
	}

	bool Color::operator < ( _In_ const Color& other ) const
	{
		return ( _rgbaColor.x + _rgbaColor.y + _rgbaColor.z + _rgbaColor.w )
			< ( other._rgbaColor.x + other._rgbaColor.y + other._rgbaColor.z + other._rgbaColor.w );
	}

	bool Color::operator <= ( _In_ const Color& other ) const
	{
		return ( _rgbaColor.x + _rgbaColor.y + _rgbaColor.z + _rgbaColor.w )
			<= ( other._rgbaColor.x + other._rgbaColor.y + other._rgbaColor.z + other._rgbaColor.w );
	}

	bool Color::operator > ( _In_ const Color& other ) const
	{
		return ( _rgbaColor.x + _rgbaColor.y + _rgbaColor.z + _rgbaColor.w )
			> ( other._rgbaColor.x + other._rgbaColor.y + other._rgbaColor.z + other._rgbaColor.w );
	}

	bool Color::operator >= ( _In_ const Color& other ) const
	{
		return ( _rgbaColor.x + _rgbaColor.y + _rgbaColor.z + _rgbaColor.w )
			>= ( other._rgbaColor.x + other._rgbaColor.y + other._rgbaColor.z + other._rgbaColor.w );
	}


	// compound
	Color& Color::operator += ( _In_ const Color& other )
	{
		_rgbaColor.x += other._rgbaColor.x;
		_rgbaColor.y += other._rgbaColor.y;
		_rgbaColor.z += other._rgbaColor.z;
		_rgbaColor.w += other._rgbaColor.w;

		ToggleDirty( );

		return *this;
	}

	Color& Color::operator -= ( _In_ const Color& other )
	{
		_rgbaColor.x -= other._rgbaColor.x;
		_rgbaColor.y -= other._rgbaColor.y;
		_rgbaColor.z -= other._rgbaColor.z;
		_rgbaColor.w -= other._rgbaColor.w;

		ToggleDirty( );

		return *this;
	}

	Color& Color::operator *= ( _In_ Color& other )
	{
		if ( IsDirty( ) == true )
		{
			Clean( );
		}

		Vector4* otherColor = other.GetMultipliedColor( );

		_multipliedColor.x *= otherColor->x;
		_multipliedColor.y *= otherColor->y;
		_multipliedColor.z *= otherColor->z;
		_multipliedColor.w *= otherColor->w;

		_rgbaColor.x = _multipliedColor.x * 255;
		_rgbaColor.y = _multipliedColor.y * 255;
		_rgbaColor.z = _multipliedColor.z * 255;
		_rgbaColor.w = _multipliedColor.w * 255;

		return *this;
	}

	Color& Color::operator /= ( _In_ Color& other )
	{
		if ( IsDirty( ) == true )
		{
			Clean( );
		}

		Vector4* otherColor = other.GetMultipliedColor( );

		_multipliedColor.x /= otherColor->x;
		_multipliedColor.y /= otherColor->y;
		_multipliedColor.z /= otherColor->z;
		_multipliedColor.w /= otherColor->w;

		_rgbaColor.x = _multipliedColor.x * 255;
		_rgbaColor.y = _multipliedColor.y * 255;
		_rgbaColor.z = _multipliedColor.z * 255;
		_rgbaColor.w = _multipliedColor.w * 255;

		return *this;
	}

	Color Color::operator [] ( _In_ const int colorComponent )
	{
		switch ( colorComponent )
		{
		case 0: return _rgbaColor.x;
		case 1: return _rgbaColor.y;
		case 2: return _rgbaColor.z;
		case 3: return _rgbaColor.w;
		default: return 0;
		};
	}

#pragma endregion
}