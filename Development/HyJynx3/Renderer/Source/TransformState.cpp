/**
 * TransformState.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\TransformState.h>

namespace HyJynxRenderer
{
	// null ctor
	TransformState::TransformState()
	{ }

	// copy ctor
	TransformState::TransformState( _In_ const TransformState& other )
		: Position( other.Position ),
		Scale( other.Scale ),
		Rotation( other.Rotation )
	{ }

	// move ctor
	TransformState::TransformState( _In_ const TransformState&& other )
		: Position( other.Position ),
		Scale( other.Scale ),
		Rotation( other.Rotation )
	{ }

	// dtor
	TransformState::~TransformState( )
	{ }

#pragma region Operators

	TransformState& TransformState::operator = ( _In_ const TransformState& other )
	{
		Position = other.Position;
		Scale = other.Scale;
		Rotation = other.Rotation;
		
		return *this;
	}

	TransformState TransformState::operator / ( _In_ const TransformState& other ) const
	{
		TransformState output;

		output.Position = Position / other.Position;
		output.Rotation = Position / other.Rotation;
		output.Scale = Position / other.Scale;

		return output;
	}

	TransformState TransformState::operator / ( _In_ const float value ) const
	{
		TransformState output;

		output.Position = Position / value;
		output.Rotation = Position / value;
		output.Scale = Position / value;

		return output;
	}

	TransformState TransformState::operator / ( _In_ const double value ) const
	{
		TransformState output;

		output.Position = Position / value;
		output.Rotation = Position / value;
		output.Scale = Position / value;

		return output;
	}

	TransformState TransformState::operator / ( _In_ const long value ) const
	{
		TransformState output;

		output.Position = Position / value;
		output.Rotation = Position / value;
		output.Scale = Position / value;

		return output;
	}

	TransformState TransformState::operator / ( _In_ const LongLong value ) const
	{
		TransformState output;

		output.Position = Position / value;
		output.Rotation = Position / value;
		output.Scale = Position / value;

		return output;
	}

	TransformState TransformState::operator * ( _In_ const TransformState& other ) const
	{
		TransformState output;

		output.Position = Position * other.Position;
		output.Rotation = Position * other.Rotation;
		output.Scale = Position * other.Scale;

		return output;
	}

	TransformState TransformState::operator * ( _In_ const float value ) const
	{
		TransformState output;

		output.Position = Position * value;
		output.Rotation = Position * value;
		output.Scale = Position * value;

		return output;
	}

	TransformState TransformState::operator * ( _In_ const double value ) const
	{
		TransformState output;

		output.Position = Position * value;
		output.Rotation = Position * value;
		output.Scale = Position * value;

		return output;
	}

	TransformState TransformState::operator * ( _In_ const long value ) const
	{
		TransformState output;

		output.Position = Position * value;
		output.Rotation = Position * value;
		output.Scale = Position * value;

		return output;
	}

	TransformState TransformState::operator * ( _In_ const LongLong value ) const
	{
		TransformState output;

		output.Position = Position * value;
		output.Rotation = Position * value;
		output.Scale = Position * value;

		return output;
	}


	TransformState& TransformState::operator /= ( _In_ const TransformState& other )
	{
		Position /= other.Position;
		Rotation /= other.Rotation;
		Scale /= other.Scale;

		return *this;
	}

	TransformState& TransformState::operator /= ( _In_ const float value )
	{
		Position /= value;
		Rotation /= value;
		Scale /= value;

		return *this;
	}

	TransformState& TransformState::operator /= ( _In_ const double value )
	{
		Position /= value;
		Rotation /= value;
		Scale /= value;

		return *this;
	}

	TransformState& TransformState::operator /= ( _In_ const long value )
	{
		Position /= value;
		Rotation /= value;
		Scale /= value;

		return *this;
	}

	TransformState& TransformState::operator /= ( _In_ const LongLong value )
	{
		Position /= value;
		Rotation /= value;
		Scale /= value;

		return *this;
	}

	TransformState& TransformState::operator *= ( _In_ const TransformState& other )
	{
		Position *= other.Position;
		Rotation *= other.Rotation;
		Scale *= other.Scale;

		return *this;
	}

	TransformState& TransformState::operator *= ( _In_ const float value )
	{
		Position *= value;
		Rotation *= value;
		Scale *= value;

		return *this;
	}

	TransformState& TransformState::operator *= ( _In_ const double value )
	{
		Position *= value;
		Rotation *= value;
		Scale *= value;

		return *this;
	}

	TransformState& TransformState::operator *= ( _In_ const long value )
	{
		Position *= value;
		Rotation *= value;
		Scale *= value;

		return *this;
	}

	TransformState& TransformState::operator *= ( _In_ const LongLong value )
	{
		Position *= value;
		Rotation *= value;
		Scale *= value;

		return *this;
	}


#pragma endregion
}