/**
 * DiffuseRenderSequence.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\DiffuseRenderSequence.h>

using namespace std;
using namespace HyJynxCore;
using namespace HyJynxCollections;

namespace HyJynxRenderer
{
	// default ctor
	DiffuseRenderSequence::DiffuseRenderSequence( )
		:IRenderSequence( "DiffuseRenderSequence" )
	{

	}

	// ensure the render targets we need are created and ready
	bool DiffuseRenderSequence::ValidateTargets( _In_ Dictionary<Text, ID3D11RenderTargetView*>* const renderTargets,
		_In_ Dictionary<HyJynxCore::Text, DepthStencil*>* const depthTargets )
	{
		bool good = true;

		if ( renderTargets->ContainsKey( "diffuse" ) == false )
		{
			good = false;
		}

		if ( renderTargets->ContainsKey( "depth" ) == false )
		{
			good = false;
		}

		return good;
	}

	// if we couldn't validate our render targets, initialize them
	bool DiffuseRenderSequence::InitializeTargets( _In_ Graphics* graphics, _In_ Dictionary<Text, ID3D11RenderTargetView*>* renderTargets,
		_In_ Dictionary<HyJynxCore::Text, DepthStencil*>* depthTargets )
	{
		ID3D11RenderTargetView* diffuse = graphics->CreateRenderTarget( );

		if ( diffuse == nullptr )
		{
			LogError( "InitializeTargets() - unable to create diffuse render target." );
			return false;
		}
		renderTargets->Append( "diffuse", diffuse );

		DepthStencil* depth = graphics->CreateDepthStencil( );

		if ( depth == nullptr )
		{
			LogError( "InitializeTargets() - unable to create depth stencil target." );
			return false;
		}
		depthTargets->Append( "depth", depth );

		return true;
	}

	// clear the targets we're using to a blank slate
	bool DiffuseRenderSequence::ClearTargets( _In_ Context* context,
		_In_ Dictionary<Text, ID3D11RenderTargetView*>* renderTargets,
		_In_ Dictionary<Text, DepthStencil*>* depthTargets,
		_In_opt_ Color* color )
	{
		ID3D11RenderTargetView* diffuse = renderTargets->ValueOf( "diffuse" );

		if ( diffuse == nullptr )
		{
			LogError( "ClearTargets() - diffuse texture is unavailable." );
			return false;
		}

		context->ClearScreen( diffuse, color );

		return true;
	}

	// loop through windows/drawables and render them to the screen
	bool DiffuseRenderSequence::Render( _In_ Context* context,
		_In_ HyJynxCollections::Dictionary<HyJynxCore::Text, ID3D11RenderTargetView*>* renderTargets,
		_In_ HyJynxCollections::Dictionary<HyJynxCore::Text, DepthStencil*>* depthTargets,
		_In_ BatchContainer* batchContainer )
	{
		context->BindTargets( renderTargets->ValueOf( "diffuse" ), depthTargets->ValueOf( "depth" ) );

		// TODO: re-order material batches for optimal performance and alpha-blending

		batchContainer->Render2DLayer( context );

		batchContainer->Render3DLayer( context );

		return true;
	}
}