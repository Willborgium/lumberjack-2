/**
 * SpotLight.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer/Headers/ljrc.h>
#include <Renderer/Headers/SpotLight.h>

namespace HyJynxRenderer
{
#pragma region init

	// null ctor
	SpotLight::SpotLight( )
		: ILight( )
	{ }

	// copy ctor
	SpotLight::SpotLight( _In_ const SpotLight& other )
		: ILight( other )
	{ }

	// move ctor
	SpotLight::SpotLight( _In_ const SpotLight&& other )
		: ILight( other )
	{ }

	// dtor
	SpotLight::~SpotLight( )
	{ }

#pragma endregion

#pragma region ILight

	// Get the main color of the light source
	const Color& SpotLight::GetDiffuseColor( ) const
	{
		return _diffuseColor;
	}

	// Set the main color of the light source
	void SpotLight::SetDiffuseColor( _In_ const Color& diffuse )
	{
		_diffuseColor = diffuse;
	}

	// Get the general intensity of the light
	const float SpotLight::GetIntensity( ) const
	{
		return _intensity;
	}

	// Get the general intensity of the light
	void SpotLight::SetIntensity( _In_ const float val )
	{
		_intensity = val;
	}

	// Get the Dimensions of the light source, used for Solf-Light calculations
	const Vector3& SpotLight::GetDimensions( ) const
	{
		return _dimensions;
	}

	// Set the dimensions of the light source, used for Solf-Light calculations
	void SpotLight::SetDimensions( _In_ const Vector3& dim )
	{
		_dimensions = dim;
	}

	// Get the general direction of the light
	const Vector3& SpotLight::GetNormal( ) const
	{
		return _direction;
	}

	// Set the general direction of the light
	void SpotLight::SetNormal( _In_ const Vector3& normal )
	{
		_direction = normal;
	}

	// Get the positional Transform of the light
	Transform* SpotLight::GetTransform( )
	{
		return &_transform;
	}

	// Get the total distance this light can cover
	const float SpotLight::GetDistance( ) const
	{
		return _distance;
	}

	// Set the total distance this light can cover
	void SpotLight::SetDistance( _In_ const float val )
	{
		_distance = val;
	}

	// Get where the light begins to falloff within it's distance (0-1 scale)
	const float SpotLight::GetFalloff( ) const
	{
		return _falloff;
	}

	// Set where the light begins to falloff within it's distance (0-1 scale)
	void SpotLight::SetFalloff( _In_ const float val )
	{
		_falloff = val;
	}

	// Determine if this light casts shadows on any receiving drawables
	bool SpotLight::CastShadows( )
	{
		return _castShadows;
	}

	// Set this light to cast shadows on any receiving drawables
	void SpotLight::SetCastShadows( _In_ const bool val )
	{
		_castShadows = val;
	}

	// Get the field of view of each axis 
	const Vector3& SpotLight::GetFieldOfView( ) const
	{
		return _fov;
	}

	// Set the field of view of each local axis
	void SpotLight::SetFieldOfView( _In_ const Vector3 val )
	{
		_fov = val;
	}

#pragma endregion ILight
}