/**
 * Matrix.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\Matrix.h>

using namespace DirectX;

namespace HyJynxRenderer
{

	// identity ctor
	Matrix::Matrix( )
		: XMFLOAT4X4( )
	{ }

	// copy ctor
	Matrix::Matrix( _In_ const Matrix& other )
		: XMFLOAT4X4( other )
	{ }

	// move ctor
	Matrix::Matrix( _In_ const Matrix&& other )
		: XMFLOAT4X4( other )
	{ }

	// create a identity matrix
	Matrix Matrix::Identity( )
	{
		XMMATRIX mat = XMMatrixIdentity( );
		Matrix output;
		XMStoreFloat4x4( &output, mat );
		return output;
	}

	// equality
	bool Matrix::operator == ( _In_ const Matrix& other ) const
	{
		return _11 == other._11
			&& _12 == other._12
			&& _13 == other._13
			&& _14 == other._14
			&& _21 == other._21
			&& _22 == other._22
			&& _23 == other._23
			&& _24 == other._24
			&& _31 == other._31
			&& _32 == other._32
			&& _33 == other._33
			&& _34 == other._34
			&& _41 == other._41
			&& _42 == other._42
			&& _43 == other._43
			&& _44 == other._44;
	}

	// equality
	bool Matrix::operator != ( _In_ const Matrix& other ) const
	{
		return !( *this == other );
	}

	// assignment
	Matrix& Matrix::operator = ( _In_ const Matrix& other )
	{
		_11 = other._11;
		_12 = other._12;
		_13 = other._13;
		_14 = other._14;
		_21 = other._21;
		_22 = other._22;
		_23 = other._23;
		_24 = other._24;
		_31 = other._31;
		_32 = other._32;
		_33 = other._33;
		_34 = other._34;
		_41 = other._41;
		_42 = other._42;
		_43 = other._43;
		_44 = other._44;
		return *this;
	}

	// multiply
	Matrix Matrix::operator * ( _In_ const Matrix& other )
	{
		XMMATRIX thisMat = XMLoadFloat4x4( this );
		XMMATRIX otherMat = XMLoadFloat4x4( &other );
		XMMATRIX output = XMMatrixMultiply( thisMat, otherMat );

		Matrix outputMat;
		XMStoreFloat4x4( &outputMat, output );

		return outputMat;
	}

	// compound multiply
	Matrix& Matrix::operator *= ( _In_ const Matrix& other )
	{
		XMMATRIX thisMat = XMLoadFloat4x4( this );
		XMMATRIX otherMat = XMLoadFloat4x4( &other );
		thisMat = XMMatrixMultiply( thisMat, otherMat );
		XMStoreFloat4x4( this, thisMat );
		return *this;
	}
}