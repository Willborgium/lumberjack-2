/**
 * Model.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\Model.h>

using namespace HyJynxCollections;
using namespace HyJynxCore;

namespace HyJynxRenderer
{
	// null ctor
	Model::Model( )
	{ }

	// copy ctor
	Model::Model( _In_ const Model& other )
		: _meshList( other._meshList )
	{ }

	// move ctor
	Model::Model( _In_ const Model&& other )
		: _meshList( other._meshList )
	{ }

	// dtor
	Model::~Model( )
	{
		_meshList.Clear( );
	}

	// register a mesh
	void Model::AddMesh( _In_ Mesh* mesh )
	{
		if ( mesh != nullptr )
		{
			_meshList.Append( mesh );
		}
	}

	// remove a registered mesh
	void Model::RemoveMesh( _In_ Mesh* mesh )
	{
		if ( mesh != nullptr )
		{
			_meshList.Remove( mesh );
		}
	}

	// get a pointer to the mesh list
	DynamicCollection<Mesh*>* Model::GetMeshList( )
	{
		return &_meshList;
	}

	// get a list of meshes valid for drawing at the given distance
	void Model::GetMeshAtLOD( _In_ DynamicCollection<Mesh*>* output, _In_ float distance ) const
	{
		if ( output == nullptr || _meshList.Count( ) == 0 )
		{
			return;
		}

		_meshList.ForEach( [distance, &output] ( _In_ Mesh* mesh )
		{
			if ( distance >= mesh->GetLOD( )->LODMinimum && distance <= mesh->GetLOD( )->LODMaximum )
			{
				output->Append( mesh );
			}
		} );
	}

	// apply a material to the mesh(es) with the given name
	void Model::ApplyMaterial( _In_ Material* material, _In_ Text name )
	{
		DynamicCollection< Mesh* >* meshList = &_meshList;

		if ( name.IsNullOrEmpty( ) == false )
		{
			meshList = meshList->Where( [&name] ( _In_ Mesh* mesh )
			{
				return mesh->GetName( ) == name;
			} );
		}

		if ( meshList != nullptr )
		{
			meshList->ForEach( [&material, &name] ( _In_ Mesh* mesh )
			{
				mesh->SetMaterial( material );
			} );
		}
	}

	// apply a diffuse texture to the mesh(es) with the given name
	void Model::ApplyDiffuseTexture( _In_ Texture2D* diffuse, _In_ Text name )
	{
		DynamicCollection< Mesh* >* meshList = &_meshList;

		if ( name.IsNullOrEmpty( ) == false )
		{
			meshList = meshList->Where( [&name] ( _In_ Mesh* mesh )
			{
				return mesh->GetName( ) == name;
			} );
		}

		if ( meshList != nullptr )
		{
			meshList->ForEach( [&diffuse, &name] ( _In_ Mesh* mesh )
			{
				if ( mesh->GetMaterial( ) != nullptr )
				{
					mesh->GetMaterial( )->SetDiffuseTexture( diffuse );
				}
			} );
		}
	}

	// get number of meshes
	UInt Model::GetMeshCount( ) const
	{
		return _meshList.Count( );
	}

	// get a vertex buffer of the mesh of the given index
	VertexBuffer* Model::GetVertexBufferAtIndex( _In_opt_ UInt index )
	{
		if ( index < _meshList.Count( ) )
		{
			return _meshList[ index ]->GetVertexBuffer( );
		}
		else return nullptr;
	}
}