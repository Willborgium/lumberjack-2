/**
 * PrimitiveConfiguration.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\PrimitiveConfiguration.h>

using namespace HyJynxUtilities;
using namespace HyJynxCore;

namespace HyJynxRenderer
{
	// null ctor
	PrimitiveConfiguration::PrimitiveConfiguration( )
		: Dirty( false )
	{ }

	// copy ctor
	PrimitiveConfiguration::PrimitiveConfiguration( _In_ const PrimitiveConfiguration& other )
		: Dirty( false )
	{ }

	// copy ctor
	PrimitiveConfiguration::PrimitiveConfiguration( _In_ const PrimitiveConfiguration&& other )
		: Dirty( false )
	{ }

	// dtor
	PrimitiveConfiguration::~PrimitiveConfiguration( )
	{ }
}