/**
 * Vector2.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\Vector2.h>

using namespace DirectX;

namespace HyJynxRenderer
{
	// null ctor
	Vector2::Vector2( )
		: XMFLOAT2( 0.0f, 0.0f )
	{ }

	// single value ctor
	Vector2::Vector2( _In_ const float val )
		: XMFLOAT2( val, val )
	{ }

	// full defined ctor
	Vector2::Vector2( _In_ const float x, _In_ const float y )
		: XMFLOAT2( x, y )
	{ }

	Vector2::Vector2( _In_ const Vector3& input )
		: XMFLOAT2( input.x, input.y )
	{ }

	// copy ctor
	Vector2::Vector2( _In_ const Vector2& other )
		: XMFLOAT2( other )
	{ }

	// move ctor
	Vector2::Vector2( _In_ const Vector2&& other )
		: XMFLOAT2( other )
	{ }

	// get a vector2 set to 1
	Vector2 Vector2::One( )
	{
		return Vector2( 1.0f, 1.0f );
	}

	// get a vector2 set to 0
	Vector2 Vector2::Zero( )
	{
		return Vector2( );
	}

	// Arithmetic
	Vector2 Vector2::operator = ( _In_ const Vector2& other )
	{
		x = other.x;
		y = other.y;
		return *this;
	}

	Vector2 Vector2::operator * ( _In_ const Vector2& other ) const
	{
		return Vector2( x * other.x, y * other.y );
	}

	Vector2 Vector2::operator / ( _In_ const Vector2& other ) const
	{
		return Vector2( x / other.x, y / other.y );
	}

	Vector2 Vector2::operator / ( _In_ const float val ) const
	{
		return Vector2( x / val, y / val );
	}

	Vector2 Vector2::operator - ( _In_ const Vector2& other ) const
	{
		return Vector2( x - other.x, y - other.y );
	}

	Vector2 Vector2::operator + ( _In_ const Vector2& other ) const
	{
		return Vector2( x + other.x, y + other.y );
	}

	Vector2& Vector2::operator ++ ( )
	{
		x++;
		y++;
		return *this;
	}

	Vector2& Vector2::operator -- ( )
	{
		x--;
		y--;
		return *this;
	}

	// Comparison
	bool Vector2::operator == ( _In_ const Vector2& other ) const
	{
		return x == other.x && y == other.y;
	}

	bool Vector2::operator != ( _In_ const Vector2& other ) const
	{
		return x != other.x && y != other.y;
	}

	bool Vector2::operator > ( _In_ const Vector2& other ) const
	{
		return ( x + y ) > ( other.x + other.y );
	}

	bool Vector2::operator >= ( _In_ const Vector2& other ) const
	{
		return ( x + y ) >= ( other.x + other.y );
	}

	bool Vector2::operator < ( _In_ const Vector2& other ) const
	{
		return ( x + y ) < ( other.x + other.y );
	}

	bool Vector2::operator <= ( _In_ const Vector2& other ) const
	{
		return ( x + y ) <= ( other.x + other.y );
	}

	// Compound
	Vector2& Vector2::operator += ( _In_ const Vector2& other )
	{
		x += other.x;
		y += other.y;
		return *this;
	}

	Vector2& Vector2::operator -= ( _In_ const Vector2& other )
	{
		x -= other.x;
		y -= other.y;
		return *this;
	}

	Vector2& Vector2::operator *= ( _In_ const Vector2& other )
	{
		x *= other.x;
		y *= other.y;
		return *this;
	}

	Vector2& Vector2::operator /= ( _In_ const Vector2& other )
	{
		x /= other.x;
		y /= other.y;
		return *this;
	}

	// Member
	float Vector2::operator [] ( _In_ const UInt index ) const
	{
		switch ( index )
		{
		case 0: return x;
		case 1: return y;
		}

		return 0.0f;
	}
}