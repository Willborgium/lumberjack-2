/**
 * EnvironmentController.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\EnvironmentController.h>

namespace HyJynxRenderer
{
	// null ctor
	EnvironmentController::EnvironmentController( )
	{ }

	// copy ctor
	EnvironmentController::EnvironmentController( _In_ const EnvironmentController& )
	{ }

	// move ctor
	EnvironmentController::EnvironmentController( _In_ const EnvironmentController&& )
	{ }

	// dtor
	EnvironmentController::~EnvironmentController( )
	{ }
}