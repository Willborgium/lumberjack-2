/**
 * RendererConfiguration.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\RendererConfiguration.h>

using namespace HyJynxCore;

namespace HyJynxRenderer
{
	// null ctor
	RendererConfiguration::RendererConfiguration( )
		:Dirty( false )
	{
		// cast?
		//AddMapping( new IntConfigurationMapping( "Renderer.FrustumCullMethod", &_frustumCullMethod ) );
	}

	// copy ctor
	RendererConfiguration::RendererConfiguration( _In_ const RendererConfiguration& other )
		: _frustumCullMethod( other._frustumCullMethod )
	{ }

	// move ctor
	RendererConfiguration::RendererConfiguration( _In_ const RendererConfiguration&& other )
		: _frustumCullMethod( other._frustumCullMethod )
	{ }

	// dtor
	RendererConfiguration::~RendererConfiguration( )
	{ }

	// assignment operator
	RendererConfiguration& RendererConfiguration::operator = ( _In_ const RendererConfiguration& other )
	{
		_frustumCullMethod = other._frustumCullMethod;

		return *this;
	}

	// return the method used to cull objects
	CollisionMethod RendererConfiguration::GetFrustumCullMethod( ) const
	{
		return _frustumCullMethod;
	}

	// set how we're culling objects
	void RendererConfiguration::SetFrustumCullMethod( _In_ const CollisionMethod method )
	{
		_frustumCullMethod = method;
	}
}