/**
 * OrientedBoundingBox.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\OrientedBoundingBox.h>
#include <Renderer\Headers\Matrix3.h>
#include <Renderer\Headers\RenderMath.h>

using namespace DirectX;

namespace HyJynxRenderer
{
	// null ctor
	OrientedBoundingBox::OrientedBoundingBox()
	{
		XMMATRIX v = XMMatrixIdentity( );
		XMStoreFloat4x4( &_world, v );
	}

	// defined ctor
	OrientedBoundingBox::OrientedBoundingBox( _In_ const Vector3 min, _In_ const Vector3 max )
		:_minimum( min ),
		_maximum( max )
	{
		XMMATRIX v = XMMatrixIdentity( );
		XMStoreFloat4x4( &_world, v );

		updateFromMinMax( );
	}

	// copy ctor
	OrientedBoundingBox::OrientedBoundingBox( _In_ const OrientedBoundingBox& other )
		:_minimum( other._minimum ),
		_maximum( other._maximum ),
		_world( other._world )
	{
		updateFromMinMax( );
	}

	// move ctor
	OrientedBoundingBox::OrientedBoundingBox( _In_ const OrientedBoundingBox&& other )
		: _minimum( other._minimum ),
		_maximum( other._maximum ),
		_world( other._world )
	{
		updateFromMinMax( );
	}

	// dtor
	OrientedBoundingBox::~OrientedBoundingBox()
	{ }

	// equality operator
	bool OrientedBoundingBox::operator == ( _In_ const OrientedBoundingBox& other ) const
	{
		return _minimum == other._minimum
			&& _maximum == other._maximum
			&& _world == other._world;
	}

	// inequality operator
	bool OrientedBoundingBox::operator != ( _In_ const OrientedBoundingBox& other ) const
	{
		return !( *this == other );
	}

	// update center/extents from current min/max values
	void OrientedBoundingBox::updateFromMinMax( )
	{
		XMVECTOR center = XMLoadFloat3( &_minimum );
		XMVECTOR extents = XMLoadFloat3( &_extents );
		XMVECTOR min = XMLoadFloat3( &_minimum );
		XMVECTOR max = XMLoadFloat3( &_maximum );

		center = ( min + max ) * 0.5;
		extents = ( max - min ) * 0.5;

		XMStoreFloat3( &_center, center );
		XMStoreFloat3( &_extents, extents );
	}

	// return the minimum position value
	Vector3* OrientedBoundingBox::GetMinimum( )
	{
		return &_minimum;
	}

	// return the maximum position value
	Vector3* OrientedBoundingBox::GetMaximum( )
	{
		return &_maximum;
	}

	// return the center positional value
	Vector3* OrientedBoundingBox::GetCenter( )
	{
		return &_center;
	}

	// return the extents of this obb
	Vector3* OrientedBoundingBox::GetExtents( )
	{
		return &_extents;
	}

	// intersection test against another OBB
	bool OrientedBoundingBox::Intersects( _In_ OrientedBoundingBox* other )
	{
		XMMATRIX myWorld = XMLoadFloat4x4( &_world );
		XMVECTOR myCenter = XMLoadFloat3( &_center );
		XMVECTOR myExtents = XMLoadFloat3( &_extents );

		XMMATRIX toMe = XMMatrixInverse( nullptr, myWorld );

		XMVECTOR centerOther = XMVector3Transform( XMLoadFloat3( other->GetCenter( ) ), toMe );
		XMVECTOR extentsOther = XMLoadFloat3( other->GetExtents( ) );

		XMVECTOR seperation = centerOther - myCenter;
		
		XMFLOAT4X4 temp;
		XMStoreFloat4x4( &temp, toMe );
		Matrix3 rotations = Matrix3( temp );
		Matrix3 absRotations = Matrix3( temp );
		absRotations.Absolute( );

		float r, r0, r1, r01;

		//--- Test case 1 - X axis
		r = RenderMath::Abs( XMVectorGetX( seperation ) );
		r1 = XMVectorGetX( XMVector3Dot( extentsOther, absRotations.GetColumn( 0 ) ) );
		r01 = XMVectorGetX( myExtents ) + r1;
		if ( r > r01 ) return false;

		//--- Test case 1 - Y axis
		r = RenderMath::Abs( XMVectorGetY( seperation ) );
		r1 = XMVectorGetX( XMVector3Dot( extentsOther, absRotations.GetColumn( 1 ) ) );
		r01 = XMVectorGetY( myExtents ) + r1;
		if ( r > r01 ) return false;

		//--- Test case 1 - Z axis
		r = RenderMath::Abs( XMVectorGetZ( seperation ) );
		r1 = XMVectorGetX( XMVector3Dot( extentsOther, absRotations.GetColumn( 2 ) ) );
		r01 = XMVectorGetZ( myExtents ) + r1;
		if ( r > r01 ) return false;

		//--- Test case 2 - X axis
		r = RenderMath::Abs( XMVectorGetX( XMVector3Dot( rotations.GetRow( 0 ), seperation ) ) );
		r0 = XMVectorGetX( XMVector3Dot( myExtents, absRotations.GetRow( 0 ) ) );
		r01 = r0 + XMVectorGetX( extentsOther );
		if ( r > r01 ) return false;

		//--- Test case 2 - Y axis
		r = RenderMath::Abs( XMVectorGetX( XMVector3Dot( rotations.GetRow( 0 ), seperation ) ) );
		r0 = XMVectorGetX( XMVector3Dot( myExtents, absRotations.GetRow( 0 ) ) );
		r01 = r0 + XMVectorGetY( extentsOther );
		if ( r > r01 ) return false;

		//--- Test case 2 - Z axis
		r = RenderMath::Abs( XMVectorGetX( XMVector3Dot( rotations.GetRow( 0 ), seperation ) ) );
		r0 = XMVectorGetX( XMVector3Dot( myExtents, absRotations.GetRow( 2 ) ) );
		r01 = r0 + XMVectorGetZ( extentsOther );
		if ( r > r01 ) return false;

		//--- Test case 3 # 1
		r = RenderMath::Abs( XMVectorGetZ( seperation ) * rotations.GetValue( 0, 1 ) - XMVectorGetY( seperation ) * rotations.GetValue( 0, 2 ) );
		r0 = XMVectorGetY( myExtents ) * absRotations.GetValue( 0, 2 ) + XMVectorGetZ( myExtents ) * absRotations.GetValue( 0, 1 );
		r1 = XMVectorGetY( extentsOther ) * absRotations.GetValue( 2, 0 ) + XMVectorGetZ( extentsOther ) * absRotations.GetValue( 1, 0 );
		r01 = r0 + r1;
		if ( r > r01 ) return false;

		//--- Test case 3 # 2
		r = RenderMath::Abs( XMVectorGetZ( seperation ) * rotations.GetValue( 1, 1 ) - XMVectorGetY( seperation ) * rotations.GetValue( 1, 2 ) );
		r0 = XMVectorGetY( myExtents ) * absRotations.GetValue( 1, 2 ) + XMVectorGetZ( myExtents ) * absRotations.GetValue( 1, 1 );
		r1 = XMVectorGetX( extentsOther ) * absRotations.GetValue( 2, 0 ) + XMVectorGetZ( extentsOther ) * absRotations.GetValue( 0, 0 );
		r01 = r0 + r1;
		if ( r > r01 ) return false;

		//--- Test case 3 # 3
		r = RenderMath::Abs( XMVectorGetZ( seperation ) * rotations.GetValue( 2, 1 ) - XMVectorGetY( seperation ) * rotations.GetValue( 2, 2 ) );
		r0 = XMVectorGetY( myExtents ) * absRotations.GetValue( 2, 2 ) + XMVectorGetZ( myExtents ) * absRotations.GetValue( 2, 1 );
		r1 = XMVectorGetX( extentsOther ) * absRotations.GetValue( 1, 0 ) + XMVectorGetY( extentsOther ) * absRotations.GetValue( 0, 0 );
		r01 = r0 + r1;
		if ( r > r01 ) return false;

		//--- Test case 3 # 4
		r = RenderMath::Abs( XMVectorGetX( seperation ) * rotations.GetValue( 0, 2 ) - XMVectorGetZ( seperation ) * rotations.GetValue( 0, 0 ) );
		r0 = XMVectorGetX( myExtents ) * absRotations.GetValue( 0, 2 ) + XMVectorGetZ( myExtents ) * absRotations.GetValue( 0, 0 );
		r1 = XMVectorGetY( extentsOther ) * absRotations.GetValue( 2, 1 ) + XMVectorGetZ( extentsOther ) * absRotations.GetValue( 1, 1 );
		r01 = r0 + r1;
		if ( r > r01 ) return false;

		//--- Test case 3 # 5
		r = RenderMath::Abs( XMVectorGetX( seperation ) * rotations.GetValue( 1, 2 ) - XMVectorGetZ( seperation ) * rotations.GetValue( 1, 0 ) );
		r0 = XMVectorGetX( myExtents ) * absRotations.GetValue( 1, 2 ) + XMVectorGetZ( myExtents ) * absRotations.GetValue( 1, 0 );
		r1 = XMVectorGetX( extentsOther ) * absRotations.GetValue( 2, 1 ) + XMVectorGetZ( extentsOther ) * absRotations.GetValue( 0, 1 );
		r01 = r0 + r1;
		if ( r > r01 ) return false;

		//--- Test case 3 # 6
		r = RenderMath::Abs( XMVectorGetX( seperation ) * rotations.GetValue( 2, 2 ) - XMVectorGetZ( seperation ) * rotations.GetValue( 2, 0 ) );
		r0 = XMVectorGetX( myExtents ) * absRotations.GetValue( 2, 2 ) + XMVectorGetZ( myExtents ) * absRotations.GetValue( 2, 0 );
		r1 = XMVectorGetX( extentsOther ) * absRotations.GetValue( 1, 1 ) + XMVectorGetY( extentsOther ) * absRotations.GetValue( 0, 1 );
		r01 = r0 + r1;
		if ( r > r01 ) return false;

		//--- Test case 3 # 7
		r = RenderMath::Abs( XMVectorGetY( seperation ) * rotations.GetValue( 0, 0 ) - XMVectorGetX( seperation ) * rotations.GetValue( 0, 1 ) );
		r0 = XMVectorGetX( myExtents ) * absRotations.GetValue( 0, 1 ) + XMVectorGetY( myExtents ) * absRotations.GetValue( 0, 0 );
		r1 = XMVectorGetY( extentsOther ) * absRotations.GetValue( 2, 2 ) + XMVectorGetZ( extentsOther ) * absRotations.GetValue( 1, 2 );
		r01 = r0 + r1;
		if ( r > r01 ) return false;

		//--- Test case 3 # 8
		r = RenderMath::Abs( XMVectorGetY( seperation ) * rotations.GetValue( 1, 0 ) - XMVectorGetX( seperation ) * rotations.GetValue( 1, 1 ) );
		r0 = XMVectorGetX( myExtents ) * absRotations.GetValue( 1, 1 ) + XMVectorGetY( myExtents ) * absRotations.GetValue( 1, 0 );
		r1 = XMVectorGetX( extentsOther ) * absRotations.GetValue( 2, 2 ) + XMVectorGetZ( extentsOther ) * absRotations.GetValue( 0, 2 );
		r01 = r0 + r1;
		if ( r > r01 ) return false;

		//--- Test case 3 # 9
		r = RenderMath::Abs( XMVectorGetY( seperation ) * rotations.GetValue( 2, 0 ) - XMVectorGetX( seperation ) * rotations.GetValue( 2, 1 ) );
		r0 = XMVectorGetX( myExtents ) * absRotations.GetValue( 2, 1 ) + XMVectorGetY( myExtents ) * absRotations.GetValue( 2, 0 );
		r1 = XMVectorGetX( extentsOther ) * absRotations.GetValue( 1, 2 ) + XMVectorGetY( extentsOther ) * absRotations.GetValue( 0, 2 );
		r01 = r0 + r1;
		if ( r > r01 ) return false;

		return true;
	}
}