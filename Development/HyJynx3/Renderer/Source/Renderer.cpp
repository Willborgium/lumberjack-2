/**
 * Renderer.cpp
 * (c) 2014 All Rights Reserved
 */


#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\Renderer.h>
#include <Renderer\Headers\RenderSequenceNull.h>
#include <Renderer\Headers\Buffer.h>
#include <Renderer\Headers\ShaderFactory.h>
#include <Renderer\Headers\TextureManager.h>
#include <Renderer\Headers\Animator.h>
#include <Engine\Headers\KeyValuePair.h>
#include <Logger\Headers\Logger.h>

using namespace std;
using namespace DirectX;
using namespace HyJynxCore;
using namespace HyJynxCollections;
using namespace HyJynxLogger;

namespace HyJynxRenderer
{
	// Rasterizer name definitions
	const Text Renderer::RasterizerState::FrontFaceSolid =			"FrontFaceSolid";
	const Text Renderer::RasterizerState::Solid2DManualDepth =		"Solid2DManualDepth";
	const Text Renderer::RasterizerState::WireFrame =				"WireFrame";
	const Text Renderer::RasterizerState::WireFrameManualDepth =	"WireFrameManualDepth";

	// null ctor
	Renderer::Renderer( )
		:ILog( " ** Renderer ** " ),
		_graphics( ),
		_renderSequence( anew (RenderSequenceNull)( ) )
	{ }

	// dtor
	Renderer::~Renderer( )
	{
		Shutdown( );
	}

	// shutdown everything 
	void Renderer::Shutdown( )
	{
		// windows
		_windowList.ForEach( [ ] ( _In_ Window* window )
		{
			window->Release( );
		} );
		_windowList.Clear( );
		
		// rasterizers
		_rasterizerStates.ForEach( [ ] ( _In_ KeyValuePair<Text, ID3D11RasterizerState*> state )
		{
			state.Value->Release( );
		} );
		_rasterizerStates.Clear( );

		// components
		ReleaseRenderTargets( );
		_graphics.Shutdown( );
		_runTimer.Stop( );
		_activeCamera = nullptr;
		_renderSequence = nullptr;
	}

	// get children loggers composed here
	void Renderer::GetChildrenLoggers( _In_ DynamicCollection< ILog* >* const children )
	{
		children->Append( &_graphics );
		children->Append( ShaderFactory::Instance( ) );
		children->Append( TextureManager::Instance( ) );
		children->Append( Animator::Instance( ) );
	}

	// get the active camera
	Camera* Renderer::GetActiveCamera( ) const
	{
		return _activeCamera;
	}

	// set a new camera as active
	void Renderer::SetActiveCamera( _In_ Camera* camera )
	{
		_activeCamera = camera;
	}

	// load the different rasterizer states
	bool _initializeRasterizerStates( _In_ ID3D11Device* device, _In_ RasterizerStateList* states )
	{
		ID3D11RasterizerState* rasterState;

		// TODO: optimize these when more relevant systems are running

		D3D11_RASTERIZER_DESC rasterDesc;
		rasterDesc.AntialiasedLineEnable = false;
		rasterDesc.CullMode = D3D11_CULL_MODE::D3D11_CULL_BACK;
		rasterDesc.DepthBias = 0;
		rasterDesc.DepthBiasClamp = 0.0f;
		rasterDesc.DepthClipEnable = true;
		rasterDesc.FillMode = D3D11_FILL_MODE::D3D11_FILL_SOLID;
		rasterDesc.FrontCounterClockwise = false;
		rasterDesc.MultisampleEnable = true;
		rasterDesc.ScissorEnable = false;
		rasterDesc.SlopeScaledDepthBias = 0.0f;

		// - default front face
		if ( FAILED( device->CreateRasterizerState( &rasterDesc, &rasterState ) ) )
		{
			return false;
		}

		states->Append( Renderer::RasterizerState::FrontFaceSolid, rasterState );
		rasterState = nullptr;

		rasterDesc.DepthClipEnable = false;
		rasterDesc.CullMode = D3D11_CULL_MODE::D3D11_CULL_NONE;

		// - default front face with depth test off
		if ( FAILED( device->CreateRasterizerState( &rasterDesc, &rasterState ) ) )
		{
			states->ForEach( [ ] ( _In_ KeyValuePair<Text, ID3D11RasterizerState*> state )
			{
				state.Value->Release( );
			} );
			return false;
		}
		states->Append( Renderer::RasterizerState::Solid2DManualDepth, rasterState );
		rasterState = nullptr;

		// - wire frame with manual depth
		rasterDesc.FillMode = D3D11_FILL_MODE::D3D11_FILL_WIREFRAME;

		if ( FAILED( device->CreateRasterizerState( &rasterDesc, &rasterState ) ) )
		{
			states->ForEach( [ ] ( _In_ KeyValuePair<Text, ID3D11RasterizerState*> state )
			{
				state.Value->Release( );
			} );
			return false;
		}
		states->Append( Renderer::RasterizerState::WireFrameManualDepth, rasterState );
		rasterState = nullptr;

		// - wire frame with normal depth
		rasterDesc.DepthClipEnable = true;
		rasterDesc.CullMode = D3D11_CULL_MODE::D3D11_CULL_BACK;

		if ( FAILED( device->CreateRasterizerState( &rasterDesc, &rasterState ) ) )
		{
			states->ForEach( [ ] ( _In_ KeyValuePair<Text, ID3D11RasterizerState*> state )
			{
				state.Value->Release( );
			} );
			return false;
		}
		states->Append( Renderer::RasterizerState::WireFrame, rasterState );

		return true;
	}

	// initialize DX11 graphics
	bool Renderer::Initialize( _In_ HWND* window, _In_ GraphicsConfiguration* gConfig, _In_ RendererConfiguration* rConfig )
	{
		_screen = Screen( window );

		if ( gConfig != nullptr )
		{
			_graphics.SetConfiguration( gConfig );
		}
		else
		{
			LogWarning( "Initialize( window, nullptr, rConfig ) - graphics config is null, using defaults." );
		}

		if ( rConfig != nullptr )
		{
			_config = *rConfig;
		}
		else
		{
			LogWarning( "Initialize( window, rconfig, nullptr ) - render config is null, using defaults." );
		}

		if ( _graphics.Initialize( _screen.GetWindow( ) ) == false )
		{
			LogError( "Graphics Library failed to initialize." );
			return false;
		}

		if ( _renderSequence->InitializeTargets( &_graphics, &_renderTargets, &_depthTargets ) == false )
		{
			LogError( "RenderSequence Failed to initialize Render Targets" );
			return false;
		}

		if ( ShaderFactory::Instance( )->Initialize( ) == false )
		{
			LogError( "ShaderFactory Failed to initialize." );
			return false;
		}

		if ( _initializeRasterizerStates( _graphics.GetDevice( ), &_rasterizerStates ) == false )
		{
			LogError( "Rasterizer States Failed to initialize." );
			return false;
		}

		_runTimer.SetInfiniteLooping( true );
		_runTimer.Start( );

		Logger::Log( "Renderer Initialized." );

		return true;
	}

	// reinitialize renderer with current graphics settings
	bool Renderer::ReInitialize( )
	{
		// TODO
		return true;
	}

	// register a window for rendering
	void Renderer::AddWindow( _In_ Window* window )
	{
		if ( window != nullptr )
		{
			_windowList.Append( window );
		}
		else
		{
			LogWarning( "AddWindow( nullptr ) - Window* was not added." );
		}
	}

	// remove a window from rendering
	void Renderer::RemoveWindow( _In_ Window* window )
	{
		if ( window != nullptr )
		{
			UINT sizeBefore = _windowList.Count( );

			_windowList.Remove( window );

			if ( sizeBefore == _windowList.Count( ) )
			{
				Text log = "RemoveWindow( name: ";
				log += window->GetName( );
				log += " ) - not found in windowList. 1)never added 2)dev. pointer error 3)renderer fatal.";
				LogWarning( log );
			}
		}
		else
		{
			LogWarning( "RemoveWindow( nullptr ) - no Window was removed." );
		}
	}

	// release all current render targets
	void Renderer::ReleaseRenderTargets( )
	{
		LogInfo( "ReleaseRenderTargets() --" );

		_renderTargets.ForEach( [this] ( _In_ KeyValuePair<Text, ID3D11RenderTargetView*> kvPair )
		{

			Text log = "Target: ";
			log += kvPair.Key;

			if ( kvPair.Value )
			{
				kvPair.Value->Release( );
				log += " Released.";
			}
			else
			{
				log += " Failed.";
			}

			LogInfo( log );
		} );
		_renderTargets.Clear( );

		_depthTargets.ForEach( [ ] ( _In_ KeyValuePair<Text, DepthStencil*> kvPair )
		{
			if ( kvPair.Value )
			{
				kvPair.Value->Release( );
			}
		} );
		_depthTargets.Clear( );
	}

	// get a loaded rasterizer
	ID3D11RasterizerState* Renderer::GetRasterizer( _In_ HyJynxCore::Text name )
	{
		if ( _rasterizerStates.ContainsKey( name ) == true )
		{
			return _rasterizerStates[ name ];
		}
		else
		{
			Text log = "GetRasterizer(";
			log += name;
			log += ") - Key does not exist, you can create your own rasterizer, or use Keys from RasterizerState.";
			LogWarning( log );
			return nullptr;
		}
	}

#pragma region Rendering

	// check to see if we're ready to render
	bool Renderer::PreRenderCheck( )
	{
		if ( _graphics.GetConfiguration()->IsDirty( ) == true )
		{
			LogInfo( "PreRenderCheck() - ReInitializing Graphics Library" );

			ReleaseRenderTargets( );

			if ( _graphics.ReInitialize( ) == false )
			{
				LogError( "PreRenderCheck() - unable to Reinitialize Graphics Library" );

				return false;
			}

			if ( _renderSequence->InitializeTargets( &_graphics, &_renderTargets, &_depthTargets ) == false )
			{
				LogError( "PreRenderCheck() - unable to Initialize Render Targets" );

				return false;
			}

			_graphics.GetConfiguration( )->ToggleClean( );
		}

		return true;
	}

	// kicks off a render sequence
	// In here we cull all our windows and it's drawables, and offload the batchContainer 
	// to the RenderSequence object, who has to do it's own customization, 
	// and actually call Drawable.Render()
	const RenderReport& Renderer::Render( )
	{
		_renderReport.MarkSequenceStart( );

		if ( PreRenderCheck( ) == false )
		{
			LogError( "Render() - Unable to Render, PreRenderCheck Failed." );

			_renderReport.SetRenderStatus( RENDER_STATUS_FAIL );

			return _renderReport;
		}

		Context* context = _graphics.GetDefaultContext( );

		if ( context == nullptr )
		{
			LogError( "Render() - Unable to Render, Graphics Library has an invalid Context. Check DX Initialization." );

			_renderReport.SetRenderStatus( RENDER_STATUS_FAIL );

			return _renderReport;
		}

		_renderSequence->ClearTargets( context, &_renderTargets, &_depthTargets, &_clearColor );

		// TODO: use actual timing values, and not dummy 1.0f
		CBCycle* cycleBound = context->GetCycleConstantBuffer( );
		cycleBound->Time.x = 1.0f;	// elapsed milliseconds
		cycleBound->Time.y = 1.0f;	// total milliseconds
		cycleBound->Time.z = 1.0f;	// reserved.
		cycleBound->Time.w = 1.0f;	// reserved.
		cycleBound->SkyColor = XMFLOAT4( 0.01f, 0.2f, 0.95f, 1.0f );
		cycleBound->GroundColor = XMFLOAT4( 0.4f, 0.2f, 0.6f, 1.0f );
		context->UpdateCycleConstantBuffer( );

		ViewFrustum* frustum = nullptr;

		if ( _activeCamera != nullptr )
		{
			_activeCamera->UpdateConstants( context );
			frustum = _activeCamera->GetViewFrustum( );
		}

		_renderReport.MarkUpdateStart( );

		BatchContainer batchContainer;
		CollisionMethod cullMethod = _config.GetFrustumCullMethod( );

		for ( UInt index = 0; index < _windowList.Count( ); index++ )
		{
			_windowList[ index ]->Update( context, _runTimer );
			if ( frustum != nullptr )
			{
				_windowList[ index ]->FrustumCull( *frustum, cullMethod, batchContainer );
			}
		}

		_renderReport.MarkUpdateEnd( );

		_renderReport.MarkRenderStart( );

		_renderSequence->Render( context, &_renderTargets, &_depthTargets, &batchContainer );

		_graphics.Preset( );

		_renderReport.MarkRenderEnd( );

		_renderReport.Finalize( );

		_renderReport.SetRenderStatus( RENDER_STATUS_OK );

		return _renderReport;
	}

	// graphics library return
	Graphics* Renderer::GetGraphicsLibrary( )
	{
		return &_graphics;
	}

#pragma endregion

#pragma region RenderSequence

	// return the render sequence interface being used
	IRenderSequence* Renderer::GetRenderSequence( ) const
	{
		return _renderSequence;
	}

	// set and initialize a new render sequence
	void Renderer::SetRenderSequence( _In_opt_ IRenderSequence* sequence )
	{
		if ( sequence != nullptr )
		{
			_renderSequence = sequence;
		}
		else
		{
			LogError( "SetRenderSequence(nullptr) - defaulting to RenderSequenceNull" );
			_renderSequence = anew (RenderSequenceNull)( );
		}

		if ( _renderSequence->ValidateTargets( &_renderTargets, &_depthTargets ) == false )
		{
			ReleaseRenderTargets( );

			if ( _renderSequence->InitializeTargets( &_graphics, &_renderTargets, &_depthTargets ) == false )
			{
				Text log = "SetRenderSequence( name: ";
				log += sequence->GetName( );
				log += " ) - unable to initialize Render Targets.";
				LogError( log );
				return;
			}
		}
	}

#pragma endregion

	// set renderer's clear color
	void Renderer::SetClearColor( _In_opt_ Color* color )
	{
		if ( color != nullptr )
		{
			_clearColor.SetColor( color );
		}
		else
		{
			_clearColor.SetColor( 0.0f );
		}
	}

	// get the width/height of the drawable screen
	Vector2 Renderer::GetScreenSize( ) const
	{
		return Vector2( static_cast< float > ( _graphics.GetWidth( ) ), 
			static_cast< float > ( _graphics.GetHeight( ) ) );
	}

	// Determine the maximum directX the hardware supports
	D3D_FEATURE_LEVEL Renderer::GetDeviceFeatureLevel( )
	{
		return _graphics.GetDevice( )->GetFeatureLevel( );
	}

	// determine hardware support of the given DXGI format
	UInt Renderer::GetFormatSupport( _In_ DXGI_FORMAT format )
	{
		UInt output = 0;
		_graphics.GetDevice( )->CheckFormatSupport( format, &output );
		return output;
	}

	// change how renderer will perform frustum culling
	void Renderer::SetCullMethod( _In_ const CollisionMethod method )
	{
		_config.SetFrustumCullMethod( method );
	}
}