/**
* Texture2D.cpp
* (c) 2014 All Rights Reserved
*/

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\Texture2D.h>
#include <Renderer\Headers\TextureManager.h>

namespace HyJynxRenderer
{
	// null ctor
	Texture2D::Texture2D()
		: Texture( )
	{ }

	// default ctor
	Texture2D::Texture2D( _In_ LoadedTexture* tex )
		: Texture( tex )
	{ }

	// copy ctor
	Texture2D::Texture2D( _In_ const Texture2D& )
	{ }

	// move ctor
	Texture2D::Texture2D( _In_ const Texture2D&& )
	{ }

	// dtor
	Texture2D::~Texture2D( )
	{ }

	// local helper function to read from cpu data
	void Texture2D::ReadFromCpuData( _In_ LoadedTexture* texture, _Out_ Color** output, _In_ Point3 indicies[ ], _In_ UInt count )
	{
		for ( UInt index = 0; index < count; index++ )
		{
			if ( indicies[ index ].X >= 0 && indicies[ index ].X < static_cast< int > ( texture->LoadedWidth )
				&& indicies[ index ].Y >= 0 && indicies[ index ].Y < static_cast< int > ( texture->LoadedHeight ) )
			{
				*output[ index ] = Color( ); // TODO: cast cpu data at the given index
			}
			else
			{
				*output[ index ] = Color( );
			}
		}
	}

	// local helper function to read from gpu data
	void Texture2D::ReadFromGpuData( _In_ Context* context, _In_ LoadedTexture* texture, _Out_ Color** output, _In_ Point3 indicies[ ], _In_ UInt count )
	{
		// TODO: map gpu data, and cast/create colors
	}
}