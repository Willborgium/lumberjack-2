/**
 * Material.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\Material.h>
#include <Renderer\Headers\RenderMath.h>

using namespace HyJynxCore;
using namespace HyJynxLogger;

namespace HyJynxRenderer
{
	// null ctor
	Material::Material( )
		: ILog( "Material" )
	{ }

	// default ctor
	Material::Material( _In_ int id )
		: ILog( "Material" ),
		_materialID( id )
	{ }

	// copy ctor
	Material::Material( _In_ const Material& other )
		: ILog( "Material" ),
		_materialID( other._materialID ),
		_shader( other._shader )
	{ }

	// move ctor
	Material::Material( _In_ const Material&& other )
		: ILog( "Material" ),
		_materialID( other._materialID ),
		_shader( other._shader )
	{ }

	// dtor
	Material::~Material( )
	{
		_materialID = MaterialLibrary::Material_Unknown;
		if ( _shader != nullptr )
		{
			_shader->Release( );
			_shader = nullptr;
		}
	}

	// get composed loggers
	void Material::GetChildrenLoggers( _In_ HyJynxCollections::DynamicCollection< ILog* >* const children )
	{
		if ( _shader != nullptr )
		{
			children->Append( _shader );
		}
	}

	// determine if the shader is in a renderable state
	bool Material::IsValid( ) const
	{
		return _shader != nullptr;
	}

	// get the ID of this material type
	int Material::GetID( ) const
	{
		return _materialID;
	}

	// retrieve the shader
	Shader* const Material::GetShader( )
	{
		return _shader;
	}

	// set the diffuse texture
	void Material::SetDiffuseTexture( _In_ Texture2D* diffuse )
	{
		_diffuseTexture = diffuse;
	}

	// get diffuse texture
	Texture2D* Material::GetDiffuseTexture( )
	{
		return _diffuseTexture;
	}

	// set the normal map texture
	void Material::SetNormalTexture( _In_ Texture2D* normals )
	{
		_normalTexture = normals;
	}

	// get the normal map texture
	Texture2D* Material::GetNormalTexture( )
	{
		return _normalTexture;
	}

	// set the environment texture
	void Material::SetEnvironmentTexture( _In_ Texture3D* env )
	{
		_environmentTexture = env;
	}

	Texture3D* Material::GetEnvironmentTexture( )
	{
		return _environmentTexture;
	}
}