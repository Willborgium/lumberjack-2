/**
 * Vector4.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\Vector4.h>

using namespace DirectX;

namespace HyJynxRenderer
{
	// null ctor
	Vector4::Vector4( )
		: XMFLOAT4( )
	{ }

	// single value ctor
	Vector4::Vector4( _In_ const float val )
		: XMFLOAT4( val, val, val, val )
	{ }

	// XYZ ctor
	Vector4::Vector4( _In_ const float x, _In_ const float y, _In_ const float z )
		: XMFLOAT4( x, y, z, 1.0f )
	{ }

	// full ctor
	Vector4::Vector4( _In_ const float x, _In_ const float y, _In_ const float z, _In_ const float w )
		: XMFLOAT4( x, y, z, w )
	{ }

	// full ctor
	Vector4::Vector4( _In_ const Vector3 xyz, _In_ const float w )
		: XMFLOAT4( xyz.x, xyz.y, xyz.z, w )
	{ }

	// copy ctor
	Vector4::Vector4( _In_ const Vector4& other )
		: XMFLOAT4( other )
	{ }

	// move ctor
	Vector4::Vector4( _In_ const Vector4&& other )
		: XMFLOAT4( other )
	{ }

	// get a vector4 set to 1
	Vector4 Vector4::One( )
	{
		return Vector4( 1.0f, 1.0f, 1.0f, 1.0f );
	}

	// get a vector4 set to 0
	Vector4 Vector4::Zero( )
	{
		return Vector4( );
	}

	// recieve normalized copy
	Vector4 Vector4::Normalize( _In_ const Vector4& other )
	{
		Vector4 output = other;
		output.Normalize( );
		return output;
	}

	// normalize ourselves
	void Vector4::Normalize( )
	{
		float length = std::sqrt( ( x*x ) + ( y*y ) + ( z*z ) + ( w*w ) );
		if ( length != 0 )
		{
			x /= length;
			y /= length;
			z /= length;
			w /= length;
		}
	}

	// normalize the given dx vector
	XMFLOAT4 Vector4::Normalize( _In_ const XMFLOAT4& other )
	{
		XMFLOAT4 output = other;
		float length = std::sqrt( ( output.x*output.x ) + ( output.y*output.y ) 
			+ ( output.z*output.z ) + ( output.w*output.w ) );
		if ( length != 0 )
		{
			output.x /= length;
			output.y /= length;
			output.z /= length;
			output.w /= length;
		}
		return output;
	}

	// Arithmetic 
	Vector4 Vector4::operator = ( _In_ const Vector4& other )
	{
		x = other.x;
		y = other.y;
		z = other.z;
		w = other.w;
		return *this;
	}

	Vector4 Vector4::operator * ( _In_ const Vector4& other ) const
	{
		return Vector4( x * other.x, y * other.y, z * other.z, w * other.w );
	}

	Vector4 Vector4::operator / ( _In_ const Vector4& other ) const
	{
		return Vector4( x / other.x, y / other.y, z / other.z, w / other.w );
	}

	Vector4& Vector4::operator ++ ( )
	{
		x++;
		y++;
		z++;
		w++;
		return *this;
	}

	Vector4& Vector4::operator -- ( )
	{
		x--;
		y--;
		z--;
		w--;
		return *this;
	}


	// Comparison
	bool Vector4::operator == ( _In_ const Vector4& other ) const
	{
		return x == other.x && y == other.y && z == other.z && w == other.w;
	}

	bool Vector4::operator != ( _In_ const Vector4& other ) const
	{
		return x != other.x && y != other.y && z != other.z && w != other.w;
	}

	bool Vector4::operator > ( _In_ const Vector4& other ) const
	{
		return ( x + y + z + w ) > ( other.x + other.y + other.z + other.w );
	}

	bool Vector4::operator >= ( _In_ const Vector4& other ) const
	{
		return ( x + y + z + w ) >= ( other.x + other.y + other.z + other.w );
	}

	bool Vector4::operator < ( _In_ const Vector4& other ) const
	{
		return ( x + y + z + w ) < ( other.x + other.y + other.z + other.w);
	}

	bool Vector4::operator <= ( _In_ const Vector4& other ) const
	{
		return ( x + y + z + w ) <= ( other.x + other.y + other.z + other.w );
	}


	// Compound
	Vector4& Vector4::operator += ( _In_ const Vector4& other )
	{
		x += other.x;
		y += other.y;
		z += other.z;
		w += other.w;
		return *this;
	}

	Vector4& Vector4::operator -= ( _In_ const Vector4& other )
	{
		x -= other.x;
		y -= other.y;
		z -= other.z;
		w -= other.w;
		return *this;
	}

	Vector4& Vector4::operator *= ( _In_ const Vector4& other )
	{
		x *= other.x;
		y *= other.y;
		z *= other.z;
		w *= other.w;
		return *this;
	}

	Vector4& Vector4::operator /= ( _In_ const Vector4& other )
	{
		x /= other.x;
		y /= other.y;
		z /= other.z;
		w /= other.w;
		return *this;
	}


	// Member
	float Vector4::operator [] ( _In_ const UInt index ) const
	{
		switch ( index )
		{
		case 0: return x;
		case 1: return y;
		case 2: return z;
		case 3: return w;
		}
		return 0.0f;
	}
}