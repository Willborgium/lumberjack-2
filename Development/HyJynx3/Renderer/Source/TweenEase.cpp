/**
 * TweenEase.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\TweenEase.h>
#include <Renderer\Headers\RenderMath.h>

namespace HyJynxRenderer
{
	// null ctor
	TweenEase::TweenEase( )
	{ }

	// default ctor
	TweenEase::TweenEase( _In_ const double power )
		: _power( power )
	{ }

	// copy ctor
	TweenEase::TweenEase( _In_ const TweenEase& other )
		: _power( other._power )
	{ }

	// move ctor
	TweenEase::TweenEase( _In_ const TweenEase&& other )
		: _power( other._power )
	{ }

	// dtor
	TweenEase::~TweenEase( )
	{ }

	// per-cycle update
	void TweenEase::Update( _In_ const double total, _In_ const double delta, _In_ const double maximum )
	{

	}
}