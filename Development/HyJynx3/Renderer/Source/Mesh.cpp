/**
 * Mesh.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\Mesh.h>

#include <Renderer\Headers\Buffer.h>
#include <Renderer\Headers\IDrawable.h>
#include <Renderer\Headers\Matrix.h>
#include <Renderer\Headers\Vector4.h>
#include <Renderer\Headers\Renderer.h>

using namespace DirectX;
using namespace HyJynxCore;
using namespace HyJynxCollections;

namespace HyJynxRenderer
{
	// null ctor
	Mesh::Mesh( )
	{ }

	// drawable ctor
	Mesh::Mesh( _In_ VertexBuffer* vBuffer, _In_ IndexBuffer* iBuffer )
		:_vertexBuffer( vBuffer ),
		_indexBuffer( iBuffer )
	{ }

	// drawable ctor with material
	Mesh::Mesh( _In_ VertexBuffer* vBuffer, _In_ IndexBuffer* iBuffer, _In_ Material* material )
		:_material( material ),
		_vertexBuffer( vBuffer ),
		_indexBuffer( iBuffer )
	{ }

	// fully defined drawable ctor
	Mesh::Mesh( _In_ VertexBuffer* vBuffer, _In_ IndexBuffer* iBuffer, _In_ Material* material, _In_ LODLevel* lod )
		:_material( material ),
		_vertexBuffer( vBuffer ),
		_indexBuffer( iBuffer ),
		_lodLevel( lod )
	{ }

	// copy ctor
	Mesh::Mesh( _In_ const Mesh& other )
		:_material( other._material ),
		_vertexBuffer( other._vertexBuffer ),
		_indexBuffer( other._indexBuffer ),
		_lodLevel( other._lodLevel ),
		_name( other._name )
	{ }

	// move ctor
	Mesh::Mesh( _In_ const Mesh&& other )
		:_material( other._material ),
		_vertexBuffer( other._vertexBuffer ),
		_indexBuffer( other._indexBuffer ),
		_lodLevel( other._lodLevel ),
		_name( other._name )
	{ }

	// dtor
	Mesh::~Mesh( )
	{
		_material = nullptr;
		_vertexBuffer = nullptr;
		_indexBuffer = nullptr;
		_lodLevel = nullptr;
		_name = "";
	}

	// main render 
	void Mesh::Render( _In_ IDrawable* const owner, _In_ Context* const context )
	{
		if ( _vertexBuffer == nullptr && _indexBuffer == nullptr )
		{
			return;
		}

		context->BindVertexTopology( _material->GetShader()->GetInputLayout( ) );

		context->SetActiveBuffers( _vertexBuffer, _indexBuffer );

		context->SetPrimitiveTopology( D3D_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );

		DrawableRenderMode mode = owner->GetRenderMode( );

		Text previousRaster = context->GetCurrentRasterizerState( );

		context->DrawIndexed( _indexBuffer->GetCount( ), 0, 0 );

		if ( mode == DrawableRenderMode::DRAWABLE_RENDER_SOLID_WIREFRAME
			|| mode == DrawableRenderMode::DRAWABLE_RENDER_WIREFRAME )
		{
			context->SetRasterizer( Renderer::RasterizerState::WireFrameManualDepth );
			context->DrawIndexed( _indexBuffer->GetCount( ), 0, 0 );
		}

		context->SetRasterizer( previousRaster );
	}

	// vertex buffer retrieval
	VertexBuffer* const Mesh::GetVertexBuffer( ) const
	{
		return _vertexBuffer;
	}

	// index buffer retrieval
	IndexBuffer* const Mesh::GetIndexBuffer( ) const
	{
		return _indexBuffer;
	}

	// material retrieval
	Material* const Mesh::GetMaterial( ) const
	{
		return _material;
	}

	// sets a new material to the mesh
	void Mesh::SetMaterial( _In_ Material* mat )
	{
		_material = mat;
	}

	// lod retrieval
	const LODLevel* const Mesh::GetLOD( ) const
	{
		return _lodLevel;
	}

	// get the name idnetifier of this mesh
	const HyJynxCore::Text& Mesh::GetName( ) const
	{
		return _name;
	}

	// override the loaded name identifer of this mesh with another
	void Mesh::SetName( _In_ const HyJynxCore::Text& name )
	{
		_name = name;
	}
}