/**
 * RenderReport.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\RenderReport.h>

using namespace HyJynxCore;

namespace HyJynxRenderer
{
	// default ctor
	RenderReport::RenderReport()
		: _cycleCounter(),
		_stepCounter(),
		_avgCounter(),
		_avgInterval(1.0)
	{
		/*_avgCounter.SetInterval( TimeSpan( _avgInterval * 60 ) );

		Function< void > averageCb = [ ] ( _In_ CaptureData& capture )
		{
			( capture[ "this" ].As< RenderReport >( ) ).UpdateAverages( );
		};

		_avgCounter.Start();*/
	}

	// update the current average values
	void RenderReport::UpdateAverages()
	{

	}

	// reset cycle counters, sequence is about to begin
	void RenderReport::MarkSequenceStart()
	{
		_cycleCounter.Start();
	}

	// mark cycle drawable update begin time
	void RenderReport::MarkUpdateStart()
	{
		_stepCounter.Start();
	}

	// mark cycle drawable update end time
	void RenderReport::MarkUpdateEnd()
	{
		_updateTime = _stepCounter.GetElapsed();
		
		_stepCounter.Stop();
	}

	// mark cycle drawable render time
	void RenderReport::MarkRenderStart()
	{
		_stepCounter.Start();
	}

	// mark cycle drawable render end time
	void RenderReport::MarkRenderEnd()
	{
		_renderTime = _stepCounter.GetElapsed();
	}

	// calculate averages and determine if we're running OK
	void RenderReport::Finalize()
	{
		_cycleTime = _cycleCounter.GetElapsed();
		_renderStatus = RENDER_STATUS_OK;
	}

	// turn on or off averaging calculations
	void RenderReport::SetAveragingActive(_In_ bool active)
	{
		if (active == true && active != _avgActive)
		{
			_avgActive = active;
			_avgCounter.SetInterval(_avgInterval * 60);
			_avgCounter.Start();
		}
		else if (active == false && active != _avgActive)
		{
			_avgActive = active;
			_avgCounter.Stop();
		}
	}

	// return if we're calculating averages or not
	bool RenderReport::GetAveragingActive() const
	{
		return _avgActive;
	}

	// return the status of the current render cycle
	char RenderReport::GetRenderStatus() const
	{
		return _renderStatus;
	}

	// set the status of the current render cycle
	void RenderReport::SetRenderStatus(_In_ char status)
	{
		_renderStatus = status;
	}
}