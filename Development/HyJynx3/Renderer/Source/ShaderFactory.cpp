/**
 * ShaderLibrary.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\ShaderFactory.h>
#include <Renderer\Headers\Renderer.h>
#include <Engine\Headers\Helper.h>
#include <Engine\Headers\ResourceController.h>
#include <D3Dcompiler.h>
#include <fstream>

using namespace DirectX;
using namespace HyJynxCore;
using namespace HyJynxCollections;
using namespace HyJynxLogger;
using namespace HyJynxUtilities;

namespace HyJynxRenderer
{
	// default ctor
	ShaderFactory::ShaderFactory( )
		: ILog( "ShaderFactory" )
	{ }

	// dtor
	ShaderFactory::~ShaderFactory( )
	{
		ReleaseAll( );
	}

#pragma region Wrappers

	// null ctor
	VertexDescription::VertexDescription( )
	{ }

	// defined ctor
	VertexDescription::VertexDescription( _In_ HyJynxCore::Text name,
		D3D11_INPUT_ELEMENT_DESC* desc, _In_ UInt size, _In_ UInt count )
		: Name( name ),
		Description( desc ),
		Size( size ),
		ElementCount( count )
	{ }

	// copy ctor
	VertexDescription::VertexDescription( _In_ const VertexDescription& other )
		: Name( other.Name ),
		Description( other.Description ),
		Size( other.Size ),
		ElementCount( other.ElementCount )
	{ }

	// move ctor
	VertexDescription::VertexDescription( _In_ const VertexDescription&& other )
		: Name( other.Name ),
		Description( other.Description ),
		Size( other.Size ),
		ElementCount( other.ElementCount )
	{ }

	// assignment
	VertexDescription& VertexDescription::operator = ( _In_ const VertexDescription& other )
	{
		Name = other.Name;
		Description = other.Description;
		Size = other.Size;
		ElementCount = other.ElementCount;

		return *this;
	}

	// ----------------------------

	// nullptr
	VertexDeclaration::VertexDeclaration( )
	{ }

	// defined ctor
	VertexDeclaration::VertexDeclaration( _In_ VertexDescription desc, _In_ ID3D11InputLayout* layout )
		: Description( desc ),
		Layout( layout )
	{ }

	// copy ctor
	VertexDeclaration::VertexDeclaration( _In_ const VertexDeclaration& other )
		: Description( other.Description ),
		Layout( other.Layout )
	{ }

	// move ctor
	VertexDeclaration::VertexDeclaration( _In_ const VertexDeclaration&& other )
		: Description( other.Description ),
		Layout( other.Layout )
	{ }

	// assignment
	VertexDeclaration& VertexDeclaration::operator = ( _In_ const VertexDeclaration& other )
	{
		Description = other.Description;
		Layout = other.Layout;

		return *this;
	}

	// ------------------------------------

	// null ctor
	LoadedShader::LoadedShader( )
	{ }

	// no-member ctor
	LoadedShader::LoadedShader( _In_ Text id, _In_ ID3D11DeviceChild* shader )
		: ID( id ),
		ShaderChild( shader )
	{ }

	// vertex shader ctor
	LoadedShader::LoadedShader( _In_ Text id, _In_ ID3D11DeviceChild* shader, _In_ VertexDeclaration* decl )
		: ID( id ),
		ShaderChild( shader ),
		VDeclaration( decl )
	{ }

	// possible pixel shader ctor
	LoadedShader::LoadedShader( _In_ HyJynxCore::Text id, _In_ ID3D11DeviceChild* shader, _In_ LinkageController* linkage)
		: ID( id ),
		ShaderChild( shader ),
		Linkage( linkage )
	{ }

	// copy ctor
	LoadedShader::LoadedShader( _In_ const LoadedShader& other )
		: ID( other.ID ),
		VDeclaration( other.VDeclaration ),
		ShaderChild( other.ShaderChild )
	{ }

	// move ctor
	LoadedShader::LoadedShader( _In_ const LoadedShader&& other )
		: ID( other.ID ),
		VDeclaration( other.VDeclaration ),
		ShaderChild( other.ShaderChild )
	{ }

	// assignment
	LoadedShader& LoadedShader::operator = ( _In_ const LoadedShader& other )
	{
		ID = other.ID;
		VDeclaration = other.VDeclaration;
		ShaderChild = other.ShaderChild;
		return *this;
	}

	// dtor
	LoadedShader::~LoadedShader( )
	{
		ID = "";
		VDeclaration = nullptr;
		ShaderChild = nullptr;
	}

#pragma endregion

#pragma region ElementDescriptions

	// TODO: change DXGI format sizes for optimal performance

	static D3D11_INPUT_ELEMENT_DESC _position[ ] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	const VertexDescription ShaderFactory::VertexElementDescription::Position = VertexDescription
	{
		"Position",
		_position,
		sizeof( Vector3 ),
		1
	};

	// -----------------

	static D3D11_INPUT_ELEMENT_DESC _positionColor[ ] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	const VertexDescription ShaderFactory::VertexElementDescription::PositionColor = VertexDescription
	{
		"Position-Color",
		_positionColor,
		sizeof( Vector3 ) + sizeof( Vector4 ),
		2
	};

	// -----------------

	static D3D11_INPUT_ELEMENT_DESC _positionColorTexture[ ] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	const VertexDescription ShaderFactory::VertexElementDescription::PositionColorTexture = VertexDescription
	{
		"Position-Color-Texture",
		_positionColorTexture,
		sizeof( Vector3 ) + sizeof( Vector4 ) + sizeof( Vector2 ),
		3
	};

	// -----------------

	static D3D11_INPUT_ELEMENT_DESC _positionNormalColor[ ] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	const VertexDescription ShaderFactory::VertexElementDescription::PositionNormalColor = VertexDescription
	{
		"Position-Normal-Color",
		_positionNormalColor,
		( sizeof( Vector3 ) * 2 ) + sizeof( Vector4 ),
		3
	};

	// -----------------

	static D3D11_INPUT_ELEMENT_DESC _positionTexture[ ] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	const VertexDescription ShaderFactory::VertexElementDescription::PositionTexture = VertexDescription
	{
		"Position-Texture",
		_positionTexture,
		sizeof( Vector3 ) + sizeof( Vector2 ),
		2
	};

	// -----------------

	static D3D11_INPUT_ELEMENT_DESC _positionNormalTexture[ ] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	const VertexDescription ShaderFactory::VertexElementDescription::PositionNormalTexture = VertexDescription
	{
		"Position-Normal-Texture",
		_positionNormalTexture,
		sizeof( Vector3 ) + sizeof( Vector3 ) + sizeof( Vector2 ),
		3
	};

	// -----------------

	static D3D11_INPUT_ELEMENT_DESC _positionNormalBinormalTangentTexture[ ] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "BINORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	const VertexDescription ShaderFactory::VertexElementDescription::PositionNormalBinormalTangentTexture = VertexDescription
	{
		"Position-Normal-Binormal-Tangent-Texture",
		_positionNormalBinormalTangentTexture,
		( sizeof( Vector3 ) * 4 ) + sizeof( Vector2 ),
		5
	};

#pragma endregion

	// release all shaders - use with caution
	void ShaderFactory::ReleaseAll( )
	{
		_shaderList.Clear( );

		if ( _classLinkage != nullptr )
		{
			_classLinkage->Release( );
			_classLinkage = nullptr;
		}
	}

	// create the initial state of the projection constant buffer
	D3D11_SUBRESOURCE_DATA getInitialProjectionConstant( )
	{
		D3D11_SUBRESOURCE_DATA output;
		CBCameraProjection constant;

		XMMATRIX mat = XMMatrixPerspectiveFovLH( 0.4f * XM_PI, XM_PIDIV4, 1, 1000.0f );
		XMMATRIX tMat = XMMatrixTranspose( mat );
		XMStoreFloat4x4( &constant.ProjectionMatrix, tMat );

		Vector2 screen = Renderer::Instance( )->GetScreenSize( );

		mat = XMMatrixOrthographicLH( screen.x, screen.y, 1.0f, 1000.0f );
		tMat = XMMatrixTranspose( mat );
		XMStoreFloat4x4( &constant.OrthographicMatrix, tMat );

		constant.ScreenSize_HalfPixel.x = screen.x;
		constant.ScreenSize_HalfPixel.y = screen.y;
		constant.ScreenSize_HalfPixel.z = 1 / screen.x;
		constant.ScreenSize_HalfPixel.w = 1 / screen.y;

		output.pSysMem = &constant;

		return output;
	}

	// get initial camera constant data
	D3D11_SUBRESOURCE_DATA getInitialCameraConstant( )
	{
		D3D11_SUBRESOURCE_DATA output = D3D11_SUBRESOURCE_DATA( );

		CBCamera constant;
		const XMFLOAT4 position = XMFLOAT4( -10.0f, 10.0f, -10.0f, 0.0f );
		const XMFLOAT4 target = XMFLOAT4( 0.0f, 1.0f, 0.0f, 0.0f );

		const XMFLOAT4 view = Vector4::Normalize( position );
		constant.CameraPosition = XMFLOAT4( -10.0f, 10.0f, -10.0f, 0.0f );
		constant.CameraVector = XMFLOAT4( 0.0f, 1.0f, 0.0f, 0.0f );

		XMVECTOR pos = XMLoadFloat4( &position );
		XMVECTOR tgt = XMLoadFloat4( &target );
		XMVECTOR up = XMLoadFloat3( &Camera::UpDirection );

		XMMATRIX mat = XMMatrixLookAtLH( pos, tgt, up );
		XMStoreFloat4x4( &constant.ViewMatrix, mat );

		output.pSysMem = &constant;

		return output;
	}

	// get the initial state of the Cycle Constant Buffer
	D3D11_SUBRESOURCE_DATA getInitialCycleConstant( )
	{
		CBCycle cycleBuffer;

		cycleBuffer.Time = Vector4::Zero( );

		D3D11_SUBRESOURCE_DATA output;
		output.pSysMem = &cycleBuffer;

		return output;
	}

	// get the initial state of the Object Constant Buffer
	D3D11_SUBRESOURCE_DATA getInitialObjectConstant( )
	{
		CBObject obj;

		XMMATRIX mat = XMMatrixIdentity( );
		XMStoreFloat4x4( &obj.WorldMatrix, mat );

		D3D11_SUBRESOURCE_DATA output;
		output.pSysMem = &obj;

		return output;
	}

	// init constant buffer pointers
	bool ShaderFactory::initializeConstants( )
	{
		ID3D11Device* device = Renderer::Instance( )->GetGraphicsLibrary( )->GetDevice( );

		D3D11_BUFFER_DESC desc;
		desc.Usage = D3D11_USAGE_DYNAMIC;
		desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		desc.MiscFlags = 0;

		desc.ByteWidth = sizeof( CBCameraProjection );
		HRESULT result = device->CreateBuffer( &desc, &getInitialProjectionConstant( ), &_projectionBuffer );
		if ( FAILED( result ) )
		{
			LogError( "initializeConstants() - CBCameraProjection Failed." );
			return false;
		}

		desc.ByteWidth = sizeof( CBCamera );
		result = device->CreateBuffer( &desc, &getInitialCameraConstant( ), &_cameraBuffer );
		if ( FAILED( result ) )
		{
			LogError( "initializeConstants() - CBCamera Failed." );
			return false;
		}

		desc.ByteWidth = sizeof( CBCycle );
		result = device->CreateBuffer( &desc, &getInitialCycleConstant( ), &_cycleBuffer );
		if ( FAILED( result ) )
		{
			LogError( "initializeConstants() - CBCycle Failed." );
			return false;
		}

		desc.ByteWidth = sizeof( CBObject );
		result = device->CreateBuffer( &desc, &getInitialObjectConstant( ), &_objectBuffer );
		if ( FAILED( result ) )
		{
			LogError( "initializeConstants() - CBObject Failed." );
			return false;
		}

		return true;
	}

	// initialize class linkage
	bool ShaderFactory::Initialize( )
	{
		Renderer::Instance( )
			->GetGraphicsLibrary( )
			->CreateClassLinkage( &_classLinkage );

		if ( _classLinkage == nullptr )
		{
			return false;
		}

		if ( initializeConstants( ) == false )
		{
			return false;
		}

		return true;
	}

#pragma region Creation

	// retrieve a loaded shader, or nullptr
	LoadedShader* const ShaderFactory::GetLoadedShader( _In_ const ShaderStage stage, _In_ const Text& path ) const
	{
		LoadedShader* output = nullptr;

		_shaderList.ForEach( [ &stage, &path, &output ] ( _In_ KeyValuePair< KeyValuePair< ShaderStage, Text >, LoadedShader > shader )
		{
			if ( shader.Key.Key == stage 
				&& shader.Key.Value == path
				&& output != nullptr )
			{
				output = &shader.Value;
			}
		} );

		return output;
	}

	// create a new shader
	Shader* ShaderFactory::CreateShader( _In_ ShaderInit& initializer )
	{
		if ( initializer.VertexPath.IsNullOrEmpty( ) == true || initializer.PixelPath.IsNullOrEmpty( ) == true )
		{
			LogError( "CreateShader() - Vertex or Pixel shaders are required, unable to build a viable Shader." );
			return nullptr;
		}

		Shader* output = anew( Shader )( );

		BuildVertexShader( initializer.VertexPath, initializer.ExpectedVertexDescription, output );

		BuildPixelShader( initializer.PixelPath, initializer.OnLinkageFound, output );

		if ( initializer.HullPath.IsNullOrEmpty( ) == false )
		{
			BuildHullShader( initializer.HullPath, output );
		}

		if ( initializer.DomainPath.IsNullOrEmpty( ) == false )
		{
			BuildDomainShader( initializer.DomainPath, output );
		}

		if ( initializer.GeometryPath.IsNullOrEmpty( ) == false )
		{
			BuildGeometryShader( initializer.GeometryPath, output );
		}

		return output;
	}

	// locally called to load a pre-compiled shader
	char* _loadBlob( _In_ Text& path, _Out_ size_t* size )
	{
		std::ifstream fStream;
		fStream.open( path, std::ifstream::in | std::ifstream::binary );
		if ( fStream.is_open( ) == true )
		{
			fStream.seekg( 0, std::ios::end );
			*size = size_t( fStream.tellg( ) );
			char* data = new char[ *size ];
			fStream.seekg( 0, std::ios::beg );
			fStream.read( data, *size );
			fStream.close( );

			return data;
		}
		else
		{
			*size = 0;
			return nullptr;
		}
	}

	// get or create a vertex layout
	VertexDeclaration* ShaderFactory::CreateVertexDeclaration( _In_ VertexDescription& desc, _In_ void* shaderCode, _In_ UInt shaderSize )
	{
		ID3D11InputLayout* layout = nullptr;

		Renderer::Instance( )->GetGraphicsLibrary( )->CreateInputLayout( desc, shaderCode, shaderSize, &layout );

		return anew( VertexDeclaration )( desc, layout );
	}

	// create a vertex shader from pre-compiled file
	void ShaderFactory::BuildVertexShader( _In_ Text path, _In_ VertexDescription& description,
		_Out_ Shader* const outputShader )
	{
		if ( path.IsNullOrEmpty( ) == true )
		{
			LogWarning( "GetVertexShader(null) - argued filepath is null or empty." );
			return;
		}

		LoadedShader* loadedShader = GetLoadedShader( ShaderStage::SHADER_STAGE_VERTEX, path );
		if ( loadedShader != nullptr )
		{
			outputShader->_vertexPath = path;
			outputShader->_vertexShader = static_cast<ID3D11VertexShader*>( loadedShader->ShaderChild );
			outputShader->_inputLayout = loadedShader->VDeclaration->Layout;
			return;
		}

		size_t size = 0;
		char* fileBlob = _loadBlob( path, &size );

		if ( fileBlob == nullptr )
		{
			LogWarning( "GetVertexShader() - unable to load file to blob." );
			return;
		}

		ID3D11VertexShader* shader;

		HRESULT result = Renderer::Instance( )
			->GetGraphicsLibrary( )
			->GetDevice( )
			->CreateVertexShader( fileBlob, size, nullptr, &shader );

		if ( FAILED( result ) )
		{
			LogWarning( "GetVertexShader() - unable to create vertex shader." );
			return;
		}

		VertexDeclaration* vdeclaration = CreateVertexDeclaration( description, fileBlob, size );

		if ( vdeclaration != nullptr )
		{
			outputShader->_inputLayout = vdeclaration->Layout;
		}

		outputShader->_vertexPath = path;
		outputShader->_vertexShader = shader;

		_shaderList.Append( KeyValuePair< ShaderStage, Text >( ShaderStage::SHADER_STAGE_VERTEX, path ), 
			LoadedShader( path, shader, vdeclaration ) );
	}

	// create a pixel shader from pre-compiled file
	void ShaderFactory::BuildPixelShader( _In_ Text path, _In_ LinkageCallback onLinkage, _Out_ Shader* const outputShader )
	{
		if ( path.IsNullOrEmpty( ) == true )
		{
			LogWarning( "GetPixelShader(null) - argued filepath is null or empty." );
			return;
		}

		LoadedShader* present = GetLoadedShader( ShaderStage::SHADER_STAGE_PIXEL, path );
		if ( present != nullptr )
		{
			outputShader->_pixelPath = path;
			outputShader->_pixelShader = static_cast<ID3D11PixelShader*>( present->ShaderChild );
			if ( present->Linkage != nullptr )
			{
				outputShader->_linkageController = present->Linkage;
			}
			return;
		}

		size_t size = 0;
		char* fileBlob = _loadBlob( path, &size );

		if ( fileBlob == nullptr )
		{
			LogWarning( "GetPixelShader() - unable to load file to blob." );
			return;
		}

		ID3D11PixelShader* shader;

		HRESULT result = Renderer::Instance( )
			->GetGraphicsLibrary( )
			->GetDevice( )
			->CreatePixelShader( fileBlob, size, _classLinkage, &shader );

		if ( FAILED( result ) )
		{
			LogWarning( "GetPixelShader() - unable to create pixel shader." );
			return;
		}

		ID3D11ShaderReflection* reflector = nullptr;
		result = D3DReflect( fileBlob, size, IID_ID3D11ShaderReflection, (void**) &reflector );
		LinkageController* linkageController = nullptr;

		if ( SUCCEEDED( result ) )
		{
			UInt numInterfaces = reflector->GetNumInterfaceSlots( );
			if ( numInterfaces > 0 )
			{
				linkageController = anew( LinkageController )( path );
				onLinkage( reflector, linkageController );
			}
		}

		outputShader->_pixelPath = path;
		outputShader->_pixelShader = shader;

		if ( linkageController != nullptr )
		{
			outputShader->_linkageController = linkageController;
			_shaderList.Append( KeyValuePair< ShaderStage, Text >( ShaderStage::SHADER_STAGE_PIXEL, path ), LoadedShader( path, shader, linkageController ) );
		}
		else
		{
			_shaderList.Append( KeyValuePair< ShaderStage, Text >( ShaderStage::SHADER_STAGE_PIXEL, path ), LoadedShader( path, shader ) );
		}
	}

	// create a hull shader from pre-compiled file
	void ShaderFactory::BuildHullShader( _In_ Text path, _Out_ Shader* const outputShader )
	{
		if ( path.IsNullOrEmpty( ) == true )
		{
			LogWarning( "GetHullShader(null) - argued filepath is null or empty." );
			return;
		}

		LoadedShader* present = GetLoadedShader( ShaderStage::SHADER_STAGE_HULL, path );
		if ( present != nullptr )
		{
			outputShader->_hullPath = path;
			outputShader->_hullShader = static_cast<ID3D11HullShader*>( present->ShaderChild );
			return;
		}

		size_t size = 0;
		char* fileBlob = _loadBlob( path, &size );

		if ( fileBlob == nullptr )
		{
			LogWarning( "GetHullShader() - unable to load file to blob." );
			return;
		}

		ID3D11HullShader* shader;

		HRESULT result = Renderer::Instance( )
			->GetGraphicsLibrary( )
			->GetDevice( )
			->CreateHullShader( fileBlob, size, _classLinkage, &shader );

		if ( FAILED( result ) )
		{
			LogWarning( "GetHullShader() - unable to create hull shader." );
			return;
		}

		outputShader->_hullPath = path;
		outputShader->_hullShader = shader;

		_shaderList.Append( KeyValuePair< ShaderStage, Text >( ShaderStage::SHADER_STAGE_HULL, path ), LoadedShader( path, shader ) );
	}

	// create a domain shader from pre-compiled file
	void ShaderFactory::BuildDomainShader( _In_ Text path, _Out_ Shader* const outputShader )
	{
		if ( path.IsNullOrEmpty( ) == true )
		{
			LogWarning( "GetDomainShader(null) - argued filepath is null or empty." );
			return;
		}

		LoadedShader* present = GetLoadedShader( ShaderStage::SHADER_STAGE_DOMAIN, path );
		if ( present != nullptr )
		{
			outputShader->_domainPath = path;
			outputShader->_domainShader = static_cast< ID3D11DomainShader* > ( present->ShaderChild );
			return;
		}

		size_t size = 0;
		char* fileBlob = _loadBlob( path, &size );

		if ( fileBlob == nullptr )
		{
			LogWarning( "GetDomainShader() - unable to load file to blob." );
			return;
		}

		ID3D11DomainShader* shader;

		HRESULT result = Renderer::Instance( )
			->GetGraphicsLibrary( )
			->GetDevice( )
			->CreateDomainShader( fileBlob, size, _classLinkage, &shader );

		if ( FAILED( result ) )
		{
			LogWarning( "GetDomainShader() - unable to create domain shader." );
			return;
		}

		outputShader->_domainPath = path;
		outputShader->_domainShader = shader;

		_shaderList.Append( KeyValuePair< ShaderStage, Text >( ShaderStage::SHADER_STAGE_DOMAIN, path ), LoadedShader( path, shader ) );
	}

	// create a geometry shader from pre-compiled file
	void ShaderFactory::BuildGeometryShader( _In_ Text path, _Out_ Shader* const outputShader )
	{
		if ( path.IsNullOrEmpty( ) == true )
		{
			LogWarning( "GetGeometryShader(null) - argued filepath is null or empty." );
			return;
		}

		LoadedShader* present = GetLoadedShader( ShaderStage::SHADER_STAGE_GEOMETRY, path );
		if ( present != nullptr )
		{
			outputShader->_geometryPath = path;
			outputShader->_geometryShader = static_cast<ID3D11GeometryShader*>( present->ShaderChild );
			return;
		}

		size_t size = 0;
		char* fileBlob = _loadBlob( path, &size );

		if ( fileBlob == nullptr )
		{
			LogWarning( "GetGeometryShader() - unable to load file to blob." );
			return;
		}

		ID3D11GeometryShader* shader;

		HRESULT result = Renderer::Instance( )
			->GetGraphicsLibrary( )
			->GetDevice( )
			->CreateGeometryShader( fileBlob, size, _classLinkage, &shader );

		if ( FAILED( result ) )
		{
			LogWarning( "GetGeometryShader() - unable to create geometry shader." );
			return;
		}

		outputShader->_geometryPath = path;
		outputShader->_geometryShader = shader;

		_shaderList.Append( KeyValuePair< ShaderStage, Text >( ShaderStage::SHADER_STAGE_GEOMETRY, path ), LoadedShader( path, shader ) );
	}

#pragma endregion

	// retrieve the buffer pointer of the Camera Projection constant
	ID3D11Buffer* ShaderFactory::GetProjectionConstant( )
	{
		return _projectionBuffer;
	}

	// retrieve the buffer pointer of the Camera constant
	ID3D11Buffer* ShaderFactory::GetCameraConstant( )
	{
		return _cameraBuffer;
	}

	// retrieve the buffer pointer of the Camera cycle constant
	ID3D11Buffer* ShaderFactory::GetCycleConstant( )
	{
		return _cycleBuffer;
	}

	// retrieve the buffer pointer of the Camera object constant
	ID3D11Buffer* ShaderFactory::GetObjectConstant( )
	{
		return _objectBuffer;
	}

	// get a buffer by gpu bound index
	ID3D11Buffer* ShaderFactory::GetBufferByConstantIndex( _In_ const UInt index )
	{
		ID3D11Buffer* output;

		switch ( index )
		{
		case 0: output = _projectionBuffer; break;
		case 1: output = _cameraBuffer; break;
		case 2: output = _cycleBuffer; break;
		case 3: output = _objectBuffer; break;
		default: output = nullptr;
		}

		return output;
	}

	// get a shader linkage class instance
	ID3D11ClassInstance* ShaderFactory::GetClassInstance( _In_ const HyJynxCore::Text name )
	{
		ID3D11ClassInstance* output = nullptr;

		if ( name.IsNullOrEmpty( ) == false && _classLinkage != nullptr)
		{
			HRESULT result = _classLinkage->GetClassInstance( name.GetValue( ), 0, &output );

			if ( FAILED( result ) )
			{
				Text log = "GetClassInstance(";
				log += name;
				log += ") - failed to get the class instance.";
				LogError( log );
				output = nullptr;
			}
		}

		return output;
	}
}