/**
 * LODLevel.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\LODLevel.h>

namespace HyJynxRenderer
{
	// LOD static definitions
	float LODLevel::LODMinimum = 0.0f;
	float LODLevel::LODMaximum = std::numeric_limits<float>::max( );
	LODLevel LODLevel::LOD_Null = LODLevel( );

	// null ctor - default values
	LODLevel::LODLevel( )
	{ }

	// min and max ctor
	LODLevel::LODLevel( _In_ const float minimum, _In_ const float maximum )
		:_minimum( minimum >= LODLevel::LODMinimum ? minimum : LODLevel::LODMinimum ),
		_maximum( maximum <= LODLevel::LODMaximum ? maximum : LODLevel::LODMaximum ),
		_minFadeStop( _minimum ),
		_maxFadeStop( _maximum )
	{ }

	// min max and fade stop ctor
	LODLevel::LODLevel( _In_ const float min, _In_ const float minStop, _In_ const float max, _In_ const float maxStop )
		: _minimum( min ),
		_minFadeStop( minStop ),
		_maxFadeStop( maxStop ),
		_maximum( max )
	{ }

	// copy ctor
	LODLevel::LODLevel( _In_ const LODLevel& other )
		: _minimum( other._minimum ),
		_maximum( other._maximum ),
		_minFadeStop( other._minFadeStop ),
		_maxFadeStop( other._maxFadeStop )
	{ }

	// move ctor
	LODLevel::LODLevel( _In_ const LODLevel&& other )
		: _minimum( other._minimum ),
		_maximum( other._maximum ),
		_minFadeStop( other._minFadeStop ),
		_maxFadeStop( other._maxFadeStop )
	{ }

	// dtor
	LODLevel::~LODLevel( )
	{
		_minimum = _minFadeStop = LODLevel::LODMinimum;
		_maximum = _maxFadeStop = LODLevel::LODMaximum;
	}

	// get suggested opacity at the argued distance
	float LODLevel::GetSuggestedOpacity( _In_ const float distance )
	{
		float output = 1.0f;

		if ( distance < _minimum || distance > _maximum )
		{
			output = 0.0f;
		}
		else if ( distance >= _minFadeStop || distance <= _maxFadeStop )
		{
			output = 1.0f;
		}

		if ( distance <= _minFadeStop && distance > _minimum )
		{
			output = ( distance - _minimum ) / ( _minFadeStop - _minimum );
		}
		else if ( distance >= _maxFadeStop && distance < _maximum )
		{
			output = ( distance - _maxFadeStop ) / ( _maximum - _maxFadeStop );
		}
		
		return output;
	}
}