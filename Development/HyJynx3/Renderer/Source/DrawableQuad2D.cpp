/**
 * DrawableQuad2D.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\DrawableQuad2D.h>
#include <Renderer\Headers\Renderer.h>
#include <Renderer\Headers\Material2D.h>
#include <Renderer\Headers\RenderMath.h>


using namespace HyJynxCollections;
using namespace HyJynxCore;
using namespace DirectX;
using namespace std;


namespace HyJynxRenderer
{
#pragma region Configuration

	// null ctor
	DrawableQuad2DConfiguration::DrawableQuad2DConfiguration( )
		:PrimitiveConfiguration( )
	{ }

	// copy ctor
	DrawableQuad2DConfiguration::DrawableQuad2DConfiguration( _In_ const DrawableQuad2DConfiguration& other )
		: PrimitiveConfiguration( other ),
		_width( other._width ),
		_height( other._height )
	{ }

	// move ctor
	DrawableQuad2DConfiguration::DrawableQuad2DConfiguration( _In_ const DrawableQuad2DConfiguration&& other )
		: PrimitiveConfiguration( other ),
		_width( other._width ),
		_height( other._height )
	{ }

	// defined ctor
	DrawableQuad2DConfiguration::DrawableQuad2DConfiguration( _In_ float w, _In_ float h )
		: PrimitiveConfiguration( ),
		_width( w ),
		_height( h )
	{ }

	// get width
	const float DrawableQuad2DConfiguration::GetWidth( ) const
	{
		return _width;
	}

	// get height
	const float DrawableQuad2DConfiguration::GetHeight( ) const
	{
		return _height;
	}

	// set width
	void DrawableQuad2DConfiguration::SetWidth( _In_ const float w )
	{
		_width = w;
		ToggleDirty( );
	}

	// set height
	void DrawableQuad2DConfiguration::SetHeight( _In_ const float h )
	{
		_height = h;
		ToggleDirty( );
	}

	// set width and height
	void DrawableQuad2DConfiguration::SetDimensions( _In_ const float x, _In_ const float y )
	{
		_width = x;
		_height = y;
		ToggleDirty( );
	}

	// get width/height container
	Vector2 DrawableQuad2DConfiguration::GetDimensions( ) const
	{
		return Vector2( _width, _height );
	}

	// set all vertex colors
	void DrawableQuad2DConfiguration::SetVertexColors( _In_ const Color& tl,
		_In_ const Color& tr, _In_ const Color& bl, _In_ const Color& br )
	{
		_topLeft = tl;
		_topRight = tr;
		_botLeft = bl;
		_botRight = br;
		ToggleDirty( );
	}

	// get vertex top-left color
	Color& DrawableQuad2DConfiguration::GetTopLeftColor( )
	{
		return _topLeft;
	}

	void DrawableQuad2DConfiguration::SetTopLeftColor( _In_ const Color& color )
	{
		_topLeft = color;
		ToggleDirty( );
	}

	// get vertex top-right color
	Color& DrawableQuad2DConfiguration::GetTopRightColor( )
	{
		return _topRight;
	}

	void DrawableQuad2DConfiguration::SetTopRightColor( _In_ const Color& color )
	{
		_topRight = color;
		ToggleDirty( );
	}

	// get bottom-left color
	Color& DrawableQuad2DConfiguration::GetBottomLeftColor( )
	{
		return _botLeft;
	}

	void DrawableQuad2DConfiguration::SetBottomLeftColor( _In_ const Color& color )
	{
		_botLeft = color;
		ToggleDirty( );
	}

	// get bottom-right color
	Color& DrawableQuad2DConfiguration::GetBottomRightColor( )
	{
		return _botRight;
	}

	void DrawableQuad2DConfiguration::SetBottomRightColor( _In_ const Color& color )
	{
		_botRight = color;
		ToggleDirty( );
	}

#pragma endregion

	// null ctor
	DrawableQuad2D::DrawableQuad2D( )
		: Drawable( ),
		Primitive( anew( DrawableQuad2DConfiguration )( ) )
	{
		initializeMatrixOverride( );
	}

	// configured ctor
	DrawableQuad2D::DrawableQuad2D( _In_ DrawableQuad2DConfiguration* config )
		: Drawable( ),
		Primitive( config )
	{
		initializeMatrixOverride( );
	}

	// copy ctor
	DrawableQuad2D::DrawableQuad2D( _In_ const DrawableQuad2D& other )
		: Drawable( other ),
		Primitive( other ),
		_transform( other._transform )
	{
		initializeMatrixOverride( );
	}

	// move ctor
	DrawableQuad2D::DrawableQuad2D( _In_ const DrawableQuad2D&& other )
		: Drawable( other ),
		Primitive( other ),
		_transform( other._transform )
	{
		initializeMatrixOverride( );
	}

	// dtor
	DrawableQuad2D::~DrawableQuad2D( )
	{ }

	// override the matrix calculation
	void DrawableQuad2D::initializeMatrixOverride( )
	{
		Function<void, Transform&, Matrix*> cb = [ ] ( _In_ CaptureData& capture, _In_ Transform& transform, _Out_ Matrix* matrix )
		{
			Vector2 screen = Renderer::Instance( )->GetScreenSize( );

			XMMATRIX output = XMMatrixIdentity( );

			// origin translation
			Vector3 temp = capture[ "collision" ].As< CollisionDescription* >( )->GetDimensions( );
			temp /= screen;
			temp *= 2.0f;
			temp *= transform.GetOrigin( );
			XMMATRIX multTemp = XMMatrixTranslation( -temp.x, temp.y, 0.0f );
			output = XMMatrixMultiply( output, multTemp );

			// local scaling
			multTemp = XMMatrixScaling( transform.GetScale( ).x, transform.GetScale( ).y, 1.0f );
			output = XMMatrixMultiply( output, multTemp );

			// local z-rotation
			XMVECTOR axis = XMVectorSet( 0.0f, 0.0f, 1.0f, 0.0f );
			multTemp = XMMatrixRotationAxis( axis, RenderMath::ToRadians( transform.GetRotation( ).z ) );
			output = XMMatrixMultiply( output, multTemp );

			// local translation
			temp = transform.GetPosition( );
			temp /= screen;
			temp *= 2.0f;
			multTemp = XMMatrixTranslation( temp.x, -temp.y, temp.z );
			output = XMMatrixMultiply( output, multTemp );

			if ( transform.GetParent( ) != nullptr )
			{
				multTemp = XMLoadFloat4x4( &transform.GetParent( )->GetMatrix( ) );
				output = XMMatrixMultiply( output, multTemp );
			}

			XMStoreFloat4x4( matrix, output );
		};

		cb.Capture( "collision", this->GetCollisionDescription() );

		_transform.SetMatrixCalculationOverride( cb );
	}

	// request and set the model object
	void DrawableQuad2D::GeneratePrimitiveModel( )
	{
		DrawableQuad2DConfiguration* config = static_cast< DrawableQuad2DConfiguration* >( _configuration );
		VertexDescription description = ShaderFactory::VertexElementDescription::PositionColorTexture;

		Vector2 screen = Renderer::Instance( )->GetScreenSize( );
		Vector2 size = Vector2( config->GetWidth( ), config->GetHeight( ) );
		size /= screen;
		size *= 2.0f;

		VertexPositionColorTexture verts[ ] =
		{
			{ Vector3( 0, -size.y ), *config->GetBottomLeftColor().GetMultipliedColor(), Vector2( 0.0f ) },
			{ Vector3( 0, 0 ), *config->GetTopLeftColor().GetMultipliedColor(), Vector2( 1.0f, 0.0f ) },
			{ Vector3( size.x, 0 ), *config->GetTopRightColor().GetMultipliedColor(), Vector2( 0.0f, 1.0f ) },
			{ Vector3( size.x, -size.y ), *config->GetBottomRightColor().GetMultipliedColor(), Vector2( 1.0f ) }
		};

		D3D11_BUFFER_DESC vertDesc;
		ZeroMemory( &vertDesc, sizeof( vertDesc ) );
		vertDesc.Usage = D3D11_USAGE::D3D11_USAGE_DEFAULT;
		vertDesc.ByteWidth = description.Size * 4;
		vertDesc.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_VERTEX_BUFFER;
		vertDesc.CPUAccessFlags = 0;

		D3D11_SUBRESOURCE_DATA vertData;
		ZeroMemory( &vertData, sizeof( vertData ) );
		vertData.pSysMem = verts;

		VertexBuffer* verticies = Renderer::Instance( )
			->GetGraphicsLibrary( )
			->CreateVertexBuffer( &vertDesc, &vertData, description, 4 );

		UInt inds[ ] = { 0, 1, 2, 3, 0, 2 };
	
		D3D11_BUFFER_DESC indDesc;
		ZeroMemory( &indDesc, sizeof( indDesc ) );
		indDesc.Usage = D3D11_USAGE_DEFAULT;
		indDesc.ByteWidth = sizeof( UInt ) * 6;
		indDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		indDesc.CPUAccessFlags = 0;

		D3D11_SUBRESOURCE_DATA indData;
		ZeroMemory( &indData, sizeof( indData ) );
		indData.pSysMem = inds;

		IndexBuffer* indicies = Renderer::Instance( )
			->GetGraphicsLibrary( )
			->CreateIndexBuffer( &indDesc, &indData, 6 );

		Mesh* mesh = anew( Mesh )( verticies, indicies );
		mesh->SetMaterial( anew( Material2D )( ) );

		_model = anew( Model )( );
		_model->AddMesh( mesh );

		_configuration->ToggleClean( );
	}

	// load model call
	void DrawableQuad2D::LoadModel( _In_ Function< void > onComplete )
	{
		// TODO: make this async

		GeneratePrimitiveModel( );

		onComplete( );
	}

	// generate data for CollisionDescription
	void DrawableQuad2D::GenerateBounding( )
	{
		DrawableQuad2DConfiguration* config = static_cast< DrawableQuad2DConfiguration* >( _configuration );
		BoundingBox* aabb = anew( BoundingBox )( Vector3( ), Vector3( config->GetWidth(), config->GetHeight() ) );
		_collision.SetCollidables( nullptr, aabb );
	}

	// per-cycle update
	void DrawableQuad2D::Update( _In_ Context* context, _In_ RenderTime time )
	{
		if ( _configuration->IsDirty( ) == true )
		{
			updateResourceData( context );
			_configuration->ToggleClean( );
		}
	}

	// called to update resource buffers with current configuration settings
	void DrawableQuad2D::updateResourceData( _In_ Context* context )
	{
		if ( _model->GetMeshCount( ) > 0 )
		{
			DrawableQuad2DConfiguration* config = static_cast< DrawableQuad2DConfiguration* >( _configuration );

			VertexBuffer* vBuffer = _model->GetVertexBufferAtIndex( 0 );
			VertexPositionColorTexture** verts = context->MapData< VertexPositionColorTexture** >
				( vBuffer->GetBuffer(), 0, D3D11_MAP::D3D11_MAP_WRITE_DISCARD, 0 );

			// TODO: not working
			if ( verts != nullptr )
			{
				Vector2 size = Vector2( config->GetWidth( ), config->GetHeight( ) );
				size /= Renderer::Instance( )->GetScreenSize( );

				verts[ 0 ]->_color = *config->GetBottomLeftColor( ).GetMultipliedColor( );
				verts[ 1 ]->_color = *config->GetTopLeftColor( ).GetMultipliedColor( );
				verts[ 2 ]->_color = *config->GetTopRightColor( ).GetMultipliedColor( );
				verts[ 3 ]->_color = *config->GetBottomRightColor( ).GetMultipliedColor( );

				verts[ 0 ]->_position.y = -size.y;
				verts[ 2 ]->_position.x = size.x;
				verts[ 3 ]->_position.x = size.x;
				verts[ 3 ]->_position.y = -size.y;

				context->UnMapData( vBuffer->GetBuffer( ) );
			}
		}
	}

	// render debugging information
	void DrawableQuad2D::RenderDebug( _In_ Context* context )
	{
		// draw some buggin shit biotch
	}

	// get transform pointer
	Transform* DrawableQuad2D::GetTransform( )
	{
		return &_transform;
	}

	// get the un-transformed width in pixels
	float DrawableQuad2D::GetWidth( )
	{
		DrawableQuad2DConfiguration* config = static_cast< DrawableQuad2DConfiguration* >( _configuration );
		return config->GetWidth( );
	}

	// get the un-transformed height in pixels
	float DrawableQuad2D::GetHeight( )
	{
		DrawableQuad2DConfiguration* config = static_cast< DrawableQuad2DConfiguration* >( _configuration );
		return config->GetHeight( );
	}

	// set the un-transformed width in pixels
	void DrawableQuad2D::SetWidth( _In_ const float val )
	{
		DrawableQuad2DConfiguration* config = static_cast< DrawableQuad2DConfiguration* >( _configuration );
		config->SetWidth( val );
	}

	// set the un-transformed height in pixels
	void DrawableQuad2D::SetHeight( _In_ const float val )
	{
		DrawableQuad2DConfiguration* config = static_cast< DrawableQuad2DConfiguration* >( _configuration );
		config->SetHeight( val );
	}
}