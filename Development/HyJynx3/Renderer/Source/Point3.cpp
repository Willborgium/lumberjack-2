/**
 * Point3.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\Point3.h>

namespace HyJynxRenderer
{
#pragma region init

	// null ctor
	Point3::Point3( )
	{ }

	// initialize all components to the argued value
	Point3::Point3( _In_ int val )
		: X( val )
	{ }

	// initialize all components seperately
	Point3::Point3( _In_ int x, _In_ int y, _In_ int z )
		: X( x ), Y( y ), Z( z )
	{ }

	// copy ctor
	Point3::Point3( _In_ const Point3& other )
		: X( other.X ), Y( other.Y ), Z( other.Z )
	{ }

	// move ctor
	Point3::Point3( _In_ const Point3&& other )
		: X( other.X ), Y( other.Y ), Z( other.Z )
	{ }

	// dtor
	Point3::~Point3( )
	{ }

#pragma endregion

#pragma region API



#pragma endregion

#pragma region Operators

	// equality operator
	bool Point3::operator == ( _In_ const Point3& other ) const
	{
		return X == other.X
			&& Y == other.Y
			&& Z == other.Z;
	}

	// inequality operator (emancipation proclimation bitch)
	bool Point3::operator != ( _In_ const Point3& other ) const
	{
		return !( *this == other );
	}

	// compound addition
	Point3& Point3::operator += ( _In_ const Point3& other )
	{
		X += other.X;
		Y += other.Y;
		Z += other.Z;
		return *this;
	}

	// compound subtraction
	Point3& Point3::operator -= ( _In_ const Point3& other )
	{
		X -= other.X;
		Y -= other.Y;
		Z -= other.Z;
		return *this;
	}

	// compound multiplication
	Point3& Point3::operator *= ( _In_ const Point3& other )
	{
		X *= other.X;
		Y *= other.Y;
		Z *= other.Z;
		return *this;
	}

	// compound division
	Point3& Point3::operator /= ( _In_ const Point3& other )
	{
		X /= other.X != 0 ? other.X : 1;
		Y /= other.Y != 0 ? other.Y : 1;
		Z /= other.Z != 0 ? other.Z : 1;
		return *this;
	}

	// subtraction
	Point3 Point3::operator - ( _In_ const Point3& other ) const
	{
		Point3 out;
		out.X = X - other.X;
		out.Y = Y - other.Y;
		out.Z = Z - other.Z;
		return out;
	}

	// addition
	Point3 Point3::operator + ( _In_ const Point3& other ) const
	{
		Point3 out;
		out.X = X + other.X;
		out.Y = Y + other.Y;
		out.Z = Z + other.Z;
		return out;
	}

	// multiplication
	Point3 Point3::operator * ( _In_ const Point3& other ) const
	{
		Point3 out;
		out.X = X * other.X;
		out.Y = Y * other.Y;
		out.Z = Z * other.Z;
		return out;
	}

	// division
	Point3 Point3::operator / ( _In_ const Point3& other ) const
	{
		Point3 out;
		out.X = X / other.X != 0 ? other.X : 1;
		out.Y = Y / other.Y != 0 ? other.Y : 1;
		out.Z = Z / other.Z != 0 ? other.Z : 1;
		return out;
	}

#pragma endregion
}