/**
 * Screen.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\Screen.h>

namespace HyJynxRenderer
{
	// null ctor
	Screen::Screen()
		:_hwnd( nullptr )
	{

	}

	// ctor with window handle
	Screen::Screen(_In_ HWND* window)
		:_hwnd( window )
	{

	}

	// dtor
	Screen::~Screen()
	{

	}
}