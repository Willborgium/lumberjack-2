/**
 * RenderSequenceNull.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\RenderSequenceNull.h>

using namespace HyJynxCollections;
using namespace HyJynxCore;

namespace HyJynxRenderer
{
	// default ctor
	RenderSequenceNull::RenderSequenceNull()
		:IRenderSequence("RenderSequenceNull")
	{ }

	// ensure the render targets we need are created and ready
	bool RenderSequenceNull::ValidateTargets(_In_ Dictionary<Text, ID3D11RenderTargetView*>* const renderTargets,
		_In_ Dictionary<Text, DepthStencil*>* const depthTargets )
	{
		if (renderTargets->ContainsKey("diffuse") == true)
		{
			return true;
		}

		return false;
	}

	// if we couldn't validate our render targets, initialize them
	bool RenderSequenceNull::InitializeTargets( _In_ Graphics* graphics, _In_ Dictionary<Text, ID3D11RenderTargetView*>* renderTargets,
		_In_ Dictionary<Text, DepthStencil*>* depthTargets )
	{
		if ( graphics == nullptr )
		{
			LogError( "InitializeTargets( graphics: nullptr ) - cannot continue." );
			return false;
		}

		ID3D11RenderTargetView* diffuse = graphics->CreateRenderTarget();

		if (diffuse == nullptr)
		{
			LogError( "InitializeTargets() - unable to create diffuse render target." );
			return false;
		}

		renderTargets->Append("diffuse", diffuse);

		return true;
	}

	// clear the targets we're using to a blank slate
	bool RenderSequenceNull::ClearTargets( _In_ Context* context, 
		_In_ Dictionary<Text, ID3D11RenderTargetView*>* renderTargets,
		_In_ Dictionary<Text, DepthStencil*>* depthTargets,
		_In_opt_ Color* color )
	{
		ID3D11RenderTargetView* diffuse = renderTargets->ValueOf("diffuse");

		if (diffuse == nullptr)
		{
			// log warning - diffuse target is not present (should never happen)
			return false;
		}

		context->ClearScreen(diffuse, color);

		return true;
	}

	// loop through windows/drawables and render them to the screen
	bool RenderSequenceNull::Render( _In_ Context*,
		_In_ Dictionary<HyJynxCore::Text, ID3D11RenderTargetView*>*,
		_In_ Dictionary<HyJynxCore::Text, DepthStencil*>*,
		_In_ BatchContainer* )
	{
		return true;
	}
}