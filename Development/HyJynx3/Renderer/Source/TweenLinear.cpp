/**
 * TweenLinear.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\TweenLinear.h>

namespace HyJynxRenderer
{
	// null ctor
	TweenLinear::TweenLinear()
	{ }

	// copy ctor
	TweenLinear::TweenLinear( _In_ const TweenLinear& )
	{ }

	// move ctor
	TweenLinear::TweenLinear( _In_ const TweenLinear&& )
	{ }

	// dtor
	TweenLinear::~TweenLinear( )
	{ }

	// per-cycle update
	void TweenLinear::Update( _In_ const double total, _In_ const double delta, _In_ const double maximum )
	{

	}
}