/**
 * LinkageController.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\LinkageController.h>

using namespace HyJynxCore;

namespace HyJynxRenderer
{
	// null ctor
	LinkageController::LinkageController( )
	{ }

	// default ctor
	LinkageController::LinkageController( _In_ const HyJynxCore::Text name )
		: _owningShader( name )
	{ }

	// copy ctor
	LinkageController::LinkageController( _In_ const LinkageController& )
	{ }

	// move ctor
	LinkageController::LinkageController( _In_ const LinkageController&& )
	{ }

	// dtor
	LinkageController::~LinkageController( )
	{ }

	// initialize the dynamic linkage array
	void LinkageController::InitializeLinkageArray( _In_ const UInt slotCount )
	{
		if ( slotCount > 0 )
		{
			_interfaceSlotCount = slotCount;
			_linkageArray = ( ID3D11ClassInstance** ) malloc( sizeof( ID3D11ClassInstance* ) * slotCount );
		}
	}

	// register the base abtract type variables here, which will be permutated
	bool LinkageController::AddAbstractVariable( _In_ ID3D11ShaderReflection* const reflector, _In_ const Text name )
	{
		if ( reflector != nullptr && name.IsNullOrEmpty( ) == false )
		{
			ID3D11ShaderReflectionVariable* variable = reflector->GetVariableByName( name.GetValue( ) );
			if ( variable != nullptr )
			{
				_variables.Append( name, variable->GetInterfaceSlot( 0 ) );
				return true;
			}
		}

		return false;
	}

	// get the array of class linkage permutations
	ID3D11ClassInstance** LinkageController::GetClassLinkageArray( )
	{
		return _linkageArray; // TODO: needs modification to get the array from IDrawable object.
	}

	// get the number of interface slots in this linkage
	UInt LinkageController::GetInterfaceCount( ) const
	{
		return _interfaceSlotCount;
	}
}