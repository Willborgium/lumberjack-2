/**
 * Vertex.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\Vertex.h>

using namespace DirectX;

namespace HyJynxRenderer
{
	// ===================================================

	VertexPosition::VertexPosition( )
		: _position()
	{ }

	VertexPosition::VertexPosition( _In_ Vector3 pos )
		: _position( pos )
	{ }

	VertexPosition::VertexPosition( _In_ float x, _In_ float y, _In_ float z )
		: _position( x, y, z )
	{ }

	// ===================================================

	VertexPositionColor::VertexPositionColor()
		: _position(),
		_color()
	{ }

	VertexPositionColor::VertexPositionColor( _In_ Vector3 pos, _In_ Vector4 col )
		: _position( pos ),
		_color( col )
	{ }

	// ===================================================

	VertexPositionColorTexture::VertexPositionColorTexture( )
		: _position( ),
		_color( )
	{ }

	VertexPositionColorTexture::VertexPositionColorTexture( _In_ Vector3 pos, _In_ Vector4 col, _In_ Vector2 tex )
		: _position( pos ),
		_color( col ),
		_texCoord( tex )
	{ }

	// ===================================================

	VertexPositionNormalColor::VertexPositionNormalColor()
		:_position(),
		_normal(),
		_color()
	{ }

	VertexPositionNormalColor::VertexPositionNormalColor( _In_ Vector3 pos, _In_ Vector3 norm, _In_ Vector4 col )
		: _position( pos ),
		_normal( norm ),
		_color( col )
	{ }

	// ===================================================

	VertexPositionTexture::VertexPositionTexture()
		: _position(),
		_texCoord()
	{ }

	VertexPositionTexture::VertexPositionTexture( _In_ Vector3 position, _In_ Vector2 texCoord )
		: _position( position ),
		_texCoord( texCoord )
	{ }

	// ===================================================

	VertexPositionNormalTexture::VertexPositionNormalTexture()
		: _position(),
		_normal(),
		_texCoord()
	{ }

	VertexPositionNormalTexture::VertexPositionNormalTexture( _In_ Vector3 position, _In_ Vector3 normal, _In_ Vector2 texCoord )
		: _position( position ),
		_normal( normal ),
		_texCoord( texCoord )
	{ }

	// ===================================================
}