/**
 * Matrix3.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\Matrix3.h>

using namespace DirectX;

namespace HyJynxRenderer
{
	// identity ctor
	Matrix3::Matrix3( )
	{
		_elements[ 0 ] = XMVectorSet( 1.0f, 0.0f, 0.0f, 1.0f );
		_elements[ 1 ] = XMVectorSet( 0.0f, 1.0f, 0.0f, 1.0f );
		_elements[ 2 ] = XMVectorSet( 0.0f, 0.0f, 1.0f, 1.0f );
	}

	// default ctor
	Matrix3::Matrix3( _In_ XMFLOAT4X4 mat )
	{
		float x, y, z;

		x = mat._11;
		y = mat._12;
		z = mat._13;

		_elements[ 0 ] = XMVectorSet( x, y, z, 1.0f );

		x = mat._21;
		y = mat._22;
		z = mat._23;

		_elements[ 1 ] = XMVectorSet( x, y, z, 1.0f );

		x = mat._31;
		y = mat._32;
		z = mat._33;

		_elements[ 2 ] = XMVectorSet( x, y, z, 1.0f );
	}

	// set elements to an absolute value
	void Matrix3::Absolute( )
	{
		_elements[ 0 ] = XMVectorAbs( _elements[ 0 ] );
		_elements[ 1 ] = XMVectorAbs( _elements[ 1 ] );
		_elements[ 2 ] = XMVectorAbs( _elements[ 2 ] );
	}

	// get a row out of this matrix
	XMVECTOR Matrix3::GetRow(_In_ UInt index)
	{
		return _elements[ index ];
	}

	// get a column of this matrix
	XMVECTOR Matrix3::GetColumn( _In_ UInt index )
	{
		float x, y, z;

		switch ( index )
		{
		case 0:

			x = XMVectorGetX( _elements[ 0 ] );
			y = XMVectorGetX( _elements[ 1 ] );
			z = XMVectorGetX( _elements[ 2 ] );
			break;

		case 1:

			x = XMVectorGetY( _elements[ 0 ] );
			y = XMVectorGetY( _elements[ 1 ] );
			z = XMVectorGetY( _elements[ 2 ] );
			break;

		case 2:

			x = XMVectorGetZ( _elements[ 0 ] );
			y = XMVectorGetZ( _elements[ 1 ] );
			z = XMVectorGetZ( _elements[ 2 ] );
			break;
		}

		return XMVectorSet( x, y, z, 1.0f );
	}

	// get a value from the matrix
	float Matrix3::GetValue( _In_ UInt row, _In_ UInt col )
	{
		XMVECTOR rowVector = _elements[ row ];

		float val;

		switch ( col )
		{
		case 0:

			val = XMVectorGetX( rowVector );
			break;

		case 1:

			val = XMVectorGetY( rowVector );
			break;

		case 2:

			val = XMVectorGetZ( rowVector );
			break;
		}

		return val;
	}
}