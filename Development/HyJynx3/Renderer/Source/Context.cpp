/**
 * Context.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\Context.h>
#include <Renderer\Headers\VertexBuffer.h>
#include <Renderer\Headers\IndexBuffer.h>
#include <Renderer\Headers\ShaderFactory.h>
#include <Renderer\Headers\Renderer.h>
#include <DirectXMath.h>

using namespace DirectX;
using namespace HyJynxCollections;
using namespace HyJynxCore;

namespace HyJynxRenderer
{
	// null tor
	Context::Context( )
	{ }

	// default used ctor
	Context::Context( _In_ ID3D11DeviceContext* context )
		:_deviceContext( context )
	{ }

	// dtor
	Context::~Context( )
	{
		if ( _deviceContext )
		{
			_deviceContext->ClearState( );
			_deviceContext->Release( );
		}
	}

	// Get direct access to the DX11 Device Context object
	ID3D11DeviceContext* Context::GetDeviceContext( )
	{
		return _deviceContext;
	}

#pragma region PreRender

	// bind a viewport to the context
	void Context::BindViewPort( _In_ UInt index, _In_ D3D11_VIEWPORT* viewPort )
	{
		if ( _deviceContext == nullptr )
		{
			LogError( "BindViewPort() - unable to complete deviceContext is null." );
			return;
		}

		_deviceContext->RSSetViewports( index, viewPort );
	}

	// bind render/depth targets as active on the gpu
	void Context::BindTargets( _In_ ID3D11RenderTargetView* target, _In_opt_ DepthStencil* depth )
	{
		if ( _deviceContext == nullptr )
		{
			LogError( "BindViewPort() - unable to complete deviceContext is null." );
			return;
		}

		if ( target != nullptr )
		{
			_deviceContext->OMSetRenderTargets( 1, &target, depth != nullptr ? depth->GetView() : nullptr );
			if ( depth != nullptr )
			{
				_deviceContext->OMSetDepthStencilState( depth->GetState( ), 1 );
			}
		}
		else
		{
			LogError( "BindTargets() - RenderTarget is not optional." );
		}
	}

	// clear the given render target with a color
	void Context::ClearScreen( _In_ ID3D11RenderTargetView* target, _In_opt_ Color* color )
	{
		if ( color == nullptr )
		{
			LogWarning( "ClearScreen(target, nullptr) - argued color is null, will use default black." );
		}

		if ( target == nullptr )
		{
			LogWarning( "ClearScreen( nullptr, color ) - argued target is null, unable to complete." );
			return;
		}

		if ( _deviceContext == nullptr )
		{
			LogError( "ClearScreen() - device context is null, are we initialized?" );
			return;
		}

		XMFLOAT4 col;

		if ( color != nullptr )
		{
			col = *color->GetMultipliedColor( );
		}
		else
		{
			col = { 0.0f, 0.0f, 0.0f, 1.0f };
		}

		float clearColor[ 4 ] = { col.x, col.y, col.z, col.w };

		_deviceContext->ClearRenderTargetView( target, clearColor );
	}

	// set a different rasterizer to the GPU
	void Context::SetRasterizer( _In_ const Text& rasterizer )
	{
		if ( _deviceContext == nullptr )
		{
			LogError( "SetRasterizer() - device context is null, unable to continue." );
			return;
		}

		if ( _current.Rasterizer == rasterizer )
		{
			return;
		}

		ID3D11RasterizerState* state = Renderer::Instance( )->GetRasterizer( rasterizer );

		if ( state == nullptr )
		{
			LogError( "SetRasterizer( nullptr ) - argued rasterizer is null, unable to continue." );
			return;
		}

		_deviceContext->RSSetState( state );
		_current.Rasterizer = rasterizer;
	}

	// get the name of the active rasterizer
	Text Context::GetCurrentRasterizerState( ) const
	{
		return _current.Rasterizer;
	}

#pragma endregion

#pragma region Mapping

	// un-map buffer data for GPU usage
	void Context::UnMapData( _In_ ID3D11Buffer* buffer )
	{
		if ( buffer != nullptr )
		{
			_deviceContext->Unmap( buffer, 0 );
		}
	}

#pragma endregion

#pragma region ConstantBuffers

	// map the project buffer for cpu use
	CBCameraProjection* Context::GetProjectionConstantBuffer( )
	{
		if ( _deviceContext == nullptr )
		{
			LogError( "UpdateCycleConstantBuffer() - device context is null, unable to continue." );
			return nullptr;
		}

		D3D11_MAPPED_SUBRESOURCE resource;
		HRESULT result = _deviceContext->Map( ShaderFactory::Instance( )->GetProjectionConstant( ),
			0, D3D11_MAP_WRITE_DISCARD, 0, &resource );

		if ( FAILED( result ) )
		{
			return nullptr;
		}

		return reinterpret_cast< CBCameraProjection* >( resource.pData );
	}

	// update the projection constant buffer on the GPU
	void Context::UpdateProjectionConstantBuffer( )
	{
		if ( _deviceContext == nullptr )
		{
			LogError( "UpdateCycleConstantBuffer() - device context is null, unable to continue." );
			return;
		}

		_deviceContext->Unmap( ShaderFactory::Instance( )->GetProjectionConstant( ), 0 );
	}

	// map the cycle buffer for cpu use
	CBCycle* Context::GetCycleConstantBuffer( )
	{
		if ( _deviceContext == nullptr )
		{
			LogError( "UpdateCycleConstantBuffer() - device context is null, unable to continue." );
			return nullptr;
		}

		D3D11_MAPPED_SUBRESOURCE resource;
		HRESULT result = _deviceContext->Map( ShaderFactory::Instance( )->GetCycleConstant( ),
			0, D3D11_MAP_WRITE_DISCARD, 0, &resource );

		if ( FAILED( result ) )
		{
			return nullptr;
		}

		return reinterpret_cast< CBCycle* >( resource.pData );
	}

	// update constant buffers to the GPU
	void Context::UpdateCycleConstantBuffer( )
	{
		if ( _deviceContext == nullptr )
		{
			LogError( "UpdateCycleConstantBuffer() - device context is null, unable to continue." );
			return;
		}

		_deviceContext->Unmap( ShaderFactory::Instance( )->GetCycleConstant( ), 0 );
	}

	// map camera buffer for cpu use
	CBCamera* Context::GetCameraConstantBuffer( )
	{
		if ( _deviceContext == nullptr )
		{
			LogError( "UpdateCycleConstantBuffer() - device context is null, unable to continue." );
			return nullptr;
		}

		D3D11_MAPPED_SUBRESOURCE resource;
		HRESULT result = _deviceContext->Map( ShaderFactory::Instance( )->GetCameraConstant( ),
			0, D3D11_MAP_WRITE_DISCARD, 0, &resource );

		if ( FAILED( result ) )
		{
			return nullptr;
		}

		return reinterpret_cast< CBCamera* >( resource.pData );
	}

	// update camera constant buffers to the GPU
	void Context::UpdateCameraConstantBuffer( )
	{
		if ( _deviceContext == nullptr )
		{
			LogError( "UpdateCameraConstantBuffer() - device context is null, unable to continue." );
			return;
		}

		_deviceContext->Unmap( ShaderFactory::Instance( )->GetCameraConstant( ), 0 );
	}

	// map the per-object buffer for cpu use
	CBObject* Context::GetObjectConstantBuffer( )
	{
		if ( _deviceContext == nullptr )
		{
			LogError( "UpdateCycleConstantBuffer() - device context is null, unable to continue." );
			return nullptr;
		}

		D3D11_MAPPED_SUBRESOURCE resource;
		HRESULT result = _deviceContext->Map( ShaderFactory::Instance( )->GetObjectConstant( ),
			0, D3D11_MAP_WRITE_DISCARD, 0, &resource );

		if ( FAILED( result ) )
		{
			return nullptr;
		}

		return reinterpret_cast< CBObject* >( resource.pData );
	}

	// update per-object constant buffers
	void Context::UpdateObjectConstantBuffer( )
	{
		if ( _deviceContext == nullptr )
		{
			LogError( "UpdateObjectConstatBuffer() - device context is null, unable to continue." );
			return;
		}

		_deviceContext->Unmap( ShaderFactory::Instance( )->GetObjectConstant( ), 0 );
	}

	// Set the Constant Buffers as active on the GPU
	void Context::SetVertexConstantBuffer( _In_ const UInt index, _In_ ID3D11Buffer* buffer )
	{
		if ( _deviceContext == nullptr )
		{
			LogError( "SetConstantBuffers() - device context is null, unable to continue." );
			return;
		}

		_deviceContext->VSSetConstantBuffers( index, 1, &buffer );
	}

	// bind a list of sequential buffers at once
	void Context::SetVertexConstantBuffer( _In_ const UInt startIndex, 
		_In_ ID3D11Buffer* bufferArray, _In_ const UInt count )
	{
		if ( _deviceContext != nullptr )
		{
			LogError( "SetConstantBuffers() - device context is null, unable to continue." );
			return;
		}

		_deviceContext->VSSetConstantBuffers( startIndex, count, &bufferArray );
	}

#pragma endregion

#pragma region ShaderBindings

	// Set the argued shader component as the active shader on the GPU
	void Context::BindVertexShader( _In_ ID3D11VertexShader* shader )
	{
		if ( _deviceContext == nullptr )
		{
			LogError( "BindVertexShader() - device context is null, unable to continue." );
			return;
		}

		_deviceContext->VSSetShader( shader, nullptr, 0 );
	}

	// Set the argued shader component as the active shader on the GPU
	void Context::BindPixelShader( _In_ ID3D11PixelShader* shader,
		_In_opt_ ID3D11ClassInstance** instanceArray, _In_opt_ UInt instanceCount )
	{
		if ( _deviceContext == nullptr )
		{
			LogError( "BindPixelShader() - device context is null, unable to continue." );
			return;
		}

		ID3D11ClassInstance** check = nullptr;

		if ( instanceArray != nullptr && *instanceArray != nullptr )
		{
			check = instanceArray;
		}

		_deviceContext->PSSetShader( shader, check, instanceCount );
	}

	// Set the argued shader component as the active shader on the GPU
	void Context::BindHullShader( _In_ ID3D11HullShader* shader,
		_In_opt_ ID3D11ClassInstance** instanceArray, _In_opt_ UInt instanceCount )
	{
		if ( _deviceContext == nullptr )
		{
			LogError( "BindHullShader() - device context is null, unable to continue." );
			return;
		}

		ID3D11ClassInstance** check = nullptr;

		if ( instanceArray != nullptr && *instanceArray != nullptr )
		{
			check = instanceArray;
		}

		_deviceContext->HSSetShader( shader, check, instanceCount );
	}

	// Set the argued shader component as the active shader on the GPU
	void Context::BindDomainShader( _In_ ID3D11DomainShader* shader,
		_In_opt_ ID3D11ClassInstance** instanceArray, _In_opt_ UInt instanceCount )
	{
		if ( _deviceContext == nullptr )
		{
			LogError( "BindDomainShader() - device context is null, unable to continue." );
			return;
		}

		ID3D11ClassInstance** check = nullptr;

		if ( instanceArray != nullptr && *instanceArray != nullptr )
		{
			check = instanceArray;
		}

		_deviceContext->DSSetShader( shader, check, instanceCount );
	}

	// Set the argued shader component as the active shader on the GPU
	void Context::BindGeometryShader( _In_ ID3D11GeometryShader* shader,
		_In_opt_ ID3D11ClassInstance** instanceArray, _In_opt_ UInt instanceCount )
	{
		if ( _deviceContext == nullptr )
		{
			LogError( "BindGeometryShader() - device context is null, unable to continue." );
			return;
		}

		ID3D11ClassInstance** check = nullptr;

		if ( instanceArray != nullptr && *instanceArray != nullptr )
		{
			check = instanceArray;
		}

		_deviceContext->GSSetShader( shader, check, instanceCount );
	}

#pragma endregion

#pragma region Drawing

	// bind the vertex topology
	void Context::BindVertexTopology( _In_ ID3D11InputLayout* layout )
	{
		if ( _deviceContext == nullptr )
		{
			LogError( "BindVertexTopology() - device context is null, unable to continue." );
			return;
		}

		if ( _current.Layout == nullptr || _current.Layout != layout )
		{
			_deviceContext->IASetInputLayout( layout );
			_current.Layout = layout;
		}
	}

	// set active buffers
	void Context::SetActiveBuffers( _In_ VertexBuffer* verts, _In_ IndexBuffer* indicies )
	{
		if ( _deviceContext == nullptr )
		{
			LogError( "SetActiveBuffers() - device context is null, unable to continue." );
			return;
		}
		
		ID3D11Buffer* buffer = verts->GetBuffer( );
		UInt stride = verts->GetDescription( ).Size;
		UInt offset = 0;

		_deviceContext->IASetVertexBuffers( 0, 1, &buffer, &stride, &offset );

		_deviceContext->IASetIndexBuffer( indicies->GetBuffer( ), DXGI_FORMAT_R32_UINT, 0 );
	}

	// set the active primitive topology
	void Context::SetPrimitiveTopology( _In_ const D3D_PRIMITIVE_TOPOLOGY topology )
	{
		if ( _deviceContext == nullptr )
		{
			LogError( "SetActiveBuffers() - device context is null, unable to continue." );
			return;
		}

		if ( _current.VertexTopology != topology )
		{
			_deviceContext->IASetPrimitiveTopology( topology );
			_current.VertexTopology = topology;
		}
	}

	// draw verticies by their index
	void Context::DrawIndexed( _In_ const UInt count, _In_ const UInt first, _In_ const int offset )
	{
		if ( _deviceContext == nullptr )
		{
			LogError( "SetActiveBuffers() - device context is null, unable to continue." );
			return;
		}
		
		_deviceContext->DrawIndexed( count, first, offset );
	}

#pragma endregion

}