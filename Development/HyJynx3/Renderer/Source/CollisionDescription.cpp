/**
 * CollisionDescription.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\CollisionDescription.h>

using namespace DirectX;

namespace HyJynxRenderer
{
	// null ctor
	CollisionDescription::CollisionDescription( )
	{ }

	// copy ctor
	CollisionDescription::CollisionDescription( _In_ const CollisionDescription& other )
		: _sphere( other._sphere ),
		_aabb( other._aabb ),
		_dxobb( other._dxobb ),
		_obb( other._obb )
	{ }

	// move ctor
	CollisionDescription::CollisionDescription( _In_ const CollisionDescription&& other )
		: _sphere( other._sphere ),
		_aabb( other._aabb ),
		_dxobb( other._dxobb ),
		_obb( other._obb )
	{ }

	// dtor
	CollisionDescription::~CollisionDescription( )
	{
		_sphere = nullptr;
		_aabb = nullptr;
		_dxobb = nullptr;
		_obb = nullptr;
	}

	// optionally set any collidables the caller chooses
	void CollisionDescription::SetCollidables( _In_opt_ DirectX::BoundingSphere* sphere,
		_In_opt_ DirectX::BoundingBox* aabb,
		_In_opt_ DirectX::BoundingOrientedBox* obb1,
		_In_opt_ OrientedBoundingBox* obb2 )
	{
		if ( sphere != nullptr )
		{
			_sphere = sphere;
		}
		if ( aabb != nullptr )
		{
			_aabb = aabb;
		}
		if ( obb1 != nullptr )
		{
			_dxobb = obb1;
		}
		if ( obb2 != nullptr )
		{
			_obb = obb2;
		}
	}

	// return bounding sphere
	BoundingSphere* CollisionDescription::GetBoundingSphere( )
	{
		return _sphere;
	}

	// return the AABB
	BoundingBox* CollisionDescription::GetBoundingBox( )
	{
		return _aabb;
	}
	
	// return dx obb
	BoundingOrientedBox* CollisionDescription::GetOBBLevel1( )
	{
		return _dxobb;
	}

	// return the obb
	OrientedBoundingBox* CollisionDescription::GetOBBLevel2( )
	{
		return _obb;
	}

	// recursive helper function for CollisionDescription::IsInFrustum
	bool CollisionDescription::testFrustum( _In_ ViewFrustum& frustum, 
		_In_ const CollisionMethod maximum, _In_ const CollisionMethod current )
	{
		// we reached the maximum without proving we're inside the frustum
		if ( current > maximum )
		{
			return false;
		}

		switch ( current )
		{
		case CollisionMethod::COLLISION_SPHERE:

			if ( _sphere != nullptr )
			{
				if ( frustum.IsInside( *_sphere ) == false )
				{
					return testFrustum( frustum, maximum, CollisionMethod::COLLISION_AABB );
				}
			}

			break;

		case CollisionMethod::COLLISION_AABB:

			if ( _aabb != nullptr )
			{
				if ( frustum.IsInside( *_aabb ) == false )
				{
					return testFrustum( frustum, maximum, CollisionMethod::COLLISION_OBB_LEVEL1 );
				}
			}

			break;

		case CollisionMethod::COLLISION_OBB_LEVEL1:

			if ( _dxobb != nullptr )
			{
				if ( frustum.IsInside( *_dxobb ) == false )
				{
					return testFrustum( frustum, maximum, CollisionMethod::COLLISION_OBB_LEVEL2 );
				}
			}

			break;

		case CollisionMethod::COLLISION_OBB_LEVEL2:

			if ( _obb != nullptr )
			{
				return frustum.IsInside( *_obb );
			}

			break;
		}

		// we'd return out from the switch, or we'll have to default true here
		return true;
	}

	// determine if we're in the argued frustum
	bool CollisionDescription::IsInFrustum( _In_ ViewFrustum& frustum, _In_ const CollisionMethod maximum )
	{
		if ( maximum == CollisionMethod::NONE )
		{
			return true;
		}
		else
		{
			return testFrustum( frustum, maximum, CollisionMethod::COLLISION_SPHERE );
		}
	}

	// get the dimensions of this description
	Vector3 CollisionDescription::GetDimensions( ) const
	{
		if ( _obb != nullptr )
		{
			return *_obb->GetExtents( );
		}

		if ( _dxobb != nullptr )
		{
			return Vector3( _dxobb->Extents );
		}

		if ( _aabb != nullptr )
		{
			return Vector3( _aabb->Extents );
		}

		if ( _sphere != nullptr )
		{
			return Vector3( _sphere->Radius * 2.0f );
		}

		return Vector3::Zero( );
	}
}