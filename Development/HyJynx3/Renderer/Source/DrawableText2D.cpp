/**
 * DrawableText2D.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\DrawableText2D.h>

using namespace HyJynxCore;
using namespace HyJynxLogger;

namespace HyJynxRenderer
{
	// null ctor
	DrawableText2D::DrawableText2D()
		: Drawable( )
	{ }

	// copy ctor
	DrawableText2D::DrawableText2D( _In_ const DrawableText2D& other )
		: Drawable( other )
	{ }

	// move ctor
	DrawableText2D::DrawableText2D( _In_ const DrawableText2D&& other )
		: Drawable( other )
	{ }

	// dtor
	DrawableText2D::~DrawableText2D( )
	{ }

	// drawables are constructed with the data they need to load,
	// this kicks off the sequence to load and and register the mesh buffers
	void DrawableText2D::LoadModel( _In_ Function< void > onComplete )
	{ }

	// this should be called after a drawable is constructed, and LoadModel() called
	void DrawableText2D::GenerateBounding( )
	{ }

	// per-cycle update call
	void DrawableText2D::Update( _In_ Context* context, _In_ const RenderTime renderTime )
	{ }

	// when we're in development, this API will help us draw debugging information to the screen
	void DrawableText2D::RenderDebug( _In_ Context* context )
	{ }
}