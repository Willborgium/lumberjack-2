/**
 * ViewFrustum.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\ViewFrustum.h>

using namespace DirectX;
using namespace HyJynxCollections;
using namespace HyJynxLogger;
using namespace HyJynxUtilities;

namespace HyJynxRenderer
{
	// null ctor
	ViewFrustum::ViewFrustum( )
		: Dirty( false ),
		ILog( "ViewFrustum" )
	{
		XMMATRIX mat = XMMatrixPerspectiveFovLH( _fieldOfView * XM_PI, _aspectRatio, _nearClip, _farClip );
		BoundingFrustum::CreateFromMatrix( _frustum, mat );
		XMStoreFloat4x4( &_projectionMatrix, mat );
	}

	// copy ctor
	ViewFrustum::ViewFrustum( const ViewFrustum& other )
		: Dirty( false ), 
		ILog( "ViewFrustum" ),
		_fieldOfView( other._fieldOfView ),
		_nearClip( other._nearClip ),
		_farClip( other._farClip ),
		_aspectRatio( other._aspectRatio )
	{
		XMMATRIX mat = XMMatrixPerspectiveFovLH( _fieldOfView * XM_PI, _aspectRatio, _nearClip, _farClip );
		BoundingFrustum::CreateFromMatrix( _frustum, mat );
		XMStoreFloat4x4( &_projectionMatrix, mat );
	}

	// move ctor
	ViewFrustum::ViewFrustum( _In_ const ViewFrustum&& other )
		: Dirty( false ),
		ILog( "ViewFrustum" ),
		_fieldOfView( other._fieldOfView ),
		_nearClip( other._nearClip ),
		_farClip( other._farClip ),
		_aspectRatio( other._aspectRatio )
	{
		XMMATRIX mat = XMMatrixPerspectiveFovLH( _fieldOfView * XM_PI, _aspectRatio, _nearClip, _farClip );
		BoundingFrustum::CreateFromMatrix( _frustum, mat );
		XMStoreFloat4x4( &_projectionMatrix, mat );
	}

	// dtor
	ViewFrustum::~ViewFrustum( )
	{ }

	// update the frustum with current values
	void ViewFrustum::CleanFrustum( )
	{
		XMMATRIX mat = XMMatrixPerspectiveFovLH( _fieldOfView * XM_PI, _aspectRatio, _nearClip, _farClip );
		BoundingFrustum::CreateFromMatrix( _frustum, mat );
		XMStoreFloat4x4( &_projectionMatrix, mat );
		ToggleClean( );
	}

	// DX frustum return
	BoundingFrustum& ViewFrustum::GetFrustum( )
	{
		if ( IsDirty( ) == true )
		{
			CleanFrustum( );
		}

		return _frustum;
	}

	// clean, get the projection matrix
	Matrix& ViewFrustum::GetProjectionMatrix( )
	{
		if ( IsDirty( ) == true )
		{
			CleanFrustum( );
		}

		return _projectionMatrix;
	}

	// get the projection matrix loaded into a byte aligned simd reg
	DirectX::XMMATRIX ViewFrustum::GetProjectionMatrixXM( )
	{
		if ( IsDirty( ) == true )
		{
			CleanFrustum( );
		}

		return XMLoadFloat4x4( &_projectionMatrix );
	}

	// update position/orientation
	void ViewFrustum::Update( _In_ const Vector4& orientation, _In_ const Vector3& position )
	{
		_frustum.Orientation = orientation;
		_frustum.Origin = position;
	}

#pragma region Culling

	// test the bounding sphere for collision
	bool ViewFrustum::IsInside( _In_ BoundingSphere& subject, _Out_opt_ ContainmentType* type )
	{
		XMVECTOR left, top, right, bottom, nearPlane, farPlane;

		_frustum.GetPlanes( &nearPlane, &farPlane, &right, &left, &top, &bottom );

		ContainmentType outputType = subject.ContainedBy( left, top, right, bottom, nearPlane, farPlane );
		
		if ( type != nullptr )
		{
			*type = outputType;
		}

		return outputType > ContainmentType::DISJOINT;
	}

	// test the bounding box for collision
	bool ViewFrustum::IsInside( _In_ BoundingBox& subject, _Out_opt_ ContainmentType* type )
	{
		XMVECTOR left, top, right, bottom, nearPlane, farPlane;

		_frustum.GetPlanes( &nearPlane, &farPlane, &right, &left, &top, &bottom );

		ContainmentType outputType = subject.ContainedBy( left, top, right, bottom, nearPlane, farPlane );

		if ( type != nullptr )
		{
			*type = outputType;
		}

		return outputType > ContainmentType::DISJOINT;
	}

	// test the OBB for collision
	bool ViewFrustum::IsInside( _In_ BoundingOrientedBox& subject, _Out_opt_ ContainmentType* type )
	{
		XMVECTOR left, top, right, bottom, nearPlane, farPlane;

		_frustum.GetPlanes( &nearPlane, &farPlane, &right, &left, &top, &bottom );

		ContainmentType outputType = subject.ContainedBy( left, top, right, bottom, nearPlane, farPlane );

		if ( type != nullptr )
		{
			*type = outputType;
		}

		return outputType > ContainmentType::DISJOINT;
	}

	// test the frustum for collision
	bool ViewFrustum::IsInside( _In_ ViewFrustum& subject, _Out_opt_ ContainmentType* type )
	{
		XMVECTOR left, top, right, bottom, nearPlane, farPlane;

		_frustum.GetPlanes( &nearPlane, &farPlane, &right, &left, &top, &bottom );

		ContainmentType outputType = subject.GetFrustum( ).ContainedBy( left, top, right, bottom, nearPlane, farPlane );

		if ( type != nullptr )
		{
			*type = outputType;
		}

		return outputType > ContainmentType::DISJOINT;
	}

	// test the ray for collision
	bool ViewFrustum::IsInside( _In_ Ray& subject, _Out_opt_ float* outDistance )
	{
		XMVECTOR origin = XMLoadFloat3( subject.GetOrigin( ) );
		XMVECTOR direction = XMLoadFloat3( subject.GetVector( ) );

		float distance;

		if ( _frustum.Intersects( origin, direction, distance ) == true )
		{
			*outDistance = distance;
			return true;
		}

		return false;
	}

	// test the OBB for collision
	bool ViewFrustum::IsInside( _In_ OrientedBoundingBox& obb )
	{
		// TODO
		LogWarning( "IsInside( OrientedBoundingBox* ) - UnImplemented Function! TODO" );
		return true;
	}

#pragma endregion

#pragma region Accessors

	// get the current near-clipping range
	float ViewFrustum::GetNearClipRange( ) const
	{
		return _nearClip;
	}

	// set a new value to the near-clip range, will recalculate proj. matrix
	void ViewFrustum::SetNearClipRange( _In_ float clip )
	{
		if ( clip < 0 )
		{
			LogWarning( "SetNearClipRange( clip < 0 ) - near clipping range cannot be less than 0, no change occured." );
			return;
		}

		_nearClip = clip;

		ToggleDirty( );
	}

	// get the far clipping range
	float ViewFrustum::GetFarClipRange( ) const
	{
		return _farClip;
	}

	// set a ne value to the far-clip range, will recalculate proj. matrix
	void ViewFrustum::SetFarClipRange( _In_ float clip )
	{
		if ( clip < 0 )
		{
			LogWarning( "SetFarClipRange( clip < 0 ) - far clipping range cannot be less than 0, no change occured." );
			return;
		}

		_farClip = clip;

		ToggleDirty( );
	}

	// return the current aspect ratio
	float ViewFrustum::GetAspectRatio( ) const
	{
		return _aspectRatio;
	}

	// set a new aspect ratio, and recalculate projection matrix
	void ViewFrustum::SetAspectRatio( _In_ float aspectRatio )
	{
		if ( aspectRatio == 0 )
		{
			LogWarning( "SetAspectRatio(0) - Aspect Ratio cannot be 0, no change occurred." );
			return;
		}

		_aspectRatio = aspectRatio;

		ToggleDirty( );
	}

	// get the field of view
	float ViewFrustum::GetFieldOfView( ) const
	{
		return _fieldOfView;
	}

	// set a new field of view
	void ViewFrustum::SetFieldOfView( _In_ float fov )
	{
		if ( fov < 0 )
		{
			LogWarning( "SetFieldOfView( fov < 0 ) - Field of View cannot be less than 0, no change occurred." );
			return;
		}

		_fieldOfView = fov;

		ToggleDirty( );
	}

#pragma endregion
}