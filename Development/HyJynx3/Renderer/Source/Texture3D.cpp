/**
* Texture3D.cpp
* (c) 2014 All Rights Reserved
*/

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\Texture3D.h>
#include <Renderer\Headers\TextureManager.h>

namespace HyJynxRenderer
{
	// null ctor
	Texture3D::Texture3D( )
		: Texture( )
	{ }

	// default ctor
	//
	Texture3D::Texture3D( _In_ LoadedTexture* tex )
		: Texture( tex )
	{ }

	// copy ctor
	Texture3D::Texture3D( _In_ const Texture3D& )
	{ }

	// move ctor
	Texture3D::Texture3D( _In_ const Texture3D&& )
	{ }

	// dtor
	Texture3D::~Texture3D( )
	{ }

	// local helper function to read from cpu data
	void Texture3D::ReadFromCpuData( _In_ LoadedTexture* texture, _Out_ Color** output, _In_ Point3 indicies[ ], _In_ UInt count )
	{
		for ( UInt index = 0; index < count; index++ )
		{
			if ( indicies[ index ].X >= 0 && indicies[ index ].X < static_cast< int > ( texture->LoadedWidth )
				&& indicies[ index ].Y >= 0 && indicies[ index ].Y < static_cast< int > ( texture->LoadedHeight )
				&& indicies[ index ].Z >= 0 && indicies[ index ].Z < static_cast< int > ( texture->LoadedDepth ) )
			{
				*output[ index ] = Color( ); // TODO
			}
			else
			{
				*output[ index ] = Color( );
			}
		}
	}

	// local helper function to read from gpu data
	void Texture3D::ReadFromGpuData( _In_ Context* context, _In_ LoadedTexture* texture, _Out_ Color** output, _In_ Point3 indicies[ ], _In_ UInt count )
	{
		// TODO
	}
}