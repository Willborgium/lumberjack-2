/**
 * AnimationController.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\AnimationController.h>
#include <Renderer\Headers\Animator.h>

namespace HyJynxRenderer
{
	// null ctor
	AnimationController::AnimationController()
	{ }

	// default ctor
	AnimationController::AnimationController( _In_ IAnimation* const anim )
		: _animation( anim )
	{ }

	// copy ctor
	AnimationController::AnimationController( _In_ const AnimationController& other )
		: _animation( other._animation )
	{ }

	// move ctor
	AnimationController::AnimationController( _In_ const AnimationController&& other )
		: _animation( other._animation )
	{ }

	// dtor
	AnimationController::~AnimationController( )
	{
		_animation = nullptr;
	}

	// temporarily pause the animation this controller represents
	void AnimationController::Pause( )
	{
		if ( IsValid( ) == true )
		{
			_animation->SetActive( false );
		}
	}

	// resume a previously paused animation, calling this on an active animation has no effect
	void AnimationController::Resume( )
	{
		if ( IsValid( ) == true )
		{
			_animation->SetActive( true );
		}
	}

	// Set the animation to completion values (onComplete callback will occur)
	void AnimationController::Finish( )
	{
		if ( IsValid( ) == true )
		{
			_animation->SetProgress( 1.0 );
		}
	}

	// Set the animation to beginning values
	void AnimationController::Reset( )
	{
		if ( IsValid( ) == true )
		{
			_animation->SetProgress( 0.0 );
		}
	}

	// stop the aniation completely
	void AnimationController::Stop( )
	{
		if ( IsValid( ) == true )
		{
			_animation->Release( );
			_animation = nullptr;
		}
	}

	// set the FPS of the animation
	void AnimationController::SetSpeed( _In_ const double fps )
	{
		if ( IsValid() == true )
		{
			_animation->SetSpeed( fps );
		}
	}

	// get animation FPS
	double AnimationController::GetSpeed( )
	{
		return _animation != nullptr ? _animation->GetSpeed( ) : 0.0;
	}

	// determine if the controller is valid
	bool AnimationController::IsValid( )
	{
		bool output = true;

		if ( _animation != nullptr )
		{
			output = _animation->IsValid( );
		}
		else
		{
			output = false;
		}

		return output;
	}
}