/**
 * SceneLoader.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\SceneLoader.h>
#include <Renderer\Headers\FBXSceneIO.h>

using namespace HyJynxCore;
using namespace std;


namespace HyJynxRenderer
{
#pragma region ctorDtor

	// null ctor
	SceneLoader::SceneLoader()
	{
		_operations = new FBXSceneIO( );
	}

	// default ctor
	SceneLoader::SceneLoader( _In_ ISceneIO* const behavior )
	{
		_operations = behavior;
	}

	// copy ctor
	SceneLoader::SceneLoader( _In_ const SceneLoader& behavior )
	{ }

	// move ctor
	SceneLoader::SceneLoader( _In_ const SceneLoader&& )
	{ }

	// dtor
	SceneLoader::~SceneLoader( )
	{ }

#pragma endregion

#pragma region API

	// open and load a scene from file
	void SceneLoader::LoadFromFile( _In_ Text filepath,
		_In_ Function< void, Window* > onSuccess,
		_In_ Function< void, Text > onFail )
	{
		if ( filepath.IsNullOrEmpty( ) == false )
		{
			// TODO: make this async
			_operations->InitializeFromFile( filepath, onSuccess, onFail );
		}
		else
		{
			onFail( "Argued filepath is null or empty." );
		}
	}

	// load a scene from data
	void SceneLoader::LoadFromData( _In_ char* const fileData, 
		_In_ const UInt size,
		_In_ Function< void, Window* > onSuccess,
		_In_ Function< void, Text > onFail )
	{
		if ( fileData != nullptr )
		{
			_operations->InitializeFromData( fileData, size, onSuccess, onFail );
		}
		else
		{
			onFail( "Argued file data is null" );
		}
	}

	// Save the current scene to file
	void SceneLoader::SaveToFile( _In_ Text filepath,
		_In_ Window* const window,
		_In_ Function< void > onSuccess,
		_In_ Function< void, Text > onFail )
	{
		if ( filepath.IsNullOrEmpty( ) == false )
		{
			// TODO: make this async
			_operations->SaveToFile( filepath, window, onSuccess, onFail );
		}
		else
		{
			onFail( "Argued filepath is null or empty." );
		}
	}

#pragma endregion
}