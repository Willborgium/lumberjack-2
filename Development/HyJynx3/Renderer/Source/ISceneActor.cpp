/**
 * ISceneActor.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\ISceneActor.h>

using namespace HyJynxCore;

namespace HyJynxRenderer
{

#pragma region ctorDtor

	// null ctor
	ISceneActor::ISceneActor( )
	{ }

	// copy ctor
	ISceneActor::ISceneActor( _In_ const ISceneActor& other )
		: _id( other._id )
	{ }

	// move ctor
	ISceneActor::ISceneActor( _In_ const ISceneActor&& other )
		: _id( other._id )
	{ }

	// dtor
	ISceneActor::~ISceneActor( )
	{
		_id = "";
	}

#pragma endregion

	// get the ID of the actor
	const Text ISceneActor::GetActorID( ) const
	{
		return _id;
	}

	// set the ID of the actor
	void ISceneActor::SetActorID( _In_ const Text id )
	{
		_id = id;
	}
}