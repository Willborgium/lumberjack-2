/**
 * Graphics.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\Graphics.h>
#include <Logger\Headers\Logger.h>
#include <vector>

using namespace HyJynxLogger;
using namespace DirectX;
using namespace std;

namespace HyJynxRenderer
{
	// ============================================================
	//				Initialization
	// ============================================================

	// null ctor
	Graphics::Graphics( )
		: ILog( "DirectX 11 Graphics Library" ),
		_configuration( )
	{ }

	// configured ctor
	Graphics::Graphics( _In_ GraphicsConfiguration setting )
		: ILog( "DirectX 11 Graphics Library" ),
		_configuration( setting )
	{ }

	// copy ctor
	Graphics::Graphics( _In_ const Graphics& other )
		: ILog( "DirectX 11 Graphics Library" ),
		_configuration( other._configuration )
	{ }

	// move ctor
	Graphics::Graphics( _In_ const Graphics&& other )
		: ILog( "DirectX 11 Graphics Library" ),
		_configuration( other._configuration )
	{ }

	// dtor
	Graphics::~Graphics( )
	{
		Shutdown( );
	}

	// release directX components
	void Graphics::Shutdown( )
	{
		if ( _configuration.GetFullscreen( ) == true )
		{
			if ( _swapChain != nullptr )
			{
				_swapChain->SetFullscreenState( false, nullptr );
			}
		}

		SAFE_RELEASE( _d3dDevice );
		_d3dDevice = nullptr;

		SAFE_RELEASE( _swapChain );
		_swapChain = nullptr;

		// We shouldn't delete our own objects unless they were created with regular new.
		//SAFE_DELETE( _context );
	}

	// get a reference to the configuration object
	GraphicsConfiguration* Graphics::GetConfiguration( )
	{
		return &_configuration;
	}

	// set a totally new configuration
	void Graphics::SetConfiguration( _In_ GraphicsConfiguration* config )
	{
		_configuration = *config;
	}

	// get a list of hardware adapters
	vector< IDXGIAdapter* > _getAdapterList( )
	{
		IDXGIAdapter* adapter;
		vector <IDXGIAdapter*> adapterList;
		IDXGIFactory* factory = NULL;

		if ( FAILED( CreateDXGIFactory( __uuidof( IDXGIFactory ), ( void** ) &factory ) ) )
		{
			return adapterList;
		}

		for ( UInt i = 0; factory->EnumAdapters( i, &adapter ) != DXGI_ERROR_NOT_FOUND; i++ )
		{
			adapterList.push_back( adapter );
		}

		if ( factory )
		{
			factory->Release( );
		}

		return adapterList;
	}

	// with the given adapter, get a list of all display modes it supports
	DXGI_MODE_DESC* _getAdapterModeList( _In_ IDXGIAdapter* adapter )
	{
		if ( adapter == nullptr )
		{
			return nullptr;
		}

		IDXGIOutput* output = nullptr;

		if ( FAILED( adapter->EnumOutputs( 0, &output ) ) )
		{
			return nullptr;
		}

		UInt totalModes = 0;
		DXGI_MODE_DESC* displayModes = NULL;
		DXGI_FORMAT format = DXGI_FORMAT_R32G32B32A32_FLOAT;

		if ( FAILED( output->GetDisplayModeList( format, 0, &totalModes, NULL ) ) )
		{
			return nullptr;
		}

		displayModes = new DXGI_MODE_DESC[ totalModes ];

		if ( FAILED( output->GetDisplayModeList( format, 0, &totalModes, displayModes ) ) )
		{
			return nullptr;
		}

		return displayModes;
	}

	// get the adapter we think is considered the 'best'
	IDXGIAdapter* _getPreferredAdapter( _In_ vector<IDXGIAdapter*>* adapterList )
	{
		if ( adapterList->size( ) == 0 )
		{
			return nullptr;
		}

		if ( adapterList->size( ) == 1 )
		{
			return adapterList->at( 0 );
		}

		DXGI_MODE_DESC* modeList = nullptr;
		UInt highestDensity = -1;
		IDXGIAdapter* output = nullptr;

		for ( UInt index = 0; index < adapterList->size( ); index++ )
		{
			modeList = _getAdapterModeList( adapterList->at( index ) );
			
			if ( modeList != nullptr)
			{
				UInt totalpixels = modeList->Width * modeList->Height;
				
				if ( totalpixels > highestDensity )
				{
					highestDensity = totalpixels;
					output = adapterList->at( index );
				}

				delete modeList;
			}
		}

		return output;
	}

	// init in immediate mode
	bool Graphics::Initialize( _In_ HWND* window )
	{
		vector<IDXGIAdapter*> adapterList = _getAdapterList( );

		if ( adapterList.size( ) == 0 )
		{
			LogError( "InitializeImmediateMode(HWND) - No Adapaters." );
			Logger::Log( "Graphics Library Failed." );
			return false;
		}

		IDXGIAdapter* adapter = _getPreferredAdapter( &adapterList );

		if ( adapter == nullptr )
		{
			LogError( "InitializeImmediateMode(HWND) - No Preferred Adapter." );
			Logger::Log( "Graphics Library Failed." );
			return false;
		}

		DXGI_SWAP_CHAIN_DESC sd;
		ZeroMemory( &sd, sizeof( sd ) );
		sd.BufferCount = 1;
		sd.BufferDesc.Width = _configuration.GetWidth();
		sd.BufferDesc.Height = _configuration.GetHeight();
		sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		sd.BufferDesc.RefreshRate.Numerator = _configuration.GetRefreshRate();
		sd.BufferDesc.RefreshRate.Denominator = 1;
		sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		sd.OutputWindow = *window;
		sd.SampleDesc.Count = 1;
		sd.SampleDesc.Quality = 0;
		sd.Windowed = !_configuration.GetFullscreen();

		if ( _configuration.GetFullscreen( ) == true )
		{
			sd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
		}

		D3D_FEATURE_LEVEL featureLevel = D3D_FEATURE_LEVEL_11_0;
		UInt numberLevels = 1;
		D3D_FEATURE_LEVEL supportedFeatureLevel;
		ID3D11DeviceContext* context;
		UInt creationFlags = D3D11_CREATE_DEVICE_FLAG::D3D11_CREATE_DEVICE_BGRA_SUPPORT;

#ifdef _DEBUG
		if ( _configuration.GetDirectXDebug( ) == true )
		{
			Logger::Log( "DirectX 11 will be created in Debug Mode" );
			creationFlags |= D3D11_CREATE_DEVICE_FLAG::D3D11_CREATE_DEVICE_DEBUG;
		}
#endif

		if ( FAILED( D3D11CreateDeviceAndSwapChain(
			nullptr, 
			D3D_DRIVER_TYPE_HARDWARE,
			nullptr,
			0,
			&featureLevel,
			numberLevels,
			D3D11_SDK_VERSION,
			&sd,
			&_swapChain,
			&_d3dDevice,
			&supportedFeatureLevel,
			&context ) ) )
		{
			LogError( "InitializeImmediateMode(HWND) - Failed to create Device and SwapChain" );
			Logger::Log( "Graphics Library Failed." );
			return false;
		}

		_context = anew ( Context )( context );

		D3D11_VIEWPORT viewPort;
		viewPort.Width = ( float ) _configuration.GetWidth();
		viewPort.Height = ( float ) _configuration.GetHeight();
		viewPort.TopLeftX = viewPort.TopLeftY = 0;
		viewPort.MinDepth = 0.0f;
		viewPort.MaxDepth = 1.0f;

		_context->BindViewPort( 1, &viewPort );

		Logger::Log( "Graphics Library Initialized." );

		return true;
	}

	// reinitialize graphics library with current values
	bool Graphics::ReInitialize( )
	{
		// TODO (DO THIS ALREADY DAN!!)
		return true;
	}

	// create a new unbound render target
	ID3D11RenderTargetView* Graphics::CreateRenderTarget( _In_opt_ int width, _In_opt_ int height )
	{
		if ( _swapChain == nullptr )
		{
			LogError( "CreateRenderTarget() - Graphics Library is UnInitialized. SwapChain is Null." );
			return nullptr;
		}

		ID3D11Texture2D* backBuffer;

		HRESULT result = _swapChain->GetBuffer( 0, __uuidof( ID3D11Texture2D ), ( LPVOID* ) &backBuffer );

		if ( FAILED( result ) )
		{
			LogError( "CreateRenderTarget() - Failed to retrieve SwapChain Buffer." );
			return nullptr;
		}

		// TODO: width / height consideration

		ID3D11RenderTargetView* renderTarget;

		result = _d3dDevice->CreateRenderTargetView( backBuffer, nullptr, &renderTarget );

		if ( FAILED( result ) )
		{
			LogError( "CreateRenderTarget() - Failed to create Render Target." );
			return nullptr;
		}

		return renderTarget;
	}

	// create a depth stencil
	DepthStencil* Graphics::CreateDepthStencil( _In_opt_ int width, _In_opt_ int height )
	{
		ID3D11DepthStencilView* view;
		ID3D11Texture2D* texture;

		D3D11_TEXTURE2D_DESC descDepth;
		ZeroMemory( &descDepth, sizeof( descDepth ) );
		descDepth.Width = width > 0 ? width : _configuration.GetWidth( );
		descDepth.Height = height > 0 ? height : _configuration.GetHeight();
		descDepth.MipLevels = 1;
		descDepth.ArraySize = 1;
		descDepth.Format = DXGI_FORMAT::DXGI_FORMAT_R32_TYPELESS;
		descDepth.SampleDesc.Count = 1;
		descDepth.SampleDesc.Quality = 0;
		descDepth.Usage = D3D11_USAGE::D3D11_USAGE_DEFAULT;
		descDepth.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_DEPTH_STENCIL;
		descDepth.CPUAccessFlags = 0;
		descDepth.MiscFlags = 0;

		if ( FAILED( _d3dDevice->CreateTexture2D( &descDepth, NULL, &texture ) ) )
		{
			LogError( "CreateDepthStencil() - failed to create depth stencil texture." );
			return nullptr;
		}

		D3D11_DEPTH_STENCIL_DESC dsDesc;

		// Depth test parameters
		dsDesc.DepthEnable = true;
		dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK::D3D11_DEPTH_WRITE_MASK_ALL;
		dsDesc.DepthFunc = D3D11_COMPARISON_FUNC::D3D11_COMPARISON_LESS;

		// Stencil test parameters
		dsDesc.StencilEnable = true;
		dsDesc.StencilReadMask = 0xFF;
		dsDesc.StencilWriteMask = 0xFF;

		// Stencil operations if pixel is front-facing
		dsDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP::D3D11_STENCIL_OP_KEEP;
		dsDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP::D3D11_STENCIL_OP_INCR;
		dsDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP::D3D11_STENCIL_OP_KEEP;
		dsDesc.FrontFace.StencilFunc = D3D11_COMPARISON_FUNC::D3D11_COMPARISON_ALWAYS;

		// Stencil operations if pixel is back-facing
		dsDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP::D3D11_STENCIL_OP_KEEP;
		dsDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP::D3D11_STENCIL_OP_DECR;
		dsDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP::D3D11_STENCIL_OP_KEEP;
		dsDesc.BackFace.StencilFunc = D3D11_COMPARISON_FUNC::D3D11_COMPARISON_ALWAYS;

		// Create depth stencil state
		ID3D11DepthStencilState* state;
		if ( FAILED( _d3dDevice->CreateDepthStencilState( &dsDesc, &state ) ) )
		{
			LogError( "CreateDepthStencil() - failed to create depth stencil state." );
			return nullptr;
		}

		// Create the depth stencil view
		D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
		ZeroMemory( &descDSV, sizeof( descDSV ) );
		descDSV.Format = descDepth.Format;
		descDSV.ViewDimension = D3D11_DSV_DIMENSION::D3D11_DSV_DIMENSION_TEXTURE2D;
		descDSV.Texture2D.MipSlice = 0;

		if ( FAILED( _d3dDevice->CreateDepthStencilView( texture, &descDSV, &view ) ) )
		{
			LogError( "CreateDepthStencil() - failed to create depth stencil view" );
			return nullptr;
		}

		return anew( DepthStencil )( view, texture, state );
	}

	// create a vertex input layout
	void Graphics::CreateInputLayout( _In_ VertexDescription& description, 
		_In_ void* shaderCode, _In_ UInt codeSize, _Out_ ID3D11InputLayout** layout )
	{
		if ( _d3dDevice == nullptr )
		{
			LogError( "CreateInputLayout() - UnInitialized. Device is Null." );
			return;
		}

		HRESULT result = _d3dDevice->CreateInputLayout( description.Description, 
			description.ElementCount, shaderCode, codeSize, layout );

		if ( FAILED( result ) )
		{
			LogWarning( "CreateInputLayout() - Failed to create input layout." );
			*layout = nullptr;
			return;
		}
	}

	// create dynamic shader linkage object
	void Graphics::CreateClassLinkage( _Out_ ID3D11ClassLinkage** linkage )
	{
		if ( _d3dDevice == nullptr )
		{
			LogError( "CreateInputLayout() - UnInitialized. Device is Null." );
			return;
		}

		HRESULT result = _d3dDevice->CreateClassLinkage( linkage );

		if ( FAILED( result ) )
		{
			LogWarning( "CreateClassLinkage() - Failed to create class linkage object" );
		}
	}

	// create the directX 11 vertexBuffer
	VertexBuffer* Graphics::CreateVertexBuffer( _In_ D3D11_BUFFER_DESC* description, 
		_In_ D3D11_SUBRESOURCE_DATA* data, _In_ VertexDescription desc, _In_ const UInt count )
	{
		if ( _d3dDevice == nullptr )
		{
			LogError( "CreateVertexBuffer() - UnInitialized. Device is Null." );
			return nullptr;
		}

		ID3D11Buffer* buffer;
		if ( FAILED( _d3dDevice->CreateBuffer( description, data, &buffer ) ) )
		{
			return nullptr;
		}

		return anew ( VertexBuffer )( desc, buffer, count );
	}

	// create the directX11 indexBuffer
	IndexBuffer* Graphics::CreateIndexBuffer( _In_ D3D11_BUFFER_DESC* description, 
		_In_ D3D11_SUBRESOURCE_DATA* data, _In_ const UInt count )
	{
		if ( _d3dDevice == nullptr )
		{
			LogError( "CreateIndexBuffer() - UnInitialized. Device is Null." );
			return nullptr;
		}

		ID3D11Buffer* buffer;

		if ( FAILED( _d3dDevice->CreateBuffer( description, data, &buffer ) ) )
		{
			return nullptr;
		}

		return anew ( IndexBuffer )( buffer, count );
	}

	// present the backbuffer to the screen
	void Graphics::Preset( _In_opt_ UInt sync, _In_opt_ UInt flags )
	{
		if ( _swapChain == nullptr )
		{
			LogError( "Preset() - UnInitialized. SwapChain is Null." );
			return;
		}

		_swapChain->Present( sync, flags );
	}

	// Get the default used context (for immediate mode)
	Context* Graphics::GetDefaultContext( )
	{
		return _context;
	}

	// get the directx11 device
	ID3D11Device* Graphics::GetDevice( )
	{
		return _d3dDevice;
	}

	// width of screen
	UInt Graphics::GetWidth( ) const
	{
		return _configuration.GetWidth( );
	}

	// height of screen
	UInt Graphics::GetHeight( ) const
	{
		return _configuration.GetHeight( );
	}

	// ============================================================
	//				Testing API
	// ============================================================

	// determine if we can multi-thread render
	bool Graphics::CanDeferredRender( )
	{
		if ( _d3dDevice != nullptr )
		{

			HRESULT hr = _d3dDevice->CheckFeatureSupport(
				D3D11_FEATURE_THREADING,
				NULL,
				sizeof( D3D11_FEATURE_THREADING )
				);

			return !FAILED( hr );
		}
		else
		{
			LogError( "CanDeferredRender() - Unable to check. Device is Null." );
			return false;
		}
	}
}