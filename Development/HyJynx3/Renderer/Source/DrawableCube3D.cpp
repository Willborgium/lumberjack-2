/**
 * DrawableCube3D.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\DrawableCube3D.h>
#include <Renderer\Headers\Renderer.h>
#include <Renderer\Headers\Material3D.h>

using namespace HyJynxCore;

namespace HyJynxRenderer
{
	// null ctor
	DrawableCube3DConfiguration::DrawableCube3DConfiguration( )
		: PrimitiveConfiguration( )
	{ }

	// dimensions defined ctor
	DrawableCube3DConfiguration::DrawableCube3DConfiguration( _In_ const Vector3 dimensions )
		: PrimitiveConfiguration( ),
		_dimensions( dimensions )
	{ }

	// dimensions, solid color defined ctor
	DrawableCube3DConfiguration::DrawableCube3DConfiguration( _In_ const Vector3 dim, _In_ const Color col )
		: PrimitiveConfiguration( ),
		_dimensions( dim ),
		_topColor( col ),
		_bottomColor( col )
	{ }

	// dimensions, top-to-bottom gradient color defined ctor
	DrawableCube3DConfiguration::DrawableCube3DConfiguration( _In_ const Vector3 dim, _In_ const Color top, _In_ const Color bot )
		: PrimitiveConfiguration( ),
		_dimensions( dim ),
		_topColor( top ),
		_bottomColor( bot )
	{ }

	// copy ctor
	DrawableCube3DConfiguration::DrawableCube3DConfiguration( _In_ const DrawableCube3DConfiguration& other )
		: PrimitiveConfiguration( other ),
		_dimensions( other._dimensions ),
		_topColor( other._topColor ),
		_bottomColor( other._bottomColor )
	{ }

	// move ctor
	DrawableCube3DConfiguration::DrawableCube3DConfiguration( _In_ const DrawableCube3DConfiguration&& other )
		: PrimitiveConfiguration( other ),
		_dimensions( other._dimensions ),
		_topColor( other._topColor ),
		_bottomColor( other._bottomColor )
	{ }

	// dtor
	DrawableCube3DConfiguration::~DrawableCube3DConfiguration( )
	{ }

	// Get dimensions of the cube
	Vector3 DrawableCube3DConfiguration::GetDimensions( ) const
	{
		return _dimensions;
	}

	// set the dimensions of the cube
	void DrawableCube3DConfiguration::SetDimensions( _In_ const Vector3 dimensions )
	{
		bool dirty = false;

		if ( _dimensions.x != dimensions.x )
		{
			_dimensions.x = dimensions.x;
			dirty = true;
		}
		if ( _dimensions.y != dimensions.y )
		{
			_dimensions.y = dimensions.y;
			dirty = true;
		}
		if ( _dimensions.z != dimensions.z )
		{
			_dimensions.z = dimensions.z;
			dirty = true;
		}

		if ( dirty == true )
		{
			ToggleDirty( );
		}
	}

	// set the dimensions of the cube
	void DrawableCube3DConfiguration::SetDimensions( _In_opt_ const float x, _In_opt_ const float y, _In_opt_ const float z)
	{
		bool dirty = false;

		if ( x != NULL && _dimensions.x != x )
		{
			_dimensions.x = x;
			dirty = true;
		}
		if ( y != NULL && _dimensions.y != y )
		{
			_dimensions.y = y;
			dirty = true;
		}
		if ( z != NULL && _dimensions.z != z )
		{
			_dimensions.z = z;
			dirty = true;
		}

		if ( dirty == true )
		{
			ToggleDirty( );
		}
	}

	// get the color applied to the top of the cube
	Color DrawableCube3DConfiguration::GetTopColor( ) const
	{
		return _topColor;
	}

	// set the color applied to the top of the cube
	void DrawableCube3DConfiguration::SetTopColor( _In_ const Color color )
	{
		_topColor = color;
		ToggleDirty( );
	}

	// set the color applied to the top of the cube
	void DrawableCube3DConfiguration::SetTopColor( _In_ const float r, _In_ const float g, _In_ const float b, _In_ const float a )
	{
		_topColor.SetColor( r, g, b, a );
		ToggleDirty( );
	}

	// get the color applied to the bottom of the cube
	Color DrawableCube3DConfiguration::GetBottomColor( ) const
	{
		return _bottomColor;
	}

	// set the color applied to the bottom of the cube
	void DrawableCube3DConfiguration::SetBottomColor( _In_ const Color color )
	{
		_bottomColor = color;
		ToggleDirty( );
	}

	// set the color applied to the bottom of the cube
	void DrawableCube3DConfiguration::SetBottomColor( _In_ const float r, _In_ const float g, _In_ const float b, _In_ const float a )
	{
		_bottomColor.SetColor( r, g, b, a );
		ToggleDirty( );
	}
	// --------------------------------------------------------

	// null ctor
	DrawableCube3D::DrawableCube3D( )
		: Drawable( ),
		Primitive( )
	{ }

	// configured ctor
	DrawableCube3D::DrawableCube3D( _In_ DrawableCube3DConfiguration* config )
		: Drawable( ),
		Primitive( config )
	{ }

	// copy ctor
	DrawableCube3D::DrawableCube3D( _In_ const DrawableCube3D& other )
		: Drawable( other ),
		Primitive( other )
	{ }

	// move ctor
	DrawableCube3D::DrawableCube3D( _In_ const DrawableCube3D&& other )
		: Drawable( other ),
		Primitive( other )
	{ }

	// good ol' dtor
	DrawableCube3D::~DrawableCube3D( )
	{ }

	// load the primitive model based off of configuration settings
	void DrawableCube3D::GeneratePrimitiveModel( )
	{
		DrawableCube3DConfiguration* config = static_cast< DrawableCube3DConfiguration* >( _configuration );
		VertexDescription description = ShaderFactory::VertexElementDescription::PositionColorTexture;

		Vector3 size = config->GetDimensions( );
		Vector3 og = _transform.GetOrigin( );

		VertexPositionColorTexture verts[ ] =
		{
			// -- Top --
			{
				Vector3( 0 - (size.x * og.x), size.y - (size.y * og.y), 0 - (size.z * og.z) ),
				*config->GetTopColor().GetMultipliedColor(),
				Vector2( )
			},
			{
				Vector3( 0 - (size.x * og.x), size.y - (size.y * og.y), size.z - (size.z * og.z) ),
				*config->GetTopColor( ).GetMultipliedColor( ),
				Vector2( )
			},
			{
				Vector3( size.x - (size.x * og.x), size.y - (size.y * og.y), size.z - (size.z * og.z) ),
				*config->GetTopColor( ).GetMultipliedColor( ),
				Vector2( )
			},
			{
				Vector3( size.x - (size.x * og.x), size.y - (size.y * og.y), size.z - (size.z * og.z) ),
				*config->GetTopColor( ).GetMultipliedColor( ),
				Vector2( )
			},

			// -- Bottom --
			{
				Vector3( 0 - ( size.x * og.x ), 0 - (size.y * og.y), 0 - (size.z * og.z) ),
				*config->GetBottomColor().GetMultipliedColor( ),
				Vector2( )
			},
			{
				Vector3( 0 - ( size.x * og.x ), 0 - ( size.y * og.y ), size.z - (size.z * og.z) ),
				*config->GetBottomColor( ).GetMultipliedColor( ),
				Vector2( )
			},
			{
				Vector3( size.x - (size.x * og.x), 0 - (size.y * og.y), size.z - (size.z * og.z) ),
				*config->GetBottomColor( ).GetMultipliedColor( ),
				Vector2( )
			},
			{
				Vector3( size.x - (size.x * og.x), 0 - (size.y * og.y), 0 - (size.z * og.z) ),
				*config->GetBottomColor( ).GetMultipliedColor( ),
				Vector2( )
			}
		};

		D3D11_BUFFER_DESC vertDesc;
		ZeroMemory( &vertDesc, sizeof( vertDesc ) );
		vertDesc.Usage = D3D11_USAGE::D3D11_USAGE_DEFAULT;
		vertDesc.ByteWidth = description.Size * 8;
		vertDesc.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_VERTEX_BUFFER;
		vertDesc.CPUAccessFlags = 0;

		D3D11_SUBRESOURCE_DATA vertData;
		ZeroMemory( &vertData, sizeof( vertData ) );
		vertData.pSysMem = verts;

		VertexBuffer* verticies = Renderer::Instance( )
			->GetGraphicsLibrary( )
			->CreateVertexBuffer( &vertDesc, &vertData, description, 8 );

		UInt inds[ ] = 
		{
			// top
			0, 1, 2,
			3, 0, 2,

			// front
			4, 0, 3,
			7, 4, 3,

			// right
			7, 3, 2,
			6, 7, 2,

			// back
			6, 2, 1,
			5, 6, 1,

			// left
			5, 1, 0, 
			4, 5, 0,

			// back
			4, 5, 6,
			7, 4, 6
		};

		D3D11_BUFFER_DESC indDesc;
		ZeroMemory( &indDesc, sizeof( indDesc ) );
		indDesc.Usage = D3D11_USAGE_DEFAULT;
		indDesc.ByteWidth = sizeof( UInt ) * 6;
		indDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		indDesc.CPUAccessFlags = 0;

		D3D11_SUBRESOURCE_DATA indData;
		ZeroMemory( &indData, sizeof( indData ) );
		indData.pSysMem = inds;

		IndexBuffer* indicies = Renderer::Instance( )
			->GetGraphicsLibrary( )
			->CreateIndexBuffer( &indDesc, &indData, 24 );

		Mesh* mesh = anew( Mesh )( verticies, indicies );
		mesh->SetMaterial( anew( Material3D )( ) );

		_model = anew( Model )( );
		_model->AddMesh( mesh );

		_configuration->ToggleClean( );
	}

	// load the cube, ready for rendering
	void DrawableCube3D::LoadModel( _In_ Function< void > callback )
	{
		// TODO: make this async

		GeneratePrimitiveModel( );
		
		callback( );
	}

	// generate collision description
	void DrawableCube3D::GenerateBounding( )
	{
		// TODO
	}

	// per-cycle update call
	void DrawableCube3D::Update( _In_ Context* context, _In_ const RenderTime time )
	{
		if ( _configuration->IsDirty( ) )
		{
			updateSubresourceData( );
			_configuration->ToggleClean( );
		}
	}

	// update the data this primitive has on the GPU
	void DrawableCube3D::updateSubresourceData( )
	{
		// map vertex buffer
		// update vertex buffer
		// un-map vertex buffer
	}

	// when we're in development, this API will help us draw debugging information to the screen
	void DrawableCube3D::RenderDebug( _In_ Context* context )
	{ }

	// get the on-screen transofmration container
	Transform* DrawableCube3D::GetTransform( )
	{
		return &_transform;
	}
}