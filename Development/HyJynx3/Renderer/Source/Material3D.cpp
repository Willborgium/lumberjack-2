/**
 * Material3D.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\Material3D.h>
#include <Renderer\Headers\Renderer.h>
#include <Renderer\Headers\MaterialLibrary.h>
#include <Renderer\Headers\ShaderFactory.h>
#include <Renderer\Headers\LinkageController.h>
#include <Engine\Headers\ResourceController.h>
#include <Engine\Headers\Function.h>
#include <Logger\Headers\Logger.h>

using namespace HyJynxCore;
using namespace HyJynxLogger;
using namespace DirectX;

namespace HyJynxRenderer
{
	// helper local method - create the callback that initializes the linkage controller
	LinkageCallback getLinkageCallback( )
	{
		LinkageCallback callback = [ ] ( _In_ ID3D11ShaderReflection* const reflector, _In_ LinkageController* const controller )
		{
			if ( reflector == nullptr || controller == nullptr )
			{
				return;
			}

			controller->InitializeLinkageArray( reflector->GetNumInterfaceSlots( ) );

			if ( controller->AddAbstractVariable( reflector, "AmbientLighting" ) == false )
			{
				Logger::Log( "Unable to retrieve AmbientLighting abstract." );
			}
			if ( controller->AddAbstractVariable( reflector, "DirectLighting" ) == false )
			{
				Logger::Log( "Unable to retrieve DirectLighting abstract." );
			}
			if ( controller->AddAbstractVariable( reflector, "EnvironmentLighting" ) == false )
			{
				Logger::Log( "Unable to retrieve EnvironmentLighting abstract." );
			}
			if ( controller->AddAbstractVariable( reflector, "Material" ) == false )
			{
				Logger::Log( "Unable to retrieve Material abstract." );
			}
		};

		return callback;
	}

	// null ctor
	Material3D::Material3D( )
		: Material( MaterialLibrary::ShaderLinkage3DLayer )
	{
		ShaderInit initData;

		initData.ExpectedVertexDescription = ShaderFactory::VertexElementDescription::PositionColorTexture;
		initData.VertexPath = ResourceController::GetPath( Shader::VertexShader::Projected );
		initData.PixelPath = ResourceController::GetPath( "PSDriver3D.cso" );
		initData.OnLinkageFound = getLinkageCallback( );

		_shader = ShaderFactory::Instance( )->CreateShader( initData );
	}

	// copy ctor
	Material3D::Material3D( _In_ const Material3D& other )
		: Material( other )
	{ }

	// move ctor
	Material3D::Material3D( _In_ const Material3D&& other )
		: Material( other )
	{ }

	// dtor
	Material3D::~Material3D( )
	{ }

	// update and set buffers to the shader which all following objects will use
	void Material3D::BatchBind( _In_ Context* const context )
	{
		_shader->Bind( context );

		// TODO: make the API to set all of these at once

		/*

		ID3D11Buffer* buffers = {
			ShaderFactory::Instance( )->GetProjectionConstant( ),
			ShaderFactory::Instance( )->GetCameraConstant( ),
			ShaderFactory::Instance( )->GetCycleConstant( ),
			ShaderFactory::Instance( )->GetObjectConstant( )
		};
		context->SetVertexConstantBuffer( 0, buffers, 4 );

		*/

		context->SetVertexConstantBuffer( static_cast<UInt>( ConstantBufferIndicies::CONSTANT_BUFFER_CAMERA_PROJECTION ), ShaderFactory::Instance( )->GetProjectionConstant( ) );
		context->SetVertexConstantBuffer( static_cast<UInt>( ConstantBufferIndicies::CONSTANT_BUFFER_CAMERA ), ShaderFactory::Instance( )->GetCameraConstant( ) );
		context->SetVertexConstantBuffer( static_cast<UInt>( ConstantBufferIndicies::CONSTANT_BUFFER_CYCLE ), ShaderFactory::Instance( )->GetCycleConstant( ) );
		context->SetVertexConstantBuffer( static_cast<UInt>( ConstantBufferIndicies::CONSTANT_BUFFER_OBJECT ), ShaderFactory::Instance( )->GetObjectConstant( ) );

		context->SetRasterizer( Renderer::RasterizerState::FrontFaceSolid );
	}

	// bind the argued drawable containers to the argued constant buffer
	void Material3D::ObjectBind( _In_ IDrawable* const drawable, 
		_In_ Mesh* const mesh, 
		_In_ CBObject* const objConstant )
	{
		// TODO: update class linkage array (althought this happens during shader bind)
		XMMATRIX mat = XMLoadFloat4x4( &drawable->GetTransform( )->GetMatrix( ) );
		XMStoreFloat4x4( &objConstant->WorldMatrix, XMMatrixTranspose( mat ) );
	}
}