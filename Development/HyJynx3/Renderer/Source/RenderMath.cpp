/**
 * RenderMath.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\RenderMath.h>

using namespace DirectX;

namespace HyJynxRenderer
{
	// distance between two transform objects
	float RenderMath::Distance( _In_ const Transform& one, _In_ const Transform& two )
	{
		float dx = one.GetPosition( ).x - two.GetPosition( ).x,
			dy = one.GetPosition( ).y - two.GetPosition( ).y,
			dz = one.GetPosition( ).z - two.GetPosition( ).z;

		return Abs( ( dx * dx ) + ( dy * dy ) + ( dz * dz ) );
	}

	// distance between two XMFLOAT positions
	float RenderMath::Distance( _In_ const XMFLOAT3& pos1, _In_ const XMFLOAT3& pos2 )
	{
		float dx = pos1.x - pos2.x,
			dy = pos1.y - pos2.y,
			dz = pos1.z - pos2.z;
		
		return sqrt( ( dx * dx ) + ( dy * dy ) + ( dz * dz ) );
	}

	// radians to degrees
	float RenderMath::ToDegrees( _In_ const float rads )
	{
		return rads * ( 180.0f / XM_PI );
	}

	// degrees to radians
	float RenderMath::ToRadians( _In_ const float degs )
	{
		return degs * ( XM_PI / 180.0f );
	}

}