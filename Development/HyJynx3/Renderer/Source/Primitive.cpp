/**
 * Primitive.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\Primitive.h>

namespace HyJynxRenderer
{
	// null ctor
	Primitive::Primitive( )
		: _configuration( nullptr )
	{ }

	// default ctor
	Primitive::Primitive( _In_ PrimitiveConfiguration* config )
		: _configuration( config )
	{ }

	// copy ctor
	Primitive::Primitive( _In_ const Primitive& )
	{ }

	// dtor
	Primitive::~Primitive( )
	{ }

	// retreive configuration pointer
	PrimitiveConfiguration* Primitive::GetPrimitiveConfiguration( ) const
	{
		return _configuration;
	}
}