/**
 * IRenderSequence.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\IRenderSequence.h>

using namespace HyJynxCore;
using namespace HyJynxCollections;
using namespace HyJynxLogger;

namespace HyJynxRenderer
{
	// null ctor
	IRenderSequence::IRenderSequence( )
		: ILog( "no-name" ),
		_name( "no-name" )
	{ }

	// default ctor
	IRenderSequence::IRenderSequence( _In_ Text txt )
		: ILog( txt ),
		_name( txt )
	{ }

	// name ID return
	Text IRenderSequence::GetName( ) const
	{
		return _name;
	}
}