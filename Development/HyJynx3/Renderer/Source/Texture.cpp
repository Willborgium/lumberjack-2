/**
 * Texture.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\Texture.h>
#include <Renderer\Headers\TextureManager.h>

using namespace HyJynxLogger;

namespace HyJynxRenderer
{
	// null ctor
	Texture::Texture()
		: ILog( "Texture" )
	{ }

	// default ctor
	Texture::Texture( _In_ LoadedTexture* tex )
		: ILog( "Texture" ),
		_texture( tex )
	{ }

	// copy ctor
	Texture::Texture( _In_ const Texture& other )
		: ILog( other),
		_texture( other._texture )
	{ }

	// move ctor
	Texture::Texture( _In_ const Texture&& other )
		: ILog( other ),
		_texture( other._texture )
	{ }

	// dtor
	Texture::~Texture( )
	{
		_texture = nullptr;
	}

	// get the resource view used in shader binding
	// - returns ID3D11ShaderResrouceView*: dx11 resource view
	//
	ID3D11ShaderResourceView* Texture::GetResourceView( )
	{
		ID3D11ShaderResourceView* output = nullptr;

		if ( _texture != nullptr )
		{
			output = _texture->ResourceView;
		}

		return output;
	}

	// get the texture resource used in shader mapping
	ID3D11Resource* Texture::GetTexture( )
	{
		ID3D11Resource* output = nullptr;

		if ( _texture != nullptr )
		{
			output = _texture->Texture;
		}

		return output;
	}

	// local helper function to set all output pixels to white, due to an error
	void setToError( _Out_ Color** output, _In_ UInt count )
	{
		for ( UInt index = 0; index < count; index++ )
		{
			*output[ index ] = Color( );
		}
	}

	// get an array of pixels at the given indicies
	Color* Texture::GetPixel( _In_opt_ Context* context, _In_ Point3 indicies[ ] )
	{
		if ( indicies == nullptr )
		{
			return nullptr;
		}

		UInt count = COUNT( indicies );

		if ( count == 0 )
		{
			return nullptr;
		}

		Color* output = anewa( Color, count );

		if ( _texture->CpuMemory != nullptr )
		{
			ReadFromCpuData( _texture, &output, indicies, count );
		}
		else if ( _texture->Texture != nullptr && context != nullptr )
		{
			ReadFromGpuData( context, _texture, &output, indicies, count );
		}
		else
		{
			LogError( "GetPixel() - Error! No valid data to read pixels from!" );
			setToError( &output, count );
		}

		return output;
	}
}