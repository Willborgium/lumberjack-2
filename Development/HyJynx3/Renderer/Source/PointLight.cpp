/**
 * PointLight.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\PointLight.h>

namespace HyJynxRenderer
{

#pragma region init

	// null ctor
	PointLight::PointLight( )
		: ILight( )
	{ }

	// copy ctor
	PointLight::PointLight( _In_ const PointLight& other )
		: ILight( other )
	{ }

	// move ctor
	PointLight::PointLight( _In_ const PointLight&& other )
		: ILight( other )
	{ }

	// dtor
	PointLight::~PointLight( )
	{ }

#pragma endregion

#pragma region ILight

	// Get the main color of the light source
	const Color& PointLight::GetDiffuseColor( ) const
	{
		return _diffuseColor;
	}

	// Set the main color of the light source
	void PointLight::SetDiffuseColor( _In_ const Color& diffuse )
	{
		_diffuseColor = diffuse;
	}

	// Get the general intensity of the light
	const float PointLight::GetIntensity( ) const
	{
		return _intensity;
	}

	// Get the general intensity of the light
	void PointLight::SetIntensity( _In_ const float val )
	{
		_intensity = val;
	}

	// Get the Dimensions of the light source, used for Solf-Light calculations
	const Vector3& PointLight::GetDimensions( ) const
	{
		return _dimensions;
	}

	// Set the dimensions of the light source, used for Solf-Light calculations
	void PointLight::SetDimensions( _In_ const Vector3& dim )
	{
		_dimensions = dim;
	}

	// Get the general direction of the light
	const Vector3& PointLight::GetNormal( ) const
	{
		return Vector3::Zero( );
	}

	// Set the general direction of the light
	void PointLight::SetNormal( _In_ const Vector3& )
	{
		// do nothing
	}

	// Get the positional Transform of the light
	Transform* PointLight::GetTransform( )
	{
		return &_transform;
	}

	// Get the total distance this light can cover
	const float PointLight::GetDistance( ) const
	{
		return _distance;
	}

	// Set the total distance this light can cover
	void PointLight::SetDistance( _In_ const float val )
	{
		_distance = val;
	}

	// Get where the light begins to falloff within it's distance (0-1 scale)
	const float PointLight::GetFalloff( ) const
	{
		return _falloff;
	}

	// Set where the light begins to falloff within it's distance (0-1 scale)
	void PointLight::SetFalloff( _In_ const float val )
	{
		_falloff = val;
	}

	// Determine if this light casts shadows on any receiving drawables
	bool PointLight::CastShadows( )
	{
		return _castShadows;
	}

	// Set this light to cast shadows on any receiving drawables
	void PointLight::SetCastShadows( _In_ const bool val )
	{
		_castShadows = val;
	}

	// Get the field of view of each axis 
	const Vector3& PointLight::GetFieldOfView( ) const
	{
		return _fov;
	}

	// Set the field of view of each local axis
	void PointLight::SetFieldOfView( _In_ const Vector3 val )
	{
		_fov = val;
	}

#pragma endregion ILight
}