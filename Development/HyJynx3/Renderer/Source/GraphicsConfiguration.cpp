/**
 * GraphicsConfiguration.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\GraphicsConfiguration.h>

using namespace HyJynxUtilities;
using namespace HyJynxCore;

namespace HyJynxRenderer
{
	// null ctor
	GraphicsConfiguration::GraphicsConfiguration( )
		: Dirty( false )
	{ }

	// copy ctory
	GraphicsConfiguration::GraphicsConfiguration( const GraphicsConfiguration& other )
		: Dirty( true ),
		_directXDebug( other._directXDebug ),
		_width( other._width ),
		_height( other._height ),
		_driverType( other._driverType ),
		_refreshRate( other._refreshRate ),
		_bufferCount( other._bufferCount ),
		_msaa( other._msaa ),
		_fullscreen( other._fullscreen )
	{ }

	// move ctor
	GraphicsConfiguration::GraphicsConfiguration( const GraphicsConfiguration&& other )
		: Dirty( true ),
		_directXDebug( other._directXDebug ),
		_width( other._width ),
		_height( other._height ),
		_driverType( other._driverType ),
		_refreshRate( other._refreshRate ),
		_bufferCount( other._bufferCount ),
		_msaa( other._msaa ),
		_fullscreen( other._fullscreen )
	{ }

	// dtor
	GraphicsConfiguration::~GraphicsConfiguration( )
	{ }

	// assignment operator
	GraphicsConfiguration GraphicsConfiguration::operator = ( _In_ const GraphicsConfiguration& other )
	{
		_directXDebug = other._directXDebug;
		_width = other._width;
		_height = other._height;
		_driverType = other._driverType;
		_refreshRate = other._refreshRate;
		_bufferCount = other._bufferCount;
		_msaa = other._msaa;
		_fullscreen = other._fullscreen;

		return *this;
	}

	// get dx debug create flag yay/nay
	bool GraphicsConfiguration::GetDirectXDebug( ) const
	{
		return _directXDebug;
	}

	// get screen width
	UInt GraphicsConfiguration::GetWidth( ) const
	{
		return _width;
	}

	// get screen height
	UInt GraphicsConfiguration::GetHeight( ) const
	{
		return _height;
	}

	// set dx debug more
	void GraphicsConfiguration::SetDirectXDebug( _In_ bool debug )
	{
		_directXDebug = debug;
		ToggleDirty( );
	}

	// set screen width
	void GraphicsConfiguration::SetWidth( _In_ UInt w )
	{
		_width = w;
		ToggleDirty( );
	}

	// set height
	void GraphicsConfiguration::SetHeight( _In_ UInt h )
	{
		_height = h;
		ToggleDirty( );
	}

	// return fullscreen
	bool GraphicsConfiguration::GetFullscreen( ) const
	{
		return _fullscreen;
	}

	// set fullscreen mode
	void GraphicsConfiguration::SetFullscreen( _In_ bool jimmy )
	{
		_fullscreen = jimmy; // I'm bored. so what.
		ToggleDirty( );
	}

	// get refresh rate
	UInt GraphicsConfiguration::GetRefreshRate( ) const
	{
		return _refreshRate;
	}

	// set refresh rate
	void GraphicsConfiguration::SetRefreshRate( _In_ UInt rate )
	{
		_refreshRate = rate;
		ToggleDirty( );
	}

	// retrieve the current msaa quality
	MsaaQuality GraphicsConfiguration::GetMSAAQuality( ) const
	{
		return _msaa;
	}

	// set the msaa quality
	void GraphicsConfiguration::SetMSAAQuality( _In_ MsaaQuality msaa )
	{
		_msaa = msaa;
		ToggleDirty( );
	}

	// get how many back-buffers we'll use
	UInt GraphicsConfiguration::GetBufferCount( ) const
	{
		return _bufferCount;
	}

	// set how many back-buffers renderer will use
	void GraphicsConfiguration::SetBufferCount( _In_ UInt count )
	{
		_bufferCount = count;
		ToggleDirty( );
	}

	// get the current driver type
	D3D_DRIVER_TYPE GraphicsConfiguration::GetDriverType( ) const
	{
		return _driverType;
	}

	// set the driver type
	void GraphicsConfiguration::SetDriverType( _In_ D3D_DRIVER_TYPE driver )
	{
		_driverType = driver;
		ToggleDirty( );
	}
}