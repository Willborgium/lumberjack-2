/**
 * Texture1D.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\Texture1D.h>
#include <Renderer\Headers\TextureManager.h>

namespace HyJynxRenderer
{
	// null ctor
	Texture1D::Texture1D( )
		: Texture( )
	{ }

	// default ctor
	Texture1D::Texture1D( _In_ LoadedTexture* tex )
		: Texture( tex )
	{ }

	// copy ctor
	Texture1D::Texture1D( _In_ const Texture1D& )
	{ }

	// move ctor
	Texture1D::Texture1D( _In_ const Texture1D&& )
	{ }

	// dtor
	Texture1D::~Texture1D( )
	{ }

	// equality operator
	bool Texture1D::operator == ( _In_ const Texture1D& other ) const
	{
		return _texture == other._texture;
	}

	// inequality operator
	bool Texture1D::operator != ( _In_ const Texture1D& other ) const
	{
		return !( *this == other );
	}

	// local helper function to read from cpu data
	void Texture1D::ReadFromCpuData( _In_ LoadedTexture* texture, _Out_ Color** output, _In_ Point3 indicies[ ], _In_ UInt count )
	{
		for ( UInt index = 0; index < count; index++ )
		{
			if ( indicies[ index ].X >= 0 && indicies[ index ].X < static_cast< int > ( texture->LoadedWidth ) )
			{
				*output[ index ] = Color( ); // TODO: cast cpu data at the given index
			}
			else
			{
				*output[ index ] = Color( );
			}
		}
	}

	// local helper function to read from gpu data
	void Texture1D::ReadFromGpuData( _In_ Context* context, _In_ LoadedTexture* texture, _Out_ Color** output, _In_ Point3 indicies[ ], _In_ UInt count )
	{
		// TODO: map gpu data, and cast/create colors
	}
}