/**
 * BatchContainer.h
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\BatchContainer.h>
#include <Renderer\Headers\RenderMath.h>

using namespace HyJynxCollections;
using namespace DirectX;

namespace HyJynxRenderer
{
#pragma region Wrappers

	// null ctor
	BatchItem::BatchItem( )
	{ }

	// default used ctor
	BatchItem::BatchItem( _In_ IDrawable* drawable, _In_ Mesh* mesh )
		: Drawable( drawable ),
		DrawableMesh( mesh )
	{ }

	// dtor
	BatchItem::~BatchItem( )
	{
		Drawable = nullptr;
		DrawableMesh = nullptr;
	}

	// comparison operator
	bool BatchItem::operator == ( _In_ const BatchItem& other ) const
	{
		return ( Drawable == other.Drawable && DrawableMesh == other.DrawableMesh );
	}

	// --------------------------------------------------------

	// null ctor
	MaterialBatch::MaterialBatch( )
	{ }

	// default used ctor
	MaterialBatch::MaterialBatch( _In_ int id )
		: MaterialID( id )
	{ }

	// comparison operator needed for dynamicCollection
	bool MaterialBatch::operator == ( _In_ const MaterialBatch& other ) const
	{
		return MaterialID == other.MaterialID;
	}

	// safely retrieve the material representing this batch
	Material* const MaterialBatch::GetMaterial( )
	{
		Material* output = nullptr;

		if ( MeshList.Count( ) > 0 )
		{
			output = MeshList[ 0 ]->DrawableMesh->GetMaterial( );
		}

		return output;
	}

#pragma endregion

	// null ctor
	BatchContainer::BatchContainer( )
	{ }

	// copy ctor
	BatchContainer::BatchContainer( _In_ const BatchContainer& )
	{ }

	// move ctor
	BatchContainer::BatchContainer( _In_ const BatchContainer&& )
	{ }

	// dtor
	BatchContainer::~BatchContainer( )
	{
		_2DLayer.Clear( );
		_3DLayer.Clear( );
	}

	// comparison operator
	bool BatchContainer::operator == ( _In_ const BatchContainer& batch ) const
	{
		return true;
	}

	// 2D renderables retrieval
	DynamicCollection< MaterialBatch* >* BatchContainer::Get2DLayer( )
	{
		return &_2DLayer;
	}

	// 3D renderables retrieval
	DynamicCollection< MaterialBatch* >* BatchContainer::Get3DLayer( )
	{
		return &_3DLayer;
	}

	// register a drawable for rendering
	void BatchContainer::RegisterDrawable( _In_ WindowDrawType type, _In_ IDrawable* drawable, _In_ const XMFLOAT3& camPosition )
	{
		DynamicCollection< Mesh* > meshList;

		if ( drawable->GetModel( ) != nullptr )
		{
			drawable->GetModel( )->GetMeshAtLOD( &meshList, RenderMath::Distance( camPosition, drawable->GetTransform()->GetPosition() ));
		}

		meshList.ForEach( [this, type, &drawable] ( _In_ Mesh* mesh )
		{
			if ( mesh->GetMaterial( ) != nullptr )
			{
				switch ( type )
				{
				case WindowDrawType::WINDOW_DRAW_2D:
					AddMesh2D( mesh, drawable );
					break;

				case WindowDrawType::WINDOW_DRAW_3D:
					AddMesh3D( mesh, drawable );
					break;
				}
			}
		} );
	}

	// add or create a material batch for 2D
	void BatchContainer::AddMesh2D( _In_ Mesh* mesh, _In_ IDrawable* drawable )
	{
		if ( mesh != nullptr )
		{
			if ( mesh->GetMaterial( )->IsValid( ) == false )
			{
				return;
			}

			bool found = false;
			int matId = mesh->GetMaterial( )->GetID( );

			for ( UInt index = 0; index < _2DLayer.Count( ); index++ )
			{
				if ( _2DLayer[ index ]->MaterialID == matId )
				{
					found = true;
					_2DLayer[ index ]->MeshList.Append( anew( BatchItem )( drawable, mesh ) );
					break;
				}
			}

			if ( found == false )
			{
				MaterialBatch* batch = anew( MaterialBatch )( matId );
				batch->MeshList.Append( anew( BatchItem )( drawable, mesh ) );
				_2DLayer.Append( batch );
			}
		}
	}

	// add or create a material batch for 3D
	void BatchContainer::AddMesh3D( _In_ Mesh* mesh, _In_ IDrawable* drawable )
	{
		if ( mesh != nullptr )
		{
			if ( mesh->GetMaterial( )->IsValid( ) == false )
			{
				return;
			}

			bool found = false;
			int matId = mesh->GetMaterial( )->GetID( );

			for ( UInt index = 0; index < _3DLayer.Count( ); index++ )
			{
				if ( _3DLayer[ index ]->MaterialID == matId )
				{
					_3DLayer[ index ]->MeshList.Append( anew( BatchItem )( drawable, mesh ) );
					found = true;
					break;
				}
			}

			if ( found == false )
			{
				MaterialBatch* batch = anew( MaterialBatch )( matId );
				batch->MeshList.Append( anew( BatchItem )( drawable, mesh ) );
				_3DLayer.Append( batch );
			}
		}
	}

	// local helper method to run the typical batch routine
	void renderLayer( _In_ Context* const context, _In_ DynamicCollection< MaterialBatch* >* batch )
	{
		Material* batchMaterial = nullptr;
		CBObject* objBound = nullptr;

		for ( UInt index = 0; index < batch->Count( ); index++ )
		{
			batchMaterial = batch->ElementAt( index )->GetMaterial( );

			if ( batchMaterial == nullptr )
			{
				continue;
			}

			batchMaterial->BatchBind( context );

			batch->ElementAt( index )->MeshList.ForEach( [&batchMaterial, &context, &objBound] ( _In_ BatchItem* batchItem )
			{
				objBound = context->GetObjectConstantBuffer( );

				batchMaterial->ObjectBind( batchItem->Drawable, batchItem->DrawableMesh, objBound );

				context->UpdateObjectConstantBuffer( );

				objBound = nullptr;

				batchItem->DrawableMesh->Render( batchItem->Drawable, context );
			} );

			batchMaterial = nullptr;
		}
	}

	// Render the 2D Layer batches as it stands to the context
	void BatchContainer::Render2DLayer( _In_ Context* const context )
	{
		if ( _2DLayer.Count( ) > 0 )
		{
			renderLayer( context, &_2DLayer );
		}
	}

	//
	// Render the 3D Layer batches as it stands to the context
	void BatchContainer::Render3DLayer( _In_ Context* const context )
	{
		if ( _3DLayer.Count( ) > 0 )
		{
			renderLayer( context, &_3DLayer );
		}
	}
}