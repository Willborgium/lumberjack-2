/**
 * IndexBuffer.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\IndexBuffer.h>

namespace HyJynxRenderer
{
	// null ctor
	IndexBuffer::IndexBuffer()
	{ }

	// defined ctor
	IndexBuffer::IndexBuffer( _In_ ID3D11Buffer* indicies, _In_ const UInt count )
		: _buffer( indicies ),
		_count( count )
	{ }

	// copy ctor
	IndexBuffer::IndexBuffer( _In_ const IndexBuffer& other )
		: _buffer( other._buffer ),
		_count( other._count )
	{ }

	// move ctor
	IndexBuffer::IndexBuffer( _In_ const IndexBuffer&& other )
		: _buffer( other._buffer ),
		_count( other._count )
	{ }

	// dtor
	IndexBuffer::~IndexBuffer()
	{
		_buffer = nullptr;
		_count = 0;
	}

	// buffer return
	ID3D11Buffer* IndexBuffer::GetBuffer()
	{
		return _buffer;
	}

	// number of indicies return
	UInt IndexBuffer::GetCount( ) const
	{
		return _count;
	}
}