/**
 * Ray.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\Ray.h>

using namespace DirectX;

namespace HyJynxRenderer
{
	// null ctor
	Ray::Ray( )
		:_maxDistance( std::numeric_limits< float >::max( ) )
	{ }

	// defined ctor
	Ray::Ray( _In_ Vector3 origin, _In_ Vector3 vector )
		:_origin( origin ),
		_vector( vector ),
		_maxDistance( std::numeric_limits< float >::max( ) )
	{ }

	// fully defined ctor
	Ray::Ray( _In_ Vector3 origin, _In_ Vector3 vector, _In_ float distance )
		: _origin( origin ),
		_vector( vector ),
		_maxDistance( distance > 0 ? distance : distance * -1 )
	{ }

	// copy ctor
	Ray::Ray( _In_ const Ray& other )
		: _origin( other._origin ),
		_vector( other._vector ),
		_maxDistance( other._maxDistance )
	{ }

	// move ctor
	Ray::Ray( _In_ const Ray&& other )
		: _origin( other._origin ),
		_vector( other._vector ),
		_maxDistance( other._maxDistance )
	{ }

	// dtor
	Ray::~Ray( )
	{ }

	// normalize vector
	void Ray::Normalize( )
	{
		Normalize( this );
	}

	// static normalize method
	void Ray::Normalize( _In_ Ray* ray )
	{
		if ( ray == nullptr )
		{
			return;
		}

		XMFLOAT3* vector = ray->GetVector( );

		float length = std::sqrt( ( vector->x * vector->x ) + ( vector->y * vector->y ) + ( vector->z * vector->z ) );

		if ( length != 0.0f )
		{
			vector->x /= length;
			vector->y /= length;
			vector->z /= length;
		}
	}

	// get the origin of the ray
	Vector3* Ray::GetOrigin( )
	{
		return &_origin;
	}

	// get the direction of the ray
	Vector3* Ray::GetVector( )
	{
		return &_vector;
	}

	// returns the maximum distance
	float Ray::GetDistance( ) const
	{
		return _maxDistance;
	}

	// set the maximum distance
	void Ray::SetDistance( _In_ const float val )
	{
		_maxDistance = val > 0 ? val : val * -1;
	}
}