/**
 * Shader.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\Shader.h>
#include <Renderer\Headers\Context.h>

using namespace HyJynxCore;
using namespace HyJynxLogger;

namespace HyJynxRenderer
{
	// static member initialization
	const Text Shader::NoShaderPath = "NO_SHADER_SET";
	const Text Shader::VertexShader::UnProjected = "VSUnProjected.cso";
	const Text Shader::VertexShader::Projected = "VSProjected.cso";
	const Text Shader::VertexShader::SkeletonUnProjected = "VSSkeletonUnProjected.cso";
	const Text Shader::VertexShader::SkeletonProjected = "VSSkeletonProjected.cso";

	// null ctor
	Shader::Shader( )
		: ILog( "Shader" )
	{ }

	// copy ctor
	Shader::Shader( _In_ const Shader& other )
		: ILog( other ),
		_vertexPath( other._vertexPath ),
		_vertexShader( other._vertexShader ),
		_pixelPath( other._pixelPath ),
		_pixelShader( other._pixelShader ),
		_hullPath( other._hullPath ),
		_hullShader( other._hullShader ),
		_domainPath( other._domainPath ),
		_domainShader( other._domainShader ),
		_geometryPath( other._geometryPath ),
		_geometryShader( other._geometryShader ),
		_inputLayout( other._inputLayout )
	{ }

	// move ctor
	Shader::Shader( _In_ const Shader&& other )
		: ILog( other ),
		_vertexPath( other._vertexPath ),
		_vertexShader( other._vertexShader ),
		_pixelPath( other._pixelPath ),
		_pixelShader( other._pixelShader ),
		_hullPath( other._hullPath ),
		_hullShader( other._hullShader ),
		_domainPath( other._domainPath ),
		_domainShader( other._domainShader ),
		_geometryPath( other._geometryPath ),
		_geometryShader( other._geometryShader ),
		_inputLayout( other._inputLayout )
	{ }

	// dtor
	Shader::~Shader( )
	{
		Release( );
	}

	// remove references
	void Shader::Release( )
	{
		_vertexPath = _pixelPath = _hullPath = _domainPath = _geometryPath = Shader::NoShaderPath;
		_vertexShader = nullptr;
		_pixelShader = nullptr;
		_hullShader = nullptr;
		_domainShader = nullptr;
		_geometryShader = nullptr;
		_inputLayout = nullptr;
	}

	// assignment operator
	Shader& Shader::operator = ( _In_ const Shader& other )
	{
		_vertexPath = other._vertexPath;
		_vertexShader = other._vertexShader;

		_pixelPath = other._pixelPath;
		_pixelShader = other._pixelShader;

		_hullPath = other._hullPath;
		_hullShader = other._hullShader;

		_domainPath = other._domainPath;
		_domainShader = other._domainShader;

		_geometryPath = other._geometryPath;
		_geometryShader = other._geometryShader;

		_inputLayout = other._inputLayout;

		return *this;
	}

	// comparison operator
	bool Shader::operator == ( _In_ const Shader& other )
	{
		if ( _vertexPath != other._vertexPath
			|| _pixelPath != other._pixelPath
			|| _hullPath != other._hullPath
			|| _domainPath != other._domainPath
			|| _geometryPath != other._geometryPath )
		{
			return false;
		}

		return true;
	}

	// set this as the active shader
	void Shader::Bind( _In_ Context* const context )
	{
		if ( _vertexShader != nullptr )
		{
			context->BindVertexShader( _vertexShader );
		}

		if ( _pixelShader != nullptr )
		{
			if ( _linkageController != nullptr )
			{
				context->BindPixelShader( _pixelShader, 
					_linkageController->GetClassLinkageArray( ),  // TODO: this needs to change per-object basis
					_linkageController->GetInterfaceCount( ) );
			}
			else
			{
				context->BindPixelShader( _pixelShader );
			}
		}

		if ( _hullShader != nullptr )
		{
			context->BindHullShader( _hullShader );
		}

		if ( _domainShader != nullptr )
		{
			context->BindDomainShader( _domainShader );
		}

		if ( _geometryShader != nullptr )
		{
			context->BindGeometryShader( _geometryShader );
		}
	}

	// retrieve the dx11 input layout
	ID3D11InputLayout* const Shader::GetInputLayout( )
	{
		return _inputLayout;
	}
}