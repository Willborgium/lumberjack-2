/**
 * StationaryCameraBehavior.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\StationaryCameraBehavior.h>

namespace HyJynxRenderer
{
	// null ctor
	StationaryCameraBehavior::StationaryCameraBehavior()
	{ }

	// copy ctor
	StationaryCameraBehavior::StationaryCameraBehavior( _In_ const StationaryCameraBehavior& other )
	{ }

	// move ctor
	StationaryCameraBehavior::StationaryCameraBehavior( _In_ const StationaryCameraBehavior&& other )
	{ }

	// dtor
	StationaryCameraBehavior::~StationaryCameraBehavior()
	{ }

	// update call - we're stationary so we don't do much.
	void StationaryCameraBehavior::Update()
	{ }
}