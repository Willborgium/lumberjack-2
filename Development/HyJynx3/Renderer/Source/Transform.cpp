/**
 * Transform.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\Transform.h>

using namespace std;
using namespace DirectX;
using namespace HyJynxCollections;
using namespace HyJynxCore;

namespace HyJynxRenderer
{
	// null ctor
	Transform::Transform( )
		:Dirty( true ),
		ILog( "Transform" )
	{
		_matrix = Matrix::Identity( );
	}

	// copy ctor
	Transform::Transform( _In_ const Transform& other )
		:Dirty( true ),
		ILog( "Transform" ),
		_scale( other._scale ),
		_rotation( other._rotation ),
		_origin( other._origin ),
		_position( other._position )
	{ }

	// move ctor
	Transform::Transform( _In_ const Transform&& other )
		:Dirty( true ),
		ILog( "Transform" ),
		_scale( other._scale ),
		_rotation( other._rotation ),
		_origin( other._origin ),
		_position( other._position )
	{ }

	// dtor
	Transform::~Transform( )
	{
		_onChange.Clear( );
		_matrixOverride.ClearCapture( );
	}

	// equals operator, due to move ctor
	Transform& Transform::operator = ( _In_ const Transform& other )
	{
		SetDirty( true );
		_scale = other._scale;
		_rotation = other._rotation;
		_origin = other._origin;
		_position = other._position;

		return *this;
	}

	// comparison operator
	bool Transform::operator == ( _In_ const Transform& other )
	{
		return _position == other._position
			&& _scale == other._scale
			&& _rotation == other._rotation
			&& _origin == other._origin;
	}

	// calculate matrix
	void Transform::CalculateMatrix( )
	{
		if ( _matrixOverride.HasValue() == true )
		{
			_matrixOverride( *this, &_matrix );
		}
		else
		{
			XMMATRIX output = XMMatrixIdentity( );
			XMMATRIX multTemp = XMMatrixScaling( _scale.x, _scale.y, _scale.z );
			
			output = XMMatrixMultiply( output, multTemp );

			XMVECTOR rotation = XMLoadFloat3( &_rotation );
			rotation = XMQuaternionRotationRollPitchYawFromVector( rotation );

			output = XMMatrixRotationQuaternion( rotation );

			multTemp = XMMatrixTranslation( _position.x, _position.y, _position.z );
			output = XMMatrixMultiply( output, multTemp );

			if ( _parent != nullptr )
			{
				multTemp = XMLoadFloat4x4( &_parent->GetMatrix( ) );
				output = XMMatrixMultiply( output, multTemp );
			}

			XMStoreFloat4x4( &_matrix, output );
		}

		ToggleClean( );
	}

	// return on-change delegate
	Delegate< Transform& >* Transform::GetOnChangeDelegate( )
	{
		return &_onChange;
	}

	// reset to initial values
	void Transform::Reset( )
	{
		_position.x = _position.y = _position.z = 0.0f;
		_rotation.x = _rotation.y = _rotation.z = 0.0f;
		_origin.x = _origin.y = _origin.z = 0.0f;
		_scale.x = _scale.y = _scale.z = 1.0f;
		
		ToggleDirty( );
	}

	// are events active?
	bool Transform::EventsActive( ) const
	{
		return _eventsActive;
	}

	// set events active
	void Transform::SetEventsActive( _In_ const bool val )
	{
		_eventsActive = val;
	}

	// calculate and return the matrix we represent
	Matrix& Transform::GetMatrix( )
	{
		if ( IsDirty( ) )
		{
			CalculateMatrix( );
		}

		return _matrix;
	}

	// get the position component
	const Vector3& Transform::GetPosition( ) const
	{
		return _position;
	}

	// get the rotation component
	const Vector3& Transform::GetRotation( ) const
	{
		return _rotation;
	}

	// get the quaternion orientation component
	const Vector4& Transform::GetOrientation( ) const
	{
		return _rotationQuat;
	}

	// get the scaling component
	const Vector3& Transform::GetScale( ) const
	{
		return _scale;
	}

	// get the origin component
	const Vector3& Transform::GetOrigin( ) const
	{
		return _origin;
	}

	// set the matrix calculation override
	//void Transform::SetMatrixCalculationOverride( _In_opt_ function<void( _In_ Transform&, _Out_ Matrix* )> callback )
	//{
	//	_calculationOverride = callback;
	//}

	// set the matrix calculation override
	void Transform::SetMatrixCalculationOverride( _In_opt_ Function<void, Transform&, Matrix*> callback )
	{
		_matrixOverride = callback;
	}

	// set position values
	void Transform::SetPosition( _In_ const float x, _In_ const float y, _In_ const float z )
	{
		bool dirty = false;

		if ( x != _position.x )
		{
			dirty = true;
			_position.x = x;
		}
		if ( y != _position.y )
		{
			dirty = true;
			_position.y = y;
		}
		if ( z != _position.z )
		{
			dirty = true;
			_position.z = z;
		}

		if ( dirty == true )
		{
			ToggleDirty( );
		}
	}

	// add relative position
	void Transform::AddPosition( _In_ const float x, _In_ const float y, _In_ const float z )
	{
		if ( x != 0 )
		{
			_position.x += x;
		}
		if ( y != 0 )
		{
			_position.y += y;
		}
		if ( z != 0 )
		{
			_position.z += z;
		}
		if ( x != 0 || y != 0 || z != 0 )
		{
			ToggleDirty( );
		}

	}

	// set position values
	void Transform::SetPosition( _In_ const Vector3& pos )
	{
		_position = pos;
		ToggleDirty( );
	}

	// set all position values
	void Transform::SetRotation( _In_ const float x, _In_ const float y, _In_ const float z )
	{
		bool dirty = false;

		if ( x != _rotation.x )
		{
			dirty = true;
			_rotation.x = x;
		}
		if ( y != _rotation.y )
		{
			dirty = true;
			_rotation.y = y;
		}
		if ( z != _rotation.z )
		{
			dirty = true;
			_rotation.z = z;
		}

		if ( dirty == true )
		{
			ToggleDirty( );
		}
	}

	// set all rotation values
	void Transform::SetRotation( _In_ const Vector3& rot )
	{
		_rotation = rot;
		ToggleDirty( );
	}

	// add rotation to components
	void Transform::AddRotation( _In_ const float x, _In_ const float y, _In_ const float z )
	{
		_rotation.x += x;
		_rotation.y += y;
		_rotation.z += z;
		
		if ( x != 0.0f || y != 0.0f || z != 0.0f )
		{
			ToggleDirty( );
		}
	}

	// set all scaling values
	void Transform::SetScale( _In_ const float x, _In_ const float y, _In_ const float z )
	{
		bool dirty = false;

		if ( x != _scale.x )
		{
			dirty = true;
			_scale.x = x;
		}
		if ( y != _scale.y )
		{
			dirty = true;
			_scale.y = y;
		}
		if ( z != _scale.z )
		{
			dirty = true;
			_scale.z = z;
		}

		if ( dirty == true )
		{
			ToggleDirty( );
		}
	}

	// set all scale values
	void Transform::SetScale( _In_ const Vector3& scale )
	{
		_scale = scale;
		ToggleDirty( );
	}

	// set the point considered origin
	void Transform::SetOrigin( _In_ const float x, _In_ const float y, _In_ const float z )
	{
		bool dirty = false;

		if ( x != _origin.x )
		{
			dirty = true;
			_origin.x = x;
		}
		if ( y != _origin.y )
		{
			dirty = true;
			_origin.y = y;
		}
		if ( z != _origin.z )
		{
			dirty = true;
			_origin.z = z;
		}

		if ( dirty == true )
		{
			ToggleDirty( );
		}
	}
	
	// set the point considered origin
	void Transform::SetOrigin( _In_ const Vector3& origin )
	{
		_origin = origin;
		ToggleDirty( );
	}

	// get the calculated left position
	float Transform::GetLeft( _In_ const Transform& transform, _In_ const float width )
	{
		if ( width == 0 )
		{
			return 0.0f;
		}

		return transform.GetPosition( ).x + ( width * transform.GetOrigin( ).x );
	}

	// get the calculated top position
	float Transform::GetTop( _In_ const Transform& transform, _In_ const float height )
	{
		if ( height == 0 )
		{
			return 0.0f;
		}

		return transform.GetPosition( ).y + ( height * transform.GetOrigin( ).y );
	}

	// get the calculated exact right position
	float Transform::GetRight( _In_ const Transform& transform, _In_ const float width )
	{
		if ( width == 0 )
		{
			return 0.0f;
		}

		return Transform::GetLeft( transform, width ) + width;
	}

	// get the calculated bottom position
	float Transform::GetBottom( _In_ const Transform& transform, _In_ const float height )
	{
		if ( height == 0 )
		{
			return 0.0f;
		}

		return Transform::GetTop( transform, height ) + height;
	}

	// get the calculated front position
	float Transform::GetFront( _In_ const Transform& transform, _In_ const float length )
	{
		if ( length == 0 )
		{
			return 0.0f;
		}

		return transform.GetPosition( ).z + ( length * transform.GetOrigin( ).z );
	}

	// get the calculated back position
	float Transform::GetBack( _In_ const Transform& transform, _In_ const float length )
	{
		if ( length == 0 )
		{
			return 0.0f;
		}

		return Transform::GetFront( transform, length ) + length;
	}

	// get the calculated center position
	Vector3 Transform::GetCenter( _In_ const Transform& transform, 
		_In_ const float width, _In_ const float height, _In_ const float length )
	{
		Vector3 output;

		output.x = Transform::GetLeft( transform, width ) + (width * 0.5f);
		output.y = Transform::GetTop( transform, height ) + (height * 0.5f);
		output.z = Transform::GetFront( transform, length ) + (length * 0.5f);

		return output;
	}

	// index operator
	float Transform::operator [] ( _In_ const Text name )
	{
		float output = 0.0f;

		// how can I optimize this? - if-else-if-else sucks
		if ( name == "x" )
			output = _position.x;
		else if ( name == "y" )
			output = _position.y;
		else if ( name == "z" )
			output = _position.z;
		else if ( name == "rotationX" )
			output = _rotation.x;
		else if ( name == "rotationY" )
			output = _rotation.y;
		else if ( name == "rotationZ" )
			output = _rotation.z;
		else if ( name == "scaleX" )
			output = _scale.x;
		else if ( name == "scaleY" )
			output = _scale.y;
		else if ( name == "scaleZ" )
			output = _scale.z;
		else if ( name == "originX" )
			output = _origin.x;
		else if ( name == "originY" )
			output = _origin.y;
		else if ( name == "originZ" )
			output = _origin.z;

		return output;
	}

	// return the current state of the transform
	TransformState Transform::GetState( ) const
	{
		TransformState output;
		output.Position = _position;
		output.Rotation = _rotation;
		output.Scale = _scale;
		return output;
	}

	// return to a previous state
	void Transform::ToState( _In_ const TransformState& state )
	{
		_position = state.Position;
		_rotation = state.Rotation;
		_scale = state.Scale;
		ToggleDirty( );
	}

	// set a parent to this transform
	void Transform::SetParent( _In_opt_ Transform* parent )
	{
		_parent = parent;
		ToggleDirty( );
	}

	// get the parent of this transform
	Transform* Transform::GetParent( )
	{
		return _parent;
	}

#pragma region ParentalGetters

	// recursive helper method to combine scale of all parents
	Vector3& getParentScale( _In_ Transform& transform, _In_ Vector3& current )
	{
		current *= transform.GetScale( );
		if ( transform.GetParent( ) != nullptr )
		{
			return getParentScale( *transform.GetParent( ), current );
		}
		else
		{
			return current;
		}
	}

	// get the combined scale of all parents
	Vector3 Transform::GetCombinedScale( ) const
	{
		if ( _parent != nullptr )
		{
			Vector3 output = Vector3( _scale );
			output = getParentScale( *_parent, output );
			return output;
		}
		else
		{
			// you f'cked up.
			return Vector3( _scale );
		}
	}

	// recursive helper method to combine all translation of parents
	Vector3& getParentPosition( _In_ Transform& transform, _In_ Vector3& current )
	{
		current += transform.GetPosition( );
		if ( transform.GetParent( ) != nullptr )
		{
			return getParentPosition( *transform.GetParent( ), current );
		}
		else
		{
			return current;
		}
	}

	// combine all parent's translations
	Vector3 Transform::GetCombinedPosition( ) const
	{
		if ( _parent != nullptr )
		{
			Vector3 output = Vector3( _position );
			output = getParentPosition( *_parent, output );
			return output;
		}
		else
		{
			// you f'cked up
			return Vector3( _position );
		}
	}

#pragma endregion
}