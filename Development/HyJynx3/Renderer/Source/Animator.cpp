/**
 * Animator.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\Animator.h>

namespace HyJynxRenderer
{
	// default ctor
	Animator::Animator()
		: ILog( "Animator" )
	{ }

	// good ol dtor
	Animator::~Animator()
	{
		_animationList.ForEach( [ ] ( _In_ IAnimation* animation )
		{
			animation->Release( );
		} );
		_animationList.Clear( );
	}

	// process active animations
	void Animator::Update( )
	{
		// TODO: calculate delta times
		// TODO: keep track of completed animations

		_animationList.ForEach( [ ] ( _In_ IAnimation* animation )
		{
			if ( animation->IsActive( ) == true )
			{
				animation->Update( 1.0 ); // TODO: pass calculated delta times
			}
		} );

		// TODO: remove complete animations
		// TODO: invalidate the AnimationController, by removing its IAnimation reference
	}

	// stops and removes an active animation
	void Animator::Remove( _In_ IAnimation* animation )
	{
		if ( animation != nullptr )
		{
			_animationList.Remove( animation );
		}
		else
		{
			LogWarning( "Remove(nullptr) - invalid arguments." );
		}
	}
}