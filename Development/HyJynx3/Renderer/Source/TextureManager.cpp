/**
 * TextureManager.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\TextureManager.h>
#include <Renderer\Headers\Renderer.h>
#include <Logger\Headers\Logger.h>


using namespace HyJynxCollections;
using namespace HyJynxLogger;
using namespace HyJynxCore;
using namespace HyJynxCollections;
using namespace std;

namespace HyJynxRenderer
{
#pragma region ConversionTables
	
	TextureManager::PixelTypeConversionTable::WICTranslate TextureManager::PixelTypeConversionTable::_WICFormats[ ] =
	{
		{ 
			GUID_WICPixelFormat128bppRGBAFloat, 
			DXGI_FORMAT::DXGI_FORMAT_R32G32B32A32_FLOAT 
		},

		{ 
			GUID_WICPixelFormat64bppRGBAHalf, 
			DXGI_FORMAT::DXGI_FORMAT_R16G16B16A16_FLOAT 
		},
		{ 
			GUID_WICPixelFormat64bppRGBA, 
			DXGI_FORMAT::DXGI_FORMAT_R16G16B16A16_UNORM 
		},

		{ 
			GUID_WICPixelFormat32bppRGBA, 
			DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM 
		},
		{ 
			GUID_WICPixelFormat32bppBGRA, 
			DXGI_FORMAT::DXGI_FORMAT_B8G8R8A8_UNORM 
		},
		{ 
			GUID_WICPixelFormat32bppBGR, 
			DXGI_FORMAT::DXGI_FORMAT_B8G8R8X8_UNORM 
		},

		{ 
			GUID_WICPixelFormat32bppRGBA1010102XR, 
			DXGI_FORMAT::DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM 
		},
		{ 
			GUID_WICPixelFormat32bppRGBA1010102, 
			DXGI_FORMAT::DXGI_FORMAT_R10G10B10A2_UNORM 
		},
		{ 
			GUID_WICPixelFormat32bppRGBE, 
			DXGI_FORMAT::DXGI_FORMAT_R9G9B9E5_SHAREDEXP 
		},

#ifdef DXGI_1_2_FORMATS

		{ 
			GUID_WICPixelFormat16bppBGRA5551, 
			DXGI_FORMAT::DXGI_FORMAT_B5G5R5A1_UNORM 
		},
		{ 
			GUID_WICPixelFormat16bppBGR565, 
			DXGI_FORMAT::DXGI_FORMAT_B5G6R5_UNORM 
		},

#endif // DXGI_1_2_FORMATS

		{ 
			GUID_WICPixelFormat32bppGrayFloat, 
			DXGI_FORMAT::DXGI_FORMAT_R32_FLOAT 
		},
		{ 
			GUID_WICPixelFormat16bppGrayHalf, 
			DXGI_FORMAT::DXGI_FORMAT_R16_FLOAT 
		},
		{ 
			GUID_WICPixelFormat16bppGray, 
			DXGI_FORMAT::DXGI_FORMAT_R16_UNORM 
		},
		{ 
			GUID_WICPixelFormat8bppGray, 
			DXGI_FORMAT::DXGI_FORMAT_R8_UNORM 
		},

		{ 
			GUID_WICPixelFormat8bppAlpha, 
			DXGI_FORMAT::DXGI_FORMAT_A8_UNORM 
		},

#if ( _WIN32_WINNT >= 0x0602 )
		{ 
			GUID_WICPixelFormat96bppRGBFloat, 
			DXGI_FORMAT::DXGI_FORMAT_R32G32B32_FLOAT 
		},
#endif
	};

	TextureManager::PixelTypeConversionTable::WICConvert TextureManager::PixelTypeConversionTable::_WICConvertTable[ ] =
	{
		{ // DXGI_FORMAT_R8_UNORM
			GUID_WICPixelFormatBlackWhite, 
			GUID_WICPixelFormat8bppGray 
		},

		{ // DXGI_FORMAT_R8G8B8A8_UNORM 
			GUID_WICPixelFormat1bppIndexed, 
			GUID_WICPixelFormat32bppRGBA 
		}, 
		{ // DXGI_FORMAT_R8G8B8A8_UNORM
			GUID_WICPixelFormat2bppIndexed, 
			GUID_WICPixelFormat32bppRGBA 
		},  
		{ // DXGI_FORMAT_R8G8B8A8_UNORM 
			GUID_WICPixelFormat4bppIndexed, 
			GUID_WICPixelFormat32bppRGBA 
		}, 
		{ // DXGI_FORMAT_R8G8B8A8_UNORM 
			GUID_WICPixelFormat8bppIndexed, 
			GUID_WICPixelFormat32bppRGBA 
		}, 

		{ // DXGI_FORMAT_R8_UNORM 
			GUID_WICPixelFormat2bppGray, 
			GUID_WICPixelFormat8bppGray 
		}, 
		{ // DXGI_FORMAT_R8_UNORM 
			GUID_WICPixelFormat4bppGray, 
			GUID_WICPixelFormat8bppGray 
		}, 

		{ // DXGI_FORMAT_R16_FLOAT 
			GUID_WICPixelFormat16bppGrayFixedPoint, 
			GUID_WICPixelFormat16bppGrayHalf 
		}, 
		{ // DXGI_FORMAT_R32_FLOAT 
			GUID_WICPixelFormat32bppGrayFixedPoint, 
			GUID_WICPixelFormat32bppGrayFloat 
		}, 

#ifdef DXGI_1_2_FORMATS

		{ // DXGI_FORMAT_B5G5R5A1_UNORM
			GUID_WICPixelFormat16bppBGR555, 
			GUID_WICPixelFormat16bppBGRA5551 
		}, 

#else

		{ // DXGI_FORMAT_R8G8B8A8_UNORM
			GUID_WICPixelFormat16bppBGR555, 
			GUID_WICPixelFormat32bppRGBA 
		}, 
		{ // DXGI_FORMAT_R8G8B8A8_UNORM
			GUID_WICPixelFormat16bppBGRA5551, 
			GUID_WICPixelFormat32bppRGBA 
		}, 
		{ // DXGI_FORMAT_R8G8B8A8_UNORM
			GUID_WICPixelFormat16bppBGR565, 
			GUID_WICPixelFormat32bppRGBA 
		}, 

#endif // DXGI_1_2_FORMATS

		{ // DXGI_FORMAT_R10G10B10A2_UNORM
			GUID_WICPixelFormat32bppBGR101010, 
			GUID_WICPixelFormat32bppRGBA1010102 
		}, 

		{ // DXGI_FORMAT_R8G8B8A8_UNORM 
			GUID_WICPixelFormat24bppBGR, 
			GUID_WICPixelFormat32bppRGBA 
		}, 
		{ // DXGI_FORMAT_R8G8B8A8_UNORM 
			GUID_WICPixelFormat24bppRGB, 
			GUID_WICPixelFormat32bppRGBA 
		}, 
		{ // DXGI_FORMAT_R8G8B8A8_UNORM 
			GUID_WICPixelFormat32bppPBGRA, 
			GUID_WICPixelFormat32bppRGBA 
		},
		{ // DXGI_FORMAT_R8G8B8A8_UNORM 
			GUID_WICPixelFormat32bppPRGBA, 
			GUID_WICPixelFormat32bppRGBA 
		}, 

		{ // DXGI_FORMAT_R16G16B16A16_UNORM
			GUID_WICPixelFormat48bppRGB, 
			GUID_WICPixelFormat64bppRGBA 
		}, 
		{ // DXGI_FORMAT_R16G16B16A16_UNORM
			GUID_WICPixelFormat48bppBGR, 
			GUID_WICPixelFormat64bppRGBA 
		}, 
		{ // DXGI_FORMAT_R16G16B16A16_UNORM
			GUID_WICPixelFormat64bppBGRA, 
			GUID_WICPixelFormat64bppRGBA 
		}, 
		{ // DXGI_FORMAT_R16G16B16A16_UNORM
			GUID_WICPixelFormat64bppPRGBA, 
			GUID_WICPixelFormat64bppRGBA 
		}, 
		{ // DXGI_FORMAT_R16G16B16A16_UNORM
			GUID_WICPixelFormat64bppPBGRA, 
			GUID_WICPixelFormat64bppRGBA 
		}, 

		{ // DXGI_FORMAT_R16G16B16A16_FLOAT 
			GUID_WICPixelFormat48bppRGBFixedPoint, 
			GUID_WICPixelFormat64bppRGBAHalf 
		}, 
		{ // DXGI_FORMAT_R16G16B16A16_FLOAT 
			GUID_WICPixelFormat48bppBGRFixedPoint, 
			GUID_WICPixelFormat64bppRGBAHalf 
		}, 
		{ // DXGI_FORMAT_R16G16B16A16_FLOAT 
			GUID_WICPixelFormat64bppRGBAFixedPoint, 
			GUID_WICPixelFormat64bppRGBAHalf 
		}, 
		{ // DXGI_FORMAT_R16G16B16A16_FLOAT 
			GUID_WICPixelFormat64bppBGRAFixedPoint, 
			GUID_WICPixelFormat64bppRGBAHalf 
		}, 
		{ // DXGI_FORMAT_R16G16B16A16_FLOAT 
			GUID_WICPixelFormat64bppRGBFixedPoint, 
			GUID_WICPixelFormat64bppRGBAHalf 
		}, 
		{ // DXGI_FORMAT_R16G16B16A16_FLOAT 
			GUID_WICPixelFormat64bppRGBHalf, 
			GUID_WICPixelFormat64bppRGBAHalf 
		}, 
		{ // DXGI_FORMAT_R16G16B16A16_FLOAT
			GUID_WICPixelFormat48bppRGBHalf, 
			GUID_WICPixelFormat64bppRGBAHalf 
		},  

		{ // DXGI_FORMAT_R32G32B32A32_FLOAT
			GUID_WICPixelFormat96bppRGBFixedPoint, 
			GUID_WICPixelFormat128bppRGBAFloat 
		},  
		{ // DXGI_FORMAT_R32G32B32A32_FLOAT
			GUID_WICPixelFormat128bppPRGBAFloat, 
			GUID_WICPixelFormat128bppRGBAFloat 
		},  
		{ // DXGI_FORMAT_R32G32B32A32_FLOAT 
			GUID_WICPixelFormat128bppRGBFloat, 
			GUID_WICPixelFormat128bppRGBAFloat 
		}, 
		{ // DXGI_FORMAT_R32G32B32A32_FLOAT 
			GUID_WICPixelFormat128bppRGBAFixedPoint, 
			GUID_WICPixelFormat128bppRGBAFloat 
		}, 
		{ // DXGI_FORMAT_R32G32B32A32_FLOAT 
			GUID_WICPixelFormat128bppRGBFixedPoint, 
			GUID_WICPixelFormat128bppRGBAFloat 
		}, 

		{ // DXGI_FORMAT_R8G8B8A8_UNORM 
			GUID_WICPixelFormat32bppCMYK, 
			GUID_WICPixelFormat32bppRGBA 
		}, 
		{ // DXGI_FORMAT_R16G16B16A16_UNORM
			GUID_WICPixelFormat64bppCMYK, 
			GUID_WICPixelFormat64bppRGBA 
		}, 
		{ // DXGI_FORMAT_R16G16B16A16_UNORM
			GUID_WICPixelFormat40bppCMYKAlpha, 
			GUID_WICPixelFormat64bppRGBA 
		}, 
		{ // DXGI_FORMAT_R16G16B16A16_UNORM
			GUID_WICPixelFormat80bppCMYKAlpha, 
			GUID_WICPixelFormat64bppRGBA 
		}, 

#if ( _WIN32_WINNT >= 0x0602 )
		{ // DXGI_FORMAT_R8G8B8A8_UNORM
			GUID_WICPixelFormat32bppRGB, 
			GUID_WICPixelFormat32bppRGBA 
		}, 
		{ // DXGI_FORMAT_R16G16B16A16_UNORM
			GUID_WICPixelFormat64bppRGB, 
			GUID_WICPixelFormat64bppRGBA 
		}, 
		{ // DXGI_FORMAT_R16G16B16A16_FLOAT 
			GUID_WICPixelFormat64bppPRGBAHalf, 
			GUID_WICPixelFormat64bppRGBAHalf 
		}, 
#endif
	};

	// convert from WIC pixel type to DXGI pixel type
	DXGI_FORMAT TextureManager::PixelTypeConversionTable::WICToDXGI( _In_ const GUID& guid )
	{
		for ( size_t i = 0; i < _countof( _WICFormats ); ++i )
		{
			if ( memcmp( &_WICFormats[ i ].WIC, &guid, sizeof( GUID ) ) == 0 )
				return _WICFormats[ i ].Format;
		}

		return DXGI_FORMAT::DXGI_FORMAT_UNKNOWN;
	}

	// get the pixel bit depth of the argued WIC format
	size_t TextureManager::PixelTypeConversionTable::WICBitsPerPixel( 
		_In_ IWICImagingFactory* const factory, _In_ const GUID& targetGuid )
	{
		if ( factory == nullptr )
		{
			return 0;
		}

		IWICComponentInfo* info;
		if ( FAILED( factory->CreateComponentInfo( targetGuid, &info ) ) )
		{
			return 0;
		}

		WICComponentType type;
		if ( FAILED( info->GetComponentType( &type ) ) )
		{
			info->Release( );
			return 0;
		}

		if ( type != WICComponentType::WICPixelFormat )
		{
			info->Release( );
			return 0;
		}

		IWICPixelFormatInfo* pixelInfo;
		if ( FAILED( info->QueryInterface( __uuidof( IWICPixelFormatInfo ), reinterpret_cast< void** >( &pixelInfo ) ) ) )
		{
			info->Release( );
			return 0;
		}

		UINT bits;
		if ( FAILED( pixelInfo->GetBitsPerPixel( &bits ) ) )
		{
			info->Release( );
			pixelInfo->Release( );
			return 0;
		}

		info->Release( );
		pixelInfo->Release( );

		return bits;
	}

#pragma endregion

#pragma region ctorDtor

	// default ctor
	TextureManager::TextureManager( )
		: ILog( "TextureManager" )
	{ }

	// dtor
	TextureManager::~TextureManager( )
	{
		ReleaseAll( );

		if ( _factory != nullptr )
		{
			_factory->Release( );
			_factory = nullptr;
		}
	}

	// release all textures from memory
	void TextureManager::ReleaseAll( )
	{
		_textureList.Clear( );
	}

	// hidden helper method to get max tex size
	UInt getMaximumTextureSize( )
	{
		UInt output = 0;

		switch ( Renderer::Instance( )->GetDeviceFeatureLevel( ) )
		{
		case D3D_FEATURE_LEVEL_9_1:
		case D3D_FEATURE_LEVEL_9_2:
			output = 2048;
			break;

		case D3D_FEATURE_LEVEL_9_3:
			output = 4096;
			break;

		case D3D_FEATURE_LEVEL_10_0:
		case D3D_FEATURE_LEVEL_10_1:
			output = 8192;
			break;

		default:
			output = D3D11_REQ_TEXTURE2D_U_OR_V_DIMENSION;
			break;
		}

		return output;
	}

	// init WIC decoder factory
	bool TextureManager::Initialize( )
	{
		if ( _factory != nullptr )
		{
			_factory->Release( );
			_factory = nullptr;
		}

		HRESULT result = CoCreateInstance( CLSID_WICImagingFactory, nullptr,
			CLSCTX_INPROC_SERVER, IID_IWICImagingFactory, ( LPVOID* ) &_factory );

		if ( FAILED( result ) )
		{
			Logger::Log( "TextureManager::InitializeFactory() - Failed to create Factory Instance." );
			_factory = nullptr;
			return false;
		}

		_maxTextureSize = getMaximumTextureSize( );

		return true;
	}

#pragma endregion

#pragma region GetAPI

	// retrieve a 1D texture, if it exists
	Texture1D* TextureManager::GetTexture1D( _In_ const TextureInit* const init )
	{
		Texture1D* output = nullptr;
		TexturePair pair;

		for ( UInt index = 0; index < _textureList.Count( ); index++ )
		{
			pair = _textureList.ElementAt( index );
			if ( pair.Key.Key == TextureType::TEXTURE_TYPE_1D
				&& pair.Key.Value == init )
			{
				output = anew( Texture1D )( /* stuff */ );
				break;
			}
		}

		return output;
	}

	// retrieve a 2D texture, if it exists
	Texture2D* TextureManager::GetTexture2D( _In_ const TextureInit* const init )
	{
		Texture2D* output = nullptr;
		TexturePair pair;

		for ( UInt index = 0; index < _textureList.Count( ); index++ )
		{
			pair = _textureList.ElementAt( index );
			if ( pair.Key.Key == TextureType::TEXTURE_TYPE_2D
				&& pair.Key.Value == init )
			{
				output = anew( Texture2D )( /* stuff */ );
				break;
			}
		}

		return output;
	}

	// retrieve a 3D texture, if it exists
	Texture3D* TextureManager::GetTexture3D( _In_ const TextureInit* const init )
	{
		Texture3D* output = nullptr;
		TexturePair pair;

		for ( UInt index = 0; index < _textureList.Count( ); index++ )
		{
			pair = _textureList.ElementAt( index );
			if ( pair.Key.Key == TextureType::TEXTURE_TYPE_3D
				&& pair.Key.Value == init )
			{
				output = anew( Texture3D )( /* stuff */ );
				break;
			}
		}

		return output;
	}

	// request for a 1D texture resource
	void TextureManager::RequestTexture1D( _In_ TextureInit* init,
		_In_ Function< void, Texture1D* > onSuccess,
		_In_opt_ Function< void, TextureRequestError > onFail )
	{
		if ( _factory == nullptr )
		{
			LogError( "TextureManager is not initialized." );
			if ( onFail.HasValue() == true )
			{
				onFail( TextureRequestError::TEXTURE_ERROR_FACTORY_UNINITIALIZED );
			}
			return;
		}

		if ( init->Filepath.IsNullOrEmpty( ) == false )
		{
			Texture1D* output = GetTexture1D( init );
			
			if ( output != nullptr )
			{
				onSuccess( output );
			}
			else
			{
				AddTo1DQueue( init, onSuccess, onFail );
				LoadTexture1D( init );
			}
		}
		else
		{
			if ( onFail.HasValue() == true )
			{
				onFail( TextureRequestError::TEXTURE_ERROR_BAD_FILEPATH );
			}
		}
	}

	// request for a 2D texture resource
	void TextureManager::RequestTexture2D( _In_ TextureInit* init,
		_In_ Function< void, Texture2D* > onSuccess,
		_In_opt_ Function< void, TextureRequestError > onFail )
	{
		if ( _factory == nullptr )
		{
			LogWarning( "TextureManager is not initialized." );
			if ( onFail.HasValue() == true )
			{
				onFail( TextureRequestError::TEXTURE_ERROR_FACTORY_UNINITIALIZED );
				return;
			}
		}

		if ( init->Filepath.IsNullOrEmpty( ) == false )
		{
			Texture2D* output = GetTexture2D( init );

			if ( output != nullptr )
			{
				onSuccess( output );
			}
			else
			{
				AddTo2DQueue( init, onSuccess, onFail );
				LoadTexture2D( init );
			}
		}
		else
		{
			if ( onFail.HasValue() == true )
			{
				onFail( TextureRequestError::TEXTURE_ERROR_BAD_FILEPATH );
			}
		}
	}

	// request for a 3D texture resource
	void TextureManager::RequestTexture3D( _In_ TextureInit* init,
		_In_ Function< void, Texture3D* > onSuccess,
		_In_opt_ Function< void, TextureRequestError > onFail )
	{
		if ( _factory == nullptr )
		{
			LogWarning( "TextureManager is not initialized." );
			if ( onFail.HasValue() == true )
			{
				onFail( TextureRequestError::TEXTURE_ERROR_FACTORY_UNINITIALIZED );
			}
			return;
		}

		if ( init->Filepath.IsNullOrEmpty( ) == false )
		{
			Texture3D* output = GetTexture3D( init );

			if ( output != nullptr )
			{
				onSuccess( output );
			}
			else
			{
				AddTo3DQueue( init, onSuccess, onFail );
				LoadTexture3D( init );
			}
		}
		else
		{
			if ( onFail.HasValue() == true )
			{
				onFail( TextureRequestError::TEXTURE_ERROR_BAD_FILEPATH );
			}
		}
	}

#pragma endregion

#pragma region Loading

	// get a frame decoder from decrypted data
	IWICBitmapFrameDecode* TextureManager::GetTextureFrameFromData( 
		_In_bytecount_( sizeInBytes ) char* const data, _In_ size_t sizeInBytes )
	{
		IWICStream* stream;
		if ( FAILED( _factory->CreateStream( &stream ) ) )
		{
			return nullptr;
		}

		// TODO: reinterpret_cast?
		if ( FAILED( stream->InitializeFromMemory( ( uint8_t* ) data, static_cast< DWORD >( sizeInBytes ) ) ) )
		{
			stream->Release( );
			return nullptr;
		}

		IWICBitmapDecoder* decoder = nullptr;

		if ( FAILED( _factory->CreateDecoderFromStream( stream, 0, WICDecodeMetadataCacheOnDemand, &decoder ) ) )
		{
			stream->Release( );
			return nullptr;
		}

		IWICBitmapFrameDecode* output = nullptr;

		if ( FAILED( decoder->GetFrame( 0, &output ) ) )
		{
			output = nullptr;
		}

		stream->Release( );
		decoder->Release( );

		return output;
	}
	
	// get a frame decoder from file
	IWICBitmapFrameDecode* TextureManager::GetTextureFrameFromFile( _In_ const HyJynxCore::Text& filepath )
	{
		IWICBitmapDecoder* decoder = nullptr;
		IWICBitmapFrameDecode* output = nullptr;

		if ( SUCCEEDED( _factory->CreateDecoderFromFilename( ( LPCWSTR ) filepath.GetValue( ), 0,
			GENERIC_READ, WICDecodeMetadataCacheOnDemand, &decoder ) ) )
		{
			if ( FAILED( decoder->GetFrame( 0, &output ) ) )
			{
				output = nullptr;
				Text log = "GetTextureDecoderFromFile(";
				log += filepath;
				log += ") - Failed to get frame decode.";
				LogWarning( log );
			}

			decoder->Release( );
		}
		else
		{
			output = nullptr;
			Text log = "GetTextureDecoderFromFile(";
			log += filepath;
			log += ") - Failed to create image decoder.";
			LogWarning( log );
		}

		return output;
	}

	// determine the DXGI pixel format of the argued frame decode
	DXGI_FORMAT TextureManager::GetPixelFormat( _In_ IWICBitmapFrameDecode* const frame, _Out_ size_t* bpp, _Out_ WICPixelFormatGUID* wicFormat )
	{
		if ( frame == nullptr )
		{
			return DXGI_FORMAT::DXGI_FORMAT_UNKNOWN;
		}

		WICPixelFormatGUID pixelFormat;
		if ( FAILED( frame->GetPixelFormat( &pixelFormat ) ) )
		{
			return DXGI_FORMAT::DXGI_FORMAT_UNKNOWN;
		}

		WICPixelFormatGUID convertGUID;
		memcpy( &convertGUID, &pixelFormat, sizeof( WICPixelFormatGUID ) );
		*wicFormat = convertGUID;

		DXGI_FORMAT format = PixelTypeConversionTable::WICToDXGI( pixelFormat );
		if ( format == DXGI_FORMAT::DXGI_FORMAT_UNKNOWN )
		{
			for ( size_t i = 0; i < _countof( PixelTypeConversionTable::_WICConvertTable ); ++i )
			{
				if ( memcmp( &PixelTypeConversionTable::_WICConvertTable[ i ].Source, 
					&pixelFormat, sizeof( WICPixelFormatGUID ) ) == 0 )
				{
					memcpy( &convertGUID, &PixelTypeConversionTable::_WICConvertTable[ i ].Target, 
						sizeof( WICPixelFormatGUID ) );

					format = PixelTypeConversionTable::WICToDXGI( PixelTypeConversionTable::_WICConvertTable[ i ].Target );
					
					if ( format != DXGI_FORMAT::DXGI_FORMAT_UNKNOWN )
					{
						*bpp = PixelTypeConversionTable::WICBitsPerPixel( _factory, pixelFormat );
						return format;
					}
				}
			}
		}
		else
		{
			*bpp = PixelTypeConversionTable::WICBitsPerPixel( _factory, pixelFormat );
			return format;
		}

		return format;
	}

	// local method to test if we want to go to the gpu
	bool loadToGpu( TextureLoadTarget target )
	{
		return target == TextureLoadTarget::TEXTURE_LOAD_GPU
			|| target == TextureLoadTarget::TEXTURE_LOAD_GPU_CPU;
	}

	// local method to test if we want to go to the cpu
	bool loadToCpu( TextureLoadTarget target )
	{
		return target == TextureLoadTarget::TEXTURE_LOAD_CPU
			|| target == TextureLoadTarget::TEXTURE_LOAD_GPU_CPU;
	}

	// pre-initialization common to all texture types
	PreInitOutput TextureManager::PreInitTexture( _In_ const TextureInit* const init )
	{
		PreInitOutput output;

		IWICBitmapFrameDecode* frame = nullptr;
		if ( init->ImageData != nullptr && init->ImageDataSizeInBytes != 0 )
		{
			frame = GetTextureFrameFromData( init->ImageData, init->ImageDataSizeInBytes );
		}
		else
		{
			frame = GetTextureFrameFromFile( init->Filepath );
		}

		if ( frame == nullptr )
		{
			output.Status = TextureRequestError::TEXTURE_ERROR_FRAME_FAIL;
			return output;
		}

		output.Frame = frame;

		size_t size;
		WICPixelFormatGUID wicFormat;
		DXGI_FORMAT format = GetPixelFormat( frame, &size, &wicFormat );
		if ( format == DXGI_FORMAT::DXGI_FORMAT_UNKNOWN )
		{
			output.Status = TextureRequestError::TEXTURE_ERROR_UNSUPPORTED_PIXEL_TYPE;
			return output;
		}

		output.DxgiFormat = format;
		output.PixelSizeInBytes = size;
		output.WICFormat = wicFormat;

		if ( !( Renderer::Instance( )->GetFormatSupport( format ) & D3D11_FORMAT_SUPPORT::D3D11_FORMAT_SUPPORT_TEXTURE1D ) )
		{
			// TODO: observe this (auto-fallback)
			output.DxgiFormat = DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM;
			output.PixelSizeInBytes = 32;
		}

		UInt nativeWidth = 0, nativeHeight = 0;
		if ( FAILED( frame->GetSize( &nativeWidth, &nativeHeight ) ) )
		{
			output.Status = TextureRequestError::TEXTURE_ERROR_DIMENSION_INQUIRE_FAILED;
			return output;
		}

		output.NativeWidth = nativeWidth;
		output.NativeHeight = nativeHeight;

		UInt loadWidth = nativeWidth, loadHeight = nativeHeight;
		if ( ( nativeWidth > _maxTextureSize || nativeHeight > _maxTextureSize )
			&& loadToGpu( init->TextureLoadTarget ) == true )
		{
			float aspectRatio = static_cast< float >( nativeHeight ) / static_cast< float >( nativeWidth );
			if ( nativeWidth > nativeHeight )
			{
				loadWidth = _maxTextureSize;
				loadHeight = static_cast< UInt >( static_cast< float >( _maxTextureSize ) * aspectRatio );
			}
			else
			{
				loadWidth = static_cast< UInt > ( static_cast< float >( _maxTextureSize ) * aspectRatio );
				loadHeight = _maxTextureSize;
			}
		}

		output.LoadWidth = loadWidth;
		output.LoadHeight = loadHeight;

		output.RowPitch = ( loadWidth * size + 7 ) / 8;
		output.ImageSizeInBytes = output.RowPitch * loadHeight;

		return output;
	}

	// copies frame pixels to cpu memory, accounts for auto resize, and format conversions
	// TODO: this can probably be done better - its kinda long and ugly
	void TextureManager::LoadCPUData( _In_ PreInitOutput* init, _Out_ uint8_t** output )
	{
		HRESULT result;

		if ( memcmp( &init->WICFormat, &init->DxgiFormat, sizeof( GUID ) ) == 0
			&& init->NativeWidth == init->LoadWidth
			&& init->NativeHeight == init->LoadHeight )
		{
			// no resize, no conversion
			result = init->Frame->CopyPixels( 0, init->RowPitch, init->ImageSizeInBytes, *output );
			if ( FAILED( result ) )
			{
				init->Status = TextureRequestError::TEXTURE_ERROR_COPY_PIXEL_FAIL;
			}
		}
		else if ( init->NativeWidth != init->LoadWidth
			|| init->NativeHeight != init->LoadHeight )
		{
			// resize
			IWICBitmapScaler* scaler;
			result = _factory->CreateBitmapScaler( &scaler );
			if ( FAILED( result ) )
			{
				init->Status = TextureRequestError::TEXTURE_ERROR_RESIZE_FAIL;
				return;
			}

			result = scaler->Initialize( init->Frame, init->LoadWidth, init->LoadHeight, WICBitmapInterpolationModeFant );
			if ( FAILED( result ) )
			{
				init->Status = TextureRequestError::TEXTURE_ERROR_RESIZE_FAIL;
				return;
			}

			WICPixelFormatGUID formatScalar;
			result = scaler->GetPixelFormat( &formatScalar );
			if ( FAILED( result ) )
			{
				init->Status = TextureRequestError::TEXTURE_ERROR_RESIZE_FAIL;
				return;
			}

			if ( memcmp( &init->WICFormat, &formatScalar, sizeof( GUID ) ) == 0 )
			{
				// resize only
				result = scaler->CopyPixels( 0, init->LoadWidth, init->LoadHeight, *output );
				if ( FAILED( result ) )
				{
					init->Status = TextureRequestError::TEXTURE_ERROR_COPY_PIXEL_FAIL;
					return;
				}
			}
			else
			{
				// resize & convert
				IWICFormatConverter* converter;
				result = _factory->CreateFormatConverter( &converter );
				if ( FAILED( result ) )
				{
					init->Status = TextureRequestError::TEXTURE_ERROR_CONVERSION_FAIL;
					return;
				}

				result = converter->Initialize( scaler, init->WICFormat,
					WICBitmapDitherTypeErrorDiffusion, 0, 0, WICBitmapPaletteTypeCustom );
				if ( FAILED( result ) )
				{
					converter->Release( );
					init->Status = TextureRequestError::TEXTURE_ERROR_CONVERSION_FAIL;
					return;
				}

				result = converter->CopyPixels( 0, init->RowPitch, init->ImageSizeInBytes, *output );
				if ( FAILED( result ) )
				{
					converter->Release( );
					init->Status = TextureRequestError::TEXTURE_ERROR_COPY_PIXEL_FAIL;
					return;
				}
			}
		}
		else
		{
			// conversion only
			IWICFormatConverter* converter;
			result = _factory->CreateFormatConverter( &converter );
			if ( FAILED( result ) )
			{
				init->Status = TextureRequestError::TEXTURE_ERROR_CONVERSION_FAIL;
				return;
			}

			// TODO: expand with optional dither and threshold
			result = converter->Initialize( init->Frame,
				GUID_WICPixelFormat32bppPRGBA,
				WICBitmapDitherType::WICBitmapDitherTypeNone,
				nullptr,
				0,
				WICBitmapPaletteType::WICBitmapPaletteTypeCustom );

			if ( FAILED( result ) )
			{
				converter->Release( );
				init->Status = TextureRequestError::TEXTURE_ERROR_CONVERSION_FAIL;
				return;
			}

			result = converter->CopyPixels( 0, init->RowPitch, init->ImageSizeInBytes, *output );
			if ( FAILED( result ) )
			{
				converter->Release( );
				init->Status = TextureRequestError::TEXTURE_ERROR_COPY_PIXEL_FAIL;
				return;
			}
		}
	}

	// ================ 1D Loading ================
	// ============================================

	// load the argued 1D texture data to the gpu
	void TextureManager::LoadGPUResource1D(
		_In_ const TextureInit* const init,
		_In_ const PreInitOutput& preInit,
		_In_ D3D11_TEXTURE1D_DESC& description,
		_In_ D3D11_SUBRESOURCE_DATA& data,
		_Out_ ID3D11Texture1D** texture,
		_Out_ ID3D11ShaderResourceView** resource )
	{
		HRESULT result = Renderer::Instance( )
			->GetGraphicsLibrary( )
			->GetDevice( )
			->CreateTexture1D( &description, &data, texture );

		if ( FAILED( result ) )
		{
			*texture = nullptr;
			return;
		}

		D3D11_SHADER_RESOURCE_VIEW_DESC resourceDesc;
		memset( &resourceDesc, 0, sizeof( resourceDesc ) );
		resourceDesc.Format = preInit.DxgiFormat;
		resourceDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE1D;
		resourceDesc.Texture1D.MipLevels = init->AutoMipMap ? -1 : 1;

		result = Renderer::Instance( )
			->GetGraphicsLibrary( )
			->GetDevice( )
			->CreateShaderResourceView( *texture, &resourceDesc, resource );

		if ( FAILED( result ) )
		{
			( *texture )->Release( );
			*texture = nullptr;
			*resource = nullptr;
			return;
		}

		if ( init->AutoMipMap == true )
		{
			// TODO: this dereference sucks... fix it somehow
			Renderer::Instance( )
				->GetGraphicsLibrary( )
				->GetDefaultContext( )
				->GetDeviceContext( )
				->UpdateSubresource( *texture, 0, nullptr, data.pSysMem, preInit.RowPitch, preInit.ImageSizeInBytes );

			Renderer::Instance( )
				->GetGraphicsLibrary( )
				->GetDefaultContext( )
				->GetDeviceContext( )
				->GenerateMips( *resource );
		}
	}

	// load a 1D texture from file
	void TextureManager::LoadTexture1D( _In_ TextureInit* const init )
	{
		// TODO: async (based on flag within TextureInit)

		PreInitOutput preInit = PreInitTexture( init );

		if ( preInit.Status != TextureRequestError::TEXTURE_ERROR_NONE )
		{
			Process1DQueue( init, preInit.Status );
			return;
		}

		uint8_t* cpuMemory( anewa( uint8_t, preInit.ImageSizeInBytes ) );
		LoadCPUData( &preInit, &cpuMemory );

		if ( preInit.Status != TextureRequestError::TEXTURE_ERROR_NONE )
		{
			Process1DQueue( init, preInit.Status );
			return;
		}

		if ( init->AutoMipMap == true )
		{
			init->AutoMipMap = (Renderer::Instance( )->GetFormatSupport( preInit.DxgiFormat )
				& D3D11_FORMAT_SUPPORT_MIP_AUTOGEN) == 0;
		}

		ID3D11Texture1D* dxTexture = nullptr;
		ID3D11ShaderResourceView* dxResource = nullptr;

		if ( loadToGpu( init->TextureLoadTarget ) == true )
		{
			// TODO: research any possible optimizations or automations here
			D3D11_TEXTURE1D_DESC desc;
			desc.Width = preInit.LoadWidth;
			desc.MipLevels = init->AutoMipMap ? 0 : 1;
			desc.ArraySize = 1;
			desc.Format = preInit.DxgiFormat;
			desc.Usage = D3D11_USAGE_DEFAULT;
			desc.BindFlags = init->AutoMipMap ?
				// TODO: why render target for mipMaps?
				D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET :
				D3D11_BIND_SHADER_RESOURCE;
			desc.CPUAccessFlags = 0;
			desc.MiscFlags = init->AutoMipMap ? D3D11_RESOURCE_MISC_GENERATE_MIPS : 0;

			D3D11_SUBRESOURCE_DATA initData;
			initData.pSysMem = cpuMemory;
			initData.SysMemPitch = static_cast<UINT>( preInit.RowPitch );
			initData.SysMemSlicePitch = static_cast<UINT>( preInit.ImageSizeInBytes );

			LoadGPUResource1D( init, preInit, desc, initData, &dxTexture, &dxResource );

			if ( dxTexture == nullptr || dxResource == nullptr )
			{
				Process1DQueue( init, TextureRequestError::TEXTURE_ERROR_GPU_LOAD_FAIL );
				return;
			}

#if defined(_DEBUG)
			dxTexture->SetPrivateData( WKPDID_D3DDebugObjectName,
				sizeof( "Texture1D" ) - 1,
				"Texture1D" );
#endif
		}

		if ( loadToCpu( init->TextureLoadTarget ) == false )
		{
			cpuMemory = nullptr;
		}

		LoadedTexture storedData =
		{
			init->Filepath,
			dxResource,
			dxTexture,
			cpuMemory,
			preInit.ImageSizeInBytes,
			preInit.NativeWidth,
			preInit.NativeHeight,
			0, // TODO
			preInit.LoadWidth,
			preInit.LoadHeight,
			0, // TODO
			TextureType::TEXTURE_TYPE_1D
		};

		_textureList.Append( KeyValuePair< TextureType, TextureInit* >( TextureType::TEXTURE_TYPE_1D, init ), storedData );

		Process1DQueue( init, storedData );
	}

	// ================ 2D Loading ================
	// ============================================

	// load the argued 2D texture data to the gpu
	void TextureManager::LoadGPUResource2D( 
		_In_ const TextureInit* const init, 
		_In_ const PreInitOutput& preInit, 
		_In_ D3D11_TEXTURE2D_DESC& description,
		_In_ D3D11_SUBRESOURCE_DATA& data, 
		_Out_ ID3D11Texture2D** texture, 
		_Out_ ID3D11ShaderResourceView** resource )
	{
		HRESULT result = Renderer::Instance( )
			->GetGraphicsLibrary( )
			->GetDevice( )
			->CreateTexture2D( &description, &data, texture );

		if ( FAILED( result ) )
		{
			*texture = nullptr;
			return;
		}

		D3D11_SHADER_RESOURCE_VIEW_DESC resourceDesc;
		memset( &resourceDesc, 0, sizeof( resourceDesc ) );
		resourceDesc.Format = preInit.DxgiFormat;
		resourceDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		resourceDesc.Texture2D.MipLevels = init->AutoMipMap ? -1 : 1;

		result = Renderer::Instance( )
			->GetGraphicsLibrary( )
			->GetDevice( )
			->CreateShaderResourceView( *texture, &resourceDesc, resource );

		if ( FAILED( result ) )
		{
			(*texture)->Release( );
			*texture = nullptr;
			*resource = nullptr;
			return;
		}

		if ( init->AutoMipMap == true )
		{
			// TODO: this dereference sucks... fix it somehow
			Renderer::Instance( )
				->GetGraphicsLibrary( )
				->GetDefaultContext( )
				->GetDeviceContext( )
				->UpdateSubresource( *texture, 0, nullptr, data.pSysMem, preInit.RowPitch, preInit.ImageSizeInBytes );

			Renderer::Instance( )
				->GetGraphicsLibrary( )
				->GetDefaultContext( )
				->GetDeviceContext( )
				->GenerateMips( *resource );
		}
	}

	// load a 2D texture from file
	void TextureManager::LoadTexture2D( _In_ TextureInit* const init )
	{
		// TODO: async (based on flag within TextureInit)

		PreInitOutput preInit = PreInitTexture( init );

		if ( preInit.Status != TextureRequestError::TEXTURE_ERROR_NONE )
		{
			Process2DQueue( init, preInit.Status );
			return;
		}

		uint8_t* cpuMemory( anewa( uint8_t, preInit.ImageSizeInBytes ) );
		LoadCPUData( &preInit, &cpuMemory );

		if ( preInit.Status != TextureRequestError::TEXTURE_ERROR_NONE )
		{
			Process2DQueue( init, preInit.Status );
			return;
		}

		if ( init->AutoMipMap == true )
		{
			init->AutoMipMap = ( Renderer::Instance( )->GetFormatSupport( preInit.DxgiFormat )
				& D3D11_FORMAT_SUPPORT_MIP_AUTOGEN) == 0;
		}

		ID3D11Texture2D* dxTexture = nullptr;
		ID3D11ShaderResourceView* dxResource = nullptr;

		if ( loadToGpu( init->TextureLoadTarget ) == true )
		{
			// TODO: research any possible optimizations or automations here
			D3D11_TEXTURE2D_DESC desc;
			desc.Width = preInit.LoadWidth;
			desc.Height = preInit.LoadHeight;
			desc.MipLevels = init->AutoMipMap ? 0 : 1;
			desc.ArraySize = 1;
			desc.Format = preInit.DxgiFormat;
			desc.SampleDesc.Count = 1;
			desc.SampleDesc.Quality = 0;
			desc.Usage = D3D11_USAGE_DEFAULT;
			desc.BindFlags = init->AutoMipMap ?
				// TODO: why render target for mipMaps?
				D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET :
				D3D11_BIND_SHADER_RESOURCE;
			desc.CPUAccessFlags = 0;
			desc.MiscFlags = init->AutoMipMap ? D3D11_RESOURCE_MISC_GENERATE_MIPS : 0;

			D3D11_SUBRESOURCE_DATA initData;
			initData.pSysMem = cpuMemory;
			initData.SysMemPitch = static_cast<UINT>( preInit.RowPitch );
			initData.SysMemSlicePitch = static_cast<UINT>( preInit.ImageSizeInBytes );

			LoadGPUResource2D( init, preInit, desc, initData, &dxTexture, &dxResource );

			if ( dxTexture == nullptr || dxResource == nullptr )
			{
				Process2DQueue( init, TextureRequestError::TEXTURE_ERROR_GPU_LOAD_FAIL );
				return;
			}

#if defined(_DEBUG)
			dxTexture->SetPrivateData( WKPDID_D3DDebugObjectName,
				sizeof( "Texture2D" ) - 1,
				"Texture2D" );
#endif
		}

		if ( loadToCpu( init->TextureLoadTarget ) == false )
		{
			cpuMemory = nullptr;
		}

		LoadedTexture storedData =
		{
			init->Filepath,
			dxResource,
			dxTexture,
			cpuMemory,
			preInit.ImageSizeInBytes,
			preInit.NativeWidth,
			preInit.NativeHeight,
			0, // TODO
			preInit.LoadWidth,
			preInit.LoadHeight,
			0, // TODO
			TextureType::TEXTURE_TYPE_2D
		};

		_textureList.Append( KeyValuePair< TextureType, TextureInit* >( TextureType::TEXTURE_TYPE_2D, init ), storedData );

		Process2DQueue( init, storedData );
	}

	// ================ 3D Loading ================
	// ============================================

	// load the argued 3D texture data to the gpu
	void TextureManager::LoadGPUResource3D(
		_In_ const TextureInit* const init,
		_In_ const PreInitOutput& preInit,
		_In_ D3D11_TEXTURE3D_DESC& description,
		_In_ D3D11_SUBRESOURCE_DATA& data,
		_Out_ ID3D11Texture3D** texture,
		_Out_ ID3D11ShaderResourceView** resource )
	{
		HRESULT result = Renderer::Instance( )
			->GetGraphicsLibrary( )
			->GetDevice( )
			->CreateTexture3D( &description, &data, texture );

		if ( FAILED( result ) )
		{
			*texture = nullptr;
			return;
		}

		D3D11_SHADER_RESOURCE_VIEW_DESC resourceDesc;
		memset( &resourceDesc, 0, sizeof( resourceDesc ) );
		resourceDesc.Format = preInit.DxgiFormat;
		resourceDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE3D;
		resourceDesc.Texture3D.MipLevels = init->AutoMipMap ? -1 : 1;

		result = Renderer::Instance( )
			->GetGraphicsLibrary( )
			->GetDevice( )
			->CreateShaderResourceView( *texture, &resourceDesc, resource );

		if ( FAILED( result ) )
		{
			( *texture )->Release( );
			*texture = nullptr;
			*resource = nullptr;
			return;
		}

		if ( init->AutoMipMap == true )
		{
			// TODO: this dereference sucks... fix it somehow
			Renderer::Instance( )
				->GetGraphicsLibrary( )
				->GetDefaultContext( )
				->GetDeviceContext( )
				->UpdateSubresource( *texture, 0, nullptr, data.pSysMem, preInit.RowPitch, preInit.ImageSizeInBytes );

			Renderer::Instance( )
				->GetGraphicsLibrary( )
				->GetDefaultContext( )
				->GetDeviceContext( )
				->GenerateMips( *resource );
		}
	}

	// load a 3D texture from file
	void TextureManager::LoadTexture3D( _In_ TextureInit* const init )
	{
		// TODO: async (based on flag within TextureInit)

		PreInitOutput preInit = PreInitTexture( init );

		if ( preInit.Status != TextureRequestError::TEXTURE_ERROR_NONE )
		{
			Process3DQueue( init, preInit.Status );
			return;
		}

		uint8_t* cpuMemory( anewa( uint8_t, preInit.ImageSizeInBytes ) );
		LoadCPUData( &preInit, &cpuMemory );

		if ( preInit.Status != TextureRequestError::TEXTURE_ERROR_NONE )
		{
			Process3DQueue( init, preInit.Status );
			return;
		}

		if ( init->AutoMipMap == true )
		{
			init->AutoMipMap = ( Renderer::Instance( )->GetFormatSupport( preInit.DxgiFormat )
				& D3D11_FORMAT_SUPPORT_MIP_AUTOGEN) == 0;
		}

		ID3D11Texture3D* dxTexture = nullptr;
		ID3D11ShaderResourceView* dxResource = nullptr;

		if ( loadToGpu( init->TextureLoadTarget ) == true )
		{
			// TODO: research any possible optimizations or automations here
			D3D11_TEXTURE3D_DESC desc;
			desc.Width = preInit.LoadWidth;
			desc.Height = preInit.LoadHeight;
			desc.Depth = 255; // TODO: uh...
			desc.MipLevels = init->AutoMipMap ? 0 : 1;
			desc.Format = preInit.DxgiFormat;
			desc.Usage = D3D11_USAGE_DEFAULT;
			desc.BindFlags = init->AutoMipMap ?
				// TODO: why render target for mipMaps?
				D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET :
				D3D11_BIND_SHADER_RESOURCE;
			desc.CPUAccessFlags = 0;
			desc.MiscFlags = init->AutoMipMap ? D3D11_RESOURCE_MISC_GENERATE_MIPS : 0;

			D3D11_SUBRESOURCE_DATA initData;
			initData.pSysMem = cpuMemory;
			initData.SysMemPitch = static_cast<UINT>( preInit.RowPitch );
			initData.SysMemSlicePitch = static_cast<UINT>( preInit.ImageSizeInBytes );

			LoadGPUResource3D( init, preInit, desc, initData, &dxTexture, &dxResource );

			if ( dxTexture == nullptr || dxResource == nullptr )
			{
				Process3DQueue( init, TextureRequestError::TEXTURE_ERROR_GPU_LOAD_FAIL );
				return;
			}

#if defined(_DEBUG)
			dxTexture->SetPrivateData( WKPDID_D3DDebugObjectName,
				sizeof( "Texture3D" ) - 1,
				"Texture3D" );
#endif
		}

		if ( loadToCpu( init->TextureLoadTarget ) == false )
		{
			// TODO: free memory block manually through allocator
			cpuMemory = nullptr;
		}

		LoadedTexture storedData =
		{
			init->Filepath,
			dxResource,
			dxTexture,
			cpuMemory,
			preInit.ImageSizeInBytes,
			preInit.NativeWidth,
			preInit.NativeHeight,
			0,
			preInit.LoadWidth,
			preInit.LoadHeight,
			0,
			TextureType::TEXTURE_TYPE_3D
		};

		_textureList.Append( KeyValuePair< TextureType, TextureInit* >( TextureType::TEXTURE_TYPE_3D, init ), storedData );

		Process3DQueue( init, storedData );
	}

#pragma endregion

#pragma region QueueProcesses

	// Add a requester to the 1D queue
	void TextureManager::AddTo1DQueue( _In_ TextureInit* const key,
		Function< void, Texture1D* > onSuccess,
		Function< void, TextureRequestError > onFail )
	{
		bool found = false;
		KeyValuePair< TextureInit*, DynamicCollection< TextureLoader< Texture1D >* > >* pair;

		for ( UInt index = 0; index < _1DQueue.Count( ); index++ )
		{
			pair = &_1DQueue.ElementAt( index );
			if ( pair->Key == key || *pair->Key == *key )
			{
				pair->Value.Append( anew( TextureLoader< Texture1D > ) ( key, onSuccess, onFail ) );
				found = true;
				break;
			}
		}

		if ( found == false )
		{
			DynamicCollection< TextureLoader< Texture1D >* > requesterList;
			requesterList.Append( anew( TextureLoader< Texture1D > ) ( key, onSuccess, onFail ) );
			_1DQueue.Append( key, requesterList );
		}
	}

	// Add a requester to the 2D queue
	void TextureManager::AddTo2DQueue( _In_ TextureInit* const key,
		Function< void, Texture2D* > onSuccess,
		Function< void, TextureRequestError > onFail )
	{
		bool found = false;
		KeyValuePair< TextureInit*, DynamicCollection< TextureLoader< Texture2D >* > >* pair;

		for ( UInt index = 0; index < _2DQueue.Count( ); index++ )
		{
			pair = &_2DQueue.ElementAt( index );
			if ( pair->Key == key || *pair->Key == *key )
			{
				pair->Value.Append( anew( TextureLoader< Texture2D > )( key, onSuccess, onFail ) );
				found = true;
				break;
			}
		}

		if ( found == false )
		{
			DynamicCollection< TextureLoader< Texture2D >* > requesterList;
			requesterList.Append( anew( TextureLoader< Texture2D > )( key, onSuccess, onFail ) );
			_2DQueue.Append( key, requesterList );
		}
	}

	// Add a requester to the 3D queue
	void TextureManager::AddTo3DQueue( _In_ TextureInit* const key,
		_In_ Function< void, Texture3D* > onSuccess,
		_In_ Function< void, TextureRequestError > onFail )
	{
		bool found = false;
		KeyValuePair< TextureInit*, DynamicCollection< TextureLoader< Texture3D >* > >* pair;

		for ( UInt index = 0; index < _3DQueue.Count( ); index++ )
		{
			pair = &_3DQueue.ElementAt( index );
			if ( pair->Key == key || *pair->Key == *key )
			{
				pair->Value.Append( anew( TextureLoader< Texture3D > ) ( key, onSuccess, onFail ) );
				found = true;
				break;
			}
		}

		if ( found == false )
		{
			DynamicCollection< TextureLoader< Texture3D >* > requesterList;
			requesterList.Append( anew( TextureLoader< Texture3D > ) ( key, onSuccess, onFail ) );
			_3DQueue.Append( key, requesterList );
		}

		pair = nullptr;
	}

	// callback to those in the 1D queue waiting for the argued texture
	void TextureManager::Process1DQueue( _In_ const TextureInit* const key, _In_ LoadedTexture& tex )
	{
		bool found = false;
		KeyValuePair< TextureInit*, DynamicCollection< TextureLoader< Texture1D >* > >* pair;

		for ( UInt index = 0; index < _1DQueue.Count( ); index++ )
		{
			pair = &_1DQueue.ElementAt( index );
			if ( pair->Key == key || *pair->Key == *key )
			{
				pair->Value.ForEach( [&tex] ( _In_ TextureLoader< Texture1D >* loader )
				{
					loader->OnSuccessCallback( anew( Texture1D )( &tex ) );
				} );

				pair = nullptr;

				_1DQueue.RemoveAt( index );

				found = true;

				break;
			}
		}

		if ( found == false )
		{
			LogError( "Process1DQueue(Success) - M.I.A. Requester!" );
		}

		pair = nullptr;
	}

	// callback to those in the 1D queue informing the given texture has failed
	void TextureManager::Process1DQueue( _In_ const TextureInit* const key, _In_ TextureRequestError error )
	{
		bool found = false;
		KeyValuePair< TextureInit*, DynamicCollection< TextureLoader< Texture1D >* > >* pair;

		for ( UInt index = 0; index < _1DQueue.Count( ); index++ )
		{
			pair = &_1DQueue.ElementAt( index );
			if ( pair->Key == key || *pair->Key == *key )
			{
				pair->Value.ForEach( [error] ( _In_ TextureLoader< Texture1D >* loader )
				{
					if ( loader->OnFailCallback.HasValue() == true )
					{
						loader->OnFailCallback( error );
					}
				} );

				pair = nullptr;

				_1DQueue.RemoveAt( index );

				found = true;

				break;
			}
		}

		if ( found == false )
		{
			LogError( "Process1DQueue(Fail) - M.I.A. Requester!" );
		}

		pair = nullptr;
	}

	// callback to those in the 2D queue waiting for the argued texture
	void TextureManager::Process2DQueue( _In_ const TextureInit* const key, _In_ LoadedTexture& tex )
	{
		bool found = false;
		KeyValuePair< TextureInit*, DynamicCollection< TextureLoader< Texture2D >* > >* pair;

		for ( UInt index = 0; index < _2DQueue.Count( ); index++ )
		{
			pair = &_2DQueue.ElementAt( index );
			if ( pair->Key == key || *pair->Key == *key )
			{
				pair->Value.ForEach( [&tex] ( _In_ TextureLoader< Texture2D >* loader )
				{
					loader->OnSuccessCallback( anew( Texture2D )( &tex ) );
				} );

				pair = nullptr;

				_2DQueue.RemoveAt( index );

				found = true;

				break;
			}
		}

		if ( found == false )
		{
			LogError( "Process2DQueue(Success) - M.I.A. Requester!" );
		}

		pair = nullptr;
	}

	// callback to those in the 2D queue informing the given texture has failed
	void TextureManager::Process2DQueue( _In_ const TextureInit* const key, _In_ TextureRequestError error )
	{
		bool found = false;
		KeyValuePair< TextureInit*, DynamicCollection< TextureLoader< Texture2D >* > >* pair;

		for ( UInt index = 0; index < _2DQueue.Count( ); index++ )
		{
			pair = &_2DQueue.ElementAt( index );
			if ( pair->Key == key || *pair->Key == *key )
			{
				pair->Value.ForEach( [error] ( _In_ TextureLoader< Texture2D >* loader )
				{
					if ( loader->OnFailCallback.HasValue() == true )
					{
						loader->OnFailCallback( error );
					}
				} );

				pair = nullptr;

				_2DQueue.RemoveAt( index );

				found = true;

				break;
			}
		}

		if ( found == false )
		{
			LogError( "Process2DQueue(Fail) - M.I.A. Requester!" );
		}

		pair = nullptr;
	}

	// callback to those in the 3D queue waiting for the argued texture
	void TextureManager::Process3DQueue( _In_ const TextureInit* const key, _In_ LoadedTexture& tex )
	{
		bool found = false;
		KeyValuePair< TextureInit*, DynamicCollection< TextureLoader< Texture3D >* > >* pair;

		for ( UInt index = 0; index < _3DQueue.Count( ); index++ )
		{
			pair = &_3DQueue.ElementAt( index );
			if ( pair->Key == key || *pair->Key == *key )
			{
				pair->Value.ForEach( [&tex] ( _In_ TextureLoader< Texture3D >* loader )
				{
					loader->OnSuccessCallback( anew( Texture3D )( &tex ) );
				} );

				pair = nullptr;

				_3DQueue.RemoveAt( index );

				found = true;

				break;
			}
		}

		if ( found == false )
		{
			LogError( "Process3DQueue(Success) - M.I.A. Requester!" );
		}

		pair = nullptr;
	}

	// callback to those in the 3D queue informing the given texture has failed
	void TextureManager::Process3DQueue( _In_ const TextureInit* const key, _In_ TextureRequestError error )
	{
		bool found = false;
		KeyValuePair< TextureInit*, DynamicCollection< TextureLoader< Texture3D >* > >* pair;

		for ( UInt index = 0; index < _3DQueue.Count( ); index++ )
		{
			pair = &_3DQueue.ElementAt( index );
			if ( pair->Key == key || *pair->Key == *key )
			{
				pair->Value.ForEach( [error] ( _In_ TextureLoader< Texture3D >* loader )
				{
					if ( loader->OnFailCallback.HasValue() == true )
					{
						loader->OnFailCallback( error );
					}
				} );

				pair = nullptr;

				_3DQueue.RemoveAt( index );

				found = true;

				break;
			}
		}

		if ( found == false )
		{
			LogError( "Process3DQueue(Fail) - M.I.A. Requester!" );
		}
	}

#pragma endregion

#pragma region Miscelleny

	// get how many bytes of texture data is loaded on the GPU
	UInt TextureManager::TotalGPUBytesLoaded( _Out_opt_ UInt* const totalTextures ) const
	{
		UInt total = 0;

		_textureList.ForEach( [&total, &totalTextures] ( _In_ TexturePair pair )
		{
			if ( pair.Value.Texture != nullptr )
			{
				total += pair.Value.SizeInBytes;
				*totalTextures += 1;
			}
		} );

		return total;
	}

	// get how many bytes of texture data is loaded on the CPU
	UInt TextureManager::TotalCPUBytesLoaded( _Out_opt_ UInt* const totalTextures ) const
	{
		UInt total = 0;

		_textureList.ForEach( [&total, &totalTextures] ( _In_ TexturePair pair )
		{
			if ( pair.Value.CpuMemory != nullptr )
			{
				total += pair.Value.SizeInBytes;
				*totalTextures += 1;
			}
		} );

		return total;
	}

	// get how many bytes of teture data is currently loaded
	UInt TextureManager::TotalBytesLoaded( _Out_opt_ UInt* const totalTextures ) const
	{
		UInt total = 0;

		_textureList.ForEach( [&total, &totalTextures] ( _In_ TexturePair pair )
		{
			total += pair.Value.SizeInBytes;
			*totalTextures += 1;
		} );

		return total;
	}

	// get a readable informational string
	Text TextureManager::GetLog( ) const
	{
		UInt totalTextures = 0;
		UInt totalMemory = TotalBytesLoaded( &totalTextures );

		UInt totalGpuTextures = 0;
		UInt totalGpuMemory = TotalGPUBytesLoaded( &totalGpuMemory );

		UInt totalCpuTextures = 0;
		UInt totalCpuMemory = TotalCPUBytesLoaded( &totalCpuTextures );

		// TODO: TextBuilder
		Text output = "TextureManager Stats | Totals: (";
		output += totalTextures;
		output += "/";
		output += totalMemory;
		output += ") | GPU: (";
		output += totalGpuTextures;
		output += "/";
		output += totalGpuMemory;
		output += ")";
		output += ") | CPU: (";
		output += totalCpuTextures;
		output += "/";
		output += totalCpuMemory;
		output += ")";

		return output;
	}

	// translate error to message
	Text TextureManager::TranslateError( _In_ const TextureRequestError error )
	{
		Text output;

		switch ( error )
		{
		case TextureRequestError::TEXTURE_ERROR_NONE:
			output = "No error has occurred.";
			break;

		case TextureRequestError::TEXTURE_ERROR_BAD_FILEPATH:
			output = "The argued filepath is null, empty, or not existent.";
			break;

		case TextureRequestError::TEXTURE_ERROR_FACTORY_UNINITIALIZED:
			output = "TextureManager is not initialized.";
			break;

		case TextureRequestError::TEXTURE_ERROR_ZERO_DIMENSIONS:
			output = "The argued file does not contain any pixels.";
			break;

		case TextureRequestError::TEXTURE_ERROR_FRAME_FAIL:
			output = "Unable to decode an image frame.";
			break;

		case TextureRequestError::TEXTURE_ERROR_COPY_PIXEL_FAIL:
			output = "Unable to decode pixels to memory.";
			break;

		case TextureRequestError::TEXTURE_ERROR_CONVERSION_FAIL:
			output = "Failed to convert image to a supported type.";
			break;

		case TextureRequestError::TEXTURE_ERROR_DIMENSION_INQUIRE_FAILED:
			output = "Unable to determine image dimensions.";
			break;

		case TextureRequestError::TEXTURE_ERROR_GPU_LOAD_FAIL:
			output = "Failed to load image memory to GPU";
			break;

		case TextureRequestError::TEXTURE_ERROR_RESIZE_FAIL:
			output = "Failed to scale image to the requested size.";
			break;

		case TextureRequestError::TEXTURE_ERROR_UNSOPPORTED_FORMAT:
			output = "Hardware is unable to support the image type.";
			break;

		case TextureRequestError::TEXTURE_ERROR_UNSUPPORTED_PIXEL_TYPE:
			output = "Image was created with an un-supported pixel type.";
			break;

		case TextureRequestError::TEXTURE_ERROR_PALLETE_FAIL:
			output = "Factory was unable to create a color pallete.";
			break;

		case TextureRequestError::TEXTURE_ERROR_UNKNOWN:
		default:
			output = "An unknown error has occurred.";
			break;
		}

		return output;
	}

#pragma endregion

}