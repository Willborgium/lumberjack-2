/**
 * DrawableContainer.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\DrawableContainer.h>

namespace HyJynxRenderer
{

#pragma region init

	// null ctor
	DrawableContainer::DrawableContainer( )
	{ }

	// copy ctor
	DrawableContainer::DrawableContainer( _In_ const DrawableContainer& other )
		: _drawableList( other._drawableList )
	{ }

	// move ctor
	DrawableContainer::DrawableContainer( _In_ const DrawableContainer&& other )
		: _drawableList( other._drawableList )
	{ }

	// dtor
	DrawableContainer::~DrawableContainer( )
	{
		_drawableList.Clear( );
	}

#pragma endregion

#pragma region API

	// Add a drawable to this container
	void DrawableContainer::AddChild( _In_ IDrawable* const drawable )
	{
		if ( drawable != nullptr )
		{
			_drawableList.Append( drawable );
		}
	}

	// Add all drawables from another container to this one
	void DrawableContainer::AddChild( _In_ DrawableContainer* const container )
	{
		if ( container != nullptr )
		{
			container->ForEach( [ this ] ( _In_ IDrawable* drawable )
			{
				if ( drawable != nullptr )
				{
					_drawableList.Append( drawable );
				}
			} );
		}
	}

	// Remove a child from the container
	void DrawableContainer::RemoveChild( _In_ IDrawable* const drawable )
	{
		if ( drawable != nullptr )
		{
			_drawableList.Remove( drawable );
		}
	}

	// iterate the list of drawables
	void DrawableContainer::ForEach( std::function< void( _In_ IDrawable* ) > cb )
	{
		if ( cb != nullptr )
		{
			_drawableList.ForEach( cb );
		}
	}

	// get on-screen transformation
	Transform* const DrawableContainer::GetTransform( )
	{
		return &_transform;
	}

#pragma endregion

#pragma region Operators

	// append a drawable
	DrawableContainer& DrawableContainer::operator += ( _In_ IDrawable* drawable )
	{
		AddChild( drawable );
		return *this;
	}

	// append a container's drawables
	DrawableContainer& DrawableContainer::operator += ( _In_ DrawableContainer* container )
	{
		AddChild( container );
		return *this;
	}

	// remove a drawable
	DrawableContainer& DrawableContainer::operator -= ( _In_ IDrawable* drawable )
	{
		RemoveChild( drawable );
		return *this;
	}


#pragma endregion
}