/**
 * FBXSceneIO.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\FBXSceneIO.h>

using namespace HyJynxCore;
using namespace std;

namespace HyJynxRenderer
{
#pragma region ctorDtor

	// null ctor
	FBXSceneIO::FBXSceneIO( )
	{ }

	// copy ctor
	FBXSceneIO::FBXSceneIO( _In_ const FBXSceneIO& other )
	{ }

	// move ctor
	FBXSceneIO::FBXSceneIO( _In_ const FBXSceneIO&& other )
	{ }

	// dtor
	FBXSceneIO::~FBXSceneIO( )
	{ }

#pragma endregion

#pragma region ISceneIO implementations

	// Initialize the scene from a file
	void FBXSceneIO::InitializeFromFile( _In_ Text filepath,
		_In_ Function< void, Window* > onSuccess,
		_In_ Function< void, Text > onFail )
	{
		FbxManager* manager = FbxManager::Create( );

		FbxIOSettings* settings = FbxIOSettings::Create( manager, IOSROOT );
		manager->SetIOSettings( settings );

		FbxImporter* importer = FbxImporter::Create( manager, "" ); // second argument??

		// ... 
	}

	void FBXSceneIO::InitializeFromData( _In_ char* const data,
		_In_ const UInt size,
		_In_ Function< void, Window* > onSuccess,
		_In_ Function< void, Text > onFail )
	{
		// TODO
	}

	// Saves the scene to file
	void FBXSceneIO::SaveToFile( _In_ Text filepath,
		_In_ Window* const window,
		_In_ Function< void > onSuccess,
		_In_ Function< void, Text > onFail )
	{
		// TODO
	}

#pragma endregion
}