/**
 * VertexBuffer.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\VertexBuffer.h>

using namespace DirectX;

namespace HyJynxRenderer
{
	// null ctor
	VertexBuffer::VertexBuffer( )
	{ }

	// default ctor
	VertexBuffer::VertexBuffer( _In_ const VertexDescription& desc, _In_ ID3D11Buffer* buffer, _In_ UInt count )
		: _description( desc ),
		_buffer( buffer ),
		_count( count )
	{ }

	// copy ctor
	VertexBuffer::VertexBuffer( _In_ const VertexBuffer& other )
		: _description( other._description ),
		_buffer( other._buffer ),
		_count( other._count )
	{ }

	// move ctor
	VertexBuffer::VertexBuffer( _In_ const VertexBuffer&& other )
		: _description( other._description ),
		_buffer( other._buffer ),
		_count( other._count )
	{ }

	// dtor
	VertexBuffer::~VertexBuffer( )
	{
		_buffer = nullptr;
		_count = 0;
	}

	// get verticies type
	VertexDescription& VertexBuffer::GetDescription( )
	{
		return _description;
	}

	// buffer retrieval
	ID3D11Buffer* VertexBuffer::GetBuffer( ) const
	{
		return _buffer;
	}

	// get the amount of verticies contained
	UInt VertexBuffer::GetCount( ) const
	{
		return _count;
	}

	// get size in bytes
	UInt VertexBuffer::GetSizeInBytes( ) const
	{
		return _count * _description.Size;
	}
}