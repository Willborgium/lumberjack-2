/**
* Point2.cpp
* (c) 2014 All Rights Reserved
*/

#include <Renderer\Headers\ljrc.h>
#include <Renderer\Headers\Point2.h>

namespace HyJynxRenderer
{
#pragma region init

	// null ctor
	Point2::Point2( )
	{ }

	// initialize all components to the argued value
	Point2::Point2( _In_ int val )
		: X( val )
	{ }

	// initialize all components seperately
	Point2::Point2( _In_ int x, _In_ int y )
		: X( x ), Y( y )
	{ }

	// copy ctor
	Point2::Point2( _In_ const Point2& other )
		: X( other.X ), Y( other.Y )
	{ }

	// move ctor
	Point2::Point2( _In_ const Point2&& other )
		: X( other.X ), Y( other.Y )
	{ }

	// dtor
	Point2::~Point2( )
	{ }

#pragma endregion

#pragma region API



#pragma endregion

#pragma region Operators

	// equality operator
	bool Point2::operator == ( _In_ const Point2& other ) const
	{
		return X == other.X
			&& Y == other.Y;
	}

	// inequality operator (emancipation proclimation bitch)
	bool Point2::operator != ( _In_ const Point2& other ) const
	{
		return !( *this == other );
	}

	// compound addition
	Point2& Point2::operator += ( _In_ const Point2& other )
	{
		X += other.X;
		Y += other.Y;
		return *this;
	}

	// compound subtraction
	Point2& Point2::operator -= ( _In_ const Point2& other )
	{
		X -= other.X;
		Y -= other.Y;
		return *this;
	}

	// compound multiplication
	Point2& Point2::operator *= ( _In_ const Point2& other )
	{
		X *= other.X;
		Y *= other.Y;
		return *this;
	}

	// compound division
	Point2& Point2::operator /= ( _In_ const Point2& other )
	{
		X /= other.X != 0 ? other.X : 1;
		Y /= other.Y != 0 ? other.Y : 1;
		return *this;
	}

	// subtraction
	Point2 Point2::operator - ( _In_ const Point2& other ) const
	{
		Point2 out;
		out.X = X - other.X;
		out.Y = Y - other.Y;
		return out;
	}

	// addition
	Point2 Point2::operator + ( _In_ const Point2& other ) const
	{
		Point2 out;
		out.X = X + other.X;
		out.Y = Y + other.Y;
		return out;
	}

	// multiplication
	Point2 Point2::operator * ( _In_ const Point2& other ) const
	{
		Point2 out;
		out.X = X * other.X;
		out.Y = Y * other.Y;
		return out;
	}

	// division
	Point2 Point2::operator / ( _In_ const Point2& other ) const
	{
		Point2 out;
		out.X = X / other.X != 0 ? other.X : 1;
		out.Y = Y / other.Y != 0 ? other.Y : 1;
		return out;
	}

#pragma endregion
}