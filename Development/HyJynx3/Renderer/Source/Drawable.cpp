/**
 * Drawable.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Renderer/Headers/ljrc.h>
#include <Renderer/Headers/Drawable.h>

using namespace HyJynxCore;

namespace HyJynxRenderer
{

#pragma region Init

	// null ctor
	Drawable::Drawable( )
		: IDrawable( )
	{ }

	// copy ctor
	Drawable::Drawable( _In_ const Drawable& other )
		: IDrawable( other ),
		_collision( other._collision ),
		_model( other._model ),
		_drawableId( other._drawableId ),
		_active( other._active ),
		_rasterizerMode( other._rasterizerMode )
	{ }

	// move ctor
	Drawable::Drawable( _In_ const Drawable&& other )
		: IDrawable( other ),
		_collision( other._collision ),
		_model( other._model ),
		_drawableId( other._drawableId ),
		_active( other._active ),
		_rasterizerMode( other._rasterizerMode )
	{ }

	// dtor
	Drawable::~Drawable( )
	{
		_model = nullptr;
		_drawableId = -1;
		_active = false;
	}

#pragma endregion Init


#pragma region IDrawable

	// drawables are constructed with the data they need to load,
	// this kicks off the sequence to load and and register the mesh buffers
	void Drawable::LoadModel( _In_ Function< void > cb )
	{
		cb( );
	}

	// this should be called after a drawable is constructed, and LoadModel() called
	void Drawable::GenerateBounding( )
	{ }

	// per-cycle update call
	void Drawable::Update( _In_ Context*, _In_ const RenderTime )
	{ }

	// when we're in development, this API will help us draw debugging information to the screen
	void Drawable::RenderDebug( _In_ Context* )
	{ }

	// Get the model containing all of the meshes in this drawable
	Model* Drawable::GetModel( )
	{
		return _model;
	}

	// get the container holding all collision descriptions
	CollisionDescription* Drawable::GetCollisionDescription( )
	{
		return &_collision;
	}

	// determine if this drawable is active or not
	bool Drawable::GetActive( ) const
	{
		return _active;
	}

	// set whether this drawable will be active in Renderer's magical eyes
	void Drawable::SetActive( _In_ const bool val )
	{
		_active = val;
	}

	// Get a transform reference used for 3D space manipulation
	Transform* Drawable::GetTransform( )
	{
		return nullptr;
	}

	// get the ID of this drawable (defaults to infinity when no ID is set)
	int Drawable::GetDrawableID( ) const
	{
		return _drawableId;
	}

	// set an identifier to this drawable
	void Drawable::SetDrawableID( _In_ const int val )
	{
		_drawableId = val;
	}

	// sets how the contained meshes will rasterize
	void Drawable::SetRenderMode( _In_ const DrawableRenderMode mode )
	{
		_rasterizerMode = mode;
	}

	// gets how the contained meshes will rasterize
	DrawableRenderMode Drawable::GetRenderMode( ) const
	{
		return _rasterizerMode;
	}

#pragma endregion IDrawable
}