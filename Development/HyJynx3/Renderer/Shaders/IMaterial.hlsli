/**
 * IMaterial.h
 * (c) 2014 All Rights Reserved
 */

//
// API all derived material types must implement
//
interface IMaterial
{
	float3 Diffuse( float2 texCoord );

	float3 Ambient( float2 texCoord );

	int SpecularPower( );
};

//
// Solid color base material
//
class SolidColorMaterial : IMaterial
{
	float3 _color;
	int _specular;

	float3 Diffuse( float2 texCoord )
	{
		return _color;
	}

	float3 Ambient( float2 texCoord )
	{
		return _color; // TODO
	}

	int SpecularPower( )
	{
		return _specular;
	}
};

//
// Textured base material
//
class TexturedMaterial : IMaterial
{
	int _specular;

	float3 Diffuse( float2 texCoord )
	{
		return float3( 1.0f, 0.0f, 0.0f ); // TODO
	}

	float3 Ambient( float2 texCoord )
	{
		return float3( 1.0f, 0.0f, 0.0f ); // TODO
	}

	int SpecularPower( )
	{
		return _specular;
	}
};

//
// plastic material with a solid color
//
class PlasticColorMaterial : SolidColorMaterial
{
	float3 Diffuse( float2 texCoord )
	{
		return float3( 1.0f, 0.0f, 0.0f ); // TODO
	}

	float3 Ambient( float2 texCoord )
	{
		return float3( 1.0f, 0.0f, 0.0f ); // TODO
	}
};

//
// plastic material with a textured color
//
class TexturedPlasticMaterial : TexturedMaterial
{
	float3 Diffuse( float2 texCoord )
	{
		return float3( 1.0f, 0.0f, 0.0f ); // TODO
	}

	float3 Ambient( float2 texCoord )
	{
		return float3( 1.0f, 0.0f, 0.0f ); // TODO
	}
};