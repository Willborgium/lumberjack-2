/**
 * VSUnProjected.hlsl
 * (c) 2014 All Rights Reserved
 */

#include "HyJynxConstantBuffer.hlsli"


// input vertex structure
struct VS_Input
{
	float3 position : POSITION0;
	float4 color : COLOR0;
	float2 texCoord : TEXCOORD0;
};

// stage output structure
struct VS_Output
{
	float4 color : COLOR0;
	float2 texCoord : TEXCOORD0;
	float4 position : SV_POSITION;
};

// stage entry point
VS_Output vs_unprojected_main( VS_Input input )
{
	VS_Output output;

	output.position = mul( mul( float4( input.position, 1.0f ), WorldMatrix ), OrthographicMatrix );
	output.color = input.color;
	output.texCoord = input.texCoord;

	return output;
}