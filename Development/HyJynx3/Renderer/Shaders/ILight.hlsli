/**
 * ILight.hlsli
 * (c) 2014 All Rights Reserved
 */

//
// Required API for all derived light types to derive from
//
interface ILight
{
	float3 Ambient( float3 vNormal );
	float3 Diffuse( float3 vNormal );
	float3 Specular( float3 vNormal, int specularPower );
};

//
// Ambience Light applied to the global drawable
//
class AmbientLight : ILight
{
	float4 _color;
	bool _enabled;

	float3 Ambient( float3 vNormal )
	{
		return float3( _color.xyz * _enabled );
	}

	float3 Diffuse( float3 vNormal )
	{
		return float3( 0.0f, 0.0f, 0.0f ); 
	}

	float3 Specular( float3 vNormal, int specularPower )
	{
		return float3( 0.0f, 0.0f, 0.0f );
	}
};

//
// Applies a gradient to the ambience lighting from pixel normal up to down
//
class HemiAmbientLight : AmbientLight
{
	float4 _skyColor;
	float4 _groundColor;
	float4 _upDirection;

	float3 Ambient( float3 normal )
	{
		float thetha = ( dot( normal.xyz, _upDirection.xyz ) + 1.0f ) / 2.0f;
		return  lerp( _groundColor.xyz, _color.xyz, thetha ) * _enabled;
	}

	float3 Diffuse( float3 vNormal )
	{
		return float3( 0.0f, 0.0f, 0.0f );
	}

	float3 Specular( float3 vNormal, int specularPower )
	{
		return float3( 0.0f, 0.0f, 0.0f );
	}
};

//
// implements an omni-directional light from a location
//
class OmniLight : AmbientLight
{
	float3 _position;
	float _radius;

	float3 Ambient( float3 normal )
	{
		return float3( 1.0f, 0.0f, 0.0f ); // TODO
	}

	float3 Diffuse( float3 vNormal )
	{
		return float3( 1.0f, 0.0f, 0.0f ); // TODO
	}

	float3 Specular( float3 vNormal, int specularPower )
	{
		return float3( 1.0f, 0.0f, 0.0f ); // TODO
	}

};

//
// implements a directional light from a position.
// has a length stored in _directionLength.w, and cone stored in _position.w
//
class DirectionalLight : AmbientLight
{
	float4 _direction;
	float4 _position;

	float3 Ambient( float3 normal )
	{
		return float3( 0.0f, 0.0f, 0.0f ); // TODO? - not really needed, won't be called
	}

	float3 Diffuse( float3 normal )
	{
		float lambert = saturate( dot( normal, _direction.xyz ) );
		return ( ( float3 )lambert * _color.xyz * _enabled );
	}

	float3 Specular( float3 normal, int specularPower )
	{
		float3 H = -normalize( CameraVector.xyz) + _direction.xyz;
		float3 halfAngle = normalize( H );
		float specular = pow( max( 0, dot( halfAngle, normalize( normal ) ) ), specularPower );

		return ( ( float3 )specular * _color.xyz * _enabled );
	}
};