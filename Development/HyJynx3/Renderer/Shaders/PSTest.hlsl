/**
 * PSTest.hlsl
 * (c) 2014 All Rights Reserved
 */

#include "HyJynxConstantBuffer.hlsli"

struct PS_Input
{
	float4 color : COLOR0;
	float2 texCoord : TEXCOORD0;
};

float4 ps_test_main( PS_Input input ) : SV_TARGET
{
	return input.color;
}