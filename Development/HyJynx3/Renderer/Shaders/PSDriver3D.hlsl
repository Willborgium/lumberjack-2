/**
 * PSDriver3D.hlsl
 * (c) 2014 All Rights Reserved
 */

#include "HyJynxConstantBuffer.hlsli"
#include "IMaterial.hlsli"
#include "ILight.hlsli"

//
// pixel shader interpolated input
//
struct DriverInput
{
	float3 Normal : NORMAL0;
	float2 TexCoord : TEXCOORD0;
};

// -- Dynamic Shader Linkage --
//		Permutation options
cbuffer cbPerPrimitive : register( b4 )
{
	SolidColorMaterial				MaterialSolidColor;
	TexturedMaterial				MaterialTextured;
	PlasticColorMaterial			MaterialPlasticSolidColor;
	TexturedPlasticMaterial			MaterialPlasticTextured;
};

// -- Dynamic Shader Linkage --
//     Permutation classes
IMaterial Material;
ILight AmbientLighting;
ILight DirectLighting;
ILight EnvironmentLighting;

// entry point
float4 ps_driver3D_main( DriverInput input ) : SV_TARGET
{
	float3 ambient = float3( 0.0f, 0.0f, 0.0f );
	ambient = Material.Ambient( input.TexCoord ) * AmbientLighting.Ambient( input.Normal );

	float3 diffuse = float3( 0.0f, 0.0f, 0.0f );
	diffuse += Material.Diffuse( input.TexCoord ) * DirectLighting.Diffuse( input.Normal );

	float3 specular = float3( 0.0f, 0.0f, 0.0f );
	specular += DirectLighting.Specular( input.Normal, Material.SpecularPower( ) );
	specular += EnvironmentLighting.Specular( input.Normal, Material.SpecularPower( ) );

	float3 Lighting = saturate( ambient + diffuse + specular );
	return float4( Lighting, 1.0f ); // TODO: use object's suggested opacity (from LOD system)
}