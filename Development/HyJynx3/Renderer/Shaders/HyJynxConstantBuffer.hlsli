/**
 * HyJynxConstantBuffer.hlsli
 * (c) 2014 All Rights Reserved
 */


//
// projection matrix constant buffer, 
// only updates for re-size and aspect ratio changes
//
cbuffer	CameraProjection : register( b0 )
{
	matrix ProjectionMatrix;
	matrix OrthographicMatrix;
	float4 ScreenSize_HalfPixel;
};

//
// per-cycle camera values, 
// only updates when changes occur to the camera pre-cycle
//
cbuffer Camera : register( b1 )
{
	matrix ViewMatrix;
	float4 CameraPosition;
	float4 CameraVector;
};

//
// per-cycle run-time vlaues, 
// always updates once pre-cycle
//
cbuffer Cycle : register( b2 )
{
	float4 Time; // x: elapsed ms, y: total ms, z:w: reserved for future use
};


//
// per-object values,
// updates for every object being drawn
//
cbuffer Object : register( b3 )
{
	matrix WorldMatrix;
};



Texture2D DiffuseTexture : register( t0 );
SamplerState DiffuseSampler : register( s0 );

Texture2D NormalTexture : register( t1 );
SamplerState NormalSampler : register( s1 );

TextureCube EnvironmentTexture : register( t2 );
SamplerState EnvironmentSampler : register( s2 );