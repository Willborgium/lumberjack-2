/**
 * VSProjected.hlsl
 * (c) 2014 All Rights Reserved
 */

#include "HyJynxConstantBuffer.hlsli"

struct VSInput
{
	float3 Position : POSITION0;
	float3 Normal : NORMAL0;
	float2 TexCoord : TEXCOORD0;
};

struct VSOutput
{
	float4 Position : SV_POSITION;
	float3 Normal : NORMAL0;
	float2 TexCoord : TEXCOORD0;
};

VSOutput vs_projected_main( VSInput input )
{
	VSOutput output;

	output.Position = mul( mul( mul( float4( input.Position, 1.0f ), WorldMatrix ), ViewMatrix ), ProjectionMatrix );
	output.Normal = input.Normal;
	output.TexCoord = input.TexCoord;

	return output;
}