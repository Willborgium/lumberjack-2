
#include <Engine\Headers\EntryPoint.h>
#include <EntryPoint\Headers\Main.h>

#include <Tests\Header\WillsTestState.h>
#include <Tests\Header\DansPlayground.h>
#include <Tests\Header\QuadTest.h>
#include <Tests\Header\CubeTest.h>
#include <Tests\Header\TextureTest.h>
#include <Tests\Header\AnimatorTest.h>
#include <Tests\Header\SceneTests.h>

bool AppMain(DynamicCollection<Data*>& input)
{
	auto x = sizeof( HyJynxMemoryManagement::AllocatorInfo );
	//DispatchingSystem::Instance ( )->AppendSystem ( new WillsTestState ( ) );
	//DispatchingSystem::Instance( )->AppendSystem( anew( DansPlayground )( ) );
	DispatchingSystem::Instance( )->AppendSystem( anew( QuadTest )( ) );
	//DispatchingSystem::Instance( )->AppendSystem( new TextureTest( ) );
	//DispatchingSystem::Instance( )->AppendSystem( new CubeTest( ) );
	//DispatchingSystem::Instance( )->AppendSystem( new AnimatorTest( ) );
	//DispatchingSystem::Instance( )->AppendSystem( new SceneTests( ) );
	return true;
}