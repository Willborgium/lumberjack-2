#include <Engine\Headers\Core.h>
using namespace HyJynxCore;

#include <Engine\Headers\Utilities.h>
using namespace HyJynxUtilities;

#include <Engine\Headers\Collections.h>
using namespace HyJynxCollections;

#include <Renderer\Headers\RendererCore.h>
using namespace HyJynxRenderer;

#include <InputEngine\Headers\Input.h>
using namespace HyJynxInput;

class WillsTestState : public HyJynxCore::ISystem
{
protected:

	Window _window;

	int localNumber = 0;

public:

	WillsTestState( )
		: ISystem( "WillsTestState" ),
		_window( WindowDrawType::WINDOW_DRAW_2D, "Test Window" )
	{
		int myVal = 5;

		Function<bool, int> a = [ ] ( CaptureData& _this, int y )
		{
			return y > _this[ "number" ].As<int>( );
		};

		a.Capture( "number", myVal );

		bool r = a( 10 );
	}

	virtual bool Initialize( ) override
	{
		Renderer::Instance( )->SetRenderSequence( anew( DiffuseRenderSequence ) ( ) );

		DrawableQuad2D* quad = anew( DrawableQuad2D )( );
		_window.AddDrawable( quad );

		auto result = Assets::GetConfigjson( );

		auto tio = Assets::GetTestItOuttxt( );
		
		return true;
	}

	virtual void Update( ) override
	{
		if (localNumber < 5 )
		{
			localNumber++;
			AsyncMethod<int, Text> myMethod( [&] ( Text val )
			{
				Helper::ShowMessage( val );
				return localNumber;
			} );

			myMethod( "HEEEEELO", [=] ( int response )
			{
				Text t = response;
				t += " just finished";
				Helper::ShowMessage( t );
			} );
		}
	}

	virtual bool Uninitialize ( ) override
	{
		HyJynxUtilities::Helper::ShowMessage ( "You've uninitialized the state!" );

		return true;
	}
};