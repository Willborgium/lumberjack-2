/**
 * CubeTest.h
 * (c) 2014 All Rights Reserved
 */

#ifndef CUBE_TEST_H
#define CUBE_TEST_H

#include <Engine\Headers\Core.h>
#include <Renderer\Headers\RendererCore.h>
#include <Logger\Headers\Logger.h>
#include <InputEngine\Headers\Input.h>
#include <stdlib.h>
#include <time.h>

using namespace HyJynxRenderer;
using namespace HyJynxInput;
using namespace HyJynxCore;
using namespace HyJynxUtilities;
using namespace HyJynxLogger;
using namespace std;

class CubeTest : public ISystem
{
protected:

	Window _window;
	Camera _camera;

	DrawableCube3D* _cube = nullptr;
	DrawableCube3DConfiguration _config;

public:

	// ctor
	CubeTest( )
		: ISystem( "Cube Unit Tests" ),
		_window( WindowDrawType::WINDOW_DRAW_3D, "Cube Unit Tests" ),
		_camera( )
	{ }

	// init
	virtual bool Initialize( ) override
	{
		Renderer::Instance( )->SetRenderSequence( anew( DiffuseRenderSequence ) ( ) );
		Renderer::Instance( )->SetActiveCamera( &_camera );
		Renderer::Instance( )->AddWindow( &_window );

		Function< void > nullcb = [ ] ( CaptureData& ) { };

		_config.SetDimensions( 20.0f, 20.0f, 20.0f );
		_config.SetTopColor( Color( 255.0f ) );
		_config.SetBottomColor( Color( 125.0f ) );

		_cube = anew( DrawableCube3D )( &_config );
		_cube->LoadModel( nullcb );
		_cube->GenerateBounding( );

		_window += _cube;

		return true;
	}

	// per-cycle
	virtual void Update( ) override
	{ }

	// release all
	virtual bool Uninitialize( ) override
	{
		return true;
	}
};

#endif // CUBE_TEST_H