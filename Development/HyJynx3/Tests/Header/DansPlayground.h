/**
 * DansPlayground.h
 */

#ifndef DANSPLAYGROUND_STATE_H
#define DANSPLAYGROUND_STATE_H


#include <Engine\Headers\Core.h>
#include <Renderer\Headers\RendererCore.h>
#include <InputEngine\Headers\Input.h>

using namespace HyJynxRenderer;
using namespace HyJynxInput;
using namespace HyJynxCore;
using namespace HyJynxUtilities;
using namespace std;

class DansPlayground : public ISystem
{
protected:

	Window _window = Window( WindowDrawType::WINDOW_DRAW_2D, "Dan's Playground" );
	Camera _camera = Camera( );

public:

	DansPlayground( )
		: ISystem( "Dan's Playground" )
	{ }

	virtual bool Initialize( ) override
	{
		Renderer::Instance( )->SetRenderSequence( anew( DiffuseRenderSequence ) ( ) );
		Renderer::Instance( )->SetActiveCamera( &_camera );
		Renderer::Instance( )->AddWindow( &_window );

		// ...

		return true;
	}

	virtual void Update( ) override
	{ }

	virtual bool Uninitialize( ) override
	{
		return true;
	}
};

#endif // DANSPLAYGROUND_STATE_H