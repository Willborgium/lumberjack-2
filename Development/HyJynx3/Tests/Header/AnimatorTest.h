#ifndef ANIMATORTEST_H
#define ANIMATORTEST_H

#include <Engine\Headers\Core.h>
#include <Renderer\Headers\RendererCore.h>
#include <InputEngine\Headers\Input.h>
#include <InputEngine\Headers\KeyEventHandler.h>
#include <Logger\Headers\Logger.h>

using namespace HyJynxRenderer;
using namespace HyJynxInput;
using namespace HyJynxCore;
using namespace HyJynxUtilities;
using namespace HyJynxLogger;
using namespace std;

class AnimatorTest : public ISystem
{
protected:

	Window _window = Window( WindowDrawType::WINDOW_DRAW_2D, "Animation Tests" );
	Camera _camera = Camera( );
	KeyboardListener*	_kbListener = nullptr;

public:

	AnimatorTest( )
		: ISystem( "Animation Tests" )
	{ }

	virtual bool Initialize( ) override
	{
		Renderer::Instance( )->SetRenderSequence( anew( DiffuseRenderSequence ) ( ) );
		Renderer::Instance( )->SetActiveCamera( &_camera );
		Renderer::Instance( )->AddWindow( &_window );
		Renderer::Instance( )->SetClearColor( anew( Color )( 12.7f, 32.1f, 242.2f ) );

		Function< void > nullcb = [ ] ( CaptureData& ) { };

		DrawableQuad2D* quad = anew( DrawableQuad2D )( );
		quad->LoadModel( nullcb );

		_window += quad;

		TransformState* tState1 = anew( TransformState )( );
		tState1->Position.y = 450;
		tState1->Position.x = 32;

		TransformState* tState2 = anew( TransformState )( );
		tState2->Position.y = 450;
		tState2->Position.x = 32;

		_kbListener = anew( KeyboardListener )( "Animation" );
		AnimationController* animController = nullptr;
		/*
		_kbListener->RegisterHandler( anew( KeyEventHandler )( Keys::Left, KeyState::Released, 
			[&animController, &tState1, &quad] ( KeyState state )
		{
			if ( animController != nullptr )
			{
				animController->Stop( );
			}

			animController = Animator::Instance( )->Animate< TransformState >(
				anew( TransformState )( quad->GetTransform( )->GetState( ) ),
				tState1,
				20000.0,
				[ ] ( )
			{
				Logger::Log( "Animation Completed." );
			},
				[&quad] ( _In_ TransformState* state )
			{
				if ( quad != nullptr )
				{
					quad->GetTransform( )->ToState( *state );
				}
			} );
		} ) );

		_kbListener->RegisterHandler( anew( KeyEventHandler )( Keys::Right, KeyState::Released, 
			[&animController, &tState2, &quad] ( KeyState state )
		{
			if ( animController != nullptr )
			{
				animController->Stop( );
			}

			animController = Animator::Instance( )->Animate< TransformState >(
				anew( TransformState )( quad->GetTransform( )->GetState( ) ),
				tState2,
				20000.0,
				[ ] ( )
			{
				Logger::Log( "Animation Completed." );
			},
				[&quad] ( _In_ TransformState* state )
			{
				if ( quad != nullptr )
				{
					quad->GetTransform( )->ToState( *state );
				}
			} );
		} ) );
		*/
		return true;
	}

	virtual void Update( ) override
	{ }

	virtual bool Uninitialize( ) override
	{
		return true;
	}
};

#endif // ANIMATORTEST_H 