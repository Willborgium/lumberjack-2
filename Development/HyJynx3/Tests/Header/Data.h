//#ifndef DATA_HEADER
//#define DATA_HEADER
//
//#include <iostream>
//using namespace std;
//
//#include "..\..\Engine\Headers\Collections.h"
//#include "..\..\Engine\Headers\Core.h"
//#include "..\..\Engine\Headers\MemoryManagement.h"
//#include "..\..\Engine\Headers\Design.h"
//#include "..\..\Engine\Headers\Scripting.h"
//#include "..\..\Engine\Headers\Utilities.h"
//
//using namespace HyJynxCollections;
//using namespace HyJynxCore;
//using namespace HyJynxMemoryManagement;
//using namespace HyJynxDesign;
//using namespace HyJynxScripting;
//using namespace HyJynxUtilities;
//
//#include <functional>
//using namespace std;
//
//#include <thread>
//
//int TestCount = 0;
//int TestsPassed = 0;
//
//class DataA
//{
//public:
//	int MyNumber;
//	Text Name;
//};
//
//class DataB
//{
//public:
//	Text TData;
//};
//
//class AtoBConverter : public IConverter
//{
//public:
//	virtual void* Convert(void* object)
//	{
//		DataB* output = new DataB();
//
//		DataA* a = static_cast<DataA*>(object);
//
//		output->TData = a->Name + ": ";
//		output->TData += Text(a->MyNumber);
//
//		return output;
//	}
//};
//
//class PrintListener : public IListener
//{
//public:
//	PrintListener(Text name)
//		: IListener(name)
//	{
//	}
//
//	virtual void ReceiveMessage(Text message)
//	{
//		cout << _name << ": " << message << endl;
//	}
//
//	virtual void ReceiveNotification(Data* data)
//	{
//		cout << "Notified of data...\n";
//	}
//};
//
//class Person
//{
//public:
//	Text Name;
//	int Age;
//	float Cash;
//	ICollection<Person*>* Friends;
//	int other;
//};
//
//class JsonToPersonConverter : public IConverter
//{
//public:
//	virtual void* Convert(void* data)
//	{
//		Person* output = new Person;
//		output->Friends = new DynamicCollection<Person*>();
//
//		if (nullptr != data)
//		{
//			JsonObject* properties = static_cast<JsonObject*>(data);
//
//			properties->ForEach([&](JsonProperty prop)
//			{
//				if (prop.Key == "Name")
//				{
//					output->Name = Text(*static_cast<Text*>(prop.Value));
//				}
//				else if (prop.Key == "Age")
//				{
//					output->Age = static_cast<Text*>(prop.Value)->ToInt();
//				}
//				if (prop.Key == "Cash")
//				{
//					output->Cash = (float)static_cast<Text*>(prop.Value)->ToDouble();
//				}
//				else if (prop.Key == "Friends")
//				{
//					IJsonArray* friends = static_cast<JsonArray*>(prop.Value);
//
//					friends->ForEach([&](void* friendVal)
//					{
//						output->Friends->Append((Person*)Convert(friendVal));
//					});
//				}
//			});
//		}
//
//		return output;
//	}
//};
//
//
//#define SETGET( TYPE, FIELD, SETTER, GETTER ) void SETTER( TYPE value ) { FIELD = value; } TYPE GETTER( ) const { return FIELD; }
//
//
//
//
//class Player
//{
//protected:
//	Text _name;
//	int _health;
//	float _cash;
//public:
//	Player()
//	{
//		_name = Text::Empty();
//		_health = 0;
//		_cash = 0.0f;
//	}
//
//	void SetAll(Text n, int h, float c)
//	{
//		_name = n;
//		_health = h;
//		_cash = c;
//	}
//
//	void PrintAll()
//	{
//		cout << _name << " has " << _health << " health and " << _cash << " cash.\n";
//	}
//
//	SETGET(Text, _name, SetName, GetName);
//	SETGET(int, _health, SetHealth, GetHealth);
//	SETGET(float, _cash, SetCash, GetCash);
//};
//
//class Base
//{
//public:
//	virtual void Print() = 0;
//};
//
//class ImplA : public Base
//{
//public:
//	virtual void Print()
//	{
//		cout << "ImplA print\n";
//	}
//};
//
//class ImplB : public Base
//{
//public:
//	virtual void Print()
//	{
//		cout << "ImplB print\n";
//	}
//};
//
//class ImplC : public ImplA
//{
//public:
//	virtual void Print()
//	{
//		cout << "ImplC print\n";
//	}
//};
//
//void PrintSomething(double value)
//{
//	cout << "Oh my gosh " << value << endl;
//}
//
//void MyMethod1()
//{
//	cout << "Method1: No Params" << endl;
//}
//
//void MyMethod2(char aByte)
//{
//	cout << "Method2: " << aByte << endl;
//}
//
//void MyMethod2(int a, char b)
//{
//	cout << "Method3: " << a << "," << b << endl;
//}
//
//auto MyMethod3(int a, char b, int c)
//{
//	cout << "Method3: " << a << "," << b << "," << c << endl;
//
//	Person* p = new Person();
//	p->Age = 14;
//	p->Cash = 117.45;
//	p->Friends = nullptr;
//	p->Name = "John Smithington";
//
//	return p;
//}
//
//
//unsigned int SizeOfParameters()
//{
//	return 0;
//}
//
//template <typename First, typename ... Args>
//unsigned int SizeOfParameters(First val, Args... vals)
//{
//	const unsigned int count = sizeof...(Args);
//
//	unsigned int output = sizeof(First);
//
//	if (count > 0)
//	{
//		output += SizeOfParameters(vals...);
//	}
//
//	return  output;
//}
//
//template <typename... Args>
//unsigned int SizeOfParameters(Args... vals)
//{
//	auto count = sizeof...(Args);
//}
//
//class ServiceA
//{
//public:
//	int value1;
//};
//
//class ServiceB : public ServiceA
//{
//public:
//	int value2;
//};
//
//class ServiceC : public ServiceB
//{
//public:
//	int value3;
//};
//
//
//
//class ServiceALocator
//{
//public:
//	static auto Instance()
//	{
//		return ServiceLocator<ServiceA>::Instance();
//	}
//};
//
//struct TestOne
//{
//	virtual int MethodA(int a) = 0;
//};
//
//struct TestTwo
//{
//	virtual int MethodA(int a) = 0;
//};
//
//class Implm : public TestOne, public TestTwo
//{
//public:
//	virtual int TestOne::MethodA(int a)
//	{
//		cout << "ONE" << a << endl;
//
//		return a;
//	}
//	virtual int TestTwo::MethodA(int a)
//	{
//		cout << "TWO" << a << endl;
//
//		return a;
//	}
//};
//
//struct AT_One
//{
//	int val1, val2;
//	float val3;
//	char val4;
//	AT_One* next;
//
//	AT_One(AT_One* n)
//		: next(n)
//	{
//	}
//
//	AT_One() { }
//
//	AT_One(int a, int b, float c, char d, AT_One* e)
//		: val1(a), val2(b), val3(c), val4(d), next(e)
//	{
//	}
//};
//
//#define STEST(TYPE, name) TYPE::Instance(); TYPE* name = new TYPE();
//
//void MethodA(int x)
//{
//	Text a = Text("Method A: ") + Text(x);
//	Helper::ShowMessage(a);
//}
//
//void MethodB(int x)
//{
//	Text a = Text("Method B: ") + Text(x);
//	Helper::ShowMessage(a);
//}
//
//void MethodC(int x)
//{
//	Text a = Text("Method C: ") + Text(x);
//	Helper::ShowMessage(a);
//}
//
//class Popsicle
//{
//public:
//	int Number;
//	Text Name;
//	double Value;
//};
//

//#endif