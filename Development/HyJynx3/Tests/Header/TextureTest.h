#ifndef TESTS_TEXTURETEST_H
#define TESTS_TEXTURETEST_H

#include <Engine\Headers\Core.h>
#include <Renderer\Headers\RendererCore.h>
#include <InputEngine\Headers\Input.h>
#include <InputEngine\Headers\KeyEventHandler.h>
#include <Logger\Headers\Logger.h>

using namespace HyJynxRenderer;
using namespace HyJynxInput;
using namespace HyJynxCore;
using namespace HyJynxUtilities;
using namespace HyJynxLogger;
using namespace std;

class TextureTest : public ISystem
{
protected:

	Window _window = Window( WindowDrawType::WINDOW_DRAW_2D, "Texture Dev Tests" );
	Camera _camera = Camera( );

public:

	TextureTest( )
		: ISystem( "Texture Dev Tests" )
	{ }

	virtual bool Initialize( ) override
	{
		Renderer::Instance( )->SetRenderSequence( anew( DiffuseRenderSequence ) ( ) );
		Renderer::Instance( )->SetActiveCamera( &_camera );
		Renderer::Instance( )->AddWindow( &_window );
		Renderer::Instance( )->SetClearColor( anew( Color )( 12.7f, 32.1f, 242.2f ) );

		Function< void > nullcb = [ ] ( CaptureData& ) { };

		DrawableQuad2D* quad = anew( DrawableQuad2D )( );
		quad->LoadModel( nullcb );

		_window += quad;

		char* data = Assets::Getbricks_diffusebmp( ).Load( );
		size_t size = Assets::Getbricks_diffusebmp( )._sizeInBytes;
		Text path = Assets::Getbricks_diffusebmp( )._filePath;

		Function< void, Texture2D* > onSuccess = [ ] ( CaptureData& capture, _In_ Texture2D* texture )
		{
			Logger::Log( "Texture Load Successful" );

			DrawableQuad2D* quad = capture[ "quad" ].As<DrawableQuad2D*>( );

			if ( quad->GetModel( ) != nullptr )
			{
				quad->GetModel( )->ApplyDiffuseTexture( texture );
			}

		};

		onSuccess.Capture( "quad", quad );

		Function< void, TextureRequestError > onFail = [ ] ( _In_ CaptureData&, _In_ TextureRequestError error )
		{
			Logger::Log( "Texture Load Failed, error = " );
			Logger::Log( TextureManager::TranslateError( error ) );
		};


		TextureManager::Instance( )->RequestTexture2D( anew( TextureInit )( path, data, size ), onSuccess, onFail );


		return true;
	}

	virtual void Update( ) override
	{ }

	virtual bool Uninitialize( ) override
	{
		return true;
	}
};

#endif // TESTS_TEXTURETEST_H 