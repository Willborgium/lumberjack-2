/**
 * QuadTest.h
 * (c) 2014 All Rights Reserved
 */

#ifndef QUADTEST_STATE_H
#define QUADTEST_STATE_H


#include <Engine\Headers\Core.h>
#include <Renderer\Headers\RendererCore.h>
#include <Logger\Headers\Logger.h>
#include <InputEngine\Headers\Input.h>
#include <Engine\Headers\Design.h>

#include <stdlib.h>
#include <time.h>

using namespace HyJynxRenderer;
using namespace HyJynxInput;
using namespace HyJynxCore;
using namespace HyJynxUtilities;
using namespace HyJynxLogger;
using namespace HyJynxDesign;
using namespace std;

//
// Play with and Test the functionality quads and general 2D drawing
//
class QuadTest : public ISystem
{
protected:

	Window				_window = Window( WindowDrawType::WINDOW_DRAW_2D, "Quad Test Window");
	Camera				_camera = Camera();
	KeyboardListener*	_kbListener = nullptr;

	DrawableQuad2D*		_quad = nullptr;
	DrawableQuad2D*		_leftQuad = nullptr;
	DrawableQuad2D*		_rightQuad = nullptr;

public:

	// ctor
	QuadTest( )
		: ISystem( "Quad Unit Tests" )
	{ }

	// initialize
	virtual bool Initialize( ) override
	{
		Renderer::Instance( )->SetRenderSequence( anew( DiffuseRenderSequence ) ( ) );
		Renderer::Instance( )->SetActiveCamera( &_camera );
		Renderer::Instance( )->AddWindow( &_window );
		Renderer::Instance( )->SetCullMethod( CollisionMethod::NONE );

		Vector2 screen = Renderer::Instance( )->GetScreenSize( );

		Color col;
		col.SetColor( 255.0f, 0, 0 );

		Function< void > nullcb = [ ] ( CaptureData& ) { };

		// middle (and parent)
		DrawableQuad2DConfiguration* config = anew( DrawableQuad2DConfiguration )( );
		config->SetDimensions( 64.0f, 64.0f );
		config->SetVertexColors( col, col, col, col );

		_quad = anew( DrawableQuad2D )( config );
		_quad->GetTransform( )->SetOrigin( 0.5f, 0.5f, 0 );
		_quad->GetTransform( )->SetPosition( screen.x * 0.5f, screen.y * 0.5f, 0 );
		_quad->LoadModel( nullcb );
		_quad->GenerateBounding( );

		_window += _quad;

		col.SetColor( 0, 255.0f, 0 );

		// left (child of middle)
		config = anew( DrawableQuad2DConfiguration )( );
		config->SetDimensions( 64.0f, 64.0f );
		config->SetVertexColors( col, col, col, col );

		_leftQuad = anew( DrawableQuad2D )( config );
		_leftQuad->GetTransform( )->SetOrigin( 0.5f, 0.5f, 0 );
		_leftQuad->GetTransform( )->SetPosition( screen.x * 0.5f - 128, screen.y * 0.5f, 0 );
		_leftQuad->GetTransform( )->SetParent( _quad->GetTransform( ) );
		_leftQuad->LoadModel( nullcb );
		_leftQuad->GenerateBounding( );

		_window += _leftQuad;

		col.SetColor( 0, 0, 255.0f );

		// right (child of middle)
		config = anew( DrawableQuad2DConfiguration )( );
		config->SetDimensions( 64.0f, 64.0f );
		config->SetVertexColors( col, col, col, col );

		_rightQuad = anew( DrawableQuad2D )( config );
		_rightQuad->GetTransform( )->SetOrigin( 0.5f, 0.5f, 0 );
		_rightQuad->GetTransform( )->SetPosition( screen.x * 0.5f + 128, screen.y * 0.5f, 0 );
		_rightQuad->GetTransform( )->SetParent( _quad->GetTransform( ) );
		_rightQuad->LoadModel( nullcb );
		_rightQuad->GenerateBounding( );

		_window += _rightQuad;

		_kbListener = anew( KeyboardListener )( "QuadTester" );

		// translation - right
		Function<void, KeyState> rightCallback = [] ( _In_ CaptureData& capture, _In_ KeyState state )
		{
			( capture[ "quad" ].As<DrawableQuad2D*>( ) )->GetTransform( )->AddPosition( 1.0f, 0, 0 );
		};
		rightCallback.Capture( "quad", _quad );
		_kbListener->RegisterHandler( Keys::Right, KeyState::Down, rightCallback );

		// translation - left
		Function<void, KeyState> leftCallback = [ ]( _In_ CaptureData& capture, _In_ KeyState state )
		{
			( capture[ "quad" ].As<DrawableQuad2D*>( ) )->GetTransform( )->AddPosition( -1.0f, 0, 0 );
		};
		leftCallback.Capture( "quad", _quad );
		_kbListener->RegisterHandler( Keys::Left, KeyState::Down, leftCallback );

		// translation - up
		Function<void, KeyState> upCallback = [ ] ( _In_ CaptureData& capture, _In_ KeyState state )
		{
			( capture[ "quad" ].As<DrawableQuad2D*>( ) )->GetTransform( )->AddPosition( 0, -1.0f, 0 );
		};
		upCallback.Capture( "quad", _quad );
		_kbListener->RegisterHandler( Keys::Up, KeyState::Down, upCallback );

		// translation - down
		Function<void, KeyState> downCallback = [ ] ( _In_ CaptureData& capture, _In_ KeyState state )
		{
			( capture[ "quad" ].As<DrawableQuad2D*>( ) )->GetTransform( )->AddPosition( 0, 1.0f, 0 );
		};
		downCallback.Capture( "quad", _quad );
		_kbListener->RegisterHandler( Keys::Down, KeyState::Down, downCallback );

		// origin		
		Function<void, KeyState> originCallback = [ ]( _In_ CaptureData& capture, _In_ KeyState state )
		{
			DrawableQuad2D* quad = capture[ "quad" ].As<DrawableQuad2D*>( );

			Vector3 origin = quad->GetTransform( )->GetOrigin( );
			if ( origin.x == 0 && origin.y == 0 )
			{
				quad->GetTransform( )->SetOrigin( 1, 0, 0 );
			}
			else if ( origin.x == 1 && origin.y == 0 )
			{
				quad->GetTransform( )->SetOrigin( 1, 1, 0 );
			}
			else if ( origin.x == 1 && origin.y == 1 )
			{
				quad->GetTransform( )->SetOrigin( 0, 1, 0 );
			}
			else if ( origin.x == 0 && origin.y == 1 )
			{
				quad->GetTransform( )->SetOrigin( 0.5f, 0.5f, 0 );
			}
			else if ( origin.x == 0.5f && origin.y == 0.5f )
			{
				quad->GetTransform( )->SetOrigin( 0, 0, 0 );
			}
		};
		originCallback.Capture( "quad", _quad );
		_kbListener->RegisterHandler( Keys::O, KeyState::Pressed, originCallback );

		// scale
		Function<void, KeyState> scaleCallback = [ ] ( _In_ CaptureData& capture, _In_ KeyState state )
		{
			DrawableQuad2D* quad = capture[ "quad" ].As<DrawableQuad2D*>( );

			Vector3 scale = quad->GetTransform( )->GetScale( );
			if ( scale.x == 1 && scale.y == 1 )
			{
				quad->GetTransform( )->SetScale( 2, 1, 1 );
			}
			else if ( scale.x == 2 && scale.y == 1 )
			{
				quad->GetTransform( )->SetScale( 2, 2, 1 );
			}
			else if ( scale.x == 2 && scale.y == 2 )
			{
				quad->GetTransform( )->SetScale( 1, 2, 1 );
			}
			else if ( scale.x == 1 && scale.y == 2 )
			{
				quad->GetTransform( )->SetScale( 1, 1, 1 );
			}
		};
		scaleCallback.Capture( "quad", _quad );
		_kbListener->RegisterHandler( Keys::S, KeyState::Pressed, scaleCallback );

		return true;
	}

	// per-cycle
	virtual void Update( ) override
	{
		_quad->GetTransform( )->AddRotation( 0, 0, 0.5f );
	}

	// unload
	virtual bool Uninitialize( ) override
	{
		_window.Release( );
		_kbListener = nullptr;
		_quad = nullptr;
		_leftQuad = nullptr;
		_rightQuad = nullptr;
		return true;
	}
};

#endif // QUADTEST_STATE_H