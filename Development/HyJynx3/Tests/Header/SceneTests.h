/**
 * SceneTests.h
 * (c) 2014 All Rights Reserved
 */

#ifndef SCENETESTS_H
#define SCENETESTS_H

#include <Engine\Headers\Core.h>
#include <Renderer\Headers\RendererCore.h>
#include <Logger\Headers\Logger.h>
#include <InputEngine\Headers\Input.h>

#include <stdlib.h>
#include <time.h>

using namespace HyJynxRenderer;
using namespace HyJynxInput;
using namespace HyJynxCore;
using namespace HyJynxUtilities;
using namespace HyJynxLogger;
using namespace std;

class SceneTests : public ISystem
{
	Camera _camera;

public:

	// ctor
	SceneTests( )
		: ISystem( "Scene Tests" )
	{ }

	// init
	virtual bool Initialize( ) override
	{
		Renderer::Instance( )->SetRenderSequence( anew( DiffuseRenderSequence ) ( ) );
		Renderer::Instance( )->SetActiveCamera( &_camera );
		Renderer::Instance( )->SetClearColor( anew( Color )( 12.7f, 32.1f, 242.2f ) );

		SceneLoader* scene = anew( SceneLoader )( anew( FBXSceneIO )( ) );

		Function<void, Window* > onSuccess = [ ] ( _In_ CaptureData&, _In_ Window* sceneWindow )
		{
			Renderer::Instance( )->AddWindow( sceneWindow );
		};

		Function<void, Text> onFail = [ ] ( _In_ CaptureData&, _In_ Text reason )
		{
			Logger::Log( reason );
		};

		scene->LoadFromFile( "oogy boogy", onSuccess, onFail );

		return true;
	}

	// per-cycle
	virtual void Update( ) override
	{ }

	// release all
	virtual bool Uninitialize( ) override
	{
		return true;
	}
};


#endif // SCENETESTS_H