/**
 * logcommon.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXLOGGER_LOGCOMMON_H
#define HYJYNXLOGGER_LOGCOMMON_H

#include <Engine\Headers\Text.h>
#include <Engine\Headers\DynamicCollection.h>
#include <Engine\Headers\TypeDefinitions.h>
#include <Engine\Headers\Function.h>
#include <sal.h>

#endif // HYJYNXLOGGER_LOGCOMMON_H