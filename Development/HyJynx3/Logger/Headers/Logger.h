/**
 * Logger.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXLOGGER_LOGGER_H
#define HYJYNXLOGGER_LOGGER_H

#include "LoggerConfiguration.h"
#include "..\..\Engine\Headers\Singleton.h"
//#include <Engine\Headers\Singleton.h> // TODO: why is this not a file?
#include "..\..\Engine\Headers\Text.h"
//#include <Engine\Headers\Text.h> // TODO: why is this not a file?
#include "..\..\Engine\Headers\TypeDefinitions.h"
//#include <Engine\Headers\TypeDefinitions.h> // TODO: why is this not a file?
#include "..\..\Engine\Headers\Function.h"
//#include <Engine\Headers\Function.h> // TODO: why is this not a file?

namespace HyJynxLogger
{
	//
	// Logger implements the API for various functionality for managing,
	// filtering, listening, and responding to logging messages.
	//
	// For now this is just a stub; When things are drawing, our own
	// implementation of a console and logger window will utilize this.
	//
	class Logger sealed : public HyJynxDesign::Singleton<Logger>
	{
		friend class HyJynxDesign::Singleton<Logger>;

	private:

		LoggerConfiguration _config;
		HyJynxCore::Function< void, HyJynxCore::Text > _logOutput;

		//
		// the static .Log() method will call this to send the message
		// - Text: message coming from Logger::Log( Text )
		//
		void log( _In_ HyJynxCore::Text );

	public:

		static UInt TotalILogsCreated;

		//
		// default ctor
		//
		Logger( );

		//
		// get the configurations object
		//
		LoggerConfiguration* GetConfiguration( );

		//
		// Directly output a message to what log output is currently set.
		// Please do not abuse this - any messages here are globally always visible.
		// - Text: log message to putput
		//
		static void Log( _In_ const HyJynxCore::Text );

		//
		// set the listener to global messages, argue nullptr (or nothing) to remove.
		// - function<void(Text)>: callback to execute with a globally visible message.
		//
		void SetGlobalOutput( _In_opt_ HyJynxCore::Function< void, HyJynxCore::Text > );

	};
}

#endif // HYJYNXLOGGER_LOGGER_H