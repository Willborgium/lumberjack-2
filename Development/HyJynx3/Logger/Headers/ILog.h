/**
 * ILog.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXLOGGER_ILOG_H
#define HYJYNXLOGGER_ILOG_H

#include <Engine\Headers\Text.h>
#include <Engine\Headers\DynamicCollection.h>
#include <Engine\Headers\Time.h>

namespace HyJynxLogger
{
	//
	// description of all possible logging types
	//
	enum LogType : short
	{
		LOG_TYPE_LOG = 1,
		LOG_TYPE_INFO = 2,
		LOG_TYPE_WARN = -1,
		LOG_TYPE_ERROR = -2,
		LOG_TYPE_CRITICAL = -3
	};

	//
	// structure to contain a recorded message
	//
	class LogMessage sealed
	{
	public:
		HyJynxCore::Text Message;
		HyJynxCore::Time TimeStamp;
		LogType Type;

		//
		// null ctor
		//
		LogMessage( )
		{ }

		//
		// default ctor
		//
		LogMessage( _In_ HyJynxCore::Text txt, _In_ HyJynxCore::Time time, _In_ LogType type )
			:Message( txt ),
			TimeStamp( time ),
			Type( type )
		{ }

		bool operator == ( const LogMessage& other ) const
		{
			return other.Message == Message && other.Type == Type;
		}

	};

	typedef HyJynxCollections::DynamicCollection<LogMessage> MessageList;

	//
	// Base class of anything that wants to log infomation to a developer
	//
	class ILog abstract
	{
	private:

		MessageList _positiveMessageList;
		MessageList _negativeMessageList;
		HyJynxCore::Text _id;

		//
		// All Logs are forwarded here for sorting and recording
		// -LogType: enumeration describing what list it will be added to
		// -Text: the message itself
		//
		void ReceiveMessage( _In_ LogType, _In_ HyJynxCore::Text );

	protected:

		//
		// send a normal log message
		// -Text: message to log
		//
		virtual void Log( _In_ HyJynxCore::Text );

		//
		// send an informational log message (like from a management system)
		// -Text: message to log
		//
		virtual void LogInfo( _In_ HyJynxCore::Text );

		//
		// send a warning log message
		// -Text: message to log
		//
		virtual void LogWarning( _In_ HyJynxCore::Text );

		//
		// send an error log message
		// -Text: message to log
		//
		virtual void LogError( _In_ HyJynxCore::Text );

		//
		// send a critial log message
		// -Text: message to log
		//
		virtual void LogCritical( _In_ HyJynxCore::Text );

	public:

		//
		// default ctor
		//
		ILog( );

		//
		// constructor with text identifier
		// Text: indentifier
		//
		ILog( _In_ HyJynxCore::Text );

		//
		// some ILogs may want to change their name - like in a copy/move ctor - this allows them
		// - Text: the new name of this ILog
		//
		void SetLogID( _In_ HyJynxCore::Text );

		//
		// dtor fool
		//
		virtual ~ILog( );

		//
		// Because some ILog's can compose other ILogs, Logger will
		// call this method to get those children ILogs
		// -DynamicCollection<ILog*>*: the list of ILogs to add children to
		//
		virtual void GetChildrenLoggers( _In_ HyJynxCollections::DynamicCollection<ILog*>* const );

		//
		// equality operator
		//
		bool operator == ( _In_ const ILog& ) const;

		//
		// inequality operator
		//
		bool operator != ( _In_ const ILog& ) const;
	};
}

#endif // HYJYNXLOGGER_ILOG_H