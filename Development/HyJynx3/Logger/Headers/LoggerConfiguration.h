/**
 * LoggerConfiguration.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXLOGGER_LOGGERCONFIGURATION_H
#define HYJYNXLOGGER_LOGGERCONFIGURATION_H

//#include <Engine\Headers\TypeDefinitions.h> // TODO: why is this not a file?
#include "..\..\Engine\Headers\TypeDefinitions.h"

namespace HyJynxLogger
{
	//
	// Describes how Logger behaves.
	//
	class LoggerConfiguration sealed
	{

	private:

		UInt _maximumILogBackLogs = 20;

	public:

		//
		// Default CTOR
		//
		LoggerConfiguration( );

		//
		// gets the value depicting how many logs a ILog will store
		// - return UInt: number of stored logs
		//
		UInt GetMaximumILogBackLog( ) const;

		//
		// sets the value depicted how many logs an ILog will store
		// - UInt: number of stored logs
		//
		void SetMaximumILogBackLog( _In_ UInt max );
	};
}

#endif // HYJYNXLOGGER_LOGGERCONFIGURATION_H