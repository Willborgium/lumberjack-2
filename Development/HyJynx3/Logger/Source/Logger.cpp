/**
 * Logger.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Logger\Headers\logcommon.h>
#include <Logger\Headers\Logger.h>

using namespace HyJynxCore;
using namespace std;

namespace HyJynxLogger
{
	UInt Logger::TotalILogsCreated = 0;

	// default ctor
	Logger::Logger()
	{ }

	// get the configuration object
	LoggerConfiguration* Logger::GetConfiguration()
	{
		return &_config;
	}

	// static log entry
	void Logger::Log( _In_ const Text msg )
	{
		Logger::Instance( )->log( msg );
	}

	// the static .Log() will call here to actually output the log
	void Logger::log( _In_ Text msg )
	{
		if ( _logOutput.HasValue() == true )
		{
			msg += "\n";
			_logOutput( msg );
		}
	}

	// set the global log output
	void Logger::SetGlobalOutput( _In_opt_ Function< void, Text > callback )
	{
		_logOutput = callback;
	}
}