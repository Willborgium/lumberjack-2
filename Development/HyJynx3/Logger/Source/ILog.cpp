/**
 * ILog.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Logger\Headers\logcommon.h>
#include <Logger\Headers\ILog.h>
#include <Logger\Headers\Logger.h>

using namespace HyJynxCore;
using namespace HyJynxCollections;

namespace HyJynxLogger
{
	// null ctor
	ILog::ILog( )
		:_id( "no-id" ),
		_positiveMessageList( ),
		_negativeMessageList( )
	{
		Logger::TotalILogsCreated++;
	}

	// identifier ctor
	ILog::ILog( _In_ Text id )
		: _id( id.IsNullOrEmpty( ) ? "no-id" : id ),
		_positiveMessageList( ),
		_negativeMessageList( )
	{
		Logger::TotalILogsCreated++;
	}

	// dtor
	ILog::~ILog( )
	{
		Logger::TotalILogsCreated--;
	}

	// set a new logging id
	void ILog::SetLogID( _In_ Text id )
	{
		_id = id;
	}

	void ILog::GetChildrenLoggers( _In_ HyJynxCollections::DynamicCollection<ILog*>* const )
	{ }

	// all logs are forwarded here for recording
	void ILog::ReceiveMessage( _In_ LogType type, _In_ Text message )
	{
		if ( type > 0 )
		{
			_positiveMessageList.Append( { message, Time( ), type } );
		}
		else
		{
			_negativeMessageList.Append( { message, Time( ), type } );
		}

		while ( _positiveMessageList.Count( ) > Logger::Instance( )->GetConfiguration( )->GetMaximumILogBackLog( ) )
		{
			_positiveMessageList.RemoveAt( 0 );
		}

		while ( _negativeMessageList.Count( ) > Logger::Instance( )->GetConfiguration( )->GetMaximumILogBackLog( ) )
		{
			_negativeMessageList.RemoveAt( 0 );
		}
	}

	// send a normal log message
	void ILog::Log( _In_ Text msg )
	{
		if ( msg.IsNullOrEmpty( ) == false )
		{
			ReceiveMessage( LogType::LOG_TYPE_LOG, msg );
		}
	}

	// send an informational log message (like from a management system)
	void ILog::LogInfo( _In_ Text msg )
	{
		if ( msg.IsNullOrEmpty( ) == false )
		{
			ReceiveMessage( LogType::LOG_TYPE_INFO, msg );
		}
	}

	// send a warning log message
	void ILog::LogWarning( _In_ Text msg )
	{
		if ( msg.IsNullOrEmpty( ) == false )
		{
			ReceiveMessage( LogType::LOG_TYPE_WARN, msg );
		}
	}

	// send an error log message
	void ILog::LogError( _In_ Text msg )
	{
		if ( msg.IsNullOrEmpty( ) == false )
		{
			ReceiveMessage( LogType::LOG_TYPE_ERROR, msg );
		}
	}

	// send a critial log message
	void ILog::LogCritical( _In_ Text msg )
	{
		if ( msg.IsNullOrEmpty( ) == false )
		{
			ReceiveMessage( LogType::LOG_TYPE_CRITICAL, msg );
		}
	}

	// equality operator
	bool ILog::operator == ( _In_ const ILog& other ) const
	{
		// TODO: observe this
		if ( _id == other._id )
		{
			return memcmp( this, &other, sizeof( ILog ) ) == 0;
		}

		return false;
	}

	// inequality operator
	bool ILog::operator != ( _In_ const ILog& other ) const
	{
		return !( *this == other );
	}
}