/**
 * LoggerConfiguration.cpp
 * (c) 2014 All Rights Reserved
 */

#include <Logger\Headers\logcommon.h>
#include <Logger\Headers\LoggerConfiguration.h>

namespace HyJynxLogger
{
	// default ctor
	LoggerConfiguration::LoggerConfiguration( )
	{ }

	// max iLog stored logs
	UInt LoggerConfiguration::GetMaximumILogBackLog( ) const
	{
		return _maximumILogBackLogs;
	}

	// set max iLog stored logs
	void LoggerConfiguration::SetMaximumILogBackLog( _In_ UInt max )
	{
		_maximumILogBackLogs = max;
	}
}