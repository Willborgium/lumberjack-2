#ifndef HYJYNX_CORE_TIMESPAN_H
#define HYJYNX_CORE_TIMESPAN_H

namespace HyJynxCore
{
	//
	// Provides a structure to represent lengths of time.
	//
	struct TimeSpan sealed
	{
	private:
		double _seconds;

	public:
		//
		// Initializes a new instance of the TimeSpan class.
		//
		TimeSpan( );

		//
		// Initializes a new instance of the TimeSpan class.
		// - seconds: The number of seconds the TimeSpan represents.
		//
		TimeSpan( double seconds );

		//
		// Initializes a new instance of the TimeSpan class.
		// - seconds:	The number of seconds in the TimeSpan.
		// - minutes:	The number of minutes in the TimeSpan.
		//
		TimeSpan( double seconds, double minutes );

		//
		// Initializes a new instance of the TimeSpan class.
		// - seconds:	The number of seconds in the TimeSpan.
		// - minutes:	The number of minutes in the TimeSpan.
		// - hours:		The number of hours in the TimeSpan.
		//
		TimeSpan( double seconds, double minutes, double hours );

		//
		// Initializes a new instance of the TimeSpan class.
		// - seconds:	The number of seconds in the TimeSpan.
		// - minutes:	The number of minutes in the TimeSpan.
		// - hours:		The number of hours in the TimeSpan.
		// - days:		The number of days in the TimeSpan.
		//
		TimeSpan( double seconds, double minutes, double hours, double days );

		//
		// Returns the total seconds this TimeSpan represents.
		// - returns:	The total seconds this TimeSpan represents.
		//
		double TotalSeconds( ) const;
		
		//
		// Returns the total minutes this TimeSpan represents.
		// - returns:	The total minutes this TimeSpan represents.
		//
		double TotalMinutes() const;

		//
		// Returns the total hours this TimeSpan represents.
		// - returns:	The total hours this TimeSpan represents.
		//
		double TotalHours() const;

		//
		// Returns the total days this TimeSpan represents.
		// - returns:	The total days this TimeSpan represents.
		//
		double TotalDays( ) const;

		//
		// Adds the given number of seconds to this TimeSpan.
		// - value:	The number of seconds to add to this TimeSpan.
		//
		void AddSeconds(double value);

		//
		// Adds the given number of minutes to this TimeSpan.
		// - value:	The number of minutes to add to this TimeSpan.
		//
		void AddMinutes(double value);

		//
		// Adds the given number of hours to this TimeSpan.
		// - value:	The number of hours to add to this TimeSpan.
		//
		void AddHours(double value);

		//
		// Adds the given number of days to this TimeSpan.
		// - value:	The number of days to add to this TimeSpan.
		//
		void AddDays(double value);

		//
		// Adds the given TimeSpan to this TimeSpan.
		// - value:	The TimeSpan to add to this TimeSpan.
		//
		void Add( TimeSpan& value );

		TimeSpan operator+( TimeSpan& rhs ) const;
		TimeSpan operator-( TimeSpan& rhs ) const;
		TimeSpan operator*( TimeSpan& rhs ) const;
		TimeSpan operator/( TimeSpan& rhs ) const;

		TimeSpan operator+=( TimeSpan& rhs );
		TimeSpan operator-=( TimeSpan& rhs );
		TimeSpan operator*=( TimeSpan& rhs );
		TimeSpan operator/=( TimeSpan& rhs );

		bool operator==( TimeSpan& rhs ) const;
		bool operator!=( TimeSpan& rhs ) const;
		bool operator>( TimeSpan& rhs ) const;
		bool operator<( TimeSpan& rhs ) const;
		bool operator>=( TimeSpan& rhs ) const;
		bool operator<=( TimeSpan& rhs ) const;
	};
}

#endif