#ifndef HYJYNX_CORE_SYSTEM_INTERFACE_H
#define HYJYNX_CORE_SYSTEM_INTERFACE_H

#include "Data.h"
#include "TypeDefinitions.h"
#include "Text.h"

namespace HyJynxCore
{
	//
	// Exposes potential states of execution for systems.
	//
	enum class SystemState
	{
		//
		// The system is yet to be properly initialized.
		// 
		Uninitialized = 0,

		//
		// The system has been properly initialized, but is not running.
		//
		Initialized,

		//
		// The system is running normally.
		//
		Running,
		
		//
		// The system has been temporarily paused.
		//
		Suspended
	};

	//
	// Provides an API for a running systematic lifecycle.
	//
	class ISystem : public Data
	{
	protected:
		SystemState _state;
	public:
		//
		// Initializes the fields of the ISystem abstract class.
		// - name: The name of the system.
		//
		ISystem( Text name )
			: Data( name ), _state( SystemState::Uninitialized )
		{
		}

		//
		// Sets the current execution state of the system.
		// - state:	The value of the state of execution for this system.
		//
		void SetState( SystemState state )
		{
			_state = state;
		}

		//
		// Compares the current state of execution to the given state.
		// - state:		The state to compare the current state of execution to.
		// - returns:	A flag indicating whether the current state of execution matches
		//				the provided value.
		//
		bool Is( SystemState state ) const
		{
			return _state == state;
		}

		//
		// Gets the current state of execution for the system.
		//
		SystemState GetState( ) const
		{
			return _state;
		}

		//
		// When overridden in a derived class, initializes the system
		// so that it can be updated.
		// - returns: A value indicating whether the system was initialized successfully.
		// 
		virtual bool Initialize( ) = 0;

		//
		// When overridden in a derived class, updates the system.
		//
		virtual void Update( ) = 0;
		
		//
		// Sets a flag indicating whether the system is running based on if it has
		// been initialized.
		//
		void Begin( )
		{
			if ( _state == SystemState::Initialized )
			{
				_state = SystemState::Running;
			}
		}

		//
		// Stops the system from running.
		//
		void End( )
		{
			if ( _state == SystemState::Running || _state == SystemState::Suspended )
			{
				_state = SystemState::Initialized;
			}
		}

		//
		// Temporarily pauses the execution of a system.
		// - returns:	A flag indicating whether the state was successfully suspended.
		//
		virtual bool Suspend( )
		{
			return true;
		}

		//
		// Resumes a system that was suspended.
		// - returns:	A flag indicating whether the state was successfully resumed.
		//
		virtual bool Resume( )
		{
			return true;
		}

		//
		// When overriden in a derived class, uninitializes the system.
		// - returns:	A flag indicating whether the state was successfully uninitialized.
		//
		virtual bool Uninitialize( ) = 0;
	};
}

#endif