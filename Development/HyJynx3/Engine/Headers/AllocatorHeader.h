#ifndef HYJYNX_MEMORY_MANAGEMENT_ALLOCATOR_HEADER_H
#define HYJYNX_MEMORY_MANAGEMENT_ALLOCATOR_HEADER_H

#include "DestructorMapping.h"

#pragma warning( disable : 4482 )

#define ALLOCATOR_HEADER_KEY 7

namespace HyJynxMemoryManagement
{
	//
	// Used in the marking of object, these values
	// determine wheter an object has been touched by the
	// allocator's mark algorithm.
	//
	enum AllocatorHeaderMark
	{
		//
		// Has not been found during traversal.
		//
		Untouched = 0,

		//
		// Has been found during traversal.
		//
		Touched = 1,
	};

	//
	// These values are used to affect the behavior of a managed
	// object's memory by changing how it is interpreted by the
	// allocator.
	//
	enum AllocatorHeaderUsage
	{
		//
		// Can be cleaned or moved.
		//
		Default = 0,

		//
		// Cannot be cleaned.
		//
		Constant = 1,
		
		//
		// Cannot be moved. ** NOT SUPPORTED **
		//
		Fixed = 2,

		//
		// Cannot be cleaned or moved. ** NOT SUPPORTED **
		//
		Permanent = 3
	};

	//
	// This structure provides the least amount of information
	// to viably support an automatic garbage collector.
	//
	struct AllocatorHeader
	{
		//
		// These bytes are reserved, and their purposes may change.
		//
		char _reserved[4];

		//
		// A pointer to a destructor mapping that is associated with this object.
		//
		DestructorMappingBase* _destructor;

		//
		// The size of the object this header is for.
		//
		unsigned int _size;

		//
		// The offset, in bytes, from the start of the name space in the allocator
		// of this object's type name.
		//
		unsigned int _nameIndex;

		//
		// Initializes a new instance of the AllocatorHeader class.
		// - size: The size, in bytes, of the object this header refers to.
		//
		AllocatorHeader(unsigned short size)
			: _size(size)
		{
			_reserved[0] = ALLOCATOR_HEADER_KEY;
			_reserved[1] = AllocatorHeaderMark::Untouched;
			_reserved[2] = AllocatorHeaderUsage::Default;
			_reserved[3] = 0;
		}
	};
}

#endif