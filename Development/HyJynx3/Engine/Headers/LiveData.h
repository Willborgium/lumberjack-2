#ifndef HYJYNX_DESIGN_LIVE_DATA_H
#define HYJYNX_DESIGN_LIVE_DATA_H

#include "Observable.h"

namespace HyJynxDesign
{
	template <typename Type>
	//
	// Provides a transparent wrapper that allows all operations that occur
	// on a given object to be observed.
	//
	class LiveData : public Observable
	{
	private:
		static Type GetDeclValue() { return _value; }
		Type _value;
	public:
		//
		// Initializes a new instance of the LiveData class.
		//
		LiveData()
		{
		}

		//
		// Initializes a new instance of the LiveData class with the given value.
		// - rhs: The value to initialize this LiveData with.
		//
		LiveData(const Type& rhs)
			: _value(rhs)
		{
		}

		//
		// Initializes a new instance of the LiveData class with the given value.
		// - rhs: The value to initialize this LiveData with.
		//
		LiveData(const LiveData<Type>& rhs)
			: _value(rhs->_value)
		{
		}

		Type& operator=(Type& rhs)
		{
			NotifyMessage(__FUNCTION__);
			return _value = rhs;
		}

		Type operator+(Type& rhs)
		{
			NotifyMessage(__FUNCTION__);
			return _value + rhs;
		}
		Type operator-(Type& rhs)
		{
			NotifyMessage(__FUNCTION__);
			return _value - rhs;
		}
		Type operator*(Type& rhs)
		{
			NotifyMessage(__FUNCTION__);
			return _value * rhs;
		}
		Type operator/(Type& rhs)
		{
			NotifyMessage(__FUNCTION__);
			return _value / rhs;
		}
		Type operator%(Type& rhs)
		{
			NotifyMessage(__FUNCTION__);
			return _value % rhs;
		}
		Type& operator++()
		{
			NotifyMessage(__FUNCTION__);
			return ++_value;
		}
		Type operator++(int)
		{
			NotifyMessage(__FUNCTION__);
			return _value++;
		}
		Type& operator--()
		{
			NotifyMessage(__FUNCTION__);
			return --_value;
		}
		Type operator--(int)
		{
			NotifyMessage(__FUNCTION__);
			return _value--;
		}

		Type operator^(Type& rhs)
		{
			NotifyMessage(__FUNCTION__);
			return _value ^ rhs;
		}
		Type operator&(Type& rhs)
		{
			NotifyMessage(__FUNCTION__);
			return _value & rhs;
		}
		Type operator|(Type& rhs)
		{
			NotifyMessage(__FUNCTION__);
			return _value | rhs;
		}
		Type operator<<(Type& rhs)
		{
			NotifyMessage(__FUNCTION__);
			return _value << rhs;
		}
		Type operator>>(Type& rhs)
		{
			NotifyMessage(__FUNCTION__);
			return _value >> rhs;
		}

		Type operator~()
		{
			NotifyMessage(__FUNCTION__);
			return ~_value;
		}
		Type operator+()
		{
			NotifyMessage(__FUNCTION__);
			return +_value;
		}
		Type operator-()
		{
			NotifyMessage(__FUNCTION__);
			return -_value;
		}

		bool operator==(Type& rhs)
		{
			NotifyMessage(__FUNCTION__);
			return _value == rhs;
		}
		bool operator!=(Type& rhs)
		{
			NotifyMessage(__FUNCTION__);
			return _value != rhs;
		}
		bool operator>=(Type& rhs)
		{
			NotifyMessage(__FUNCTION__);
			return _value >= rhs;
		}
		bool operator<=(Type& rhs)
		{
			NotifyMessage(__FUNCTION__);
			return _value <= rhs;
		}
		bool operator>(Type& rhs)
		{
			NotifyMessage(__FUNCTION__);
			return _value > rhs;
		}
		bool operator<(Type& rhs)
		{
			NotifyMessage(__FUNCTION__);
			return _value < rhs;
		}

		Type operator!()
		{
			NotifyMessage(__FUNCTION__);
			return !_value;
		}
		Type operator&&(Type& rhs)
		{
			NotifyMessage(__FUNCTION__);
			return _value && rhs;
		}
		Type operator||(Type& rhs)
		{
			NotifyMessage(__FUNCTION__);
			return _value || rhs;
		}

		template <typename ArgType>
		Type operator+=(ArgType rhs)
		{
			NotifyMessage(__FUNCTION__);
			return _value.operator+=(rhs);
		}

		Type operator+=(Type& rhs)
		{
			NotifyMessage(__FUNCTION__);
			return _value += rhs;
		}
		Type operator-=(Type& rhs)
		{
			NotifyMessage(__FUNCTION__);
			return _value -= rhs;
		}
		Type operator*=(Type& rhs)
		{
			NotifyMessage(__FUNCTION__);
			return _value *= rhs;
		}
		Type operator/=(Type& rhs)
		{
			NotifyMessage(__FUNCTION__);
			return _value /= rhs;
		}
		Type operator%=(Type& rhs)
		{
			NotifyMessage(__FUNCTION__);
			return _value %= rhs;
		}

		Type operator^=(Type& rhs)
		{
			NotifyMessage(__FUNCTION__);
			return _value ^= rhs;
		}
		Type operator&=(Type& rhs)
		{
			NotifyMessage(__FUNCTION__);
			return _value &= rhs;
		}
		Type operator|=(Type& rhs)
		{
			NotifyMessage(__FUNCTION__);
			return _value |= rhs;
		}
		Type operator<<=(Type& rhs)
		{
			NotifyMessage(__FUNCTION__);
			return _value <<= rhs;
		}
		Type operator>>=(Type& rhs)
		{
			NotifyMessage(__FUNCTION__);
			return _value >>= rhs;
		}

		auto operator*() -> decltype(*GetDeclValue())
		{
			NotifyMessage(__FUNCTION__);
			return *_value;
		}
		const Type* operator->()
		{
			NotifyMessage(__FUNCTION__);
			return &_value;
		}
		auto operator[](unsigned int index) -> decltype(GetDeclValue()[index])
		{
			NotifyMessage(__FUNCTION__);
			return _value[index];
		}
	};
}

#endif