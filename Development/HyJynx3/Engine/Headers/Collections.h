#ifndef HYJYNX_COLLECTIONS_HEADER
#define HYJYNX_COLLECTIONS_HEADER

#include "Dequeue.h"
#include "Dictionary.h"
#include "DynamicCollection.h"
#include "FixedCollection.h"
#include "ICollection.h"
#include "KeyValuePair.h"
#include "Queue.h"
#include "Stack.h"

#endif