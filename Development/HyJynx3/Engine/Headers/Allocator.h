#ifndef HYJYNX_MEMORY_MANAGEMENT_ALLOCATOR_H
#define HYJYNX_MEMORY_MANAGEMENT_ALLOCATOR_H

#include "AllocatorHeader.h"
#include "AllocatorInfo.h"
#include "AllocatorRootHeader.h"

#include <sal.h>
#include <new>

#if USE_C11
#include <type_traits>
#endif

#pragma warning( disable : 4482 )
#include <functional>

namespace HyJynxMemoryManagement
{
	//
	// Provides an automatic GC that tracks live pointers, decreases
	// fragmentation, and allows for memory usage statistics.
	//
	class Allocator
	{
	protected:

		//
		// Contains information relevant to determining when the allocator should clean up.
		//
		struct CleanDetails
		{
			bool ShouldClean = false;
		};

		//
		// Defines the size of an object's header information.
		//
		const unsigned short _headerSize;

		//
		// Provides a natively linked list of destructor mappings for use
		// in destruction of garbage.
		//
		DestructorMappingBase* _mappingRoot;

		//
		// A pointer to the first byte in the managed heap.
		//
		char* _heap;

		//
		// The size, in bytes, of the managed heap.
		//
		unsigned int _size;

		//
		// A pointer to the next free space in the managed heap.
		//
		char* _nextFree;

		//
		// A pointer to the first byte of type names
		//
		char* _nameSpace;

		//
		// The size, in bytes, of the name space
		//
		unsigned int _nameSpaceSize;

		//
		// A pointer to the next free space in the name space.
		//
		char* _nextFreeNameSpace;

		//
		// An array of known roots from which to begin live object tracking (LOT).
		//
		static AllocatorRootHeader** _roots;

		//
		// The number of known roots.
		//
		static unsigned short _rootCount;

		static unsigned short _activeRootCount;

		//
		// The structure containing diagnostic information about memory usage.
		//
		AllocatorInfo _info;

		//
		// The diagnostic information from the last time a clean cycle was run.
		//
		AllocatorInfo _lastCleanInfo;

		//
		// A flag indicating whether the allocator should overlook some of the less important
		// heuristics in favor of performing a clean.
		//
		bool _isCleanRequested = false;

		//
		// The allocator instance.
		//
		static Allocator* _instance;


		void InitializeHeap( bool createNew, bool nsCreateNew );

		//
		// Returns a pointer to the last byte in the managed heap.
		//
		void* End( ) const;

		//
		// If one exists, this method returns a pointer to the first byte of a segment
		// of unoccupied memory in the managed heap of the given size. Otherwise,
		// returns nullptr.
		//
		_Ret_maybenull_ void* GetSlot( _In_ unsigned int size );

		//
		// Frees the given header object and any associated data.
		//
		void FreeHeader( _Inout_ AllocatorHeader* header );

		//
		// Marks the given byte as being reachable.
		//
		unsigned int MarkSpot( _In_ char* ptr, _In_opt_ bool isRoot = false);

		//
		// Gets the destructor mapping for a given type, which allows for
		// dynamic destruction of unknown typing.
		//
		template <typename Type>
		_Ret_notnull_ DestructorMappingBase* GetDestructorMapping( _In_ const type_info* type );

		//
		// Returns the current number of known roots by counting the current list.
		//
		unsigned short GetCurrentRootCount( );

		//
		// Removes fragmented memory between live objects by relocating them
		// contiguously in the managed heap.
		//
		void Compact( );

		//
		// Updates all pointers to the given address and redirects them to
		// the given newAddress. This method supports the Compact method, allowing
		// it free reign to move objects without worrying about dangling references.
		//
		void Refresh( _Inout_ void* address, _In_ void* newAddress );

		//
		// Allocates a copy of the given type name in the managed name space and returns
		// the offset to it.
		//
		unsigned int RegisterName( const char* typeName );

		//
		// Evaluates whether the allocator should perform a clean.
		// - returns:	Any information relevant to the decision made regarding cleaning.
		//
		CleanDetails ShouldClean( );

	public:
		//
		// Initializes a new instance of the Allocator class.
		//
		Allocator( );

		//
		// Initializes a new instance of the Allocator class.
		// - size:	The size, in bytes, of the heap.
		//
		Allocator( _In_ unsigned int size );

		//
		// Initializes a new instance of the Allocator class.
		// - buffer:	The buffer to treat as the heap.
		// - size:		The size, in bytes, of the buffer.
		//
		Allocator( _In_ char* buffer, _In_ unsigned int size );

		//
		// Initializes a new instance of the Allocator class.
		// - buffer:	The buffer to treat as the heap.
		// - size:		The size, in bytes, of the buffer.
		// - nsBuffer:	The buffer to use as namespace storage.
		// - nsSize:	The size, in bytes, of nsBuffer.
		//
		Allocator( _In_ char* buffer, _In_ unsigned int size, _In_ char* nsBuffer, _In_ unsigned int nsSize );

		~Allocator( );

		//
		// Immediately releases the memory occupied by the given object and attempts
		// to destruct it.
		// - obj:	The object to be freed.
		//
		void Free( _In_ void* obj );

		//
		// Run a full collection on the managed heap, which includes marking, collecting,
		// compacting, and refreshing.
		//
		void Clean( );

		//
		// Add a given object to the list of known roots.
		// - root:		A pointer to the object to be treated as a tracking root.
		// - returns:	A flag indicating whether the root was added.
		//
		template <typename Type>
		static bool AddRoot( _In_ Type* root );

		//
		// Removes a given object from the list of known roots.
		// - root:		The object to be removed from the list of roots.
		// - returns:	A flag indicating whether the root was removed.
		//
		static bool RemoveRoot( _In_ void* root );

		//
		// Allocates an object of the given type.
		// - noCompact:	If true, the GC will be allowed to perform a full collection
		//				if there is not enough space to allocate the requested object.
		// - returns:	A pointer to the created object. If the object was not allocated,
		//				returns nullptr.
		//
		template <typename Type>
		_Ret_maybenull_ Type* Allocate( bool noCompact = false );

		//
		// Allocates an array of objects of the given type.
		// - count:		The number of items in the array.
		// - noCompact:	If true, the GC will be allowed to perform a full collection
		//				if there is not enough space to allocate the requested objects.
		// - returns:	A pointer to the created array of objects. If the objects were
		//				not allocated, returns nullptr.
		//
		template <typename Type>
		_Ret_maybenull_ Type* Allocate( unsigned int count, bool noCompact = false );

		//
		// Returns information about memory usage.
		// - returns: A structure containing memory statistics.
		//
		_Ret_notnull_ const AllocatorInfo& GetInfo( );

		//
		// Returns a static instance of the Allocator class.
		// - instance:	If an instance has not been created for this allocator, the provided one
		//				will be used as the instance.
		// - returns:	The instance associated with the Allocator class.
		//
		static Allocator* Instance( Allocator* instance = nullptr );

		//
		// Initializes the root count and the list of known roots for the allocator.
		// - rootCount:	The maximum number of known roots.
		// - returns:	A flag indicating whether the initialization was successful.
		//
		static bool InitializeRootSystem( unsigned int rootCount, void* pool = nullptr );

		//
		// Hints to the allocator that a clean may be necessary, allowing it to overlook
		// some heuristics in favor of a clean.
		//
		void RequestClean( );
	};

	template <typename ReturnType, typename... Args>
	ReturnType* Allocate( Args... args )
	{
		ReturnType* output = nullptr;

		if(HyJynxMemoryManagement::Allocator::Instance( ) != nullptr)
		{
			output = new( HyJynxMemoryManagement::Allocator::Instance( )->Allocate<ReturnType>( ) ) ReturnType( args... );
		}
		else
		{
			output = new ReturnType( args... );
			HyJynxMemoryManagement::Allocator::AddRoot( output );
		}

		return output;
	}

	template <typename ReturnType>
	ReturnType* AllocateArray( const unsigned int count )
	{
		ReturnType* output = nullptr;

		if ( HyJynxMemoryManagement::Allocator::Instance( ) != nullptr )
		{
			output = new( HyJynxMemoryManagement::Allocator::Instance( )->Allocate<ReturnType>( count ) ) ReturnType [ count ];
		}
		else
		{
			output = new ReturnType [ count ];
			HyJynxMemoryManagement::Allocator::AddRoot( output );
		}

		return output;
	}


#ifdef MANAGED_CODE

	template<typename T>
	struct argument_type;

	template<typename T, typename U>
	struct argument_type<T( U )> {
		typedef U type;
	};

	//
	// Allocates an object of the given type using the allocator.
	//
#define anew(Type) HyJynxMemoryManagement::Allocate<HyJynxMemoryManagement::argument_type<void(Type)>::type>

	//
	// Allocates an array of objects of the given type and size using the allocator.
	//
#define anewa(Type, Count) HyJynxMemoryManagement::AllocateArray<HyJynxMemoryManagement::argument_type<void(Type)>::type>( Count )

#else

	//
	// Allocates an object of the given type.
	//
#define anew(Type) new Type

	//
	// Allocates an array of objects of the given type and size.
	//
#define anewa(Type, Count) new Type[Count]

#endif
}

#include "Allocator.inl"

#endif