#ifndef HYJYNX_CORE_VARIANT_H
#define HYJYNX_CORE_VARIANT_H

#include <forward_list>

namespace HyJynxCore
{
	//
	// Provides a variant type, capable of storing any data type.
	//
	class Variant
	{
	private:
		type_info* _type;
		void* _value;
		bool _isPointer;
	public:
		Variant( );

		template <typename Type>
		Variant( Type& value );

		template <typename Type>
		Variant( const Type& value );

		template <typename Type>
		Variant& operator=( Type& rhs );

		template <typename Type>
		Variant& operator=( Type* rhs );

		template <typename Type>
		//
		// Returns the value of this variant casted to the specified type.
		// - returns: The value, casted to the specified type.
		//
		Type As( );

		template <typename Type>
		operator Type( );

		template <typename Type>
		//
		// Determines whether the underlying type of the value is of the specified type.
		// - returns: A flag indicating if the value of this variant is of the specified type.
		//
		bool Is( );

		template <typename Type>
		bool operator==( const Type& rhs );

		template <typename Type>
		bool operator!=( const Type& rhs );

		template <typename Type>
		Type operator+( const Type& rhs );

		template <typename Type>
		Type operator-( const Type& rhs );

		template <typename Type>
		Type operator*( const Type& rhs );

		template <typename Type>
		Type operator/( const Type& rhs );

		template <typename Type>
		Type operator%( const Type& rhs );

		template <typename Type>
		Type operator^( const Type& rhs );

		template <typename Type>
		Type operator|( const Type& rhs );

		template <typename Type>
		Type operator&( const Type& rhs );

		template <typename Type>
		Type operator+=( const Type& rhs );

		template <typename Type>
		Type operator-=( const Type& rhs );

		template <typename Type>
		Type operator*=( const Type& rhs );

		template <typename Type>
		Type operator/=( const Type& rhs );

		template <typename Type>
		Type operator%=( const Type& rhs );

		template <typename Type>
		Type operator^=( const Type& rhs );

		template <typename Type>
		Type operator|=( const Type& rhs );

		template <typename Type>
		Type operator&=( const Type& rhs );

		void operator()();

		template <typename... Args>
		void operator()(Args... args);

		template <typename ReturnType>
		//
		// Attempts to use this value as a method call with a return type
		// and no parameters.
		//
		ReturnType Call();

		template <typename ReturnType, typename... Args>
		//
		// Attempts to use this value as a method call with a return type
		// and a variable amount of parameters.
		// - args: One or more arguments.
		//
		ReturnType Call(Args... args);

		template <typename CallerType>
		//
		// Attempts to use this value as a member method call with no parameters.
		// - pThis: A pointer to the object on which this method should be called.
		//
		void ThisCall(CallerType* pThis);

		template <typename CallerType, typename... Args>
		//
		// Attempts to use this value as a member method call with parameters.
		// - pThis:	A pointer to the object on which this method should be called.
		// - args:	One or more arguments.
		//
		void ThisCall(CallerType* pThis, Args... args);
		
		template <typename ReturnType, typename CallerType>
		//
		// Attempts to use this value as a member method call with no parameters
		// and a return value.
		// - pThis:	A pointer to the object on which this method should be called.
		//
		ReturnType ThisCall(CallerType* pThis);
		
		template <typename ReturnType, typename CallerType, typename... Args>
		//
		// Attempts to use this value as a member method call with parameters
		// and a return value.
		// - pThis:	A pointer to the object on which this method should be called.
		// - args:	One or more arguments.
		//
		ReturnType ThisCall(CallerType* pThis, Args... args);
		
		const char* TypeName( ) const;
	};
	
#include "Variant.inl"
}


#endif