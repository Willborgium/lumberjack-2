#ifndef HYJYNX_COLLECTIONS_COLLECTION_INTERFACE_H
#define HYJYNX_COLLECTIONS_COLLECTION_INTERFACE_H

#include "TypeDefinitions.h"
#include <functional>

namespace HyJynxCollections
{
	template <typename ValueType>
	//
	// Provides a universal API that should be implemented for all collection
	// types.
	//
	class ICollection abstract
	{
	protected:
		//
		// Represents an value stored in a collection.
		//
		struct CollectionElement
		{
			//
			// The value represented by this element.
			//
			ValueType _value;

			//
			// Assigns a value to this element.
			// - rhs:		The value to assign.
			// - returns:	A reference to this element.
			//
			CollectionElement& operator=( ValueType rhs )
			{
				_value = rhs;
				return *this;
			}

			//
			// Assigns a value from another element to this element.
			// - rhs:		The element whose value will be used to assign.
			// - returns:	A reference to this element.
			//
			CollectionElement& operator=( CollectionElement rhs )
			{
				_value = rhs._value;
				return *this;
			}

			//
			// Converts this element to the related value type.
			// 
			operator ValueType( )
			{
				return _value;
			}

			//
			// Compares the values of the given element to this element.
			// - rhs:		The element to compare.
			// - returns:	A flag indicating equality.
			// 
			bool operator==( CollectionElement& rhs )
			{
				return _value == rhs._value;
			}

			//
			// Compares the values of the given element to this element.
			// - rhs:		The element to compare.
			// - returns:	A flag indicating inequality.
			// 
			bool operator!=( CollectionElement& rhs )
			{
				return !operator==( rhs );
			}

			//
			// Compares the given value to the value of this element.
			// - rhs:		The value to compare.
			// - returns:	A flag indicating equality.
			// 
			bool operator==( ValueType rhs )
			{
				return _value == rhs;
			}

			//
			// Compares the given value to the value of this element.
			// - rhs:		The value to compare.
			// - returns:	A flag indicating inequality.
			// 
			bool operator!=( ValueType rhs )
			{
				return !operator==( rhs );
			}
		};

	public:

		//
		// Returns the number of elements in the collection.
		// - returns: The number of elements in the collection.
		//
		virtual UInt Count( ) const = 0;

		//
		// Returns the number of elements in the collection that match
		// a given condition.
		// - condition:	The condition to test for.
		// - returns:	The number of elements in the collection.
		//
		virtual UInt Count( std::function<bool( ValueType )> condition ) const = 0;

		//
		// Determines whether all elements satisfy a given condition.
		// - condition:	The condition to test for.
		// - returns:	A flag indicating whether all items satisfy the given condition.
		//
		virtual bool All( std::function<bool( ValueType )> condition ) const = 0;

		//
		// Determines whether the collection contains any items.
		// - returns:	A flag indicating whether the collection contains any items.
		//
		virtual bool Any( ) const = 0;

		//
		// Determines whether the collection contains any items that
		// satisfy the given condition.
		// - condition: The condition to test for.
		// - returns:	A flag indicating whether the collection contains any items
		//				that satisfies the given condition.
		//
		virtual bool Any( std::function<bool( ValueType )> condition ) const = 0;

		//
		// Sets the value at the given index.
		// - index:	The index at which the given value should be set.
		// - value:	The value to set.
		//
		virtual void SetValue( UInt index, ValueType value ) = 0;

		//
		// Returns the first item in the collection.
		// - returns: The first item in the collection.
		//
		virtual ValueType First( ) const = 0;

		//
		// Returns the first item in the collection that satisfies the given predicate.
		// - returns The first item in the collection that satisfies the given predicate.
		//
		virtual ValueType First( std::function<bool( ValueType )> predicate ) const = 0;

		//
		// Returns the last item in the collection.
		// - returns: The last item in the collection.
		//
		virtual ValueType Last( ) const = 0;

		//
		// Returns the last item in the collection that satisfies the given predicate.
		// - returns The last item in the collection that satisfies the given predicate.
		//
		virtual ValueType Last( std::function<bool( ValueType )> predicate ) const = 0;

		//
		// Adds the given value into the collection as the last element.
		// - value:	The value to add to the collection.
		//
		virtual void Append( ValueType value ) = 0;

		//
		// Adds the items in the given collection to the end of the collection.
		// - values:	The values to be added to the collection.
		//
		virtual void Append( const ICollection<ValueType>* values ) = 0;

		//
		// Adds the items in the given collection to the end of the collection.
		// - values:	The values to be added to the collection.
		//
		virtual void Append( const ICollection<ValueType>&& values ) = 0;

		//
		// Inserts the given value at the given index.
		// - index:	The index at which to insert the value.
		// - value:	The value to insert.
		//
		virtual void InsertAt( UInt index, ValueType value ) = 0;

		//
		// Inserts the given collection of values at the given index.
		// - index:		The index at which to insert the values.
		// - values:	The values to insert.
		//
		virtual void InsertAt( UInt index, ICollection<ValueType>* values ) = 0;

		//
		// Returns the index at which the requested value can be found.
		// - value:		The value to look for.
		// - returns:	The index of the first occurence of the given value. If none was found,
		//				returns -1.
		//
		virtual UInt IndexOf( ValueType value ) const = 0;

		//
		// Returns the value found at the given index.
		// - index:		The index to search to.
		// - returns:	The value located at the specified index. If none was found,
		//				returns a default instance of the ValueType.
		//
		virtual ValueType ElementAt( UInt index ) const = 0;

		//
		// Returns the value found at the given index.
		// - index:		The index to search to.
		// - returns:	The value located at the specified index. If none was found,
		//				returns a default instance of the ValueType.
		//
		virtual ValueType operator[]( UInt index ) const = 0;

		//
		// Returns whether or not the given value is part of the collection.
		// - value:		The value to search for.
		// - returns:	A flag indicating whether or not the given value is contained in the collection.
		//
		virtual bool Contains( ValueType value ) const = 0;

		//
		// Returns a collection of values from this collection that satisfy a given condition.
		// - condition:	A function object [usually in the form of a lambda expression] that
		//				accepts one argument of 'ValueType' that returns a boolean indicating
		//				whether or not this object should be returned in the resultant collection.
		// - returns:	A collection of values that satisfy the given condition. If no elements were found,
		//				and empty collection is returned.
		//
		virtual ICollection<ValueType>* Where( std::function<bool( ValueType )> condition ) const = 0;

		//
		// Selects a subset of the given collection.
		// - startIndex:	The first item to select.
		// - count:			The number of items to select after the first item.
		// - returns:		A collection of values that fall within the given range.
		//
		virtual ICollection<ValueType>* SelectRange( UInt startIndex, UInt count ) const = 0;

		//
		// Returns a copy of the current collection.
		// - returns: A collection with the same elements from this collection.
		//
		virtual ICollection<ValueType>* GetCopy( ) const = 0;

		//
		// Removes an element from the collection at the given index.
		// - index:	The index at which to remove an item.
		//
		virtual void RemoveAt( UInt index ) = 0;

		//
		// Removes 'count' number of elements from the collection at the given index.
		// - index:	The index at which to start removing.
		// - count:	The number of elements to remove.
		//
		virtual void Remove( UInt startIndex, UInt count ) = 0;

		//
		// Removes the given value from the collection, if it is present.
		// - value:	The value to remove.
		//
		virtual void Remove( ValueType value ) = 0;

		//
		// Removes all items that satisfy the given condition.
		// - condition:	The condition the item must satisfy to be removed.
		//
		virtual void RemoveWhere( std::function<bool( ValueType )> condition ) = 0;

		//
		// Removes all elements from the collection.
		//
		virtual void Clear( ) = 0;

		//
		// Reverses the order of the collection.
		//
		virtual void Reverse( ) = 0;

		//
		// Iterates through the collection using the provided statement.
		// - statement:	A function object [usually in the form of a lambda expression] that accepts
		//				one argument of 'ValueType' that will be executed on each object in the collection.
		//
		virtual void ForEach( std::function<void( ValueType )> statement ) const = 0;

		//
		// Sorts the collection based on a given expression.
		// - expression:	A function object [usually in the form of a lambda expression] that accepts
		//					two arguments of 'ValueType' that returns an integer indicating the relationship
		//					between two objects. -1 indicates the first argument is less than the second
		//					argument, 0 indicates equality, and 1 indicates the first argument is greater
		//					than the second argument.
		// - returns:		A copy of the current collection that is sorted based on the given collection.
		//
		virtual ICollection<ValueType>* Sort( std::function<int( ValueType, ValueType )> expression ) const = 0;
	};
}

#endif