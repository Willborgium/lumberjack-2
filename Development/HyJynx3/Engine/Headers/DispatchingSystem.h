#ifndef HYJYNX_CORE_DISPATCHING_SYSTEM_H
#define HYJYNX_CORE_DISPATCHING_SYSTEM_H

#include "ISystem.h"
#include "DynamicCollection.h"
#include "Singleton.h"
#include "Allocator.h"

namespace HyJynxCore
{
	//
	// Provides a class that can be used to orchestrate the lifecycle of other systems.
	//
	class DispatchingSystem : public ISystem, public HyJynxDesign::Singleton<DispatchingSystem>
	{
	private:

		friend HyJynxDesign::Singleton<DispatchingSystem>;

		HyJynxCollections::DynamicCollection<ISystem*> _systems;

		DispatchingSystem( );
	public:

		//
		// Initializes critical systems involved with the dispatcher.
		//
		virtual bool Initialize( ) override;

		//
		// Updates all systems in order.
		//
		virtual void Update( ) override;

		//
		// Shuts down the operation of the dispatcher.
		//
		virtual bool Uninitialize( ) override;

		//
		// Adds the given system to be processed after all current systems.
		// - system:	The system to be added last in the order of execution.
		//
		void AppendSystem( ISystem* system );

		//
		// Inserts the given system at the given index.
		// - index:		The index at which to insert the system in the run order.
		// - system:	The system to be inserted.
		//
		void InsertSystem( UInt index, ISystem* system );

		//
		// Removes the system at the specified run index.
		// - index:		The index at which to remove a system.
		// - returns:	The system that was removed. If none was removed, returns nullptr.
		//
		ISystem* RemoveSystem( UInt index );

		//
		// Removes the first system whose name matches the given name.
		// - name:		The name of the system to remove.
		// - returns:	The system that was removed. If none was removed, returns nullptr.
		//
		ISystem* RemoveSystem( Text name );

		//
		// Changes the execution order of a system at the specified source index to
		// the given destination index.
		// - source:		The index of the system to be moved.
		// - destination:	The location to which the source sytem should be relocated.
		//
		void MoveSystem( UInt source, UInt destination );

		//
		// Changes the execution order of a system by a given name to
		// the given destination index.
		// - name:			The name of the system to be moved.
		// - destination:	The location to which the source sytem should be relocated.
		//
		void MoveSystem( Text name, UInt destination );

		//
		// Returns the first system whose name matches the given name.
		// - name:		The name of the system.
		// - returns:	The system that was found. If none was found, returns nullptr.
		//
		ISystem* GetSystem( Text name );

		//
		// Returns the first system whose name matches the given name.
		// - name:		The name of the system.
		// - returns:	The system that was found. If none was found, returns nullptr.
		//
		template <typename SystemType>
		SystemType* GetSystem( Text name )
		{
			return static_cast< SystemType* >( GetSystem( name ) );
		}

		//
		// Returns the system at the specified index.
		// - index:		The index of the system.
		// - returns:	The system found at the given index. If none was found, returns nullptr.
		//
		ISystem* GetSystem( UInt index );
		
		//
		// Returns the system at the specified index.
		// - index:		The index of the system.
		// - returns:	The system found at the given index. If none was found, returns nullptr.
		//
		template <typename SystemType>
		SystemType* GetSystem( UInt index )
		{
			return static_cast< SystemType* >( GetSystem( index ) );
		}

		//
		// Returns the index of the first system found by the given name.
		// - name:		The name of the system.
		// - returns:	The execution index of the system. If none was found, returns UInt.MaxValue.
		//
		UInt IndexOf( Text name );

		//
		// Attempts to suspend a system with the given name.
		// - name:		The name of the system to suspend.
		// - returns:	A flag indicating whether the system was successfully suspended.
		//
		bool SuspendSystem( Text name );

		//
		// Attempts to resume execution of a system with the given name.
		// - name:		The name of the system to resume.
		// - returns:	A flag indicating whether the system was successfully resumed.
		//
		bool ResumeSystem( Text name );
	};
}

#endif