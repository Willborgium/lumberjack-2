/**
 * SerialCompletionChecker.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXUTILITIES_SERIALCOMPLETIONCHECKER_H
#define HYJYNXUTILITIES_SERIALCOMPLETIONCHECKER_H

#include "DynamicCollection.h"
#include <functional>
#include <sal.h>

/*

example:

SerialCompletionChecker checker;

checker.AddStep( []( function<void(bool)> onComplete)
{
	// do some logical stuff
	onComplete( true );
},
	[]()
{
	// the step failed, react
} );

checker.AddStep( []( function<void(bool)> onComplete)
{
	// do some logical stuff
	onComplete( true );
},
	[]()
{
	// the step failed, react
} );

checker.Start( []()
{
	// all of the steps completed A-OK
} );

*/

namespace HyJynxUtilities
{

	//
	// Similar to the completion checker, this util object will perform steps
	// serially in order, and performs a callback once all "steps" have completed
	// This is useful for initialization steps, eg:
	// -start() -- check1, check2, check3, complete!
	// The logical step code can be async or blocking, be sure to call the argued
	// onComplete callback with true or false for success or failure
	//
	class SerialCompletionChecker
	{
	private:

		//
		// internally wraps a single process
		//
		struct ProcessStep sealed
		{
			std::function< void( std::function< void(bool) > ) > Proc = nullptr;
			std::function< void( void ) > OnError = nullptr;

			// null ctor
			ProcessStep( )
			{ }

			// default ctor
			ProcessStep( _In_ std::function< void( std::function< void(bool) > ) > step, 
				_In_ std::function< void( void ) > err )
				: Proc( step ),
				OnError( err )
			{ }
		};

	protected:

		HyJynxCollections::DynamicCollection< ProcessStep* > _stepList;

		//
		// recursively called to perform a step, and move-on to the next, or call the aruged complete callback
		// - UInt: current index to perform
		// - function<void(void)>: onComplete callback
		//
		void performStep( _In_ UInt, _In_ std::function< void( void ) > );

	public:

		//
		// null ctor
		//
		SerialCompletionChecker( );

		//
		// copy ctor
		//
		SerialCompletionChecker( _In_ const SerialCompletionChecker& );

		//
		// move ctor
		//
		SerialCompletionChecker( _In_ const SerialCompletionChecker&& );

		//
		// dtor
		//
		~SerialCompletionChecker( );

		//
		// Add a step to the process
		// - function<void(function<void(bool)>)>: callback performed as a step within the process
		// - function<void(void)>: callback performed if this step returned false
		//
		void AddStep( _In_ std::function< void( std::function<void(bool)> ) >, _In_ std::function<void(void)> );

		//
		// start the process - steps will be performed in the order which they were aded
		// - std::function<void(void)>: callback made when all step have completed
		//
		void Start( _In_ std::function< void( void ) > );
	};
};

#endif // HYJYNXUTILITIES_SERIALCOMPLETIONCHECKER_H 