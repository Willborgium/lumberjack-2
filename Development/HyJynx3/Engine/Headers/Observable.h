#ifndef HYJYNX_CORE_OBSERVATION_OBSERVABLE_H
#define HYJYNX_CORE_OBSERVATION_OBSERVABLE_H

#include "DynamicCollection.h"
#include "Text.h"
#include "Data.h"
#include "IListener.h"

namespace HyJynxDesign
{
	//
	// Provides an implementation of the Observer pattern.
	//
	class Observable : public HyJynxCore::Data
	{
	protected:
		HyJynxCollections::DynamicCollection<IListener*> _listeners;
	public:
		//
		// Initializes a new instance of the Observable class.
		//
		Observable()
			: HyJynxCore::Data("unnamed")
		{
		}

		//
		// Initializes a new instance of the Observable class.
		// - name: The name of the object.
		//
		Observable(HyJynxCore::Text name)
			: HyJynxCore::Data(name)
		{
		}

		//
		// Adds a listener to be notified when messages or data are sent
		// from this object.
		// - listener: The listener to add.
		//
		void AddListener(IListener* listener);

		//
		// Removes a listener from being notified by this object.
		// - listener:	The listener to remove.
		// - returns:	A flag indicating whether the listener was removed.
		//
		bool RemoveListener(IListener* listener);

		//
		// Removes a listener from being notified by this object.
		// - name:		The name of the listener to remove.
		// - returns:	A flag indicating whether the listener was removed.
		//
		bool RemoveListener(HyJynxCore::Text name);

		//
		// Notifies all listeners with a given message.
		// - message: The message to send to all listeners.
		// 
		void NotifyMessage(HyJynxCore::Text message);

		//
		// Notifies all listeners with given data.
		// - data: The data to send to all listeners.
		// 
		void Notify(HyJynxCore::Data* data);
	};
}

#endif