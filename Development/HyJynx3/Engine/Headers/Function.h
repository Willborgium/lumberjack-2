#ifndef HYJYNX_CORE_FUNCTION_H
#define HYJYNX_CORE_FUNCTION_H

#include "Dictionary.h"
#include "Text.h"
#include "Variant.h"

namespace HyJynxCore
{
	typedef HyJynxCollections::Dictionary<Text, Variant> CaptureData;

	template<typename ReturnType, typename... Args>
	//
	// A function object with capture capabilities.
	//
	class Function
	{
	protected:
		typedef ReturnType( *FuncType )( Args... );
		typedef ReturnType( *LambdaType )( CaptureData&, Args... );

		FuncType _funcCall = nullptr;
		LambdaType _lambdaCall = nullptr;

		CaptureData _this;
	public:
		//
		// Initializes a default instance of the Function class.
		//
		Function( )
		{
		}

		//
		// Initializes an instance of the Function class by copying the values of the given function object.
		// - rhs: The object to copy.
		//
		Function( const Function<ReturnType, Args...>& rhs )
			: _funcCall( rhs._funcCall ), _lambdaCall( rhs._lambdaCall ), _this( rhs._this )
		{
		}

		//
		// Initializes an instance of the Function class using the given function call.
		// - function:	The non-capturing function this object wraps.
		//
		Function( const FuncType function )
			: _funcCall( function )
		{
		}

		//
		// Initializes an instance of the Function class using the given capturing-function call.
		// - lambda:	The capturing function this object wraps.
		//
		Function( const LambdaType lambda )
			: _lambdaCall( lambda )
		{
		}

		operator FuncType( )
		{
			FuncType output = nullptr;

			if ( _funcCall == nullptr )
			{
				if ( _lambdaCall != nullptr )
				{
					output = [ ] ( Args... args )
					{
						return _lamdaCall( _this, args... );
					}
				}
			}
			else
			{
				output = _funcCall;
			}
		}

		operator LambdaType( )
		{
			LambdaType output = nullptr;

			if ( _lambdaCall == nullptr )
			{
				if ( _funcCall != nullptr )
				{
					output = [ ] ( CaptureData& a, Args... args )
					{
						return _funcCall( args... );
					}
				}
			}
			else
			{
				output = _lambdaCall;
			}
		}

		ReturnType operator()( Args... args )
		{
			if ( _funcCall != nullptr )
			{
				return _funcCall( args... );
			}

			if ( _lambdaCall != nullptr )
			{
				return _lambdaCall( _this, args... );
			}

			throw;
		}

		//
		// Captures the given value using the given name to be consumed later.
		// - name:		The name of the value being captured.
		// - value:		The value to capture.
		// - returns:	A flag indicating whether the given name already existed in the capture data.
		//
		bool Capture( Text name, Variant value )
		{
			bool result = false;

			if ( !_this.ContainsKey( name ) )
			{
				_this.Append( name, value );
				result = true;
			}

			return result;
		}

		//
		// Removes all entries in the capture data.
		//
		void ClearCapture( )
		{
			_this.Clear( );
		}

		//
		// Returns whether or not this object wraps a valid function call.
		// - returns:	A flag indicating whether this object contains a callable function.
		//
		bool HasValue( )
		{
			return _funcCall != nullptr || _lambdaCall != nullptr;
		}
	};
}

#endif