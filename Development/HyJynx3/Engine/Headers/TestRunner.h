#ifndef HYJYNX_UTILITIES_TEST_RUNNER_H
#define HYJYNX_UTILITIES_TEST_RUNNER_H

#include "Singleton.h"
#include "Text.h"
#include "Dictionary.h"
#include <functional>

typedef std::function<bool(void)> TestMethod;

namespace HyJynxUtilities
{
	//
	// Provides basic functionality for grouping and executing tests
	//
	class TestRunner : public HyJynxDesign::Singleton<TestRunner>
	{
	protected:

		HyJynxCollections::Dictionary<HyJynxCore::Text, TestMethod> _tests;
	public:
		//
		// Provides a struct with details on tests results.
		//
		struct TestResults
		{
			//
			// The number of tests that were run.
			//
			UInt TestsRun;

			//
			// The number of tests that passed.
			//
			UInt Passed;

			//
			// The number of tests that failed.
			//
			UInt Failed;

			//
			// Initializes a new instance of the TestRunner class.
			//
			TestResults();
		};

		//
		// Adds the given test to the list of tests to be run.
		// - name:		The name of the test.
		// - test:		The method to be called that will execute the test.
		// - returns:	A flag indicating whether the test was registered. If false,
		//				a test by the provided name probably already exists.
		//
		bool RegisterTest(HyJynxCore::Text name, TestMethod test);

		//
		// Executes all registered tests.
		// - callback:	The method that receives the output of the tests.
		// - returns:	A summary of the results of all of the tests.
		//
		TestResults ExecuteTests(std::function<void(HyJynxCore::Text)> callback);
	};
}

#endif