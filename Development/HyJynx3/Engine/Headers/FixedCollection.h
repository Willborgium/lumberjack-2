#ifndef HYJYNX_COLLECTIONS_FIXED_COLLECTION_H
#define HYJYNX_COLLECTIONS_FIXED_COLLECTION_H

#include "TypeDefinitions.h"
#include "ICollection.h"
#include "MemoryManagement.h"
#include <functional>

namespace HyJynxCollections
{
	template <typename ValueType>
	//
	// Provides a generic API for a scalable fixed collection of objects
	// implemented as an array.
	//
	class FixedCollection : public ICollection<ValueType>
	{
	protected:
		UInt _count;
		CollectionElement* _collection;

		void Sort( int left, int right, std::function<int( ValueType, ValueType )>& expression )
		{
			int i = left;
			int j = right;
			ValueType temp = NULL;

			CollectionElement pivot = _collection [( left + right ) / 2];

			while ( i <= j )
			{
				while ( expression( _collection [i], pivot ) < 0 )
				{
					i++;
				}

				while ( expression( _collection [j], pivot ) > 0 )
				{
					j--;
				}

				if ( i <= j )
				{
					temp = _collection [i]._value;
					_collection [i] = _collection [j];
					_collection [j] = temp;
					i++;
					j--;
				}
			}

			if ( left < j )
			{
				Sort( left, j, expression );
			}

			if ( i < right )
			{
				Sort( i, right, expression );
			}
		}
	public:
		FixedCollection( )
			: _count( 0 ), _collection( nullptr )
		{
		}

		//
		// Creates a FixedCollection with an initial capacity.
		// - count: The initial capacity of the collection.
		//
		FixedCollection( UInt count )
			: _count( 0 ), _collection( nullptr )
		{
			Resize( count );
		}

		//
		// Initializes the collection based on another collection.
		// - collection:	The collection to copy.
		//
		FixedCollection( const ICollection<ValueType>* collection )
			: _count( 0 ), _collection( nullptr )
		{
			Append( collection );
		}

		//
		// Initializes the collection based on another collection.
		// - collection:	The collection to copy.
		//
		FixedCollection( const ICollection<ValueType>&& collection )
			: _count( 0 ), _collection( nullptr )
		{
			Append( collection );
		}

		//
		// Increases the capacity of the collection by the given amount.
		// - amount:	The number of empty elements to append to the collection.
		//
		void GrowBy( UInt amount )
		{
			if ( amount != 0 )
			{
				CollectionElement* temp = anewa( CollectionElement, _count + amount );
				memset( temp, 0, sizeof( CollectionElement ) * ( _count + amount ) );
				if ( _collection != nullptr )
				{
					for ( UInt index = 0; index < _count; index++ )
					{
						temp [index] = _collection [index];
					}
					delete [] _collection;
				}
				_collection = temp;
				_count += amount;
			}
		}

		//
		// Decreases the capacity of the collection by the given amount.
		// - amount:	The number of elements to remove from the collection.
		//
		void ShrinkBy( UInt amount )
		{
			if ( _count == amount )
			{
				if ( _collection != nullptr )
				{
					delete [] _collection;
					_count = 0;
				}
			}
			else if ( amount < _count )
			{
				_count -= amount;
				CollectionElement* temp = anewa( CollectionElement, _count );
				memset( temp, 0, sizeof( CollectionElement ) * _count );
				if ( _collection != nullptr )
				{
					for ( UInt index = 0; index < _count; index++ )
					{
						temp [index] = _collection [index];
					}
					delete [] _collection;
				}
				_collection = temp;
			}
		}

		//
		// Sets the size of the collection and either grows or shinks it.
		// - count: The new size of the collection.
		//
		void Resize( UInt count )
		{
			if ( count > _count )
			{
				GrowBy( count - _count );
			}
			else if ( count < _count )
			{
				ShrinkBy( _count - count );
			}
		}

		//
		// Returns the number of elements in the collection.
		// - returns: The number of elements in the collection.
		//
		virtual UInt Count( ) const
		{
			return _count;
		}

		//
		// Returns the number of elements in the collection that match
		// a given condition.
		// - condition:	The condition to test for.
		// - returns:	The number of elements in the collection.
		//
		virtual UInt Count( std::function<bool( ValueType )> condition ) const
		{
			UInt count = 0;

			for ( UInt index = 0; index < _count; index++ )
			{
				if ( condition( _collection [index]._value ) )
				{
					count++;
				}
			}

			return count;
		}
		
		//
		// Determines whether all elements satisfy a given condition.
		// - condition:	The condition to test for.
		// - returns:	A flag indicating whether all items satisfy the given condition.
		//
		virtual bool All( std::function<bool( ValueType )> condition ) const
		{
			bool result = true;

			for ( UInt index = 0; index < _count; index++ )
			{
				result = condition( _collection [index]._value );

				if ( !result )
				{
					break;
				}
			}

			return result;
		}

		//
		// Determines whether the collection contains any items.
		// - returns:	A flag indicating whether the collection contains any items.
		//
		virtual bool Any( ) const
		{
			return _count > 0;
		}

		//
		// Determines whether the collection contains any items that
		// satisfy the given condition.
		// - condition: The condition to test for.
		// - returns:	A flag indicating whether the collection contains any items
		//				that satisfies the given condition.
		//
		virtual bool Any( std::function<bool( ValueType )> condition ) const
		{
			bool output = false;

			for ( UInt index = 0; index < _count; index++ )
			{
				output = condition( _collection [index]._value );

				if ( output )
				{
					break;
				}
			}

			return output;
		}

		//
		// Sets the value at the given index.
		// - index:	The index at which the given value should be set.
		// - value:	The value to set.
		//
		virtual void SetValue( UInt index, ValueType value )
		{
			if ( index < _count && _collection != nullptr )
			{
				_collection [index] = value;
			}
		}

		//
		// Returns the first item in the collection.
		// - returns: The first item in the collection.
		//
		virtual ValueType First( ) const
		{
			if ( _count > 0 && _collection != nullptr )
			{
				return _collection [0]._value;
			}

			return ValueType( );
		}

		//
		// Returns the first item in the collection that satisfies the given predicate.
		// - predicate: The condition to test for.
		// - returns:	The first item in the collection that satisfies the given predicate.
		//
		virtual ValueType First( std::function<bool( ValueType )> predicate ) const
		{
			ValueType result;
			memset( &result, 0, sizeof( ValueType ) );

			for ( UInt index = 0; index < _count; index++ )
			{
				if ( predicate( _collection [index]._value ) )
				{
					result = _collection [index]._value;
					break;
				}
			}

			return result;
		}

		//
		// Returns the last item in the collection.
		// - returns: The last item in the collection.
		//
		virtual ValueType Last( ) const
		{
			if ( _count > 0 && _collection != nullptr )
			{
				return _collection [_count - 1]._value;
			}

			return ValueType( );
		}

		//
		// Returns the last item in the collection that satisfies the given predicate.
		// - predicate: The condition to test for.
		// - returns:	The last item in the collection that satisfies the given predicate.
		//
		virtual ValueType Last( std::function<bool( ValueType )> predicate ) const
		{
			ValueType result;
			memset( &result, 0, sizeof( ValueType ) );

			for ( UInt index = _count - 1; index >= 0; index-- )
			{
				if ( predicate( _collection [index]._value ) )
				{
					result = _collection [index]._value;
					break;
				}
			}

			return result;
		}

		//
		// Adds the given value into the collection as the last element.
		// - value:	The value to add to the collection.
		//
		virtual void Append( ValueType value )
		{
			GrowBy( 1 );
			_collection [_count - 1] = value;
		}

		//
		// Adds the items in the given collection to the end of the collection.
		// - values:	The values to be added to the collection.
		//
		virtual void Append( const ICollection<ValueType>* values )
		{
			if ( values != nullptr )
			{
				UInt end = _count;
				GrowBy( values->Count( ) );

				for ( UInt index = 0; index < values->Count( ); index++ )
				{
					_collection [end + index] = values->ElementAt( index );
				}
			}
		}

		//
		// Adds the items in the given collection to the end of the collection.
		// - values:	The values to be added to the collection.
		//
		virtual void Append( const ICollection<ValueType>&& values )
		{
			if ( values.Count( ) > 0 )
			{
				UInt end = _count;
				GrowBy( values.Count( ) );

				for ( UInt index = 0; index < values.Count( ); index++ )
				{
					_collection [end + index] = values.ElementAt( index );
				}
			}
		}

		//
		// Inserts the given value at the given index.
		// - index:	The index at which to insert the value.
		// - value:	The value to insert.
		//
		virtual void InsertAt( UInt index, ValueType value )
		{
			if ( index <= _count )
			{
				_count++;

				CollectionElement* temp = anewa( CollectionElement, _count );

				UInt insertedCount = 0;

				for ( UInt currentIndex = 0; currentIndex < _count; currentIndex++ )
				{
					if ( currentIndex == index )
					{
						temp [currentIndex] = value;
						insertedCount++;
					}
					else
					{
						temp [currentIndex] = _collection [currentIndex - insertedCount];
					}
				}

				delete [] _collection;
				_collection = temp;
			}
		}

		//
		// Inserts the given collection of values at the given index.
		// - index:		The index at which to insert the values.
		// - values:	The values to insert.
		//
		virtual void InsertAt( UInt index, ICollection<ValueType>* values )
		{
			UInt insertCount = values->Count( );

			if ( index <= _count && values != nullptr && insertCount > 0 )
			{
				CollectionElement* temp = anewa( CollectionElement, _count + values->Count( ) );

				UInt insertIndex = 0;
				bool hasInserted = false;

				for ( UInt currentIndex = 0; currentIndex < _count; currentIndex++ )
				{
					if ( hasInserted || currentIndex != index )
					{
						temp [currentIndex + insertIndex] = _collection [currentIndex];
					}
					else
					{
						for ( insertIndex = 0; insertIndex < insertCount; insertIndex++ )
						{
							temp [currentIndex + insertIndex] = values->ElementAt( insertIndex );
						}
						currentIndex--;
						hasInserted = true;
					}
				}

				if ( _collection != nullptr )
				{
					delete [] _collection;
				}

				_count += values->Count( );
				_collection = temp;
			}
		}

		//
		// Returns the index at which the requested value can be found.
		// - value:		The value to look for.
		// - returns:	The index of the first occurence of the given value. If none was found,
		//				returns -1.
		//
		virtual UInt IndexOf( ValueType value ) const
		{
			if ( _count > 0 && _collection != nullptr )
			{
				for ( UInt index = 0; index < _count; index++ )
				{
					if ( _collection [index] == value )
					{
						return index;
					}
				}
			}

			return -1;
		}

		//
		// Returns the value found at the given index.
		// - index:		The index to search to.
		// - returns:	The value located at the specified index. If none was found,
		//				returns a default instance of the ValueType.
		//
		virtual ValueType ElementAt( UInt index ) const
		{
			if ( index < _count && _collection != nullptr )
			{
				return _collection [index]._value;
			}
			else
			{
				return ValueType( );
			}
		}

		//
		// Returns the value found at the given index.
		// - index:		The index to search to.
		// - returns:	The value located at the specified index. If none was found,
		//				returns a default instance of the ValueType.
		//
		virtual ValueType operator[]( UInt index ) const
		{
			return ElementAt( index );
		}

		//
		// Returns whether or not the given value is part of the collection.
		// - value:		The value to search for.
		// - returns:	A flag indicating whether or not the given value is contained in the collection.
		//
		virtual bool Contains( ValueType value ) const
		{
			return ( IndexOf( value ) != -1 );
		}

		template <typename ReturnType>
		// 
		// Returns a collection of items that meet a given criteria.
		// - predicate: A function object [usually in the form of a lambda expression] that
		//				accepts one argument of 'ValueType' that will be used to determine
		//				what to return from this select statement.
		// - returns:	A collection of elements of type 'ReturnType' that satisfy the given predicate.
		//				If no elements were found, an empty collection is returned.
		//
		FixedCollection<ReturnType>* Select( std::function<ReturnType( ValueType )> predicate ) const
		{
			FixedCollection<ReturnType>* out = anew( FixedCollection<ReturnType> )( );

			for ( UInt index = 0; index < _count; index++ )
			{
				out->Append( predicate( _collection [index] ) );
			}

			return out;
		}

		//
		// Returns a collection of values from this collection that satisfy a given condition.
		// - condition:	A function object [usually in the form of a lambda expression] that
		//				accepts one argument of 'ValueType' that returns a boolean indicating
		//				whether or not this object should be returned in the resultant collection.
		// - returns:	A collection of values that satisfy the given condition. If no elements were found,
		//				and empty collection is returned.
		//
		virtual FixedCollection<ValueType>* Where( std::function<bool( ValueType )> condition ) const
		{
			FixedCollection<ValueType>* out = anew( FixedCollection<ValueType> )( );

			for ( UInt index = 0; index < _count; index++ )
			{
				if ( condition( _collection [index] ) )
				{
					out->Append( _collection [index] );
				}
			}

			return out;
		}

		//
		// Selects a subset of the given collection.
		// - startIndex:	The first item to select.
		// - count:			The number of items to select after the first item.
		// - returns:		A collection of values that fall within the given range.
		//
		virtual ICollection<ValueType>* SelectRange( UInt startIndex, UInt count ) const
		{
			if ( startIndex < _count && _collection != nullptr )
			{
				FixedCollection<ValueType>* out = anew( FixedCollection<ValueType> )( );

				UInt end = ( ( startIndex + count ) > _count ) ? _count : startIndex + count;

				for ( UInt index = startIndex; index < end; index++ )
				{
					out->Append( _collection [index] );
				}

				return out;
			}

			return nullptr;
		}

		//
		// Returns a copy of the current collection.
		// - returns: A collection with the same elements from this collection.
		//
		virtual ICollection<ValueType>* GetCopy( ) const
		{
			return SelectRange( 0, _count );
		}

		//
		// Removes an element from the collection at the given index.
		// - index:	The index at which to remove an item.
		//
		virtual void RemoveAt( UInt index )
		{
			Remove( index, 1 );
		}

		//
		// Removes 'count' number of elements from the collection at the given index.
		// - index:	The index at which to start removing.
		// - count:	The number of elements to remove.
		//
		virtual void Remove( UInt startIndex, UInt count )
		{
			if ( _collection != nullptr )
			{
				CollectionElement* temp = anewa( CollectionElement, _count - count );
				UInt cnt = 0;

				if ( startIndex > 0 )
				{
					memcpy( temp, _collection, sizeof( ValueType ) * startIndex );
					cnt += startIndex;
				}

				if ( startIndex + count < _count )
				{
					memcpy( &temp [startIndex], &_collection [startIndex + count], sizeof( ValueType ) * ( _count - ( startIndex + count ) ) );
					cnt += ( _count - ( startIndex + count ) );
				}

				delete [] _collection;
				_collection = temp;
				_count = cnt;
			}
		}

		//
		// Removes the given value from the collection, if it is present.
		// - value:	The value to remove.
		//
		virtual void Remove( ValueType value )
		{
			UInt index = IndexOf( value );

			if ( index != -1 )
			{
				RemoveAt( index );
			}
		}

		//
		// Removes all items that satisfy the given condition.
		// - condition:	The condition the item must satisfy to be removed.
		//
		virtual void RemoveWhere( std::function<bool( ValueType )> condition )
		{
			bool wasLastRemoved = false;
			for ( UInt index = 0; index < _count; index++ )
			{
				index -= wasLastRemoved ? 1 : 0;
				if ( condition( _collection [index]._value ) )
				{
					RemoveAt( index );
					wasLastRemoved = true;
				}
				else
				{
					wasLastRemoved = false;
				}
			}
		}

		//
		// Removes all elements from the collection.
		//
		virtual void Clear( )
		{
			if ( _collection != nullptr )
			{
				delete [] _collection;
				_count = 0;
			}
		}

		//
		// Reverses the order of the collection.
		//
		virtual void Reverse( )
		{
			if ( _collection != nullptr )
			{
				CollectionElement* temp = anewa( CollectionElement, _count );

				for ( UInt index = 0; index < _count; index++ )
				{
					temp [index] = _collection [_count - 1 - index];
				}

				delete [] _collection;
				_collection = temp;
			}
		}

		//
		// Iterates through the collection using the provided statement.
		// - statement:	A function object [usually in the form of a lambda expression] that accepts
		//				one argument of 'ValueType' that will be executed on each object in the collection.
		//
		virtual void ForEach( std::function<void( ValueType )> statement ) const
		{
			if ( _collection != nullptr && _count > 0 )
			{
				for ( UInt index = 0; index < _count; index++ )
				{
					statement( _collection [index] );
				}
			}
		}

		//
		// Sorts the collection based on a given expression.
		// - expression:	A function object [usually in the form of a lambda expression] that accepts
		//					two arguments of 'ValueType' that returns an integer indicating the relationship
		//					between two objects. -1 indicates the first argument is less than the second
		//					argument, 0 indicates equality, and 1 indicates the first argument is greater
		//					than the second argument.
		// - returns:		A copy of the current collection that is sorted based on the given collection.
		//
		virtual ICollection<ValueType>* Sort( std::function<int( ValueType, ValueType )> expression ) const
		{
			FixedCollection<ValueType>* out = anew( FixedCollection<ValueType> )( this );

			out->Sort( 0, _count - 1, expression );

			return out;
		}
	};
}

#define SELECT( inType, outType, from, what ) from.Select<outType>( [&]( inType arg ) -> outType { return what; } )

#endif