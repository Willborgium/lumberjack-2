#ifndef HYJYNX_CORE_ASSETS_H
#define HYJYNX_CORE_ASSETS_H

#include "Asset.h"

namespace HyJynxCore
{
	class Assets
	{
	public:
		static Asset GetConfigjson( );

		static Asset Getrockjpg( );

		static Asset GetTestItOuttxt( );

		static Asset Getmdl_mushroom_asciiFBX( );

		static Asset Getmdl_mushroom_binaryFBX( );

		static Asset Getbricks_diffusebmp( );

		static Asset Getsubrockjpg( );
	};
}

#endif