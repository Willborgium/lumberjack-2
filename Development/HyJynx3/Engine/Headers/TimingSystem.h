#ifndef HYJYNX_CORE_TIMING_SYSTEM_H
#define HYJYNX_CORE_TIMING_SYSTEM_H

#include "ISystem.h"
#include "HighPrecisionTimer.h"
#include "DynamicCollection.h"
#include "Singleton.h"

namespace HyJynxCore
{
	//
	// Provides a system for updating all timers.
	//
	class TimingSystem : virtual public ISystem, public HyJynxDesign::Singleton<TimingSystem>
	{
	protected:

		friend HyJynxDesign::Singleton<TimingSystem>;

		HyJynxCollections::DynamicCollection<HighPrecisionTimer*> _timers;
		HighPrecisionTimer* _systemTimer;

		TimingSystem( );
	public:
		//
		// Initializes the timing system.
		//
		virtual bool Initialize( ) override;

		//
		// Updates all timers.
		//
		virtual void Update( ) override;

		//
		// Stops all timers and uninitializes the timing system.
		//
		virtual bool Uninitialize( ) override;

		//
		// Adds the timer to the system to be updated.
		// - timer:		The timer to be registered.
		// - returns:	A flag indicating whether the timer was registered. Will return false
		//				if the timer is already registered.
		//
		bool RegisterTimer( HighPrecisionTimer* timer );

		//
		// Removes and stops the timer from the system.
		// - timer:		The timer to be removed.
		// - returns:	A flag indicating whether the timer was removed. Will return false
		//				if the timer is not registered.
		//
		bool UnRegisterTimer( HighPrecisionTimer* timer );

		//
		// Returns whether the given timer is registered in the system.
		// - timer:		The timer to check for.
		// - returns:	A flag indicating whether the given timer is registered.
		//
		bool IsRegistered( HighPrecisionTimer* timer );

		//
		// Gets the time since the application was started.
		// - returns: The amount of time that has passed since the application was started.
		//
		TimeSpan GetSystemElapsed( );
	};
}

#endif