#ifndef HYJYNX_CORE_ASYNC_METHOD_H
#define HYJYNX_CORE_ASYNC_METHOD_H

#include <thread>
#include <sal.h>

namespace HyJynxCore
{
	template<typename ReturnType, typename... Args>
	///
	/// Executes the given method with the provided arguments. When the method
	/// has completed execution, a callback is executed.
	/// - method:	The method to be executed.
	/// - args:		The arguments to be provided to the called method.
	/// - callback:	The method to be executed when the given method is complete.
	///
	void __executeAsyncMethod( _In_ std::function<ReturnType( Args... )> method, _In_ Args... args, _In_ std::function<void( ReturnType )> callback )
	{
		callback( method( std::forward<Args...>( args... ) ) );
	}

	template<typename ArgType>
	///
	/// Provides a method to have a null callback. Do not explicitly use.
	/// - argument:	The argument being provided.
	///
	void __emptyCall( _In_ ArgType argument )
	{
	}

	template <typename ReturnType, typename... Args>
	///
	/// Provides a way to execute a given method asynchronously.
	///
	class AsyncMethod
	{
	protected:
		std::function < ReturnType( Args... ) > _method;
	public:
		///
		/// Initializes an instance of the AsyncMethod class with the given method.
		/// - method:	The method to be executed asynchronously.
		///
		AsyncMethod( _In_ std::function<ReturnType( Args... )>&& method )
			: _method( method )
		{
		}

		///
		/// Executes the method on a dedicated thread.
		/// - args:		The arguments to be provided to the method.
		/// - callback:	The optional callback to be executed when the called method is complete.
		///
		void Execute( _In_ Args... args, _In_ std::function<void( ReturnType )> callback = &__emptyCall<ReturnType> )
		{
			std::thread caller( &__executeAsyncMethod<ReturnType, Args...>, _method, std::forward<Args...>( args... ), callback );
			caller.detach( );
		}

		void operator()( _In_ Args... args, _In_ std::function<void( ReturnType )> callback = &__emptyCall<ReturnType> )
		{
			Execute( args..., callback );
		}
	};
}

#endif