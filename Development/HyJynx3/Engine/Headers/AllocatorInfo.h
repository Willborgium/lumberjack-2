#ifndef HYJYNX_MEMORY_MANAGEMENT_ALLOCATOR_INFO_H
#define HYJYNX_MEMORY_MANAGEMENT_ALLOCATOR_INFO_H

#include <memory>

namespace HyJynxMemoryManagement
{
	//
	// Provides memory usage statistics for the Allocator class.
	//
	struct AllocatorInfo final
	{
		//
		// The total number of bytes governed by the Allocator.
		//
		unsigned int TotalBytes = 0;

		//
		// The number of unused bytes.
		//
		unsigned int FreeBytes = 0;

		//
		// The number of bytes currently allocated.
		//
		unsigned int AllocatedBytes = 0;

		//
		// The number of currently allocated bytes that are part of a header structure.
		//
		unsigned int HeaderBytes = 0;

		//
		// The number of currently allocated bytes that are not part of a header structure.
		//
		unsigned int DataBytes = 0;

		//
		// The total number of bytes allocated by the Allocator.
		//
		unsigned int TotalAllocatedBytes = 0;

		//
		// The total number of bytes freed by the Allocator.
		//
		unsigned int TotalFreedBytes = 0;

		//
		// The total number of objects currently living in the managed heap.
		//
		unsigned int ObjectCount = 0;

		//
		// The number of calls to the Allocate method.
		//
		unsigned int AllocateCalls = 0;

		//
		// The number of calls to release objects from the managed heap.
		//
		unsigned int ReleaseCalls = 0;

		//
		// The number of roots used in the Allocator for cleaning.
		//
		unsigned short RootCount = 0;

		//
		// Total number of roots given headers.
		//
		unsigned short TotalRootHeaderCount = 0;

		//
		// Total number of root headers freed.
		//
		unsigned short TotalRootHeadersFreed = 0;

		//
		// The total number of times the allocator was ordered to clean up.
		//
		unsigned long long TotalCleanCalls = 0;

		//
		// The cycle number this information was created during.
		//
		unsigned long long CycleStamp = 0;
	};
}

#endif