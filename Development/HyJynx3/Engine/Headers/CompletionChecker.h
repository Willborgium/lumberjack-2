/**
 * CompletionChecker.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXUTILITIES_COMPLETIONCHECKER_H
#define HYJYNXUTILITIES_COMPLETIONCHECKER_H

/*

example:

CompletionChecker checker = CompletionChecker( list.length, []()
{
	// all of the processes have completed
} );

list.forEach( []()
{
	list[i].load();
	checker.InformProcComplete();
} );


*/

#include "TypeDefinitions.h"
#include <sal.h>
#include <functional>

namespace HyJynxUtilities
{

	//
	// Utility object to perform a callback after a specified number of 
	// of processes have completed.
	//
	class CompletionChecker
	{
	protected:

		UInt							_total = 0;
		UInt							_currentCount = 0;
		std::function< void( void ) >	_onCompleteCallback = nullptr;

	public:

		//
		// null ctor
		//
		CompletionChecker( );

		//
		// Default ctor
		// - UInt: the number of informs the check is expecting
		// - function<void(void)>: callback once all objects have completed
		//
		CompletionChecker( _In_ UInt, _In_ std::function < void( void ) > );

		//
		// copy ctor
		//
		CompletionChecker( _In_ const CompletionChecker& );

		//
		// move ctor
		//
		CompletionChecker( _In_ const CompletionChecker&& );

		//
		// dtor
		//
		~CompletionChecker( );

		//
		// adds the current complete count by 1, when all processes report they have completed,
		// onComplete callback will fire
		//
		void InformProcComplete( );

	};
};

#endif // HYJYNXUTILITIES_COMPLETIONCHECKER_H 