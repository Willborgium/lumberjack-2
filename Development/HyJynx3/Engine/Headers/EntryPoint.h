#ifndef HYJYNX_ENTRY_POINT_H
#define HYJYNX_ENTRY_POINT_H

#include "Data.h"
#include "DynamicCollection.h"

// This method serves as the entry point into the application.
extern bool AppMain(HyJynxCollections::DynamicCollection<HyJynxCore::Data*>& input);

#endif