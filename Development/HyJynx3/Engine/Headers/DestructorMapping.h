#ifndef HYJYNX_MEMORY_MANAGEMENT_DESTRUCTOR_MAPPING_H
#define HYJYNX_MEMORY_MANAGEMENT_DESTRUCTOR_MAPPING_H

namespace HyJynxMemoryManagement
{
	//
	// Provides an interface to call a destructor as a standalone method
	// on an instance of an object without type information.
	//
	struct DestructorMappingBase
	{
		//
		// The name of the type of object this destructor mapping is for.
		//
		const char* _typeName;

		//
		// A pointer to the next destructor mapping.
		//
		DestructorMappingBase* _next;

		//
		// The index into the namespace heap where the name of this type is.
		// 
		unsigned int _nameIndex;

		//
		// Initializes a new instance of the DestructorMappingBase class.
		//
		DestructorMappingBase()
			: _next(nullptr), _typeName(nullptr)
		{
		}

		//
		// When overridden in a derived class, calls the type-specific destructor
		// on the given object.
		// - object: The object on which to call the destructor.
		//
		virtual void CallDestructor(void* object) = 0;
	};

	//
	// Provides an implementation to call a destructor as a
	// standalone method on an instance of an object without
	// type information.
	//
	template <typename Type>
	struct DestructorMapping : public DestructorMappingBase
	{
		//
		// Initializes a new instance of the DestructorMapping class.
		//
		DestructorMapping()
		: DestructorMappingBase()
		{
		}

		//
		// Calls the type specific constructor on the given object.
		// - object: The object on which to call the destructor.
		//
		virtual void CallDestructor(void* object)
		{
			if (object != nullptr)
			{
				((Type*)object)->~Type();
			}
		}
	};
}

#endif