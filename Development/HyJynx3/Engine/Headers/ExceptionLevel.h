#ifndef HYJYNX_CORE_EXCEPTION_LEVEL_H
#define HYJYNX_CORE_EXCEPTION_LEVEL_H

#include "TypeDefinitions.h"

namespace HyJynxCore
{
	//
	// Provides an enumeration of exception levels.
	//
	struct ExceptionLevel abstract sealed
	{
		//
		// Suggests that the exception is purely informational and does not impact application stability.
		//
		static const USmall Information		= 0;

		//
		// Suggests that the exception may have adverse affects on application stability.
		//
		static const USmall Warning			= 1;

		//
		// Suggests that the exception may induce undefined behavior, but will not cause the application
		// to crash.
		//
		static const USmall NonFatal		= 2;

		//
		// Suggests that the exception will cause the application to crash.
		//
		static const USmall Fatal			= 3;
	};
}

#endif