#ifndef HYJYNX_CORE_TIME_H
#define HYJYNX_CORE_TIME_H

#include "TypeDefinitions.h"
#include "Text.h"

namespace HyJynxCore
{
	//
	// Provides an enumeration of the days of the week.
	//
	struct DaysOfWeek abstract sealed
	{	
		//
		// The first day of the week.
		//
		static const USmall Sunday = 1;

		//
		// The second day of the week.
		//
		static const USmall Monday = 2;

		//
		// The third day of the week.
		//
		static const USmall Tuesday = 3;

		//
		// The fourth day of the week.
		//
		static const USmall Wednesday = 4;

		//
		// The fifth day of the week.
		//
		static const USmall Thursday = 5;

		//
		// The sixth day of the week.
		//
		static const USmall Friday = 6;

		//
		// The seventh day of the week.
		//
		static const USmall Saturday	= 7;
	};

	//
	// Provides an enumeration of the months of the year.
	//
	struct Months abstract sealed
	{
		//
		// The first month of the year.
		//
		static const USmall January = 1;

		//
		// The second month of the year.
		//
		static const USmall February = 2;

		//
		// The third month of the year.
		//
		static const USmall March = 3;

		//
		// The fourth month of the year.
		//
		static const USmall April = 4;

		//
		// The fifth month of the year.
		//
		static const USmall May = 5;

		//
		// The sixth month of the year.
		//
		static const USmall June = 6;

		//
		// The seventh month of the year.
		//
		static const USmall July = 7;

		//
		// The eight month of the year.
		//
		static const USmall August = 8;

		//
		// The ninth month of the year.
		//
		static const USmall September = 9;

		//
		// The tenth month of the year.
		//
		static const USmall October = 10;

		//
		// The eleventh month of the year.
		//
		static const USmall November = 11;

		//
		// The twelfth month of the year.
		//
		static const USmall December	= 12;
	};

	//
	// Provides an enumeration of the different date formats.
	//
	struct DateFormat abstract sealed
	{
		//
		// A date format of MM/DD/YYY.
		//
		static const USmall ShortDate = 0;

		//
		// A date format of Day Name, Month Day Number(suffix) Year.
		//
		static const USmall LongDate = 1;
	};

	//
	// Provides methods to interact with the current time as well as store
	// time in a readable structure.
	//
	struct Time sealed
	{
		//
		// The second of the time stamp.
		//
		USmall Second;

		//
		// The minute of the time stamp.
		//
		USmall Minute;

		//
		// The hour of the time stamp.
		//
		USmall Hour;

		//
		// The day of the month of the time stamp.
		//
		USmall DayOfMonth;

		//
		// The day of the week of the time stamp.
		//
		USmall DayOfWeek;

		//
		// The day of the year of the time stamp.
		//
		UShort DayOfYear;

		//
		// The month of the year of the time stamp.
		//
		UShort Month;

		//
		// The year of the time stamp.
		//
		UShort Year;

		//
		// Initializes a new instance of the Time class.
		//
		Time( );

		//
		// Returns the name of the day of this time stamp.
		// - returns: The name of the day of this time stamp.
		//
		Text DayName( ) const;

		//
		// Returns the name of the month of this time stamp.
		// - returns: The name of the month of this time stamp.
		//
		Text MonthName( ) const;

		//
		// Returns the day, month, and year of this time stamp in the given format.
		// - format:	The requested format of the result.
		// - returns:	The day, month, and year of this time stamp in the given format.
		//
		Text DatePart( USmall format ) const;

		//
		// Returns the hour, minute, and second of this time stamp.
		// - returns:	The hour, minute, and second of this time stamp.
		//
		Text TimePart( ) const;

		//
		// Returns the text representation of this time stamp in the requested format.
		// - format:	The requested format of the result.
		// - returns:	The text representation of this time stamp in the requested format.
		//
		Text ToText( USmall format ) const;

		//
		// Returns the binary time code of this time stamp.
		// - returns: The binary time code of this time stamp.
		//
		TimeCode ToTimeCode( );

		//
		// Returns a time stamp that represents when this method was called.
		// - returns: Atime stamp that represents when this method was called.
		//
		static Time Now();

		//
		// Returns a time stamp that represents the date of when this method was called.
		// - returns: Atime stamp that represents the date of when this method was called.
		//
		static Time Today( );

		//
		// Converts a time stamp into a Time object.
		// - time:		The time stamp to convert.
		// - returns:	The converted Time object.
		//
		static Time FromTimeStamp(TimeStamp time);

		//
		// Converts a time code into a Time object.
		// - code:		The time code to convert.
		// - returns:	The converted Time object.
		//
		static Time FromTimeCode(TimeCode code);
	};
}

#endif