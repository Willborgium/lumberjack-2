#ifndef HYJYNX_CORE_ALLOCATOR_CONFIG_H
#define HYJYNX_CORE_ALLOCATOR_CONFIG_H

#include "Text.h"
#include "JsonConfiguration.h"

namespace HyJynxCore
{
	//
	// Provides a mapping for the Allocator section of the config file.
	//
	struct AllocatorConfig : public HyJynxCore::JsonConfigurationObject
	{
	public:
		AllocatorConfig( )
		{
			AddMapping( new HyJynxCore::IntJsonConfiguration( "Allocator.HeapSize", &HeapSize ) );
			AddMapping( new HyJynxCore::BoolJsonConfiguration( "Allocator.UseExposedHeap", &UseExposedHeap ) );
			AddMapping( new HyJynxCore::TextJsonConfiguration( "Allocator.ExposedHeapName", &ExposedHeapName ) );
			AddMapping( new HyJynxCore::BoolJsonConfiguration( "Allocator.UseNamespaceHeap", &UseNamespaceHeap ) );
			AddMapping( new HyJynxCore::IntJsonConfiguration( "Allocator.NamespaceHeapSize", &NamespaceHeapSize ) );
			AddMapping( new HyJynxCore::TextJsonConfiguration( "Allocator.NamespaceHeapName", &NamespaceHeapName ) );
		}

		int HeapSize;
		bool UseExposedHeap;
		HyJynxCore::Text ExposedHeapName;
		bool UseNamespaceHeap;
		int NamespaceHeapSize;
		HyJynxCore::Text NamespaceHeapName;
	};
}

#endif