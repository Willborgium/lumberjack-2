/**
 * Dirty.h
 */

#ifndef HYJYNXRENDERER_DIRTY_H
#define HYJYNXRENDERER_DIRTY_H

#include "..\Headers\Delegate.h"
#include <functional>
#include <sal.h>

namespace HyJynxUtilities
{
	//
	// This is just your classic bool wrapper indicating a change has occured
	// in the derived class. But of course we're cool, so we'll fire callbacks
	// indicating state changes.
	//
	class Dirty abstract
	{
	private:

		HyJynxCore::Delegate< void* > _onDirty;
		HyJynxCore::Delegate< void* > _onClean;
		bool _isDirty;

	public:

		//
		// default ctor - value of true
		//
		Dirty( );

		//
		// setup ctor - input value
		//
		Dirty( _In_ bool );

		//
		// dtor
		//
		virtual ~Dirty( );

		//
		// return true if change has occured
		// return:		true for is dirty
		//
		bool IsDirty( ) const;

		//
		// manually set dirty
		// param:		set true if change has occured
		//
		void SetDirty( _In_ bool value );

		//
		// set dirty flag to true
		//
		void ToggleDirty( );

		//
		// set dirty flag to false
		//
		void ToggleClean( );

		//
		// get the on-dirty delegate, this is executed after we go from clean to dirty state.
		//
		HyJynxCore::Delegate< void* >* GetOnDirtyDelegate( );

		//
		// get the on-clean delegate, this is executed after we go from a dirty to clean state.
		//
		HyJynxCore::Delegate< void* >* GetOnCleanDelegate( );
	};

};

#endif // HYJYNXRENDERER_DIRTY_H