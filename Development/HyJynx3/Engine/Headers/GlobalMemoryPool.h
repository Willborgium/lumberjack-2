#ifndef HYJYNX_MEMORY_MANAGEMENT_GLOBAL_MEMORY_POOL_H
#define HYJYNX_MEMORY_MANAGEMENT_GLOBAL_MEMORY_POOL_H

namespace HyJynxMemoryManagement
{
	//
	// Provides operations for sharing memory across applications.
	//
	class GlobalMemoryPool sealed
	{
	private:
		unsigned long _size;
		char* _name;
		void* _handle;
		char* _view;
		bool _isOpen;
	public:
		//
		// Initializes a new instance of the GlobalMemoryPool class.
		//
		GlobalMemoryPool( );
		
		//
		// Initializes a new instance of the GlobalMemoryPool class.
		// - name:	The name the pool will be opened under.
		// - size:	The size of the pool in bytes.
		//
		GlobalMemoryPool( char* name, unsigned long size );

		//
		// Sets the values for the name and size fields of the pool if it is not already open.
		// - name:		The name the pool will be opened under.
		// - size:		The size of the pool in bytes.
		// - returns:	A flag indicating whether or not initialization was successful. Initialization
		//				will not be successful if the pool is already opened.
		//
		bool Initialize( char* name, unsigned long size );
		
		//
		// Opens the pool for read/write access.
		// - returns: A flag indicating whether the pool was opened.
		//
		bool Open( );

		//
		// Closes the pool.
		// - returns:	A flag indicating whether the close was successful. Returns
		//				false if the pool was already closed.
		//
		bool Close( );

		//
		// Returns whether the pool is open.
		// - returns: A flag indicating whether the pool is already open.
		//
		bool IsOpen( ) const { return _isOpen; }

		//
		// Writes the given value into the pool at the specified location.
		// - value:				The bytes to be written.
		// - size:				The length, in bytes, of the bytes to be written.
		// - location [OPT]:	The location at which to start writing the bytes.
		// - returns:			A flag indicating whether or not the write was successful.
		//
		bool Write( char* value, unsigned long size, unsigned long location = 0 );

		//
		// Reads all of the bytes of the pool.
		// - returns: The bytes in the pool.
		//
		const char* Read( ) const;
	};
}

#endif