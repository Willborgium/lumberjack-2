#ifndef HYJYNX_CORE_JSON_PARSER_H
#define HYJYNX_CORE_JSON_PARSER_H

#include "Collections.h"
#include "Core.h"

namespace HyJynxCore
{
	struct JsonData;

	typedef Text JsonValue;
	typedef HyJynxCollections::DynamicCollection<JsonData*> JsonArray;
	typedef HyJynxCollections::KeyValuePair<Text, JsonData*> JsonProperty;
	typedef HyJynxCollections::Dictionary<Text, JsonData*> JsonObject;

	//
	// Provides the facility for a JSON parser to return a single value
	// that may be an array, object, property, or value.
	//
	union JsonDataUnion
	{
	public:
		// 
		// Dereferences the data pointed to by this structure as a JSON array.
		//
		JsonArray* ArrayData;

		// 
		// Dereferences the data pointed to by this structure as a JSON object.
		//
		JsonObject* ObjectData;

		//
		// Dereferences the data pointed to by this structure as a JSON property.
		//
		JsonProperty* PropertyData;

		//
		// Dereferences the data pointed to by this structure as a JSON value.
		//
		JsonValue* ValueData;

		//
		// Initializes a new instance of the JsonDataUnion class with a JsonArray.
		//
		JsonDataUnion(JsonArray* value)
			: ArrayData(value)
		{
		}

		//
		// Initializes a new instance of the JsonDataUnion class with a JsonObject.
		//
		JsonDataUnion(JsonObject* value)
			: ObjectData(value)
		{
		}

		//
		// Initializes a new instance of the JsonDataUnion class with a JsonProperty.
		//
		JsonDataUnion(JsonProperty* value)
			: PropertyData(value)
		{
		}

		//
		// Initializes a new instance of the JsonDataUnion class with a JsonValue.
		//
		JsonDataUnion(JsonValue* value)
			: ValueData(value)
		{
		}
	};

	//
	// Provides a way to interpret JSON parser results.
	//
	struct JsonData
	{
		//
		// Provides an enumeration of possible results of JSON parsing.
		//
		enum JsonDataType
		{
			//
			// A single value.
			//
			Value = 1,

			//
			// A named value.
			//
			Property = 2,

			//
			// An object with properties.
			//
			Object = 3,
			
			//
			// An array of objects.
			//
			Array = 4
		};

		//
		// Identifies the type of the Data field.
		//
		JsonDataType DataType;

		//
		// Stores the data represented by this object.
		//
		JsonDataUnion* Data;

		//
		// Initializes a new instance of the JsonData class with a JsonValue.
		//
		JsonData( JsonValue* value )
			: DataType( JsonDataType::Value ), Data( new JsonDataUnion( value ) )
		{
		}

		//
		// Initializes a new instance of the JsonData class with a JsonProperty.
		//
		JsonData( JsonProperty* value )
			: DataType( JsonDataType::Property ), Data( new JsonDataUnion( value ) )
		{
		}

		//
		// Initializes a new instance of the JsonData class with a JsonObject.
		//
		JsonData( JsonObject* value )
			: DataType( JsonDataType::Object ), Data( new JsonDataUnion( value ) )
		{
		}

		//
		// Initializes a new instance of the JsonData class with a JsonArray.
		//
		JsonData( JsonArray* value )
			: DataType( JsonDataType::Array ), Data( new JsonDataUnion( value ) )
		{
		}

		~JsonData( )
		{
			if ( Data != nullptr )
			{
				JsonArray* aData = nullptr;
				JsonObject* oData = nullptr;
				JsonData* aFirst = nullptr;
				JsonData* oFirst = nullptr;

				switch ( DataType )
				{
					case JsonDataType::Value:
						delete Data->ValueData;
						break;
					case JsonDataType::Property:
						if ( Data->PropertyData->Value != nullptr )
						{
							delete Data->PropertyData->Value;
						}
						delete Data->PropertyData;
						break;
					case JsonDataType::Object:
						oData = Data->ObjectData;
						oFirst = oData->First( ).Value;
						while ( oFirst != nullptr )
						{
							oData->RemoveAt( 0 );
							delete oFirst;
							oFirst = oData->First( ).Value;
						}
						delete Data->ObjectData;
						break;
					case JsonDataType::Array:
						aData = Data->ArrayData;
						aFirst = aData->First( );
						while ( aFirst != nullptr )
						{
							aData->RemoveAt( 0 );
							delete aFirst;
							aFirst = aData->First( );
						}
						delete Data->ArrayData;
						break;
				}
			}
		}
	};

	//
	// Provides methods that can be used to convert Text to JavaScript Object Notation (JSON).
	//
	class JsonParser sealed
	{
	private:
		static JsonArray* CreateArray( const MutableText& json, UInt startIndex, UInt* endIndex );

		static JsonProperty* CreateProperty( const MutableText& json, UInt startIndex, UInt* endIndex );

		static JsonObject* CreateObject( const MutableText& json, UInt startIndex, UInt* endIndex );

		static JsonValue* ReadToToken( const MutableText& data, UInt startIndex, char token, UInt* endIndex );

	public:
		//
		// Attempts to interpret the contents of a file as JSON.
		// - filename:	The name of the file containing the JSON to be read.
		// - returns:	A JsonData object that contains the representation of the object.
		//
		static JsonData* ReadFile( const Text& filename );

		//
		// Attempts to interpret the text as JSON.
		// - jsonAsText:	The text containing the JSON to be read.
		// - returns:		A JsonData object that contains the representation of the object.
		//
		static JsonData* TextToJson( const Text& jsonAsText );
	};
}

#endif