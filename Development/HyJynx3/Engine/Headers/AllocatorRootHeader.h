#ifndef HYJYNX_MEMORY_MANAGEMENT_ALLOCATOR_ROOT_HEADER_H
#define HYJYNX_MEMORY_MANAGEMENT_ALLOCATOR_ROOT_HEADER_H

#include "AllocatorHeader.h"

namespace HyJynxMemoryManagement
{
	//
	// Provides a structured way to manage an allocator root.
	//
	struct AllocatorRootHeader : public AllocatorHeader
	{
		//
		// The address of the data that represents the allocator root. 
		//
		void* _address;

		const char* _namePtr;

		AllocatorRootHeader( void* address, unsigned short size, const char* name )
			: AllocatorHeader( size ), _address( address ), _namePtr( name )
		{
			_destructor = nullptr;
			_nameIndex = 0;
		}
	};
}

#endif