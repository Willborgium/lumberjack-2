#ifndef HYJYNX_COLLECTIONS_STACK_H
#define HYJYNX_COLLECTIONS_STACK_H

#include "DynamicCollection.h"

namespace HyJynxCollections
{
	template <typename ValueType>
	//
	// Provides an API for a first in, last out stack.
	//
	class Stack : public DynamicCollection<ValueType>
	{
	public:
		// 
		// Initializes a default instance of the Stack class.
		// 
		Stack( )
			: DynamicCollection<ValueType>( )
		{
		}

		//
		// Initializes the stack based on a given collection.
		// - collection:	The collection to copy.
		//
		Stack( ICollection<ValueType>* collection )
			: DynamicCollection<ValueType>( collection )
		{
		}

		//
		// Adds the given value to the end of the collection.
		//
		void Push( ValueType value )
		{
			Append( value );
		}

		//
		// Removes and returns the value at the end of the collection [at the top of the stack].
		// - returns: The value removed from the top of the Stack.
		//
		ValueType Pop( )
		{
			ValueType val = ElementAt( _count );
			RemoveAt( _count );
			return val;
		}
	};
}

#endif