#ifndef HYJYNX_CORE_CONFIGURATION_MAPPING_H
#define HYJYNX_CORE_CONFIGURATION_MAPPING_H

#include "ConfigurationManager.h"
#include "Text.h"
#include "DynamicCollection.h"

#include <sal.h>

namespace HyJynxCore
{
	//
	// Provides a base for mapping a configuration value to an in-memory value.
	//
	class JsonConfiguration
	{
	public:
		JsonConfiguration( ) = delete;

		JsonConfiguration( const JsonConfiguration& ) = delete;

		//
		// Initializes a new instance of the JsonConfiguration class with the given path.
		// - path:	The path in the JSON file to the value this mapping represents.
		//
		JsonConfiguration( _In_ HyJynxCore::Text path );

		//
		// The path in the JSON file to the value this mapping represents.
		//
		HyJynxCore::Text ConfigurationPath;

		//
		// Retrieves the value at the specified path in a JSON file
		// and assigns the value to the target.
		//
		virtual void RetrieveValue( ) = 0;
	};

	//
	// Provides a way to map a Text value in a JSON file.
	//
	class TextJsonConfiguration : public JsonConfiguration
	{
	public:
		TextJsonConfiguration( ) = delete;

		TextJsonConfiguration( const TextJsonConfiguration& ) = delete;

		//
		// Initializes a new instance of the TextJsonConfiguration class with the given path and target.
		// - path:		The path in the JSON file to the value this mapping represents.
		// - target:	A pointer to the Text object this mapping represents.
		//
		TextJsonConfiguration( _In_ HyJynxCore::Text path, _In_ HyJynxCore::Text* target );

		//
		// A pointer to the Text object this mapping represents.
		//
		Text* Value;

		//
		// Retrieves the value at the specified path in a JSON file
		// and assigns the value to the target.
		//
		virtual void RetrieveValue( ) override;
	};

	//
	// Provides a way to map an integer value in a JSON file.
	//
	class IntJsonConfiguration : public JsonConfiguration
	{
	public:
		IntJsonConfiguration( ) = delete;

		IntJsonConfiguration( const IntJsonConfiguration& ) = delete;

		//
		// Initializes a new instance of the IntJsonConfiguration class with the given path and target.
		// - path:		The path in the JSON file to the value this mapping represents.
		// - target:	A pointer to the integer this mapping represents.
		//
		IntJsonConfiguration( _In_ HyJynxCore::Text path, _In_ int* target );

		//
		// A pointer to the integer this mapping represents.
		//
		int* Value;

		//
		// Retrieves the value at the specified path in a JSON file
		// and assigns the value to the target.
		//
		virtual void RetrieveValue( ) override;
	};

	//
	// Provides a way to map a double value in a JSON file.
	//
	class DoubleJsonConfiguration : public JsonConfiguration
	{
	public:
		DoubleJsonConfiguration( ) = delete;

		DoubleJsonConfiguration( const DoubleJsonConfiguration& ) = delete;

		//
		// Initializes a new instance of the DoubleJsonConfiguration class with the given path and target.
		// - path:		The path in the JSON file to the value this mapping represents.
		// - target:	A pointer to the double this mapping represents.
		//
		DoubleJsonConfiguration( _In_ HyJynxCore::Text path, _In_ double* target );

		//
		// A pointer to the double this mapping represents.
		//
		double* Value;

		//
		// Retrieves the value at the specified path in a JSON file
		// and assigns the value to the target.
		//
		virtual void RetrieveValue( ) override;
	};

	//
	// Provides a way to map a boolean value in a JSON file.
	//
	class BoolJsonConfiguration : public JsonConfiguration
	{
	public:
		BoolJsonConfiguration( ) = delete;

		BoolJsonConfiguration( const BoolJsonConfiguration& ) = delete;

		//
		// Initializes a new instance of the BoolJsonConfiguration class with the given path and target.
		// - path:		The path in the JSON file to the value this mapping represents.
		// - target:	A pointer to the boolean this mapping represents.
		//
		BoolJsonConfiguration( _In_ HyJynxCore::Text path, _In_ bool* target );

		//
		// A pointer to the boolean this mapping represents.
		//
		bool* Value;

		//
		// Retrieves the value at the specified path in a JSON file
		// and assigns the value to the target.
		//
		virtual void RetrieveValue( ) override;
	};

	//
	// Provides a way to map an object to a JSON file.
	//
	class JsonConfigurationObject
	{
	protected:
		HyJynxCollections::DynamicCollection<JsonConfiguration*> _mappings;
	public:
		//
		// Initializes a default instance of the JsonConfigurationObject class.
		//
		JsonConfigurationObject( ) = default;

		//
		// Adds the given mapping to the object.
		// - mapping:	The mapping, which describes the path and the target, to add.
		//
		void AddMapping( _In_ JsonConfiguration* mapping );

		//
		// Removes a mapping of the specified path.
		// - path:	The path in the JSON file the desired mapping to remove has.
		//
		void RemoveMapping( _In_ HyJynxCore::Text path );

		//
		// Updates the targets of all mappings by retrieving the values from their paths.
		//
		void Refresh( );
	};
}

#endif