#ifndef HYJYNX_CORE_RESOURCE_CONTROLLER_H
#define HYJYNX_CORE_RESOURCE_CONTROLLER_H

#include "Singleton.h"
#include "Dictionary.h"
#include "Text.h"

namespace HyJynxCore
{
	//
	// Provides a single point of access for all resources.
	//
	class ResourceController : public HyJynxDesign::Singleton<ResourceController>
	{
	protected:
		friend HyJynxDesign::Singleton<ResourceController>;

		struct ResourceKey
		{
			Text Name;
			UInt Group;

			bool operator==(const ResourceKey& rhs);

			bool operator!=(const ResourceKey& rhs);

			ResourceKey();

			ResourceKey(Text name);

			ResourceKey(Text name, UInt group);
		};

		HyJynxCollections::Dictionary<ResourceKey, void*> _resources;

		ResourceController();
	public:

		template <typename Type>
		//
		// Retrieves a resource by the given name.
		// - name:		The name of the resource to be retrieved.
		// - group:		The group this resource is part of.
		// - returns:	A pointer to the requested object.
		//
		Type* Get(Text name, UInt group = 0)
		{
			Type* output = nullptr;

			ResourceKey key(name, group);

			if (_resources.ContainsKey(key))
			{
				output = static_cast<Type*>(_resources[key]);
			}

			return output;
		}

		template <typename Type>
		//
		// Adds a resource to the controller.
		// - value:		The resource value.
		// - name:		The name of the resource.
		// - group:		The group this resource is part of.
		// - returns:	A flag indicating whether this resource was successfully added to the controller.
		//
		bool Add(Type* value, Text name, UInt group = 0)
		{
			bool result = false;
			ResourceKey key(name, group);

			if (!_resources.ContainsKey(key))
			{
				_resources.Append(key, value);
				result = true;
			}

			return result;
		}

		//
		// Removes the requested resource from the controller.
		// - name:		The name of the resource to remove.
		// - group:		The group from which to remove this resource.
		// - returns:	The value that was removed from the controller.
		//
		void* Remove(Text name, UInt group = 0);

		//
		// Get the modified file path leading to the resource directory
		// - Text:			The path leading to your file, eg: "Models\Player.obj"
		// - returns Text:	The modified directory location to the project's resources
		//					eg: "bin\Release\Content\Models\Player.obj"
		//
		static Text GetPath( _In_ const Text& );
	};
}

#endif