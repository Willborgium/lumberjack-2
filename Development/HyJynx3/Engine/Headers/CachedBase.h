#ifndef HYJYNX_DESIGN_CACHED_BASE_H
#define HYJYNX_DESIGN_CACHED_BASE_H

namespace HyJynxDesign
{
	template <typename ValueType>
	//
	// Provides a basic outline for caching operations.
	//
	class CachedBase
	{
	protected:
		ValueType _value;
		bool _isCached = false;
		virtual bool RetrieveInternal( ) = 0;
	public:
		//
		// Returns the underlying cached value. If it is not initialized or a refresh is requested,
		// a subsequent call to retrieve the data is made.
		// - refresh:	A flag indicating whether to force a retrieval and update the cached value.
		// - returns:	The value resulting from the last retrieval.
		//
		ValueType Retrieve( bool refresh = false )
		{
			if ( !_isCached || refresh )
			{
				_isCached = RetrieveInternal( );
			}

			return _value;
		}

		operator ValueType( )
		{
			return Retrieve( );
		}
	};
}

#endif