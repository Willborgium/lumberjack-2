#ifndef HYJYNX_UTILITIES_MATH_H
#define HYJYNX_UTILITIES_MATH_H

#include <cmath>

namespace HyJynxUtilities
{
	//
	// Put down the abbacus, the Math class has arrived.
	//
	class Math abstract sealed
	{
	public:

		static const long double Pi();
		static const long double TwoPi();
		static const long double E();

		static long double ArcCosine(long double value);
		static long double ArcSine(long double value);
		static long double ArcTangent(long double value);
		static long double ArcTangent2(long double x, long double y);

		static long double Sine(long double angle);
		static long double Cosine(long double angle);
		static long double Tangent(long double angle);

		static long double HyperbolicSine(long double angle);
		static long double HyperbolicCosine(long double angle);
		static long double HyperbolicTanget(long double angle);

		static int Ceiling(long double value);
		static int Floor(long double value);
		static int Remainder(int numerator, int denominator);
		template <typename NumericType>
		static NumericType Maximum(NumericType lhs, NumericType rhs)
		{
			return lhs > rhs ? lhs : rhs;
		}
		template <typename NumericType>
		static NumericType Minimum(NumericType lhs, NumericType rhs)
		{
			return lhs < rhs ? lhs : rhs;
		}
		static int Round(long double value)
		{
			return (value - long double(int(value))) >= .5 ? int(value) + 1 : int(value);
		}

		static long double Log(long double value);
		static long double SquareRoot(long double value);
		template <typename NumericType>
		static NumericType ToPower(NumericType value, NumericType power)
		{
			return pow(value, power);
		}
		template <typename NumericType>
		static NumericType AbsoluteValue(NumericType value)
		{
			return abs(value);
		}

		template <typename NumericType>
		static NumericType Clamp(NumericType value, NumericType min, NumericType max)
		{
			if (value > max)
			{
				return max;
			}
			else if (value < min)
			{
				return min;
			}
			else
			{
				return value;
			}
		}
		template <typename NumericType>
		static NumericType Wrap(NumericType value, NumericType min, NumericType max)
		{
			NumericType range = max - min;

			while (value > max)
			{
				value -= range;
			}

			while (value < min)
			{
				value += range;
			}

			return value;
		}

		static long double LinearInterpolation(long double percent, long double min, long double max);
		static long double SinusoidalInterpolation(long double percent, long double min, long double max, bool inverted = false);
	};
}

#endif