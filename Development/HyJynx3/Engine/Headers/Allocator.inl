#include "Allocator.h"

template <typename Type>
_Ret_notnull_ HyJynxMemoryManagement::DestructorMappingBase* HyJynxMemoryManagement::Allocator::GetDestructorMapping( _In_ const type_info* type )
{
	auto search = _mappingRoot;
	DestructorMappingBase* prev = nullptr;
	auto name = type->name( );

	while( search != nullptr && search->_typeName != name )
	{
		prev = search;
		search = search->_next;
	}

	if( search == nullptr )
	{
		search = new DestructorMapping<Type>( );
		
		if( _mappingRoot == nullptr )
		{
			_mappingRoot = search;
		}

		if(_nameSpace != nullptr )
		{
			search->_nameIndex = RegisterName( name );
			search->_typeName = name;
		}
		else
		{
			search->_typeName = name;
		}

		if( prev != nullptr )
		{
			prev->_next = search;
		}
	}

	return search;
}

template <typename Type>
_Ret_maybenull_ Type* HyJynxMemoryManagement::Allocator::Allocate( bool noCompact )
{
	auto objSize = sizeof( Type );
	auto slot = ( char* )GetSlot( _headerSize + objSize );

	if( slot == nullptr )
	{
		if( !noCompact )
		{
			Clean( );
		}

		if( _info.FreeBytes >= objSize + _headerSize )
		{
			return Allocate<Type>( true );
		}

		return nullptr;
	}

	// Create header
	auto header = new ( slot ) AllocatorHeader( objSize );
	slot += _headerSize;

	header->_destructor = GetDestructorMapping<Type>( &typeid( Type ) );
	if( _nameSpace != nullptr )
	{
		header->_nameIndex = header->_destructor->_nameIndex;
	}

	_info.AllocateCalls++;
	_info.AllocatedBytes += _headerSize + objSize;
	_info.DataBytes += objSize;
	_info.HeaderBytes += _headerSize;
	_info.ObjectCount++;
	_info.TotalAllocatedBytes += _headerSize + objSize;
	_info.FreeBytes -= _headerSize + objSize;

	return ( Type* )slot;
}

template <typename Type>
_Ret_maybenull_ Type* HyJynxMemoryManagement::Allocator::Allocate( unsigned int count, bool noCompact )
{
	if( count > 0 )
	{
		auto objSize = sizeof( Type ) * count;
		auto slot = ( char* )GetSlot( _headerSize + objSize );

		if( slot == nullptr )
		{
			if( !noCompact )
			{
				Clean( );
			}

			if( _info.FreeBytes >= objSize + _headerSize )
			{
				return Allocate<Type>( count, true );
			}

			return nullptr;
		}

		// Create header
		auto header = new ( slot ) AllocatorHeader( objSize );
		slot += _headerSize;

		header->_destructor = GetDestructorMapping<Type>( &typeid( Type[] ) );
		header->_nameIndex = header->_destructor->_nameIndex;

		_info.AllocateCalls++;
		_info.AllocatedBytes += _headerSize + objSize;
		_info.DataBytes += objSize;
		_info.HeaderBytes += _headerSize;
		_info.ObjectCount++;
		_info.TotalAllocatedBytes += _headerSize + objSize;
		_info.FreeBytes -= _headerSize + objSize;

		return ( Type* )slot;
	}
	else
	{
		return nullptr;
	}
}

template <typename Type>
bool HyJynxMemoryManagement::Allocator::AddRoot( Type* root )
{
	int index = 0;

	while ( index < _rootCount && _roots [ index ] != nullptr )
	{
		index++;
	}

	if ( index < _rootCount )
	{
		_roots [ index ] = new AllocatorRootHeader( root, sizeof( Type ), typeid( Type ).name( ) );
	
		if ( Allocator::_instance != nullptr )
		{
			_roots [ index ]->_nameIndex = Allocator::_instance->RegisterName( _roots [ index ]->_namePtr );
		}
		_activeRootCount++;
		return true;
	}

	return false;
}