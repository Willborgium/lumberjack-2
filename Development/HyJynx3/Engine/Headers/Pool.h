#ifndef HYJYNX_DESIGN_POOL_H
#define HYJYNX_DESIGN_POOL_H

#include "DynamicCollection.h"

namespace HyJynxDesign
{
	template <typename Type>
	class Pool
	{
	protected:
		struct PoolItem
		{
			bool IsFree = true;
			Type* Value = nullptr;

			PoolItem( )
			{
			}

			PoolItem( const PoolItem& rhs )
				: IsFree( rhs.IsFree ), Value( rhs.Value )
			{
			}

			PoolItem( bool isFree, Type* value )
				: IsFree( isFree ), Value( value )
			{
			}
		};

		HyJynxCollections::DynamicCollection<PoolItem*> _pool;

		unsigned int _maxCapacity = 100;

	public:
		Pool( )
		{
		}

		Pool( const Pool<Type>& rhs )
			: _pool( rhs._pool )
		{
		}

		Pool( unsigned int initialCapacity )
		{
			if ( initialCapacity > _maxCapacity )
			{
				_maxCapacity = initialCapacity;
			}

			for ( unsigned int index = 0; index < initialCapacity; index++ )
			{
				_pool.Append( new PoolItem( true, new Type( ) ) );
			}
		}

		Pool( unsigned int initialCapacity, unsigned int maxCapacity )
			: _maxCapacity( maxCapacity )
		{
			if ( initialCapacity > _maxCapacity )
			{
				_maxCapacity = initialCapacity;
			}

			for ( unsigned int index = 0; index < initialCapacity; index++ )
			{
				_pool.Append( new PoolItem( true, new Type( ) ) );
			}
		}

		unsigned int GetPoolSize( ) const
		{
			return _pool.Count( );
		}

		unsigned int GetMaxCapacity( ) const
		{
			return _maxCapacity;
		}

		bool SetMaxCapacity( unsigned int value )
		{
			bool output = false;

			if ( value >= _pool.Count( ) )
			{
				_maxCapacity = value;
				output = true;
			}

			return output;
		}

		void IncreaseCapacity( unsigned int amount )
		{
			_maxCapacity += amount;
		}

		Type* GetItem( )
		{
			Type* output = nullptr;

			auto freeItem = _pool.First( [] ( PoolItem* item )
			{
				return item->IsFree;
			} );

			if ( freeItem == nullptr && _pool.Count( ) < _maxCapacity )
			{
				freeItem = new PoolItem( true, new Type( ) );
				_pool.Append( freeItem );
			}

			if ( freeItem != nullptr )
			{
				freeItem->IsFree = false;
				output = freeItem->Value;
			}

			return output;
		}

		bool Free( Type* value )
		{
			bool output = false;

			auto freeItem = _pool.First( [&value] ( PoolItem* item )
			{
				return item->Value == value;
			} );

			if ( freeItem != nullptr )
			{
				freeItem->IsFree = true;
				freeItem->Value = new Type( );
				output = true;
			}

			return output;
		}
	};
}

#endif