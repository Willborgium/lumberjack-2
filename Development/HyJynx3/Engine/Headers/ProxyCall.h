#ifndef HYJYNX_DESIGN_PROXY_CALL_H
#define HYJYNX_DESIGN_PROXY_CALL_H

#include "FuncTable.h"

namespace HyJynxDesign
{
	template <typename T>
	//
	// Provides a predictable way to proxy a call to a function object.
	//
	class ProxyCall
	{
	private:
		std::function<T>* _target;
	public:
		//
		// Initializes a new instance of the ProxyCall template class with the given function
		// object serving as its target.
		// - target:	The function object this proxy represents.
		//
		ProxyCall( std::function<T>* target )
			: _target( target )
		{
		}

		template <typename... Args>
		auto operator()( Args... args )
		{
			return ( *_target )( args... );
		}

		operator std::function<T>( )
		{
			return ( *_target );
		}
	};

	template <typename T, typename U>
	//
	// Creates a proxy to the given method.
	// - t:			The address of the method being proxied.
	// - returns:	A ProxyCall object able to call the given method.
	//
	ProxyCall<T> Proxy( U* t )
	{
		auto exists = FuncTable::Instance( )->Contains<T>( t );

		if ( exists == nullptr )
		{
			exists = new std::function<T>( *t );
			FuncTable::Instance( )->Register( t, exists );
		}

		return ProxyCall<T>( exists );
	}
}

#endif