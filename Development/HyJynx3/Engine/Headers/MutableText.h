#ifndef HYJYNX_CORE_MUTABLE_TEXT_H
#define HYJYNX_CORE_MUTABLE_TEXT_H

namespace HyJynxCore
{
	class Text;

	//
	// Provides basic operations for editing text in place.
	//
	class MutableText
	{
	protected:
		char* _buffer;
		unsigned int _textLength;
		unsigned int _bufferSize;
		const unsigned int _growthRate = 128;

		void initialize( const char* value, unsigned int bufferSize );

		bool matches( unsigned int startIndex, const char* value, unsigned int length );
		
		unsigned int grow( unsigned int requestedLength );

		void terminate( );

		void clear( );

	public:
		//
		// Initializes a default instance of the MutableText class.
		//
		MutableText( );

		//
		// Initializes an instance of the MutableText class with a buffer of the given size.
		// - bufferSize:	The initial capacity of the buffer.
		//
		MutableText( unsigned int bufferSize );

		//
		// Initializes an instance of the MutableText class with the given value.
		// - initialValue:	The initial value of the buffer.
		//
		MutableText( const char* initialValue );

		//
		// Initializes an instance of the MutableText class with the given value
		// and a buffer of the given size.
		// - initialValue:	The initial value of the buffer.
		// - bufferSize:	The initial capacity of the buffer.
		//
		MutableText( const char* initialValue, unsigned int bufferSize );

		//
		// Initializes an instance of the MutableText class with the given value.
		// - rhs:	The initial value of the buffer.
		//
		MutableText( const Text& rhs );

		//
		// Initializes an instance of the MutableText class with the given value.
		// and a buffer of the given size.
		// - initialValue:	The initial value of the buffer.
		// - bufferSize:	The initial capacity of the buffer.
		//
		MutableText( const Text& initialValue, unsigned int bufferSize );

		//
		// Initializes an instance of the MutableText class by copying the provided MutableText.
		// - rhs:	The MutableText object to copy.
		//
		MutableText( const MutableText& rhs );

		//
		// Copies the given value on the end of the text.
		// - value:	The value to copy.
		//
		void Append( const char value );

		//
		// Copies the given value on the end of the text.
		// - value:	The value to copy.
		//
		void Append( const char* value );

		//
		// Copies the given value on the end of the text.
		// - value:	The value to copy.
		//
		void Append( const Text& value );

		MutableText& operator+=( const char value );

		MutableText& operator+=( const char* value );

		MutableText& operator+=( const Text& value );

		//
		// Replaces all occurances of a character with another character.
		// - oldValue:	The value to replace.
		// - newValue:	The value to replace with.
		//
		void Replace( const char oldValue, const char newValue );

		//
		// Replaces all occurances of a given character array with another character array.
		// - oldValue:	The value to replace.
		// - newValue:	The value to replace with.
		//
		void Replace( const char* oldValue, const char* newValue );

		//
		// Replaces all occurances of a given Text with another Text.
		// - oldValue:	The value to replace.
		// - newValue:	The value to replace with.
		//
		void Replace( const Text& oldValue, const Text& newValue );

		//
		// Prepends the text with the given Text.
		// - value:	The text to preprend.
		// - count: The number of times to prepend the value.
		//
		void PadLeft( const Text& value, unsigned int count );

		//
		// Prepends the text with the given character array.
		// - value:	The text to preprend.
		// - count: The number of times to prepend the value.
		//
		void PadLeft( const char* value, unsigned int count );

		//
		// Prepends the text with the given character.
		// - value:	The character to preprend.
		// - count: The number of times to prepend the value.
		//
		void PadLeft( const char value, unsigned int count );

		//
		// Prepends the text with whitespace.
		// - count: The number characters of whitespace to prepend.
		//
		void PadLeft( unsigned int count );

		//
		// Appends the text with the given Text.
		// - value:	The text to appends.
		// - count: The number of times to appends the value.
		//
		void PadRight( const Text& value, unsigned int count );

		//
		// Appends the text with the given character array.
		// - value:	The text to appends.
		// - count: The number of times to appends the value.
		//
		void PadRight( const char* value, unsigned int count );

		//
		// Appends the text with the given character.
		// - value:	The character to append.
		// - count: The number of times to append the value.
		//
		void PadRight( const char value, unsigned int count );

		//
		// Apprends the text with whitespace.
		// - count: The number characters of whitespace to append.
		//
		void PadRight( unsigned int count );

		//
		// Converts the text to uppercase.
		//
		void ToUpper( );

		//
		// Converts the text to lowercase.
		//
		void ToLower( );

		//
		// Removes a given number of characters from the beginning of the text.
		// - count:	The number of characters to remove.
		//
		void RemoveFromStart( unsigned int count );

		//
		// Removes all leading occurances of the given character.
		// - value:	The character to remove.
		//
		void TrimStart( const char value );

		//
		// Removes a given number of characters from the end of the text.
		// - count:	The number of characters to remove.
		//
		void RemoveFromEnd( unsigned int count );

		//
		// Removes all trailing occurances of the given character.
		// - value:	The character to remove.
		//
		void TrimEnd( const char value );

		//
		// Removes all leading and trailing occurances of the given character.
		// - value:	The character to remove.
		//
		void Trim( const char value );

		//
		// Removes all leading and trailing occurances of whitespace.
		// - value:	The character to remove.
		//
		void TrimWhitespace( );

		//
		// Retrieves the resulting text from the MutableText object and clears the buffer.
		// - returns:	The result of the operations performed on the MutableText object
		//				since the last call to GetValue or it's initialization.
		//
		char* GetValue( );

		//
		// Returns the length of the text contained in the buffer.
		//
		unsigned int TextLength( ) const;

		//
		// Returns the size in bytes of the buffer containing the text.
		//
		unsigned int BufferSize( ) const;

		//
		// Returns a character array that is a substring of the current buffer.
		// - startIndex:	The first index in the buffer of the first character of the substring.
		// - endIndex:		The index in the buffer of the last character of the substring.
		// - returns:		A character array representing the substring.
		//
		char* GetSubText( unsigned int startIndex, unsigned int endIndex ) const;

		char CharacterAt( unsigned int index ) const;

		char operator[]( unsigned int index ) const;

		bool operator==( const char* rhs ) const;

		bool operator!=( const char* rhs ) const;

		bool operator==( const char rhs ) const;

		bool operator!=( const char rhs ) const;

		bool operator==( const MutableText& rhs ) const;

		bool operator!=( const MutableText& rhs ) const;

		bool operator==( const Text& rhs ) const;

		bool operator!=( const Text& rhs ) const;

		MutableText& operator=( const char rhs );

		MutableText& operator=( const char* rhs );

		MutableText& operator=( const MutableText& rhs );

		MutableText& operator=( const Text& rhs );

		operator char*( );
	};
}

#endif