#ifndef	COLLECTIONS_QUEUE_H
#define HYJYNX_COLLECTIONS_QUEUE_H

#include "DynamicCollection.h"

namespace HyJynxCollections
{
	template <typename ValueType>
	//
	// Provides an API for a first in, first out Queue based on a DynamicCollection.
	//
	class Queue : public DynamicCollection<ValueType>
	{
	public:
		//
		// Initializes a default instance of the Queue class.
		//
		Queue( )
			: DynamicCollection<ValueType>( )
		{
		}

		//
		// Initializes the queue by copying the elements of another collection.
		// - collection:	The collection to copy.
		//
		Queue( ICollection<ValueType>* collection )
			: DynamicCollection<ValueType>( collection )
		{
		}

		//
		// Adds the given value to the end of the queue.
		// - value: The value to be added.
		//
		void Push( ValueType value )
		{
			Append( value );
		}

		//
		// Removes and returns the first element in the collection.
		// - returns: The first element in the collection.
		//
		ValueType Pop( )
		{
			ValueType val = ElementAt( 0 );
			RemoveAt( 0 );
			return val;
		}

		//
		// Returns the first element in the Queue without removing it.
		// - returns: The first element in the Queue.
		// 
		ValueType Peek()
		{
			return First();
		}
	};
}

#endif