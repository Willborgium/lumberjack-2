#ifndef HYJYNX_DESIGN_SERVICE_LOCATOR_H
#define HYJYNX_DESIGN_SERVICE_LOCATOR_H

#include "Singleton.h"
#include "Text.h"
#include "Dictionary.h"

namespace HyJynxDesign
{
	template <typename ServiceBase>
	//
	// Provides a singleton service locator with an API for
	// managing services statically.
	//
	class ServiceLocator : public Singleton<ServiceLocator<ServiceBase>>
	{
	protected:
		HyJynxCollections::Dictionary<HyJynxCore::Text, ServiceBase*> _services;

		ServiceBase* Locate(HyJynxCore::Text key)
		{
			ServiceBase* result = nullptr;

			if (_services.ContainsKey(key))
			{
				result = _services[key];
			}

			return result;
		}
	public:
		//
		// Associates the given service with the provided key.
		// - key: The key used to retrieve the service.
		// - service: The service being registered.
		// - returns: True if the given service was registered. Otherwise returns false,
		//            implying the given key is already present in the service locator.
		//
		bool RegisterService(HyJynxCore::Text key, ServiceBase* service)
		{
			bool result = false;

			if (Locate(key) == nullptr)
			{
				result = true;
				_services.Append(key, service);
			}

			return result;
		}

		template <typename ServiceType>
		//
		// Retrieves the service associated with the given key.
		// - key: The key to look for in the services dictionary.
		// - returns: The service associated with the given key. If the given key
		//            was not present, this value is nullptr.
		//
		ServiceType* Locate(HyJynxCore::Text key)
		{
			return static_cast<ServiceType*>(Locate(key));
		}

		//
		// Removes the service associated with the given key.
		// - key: The key used to look up the service to be removed.
		// - returns: The service that was removed. If no service is associated
		//            with the given key, this value is nullptr.
		//
		ServiceBase* UnRegisterService(HyJynxCore::Text key)
		{
			ServiceBase* output = Locate(key);

			if (nullptr != output)
			{
				_services.Remove(key);
			}

			return output;
		}
	};
}

#endif