#ifndef HYJYNX_CORE_HIGH_PRECISION_TIMER_H
#define HYJYNX_CORE_HIGH_PRECISION_TIMER_H

#include "TypeDefinitions.h"
#include "TimeSpan.h"
#include <functional>
#include <Windows.h>

namespace HyJynxCore
{
	//
	// Provides an enumeration of measurements of time.
	//
	class TimeLength abstract sealed
	{
	public:
		//
		// 1/1,000,000 of one second.
		//
		static const double OneMicrosecond;

		//
		// 1/1,000 of one second.
		//
		static const double OneMillisecond;

		//
		// One second. Did you really need this comment?
		//
		static const double OneSecond;
	};

	typedef std::function<void(void)> CallbackMethod;

	//
	// Provides a way to perform granular timing.
	// 
	class HighPrecisionTimer sealed
	{
	protected:
		double _frequency;
		LongLong _startTime;
		LongLong _nextTick;
		LongLong _interval;
		UInt _currentInterval;
		UInt _intervalCount;
		bool _loopInfinitely;
		bool _useCallback;

		bool _isRunning;

		CallbackMethod _callback;
	public:
		//
		// Initializes a new instance of the HighPrecisionTimer class.
		//
		HighPrecisionTimer( );

		//
		// Sets the method that will be executed each time the timer ticks.
		// - callback: The method that will be executed when the timer ticks.
		//
		void SetCallback( CallbackMethod callback );

		//
		// Sets whether or not the timer should execute a callback when it ticks.
		// - useCallback:	Sets a flag indicating whether a callback will be executed
		//					when the timer ticks.
		//
		void SetUseCallback( bool useCallback );

		//
		// Sets the length of time between ticks.
		// - interval: The time between ticks.
		//
		void SetInterval( TimeSpan interval );

		//
		// Sets the number of ticks the timer will run before it will stop.
		// - count: The number of ticks before the timer is stopped.
		//
		void SetIntervalCount( UInt count );

		//
		// Set whether or not the timer will loop indefinitely.
		// - infiniteLoop: Sets a flag indicating whether the timer will loop indefinitely.
		//
		void SetInfiniteLooping( bool infiniteLoop );

		//
		// Starts the timer.
		//
		void Start( );

		//
		// Stops the timer.
		//
		void Stop( );

		//
		// Gets the amount of time that has elapsed since the timer was started.
		// - returns: The amount of time that has passed since the timer was started.
		//
		TimeSpan GetElapsed( );

		//
		// Returns the amount of time, in system ticks, since the timer was started.
		// - returns: The amount of time, in system ticks, since the timer was started.
		//
		LongLong GetElapsedTicks();

		//
		// Update the timer.
		//
		void Update( );

		//
		// Returns whether or not the timer is currently running.
		// - returns: A flag indicating whether the time is running.
		//
		bool IsRunning( ) const;
	};
}

#endif