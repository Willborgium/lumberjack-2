#ifndef HYJYNX_COLLECTIONS_DEQUEUE_H
#define HYJYNX_COLLECTIONS_DEQUEUE_H

#include "DynamicCollection.h"

namespace HyJynxCollections
{
	template <typename ValueType>
	//
	// Provides an API for a DynamicCollection for a double ended queue.
	//
	class Dequeue : public DynamicCollection<ValueType>
	{
	public:
		Dequeue( )
			: DynamicCollection<ValueType>( )
		{
		}

		//
		// Initializes a double ended queue from a previous collection.
		// - collection:	The collection to copy.
		//
		Dequeue( ICollection<ValueType>* collection )
			: DynamicCollection<ValueType>( collection )
		{
		}

		//
		// Adds the given value into the collection as the first element.
		// - value:	The value to be added to the collection.
		//
		void PushRoot( ValueType value )
		{
			if( _count == 0 )
			{
				Append( value );
			}
			else
			{
				InsertAt( 0 );
			}
		}

		//
		// Adds the given value into the collection as the last element.
		// - value:	The value to be added to the collection.
		//
		void PushEnd( ValueType value )
		{
			Append( value );
		}

		//
		// Removes and returns the element at the begining of the collection.
		// - returns:	The value that was removed.
		//
		ValueType PopRoot( )
		{
			ValueType val = ElementAt( 0 );
			RemoveAt( 0 );
			return val;
		}

		//
		// Removes and returns the element at the end of the collection.
		// - returns: The value that was removed.
		//
		ValueType PopEnd( )
		{
			ValueType val = ElementAt( _count );
			RemoveAt( _count );
			return val;
		}
	};
}

#endif