#ifndef HYJYNX_CORE_CONVERTER_INTERFACE_H
#define HYJYNX_CORE_CONVERTER_INTERFACE_H

namespace HyJynxCore
{
	//
	// Provides an API for converting data.
	//
	class IConverter
	{
	public:

		//
		// Converts the given data using an underlying conversion method.
		// - object:	The data to be converted.
		// - returns:	When overriden in a derived class, this method returns the data
		//				returned by the underlying conversion method.
		//
		virtual void* Convert( void* object ) = 0;
	};
}

#endif