#ifndef HYJYNX_DESIGN_LISTENER_INTERFACE_H
#define HYJYNX_DESIGN_LISTENER_INTERFACE_H

#include "Text.h"
#include "Data.h"

namespace HyJynxDesign
{
	//
	// Provides an API for an object to receive data and messages.
	//
	class IListener : public HyJynxCore::Data
	{
	public:
		//
		// Enforces that subclasses must set the name of the IListener.
		// - name: The name of the object.
		// 
		IListener(HyJynxCore::Text name)
			: HyJynxCore::Data(name)
		{
		}

		//
		// When overridden in a derived class, this method handles receiving a text message.
		// - message: The message, as text, that has been sent by an observable.
		//
		virtual void ReceiveMessage(HyJynxCore::Text message) = 0;

		//
		// When overridden in a derived class, this method handles receiving data.
		// - data: The data that has been sent by an observable.
		//
		virtual void ReceiveNotification(HyJynxCore::Data* data) = 0;
	};
}

#endif