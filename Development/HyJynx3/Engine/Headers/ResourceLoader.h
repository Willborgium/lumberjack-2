#ifndef HYJYNX_CORE_RESOURCE_LOADER_H
#define HYJYNX_CORE_RESOURCE_LOADER_H

#include "Converter.h"
#include "Singleton.h"
#include "Dictionary.h"
#include "Text.h"
#include "TypeDefinitions.h"
#include "ResourceController.h"

namespace HyJynxCore
{
	//
	// Provides a single channel for loading resources from external sources.
	//
	class ResourceLoader : public HyJynxDesign::Singleton<ResourceLoader>
	{
	protected:

		friend HyJynxDesign::Singleton<ResourceLoader>;

		HyJynxCore::Converter* _converter;
		HyJynxCore::ResourceController* _resources;

		ResourceLoader( )
		{
			_converter = Converter::Instance( );
			_resources = HyJynxCore::ResourceController::Instance( );
		}
	public:

		//
		// Loads the given file into the resource controller.
		// - fileName:	The path to the file to be loaded.
		// - returns:	A falg indicating whether the file was successfully loaded.
		//
		bool LoadFile( HyJynxCore::Text fileName );
	};
}

#endif