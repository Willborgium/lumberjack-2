#ifndef HYJYNX_COLLECTIONS_KEY_VALUE_PAIR_H
#define HYJYNX_COLLECTIONS_KEY_VALUE_PAIR_H

namespace HyJynxCollections
{
	template <typename KeyType, typename ValueType>
	//
	// Provides a structure that associates two values.
	//
	struct KeyValuePair
	{
		//
		// The key to be used to retrieve the given value.
		//
		KeyType Key;

		//
		// The value associated with the key.
		//
		ValueType Value;

		//
		// Initializes an instance of the KeyValuePair class.
		//
		KeyValuePair( ) { }

		//
		// Initializes an instance of the KeyValuePair class with the given key and value.
		// - key:		The key associated with the given value.
		// - value:		The value associated with the given key.
		//
		KeyValuePair( KeyType key, ValueType value )
			: Key( key ), Value( value )
		{
		}

		//
		// Compares the keys of the given KeyValuePair and this object.
		// - rhs:		The KeyValuePair to compare against.
		// - returns:	A flag indicating equality.
		//
		bool operator==(KeyValuePair<KeyType, ValueType>& rhs)
		{
			return Key == rhs.Key;
		}

		//
		// Compares the keys of the given KeyValuePair and this object.
		// - rhs:		The KeyValuePair to compare against.
		// - returns:	A flag indicating inequality.
		//
		bool operator!=(KeyValuePair<KeyType, ValueType>& rhs)
		{
			return Key != rhs.Key;
		}
	};
}

#endif