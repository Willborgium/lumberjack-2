#ifndef HYJYNX_TYPE_DEFINITIONS_H
#define HYJYNX_TYPE_DEFINITIONS_H

#include <time.h>

typedef char					Small;
typedef unsigned char			USmall;
typedef unsigned int			UInt;
typedef unsigned short			UShort;
typedef unsigned long			ULong;
typedef long long				LongLong;
typedef unsigned long long		ULongLong;
typedef time_t					TimeStamp;
typedef long long				TimeCode;

#endif