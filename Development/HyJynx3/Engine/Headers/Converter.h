#ifndef HYJYNX_CORE_CONVERTER_H
#define HYJYNX_CORE_CONVERTER_H

#include "TypeDefinitions.h"
#include "Text.h"
#include "Dictionary.h"
#include "IConverter.h"
#include "MemoryManagement.h"
#include "Singleton.h"

#include <sal.h>

namespace HyJynxCore
{
	//
	// Provides a static point of access to a system that converts data between types
	// via user-defined converters that implement the IConverter interface.
	//
	class Converter : public HyJynxDesign::Singleton<Converter>
	{
	private:

		friend HyJynxDesign::Singleton<Converter>;

		struct ConverterKey
		{
			Text FromType;
			Text ToType;

			UInt FromTypeSize;
			UInt ToTypeSize;

			ConverterKey( );

			ConverterKey( _In_ Text from, _In_ Text to, _In_ UInt fromSize, _In_ UInt toSize );

			bool operator==( _In_ ConverterKey& rhs );

			bool operator!=( _In_ ConverterKey& rhs );
		};

		HyJynxCollections::Dictionary<ConverterKey, IConverter*> _converters;

		IConverter* GetConverter( _In_ Text fromType, _In_ Text toType, _Inout_ UInt* fromSize, _Inout_ UInt* toSize );

		void AddConverter( _In_ Text fromType, _In_ Text toType, _In_ IConverter* converter, _In_ UInt fromSize, _In_ UInt toSize );

		template <typename Type>
		Text GetTypeName( )
		{
			return const_cast< char* >( typeid( Type ).name( ) );
		}

		Converter( )
		{
		}

	public:

		template <typename FromType, typename ToType, typename ConverterType>
		//
		// Adds a new converter, of type ConverterType, to the conversion system. The converter should be
		// able to convert from FromType to ToType.
		//
		void AddConverter( )
		{
			AddConverter( GetTypeName<FromType>( ), GetTypeName<ToType>( ), anew( ConverterType )( ), sizeof( FromType ), sizeof( ToType ) );
		}

		template <typename FromType, typename ToType>
		//
		// Adds a new converter that is capable of converting from FromType to ToType.
		// - converter: The converter to be added, which can convert from FromType to ToType.
		//
		void AddConverter( _In_ IConverter* converter )
		{
			AddConverter( GetTypeName<FromType>( ), GetTypeName<ToType>( ), converter, sizeof( FromType ), sizeof( ToType ) );
		}

		template <typename ToType, typename FromType>
		//
		// Convert the given object from 'fromType' to 'toType'.
		// - fromType:	The name of the data type the data is currently in.
		// - toType:	The name of the data type the data will be converted to.
		// - object:	The object data to be converted.
		// - returns:	Returns the converted data. If no converter exists for the given 'fromType' and 'toType',
		//				this method returns nullptr.
		//
		ToType Convert( _In_ FromType object )
		{
			IConverter* converter = GetConverter( const_cast< char* >( typeid( FromType ).raw_name( ) ), const_cast< char* >( typeid( ToType ).raw_name( ) ), nullptr, nullptr );

			if ( converter != nullptr )
			{
				return ( ToType ) converter->Convert( object );
			}

			return nullptr;
		}

		//
		// Converts the given data using a converter whose FromType matches the given fromType and
		// ToType matches the given toType. The data is returned unaliased, and the outputSize
		// argument is filled with the size, in bytes, of the data returned.
		// - fromType:		The text type of the input data.
		// - toType:		The text type of the expected output.
		// - data:			The data to be converter from fromType to toType.
		// - outputSize:	The size, in bytes, of the return value.
		// - returns:		Returns the unaliased converted data. If no converter exists for the given 'fromType' and 'toType',
		//					this method returns nullptr.
		//
		void* Convert( _In_ Text& fromType, _In_ Text& toType, _In_ void* data, _Inout_ UInt* outputSize );

		template <typename ToType, typename FromType>
		//
		// Returns a value indicating whether a converter exists for the given FromType and ToType.
		// - returns: A flag indicating whether or not a converter exists for the requested types.
		//
		bool ConverterExists( )
		{
			GetConverter( const_cast< char* >( typeid( FromType ).raw_name( ) ), const_cast< char* >( typeid( ToType ).raw_name( ) ), nullptr, nullptr ) != nullptr;
		}

		//
		// Returns a value indicating whether a converter exists for the given FromType and ToType.
		// - returns: A flag indicating whether or not a converter exists for the requested types.
		//
		bool ConverterExists( _In_ Text& fromType, _In_ Text& toType );
	};
}

#endif