#include "ScriptableBase.h"
#include "MemoryManagement.h"

template <typename...ArgTypes>
ScriptableBase::MethodInfo* ScriptableBase::GetMethodInfo(void(*method)(ArgTypes...))
{
	MethodInfo* output = nullptr;

	if (method != nullptr)
	{
		output = anew(MethodInfo)();

		output->Address = (void*)method;
		output->HasReturnValue = false;
		output->ReturnValueSize = 0;
		output->ParameterCount = sizeof...(ArgTypes);

		return output;
	}
}

template <typename ReturnType, typename...ArgTypes>
ScriptableBase::MethodInfo* ScriptableBase::GetMethodInfo(ReturnType(*method)(ArgTypes...))
{
	MethodInfo* output = nullptr;

	if (method != nullptr)
	{
		output = anew(MethodInfo)();

		output->Address = (void*)method;
		output->HasReturnValue = true;
		output->ReturnValueSize = sizeof(ReturnType);
		output->ParameterCount = sizeof...(ArgTypes);
	}

	return output;
}

template <typename First, typename...Rest>
void ScriptableBase::DefineParameters(ScriptableBase::MethodInfo* method, ...)
{
	ScriptableBase::ParameterInfo* p = anew(ScriptableBase::ParameterInfo)();

	p->Size = sizeof(First);
	p->DataType = &typeid(First);
	p->TypeName = p->DataType->raw_name();

	method->ParameterTotalSize += p->Size;
	method->Parameters.Append(p);

	if (sizeof...(Rest) > 0)
	{
		DefineParameters<Rest...>(method, 42);
	}
}

template <typename First>
void ScriptableBase::DefineParameters(ScriptableBase::MethodInfo* method, int)
{
	ScriptableBase::ParameterInfo* p = anew(ScriptableBase::ParameterInfo)();

	p->Size = sizeof(First);
	p->DataType = &typeid(First);
	p->TypeName = p->DataType->raw_name();

	method->ParameterTotalSize += p->Size;
	method->Parameters.Append(p);
}

template <typename Type>
struct ScriptableBase::IsPointer
{
	static const bool value = false;
};

template <typename Type>
struct ScriptableBase::IsPointer<Type*>
{
	static const bool value = true;
};
template <typename ReturnType, typename...ArgTypes>
void ScriptableBase::AddMethod(HyJynxCore::Text name, ReturnType(*method)(ArgTypes...))
{
	ScriptableBase::MethodInfo* m = GetMethodInfo(method);

	m->ParameterTotalSize = 0;

	if (m->ParameterCount > 0)
	{
		DefineParameters<ArgTypes...>(m);
	}

	_methods.Append(name, m);
}

template <typename Type>
Type ScriptableBase::CallMethod(HyJynxCore::Text name, HyJynxCollections::ICollection<ScriptableBase::ScriptParameter*>* parameters, UInt* returnSize)
{
	void* result = CallMethod(name, parameters, returnSize);

	Type* handle = (Type*)result;
	return *handle;
}