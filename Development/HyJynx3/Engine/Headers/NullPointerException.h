#ifndef HYJYNX_CORE_NULL_POINTER_EXCEPTION_H
#define HYJYNX_CORE_NULL_POINTER_EXCEPTION_H

#include "Exception.h"
#include "Text.h"

namespace HyJynxCore
{
	//
	// Provides probably the most useful exception in the native programming world.
	//
	class NullPointerException : public Exception
	{
	public:
		//
		// Initializes a new instance of the NullPointerException class.
		// - name:	The name of the value that is nullptr.
		// - type:	The type of the value that is nullptr.
		//
		NullPointerException( Text name, Text type )
			: Exception( "Null Pointer Exception: Pointer '{0}' of type '{1}' is null." )
		{
			_message = _message.Replace( Text( "{0}" ), name );
			_message = _message.Replace( Text( "{1}" ), type );
		}
	};
}

#endif