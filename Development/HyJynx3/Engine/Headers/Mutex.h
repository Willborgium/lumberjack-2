#ifndef HYJYNX_CORE_MUTEX_H
#define HYJYNX_CORE_MUTEX_H

namespace HyJynxCore
{
	//
	// Provides an object capable of blocking a thread using a shared resource.
	//
	class Mutex
	{
	protected:
		bool* _handle;
		bool _isCurrentLocked;
	public:
		//
		// Initializes a new instance of the Mutex class with a target to lock.
		// - target:	The value to be used when locking.
		//
		Mutex ( bool* target )
			: _handle ( target ), _isCurrentLocked ( false )
		{
		}
		//
		// Initializes a new instance of the Mutex class with a target to lock.
		// - target:	The value to be used when locking.
		//
		Mutex( const bool* target )
			: _handle( const_cast<bool*>( target ) ), _isCurrentLocked( false )
		{
		}

		Mutex ( ) = delete;
		Mutex ( const Mutex&& rhs ) = delete;

		const Mutex& operator=( const Mutex&& rhs ) = delete;

		//
		// Locks the held resource. If the resource is already acquired by another lock,
		// the current thread will spin-wait.
		//
		void Lock ( );

		//
		// Attempts to lock the held resource. If the resource is already acquired by another lock,
		// the current thread will continue execution without having acquired the resource.
		// - returns:	A flag indicating whether the resource was acquired.
		//
		bool TryLock ( );

		//
		// Releases the acquisition of the lock, if one was held.
		//
		void Unlock ( );

		virtual ~Mutex ( );
	};
}

#endif