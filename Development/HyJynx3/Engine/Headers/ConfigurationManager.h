#ifndef HYJYNX_CORE_CONFIGURATION_MANAGER_H
#define HYJYNX_CORE_CONFIGURATION_MANAGER_H

#include "Singleton.h"
#include "JsonParser.h"

#include <sal.h>

namespace HyJynxCore
{
	//
	// Provides an point of access to a configuration file.
	//
	class ConfigurationManager : public HyJynxDesign::Singleton<ConfigurationManager>
	{
	protected:

		friend HyJynxDesign::Singleton<ConfigurationManager>;

		Text _fileName;
		JsonData* _configFile = nullptr;
		bool _isLoaded;

		ConfigurationManager( );

		ConfigurationManager( _In_ const ConfigurationManager& rhs );

		ConfigurationManager( _In_ const ConfigurationManager&& rhs );

		JsonData* EvaluateToLast( _In_ Text* expParts, _In_ UInt tokenCount );

	public:

		~ConfigurationManager( );

		//
		// Attempts to load the given JSON configuration file into memory.
		// - fileName:	The name of the file to load.
		// - returns:	A flag indicating whether the file was successfully loaded.
		//
		bool LoadFile( _In_ Text fileName = "" );

		//
		// Evaluates the given expression to a JsonObject*.
		// - expression:	The logic path to the requested object.
		// - returns:		If the expression evaluates succesfully, returns the object. Otherwise,
		//					returns nullptr.
		//
		JsonObject* GetObjectValue( _In_ Text expression );

		//
		// Evaluates the given expression to Text.
		// - expression:	The logic path to the requested Text.
		// - returns:		If the expression evaluates succesfully, returns the Text. Otherwise,
		//					returns Text::Empty( ).
		//
		Text GetTextValue( _In_ Text expression );

		//
		// Evaluates the given expression to an integer.
		// - expression:	The logic path to the requested integer.
		// - returns:		If the expression evaluates succesfully, returns the integer. Otherwise,
		//					returns 0.
		//
		int GetIntValue( _In_ Text expression );

		//
		// Evaluates the given expression to a double.
		// - expression:	The logic path to the requested double.
		// - returns:		If the expression evaluates succesfully, returns the double. Otherwise,
		//					returns 0.0.
		//
		double GetDoubleValue( _In_ Text expression );

		//
		// Evaluates the given expression to a boolean.
		// - expression:	The logic path to the requested boolean.
		// - returns:		If the expressive evaluates successfully, returns the double. Otherwise,
		//					returns false.
		//
		bool GetBooleanValue( _In_ Text );

	};
}

#endif