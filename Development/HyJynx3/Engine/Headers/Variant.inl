#include "Variant.h"


template <typename Type>
Variant::Variant( Type& value )
	: _value( nullptr ), _type( nullptr ), _isPointer( false )
{
	operator=( value );
}

template <typename Type>
Variant::Variant( const Type& value )
	: _value( nullptr ), _type( nullptr ), _isPointer( false )
{
	operator=( value );
}

template <typename Type>
Variant& Variant::operator=( Type& rhs )
{
	if( _value != nullptr && !_isPointer )
	{
		delete _value;
	}

	_isPointer = false;
	_value = malloc( sizeof( Type ) );
	memcpy( _value, &rhs, sizeof( Type ) );
	_type = const_cast<type_info*>( &typeid( Type ) );
	return *this;
}

template <typename Type>
Variant& Variant::operator=( Type* rhs )
{
	if( !_isPointer )
	{
		delete _value;
	}

	_isPointer = true;
	_value = (void*)rhs;
	_type = const_cast<type_info*>( &typeid( Type ) );

	return *this;
}

template <typename Type>
Type Variant::As( )
{
	if( !_isPointer )
	{
		return  *reinterpret_cast<Type*>( _value );
	}
	else
	{
		return  *reinterpret_cast<Type*>( &_value );
	}
}

template <typename Type>
Variant::operator Type( )
{
	return As<Type>( );
}

template <typename Type>
bool Variant::Is( )
{
	return _type != nullptr ?  *_type == typeid( Type ) : false;
}

template <typename Type>
bool Variant::operator==( const Type& rhs )
{
	if( _value != nullptr )
	{
		if(!_isPointer)
		{
			return *reinterpret_cast<Type*>( _value ) == rhs;
		}
		else
		{
			return  *reinterpret_cast<Type*>( &_value ) == rhs;
		}
	}

	return false;
}

template <typename Type>
bool Variant::operator!=( const Type& rhs )
{
	return ! operator==( rhs );
}

template <typename Type>
Type Variant::operator+( const Type& rhs )
{
	if( _value != nullptr )
	{
		return As<Type>( ) + rhs;
	}

	return 0;
}

template <typename Type>
Type Variant::operator-( const Type& rhs )
{
	if( _value != nullptr )
	{
		return As<Type>( ) - rhs;
	}

	return 0;
}

template <typename Type>
Type Variant::operator*( const Type& rhs )
{
	if( _value != nullptr )
	{
		return As<Type>( ) * rhs;
	}

	return 0;
}

template <typename Type>
Type Variant::operator/( const Type& rhs )
{
	if( _value != nullptr )
	{
		return As<Type>( ) / rhs;
	}

	return 0;
}

template <typename Type>
Type Variant::operator%( const Type& rhs )
{
	if( _value != nullptr )
	{
		return As<Type>( ) % rhs;
	}

	return 0;
}

template <typename Type>
Type Variant::operator^( const Type& rhs )
{
	if( _value != nullptr )
	{
		return As<Type>( ) ^ rhs;
	}

	return 0;
}

template <typename Type>
Type Variant::operator|( const Type& rhs )
{
	if( _value != nullptr )
	{
		return As<Type>( ) | rhs;
	}

	return 0;
}

template <typename Type>
Type Variant::operator&( const Type& rhs )
{
	if( _value != nullptr )
	{
		return As<Type>( ) & rhs;
	}

	return 0;
}

template <typename Type>
Type Variant::operator+=( const Type& rhs )
{
	if( _value != nullptr )
	{
		return operator=( operator+( rhs ) );
	}

	return 0;
}

template <typename Type>
Type Variant::operator-=( const Type& rhs )
{
	if( _value != nullptr )
	{
		return operator=( operator-( rhs ) );
	}

	return 0;
}

template <typename Type>
Type Variant::operator*=( const Type& rhs )
{
	if( _value != nullptr )
	{
		return operator=( operator*( rhs ) );
	}

	return 0;
}

template <typename Type>
Type Variant::operator/=( const Type& rhs )
{
	if( _value != nullptr )
	{
		return operator=( operator/( rhs ) );
	}

	return 0;
}

template <typename Type>
Type Variant::operator%=( const Type& rhs )
{
	if( _value != nullptr )
	{
		return operator=( operator%( rhs ) );
	}

	return 0;
}

template <typename Type>
Type Variant::operator^=( const Type& rhs )
{
	if( _value != nullptr )
	{
		return operator=( operator^( rhs ) );
	}

	return 0;
}

template <typename Type>
Type Variant::operator|=( const Type& rhs )
{
	if( _value != nullptr )
	{
		return operator=( operator&( rhs ) );
	}

	return 0;
}

template <typename Type>
Type Variant::operator&=( const Type& rhs )
{
	if( _value != nullptr )
	{
		return operator=( operator|( rhs ) );
	}

	return 0;
}

template <typename... Args>
void Variant::operator()(Args... args)
{
	typedef void(*fptr)(Args...);
	reinterpret_cast<fptr>(reinterpret_cast<int>(_value))(std::forward<Args...>(args...));
}

template <typename ReturnType>
ReturnType Variant::Call()
{
	typedef ReturnType(*fptr)(Args...);
	return reinterpret_cast<fptr>(reinterpret_cast<int>(_value))();
}

template <typename ReturnType, typename... Args>
ReturnType Variant::Call(Args... args)
{
	typedef ReturnType(*fptr)(Args...);
	return reinterpret_cast<fptr>(reinterpret_cast<int>(_value))(std::forward<Args...>(args...));
}
		
template <typename ReturnType, typename CallerType>
ReturnType Variant::ThisCall( CallerType* pThis )
{
	typedef ReturnType(CallerType::* fptr)( );
	fptr a;
	int* iiVal = new (&a) int;
	*iiVal = *((int*)_value);
	return (pThis->*a)( );
}

template <typename ReturnType, typename CallerType, typename... Args>
ReturnType Variant::ThisCall(CallerType* pThis, Args... args)
{
	typedef ReturnType(CallerType::* fptr)(Args...);
	fptr aPtr;
	int* iiVal = new (&aPtr) int;
	*iiVal = *((int*)_value);
	(pThis->*aPtr)(std::forward<Args...>(args...));
}

template <typename CallerType>
void Variant::ThisCall(CallerType* pThis)
{
	typedef void(CallerType::* fptr)(Args...);
	fptr aPtr;
	int* iiVal = new (&aPtr) int;
	*iiVal = *((int*)_value);
	(pThis->*aPtr)(std::forward<Args...>(args...));
}

template <typename CallerType, typename... Args>
void Variant::ThisCall( CallerType* pThis, Args... args )
{
	typedef void(CallerType::* fptr)(Args...);
	fptr aPtr;
	int* iiVal = new (&aPtr) int;
	*iiVal = *((int*)_value);
	(pThis->*aPtr)(std::forward<Args...>(args...));
}