#ifndef HYJYNX_DESIGN_SINGLETON_H
#define HYJYNX_DESIGN_SINGLETON_H

#include "MemoryManagement.h"

namespace HyJynxDesign
{
	template <typename InstanceType>
	//
	// Provides a generic base for the Singleton design pattern.
	//
	class Singleton
	{
	protected:
		Singleton(){}
		Singleton(const Singleton& rhs){}

		static InstanceType* _instance;

	public:
		//
		// Returns the single instance of the class type.
		// - returns: The instance associated with this class type.
		//
		static InstanceType* Instance()
		{
			if (nullptr == _instance)
			{
				_instance = new InstanceType( );

				HyJynxMemoryManagement::Allocator::AddRoot( _instance );
			}

			return _instance;
		}
	};

	template <typename InstanceType>
	InstanceType* Singleton<InstanceType>::_instance = nullptr;
}

#endif