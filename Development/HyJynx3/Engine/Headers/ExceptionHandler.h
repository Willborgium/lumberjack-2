#ifndef HYJYNX_CORE_EXCEPTION_HANDLER_H
#define HYJYNX_CORE_EXCEPTION_HANDLER_H

#include "DynamicCollection.h"
#include "Text.h"
#include "Exception.h"
#include "Singleton.h"

#include <functional>

namespace HyJynxCore
{
	typedef std::function<void(Exception*)> ExceptionHandlerMethod;
	//
	// Provides management for exception handling.
	//
	class ExceptionHandler sealed : public HyJynxDesign::Singleton<ExceptionHandler>
	{
	private:

		friend HyJynxDesign::Singleton<ExceptionHandler>;

		HyJynxCollections::DynamicCollection<Exception*> _exceptions;

		ExceptionHandlerMethod _fatalHandler;

		ExceptionHandler();

		void PrivateRaiseException(Exception* exception);

	public:
		//
		// Raises the given exception.
		// - exception: The exception to be raised.
		//
		static void RaiseException(Exception* exception);

		//
		// Raises a fatal exception with the given message.
		// - message: The message to give to the exception that will be raised.
		//
		static void RaiseException(Text message);

		//
		// Raises a warning exception with the given message.
		// - message: The message to give to the exception that will be raised.
		//
		static void RaiseWarning(Text message);

		//
		// Raises a message exception with the given message.
		// - message: The message to give to the exception that will be raised.
		//
		static void RaiseMessage(Text message);

		//
		// Returns a collection of all raised exceptions.
		// - returns: A collection of all raised exceptions.
		//
		HyJynxCollections::DynamicCollection<Exception*>& GetExceptions();

		//
		// Sets the event handler that is called when a fatal exception is raised.
		// - handler: The method that will be called when an exception is raised.
		//
		void SetFatalHandler(ExceptionHandlerMethod handler);
	};
}

#endif