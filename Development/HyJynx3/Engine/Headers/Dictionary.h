#ifndef HYJYNX_COLLECTIONS_DICTIONARY_H
#define HYJYNX_COLLECTIONS_DICTIONARY_H

#include "DynamicCollection.h"
#include "KeyValuePair.h"

namespace HyJynxCollections
{
	template <typename KeyType, typename ValueType>
	//
	// Provides an API for a DynamicCollection that serves as a keyed collection.
	//
	class Dictionary : public DynamicCollection<KeyValuePair<KeyType, ValueType>>
	{
	public:
		Dictionary( )
			: DynamicCollection<KeyValuePair<KeyType, ValueType>>( )
		{
		}

		//
		// Initializes the collection based on another collection.
		// - collection:	The collection to copy.
		//
		Dictionary( ICollection<KeyValuePair<KeyType, ValueType>>* collection )
			: DynamicCollection<KeyValuePair<KeyType, ValueType>>( collection )
		{
		}

		//
		// Returns the value associated with the given key.
		// If the given key is not present in the collection, this method
		// attempts to return a default instance of the given object.
		// - key:		The key of the requested data.
		// - returns:	The value associated with the given key.
		//
		ValueType ValueOf( KeyType key )
		{
			Node* node = GetNode( [&]( Node* n )
				{
					return ( n->_value.Key == key );
				} );

			if( node != nullptr )
			{
				return node->_value.Value;
			}
			else
			{
				return ValueType( );
			}
		}

		//
		// Returns the value associated with the given key.
		// If the given key is not present in the collection, this method
		// attempts to return a default instance of the given object.
		// - key:		The key of the requested data.
		// - returns:	The value associated with the given key.
		//
		ValueType operator[]( KeyType key )
		{
			return ValueOf( key );
		}

		//
		// Associates the given value with the given key and adds it to the collection.
		// - key:	The key associated with the value.
		// - value:	The value associated with the key.
		//
		void Append( KeyType key, ValueType value )
		{
			DynamicCollection<KeyValuePair<KeyType,ValueType>>::Append( KeyValuePair<KeyType,ValueType>( key, value ) );
		}

		//
		// Checks to see if the requested key is present in the collection.
		// - key:		The key to check for.
		// - returns:	A flag indicating whether or not the given key was present in the collection.
		//
		bool ContainsKey( KeyType key )
		{
			return nullptr != GetNode( [&]( Node* n )
				{
					return ( n->_value.Key == key );
				} );
		}

		//
		// Returns a collection of keys that currently exist in the dictionary.
		// - returns:	A collection of keys currently in the dictionary.
		//
		ICollection<KeyType>* GetKeys( )
		{
			DynamicCollection<KeyType>* out = anew(DynamicCollection<KeyType>)();

		}

		//
		// Removes the key from the dictionary.
		// - key:	The key to be removed.
		//
		void Remove(KeyType key)
		{
			auto match = GetNode([&](Node* n)
			{
				return (n->_value.Key == key);
			});

			if (match != nullptr)
			{
				DynamicCollection<KeyValuePair<KeyType, ValueType>>::Remove(match->_value);
			}
		}
	};

	template <typename ValueType> template <typename Selector>
	Dictionary<Selector, ICollection<ValueType>*>* DynamicCollection<ValueType>::GroupBy( std::function<Selector( ValueType )> evaluator )
	{
		Mutex lock( &_mutexHandle );
		lock.Lock( );

		Dictionary<Selector, ICollection<ValueType>*>* output = anew( ( Dictionary<Selector, ICollection<ValueType>*> ) )( );

		Node* ptr = _root;

		while ( ptr != nullptr )
		{
			auto group = evaluator( ptr->_value );

			if ( output->ContainsKey( group ) )
			{
				output->ValueOf( group )->Append( ptr->_value );
			}
			else
			{
				ICollection<ValueType>* newGroup = anew( DynamicCollection<ValueType> )( );

				newGroup->Append( ptr->_value );

				output->Append( group, newGroup );
			}

			ptr = ptr->_next;
		}

		return output;
	}
}

#endif