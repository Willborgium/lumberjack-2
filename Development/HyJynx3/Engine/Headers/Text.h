#ifndef HYJYNX_CORE_TEXT_H
#define HYJYNX_CORE_TEXT_H

#include "TypeDefinitions.h"

namespace HyJynxCore
{
	//
	// Dat API.
	//
	class Text sealed
	{
	protected:
		friend class MutableText;
		const char* _value;
		bool _ownsValue = false;
	public:

#pragma region Constructors

		//
		// Creates an instance of the Text class with a default value of nullptr.
		//
		Text( );

		//
		// Creates an instance of the Text class and sets the value to the given value.
		// - rhs: The value to create this Text object from.
		//
		Text( char rhs );

		//
		// Creates an instance of the Text class and sets the value to the given value.
		// - rhs: The value to create this Text object from.
		//
		//Text( char* rhs );

		//
		// Creates an instance of the Text class and sets the value to the given value.
		// - rhs: The value to create this Text object from.
		//
		Text( const char* rhs );

		//
		// Creates an instance of the Text class and sets the value to the given value.
		// - rhs: The value to create this Text object from.
		//
		Text( const Text& rhs );

		Text( Text&& rhs );

		//
		// Creates an instance of the Text class and sets the value to the given value.
		// - rhs: The value to create this Text object from.
		//
		Text( int rhs );

		//
		// Creates an instance of the Text class and sets the value to the given value.
		// - rhs: The value to create this Text object from.
		//
		Text( UInt rhs );

		//
		// Creates an instance of the Text class and sets the value to the given value.
		// - rhs: The value to create this Text object from.
		//
		Text( long rhs );

		//
		// Creates an instance of the Text class and sets the value to the given value.
		// - rhs: The value to create this Text object from.
		//
		Text( ULong rhs );

		//
		// Creates an instance of the Text class and sets the value to the given value.
		// - rhs: The value to create this Text object from.
		//
		Text( LongLong rhs );

		//
		// Creates an instance of the Text class and sets the value to the given value.
		// - rhs: The value to create this Text object from.
		//
		Text( ULongLong rhs );

		operator const char*( );

#pragma endregion

#pragma region Destructor

		~Text( );

#pragma endregion

#pragma region Methods

#pragma region Misc

		//
		// Returns a constant pointer to the native char array that represents the value.
		// - returns: The native character array this Text object represents.
		//
		const char* GetValue( ) const;

		//
		// Returns the length of the current text.
		// - returns: The length of the text.
		//
		UInt Length( ) const;

		//
		// Returns the hashed value of the given text.
		// - returns: The hashed value of the text.
		//
		UInt GetHashValue( ) const;

		//
		// Returns an array of Text objects that are seperated by the given token.
		// The tokenCount parameter is assigned the count of elements returned.
		// - token:				The character at which point the text will be split.
		// - tokenCount [OUT]:	The count of elements returned by the split operation.
		// - returns:			The parts of the text that remain after the split operation
		//						has completed.
		//
		Text* Split( char token, UInt* tokenCount ) const;

		//
		// Returns the character found at the given index in the current text.
		// - index:		The index of the desired lookup.
		// - returns:	The character found at the given index.
		//
		char CharacterAt( UInt index ) const;

		char operator[]( UInt index ) const;

#pragma endregion

#pragma region Contains

		//
		// Returns a value indicating whether the given character was found in the
		// text.
		// - value:		The character to look for.
		// - returns:	A flag indicating whether the given character was found.
		//
		bool Contains( char value ) const;

		//
		// Returns a value indicating whether the given character array was found in the
		// text.
		// - value:		The character array to look for.
		// - returns:	A flag indicating whether the given character array was found.
		//
		bool Contains( const char* value ) const;

		//
		// Returns a value indicating whether the given text was found in the
		// text.
		// - value:		The text to look for.
		// - returns:	A flag indicating whether the given text was found.
		//
		bool Contains( const Text& value ) const;

#pragma endregion

#pragma region StartsWith

		//
		// Returns a value indicating whether the text begins with the given character.
		// - value:		The character to look for.
		// - returns:	A flag indicating whether the text begins with the given character.
		//
		bool StartsWith( char value ) const;
		
		//
		// Returns whether or not the current text begins with the given character array.
		// - value:		The character to look for.
		// - returns:	A flag indicating whether the text begins with the given character array.
		//
		bool StartsWith( const char* value ) const;
		
		//
		// Returns whether or not the current text begins with the given text.
		// - value:		The character to look for.
		// - returns:	A flag indicating whether the text begins with the given text.
		//
		bool StartsWith( const Text& value ) const;

#pragma endregion

#pragma region EndsWith

		//
		// Returns a value indicating whether the text ends with the given character.
		// - value:		The character to look for.
		// - returns:	A flag indicating whether the text ends with the given character.
		//
		bool EndsWith( char value ) const;

		//
		// Returns a value indicating whether the text ends with the given character array.
		// - value:		The character to look for.
		// - returns:	A flag indicating whether the text ends with the given character array.
		//
		bool EndsWith( const char* value ) const;

		//
		// Returns a value indicating whether the text ends with the given text.
		// - value:		The character to look for.
		// - returns:	A flag indicating whether the text ends with the given text.
		//
		bool EndsWith( const Text& value ) const;

#pragma endregion

#pragma region CountOf

		//
		// Returns the number of occurances of the given character in the text.
		// - value:		The character to count.
		// - returns:	The count of the occurances of the given character.
		//
		UInt CountOf( char value ) const;

		//
		// Returns the number of occurances of the given characters in the text.
		// - value:		The characters to count.
		// - returns:	The count of the occurances of the given characters.
		//
		UInt CountOf( const char* value ) const;

		//
		// Returns the number of occurances of the given text in the text.
		// - value:		The text to count.
		// - returns:	The count of the occurances of the given text.
		//
		UInt CountOf( const Text& value ) const;

#pragma endregion

#pragma region Replace

		//
		// Replaces all occurances of 'oldValue' with 'newValue'.
		// - oldValue:	The value to be replaced.
		// - newValue:	The value to replace 'oldValue' with.
		// - returns:	The new character array with the replacements made.
		//
		char* Replace( char oldValue, char newValue ) const;

		//
		// Replaces all occurances of 'oldValue' with 'newValue'.
		// - oldValue:	The value to be replaced.
		// - newValue:	The value to replace 'oldValue' with.
		// - returns:	The new character array with the replacements made.
		//
		char* Replace( const char* oldValue, const char* newValue ) const;

		//
		// Replaces all occurances of 'oldValue' with 'newValue'.
		// - oldValue:	The value to be replaced.
		// - newValue:	The value to replace 'oldValue' with.
		// - returns:	The new character array with the replacements made.
		//
		char* Replace( const Text& oldValue, const Text& newValue ) const;

#pragma endregion

#pragma region Trim

		//
		// Removes all leading occurrances of the specified character.
		// - trimChar:	The character to trim.
		// - returns:	The new character array with the leading characters trimmed.
		//
		char* TrimStart( char trimChar ) const;

		//
		// Removes all trailing occurrances of the specified character.
		// - trimChar:	The character to trim.
		// - returns:	The new character array with the trailing characters trimmed.
		//
		char* TrimEnd( char trimChar ) const;

		//
		// Removes all leading and trailing whitespace.
		// - returns: The new character array without leading and trailing whitespace.
		//
		char* TrimWhitespace( ) const;

		//
		// Removes all leading and trailing occurrances of the specified character.
		// - returns: The new character array without leading and trailing whitespace.
		//
		char* Trim( char trimChar ) const;

#pragma endregion

#pragma region Remove

		//
		// Removes the given number of characters from the beginning of the text.
		// - count:		The number of characters to remove.
		// - returns:	The new character array without the leading characters.
		//
		char* RemoveFromStart( UInt count ) const;

		// Removes the given number of characters from the end of the text.
		// - count:		The number of characters to remove.
		// - returns:	The new character array without the trailing characters.
		//
		char* RemoveFromEnd( UInt count ) const;

#pragma endregion

#pragma region IndexOf

		//
		// Returns the index of the first occurance of the given character.
		// - returns:	The index of the first occurance of the given character. If it is not
		//				found, returns UInt.MaxValue.
		//
		UInt IndexOf( char value ) const;
		
		//
		// Returns the index of the first occurance of the given character array.
		// - returns:	The index of the first occurance of the given character array. If it is not
		//				found, returns UInt.MaxValue.
		//
		UInt IndexOf( const char* value ) const;
		
		//
		// Returns the index of the first occurance of the given text.
		// - returns:	The index of the first occurance of the given text. If it is not
		//				found, returns UInt.MaxValue.
		//
		UInt IndexOf( const Text& value ) const;
		
		//
		// Returns the index of the first occurance of any character in
		// the given character array.
		// - returns:	The index of the first occurance of any character. If none are found,
		//				returns UInt.MaxValue.
		//
		UInt IndexOfAny( const char* values ) const;

		//
		// Returns the index of the first occurance of any character in
		// the given text.
		// - returns:	The index of the first occurance of any character. If none are found,
		//				returns UInt.MaxValue.
		//
		UInt IndexOfAny( const Text& values ) const;

#pragma endregion

#pragma region IsNull / OrEmpty / OrWhitespace

		//
		// Returns whether the current text is null.
		// - returns: A flag indicating whether the current value is nullptr.
		//
		bool IsNull( ) const;

		//
		// Returns whether the current text is null or has no characters.
		// - returns:	A flag indicating whether the current value is nullptr
		//				or contains no characters.
		//
		bool IsNullOrEmpty( ) const;

		//
		// Returns whether the current text is null or has no characters other than whitespace.
		// - returns:	A flag indicating whether the current value is nullptr
		//				or contains no characters other than whitespace.
		//
		bool IsNullOrWhitespace( ) const;

#pragma endregion

#pragma region Pad Left

		//
		// Returns a copy of the current text leading with a specified
		// number of specified characters before the text.
		// - count: The number of characters to add to the beginning of the text.
		// - value: The character to pad.
		// - returns: A copy of the current text with the specified padding.
		//
		char* PadLeft( UInt count, char value ) const;
		
		//
		// Returns a copy of the current text leading with a specified
		// number of specified character arrays before the text.
		// - count: The number of character arrays to add to the beginning of the text.
		// - value: The characters to pad.
		// - returns: A copy of the current text with the specified padding.
		//
		char* PadLeft( UInt count, const char* value ) const;

		//
		// Returns a copy of the current text leading with a specified
		// number of specified text before the text.
		// - count: The number of texts to add to the beginning of the text.
		// - value: The text to pad.
		// - returns: A copy of the current text with the specified padding.
		//
		char* PadLeft( UInt count, const Text& value ) const;
		
		//
		// Returns a copy of the current text leading with a specified
		// number of space characters before the text.
		// - count: The number of characters to add to the beginning of the text.
		// - returns: A copy of the current text with the specified padding.
		//
		char* PadLeft( UInt count ) const;

#pragma endregion

#pragma region Pad Right
		
		//
		// Returns a copy of the current text ending with a specified
		// number of specified characters before the text.
		// - count: The number of characters to add to the end of the text.
		// - value: The character to pad.
		// - returns: A copy of the current text with the specified padding.
		//
		char* PadRight( UInt count, char value ) const;
		
		//
		// Returns a copy of the current text ending with a specified
		// number of specified character arrays before the text.
		// - count: The number of characters to add to the end of the text.
		// - value: The characters to pad.
		// - returns: A copy of the current text with the specified padding.
		//
		char* PadRight( UInt count, const char* value ) const;
		
		//
		// Returns a copy of the current text ending with a specified
		// number of specified text before the text.
		// - count: The number of texts to add to the beginning of the text.
		// - value: The character to pad.
		// - returns: A copy of the current text with the specified padding.
		//
		char* PadRight( UInt count, const Text& value ) const;
		
		//
		// Returns a copy of the current text ending with a specified
		// number of space characters before the text.
		// - count: The number of characters to add to the end of the text.
		// - returns: A copy of the current text with the specified padding.
		//
		char* PadRight( UInt count ) const;

#pragma endregion

#pragma region Subtext

		//
		// Returns the text found between the start and end positions specified.
		// - startPosition:		The position to start reading from.
		// - endPosition:		The position to end at.
		// - returns:			The text found between the start and end positions.
		//
		char* GetSubText( UInt startPosition, UInt endPosition ) const;

		//
		// Returns text starting at the specified position through the end of the text.
		// - startPosition:		The position to start reading from.
		// - returns:			The text found from the start through the end of the text.
		//
		char* GetSubText( UInt startPosition ) const;

#pragma endregion

#pragma endregion

#pragma region Operators

#pragma region Plus Operator

		char* operator+( const Text& rhs ) const;

		char* operator+( const char* rhs ) const;
		
		char* operator+( char rhs ) const;

#pragma endregion

#pragma region PlusEquals Operator

		Text& operator+=( const Text& rhs );

		Text& operator+=( const char* rhs );

		Text& operator+=( char rhs );

#pragma endregion

#pragma region Assignment Operator

		Text& operator=( const Text& rhs );

		Text& operator=( const char* rhs );
		
		Text& operator=( char rhs );

#pragma endregion

#pragma region Comparison Operators

#pragma region Equals

		bool operator==( const Text& rhs ) const;

		bool operator==( const char* rhs ) const;

		bool operator==( char rhs ) const;

#pragma endregion

#pragma region Not Equals

		bool operator!=( const Text& rhs ) const;

		bool operator!=( const char* rhs ) const;

		bool operator!=( char rhs ) const;

#pragma endregion

#pragma region Greater Than

		bool operator>( const Text& rhs ) const;
		
		bool operator>( const char* rhs ) const;
		
		bool operator>( char rhs ) const;

#pragma endregion

#pragma region Less Than

		bool operator<( const Text& rhs ) const;
		
		bool operator<( const char* rhs ) const;
		
		bool operator<( char rhs ) const;

#pragma endregion

#pragma region Greater Than or Equal To

		bool operator>=( const Text& rhs ) const;
		
		bool operator>=( const char* rhs ) const;
		
		bool operator>=( char rhs ) const;

#pragma endregion

#pragma region Less Than or Equal To

		bool operator<=( const Text& rhs ) const;
		
		bool operator<=( const char* rhs ) const;
		
		bool operator<=( char rhs ) const;

#pragma endregion

#pragma endregion

#pragma endregion

#pragma region Static Functions

		//
		// Returns an empty character array.
		// - returns: An empty character array.
		//
		static const char* Empty();

#pragma region Length

		//
		// Returns the length of the given character array, not including
		// the null terminator.
		// - value:		The character array to measure.
		// - returns:	The length of the character array.
		//
		static UInt LengthOf( const char* value );

		//
		// Returns the length of the given text, not including
		// the null terminator.
		// - value:		The text to measure.
		// - returns:	The length of the text.
		//
		static UInt LengthOf( const Text& value );

#pragma endregion

#pragma region Conversion

#pragma region ToInt

		//
		// Returns the integer representation of the current text.
		// - returns:	The integer this text represents. Returns 0 if the
		//				text is not convertible.
		//
		int ToInt( ) const;

		//
		// Returns the integer representation of the character array.
		// - returns:	The integer the character array represents. Returns 0 if the
		//				text is not convertible.
		//
		static int ToInt( const char* value );

		//
		// Returns the integer representation of the text.
		// - returns:	The integer the text represents. Returns 0 if the
		//				text is not convertible.
		//
		static int ToInt( const Text& value );

#pragma endregion

#pragma region ToDouble

		//
		// Returns the double representation of the current text.
		// - returns:	The double this text represents. Returns 0 if the
		//				text is not convertible.
		//
		double ToDouble( ) const;

		//
		// Returns the double representation of the character array.
		// - returns:	The double the character array represents. Returns 0 if the
		//				text is not convertible.
		//
		static double ToDouble( const char* value );

		//
		// Returns the double representation of the text.
		// - returns:	The double the text represents. Returns 0 if the
		//				text is not convertible.
		//
		static double ToDouble( const Text& value );

#pragma endregion

#pragma region ToLong

		//
		// Returns the long representation of the current text.
		// - returns:	The long this text represents. Returns 0 if the
		//				text is not convertible.
		//
		long ToLong( ) const;

		//
		// Returns the long representation of the character array.
		// - returns:	The long the character array represents. Returns 0 if the
		//				text is not convertible.
		//
		static long ToLong( const char* value );

		//
		// Returns the long representation of the text.
		// - returns:	The long the text represents. Returns 0 if the
		//				text is not convertible.
		//
		static long ToLong( const Text& value );

#pragma endregion

#pragma region ToLongLong

		//
		// Returns the long long representation of the current text.
		// - returns:	The long long this text represents. Returns 0 if the
		//				text is not convertible.
		//
		LongLong ToLongLong( ) const;

		//
		// Returns the long long representation of the character array.
		// - returns:	The long long the character array represents. Returns 0 if the
		//				text is not convertible.
		//
		static LongLong ToLongLong( const char* value );

		//
		// Returns the long long representation of the text.
		// - returns:	The long long the text represents. Returns 0 if the
		//				text is not convertible.
		//
		static LongLong ToLongLong( const Text& value );

#pragma endregion

#pragma region ToBoolean

		//
		// Returns the boolean representation of the current text.
		// - returns:	The boolean this text represents. Returns false if the
		//				text is not convertible.
		//
		bool ToBoolean( ) const;

		//
		// Returns the boolean representation of the current text.
		// - returns:	The boolean this text represents. Returns false if the
		//				text is not convertible.
		//
		static bool ToBoolean( const char* value );

		//
		// Returns the boolean representation of the current text.
		// - returns:	The boolean this text represents. Returns false if the
		//				text is not convertible.
		//
		static bool ToBoolean( const Text& value );

#pragma endregion

#pragma region FromInt

		//
		// Returns a character array representation of the given
		// integer with 12 character precision.
		// - rhs:		The integer to convert.
		// - returns:	The character array representation of the integer.
		//
		static char* FromInt( int rhs );

		//
		// Returns a character array representation of the given
		// unsigned integer with 11 character precision.
		// - rhs:		The unsigned integer to convert.
		// - returns:	The character array representation of the unsigned integer.
		//
		static char* FromUInt( UInt rhs );

#pragma endregion

#pragma region FromLong

		//
		// Returns a character array representation of the given
		// long integer with 12 character precision.
		// - rhs:		The long integer to convert.
		// - returns:	The character array representation of the long integer.
		//
		static char* FromLong( long rhs );
		
		//
		// Returns a character array representation of the given
		// ULong integer with 11 character precision.
		// - rhs:		The unsigned long integer to convert.
		// - returns:	The character array representation of the unsigned long integer.
		//
		static char* FromULong( ULong rhs );

#pragma endregion

#pragma region FromLongLong
		
		//
		// Returns a character array representation of the given
		// LongLong integer with 21 character precision.
		// - rhs:		The long long integer to convert.
		// - returns:	The character array representation of the long long integer.
		//
		static char* FromLongLong( LongLong rhs );
		
		//
		// Returns a character array representation of the given
		// ULongLong integer with 21 character precision.
		// - rhs:		The unsigned long long integer to convert.
		// - returns:	The character array representation of the unsigned long long integer.
		//
		static char* FromULongLong( ULongLong rhs );

#pragma endregion

#pragma endregion

#pragma region Combine

		//
		// Concatenates the two character arrays.
		// - a:			The first character array.
		// - b:			The second character array.
		// - returns:	A character array that is the concatenation of the two given
		//				character arrays.
		//
		static char* Combine( const char* a, const char* b );

		//
		// Concatenates the two texts.
		// - a:			The first text.
		// - b:			The second text.
		// - returns:	A character array that is the concatenation of the two given
		//				texts.
		//
		static char* Combine( const Text& lhs, const Text& rhs );

#pragma endregion

#pragma region Get Copy

		//
		// Returns a copy of the current text.
		// - returns: A copy of the current text.
		//
		char* GetCopy( ) const;
		
		//
		// Returns a copy of the given character array.
		// - returns: A copy of the given character array.
		//
		//static char* GetCopy( char* rhs );
		
		//
		// Returns a copy of the given character array.
		// - returns: A copy of the given character array.
		//
		static char* GetCopy( const char* rhs );

		//
		// Returns a copy of the given text.
		// - returns: A copy of the given text.
		//
		static char* GetCopy( const Text& rhs );

		//
		// Returns a copy of the given character.
		// - returns: A copy of the given character.
		//
		static char* GetCopy( char rhs );

#pragma endregion

#pragma region Compare

#pragma region AreEqual

		//
		// Compares the two character arrays for equality.
		// - a:			The first character array to compare.
		// - b:			The second character array to compare.
		// - returns:	A a flag indicating whether the two character arrays are the same.
		//
		//static bool AreEqual( char* a, char* b );

		//
		// Compares the two character arrays for equality.
		// - a:			The first character array to compare.
		// - b:			The second character array to compare.
		// - returns:	A a flag indicating whether the two character arrays are the same.
		//
		static bool AreEqual( const char* a, const char* b );

		//
		// Compares the two texts for equality.
		// - a:			The first texts to compare.
		// - b:			The second texts to compare.
		// - returns:	A a flag indicating whether the two texts are the same.
		//
		static bool AreEqual( const Text& a, const Text& b );

#pragma endregion

#pragma region Compare

		//
		// Compares the current text to the given text. 
		// - rhs:		The text to compare to.
		// - returns:	A number specifying equality. All non-zero values indicate inequality.
		//
		int CompareTo( const Text& rhs ) const;

		//
		// Compares two texts for equality.
		// - rhs:		The first text to compare.
		// - lhs:		The second text to compare.
		// - returns:	A number specifying equality. All non-zero values indicate inequality.
		//
		static int Compare( const Text& lhs, const Text& rhs );

		//
		// Compares the current text to the given character array. 
		// - rhs:		The text to compare to.
		// - returns:	A number specifying equality. All non-zero values indicate inequality.
		//
		int CompareTo( const char* rhs ) const;

		//
		// Compares two character arrays for equality.
		// - rhs:		The first character array to compare.
		// - lhs:		The second character array to compare.
		// - returns:	A number specifying equality. All non-zero values indicate inequality.
		//
		static int Compare( const char* lhs, const char* rhs );

#pragma endregion

#pragma endregion

#pragma region ToLower and ToUpper

		//
		// Returns a copy of the current text with all characters in lower-casing.
		// - returns: A copy of the current text with all character in lower-casing.
		//
		char* ToLower( ) const;

		//
		// Returns a copy of the given character array with all characters in lower-casing.
		// - returns: A copy of the given character array with all character in lower-casing.
		//
		static char* ToLower( const char* rhs );

		//
		// Returns a copy of the given text with all characters in lower-casing.
		// - returns: A copy of the given text with all character in lower-casing.
		//
		static char* ToLower( const Text& rhs );

		//
		// Returns a copy of the current text with all characters in upper-casing.
		// - returns: A copy of the current text with all character in upper-casing.
		//
		char* ToUpper( ) const;

		//
		// Returns a copy of the given character array with all characters in upper-casing.
		// - returns: A copy of the given character array with all character in upper-casing.
		//
		static char* ToUpper( const char* rhs );

		//
		// Returns a copy of the given text with all characters in upper-casing.
		// - returns: A copy of the given text with all character in upper-casing.
		//
		static char* ToUpper( const Text& rhs );

#pragma endregion

#pragma region Split

		//
		// Splits the given character array based on the given token.
		// - value:			The value to split.
		// - token:			The character at which to split.
		// - tokenCount:	The number of parts that are returned in the text array.
		// - returns:		A text array of the split parts.
		//
		static Text* Split( const char* value, char token, UInt* tokenCount );

		//
		// Splits the given text based on the given token.
		// - value:			The value to split.
		// - token:			The character at which to split.
		// - tokenCount:	The number of parts that are returned in the text array.
		// - returns:		A text array of the split parts.
		//
		static Text* Split( const Text& value, char token, UInt* tokenCount );

#pragma endregion

#pragma endregion

	};

	template <class Type>
	//
	// Provides a basic hash configuration for use in STL containers.
	//
	struct HashConfiguration
	{
		enum
		{
			bucket_size = 4,
			min_buckets = 8
		};

		size_t operator()( const Type& key_value ) const;

		bool operator()( const Type& lhs, const Type& rhs ) const;
	};
}

#endif