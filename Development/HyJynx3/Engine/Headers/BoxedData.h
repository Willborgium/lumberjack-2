#ifndef HYJYNX_UTILITIES_BOXED_DATA_H
#define HYJYNX_UTILITIES_BOXED_DATA_H

#include "Data.h"
#include "Text.h"

namespace HyJynxUtilities
{
	template <typename ValueType>
	//
	// Provides a class to allow for naming of primitive types.
	//
	struct BoxedData : public HyJynxCore::Data
	{
	public:
		//
		// Initializes a new instance of the BoxedData class.
		// - name: The name of the data.
		//
		BoxedData( HyJynxCore::Text name )
			: HyJynxCore::Data( name )
		{
		}

		//
		// Initializes a new instance of the BoxedData class.
		// - name:	The name of the data.
		// - value:	The value to be associated given name.
		//
		BoxedData( HyJynxCore::Text name, ValueType value )
			: HyJynxCore::Data( name ), Value( value )
		{
		}

		//
		// The boxed value.
		//
		ValueType Value;
	};
}

#endif