#ifndef HYJYNX_DESIGN_FUNCTABLE_H
#define HYJYNX_DESIGN_FUNCTABLE_H

#include <functional>

namespace HyJynxDesign
{
	//
	// Provides a simple direct linked list for registering and retrieving functions.
	//
	class FuncTable
	{
	private:
		//
		// Provides a structure for associating function addresses with their respective
		// function objects.
		//
		struct FuncPair
		{
			//
			// The address of the function.
			//
			void* AddrKey = nullptr;

			//
			// The address of the function object.
			//
			void* Function = nullptr;

			//
			// The address of the next FuncPair in the list.
			//
			FuncPair* Next = nullptr;

			//
			// Initializes a new FuncPair object by associating the given address and function object.
			//
			FuncPair( void* addr, void* func )
				: AddrKey( addr ), Function( func )
			{
			}
		};

		//
		// The root of the function pair list.
		//
		FuncPair* _ptrs = nullptr;

		//
		// The static singleton instance.
		//
		static FuncTable* _instance;

		//
		// Initializes a default instance of the FuncTable class.
		//
		FuncTable( );

		FuncTable( const FuncTable& ) = delete;
	public:
		//
		// Retrieves a singleton instance of the FuncTable class.
		//
		static FuncTable* Instance( );

		template <typename T>
		//
		// Checks if the given address is registered in the function table.
		// - addr:		The address of the function to look for.
		// - returns:	If the address is registered, the function object is returned. Otherwise
		//				returns nullptr.
		//
		std::function<T>* Contains( void* addr )
		{
			std::function<T>* output = nullptr;

			FuncPair* ptr = _ptrs;

			while ( ptr != nullptr )
			{
				if ( ptr->AddrKey == addr )
				{
					output = static_cast< std::function<T>* >( ptr->Function );
					break;
				}
				else
				{
					ptr = ptr->Next;
				}
			}

			return output;
		}

		//
		// Registers the given address and associates it with the function object.
		// - addr:	The address of the function to register.
		// - func:	The function object to associate with the address.
		//
		void Register( void* addr, void* func );
	};
}

#endif