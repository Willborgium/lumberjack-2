#ifndef HYJYNX_DESIGN_CALLBACK_BASE_H
#define HYJYNX_DESIGN_CALLBACK_BASE_H

#include "CaptureData.h"

namespace HyJynxDesign
{
	template <typename ReturnType, typename... Args>
	//
	// Provides a generic base for callbacks.
	//
	class CallbackBase
	{
	protected:
		CaptureData* _capture = nullptr;
	public:
		//
		// Initializes a new instance of the CallbackBase class with the provided CaptureData.
		// - capture:	The data to be capture in the current scope.
		//
		CallbackBase( CaptureData* capture )
			: _capture( capture )
		{
		}

		template <typename Type>
		//
		// Returns the value associated with the given name from the captured data.
		// - name:		The name of the data.
		// - returns:	The value associated with the given name.
		//
		Type* Get( char* name )
		{
			return _capture != nullptr ? _capture->Get<Type>( name ) : nullptr;
		}

		ReturnType Call( Args... args )
		{
			return operator()( args... );
		}

		virtual ReturnType operator()( Args... args ) = 0;
	};
}

#endif