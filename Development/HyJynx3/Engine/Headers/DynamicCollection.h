#ifndef HYJYNX_COLLECTIONS_DYNAMIC_COLLECTION_H
#define HYJYNX_COLLECTIONS_DYNAMIC_COLLECTION_H

#include "TypeDefinitions.h"
#include "ICollection.h"
#include "MemoryManagement.h"
#include "Mutex.h"

namespace HyJynxCollections
{
	template <typename KeyType, typename ValueType>
	class Dictionary;

	template <typename ValueType>
	//
	// Provides a generic API to manipulate a double-indexed, dynamically scalable, lightweight collection.
	//
	class DynamicCollection : public ICollection<ValueType>
	{
	protected:
		bool _mutexHandle;
		//
		// Represents a single element in a DynamicCollection that knows the previous and
		// next element in the sequence.
		//
		struct Node : CollectionElement
		{
			//
			// Initializes an instance of the DynamicCollection::Node class.
			// - p: The node previous to this.
			// - n: The node following this.
			// - v: The value held within this node.
			//
			Node( Node* p, Node* n, ValueType v )
				: _previous( p ),
				_next( n )
			{
				_value = v;
			}

			//
			// The node that comes before this.
			//
			Node* _previous;

			//
			// The node following this.
			//
			Node* _next;
		};

		Node* _root;
		Node* _end;
		UInt _count;

		Node* GetNode( std::function<bool( Node* )> predicate ) const
		{
			Node* end = _end;
			Node* start = _root;

			for ( UInt index = 0; index < ( _count + 1 ) / 2; index++ )
			{
				if ( end != nullptr )
				{
					if ( predicate( end ) )
					{
						return end;
					}
					else
					{
						end = end->_previous;
					}
				}

				if ( start != nullptr )
				{
					if ( predicate( start ) )
					{
						return start;
					}
					else
					{
						start = start->_next;
					}
				}
			}

			return nullptr;
		}

		Node* GetNodeByIndex( UInt index ) const
		{
			if ( index >= _count - 1 )
			{
				return _end;
			}

			if ( index == 0 )
			{
				return _root;
			}

			Node* ptr = nullptr;

			// Search from end
			if ( index > ( ( _count ) / 2 ) )
			{
				int ndx = _count - 1;
				ptr = _end;

				while ( ndx != index )
				{
					if ( ptr != nullptr )
					{
						ptr = ptr->_previous;
					}
					ndx--;
				}
			}
			else
			{
				int ndx = 0;
				ptr = _root;

				while ( ndx != index )
				{
					if ( ptr != nullptr )
					{
						ptr = ptr->_next;
					}
					ndx++;
				}
			}

			return ptr;
		}

		Node* GetNodeByValue( ValueType value ) const
		{
			Node* end = _end;
			Node* start = _root;

			for ( UInt index = 0; index < ( _count + 1 ) / 2; index++ )
			{
				if ( end != nullptr )
				{
					if ( end->_value == value )
					{
						return end;
					}
					else
					{
						end = end->_previous;
					}
				}

				if ( start != nullptr )
				{
					if ( start->_value == value )
					{
						return start;
					}
					else
					{
						start = start->_next;
					}
				}
			}

			return nullptr;
		}

		void Sort( std::function<int( ValueType, ValueType )>& expression )
		{
			_root = MergeSort( _root, expression );
		}

		Node* MergeSort( Node* n, std::function<int( ValueType, ValueType )>& expression )
		{
			Node* output = nullptr;

			if ( n == nullptr )
			{
				output = nullptr;
			}
			else if ( n->_next == nullptr )
			{
				output = n;
			}
			else
			{
				output = MSSplit( n );
				output = Merge( MergeSort( n, expression ), MergeSort( output, expression ), expression );
			}

			UpdatePreviousConnections( output );

			return output;
		}

		void UpdatePreviousConnections( Node* node )
		{
			Node* ptr = node;
			Node* prev = nullptr;

			while ( ptr != nullptr )
			{
				ptr->_previous = prev;
				prev = ptr;
				ptr = ptr->_next;
			}
		}

		Node* Merge( Node* left, Node* right, std::function<int( ValueType, ValueType )>& expression )
		{
			Node* output = nullptr;

			if ( left == nullptr )
			{
				output = right;
			}
			else if ( right == nullptr )
			{
				output = left;
			}
			else if ( expression( left->_value, right->_value ) <= 0 )
			{
				left->_next = Merge( left->_next, right, expression );
				output = left;
			}
			else
			{
				right->_next = Merge( left, right->_next, expression );
				output = right;
			}

			return output;
		}

		Node* MSSplit( Node* n )
		{
			Node* output = nullptr;

			if ( n != nullptr && n->_next != nullptr )
			{
				output = n->_next;
				n->_next = output->_next;
				output->_next = MSSplit( output->_next );
			}

			return output;
		}

		void RemoveNode( Node* ptr )
		{
			if ( ptr != nullptr )
			{
				Node* p = ptr->_previous;
				Node* n = ptr->_next;

				if ( p != nullptr )
				{
					p->_next = n;
				}
				else
				{
					_root = n;
				}

				if ( n != nullptr )
				{
					n->_previous = p;
				}
				else
				{
					_end = p;
				}

				delete ptr;
				_count--;
			}
		}

	public:
		//
		// Initializes a new instance of the DynamicCollection class.
		//
		DynamicCollection( )
			: _root( nullptr ), _end( nullptr ), _count( 0 ), _mutexHandle( false )
		{
		}

		////
		//// Initializes the collection based on another collection.
		//// - collection:	The collection to copy.
		////
		//DynamicCollection ( const DynamicCollection<ValueType>&& collection )
		//{
		//	Append ( collection );
		//}

		//
		// Initializes the collection based on another collection.
		// - collection:	The collection to copy.
		//
		DynamicCollection( const ICollection<ValueType>* collection )
			: _root( nullptr ), _end( nullptr ), _count( 0 ), _mutexHandle( false )
		{
			Append( collection );
		}

		//
		// Initializes the collection based on another collection.
		// - collection:	The collection to copy.
		//
		DynamicCollection( const ICollection<ValueType>&& collection )
			: _root( nullptr ), _end( nullptr ), _count( 0 ), _mutexHandle( false )
		{
			Append( collection );
		}

		//
		// Returns the number of elements in the collection.
		// - returns: The number of elements in the collection.
		//
		virtual UInt Count( ) const
		{
			return _count;
		}

		//
		// Returns the number of elements in the collection that match
		// a given condition.
		// - condition:	The condition to test for.
		// - returns:	The number of elements in the collection.
		//
		virtual UInt Count( std::function<bool( ValueType )> condition ) const
		{
			HyJynxCore::Mutex lock( &_mutexHandle );
			lock.Lock( );

			UInt count = 0;

			Node* ptr = _root;

			while ( ptr != nullptr )
			{
				if ( condition( ptr->_value ) )
				{
					count++;
				}

				ptr = ptr->_next;
			}

			return count;
		}
		
		//
		// Determines whether all elements satisfy a given condition.
		// - condition:	The condition to test for.
		// - returns:	A flag indicating whether all items satisfy the given condition.
		//
		virtual bool All( std::function<bool( ValueType )> condition ) const
		{
			HyJynxCore::Mutex lock( &_mutexHandle );
			lock.Lock( );

			bool result = true;

			Node* ptr = _root;

			while ( ptr != nullptr )
			{
				result = condition( ptr->_value );

				if ( !result )
				{
					break;
				}
				else
				{
					ptr = ptr->_next;
				}
			}

			return result;
		}

		//
		// Determines whether the collection contains any items.
		// - returns:	A flag indicating whether the collection contains any items.
		//
		virtual bool Any( ) const
		{
			return _root != nullptr;
		}

		//
		// Determines whether the collection contains any items that
		// satisfy the given condition.
		// - condition: The condition to test for.
		// - returns:	A flag indicating whether the collection contains any items
		//				that satisfies the given condition.
		//
		virtual bool Any( std::function<bool( ValueType )> condition ) const
		{
			HyJynxCore::Mutex lock( &_mutexHandle );
			lock.Lock( );

			bool output = false;

			Node* r = _root;
			Node* e = _end;

			while ( r != nullptr || e != nullptr )
			{
				if ( r != nullptr )
				{
					output = condition( r->_value );
					if ( output )
					{
						break;
					}
				}

				if ( e != nullptr )
				{
					output = condition( e->_value );
					if ( output )
					{
						break;
					}
				}

				if ( r == e || ( r != nullptr && r->_next == e ) )
				{
					break;
				}
				else
				{
					r = r->_next;
					e = e->_previous;
				}
			}

			return output;
		}

		//
		// Sets the value at the given index.
		// - index:	The index at which the given value should be set.
		// - value:	The value to set.
		//
		virtual void SetValue( UInt index, ValueType value )
		{
			HyJynxCore::Mutex lock( &_mutexHandle );
			lock.Lock( );

			Node* ptr = GetNodeByIndex( index );
			if ( ptr != nullptr )
			{
				ptr->_value = value;
			}
		}

		//
		// Returns the first item in the collection.
		// - returns: The first item in the collection.
		//
		virtual ValueType First( ) const
		{
			HyJynxCore::Mutex lock( &_mutexHandle );
			lock.Lock( );

			if ( _root != nullptr )
			{
				return _root->_value;
			}

			return ValueType( );
		}

		//
		// Returns the first item in the collection that satisfies the given predicate.
		// - predicate: The condition to test for.
		// - returns:	The first item in the collection that satisfies the given predicate.
		//
		virtual ValueType First( std::function<bool( ValueType )> predicate ) const
		{
			HyJynxCore::Mutex lock( &_mutexHandle );
			lock.Lock( );

			ValueType result;
			memset( &result, 0, sizeof( ValueType ) );

			Node* n = _root;

			while ( n != nullptr )
			{
				if ( predicate( n->_value ) )
				{
					result = n->_value;
					break;
				}
				else
				{
					n = n->_next;
				}
			}

			return result;
		}

		//
		// Returns the last item in the collection.
		// - returns: The last item in the collection.
		//
		virtual ValueType Last( ) const
		{
			HyJynxCore::Mutex lock( &_mutexHandle );
			lock.Lock( );

			if ( _end != nullptr )
			{
				return _end->_value;
			}

			return ValueType( );
		}

		//
		// Returns the last item in the collection that satisfies the given predicate.
		// - predicate: The condition to test for.
		// - returns:	The last item in the collection that satisfies the given predicate.
		//
		virtual ValueType Last( std::function<bool( ValueType )> predicate ) const
		{
			HyJynxCore::Mutex lock( &_mutexHandle );
			lock.Lock( );

			ValueType result;
			memset( &result, 0, sizeof( ValueType ) );

			Node* n = _end;

			while ( n != nullptr )
			{
				if ( predicate( n->_value ) )
				{
					result = n->_value;
					break;
				}
				else
				{
					n = n->_previous;
				}
			}

			return result;
		}

		//
		// Adds the given value into the collection as the last element.
		// - value:	The value to add to the collection.
		//
		virtual void Append( ValueType value )
		{
			HyJynxCore::Mutex lock( &_mutexHandle );
			lock.Lock( );

			if ( _count > 0 )
			{
				_end = _end->_next = anew( Node )( _end, nullptr, value );
			}
			else
			{
				_end = _root = anew( Node )( nullptr, nullptr, value );
			}
			_count++;
		}

		//
		// Adds the items in the given collection to the end of the collection.
		// - values:	The values to be added to the collection.
		//
		virtual void Append( const ICollection<ValueType>* values )
		{
			HyJynxCore::Mutex lock( &_mutexHandle );
			lock.Lock( );

			if ( values != nullptr && values->Count( ) > 0 )
			{
				for ( UInt index = 0; index < values->Count( ); index++ )
				{
					Append( values->ElementAt( index ) );
				}
			}
		}

		//
		// Adds the items in the given collection to the end of the collection.
		// - values:	The values to be added to the collection.
		//
		virtual void Append( const ICollection<ValueType>&& values )
		{
			HyJynxCore::Mutex lock( &_mutexHandle );
			lock.Lock( );

			if ( values.Count( ) > 0 )
			{
				for ( UInt index = 0; index < values.Count( ); index++ )
				{
					Append( values.ElementAt( index ) );
				}
			}
		}

		//
		// Inserts the given value at the given index.
		// - index:	The index at which to insert the value.
		// - value:	The value to insert.
		//
		virtual void InsertAt( UInt index, ValueType value )
		{
			HyJynxCore::Mutex lock( &_mutexHandle );
			lock.Lock( );

			Node* ptr = GetNodeByIndex( index );
			if ( ptr != nullptr )
			{
				ptr->_previous = ptr->_previous->_next = anew( Node )( ptr->_previous, ptr, value );
				_count++;
			}
		}

		//
		// Inserts the given collection of values at the given index.
		// - index:		The index at which to insert the values.
		// - values:	The values to insert.
		//
		virtual void InsertAt( UInt index, ICollection<ValueType>* values )
		{
			HyJynxCore::Mutex lock( &_mutexHandle );
			lock.Lock( );

			Node* end = GetNodeByIndex( index );

			if ( end != nullptr )
			{
				Node* start = end->_previous;

				if ( start == nullptr )
				{
					start = _root;
				}

				for ( UInt ndx = 0; ndx < values->Count( ); ndx++ )
				{
					start->_next = anew( Node )( start, nullptr, values->ElementAt( ndx ) );
					_count++;
					start = start->_next;
				}
				start->_next = end;
			}
		}

		//
		// Returns the index at which the requested value can be found.
		// - value:		The value to look for.
		// - returns:	The index of the first occurence of the given value. If none was found,
		//				returns -1.
		//
		virtual UInt IndexOf( ValueType value ) const
		{
			HyJynxCore::Mutex lock( &_mutexHandle );
			lock.Lock( );

			Node* end = _end;
			Node* start = _root;

			for ( UInt index = 0; index < ( _count + 1 ) / 2; index++ )
			{
				if ( end != nullptr )
				{
					if ( end->_value == value )
					{
						return _count - index - 1;
					}
					else
					{
						end = end->_previous;
					}
				}

				if ( start != nullptr )
				{
					if ( start->_value == value )
					{
						return index;
					}
					else
					{
						start = start->_next;
					}
				}
			}

			return -1;
		}

		//
		// Returns the value found at the given index.
		// - index:		The index to search to.
		// - returns:	The value located at the specified index. If none was found,
		//				returns a default instance of the ValueType.
		//
		virtual ValueType ElementAt( UInt index ) const
		{
			HyJynxCore::Mutex lock( &_mutexHandle );
			lock.Lock( );

			Node* ptr = GetNodeByIndex( index );
			if ( ptr != nullptr )
			{
				return ptr->_value;
			}

			return ValueType( );
		}

		//
		// Returns the value found at the given index.
		// - index:		The index to search to.
		// - returns:	The value located at the specified index. If none was found,
		//				returns a default instance of the ValueType.
		//
		virtual ValueType operator[]( UInt index ) const
		{
			return ElementAt( index );
		}

		//
		// Returns whether or not the given value is part of the collection.
		// - value:		The value to search for.
		// - returns:	A flag indicating whether or not the given value is contained in the collection.
		//
		virtual bool Contains( ValueType value ) const
		{
			return IndexOf( value ) != -1;
		}

		template <typename ReturnType>
		// 
		// Returns a subset of the current data.
		// - predicate: A function object [usually in the form of a lambda expression] that
		//				accepts one argument of 'ValueType' that will be used to determine
		//				what to return from this select statement.
		// - returns:	A collection of elements of type 'ReturnType' that satisfy the given predicate.
		//				If no elements were found, an empty collection is returned.
		//
		DynamicCollection<ReturnType>* Select( std::function<ReturnType( ValueType )> predicate ) const
		{
			HyJynxCore::Mutex lock( &_mutexHandle );
			lock.Lock( );

			DynamicCollection<ReturnType>* out = anew( DynamicCollection<ReturnType> )( );

			Node* sptr = _root;

			while ( sptr != nullptr )
			{
				out->Append( predicate( sptr->_value ) );
				sptr = sptr->_next;
			}

			return out;
		}

		//
		// Returns a collection of values from this collection that satisfy a given condition.
		// - condition:	A function object [usually in the form of a lambda expression] that
		//				accepts one argument of 'ValueType' that returns a boolean indicating
		//				whether or not this object should be returned in the resultant collection.
		// - returns:	A collection of values that satisfy the given condition. If no elements were found,
		//				and empty collection is returned.
		//
		virtual DynamicCollection<ValueType>* Where( std::function<bool( ValueType )> condition ) const
		{
			HyJynxCore::Mutex lock( &_mutexHandle );
			lock.Lock( );

			DynamicCollection<ValueType>* out = anew( DynamicCollection<ValueType> )( );

			Node* sptr = _root;
			Node* eptr = _end;
			Node* nsptr = nullptr;
			Node* neptr = nullptr;

			for ( UInt index = 0; index < ( _count + 1 ) / 2; index++ )
			{
				if ( sptr != nullptr )
				{
					if ( condition( sptr->_value ) )
					{
						out->Append( sptr->_value );
					}

					nsptr = sptr->_next;
				}

				if ( eptr != nullptr && eptr != sptr )
				{
					if ( condition( eptr->_value ) )
					{
						out->Append( eptr->_value );
					}

					neptr = eptr->_previous;
				}

				sptr = nsptr;
				eptr = neptr;
			}

			return out;
		}

		//
		// Selects a subset of the given collection.
		// - startIndex:	The first item to select.
		// - count:			The number of items to select after the first item.
		// - returns:		A collection of values that fall within the given range.
		//
		virtual ICollection<ValueType>* SelectRange( UInt startIndex, UInt count ) const
		{
			HyJynxCore::Mutex lock( &_mutexHandle );
			lock.Lock( );

			if ( startIndex < _count )
			{
				DynamicCollection<ValueType>* out = anew( DynamicCollection<ValueType> )( );

				Node* ptr = GetNodeByIndex( startIndex );

				while ( ptr != nullptr && count > 0 )
				{
					out->Append( ptr->_value );
					ptr = ptr->_next;
					count--;
				}

				return out;
			}

			return nullptr;
		}

		//
		// Returns a copy of the current collection.
		// - returns: A collection with the same elements from this collection.
		//
		virtual ICollection<ValueType>* GetCopy( ) const
		{
			return SelectRange( 0, _count );
		}

		//
		// Removes an element from the collection at the given index.
		// - index:	The index at which to remove an item.
		//
		virtual void RemoveAt( UInt index )
		{
			HyJynxCore::Mutex lock( &_mutexHandle );
			lock.Lock( );

			Node* ptr = GetNodeByIndex( index );

			RemoveNode( ptr );
		}

		//
		// Removes 'count' number of elements from the collection at the given index.
		// - index:	The index at which to start removing.
		// - count:	The number of elements to remove.
		//
		virtual void Remove( UInt startIndex, UInt count )
		{
			HyJynxCore::Mutex lock( &_mutexHandle );
			lock.Lock( );

			Node* sptr = GetNodeByIndex( startIndex );

			if ( sptr != nullptr )
			{
				Node* start = sptr->_previous;

				for ( UInt index = 0; index < count; index++ )
				{
					Node* next = sptr->_next;
					delete sptr;
					_count--;
					sptr = next;
					if ( sptr == nullptr )
					{
						break;
					}
				}

				if ( sptr == nullptr )
				{
					_end = start;
				}
				else
				{
					sptr->_previous = start;
				}

				if ( start == nullptr )
				{
					_root = sptr;
				}
				else
				{
					start->_next = sptr;
				}

			}
		}

		//
		// Removes the given value from the collection, if it is present.
		// - value:	The value to remove.
		//
		virtual void Remove( ValueType value )
		{
			RemoveAt( IndexOf( value ) );
		}

		//
		// Removes all items that satisfy the given condition.
		// - condition:	The condition the item must satisfy to be removed.
		//
		virtual void RemoveWhere( std::function<bool( ValueType )> condition )
		{
			HyJynxCore::Mutex lock( &_mutexHandle );
			lock.Lock( );

			Node* ptr = _root;

			while ( ptr != nullptr )
			{
				Node* next = ptr->_next;

				if ( condition( ptr->_value ) )
				{
					RemoveNode( ptr );
				}

				ptr = next;
			}
		}

		//
		// Removes all elements from the collection.
		//
		virtual void Clear( )
		{
			HyJynxCore::Mutex lock( &_mutexHandle );
			lock.Lock( );

			//Node* ptr = _root;
			//for ( UInt index = 0; index < _count; index++ )
			//{
			//	if ( ptr != nullptr )
			//	{
			//		Node* n = ptr->_next;
			//		delete ptr;
			//		ptr = n;
			//	}
			//}
			_root = _end = nullptr;
			_count = 0;
		}

		//
		// Reverses the order of the collection.
		//
		virtual void Reverse( )
		{
			HyJynxCore::Mutex lock( &_mutexHandle );
			lock.Lock( );

			Node* ptr = _root;
			for ( UInt index = 0; index < _count; index++ )
			{
				if ( ptr != nullptr )
				{
					Node* n = ptr->_next;
					ptr->_next = ptr->_previous;
					ptr->_previous = n;
					ptr = n;
				}
			}
			Node* e = _root;
			_root = _end;
			_end = e;
		}

		//
		// Iterates through the collection using the provided statement.
		// - statement:	A function object [usually in the form of a lambda expression] that accepts
		//				one argument of 'ValueType' that will be executed on each object in the collection.
		//
		virtual void ForEach( std::function<void( ValueType )> statement ) const
		{
			HyJynxCore::Mutex lock( &_mutexHandle );
			lock.Lock( );

			Node* ptr = _root;
			while ( ptr != nullptr )
			{
				statement( ptr->_value );
				ptr = ptr->_next;
			}
		}

		//
		// Sorts the collection based on a given expression.
		// - expression:	A function object [usually in the form of a lambda expression] that accepts
		//					two arguments of 'ValueType' that returns an integer indicating the relationship
		//					between two objects. -1 indicates the first argument is less than the second
		//					argument, 0 indicates equality, and 1 indicates the first argument is greater
		//					than the second argument.
		// - returns:		A copy of the current collection that is sorted based on the given collection.
		//
		virtual ICollection<ValueType>* Sort( std::function<int( ValueType, ValueType )> expression ) const
		{
			HyJynxCore::Mutex lock( &_mutexHandle );
			lock.Lock( );

			DynamicCollection<ValueType>* out = anew( DynamicCollection<ValueType> )( this );

			out->Sort( expression );

			return out;
		}
		
		template <typename Selector>
		Dictionary<Selector, ICollection<ValueType>*>* GroupBy( std::function<Selector( ValueType )> evaluator );
	};
}

#endif