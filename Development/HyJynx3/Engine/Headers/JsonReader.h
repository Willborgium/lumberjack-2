#ifndef HYJYNX_CORE_JSON_READER_H
#define HYJYNX_CORE_JSON_READER_H

#include "JsonParser.h"

namespace HyJynxCore
{
	class JsonReader
	{
	public:
		//
		// Evaluates the property on the object as Text.
		// - object:		The object on which to evaluate the property.
		// - propertyName:	The name of the property to evaluate.
		// - returns:		The value of the given property as Text.
		//
		static Text& PropertyValueText( JsonData* object, Text& propertyName );

		//
		// Evaluates the property on the object as an integer.
		// - object:		The object on which to evaluate the property.
		// - propertyName:	The name of the property to evaluate.
		// - returns:		The value of the given property as an integer.
		//
		static int PropertyValueInt( JsonData* object, Text& propertyName );

		//
		// Evaluates the property on the object as a double.
		// - object:		The object on which to evaluate the property.
		// - propertyName:	The name of the property to evaluate.
		// - returns:		The value of the given property as a double.
		//
		static double PropertyValueDouble( JsonData* object, Text& propertyName );

		//
		// Evaluates the property on the object as a JsonObject.
		// - object:		The object on which to evaluate the property.
		// - propertyName:	The name of the property to evaluate.
		// - returns:		The value of the given property as a JsonObject.
		//
		static JsonObject* PropertyValueData( JsonData* object, Text& propertyName );

		//
		// Evaluates the property on the object as a boolean.
		// - object:		The object on which to evaluate the property.
		// - propertyName:	The name of the property to evaluate.
		// - returns:		The value of the given property as a boolean.
		//
		static bool PropertyValueBoolean( _In_ JsonData*, _In_ Text& );
	};
}

#endif