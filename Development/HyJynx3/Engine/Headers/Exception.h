#ifndef HYJYNX_CORE_EXCEPTION_H
#define HYJYNX_CORE_EXCEPTION_H

#include "Time.h"
#include "Text.h"

namespace HyJynxCore
{
	//
	// Provides a base class for all exceptions that has a timestamp, a message, and an
	// exception level.
	//
	class Exception
	{
	protected:
		Time _timestamp;
		Text _message;
		USmall _level;
	public:
		//
		// Initializes a new exception with the given message and a level of Fatal.
		// - message:	The message associated with the exception.
		//
		Exception( Text message );

		//
		// Initializes a new exception with the given message and level.
		// - message:	The message associated with the exception.
		// - level:		The level of the exception. [Use the ExceptionLevel enumeration]
		//
		Exception( Text message, USmall level );

		//
		// Returns the message associated with this exception.
		// - returns: The message associated with this exception.
		//
		Text GetMessageText( ) const;

		//
		// Returns the timestamp associated with this exception.
		// - returns: The timestamp associated with this exception.
		//
		Time GetTimeStamp( ) const;

		//
		// Returns the level associated with this exception.
		// - returns: The exception level associated with this exception.
		//
		USmall GetLevel( ) const;
	};
}

#endif