#ifndef HYJYNX_DESIGN_CAPTURE_DATA_H
#define HYJYNX_DESIGN_CAPTURE_DATA_H

#include <memory>
#include "Allocator.h"

namespace HyJynxDesign
{
	//
	// Provides the ability to capture and store data temporarily for the purpose
	// of accessing it in a different scope.
	//
	class CaptureData
	{
	private:
		struct Pair
		{
			char* _name = nullptr;
			void* _value = nullptr;

			Pair( char* n, void* v )
				: _name( n ), _value( v )
			{
			}
		};

		Pair** _captured = nullptr;
		unsigned short _captureCount = 0;
	public:
		//
		// Initializes a new instance of the CaptureData class with the given
		// number of entries in the capture data table.
		// - captureCount:	The number of blank entries to create in the capture table.
		//
		CaptureData( unsigned short captureCount = 0 )
			: _captureCount( captureCount )
		{
			if ( _captureCount > 0 )
			{
				_captured = anewa( Pair*, _captureCount );
				memset( _captured, 0, sizeof( Pair* ) * _captureCount );
			}
		}

		template <typename Type>
		//
		// Retrieves the captured data associated with the given name.
		// - name:	The name of the captured data.
		//
		Type* Get( char* name )
		{
			void* output = nullptr;

			for ( unsigned short index = 0; index < _captureCount; index++ )
			{
				if ( _captured[ index ] != nullptr && _captured[ index ]->_name == name )
				{
					output = _captured[ index ]->_value;
					break;
				}
			}

			return ( Type* ) output;
		}

		//
		// Associates the value with the given name.
		// - name:		The name of the data.
		// - value:		The value to associate with the name.
		// - returns:	A flag indicating whether the value was successfully stored.
		//
		bool Capture( char* name, void* value )
		{
			bool result = false;

			for ( unsigned short index = 0; index < _captureCount; index++ )
			{
				if ( _captured[ index ] == nullptr )
				{
					_captured[ index ] = anew( Pair )( name, value );
					result = true;
					break;
				}
			}

			return result;
		}
	};

#define BYVAL(val) #val, val
#define BYREF(val) #val, &val

	template <typename... Args>
	//
	// Creates a CaptureData object out of the provided arguments.
	// - args:		The values to capture, provided in a name/value sequence.
	// - returns:	A CaptureData object containing the provided data.
	// - example:	Capture( "name", name, "person", person, "cash", cash );
	//
	CaptureData* Capture( Args... args )
	{
		auto output = anew( CaptureData )( sizeof...( args ) / 2 );

		if ( sizeof...( args ) > 0 )
		{
			ContinueCapture( output, args... );
		}

		return output;
	}

	template <typename ValueType, typename... Args>
	//
	// Provides additional parsing for the Capture method.
	//
	void ContinueCapture( CaptureData* capture, const char* name, ValueType* value, Args... args )
	{
		capture->Capture( const_cast< char* >( name ), value );

		if ( sizeof...( args ) > 0 )
		{
			ContinueCapture( capture, args... );
		}
	}

	//
	// Provides terminal processing for the Capture method.
	//
	void ContinueCapture( CaptureData* capture );
}

#endif