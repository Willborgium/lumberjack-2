#ifndef HYJYNX_CORE_DELEGATE_H
#define HYJYNX_CORE_DELEGATE_H

#include "Dictionary.h"
#include "Text.h"
#include <functional>

namespace HyJynxCore
{
	//
	// Implements a multi-method caller.
	//
	template <typename... Args>
	class Delegate
	{
	protected:

		HyJynxCollections::Dictionary<Text, std::function<void( Args... )>> _methods;

	public:

		//
		// Initializes a default instance of the Delegate class.
		//
		Delegate( )
		{ }

		//
		// Adds the given method to the delegate.
		// - name:		The name by which the method can be referenced.
		// - method:	The method to be added.
		//
		void Append( Text name, std::function<void( Args... )> method )
		{
			_methods.Append( name, method );
		}

		//
		// Executes all methods registered to this delegate.
		// - args: The arguments necessary to call the delegate.
		//
		void Execute( Args... args )
		{
			_methods.ForEach( [&] ( KeyValuePair<Text, std::function<void(Args...)>>& method )
			{
				method.Value( args... );
			} );
		}

		//
		// Removes all methods registered to this delegate.
		//
		void Clear( )
		{
			_methods.Clear( );
		}

		//
		// Removes the given method from the delegate.
		// - name: The name of the method to be removed.
		// - returns bool: true if the name was found and removed
		//
		bool Remove( Text name )
		{
			bool result = false;

			if ( _methods.ContainsKey( name ) )
			{
				_methods.Remove( name );
				result = true;
			}

			return result;
		}

		void operator -= ( Text name )
		{
			Remove( name );
		}

		void operator () ( Args... args )
		{
			Execute( args... );
		}
	};
}

#endif