#ifndef HYJYNX_CORE_DATA_H
#define HYJYNX_CORE_DATA_H

#include "Text.h"

namespace HyJynxCore
{
	//
	// Provides a simple base for named data.
	//
	class Data
	{
	protected:
		Text _name;
	public:
		//
		// Initializes an instance of the Data class with the given name.
		// - name: The name the Data can be associated with.
		//
		Data( Text name );

		//
		// Returns the name of the object.
		// - returns: The name of the Data.
		//
		Text& GetName( );
		
		//
		// When overriden in a derived class, converts the current object to text.
		// - returns: A textual representation of the object.
		//
		virtual Text ToText( );
	};
}

#endif