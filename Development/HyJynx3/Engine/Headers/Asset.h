#ifndef HYJYNX_CORE_ASSET_H
#define HYJYNX_CORE_ASSET_H

namespace HyJynxCore
{
	//
	// Provides a mechanism for retrieving the data of an encrypted file.
	//
	struct Asset
	{
		//
		// The size, in bytes, of the decrypted data.
		//
		unsigned int _sizeInBytes;

		//
		// The relative path to the file.
		//
		char const* _filePath;

		Asset( ) = delete;
		
		//
		// Initializes a new instance of the Asset class with the specified path and size.
		// - filePath:		The relative path to the file.
		// - sizeInBytes:	The size, in bytes, of the decrypted data.
		//
		Asset( char* filePath, unsigned int sizeInBytes );

		//
		// Loads and decrypts the file this asset represents.
		// - returns:	The decrypted data found at the file path.
		//
		char* Load( );
	};
}

#endif