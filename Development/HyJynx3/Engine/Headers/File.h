#ifndef HYJYNX_CORE_FILE_H
#define HYJYNX_CORE_FILE_H

#include "TypeDefinitions.h"
#include <fstream>

namespace HyJynxCore
{
	typedef std::fstream ASCIIFile;

	//
	// Provides basic operations for opening, reading, and writing to a file.
	//
	class File
	{
	protected:
		char* _path;
		bool _isOpen;
		int _mode;
		ASCIIFile _file;
	public:

		//
		// Provides an enumeration of modes that a file can be opened with.
		//
		static class OpenMode abstract sealed
		{
		public:
			//
			// The file is not open.
			//
			static const int Closed = 0;

			//
			// The file can be read.
			//
			static const int Read = 1;

			//
			// The file can be appended.
			//
			static const int Write = 2;

			//
			// The file can be overwritten.
			//
			static const int Overwrite = 3;

			//
			// The file supports reading and writing.
			//
			static const int ReadWrite = 4;
		};

		//
		// Initializes a new instance of the File class.
		//
		File();

		//
		// Initializes a new instance of the File class with the given path, but
		// does not open it.
		// - path: That path of the file on the system.
		//
		File(char* path);

		//
		// Initializes a new instance of the File class with the given path, but
		// does not open it.
		// - path: That path of the file on the system.
		//
		File(const char* path);

		~File();

		//
		// Returns whether or not the file is open.
		//
		bool IsOpen() const;

		//
		// Returns the mode in which this file is opened for.
		//
		int Mode() const;

		//
		// Opens the file in the given mode.
		// - mode:		The manner in which the file should be opened. Acceptable values are defined by
		//				the HyJynxCore::File::OpenMode class.
		// - isBinary:	Sets whether the file should be interpreted as binary.
		//
		void Open(int mode, bool isBinary = false);

		//
		// Opens the argued filepath in the given mode
		// - path:		The filepath of the file in question
		// - mode:		The manner in which the file should be opened. Acceptable values are defined by
		//				the HyJynxCore::File::OpenMode class.
		// - isBinary:	Sets whether the file should be interpreted as binary.
		//
		void Open( _In_ char* path, _In_ int mode, bool isBinary = false );

		//
		// Closes the file.
		//
		void Close();

		//
		// Reads a number of bytes from the beginning of the file. Calling this method will fail if the file is not opened for reading.
		//
		char* Read(LongLong count);

		//
		// Returns the length, in bytes, of the file stream.
		//
		LongLong GetFileLength();

		//
		// Returns the data that is read from the file stream from a given starting point.
		// - start:		The index at which to start reading from.
		// - count:		The number of bytes to read from the starting position.
		// - returns:	If the file is open for reading, returns the bytes that were read from
		//				the given start position. Otherwise, returns nullptr.
		//
		char* Read(LongLong start, LongLong count);

		//
		// Reads the entire file.
		// - returns: The entire contents of the file.
		//
		char* ReadToEnd();

		//
		// Writes the given Text to the file, overwriting all previous data. Calling this method will fail if the file is not opened for overwriting.
		//
		void Write(char* value);

		//
		// Writes the given Text to the end of the file. Calling this method will fail if the file is not opened for writing.
		//
		void Append(char* value);

		//
		// Writes the given Text to the end of the file followed by a carriage return. Calling this method will fail if the file is not opened for writing.
		//
		void AppendLine(char* value);
	};
}

#endif