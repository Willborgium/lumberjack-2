#ifndef HYJYNX_SCRIPTING_SCRIPTABLE_BASE_H
#define HYJYNX_SCRIPTING_SCRIPTABLE_BASE_H

#include "TypeDefinitions.h"

#include "Text.h"
#include "Collections.h"

namespace HyJynxScripting
{
	struct ScriptParameter
	{
		HyJynxCore::Text Type;
		HyJynxCore::Text Name;
		HyJynxCore::Text Value;
		bool IsStronglyTyped;
	};

	class ScriptableBase
	{
	protected:

		struct ParameterInfo
		{
			UInt Size;
			const type_info* DataType;
			HyJynxCore::Text TypeName;
		};

		struct MethodInfo
		{
			void* Address;
			bool HasReturnValue;
			UInt ReturnValueSize;
			UInt ParameterCount;
			UInt ParameterTotalSize;

			HyJynxCollections::DynamicCollection<ParameterInfo*> Parameters;
		};

		template <typename...ArgTypes>
		MethodInfo* GetMethodInfo(void(*method)(ArgTypes...));

		template <typename ReturnType, typename...ArgTypes>
		MethodInfo* GetMethodInfo(ReturnType(*method)(ArgTypes...));

		template <typename First, typename...Rest>
		void DefineParameters(MethodInfo* method, ...);

		template <typename First>
		void DefineParameters(MethodInfo* method, int);

		template <typename Type>
		struct IsPointer;

		template <typename Type>
		struct IsPointer<Type*>;

		HyJynxCore::Text _textType;

		HyJynxCollections::Dictionary<HyJynxCore::Text, MethodInfo*> _methods;
	public:

		template <typename ReturnType, typename...ArgTypes>
		void AddMethod(HyJynxCore::Text name, ReturnType(*method)(ArgTypes...));

		void* CallMethod(HyJynxCore::Text name, HyJynxCollections::ICollection<ScriptParameter*>* parameters, UInt* returnSize);

		template <typename Type>
		Type CallMethod(HyJynxCore::Text name, HyJynxCollections::ICollection<ScriptParameter*>* parameters, UInt* returnSize);
	};

#include "ScriptableBase.inl"
}

#endif