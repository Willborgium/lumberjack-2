#ifndef HYJYNX_DESIGN_DYNAMIC_CALLBACK_H
#define HYJYNX_DESIGN_DYNAMIC_CALLBACK_H

#include "CallbackBase.h"
#include "CaptureData.h"
#include "ProxyCall.h"
#include "Allocator.h"

#include <functional>

namespace HyJynxDesign
{
	template <typename ReturnType, typename... Args>
	//
	// Provides the ability to dynamically create a callback, as opposed to
	// defining one out-of-line.
	//
	class DynamicCallback : public CallbackBase < ReturnType, Args... >
	{
	private:
		typedef ProxyCall<ReturnType( CaptureData*, Args... )> CallType;
		CallType _callee;
	public:
		DynamicCallback( CallType method, CaptureData* capture = nullptr )
			: CallbackBase( capture ), _callee( method )
		{
		}

		ReturnType operator()( Args... args )
		{
			return _callee( _capture, args... );
		}
	};

	template <typename ReturnType, typename... Args, typename U>
	DynamicCallback<ReturnType, Args...>* Callback( U* method, CaptureData* capture = nullptr )
	{
		return anew( (DynamicCallback<ReturnType, Args...>) )( Proxy<ReturnType( CaptureData*, Args... )>( method ), capture );
	}
}

#endif