#ifndef HYJYNX_UTILITIES_HELPER_H
#define HYJYNX_UTILITIES_HELPER_H

#include "GlobalMemoryPool.h"
#include "TypeDefinitions.h"
#include "Text.h"
#include "Collections.h"

#include <Windows.h>
#include <functional>

namespace HyJynxUtilities
{
	//
	// Provides a class that has miscellaneous, nice-to-have methods.
	//
	class Helper abstract sealed
	{
	public:
		//
		// The name of the COM class that represents the window to be created by CreateDefaultWindow.
		//
		static const char* WindowClassName;

		//
		// Returns a handle to a window of a given size that has a caption, minimize and close buttons, and is not resizable.
		//
		static HWND CreateDefaultWindow( WNDPROC winproc, HINSTANCE instance, HyJynxCore::Text caption, int width, int height );

		//
		// Checks the provided GlobalMemoryPool to see if the first four bytes are 0xDEADBEEF, signaling
		// that this is not the first call to IsFirstInstance.
		// - pool:				The pool to check in.
		// - markFirstInstance:	If true, if this is the first instance found in the given pool, it will be marked as such.
		//
		static bool IsFirstInstance( HyJynxMemoryManagement::GlobalMemoryPool& pool, bool markFirstInstance );

		//
		// Displays a Win32 MessageBox.
		// - message: The message to display on the message box.
		//
		static void ShowMessage( HyJynxCore::Text message );

		//
		// Returns a list of file paths that match the specified filter.
		// - directory:		The directory to search in.
		// - filter:		The type of file to look for.
		// - returns:		A collection of file paths that match the specified filter.
		//
		static HyJynxCollections::DynamicCollection<HyJynxCore::Text> FindFiles(HyJynxCore::Text directory, HyJynxCore::Text filter = "*.*");

		//
		// Expands a directory that contains .. into the actual directory.
		// - directory:	The directory to expand.
		// - returns:	The expanded version of the given directory.
		//
		static HyJynxCore::Text ExpandDirectory(HyJynxCore::Text directory);
	};
}

#endif