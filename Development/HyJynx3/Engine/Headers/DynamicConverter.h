#ifndef HYJYNX_CORE_DYNAMIC_CONVERTER_H
#define HYJYNX_CORE_DYNAMIC_CONVERTER_H

#include "IConverter.h"
#include <functional>

namespace HyJynxCore
{
	//
	// Provides a class that can represent a converter that exists as
	// a lambda method or function object.
	//
	class DynamicConverter : public IConverter
	{
	private:
		std::function<void*(void*)> _method;

	public:
		//
		// Creates an instance of the DynamicConverter class with no underlying conversion method.
		//
		DynamicConverter( );

		//
		// Creates an instance of the DynamicConverter class with the given
		// method as the underlying conversion method.
		// - method:	The method to be used when Convert is called.
		//
		DynamicConverter( std::function<void*(void*)> method );

		//
		// Sets the underlying method used for conversions.
		// - method:	The method to be used when Convert is called.
		//
		void SetConvertMethod( std::function<void*(void*)> method );

		//
		// Converts the given data using a dynamically declared method.
		// - object:	The data to be converted.
		// - returns:	The data returned by the underlying conversion method. Because
		//				this method is declared outside of the scope of this class, it's return
		//				value cannot be guaranteed.
		//
		virtual void* Convert( void* object );
	};
}

#endif