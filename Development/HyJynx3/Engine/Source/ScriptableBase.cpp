#include "..\Headers\Scripting.h"
#include "..\Headers\Core.h"
#include "..\Headers\MemoryManagement.h"
#include "..\Headers\Collections.h"

using namespace HyJynxScripting;
using namespace HyJynxCore;
using namespace HyJynxMemoryManagement;
using namespace HyJynxCollections;

void* ScriptableBase::CallMethod(Text name, ICollection<ScriptParameter*>* parameters, UInt* returnSize)
{
	return nullptr;
//	void* outVal = nullptr;
//
//	if (_textType.IsNullOrWhitespace())
//	{
//		_textType = typeid(Text*).raw_name();
//	}
//
//	auto candidates = _methods.Where([&](KeyValuePair<Text, MethodInfo*>& method)
//	{
//		return method.Key == name && method.Value->ParameterCount == parameters->Count();
//	});
//
//	if (candidates != nullptr)
//	{
//		MethodInfo* method = nullptr;
//
//		UInt count = candidates->Count();
//
//		if (count == 1)
//		{
//			method = candidates->First().Value;
//		}
//		else
//		{
//			bool isComplete = false;
//
//			candidates->ForEach([&](KeyValuePair<Text, MethodInfo*> candidate)
//			{
//				if (isComplete)
//				{
//					return;
//				}
//
//				bool isMatch = true;
//
//				for (UInt index = 0; index < candidate.Value->ParameterCount; index++)
//				{
//					auto scriptParam = parameters->ElementAt(index);
//					auto candidateParam = candidate.Value->Parameters.ElementAt(index);
//
//					try
//					{
//						if (candidateParam->TypeName != scriptParam->Type && scriptParam->Value != nullptr && scriptParam->Value != "null")
//						{
//							if (!Converter::Instance()->ConverterExists(_textType, candidateParam->TypeName))
//							{
//								isMatch = false;
//								break;
//							}
//						}
//					}
//					catch (...)
//					{
//						isMatch = false;
//						break;
//					}
//				}
//
//				if (isMatch)
//				{
//					method = candidate.Value;
//					isComplete = true;
//					return;
//				}
//			});
//		}
//
//		if (method != nullptr)
//		{
//			DynamicCollection<void*> paramValues;
//			UInt bufferElementCount = 0;
//			int* buffer = nullptr;
//
//			for (UInt index = 0; index < method->ParameterCount; index++)
//			{
//				void* data = Converter::Instance()->Convert(_textType, Text(method->Parameters.ElementAt(index)->TypeName), &parameters->ElementAt(index)->Value, nullptr);
//				paramValues.Append(data);
//			}
//
//			if (method->ParameterTotalSize > 0)
//			{
//				bufferElementCount = (method->ParameterTotalSize / 4) + 1;
//				buffer = anewa(int,bufferElementCount);
//				memset(buffer, 0, bufferElementCount * sizeof(int));
//			}
//
//			// if it's a this-call, pass the this pointer here
//
//			UInt currentOffset = 0;
//			UInt currentIndex = 0;
//			paramValues.ForEach([&](void* value)
//			{
//				int size = method->Parameters.ElementAt(currentIndex)->Size;
//				memcpy(((char*)buffer) + currentOffset, value, size);
//				currentOffset += size;
//			});
//
//			void* methodAddr = method->Address;
//			int registerCount = 0;
//
//			*returnSize = method->ReturnValueSize;
//			int returnValueCount = 0;
//
//			if (method->HasReturnValue && *returnSize > 4)
//			{
//				outVal = method->HasReturnValue ? anewa(char,*returnSize) : nullptr;
//				returnValueCount = (*returnSize / 4) + (*returnSize % 4 != 0 ? 1 : 0);
//				memset(outVal, 0, *returnSize);
//
//				int rSize = *returnSize;
//
//				__asm
//				{
//					push rSize
//						lea eax, [outVal]
//				}
//			}
//
//			for (int index = bufferElementCount - 1; index >= 0; index--)
//			{
//				int val = buffer[index];
//
//				__asm
//				{
//					push val
//				}
//
//				registerCount++;
//			}
//
//			if (method->HasReturnValue && *returnSize > 4)
//			{
//				__asm
//				{
//					lea edx, [outVal]
//						push edx
//				}
//			}
//
//			int espOffset = (bufferElementCount + ((method->HasReturnValue && *returnSize > 4) ? 2 : 0)) * 4;
//
//			if (*returnSize <= 4)
//			{
//				__asm
//				{
//					call methodAddr
//						mov outVal, eax
//						add esp, espOffset
//				}
//			}
//			else
//			{
//				__asm
//				{
//					call methodAddr
//						add esp, espOffset
//				}
//			}
//
//			delete[] buffer;
//
//			while (paramValues.Count() > 0)
//			{
//				delete paramValues.First();
//				paramValues.RemoveAt(0);
//			}
//		}
//	}
//
//	// Don't worry about this, it's correct. Like a transformer, there's
//	// more than meets the eye here.
//#pragma warning( disable : 4172 )
//	return &outVal;
}