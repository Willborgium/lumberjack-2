#include "..\Headers\Core.h"
#include "..\Headers\Utilities.h"
#include "..\Headers\MemoryManagement.h"
#include "..\Headers\Collections.h"

#include <functional>

using namespace HyJynxCore;
using namespace HyJynxUtilities;
using namespace HyJynxMemoryManagement;
using namespace HyJynxCollections;


ExceptionHandler::ExceptionHandler( )
{
	_fatalHandler = [&]( Exception* err )
		{
			//Helper::ShowMessage( err->GetMessageText( ) );
		};
}

void ExceptionHandler::PrivateRaiseException( Exception* exception )
{
	if( exception != nullptr )
	{
		_exceptions.Append( exception );

		if( exception->GetLevel( ) == ExceptionLevel::Fatal )
		{
			_fatalHandler(exception);
		}
	}
}

void ExceptionHandler::RaiseException( Exception* exception )
{
	Instance( )->PrivateRaiseException( exception );
}

void ExceptionHandler::RaiseException( Text message )
{
	RaiseException( anew(Exception)( message, ExceptionLevel::Fatal ) );
}

void ExceptionHandler::RaiseWarning( Text message )
{
	RaiseException( anew(Exception)( message, ExceptionLevel::Warning ) );
}

void ExceptionHandler::RaiseMessage( Text message )
{
	RaiseException( anew(Exception)( message, ExceptionLevel::Information ) );
}
			
DynamicCollection<Exception*>& ExceptionHandler::GetExceptions( )
{
	return _exceptions;
}

void ExceptionHandler::SetFatalHandler( std::function<void(Exception*)> handler )
{
	_fatalHandler = handler;
}