#include "..\Headers\Utilities.h"
using namespace HyJynxUtilities;

const long double Pi()
{
	return 3.141592653589793;
}
const long double TwoPi()
{
	return 6.283185307179586;
}
const long double E()
{
	return 2.718281828459045;
}

long double ArcCosine(long double value)
{
	return acos(value);
}
long double ArcSine(long double value)
{
	return asin(value);
}
long double ArcTangent(long double value)
{
	return atan(value);
}
long double ArcTangent2(long double x, long double y)
{
	return atan2(y, x);
}

long double Sine(long double angle)
{
	return sin(angle);
}
long double Cosine(long double angle)
{
	return cos(angle);
}
long double Tangent(long double angle)
{
	return tan(angle);
}

long double HyperbolicSine(long double angle)
{
	return sinh(angle);
}
long double HyperbolicCosine(long double angle)
{
	return cosh(angle);
}
long double HyperbolicTanget(long double angle)
{
	return tanh(angle);
}

int Ceiling(long double value)
{
	return int(ceil(value));
}
int Floor(long double value)
{
	return int(floor(value));
}
int Remainder(int numerator, int denominator)
{
	return numerator % denominator;
}

long double Log(long double value)
{
	return log(value);
}

long double SquareRoot(long double value)
{
	return sqrt(value);
}

long double LinearInterpolation(long double percent, long double min, long double max)
{
	return min + ((max - min) * percent);
}

long double SinusoidalInterpolation(long double percent, long double min, long double max, bool inverted = false)
{
	if (inverted)
	{
		return min + ((max - min) * (Cosine(TwoPi() * percent)));
	}
	else
	{
		return min + ((max - min) * (Sine(TwoPi() * percent)));
	}
}