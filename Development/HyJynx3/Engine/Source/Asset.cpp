#include "..\Headers\Core.h"
using namespace HyJynxCore;

Asset::Asset( char* filePath, unsigned int sizeInBytes )
	: _filePath( filePath ), _sizeInBytes( sizeInBytes )
{
}

char* Asset::Load( )
{
	char* output = nullptr;

	File target( _filePath );

	target.Open( File::OpenMode::Read, true );

	char* encryptedData = target.ReadToEnd( );
	LongLong encryptedLength = target.GetFileLength( );
	output = Decrypt( encryptedData, ( unsigned int ) encryptedLength );

	return output;
}