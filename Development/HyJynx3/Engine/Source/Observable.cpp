#include "..\Headers\Design.h"
#include "..\Headers\Core.h"

using namespace HyJynxDesign;
using namespace HyJynxCore;

void Observable::AddListener( IListener* listener )
{
	if( !_listeners.Contains( listener ) )
	{
		_listeners.Append( listener );
	}
}

bool Observable::RemoveListener( IListener* listener )
{
	if( _listeners.Contains( listener ) )
	{
		_listeners.Remove( listener );
		return true;
	}

	return false;
}

bool Observable::RemoveListener( Text name )
{
	IListener* toRemove = nullptr;

	_listeners.ForEach( [&]( IListener* listener )
		{
			if( listener->GetName( ) == name )
			{
				toRemove = listener;
			}
		} );

	if( toRemove != nullptr )
	{
		_listeners.Remove( toRemove );
		return true;
	}

	return false;
}

void Observable::NotifyMessage( Text message )
{
	if( message.Length( ) > 0 )
	{
		_listeners.ForEach( [&message]( IListener* listener )
			{
				listener->ReceiveMessage( message );
			} );
	}
}

void Observable::Notify( Data* data )
{
	if( data != nullptr )
	{
		_listeners.ForEach( [&data]( IListener* listener )
			{
				listener->ReceiveNotification( data );
			} );
	}
}