/**
 * Dirty.cpp
 */

#include "..\Headers\Core.h"
#include "..\Headers\Dirty.h"

using namespace std;
using namespace HyJynxCore;

namespace HyJynxUtilities
{
	// default ctor - value of true
	Dirty::Dirty( )
		: _isDirty( true )
	{
	}

	// setup ctor - input value
	Dirty::Dirty( _In_ bool value )
		: _isDirty( value )
	{
	}

	// dtor
	Dirty::~Dirty( )
	{
	}

	// return true if change has occured
	bool Dirty::IsDirty( ) const
	{
		return _isDirty;
	}

	// manually set dirty
	void Dirty::SetDirty( _In_ bool value )
	{
		_isDirty = value;
	}

	// set dirty flag to true
	void Dirty::ToggleDirty( )
	{
		if ( _isDirty == false )
		{
			//_onDirty( [ ] ( this ) { } );
		}

		_isDirty = true;
	}

	// set dirty flag to false
	void Dirty::ToggleClean( )
	{
		if ( _isDirty == true )
		{
			//_onClean.Execute( [ ] ( this ) { } );
		}

		_isDirty = false;
	}

	// get the on-dirty delegate, this is executed after we go from clean to dirty state.
	Delegate< void* >* Dirty::GetOnDirtyDelegate( )
	{
		return &_onDirty;
	}

	// get the on-clean delegate, this is executed after we go from a dirty to clean state.
	Delegate< void* >* Dirty::GetOnCleanDelegate( )
	{
		return &_onClean;
	}
}