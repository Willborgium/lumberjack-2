//#include <Windows.h>
//
//#include "..\Headers\Core.h"
//#include "..\Headers\MemoryManagement.h"
//#include "..\Headers\Collections.h"
//#include "..\Headers\Design.h"
//#include "..\Headers\Scripting.h"
//#include "..\Headers\Utilities.h"
//
//#include "..\Headers\EntryPoint.h"
//
//using namespace HyJynxCore;
//using namespace HyJynxMemoryManagement;
//using namespace HyJynxCollections;
//using namespace HyJynxDesign;
//using namespace HyJynxScripting;
//using namespace HyJynxUtilities;
//
//LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
//{
//	switch (message)
//	{
//	case WM_DESTROY:
//		PostQuitMessage(0);
//		break;
//	default:
//		return DefWindowProc(hWnd, message, wParam, lParam);
//	}
//
//	return 0;
//}
//
//int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, char* lpCmdLine, int nCmdShow)
//{
//	GlobalMemoryPool pool("HyJynx Instance", 4);
//
//	pool.Open();
//
//	// Initialize the system timer.
//	auto timer = TimingSystem::Instance();
//	timer->Initialize();
//	timer->Begin();
//
//	DynamicCollection<Data*> data;
//	data.Append(new BoxedData<Text>("CommandLineArguments", lpCmdLine));
//
//	if (!AppMain(data))
//	{
//		pool.Close();
//		return 1;
//	}
//
//	if (!Helper::IsFirstInstance(pool, true))
//	{
//		return 1;
//	}
//
//	HWND hWnd = Helper::CreateDefaultWindow(WndProc, hInstance, "HyJynx Engine", 1024, 768);
//
//
//	if (hWnd == nullptr)
//	{
//		pool.Close();
//		return 1;
//	}
//
//	// Initialize the dispatcher
//	auto dispatcher = DispatchingSystem::Instance();
//	dispatcher->Initialize();
//
//	MSG msg;
//	ZeroMemory(&msg, sizeof(MSG));
//
//	ShowWindow(hWnd, nCmdShow);
//	UpdateWindow(hWnd);
//
//	while (msg.message != WM_QUIT)
//	{
//		timer->Update();
//		if (PeekMessage(&msg, nullptr, 0U, 0U, PM_REMOVE))
//		{
//			TranslateMessage(&msg);
//			DispatchMessage(&msg);
//		}
//		else
//		{
//			// input
//			dispatcher->Update();
//			// render
//
//		}
//	}
//
//	timer->End();
//	timer->Uninitialize();
//
//	dispatcher->End();
//	dispatcher->Uninitialize();
//
//	pool.Close();
//
//	return int(msg.wParam);
//}