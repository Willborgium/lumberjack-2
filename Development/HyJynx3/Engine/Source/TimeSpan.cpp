#include "..\Headers\Core.h"
using namespace HyJynxCore;

TimeSpan::TimeSpan( )
	: _seconds( 0 )
{
}
TimeSpan::TimeSpan( double seconds )
	: _seconds( seconds )
{
}
TimeSpan::TimeSpan( double seconds, double minutes )
	: _seconds( seconds + ( minutes * 60 ) )
{
}
TimeSpan::TimeSpan( double seconds, double minutes, double hours )
	: _seconds( seconds + ( minutes * 60.0 ) + ( hours * 3600.0 ) )
{
}
TimeSpan::TimeSpan( double seconds, double minutes, double hours, double days )
	: _seconds( seconds + ( minutes * 60.0 ) + ( hours * 3600.0 ) + ( days * 86400.0 ) )
{
}

double TimeSpan::TotalSeconds( ) const
{
	return _seconds;
}
double TimeSpan::TotalMinutes( ) const
{
	return _seconds / 60.0;
}
double TimeSpan::TotalHours( ) const
{
	return _seconds / 3600.0;
}
double TimeSpan::TotalDays( ) const
{
	return _seconds / 86400.0;
}

void TimeSpan::AddSeconds( double value )
{
	_seconds += value;
}
void TimeSpan::AddMinutes( double value )
{
	_seconds += value * 60.0;
}
void TimeSpan::AddHours( double value )
{
	_seconds += value * 3600.0;
}
void TimeSpan::AddDays( double value )
{
	_seconds += value * 86400.0;
}
void TimeSpan::Add( TimeSpan& value )
{
	_seconds += value._seconds;
}

TimeSpan TimeSpan::operator+( TimeSpan& rhs ) const
{
	return TimeSpan( _seconds + rhs._seconds );
}
TimeSpan TimeSpan::operator-( TimeSpan& rhs ) const
{
	return TimeSpan( _seconds - rhs._seconds );
}
TimeSpan TimeSpan::operator*( TimeSpan& rhs ) const
{
	return TimeSpan( _seconds * rhs._seconds );
}
TimeSpan TimeSpan::operator/( TimeSpan& rhs ) const
{
	return TimeSpan( _seconds / rhs._seconds );
}

TimeSpan TimeSpan::operator+=( TimeSpan& rhs )
{
	_seconds = ( _seconds + rhs._seconds );
	return TimeSpan( _seconds );
}
TimeSpan TimeSpan::operator-=( TimeSpan& rhs )
{
	_seconds = ( _seconds - rhs._seconds );
	return TimeSpan( _seconds );
}
TimeSpan TimeSpan::operator*=( TimeSpan& rhs )
{
	_seconds = ( _seconds * rhs._seconds );
	return TimeSpan( _seconds );
}
TimeSpan TimeSpan::operator/=( TimeSpan& rhs )
{
	_seconds = ( _seconds / rhs._seconds );
	return TimeSpan( _seconds );
}

bool TimeSpan::operator==( TimeSpan& rhs ) const
{
	return _seconds == rhs._seconds;
}
bool TimeSpan::operator!=( TimeSpan& rhs ) const
{
	return _seconds != rhs._seconds;
}
bool TimeSpan::operator>( TimeSpan& rhs ) const
{
	return _seconds > rhs._seconds;
}
bool TimeSpan::operator<( TimeSpan& rhs ) const
{
	return _seconds < rhs._seconds;
}
bool TimeSpan::operator>=( TimeSpan& rhs ) const
{
	return _seconds >= rhs._seconds;
}
bool TimeSpan::operator<=( TimeSpan& rhs ) const
{
	return _seconds <= rhs._seconds;
}