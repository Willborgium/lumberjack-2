/**
 * SerialCompletionChecker.cpp
 * (c) 2014 All Rights Reserved
 */

#include "..\Headers\SerialCompletionChecker.h"

#include "..\..\Logger\Headers\Logger.h"

using namespace HyJynxLogger;
using namespace std;

namespace HyJynxUtilities
{
	// null & default ctor
	SerialCompletionChecker::SerialCompletionChecker()
	{ }

	// copy ctor
	SerialCompletionChecker::SerialCompletionChecker( _In_ const SerialCompletionChecker& other )
		: _stepList( other._stepList )
	{ }

	// move ctor
	SerialCompletionChecker::SerialCompletionChecker( _In_ const SerialCompletionChecker&& other )
		: _stepList( other._stepList )
	{ }

	// dtor
	SerialCompletionChecker::~SerialCompletionChecker( )
	{
		_stepList.Clear( );
	}

	// Add a step to the process
	void SerialCompletionChecker::AddStep(
		_In_ function< void( function<void(bool)> ) > step,
		_In_ function< void( void ) > onFail )
	{
		if ( step != nullptr && onFail != nullptr )
		{
			_stepList.Append( anew( ProcessStep )( step, onFail ) );
		}
		else
		{
			Logger::Log( "SerialCompletionChecker(nullptr) - invalid argument, will not add." );
		}
	}

	// recursive method to serially perform the steps
	void SerialCompletionChecker::performStep( _In_ UInt index, _In_ function< void( void ) > onComplete )
	{
		if ( index >= _stepList.Count( ) )
		{
			onComplete( ); // recursion stop
		}
		else
		{
			ProcessStep* step = _stepList.ElementAt( index );
			if ( step == nullptr )
			{
				onComplete( ); // recusion stop
			}
			else
			{
				step->Proc( [ &step, index, &onComplete, this ] ( _In_ bool success )
				{
					if ( success == true )
					{
						performStep( index + 1, onComplete ); // recursion go
					}
					else
					{
						if ( step->OnError != nullptr )
						{
							step->OnError( ); // recursion stop
						}
					}
				} );
			}
		}
	}

	// start the process - steps will be performed in the order which they were aded
	void SerialCompletionChecker::Start( _In_ function< void( void ) > onComplete )
	{
		// TODO: Begin recursive calls Async
		if ( _stepList.Count( ) > 0 )
		{
			performStep( 0, onComplete );
		}
	}
}