#include "..\Headers\Core.h"
using namespace HyJynxCore;

Data::Data( Text name )
	: _name( name )
{
}

Text& Data::GetName( )
{
	return _name;
}

Text Data::ToText( )
{
	return _name;
}