#include "..\Headers\Core.h"
using namespace HyJynxCore;

ConfigurationManager::ConfigurationManager( )
{
}

ConfigurationManager::ConfigurationManager( const ConfigurationManager& rhs )
{
}

ConfigurationManager::ConfigurationManager( const ConfigurationManager&& rhs )
{
}

JsonData* ConfigurationManager::EvaluateToLast( Text* expParts, UInt tokenCount )
{
	JsonData* output = nullptr;

	if ( _isLoaded )
	{
		output = _configFile;
		for ( UInt index = 0; index < tokenCount - 1; index++ )
		{
			if ( output != nullptr && output->DataType == JsonData::JsonDataType::Object )
			{
				output = output->Data->ObjectData->ValueOf( expParts [ index ] );
			}
			else
			{
				break;
			}
		}
	}

	return output;
}
ConfigurationManager::~ConfigurationManager( )
{
}

bool ConfigurationManager::LoadFile( Text fileName )
{
	bool result = false;

	if ( !fileName.IsNullOrWhitespace( ) )
	{
		_fileName = fileName;
		_configFile = JsonParser::ReadFile( fileName );
	}
	else
	{
		_fileName = "Config.json";
		Asset configFile = HyJynxCore::Assets::GetConfigjson( );
		char* configFileRaw = configFile.Load( );
		_configFile = JsonParser::TextToJson( configFileRaw );
		delete [] configFileRaw;
	}

	_isLoaded = result = _configFile != nullptr;

	return result;
}

JsonObject* ConfigurationManager::GetObjectValue( Text expression )
{
	JsonObject* output = nullptr;

	if ( _isLoaded )
	{
		UInt tokenCount = 0;
		auto expParts = expression.Split( '.', &tokenCount );

		auto result = EvaluateToLast( expParts, tokenCount );

		if ( result != nullptr )
		{
			output = JsonReader::PropertyValueData( result, expParts [ tokenCount - 1 ] );
		}
	}

	return output;
}

Text ConfigurationManager::GetTextValue( Text expression )
{
	Text output = Text::Empty( );

	if ( _isLoaded )
	{
		UInt tokenCount = 0;
		auto expParts = expression.Split( '.', &tokenCount );

		auto result = EvaluateToLast( expParts, tokenCount );

		if ( result != nullptr )
		{
			output = JsonReader::PropertyValueText( result, expParts [ tokenCount - 1 ] );
		}
	}

	return output;
}

int ConfigurationManager::GetIntValue( Text expression )
{
	int output = 0;

	if ( _isLoaded )
	{
		UInt tokenCount = 0;
		auto expParts = expression.Split( '.', &tokenCount );

		auto result = EvaluateToLast( expParts, tokenCount );

		if ( result != nullptr )
		{
			output = JsonReader::PropertyValueInt( result, expParts [ tokenCount - 1 ] );
		}
	}

	return output;
}

double ConfigurationManager::GetDoubleValue( Text expression )
{
	double output = 0.0;

	if ( _isLoaded )
	{
		UInt tokenCount = 0;
		auto expParts = expression.Split( '.', &tokenCount );

		auto result = EvaluateToLast( expParts, tokenCount );

		if ( result != nullptr )
		{
			output = JsonReader::PropertyValueDouble( result, expParts [ tokenCount - 1 ] );
		}
	}

	return output;
}

// retrieve the json expression as a bool
bool ConfigurationManager::GetBooleanValue( Text expression )
{
	bool output = false;

	if ( _isLoaded )
	{
		UInt tokenCount = 0;
		auto expParts = expression.Split( '.', &tokenCount );

		auto result = EvaluateToLast( expParts, tokenCount );

		if ( result != nullptr )
		{
			output = JsonReader::PropertyValueBoolean( result, expParts[ tokenCount - 1 ] );
		}
	}

	return output;
}