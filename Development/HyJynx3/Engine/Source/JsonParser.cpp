#include "..\Headers\Core.h"
#include "..\Headers\MemoryManagement.h"

using namespace HyJynxCore;
using namespace HyJynxMemoryManagement;

JsonArray* JsonParser::CreateArray( const MutableText& json, UInt startIndex, UInt* endIndex )
{
	JsonArray* output = new JsonArray( );

	UInt index = startIndex;

	char currentCharacter = json.CharacterAt( index );

	while ( currentCharacter != ']' )
	{
		switch ( currentCharacter )
		{
		case '[':
			output->Append( new JsonData( CreateArray( json, index + 1, &index ) ) );
			break;
		case '{':
			output->Append( new JsonData( CreateObject( json, index + 1, &index ) ) );
			break;
		case '\"':
			output->Append( new JsonData( ReadToToken( json, index + 1, '\"', &index ) ) );
			index++;
			break;
		case ',':
			index++;
			break;
		default:
			output->Append( new JsonData( ReadToToken( json, index, ',', &index ) ) );
			break;
		}

		currentCharacter = json.CharacterAt( index );
	}

	*endIndex = index + 1;

	return output;
}

JsonProperty* JsonParser::CreateProperty( const MutableText& json, UInt startIndex, UInt* endIndex )
{
	JsonProperty* output = new JsonProperty( );

	UInt index = startIndex;

	auto key = ReadToToken( json, index, '\"', &index );
	output->Key = *key;
	//delete key;

	Text* temp = nullptr;

	if ( json.CharacterAt( index + 1 ) == ':' )
	{
		index += 2;

		switch ( json.CharacterAt( index ) )
		{
		case '[':
			output->Value = new JsonData( CreateArray( json, index + 1, &index ) );
			break;
		case '{':
			output->Value = new JsonData( CreateObject( json, index + 1, &index ) );
			break;
		case '\"':
			output->Value = new JsonData( ReadToToken( json, index + 1, '\"', &index ) );
			index++;
			break;
		default:
			output->Value = new JsonData( ReadToToken( json, index, ',', &index ) );
			break;
		}
	}

	*endIndex = index;

	return output;
}

JsonObject* JsonParser::CreateObject( const MutableText& json, UInt startIndex, UInt* endIndex )
{
	JsonObject* output = new JsonObject( );

	UInt index = startIndex;
	JsonProperty* result = nullptr;

	char currentCharacter = json.CharacterAt( index );
	while ( currentCharacter != '}' )
	{
		switch ( currentCharacter )
		{
		case '\"':
			result = CreateProperty( json, index + 1, &index );
			output->Append( result->Key, result->Value );
			//delete result;
			break;
		case ',':
			index++;
			break;
		}

		currentCharacter = json.CharacterAt( index );
	}

	*endIndex = index + 1;
	
	return output;
}

JsonValue* JsonParser::ReadToToken( const MutableText& data, UInt startIndex, char token, UInt* endIndex )
{
	UInt index = startIndex;

	char currentCharacter = data.CharacterAt( index );

	UInt length = data.TextLength( );

	while ( index < length && currentCharacter != token && currentCharacter != '}' && currentCharacter != ']' )
	{
		index++;
		currentCharacter = data.CharacterAt( index );
	}

	*endIndex = index;

	return new Text( data.GetSubText( startIndex, *endIndex ) );
}

JsonData* JsonParser::TextToJson( const Text& jsonAsText )
{
	JsonData* output = nullptr;

	MutableText noWhitespaceJSON( nullptr, jsonAsText.Length( ) );
	bool inQuotes = false;

	for ( UInt index = 0; index < jsonAsText.Length( ); index++ )
	{
		switch ( jsonAsText[ index ] )
		{
		case '\t':
		case '\r':
		case ' ':
			if (inQuotes)
			{
				noWhitespaceJSON += jsonAsText[ index ];
			}
			break;
		case '\n':
			break;
		case '\"':
			inQuotes = !inQuotes;
			noWhitespaceJSON += jsonAsText[ index ];
			break;
		default:
			noWhitespaceJSON += jsonAsText[ index ];
			break;
		}
	}

	UInt endIndex = 0;
	char firstChar = noWhitespaceJSON.CharacterAt( 0 );
	if (firstChar == '{')
	{
		output = new JsonData( CreateObject( noWhitespaceJSON, 1, &endIndex ) );
	}
	else if (firstChar == '[')
	{
		output = new JsonData( CreateArray( noWhitespaceJSON, 1, &endIndex ) );
	}

	return output;
}

JsonData* JsonParser::ReadFile( const Text& filename )
{
	JsonData* output = nullptr;

	File json( filename.GetValue() );
	json.Open( File::OpenMode::Read );
	Text rawJSON( json.ReadToEnd( ) );
	json.Close( );

	output = TextToJson(rawJSON);

	return output;
}