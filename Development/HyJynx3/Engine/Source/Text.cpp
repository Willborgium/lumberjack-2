#include "..\Headers\Core.h"
#include "..\Headers\MemoryManagement.h"

using namespace HyJynxCore;
using namespace HyJynxMemoryManagement;

#include <stdlib.h>
#include <cstring>

#pragma warning( disable : 4521 4522 )

#pragma region Constructors

// Creates an instance of the Text class with a default value of nullptr.
Text::Text()
{
	_value = nullptr;
}

// Creates an instance of the Text class and sets the value to the given value.
Text::Text(char rhs)
{
	_value = GetCopy(rhs);
	_ownsValue = true;
}

// Creates an instance of the Text class and sets the value to the given value.
Text::Text(const char* rhs)
{
	_value = rhs;// GetCopy( rhs );
}

// Creates an instance of the Text class and sets the value to the given value.
Text::Text(const Text& rhs)
{
	_value = rhs._value;
	_ownsValue = false;
}

Text::Text( Text&& rhs )
{
	_value = rhs._value;
	_ownsValue = rhs._ownsValue;
	rhs._ownsValue = false;
}

// Creates an instance of the Text class and sets the value to the given value.
Text::Text(int rhs)
{
	_value = FromInt( rhs );
	_ownsValue = true;
}

// Creates an instance of the Text class and sets the value to the given value.
Text::Text(UInt rhs)
{
	_value = FromUInt( rhs );
	_ownsValue = true;
}

// Creates an instance of the Text class and sets the value to the given value.
Text::Text(long rhs)
{
	_value = FromLong( rhs );
	_ownsValue = true;
}

// Creates an instance of the Text class and sets the value to the given value.
Text::Text(ULong rhs)
{
	_value = FromULong( rhs );
	_ownsValue = true;
}

// Creates an instance of the Text class and sets the value to the given value.
Text::Text(LongLong rhs)
{
	_value = FromLongLong( rhs );
	_ownsValue = true;
}

// Creates an instance of the Text class and sets the value to the given value.
Text::Text(ULongLong rhs)
{
	_value = FromULongLong( rhs );
	_ownsValue = true;
}

Text::operator const char*()
{
	return _value;
}

#pragma endregion

#pragma region Destructor

Text::~Text()
{
	if (_value && _ownsValue)
	{
		delete[] _value;
	}
}

#pragma endregion

#pragma region Methods

#pragma region Misc

// Returns a constant pointer to the native char array that represents the value.
const char* Text::GetValue() const
{
	return _value;
}

// Returns the length of the current text.
UInt Text::Length() const
{
	return nullptr != _value ? strlen(_value) : 0;
}

// Returns the hashed value of the given text.
UInt Text::GetHashValue() const
{
	size_t hash = 0;

	for (UInt index = 0; index < Length(); index++)
	{
		hash += _value[index];
	}

	return hash;
}

// Returns an array of Text objects that are seperated by the given token.
// The tokenCount parameter is assigned the count of elements returned.
Text* Text::Split(char token, UInt* tokenCount) const
{
	return Split(_value, token, tokenCount);
}

// Returns the character found at the given index in the current text.
char Text::CharacterAt(UInt index) const
{
	if (index < Length())
	{
		return _value[index];
	}
	else
	{
		return 0;
	}
}

char Text::operator[]( UInt index ) const
{
	return CharacterAt( index );
}

#pragma endregion

#pragma region Contains

// Returns a value indicating whether the given character was found in the
// text.
bool Text::Contains(char value) const
{
	bool result = false;
	UInt length = Length();

	if (length > 0)
	{
		for (UInt index = 0; index < length; index++)
		{
			if (_value[index] == value)
			{
				result = true;
				break;
			}
		}
	}

	return result;
}

// Returns a value indicating whether the given array of characters
// was found in the text.
bool Text::Contains(const char* value) const
{
	bool result = false;
	UInt length = Length();
	UInt inLength = LengthOf(value);

	if (length > 0 && inLength > 0 && length >= inLength)
	{
		bool isSearching = false;
		UInt searchIndex = 0;

		for (UInt index = 0; index < length; index++)
		{
			if (!isSearching)
			{
				if (_value[index] == value[0])
				{
					isSearching = true;
					searchIndex = 1;
				}
				else
				{
					isSearching = false;
					searchIndex = 0;
				}
			}
			else
			{
				if (_value[index] == value[searchIndex])
				{
					if (searchIndex == inLength - 1)
					{
						result = true;
						break;
					}
					else
					{
						searchIndex++;
					}
				}
				else
				{
					isSearching = false;
					searchIndex = 0;
				}
			}
		}
	}

	return result;
}

// Returns a value indicating whether the given text
// was found in the text.
bool Text::Contains(const Text& value) const
{
	return Contains(value._value);
}

#pragma endregion

#pragma region StartsWith

// Returns whether or not the current text begins with the given character.
bool Text::StartsWith(char value) const
{
	bool result = false;

	if (Length() > 0)
	{
		result = _value[0] == value;
	}

	return result;
}

// Returns whether or not the current text begins with the given character array.
bool Text::StartsWith(const char* value) const
{
	bool result = false;
	UInt length = Length();

	if (length > 0)
	{
		UInt vLength = LengthOf(value);

		if (length >= vLength && vLength > 0)
		{
			bool isEqual = true;

			for (UInt index = 0; index < vLength; index++)
			{
				if (_value[index] != value[index])
				{
					isEqual = false;
					break;
				}
			}

			result = isEqual;
		}
	}

	return result;
}

// Returns whether or not the current text begins with the given text.
bool Text::StartsWith(const Text& value) const
{
	return StartsWith(value._value);
}

#pragma endregion

#pragma region EndsWith

// Returns whether or not the current text ends with the given character.
bool Text::EndsWith(char value) const
{
	bool result = false;
	UInt length = Length();

	if (length > 0)
	{
		result = _value[length - 1] == value;
	}

	return result;
}

// Returns whether or not the current text ends with the given character array.
bool Text::EndsWith(const char* value) const
{
	bool result = false;
	UInt length = Length();

	if (length > 0)
	{
		UInt vLength = LengthOf(value);

		if (length >= vLength && vLength > 0)
		{
			UInt startIndex = length - vLength;

			bool isEqual = true;

			for (UInt index = 0; index < vLength; index++)
			{
				if (_value[startIndex + index] != value[index])
				{
					isEqual = false;
					break;
				}
			}

			result = isEqual;
		}
	}

	return result;
}

// Returns whether or not the current text ends with the given text.
bool Text::EndsWith(const Text& value) const
{
	return EndsWith(value._value);
}

#pragma endregion

#pragma region CountOf

// Returns the number of occurances of the given character in the text.
UInt Text::CountOf(char value) const
{
	UInt result = 0;
	UInt length = Length();

	if (length > 0)
	{
		for (UInt index = 0; index < length; index++)
		{
			if (_value[index] == value)
			{
				result++;
			}
		}
	}

	return result;
}

// Returns the number of occurances of the given array of characters in the text.
UInt Text::CountOf(const char* value) const
{
	UInt result = 0;
	UInt length = Length();
	UInt inLength = LengthOf(value);

	if (length > 0 && inLength > 0 && length >= inLength)
	{
		bool isSearching = false;
		UInt searchIndex = 0;

		for (UInt index = 0; index < length; index++)
		{
			if (!isSearching)
			{
				if (_value[index] == value[0])
				{
					isSearching = true;
					searchIndex = 1;
				}
				else
				{
					isSearching = false;
					searchIndex = 0;
				}
			}
			else
			{
				if (_value[index] == value[searchIndex])
				{
					if (searchIndex == inLength - 1)
					{
						result++;
						isSearching = false;
						searchIndex = 0;
					}
					else
					{
						searchIndex++;
					}
				}
				else
				{
					isSearching = false;
					searchIndex = 0;
				}
			}
		}
	}

	return result;
}

// Returns the number of occurances of the given text in the text.
UInt Text::CountOf(const Text& value) const
{
	return CountOf(value._value);
}

#pragma endregion

#pragma region Replace

// Replaces all occurances of 'oldValue' with 'newValue'.
char* Text::Replace(char oldValue, char newValue) const
{
	char* output = nullptr;

	UInt length = Length();

	if (length > 0)
	{
		output = GetCopy(_value);

		for (UInt index = 0; index < length; index++)
		{
			if (output[index] == oldValue)
			{
				output[index] = newValue;
			}
		}
	}

	return output;
}

// Replaces all occurances of 'oldValue' with 'newValue'.
char* Text::Replace(const char* oldValue, const char* newValue) const
{
	UInt length = Length();
	UInt oldLength = LengthOf(oldValue);
	UInt newLength = LengthOf(newValue);
	UInt outputLength = 0;

	char* output = nullptr;

	if (length > 0 && oldLength > 0)
	{
		UInt count = CountOf(oldValue);
		outputLength = length + (count * (newLength - oldLength));

		output = new char [ outputLength + 1 ];//anewa(char,outputLength + 1);
		output[outputLength] = 0;

		bool isSearching = false;
		UInt searchIndex = 0;
		UInt outputIndex = 0;

		for (UInt index = 0; index < length; index++)
		{
			if (!isSearching)
			{
				if (_value[index] == oldValue[0])
				{
					isSearching = true;
					searchIndex = 1;
				}
				else
				{
					isSearching = false;
					searchIndex = 0;
					output[outputIndex] = _value[index];
					outputIndex++;
				}
			}
			else
			{
				if (_value[index] == oldValue[searchIndex])
				{
					if (searchIndex == oldLength - 1)
					{
						memcpy(&output[outputIndex], newValue, newLength);
						outputIndex += newLength;
						isSearching = false;
						searchIndex = 0;
					}
					else
					{
						searchIndex++;
					}
				}
				else
				{
					memcpy(&output[outputIndex], &_value[index - searchIndex], searchIndex + 1);
					outputIndex += searchIndex + 1;
					isSearching = false;
					searchIndex = 0;
				}
			}
		}
	}

	return output;
}

// Replaces all occurances of 'oldValue' with 'newValue'.
char* Text::Replace(const Text& oldValue, const Text& newValue) const
{
	return Replace(oldValue._value, newValue._value);
}

#pragma endregion

#pragma region Trim

// Removes all leading occurrances of the specified character.
char* Text::TrimStart(char trimChar) const
{
	char* output = nullptr;

	auto length = Length();

	if (length > 0)
	{
		UInt startIndex = 0;

		while (startIndex < length && _value[startIndex] == trimChar)
		{
			startIndex++;
		}

		auto size = length - startIndex + 1;
		output = new char [ size ];//anewa(char,size);
		memcpy(output, &_value[startIndex], size);
	}

	return output;
}

// Removes all trailing occurrances of the specified character.
char* Text::TrimEnd(char trimChar) const
{
	char* output = nullptr;

	int length = Length();

	if (length > 0)
	{
		int endIndex = length - 1;

		while (endIndex > 0 && _value[endIndex] == trimChar)
		{
			endIndex--;
		}

		auto size = endIndex + 1;
		output = new char [ size ];//anewa(char,size);
		output[size - 1] = 0;
		memcpy(output, _value, size - 1);
	}

	return output;
}

// Removes all leading and trailing whitespace.
char* Text::TrimWhitespace() const
{
	return Trim(' ');
}

// Removes all leading and trailing occurrances of the specified character.
char* Text::Trim(char trimChar) const
{
	char* output = nullptr;

	int length = Length();

	if (length > 0)
	{
		int startIndex = 0;

		while (startIndex < length && _value[startIndex] == trimChar)
		{
			startIndex++;
		}

		int endIndex = length - 1;

		while (endIndex > startIndex && _value[endIndex] == trimChar)
		{
			endIndex--;
		}

		int size = (endIndex >= startIndex) ? endIndex - startIndex + 1 : 1;
		output = new char [ size + 1 ];//anewa(char,size + 1);
		output[size] = 0;
		memcpy(output, &_value[startIndex], size);
	}

	return output;
}

#pragma endregion

#pragma region Remove

// Removes the given number of characters from the beginning of the text.
char* Text::RemoveFromStart(UInt count) const
{
	UInt length = Length();
	UInt outLength = 0;

	if (length >= count)
	{
		outLength = length - count + 1;
	}
	else
	{
		outLength = 1;
	}

	auto output = new char [ outLength ];//anewa(char,outLength);
	output[outLength - 1] = 0;

	if (outLength > 1)
	{
		memcpy(output, &_value[count], outLength);
	}

	return output;
}

// Removes the given number of characters from the end of the text.
char* Text::RemoveFromEnd(UInt count) const
{
	UInt length = Length();
	UInt outLength = 0;

	if (length >= count)
	{
		outLength = length - count + 1;
	}
	else
	{
		outLength = 1;
	}

	auto output = new char [ outLength ];//anewa(char,outLength);
	output[outLength - 1] = 0;

	if (outLength > 1)
	{
		memcpy(output, _value, outLength - 1);
	}

	return output;
}

#pragma endregion

#pragma region IndexOf

// Returns the index of the first occurance of the given character.
// Returns the maximum value of an unsigned int if the given character is not found.
UInt Text::IndexOf(char value) const
{
	UInt result = ~0;
	UInt length = Length();

	if (length > 0)
	{
		for (UInt index = 0; index < length; index++)
		{
			if (_value[index] == value)
			{
				result = index;
				break;
			}
		}
	}

	return result;
}

// Returns the index of the first occurance of the given character array.
// Returns the maximum value of an unsigned int if the given character array is not found.
UInt Text::IndexOf(const char* value) const
{
	UInt result = ~0;
	UInt length = Length();

	if (length > 0)
	{
		UInt vLength = LengthOf(value);

		if (length > vLength && vLength > 0)
		{
			bool isSearching = false;
			UInt searchIndex = 0;

			for (UInt index = 0; index < length; index++)
			{
				if (!isSearching)
				{
					if (_value[index] == value[searchIndex])
					{
						isSearching = true;
						searchIndex++;
					}
				}
				else
				{
					if (_value[index] == value[searchIndex])
					{
						if (searchIndex == vLength - 1)
						{
							result = index - searchIndex;
							break;
						}
						else
						{
							searchIndex++;
						}
					}
					else
					{
						isSearching = false;
						searchIndex = 0;
					}
				}
			}
		}
	}

	return result;
}

// Returns the index of the first occurance of the given text.
// Returns the maximum value of an unsigned int if the given text is not found.
UInt Text::IndexOf(const Text& value) const
{
	return IndexOf(value._value);
}

// Returns the index of the first occurance of any character in
// the given character array. Returns the maximum value of an unsigned int
// if the none of given characters are not found.
UInt Text::IndexOfAny(const char* values) const
{
	UInt result = ~0;
	UInt length = Length();

	if (length > 0)
	{
		UInt vLength = LengthOf(values);

		if (vLength > 0)
		{
			for (UInt index = 0; index < length; index++)
			{
				for (UInt vIndex = 0; vIndex < vLength; vIndex++)
				{
					if (_value[index] == values[vIndex])
					{
						result = index;
						break;
					}
				}

				if (result != ~0)
				{
					break;
				}
			}
		}
	}

	return result;
}

// Returns the index of the first occurance of any character in
// the given text. Returns the maximum value of an unsigned int
// if the none of given characters are not found.
UInt Text::IndexOfAny(const Text& values) const
{
	return IndexOfAny(values._value);
}

#pragma endregion

#pragma region IsNull / OrEmpty / OrWhitespace

// Returns whether the current text is null.
bool Text::IsNull() const
{
	return nullptr == _value;
}

// Returns whether the current text is null or has no characters.
bool Text::IsNullOrEmpty() const
{
	return IsNull() || Length() == 0;
}

// Returns whether the current text is null, has no characters,
// or only whitespace characters.
bool Text::IsNullOrWhitespace() const
{
	bool result = true;

	if (!IsNull())
	{
		UInt length = Length();

		if (length > 0)
		{
			for (UInt index = 0; index < length; index++)
			{
				switch (_value[index])
				{
				case 9:		// horizontal tab
				case 10:	// new line
				case 11:	// vertical tab
				case 13:	// carriage return
				case 32:	// space
					result = true;
					break;
				default:
					result = false;
					break;
				}

				if (!result)
				{
					break;
				}
			}
		}
	}

	return result;
}

#pragma endregion

#pragma region Pad Left

// Returns a copy of the current text leading with a specified
// number of specified characters before the text.
char* Text::PadLeft(UInt count, char value) const
{
	UInt length = Length();
	UInt newLength = count + length + 1;

	auto output = new char [ newLength ];//anewa(char,newLength);

	output[newLength - 1] = 0;

	memset(output, value, count);
	memcpy(output + count, _value, length);

	return output;
}

// Returns a copy of the current text leading with a specified
// number of specified character arrays before the text.
char* Text::PadLeft(UInt count, const char* value) const
{
	UInt length = Length();
	UInt vLength = LengthOf(value);
	char* output = nullptr;

	if (vLength > 0)
	{
		UInt newLength = (count * vLength) + length + 1;

		output = new char [ newLength ];//anewa(char,newLength);

		output[newLength - 1] = 0;

		for (UInt index = 0; index < count; index++)
		{
			memcpy(output + (index * vLength), value, vLength);
		}

		memcpy(output + (count * vLength), _value, length);
	}
	else
	{
		output = GetCopy();
	}

	return output;
}

// Returns a copy of the current text leading with a specified
// number of specified text before the text.
char* Text::PadLeft(UInt count, const Text& value) const
{
	return PadLeft(count, value._value);
}

// Returns a copy of the current text leading with a specified
// number of space characters before the text.
char* Text::PadLeft(UInt count) const
{
	return PadLeft(count, ' ');
}

#pragma endregion

#pragma region Pad Right

// Returns a copy of the current text ending with a specified
// number of specified characters before the text.
char* Text::PadRight(UInt count, char value) const
{
	UInt length = Length();
	UInt newLength = count + length + 1;

	auto output = new char [ newLength ];//anewa(char,newLength);

	output[newLength - 1] = 0;
	memcpy(output, _value, length);
	memset(output + length, value, count);

	return output;
}

// Returns a copy of the current text ending with a specified
// number of specified character arrays before the text.
char* Text::PadRight(UInt count, const char* value) const
{
	UInt length = Length();
	UInt vLength = LengthOf(value);
	UInt newLength = length + (count * vLength) + 1;

	auto output = new char [ newLength ];// anewa( char, newLength );

	output[newLength - 1] = 0;
	memcpy(output, _value, length);

	for (UInt index = 0; index < count; index++)
	{
		memcpy(output + length + (index * vLength), value, vLength);
	}

	return output;
}

// Returns a copy of the current text ending with a specified
// number of specified text before the text.
char* Text::PadRight(UInt count, const Text& value) const
{
	return PadRight(count, value._value);
}

// Returns a copy of the current text ending with a specified
// number of space characters before the text.
char* Text::PadRight(UInt count) const
{
	return PadRight(count, ' ');
}

#pragma endregion

#pragma region Subtext

char* Text::GetSubText(UInt startPosition, UInt endPosition) const
{
	char* output = nullptr;

	if (endPosition > startPosition)
	{
		UInt length = Length();

		if (endPosition <= length)
		{
			UInt outLength = endPosition - startPosition + 1;

			output = new char [ outLength ];// anewa( char, outLength );
			output[outLength - 1] = 0;

			memcpy(output, _value + startPosition, outLength - 1);
		}
	}

	return output;
}

char* Text::GetSubText(UInt startPosition) const
{
	return GetSubText(startPosition, Length());
}

#pragma endregion

#pragma endregion

#pragma region Operators

#pragma region Plus Operator

char* Text::operator+(const Text& rhs) const
{
	return Combine(_value, rhs._value);
}

char* Text::operator+(const char* rhs) const
{
	return Combine(_value, rhs);
}

char* Text::operator+(char rhs) const
{
	char* output = nullptr;
	char* temp = GetCopy(rhs);
	output = Combine(_value, temp);
	delete[] temp;
	return output;
}

#pragma endregion

#pragma region PlusEquals Operator

Text& Text::operator+=(const Text& rhs)
{
	const char* pVal = _value;

	_value = Combine(pVal, rhs._value);

	if (pVal != nullptr && _ownsValue)
	{
		delete[] pVal;
	}

	_ownsValue = true;

	return *this;
}

Text& Text::operator+=(const char* rhs)
{
	const char* pVal = _value;

	_value = Combine(pVal, rhs);

	if ( pVal != nullptr && _ownsValue )
	{
		delete[ ] pVal;
	}

	_ownsValue = true;

	return *this;
}

Text& Text::operator+=(char rhs)
{
	const char* pVal = _value;
	char* buffer = new char [ 2 ];// anewa( char, 2 );
	buffer[0] = rhs;
	buffer[1] = '\0';
	_value = Combine(pVal, buffer);

	if ( pVal != nullptr && _ownsValue )
	{
		delete[ ] pVal;
	}

	_ownsValue = true;

	if (buffer != nullptr)
	{
		delete[] buffer;
	}

	return *this;
}

#pragma endregion

#pragma region Assignment Operator

Text& Text::operator=(const Text& rhs)
{
	if ( _value != nullptr && _ownsValue )
	{
		delete[ ] _value;
	}

	_value = GetCopy(rhs);
	_ownsValue = true;

	return *this;
}

Text& Text::operator=(const char* rhs)
{
	if ( _value != nullptr && _ownsValue )
	{
		delete[] _value;
	}

	_value = rhs;
	_ownsValue = false;

	return *this;
}

Text& Text::operator=(char rhs)
{
	if ( _value != nullptr && _ownsValue )
	{
		delete[] _value;
	}

	_value = GetCopy(rhs);
	_ownsValue = true;

	return *this;
}

#pragma endregion

#pragma region Comparison Operators

#pragma region Equals

bool Text::operator==(const Text& rhs) const
{
	return AreEqual(_value, rhs._value);
}

bool Text::operator==(const char* rhs) const
{
	return AreEqual(_value, rhs);
}

bool Text::operator==(char rhs) const
{
	return Length() == 1 && _value[0] != rhs;
}

#pragma endregion

#pragma region Not Equals

bool Text::operator!=(const Text& rhs) const
{
	return !AreEqual(_value, rhs._value);
}

bool Text::operator!=(const char* rhs) const
{
	return !AreEqual(_value, rhs);
}

bool Text::operator!=(char rhs) const
{
	return Length() == 1 && _value[0] == rhs;
}

#pragma endregion

#pragma region Greater Than

bool Text::operator>(const Text& rhs) const
{
	return (CompareTo(rhs) == 1);
}

bool Text::operator>(const char* rhs) const
{
	return (CompareTo(rhs) == 1);
}

bool Text::operator>(char rhs) const
{
	return Length() == 1 && _value[0] > rhs;
}

#pragma endregion

#pragma region Less Than

bool Text::operator<(const Text& rhs) const
{
	return (Compare(_value, rhs._value) == -1);
}

bool Text::operator<(const char* rhs) const
{
	return (CompareTo(rhs) == -1);
}

bool Text::operator<(char rhs) const
{
	return Length() == 1 && _value[0] < rhs;
}

#pragma endregion

#pragma region Greater Than or Equal To

bool Text::operator>=(const Text& rhs) const
{
	return (CompareTo(rhs) != -1);
}

bool Text::operator>=(const char* rhs) const
{
	return (CompareTo(rhs) != -1);
}

bool Text::operator>=(char rhs) const
{
	return Length() == 1 && _value[0] >= rhs;
}

#pragma endregion

#pragma region Less Than or Equal To

bool Text::operator<=(const Text& rhs) const
{
	return (CompareTo(rhs) != 1);
}

bool Text::operator<=(const char* rhs) const
{
	return (CompareTo(rhs) != 1);
}

bool Text::operator<=(char rhs) const
{
	return Length() == 1 && _value[0] <= rhs;
}

#pragma endregion

#pragma endregion

#pragma endregion

#pragma region Functions

#pragma region Length

const char* Text::Empty()
{
	return "";
}

// Returns the length of the given character array, not including
// the null terminator.
UInt Text::LengthOf(const char* value)
{
	return strlen(value);
}

// Returns the length of the given text, not including
// the null terminator.
UInt Text::LengthOf(const Text& value)
{
	return LengthOf(value._value);
}

#pragma endregion

#pragma region Conversion

#pragma region ToInt

// Returns the integer representation of the current text.
// If the current text does not convert, the result is 0.
int Text::ToInt() const
{
	return ToInt(_value);
}

// Returns the integer representation of the given character array.
// If the current text does not convert, the result is 0.
int Text::ToInt(const char* value)
{
	return atoi(value);
}

// Returns the integer representation of the given text.
// If the current text does not convert, the result is 0.
int Text::ToInt(const Text& value)
{
	return value.ToInt();
}

#pragma endregion

#pragma region ToDouble

// Returns the double-precision representation of the current text.
// If the current text does not convert, the result is 0.
double Text::ToDouble() const
{
	return ToDouble(_value);
}

// Returns the double-precision representation of the given character array.
// If the current text does not convert, the result is 0.
double Text::ToDouble(const char* value)
{
	return atof(value);
}

// Returns the double-precision representation of the given text.
// If the current text does not convert, the result is 0.
double Text::ToDouble(const Text& value)
{
	return value.ToDouble();
}

#pragma endregion

#pragma region ToLong

// Returns the long integer representation of the current text.
// If the current text does not convert, the result is 0.
long Text::ToLong() const
{
	return ToLong(_value);
}

// Returns the long integer representation of the given character array.
// If the current text does not convert, the result is 0.
long Text::ToLong(const char* value)
{
	return atol(value);
}

// Returns the long integer representation of the given text.
// If the current text does not convert, the result is 0.
long Text::ToLong(const Text& value)
{
	return value.ToLong();
}

#pragma endregion

#pragma region ToLongLong

// Returns the LongLong integer representation of the current text.
// If the current text does not convert, the result is 0.
LongLong Text::ToLongLong() const
{
	return ToLongLong(_value);
}

// Returns the LongLong integer representation of the given character array.
// If the current text does not convert, the result is 0.
LongLong Text::ToLongLong(const char* value)
{
	return _atoi64(value);
}

// Returns the LongLong integer representation of the given text.
// If the current text does not convert, the result is 0.
LongLong Text::ToLongLong(const Text& value)
{
	return value.ToLongLong();
}

#pragma endregion

#pragma region ToBoolean

// returns the bool representation of the given char array
// if the text does not convert, returns false
bool Text::ToBoolean( ) const
{
	return ToBoolean( _value );
}

// returns the bool representation of the given char array
// if the text does not convert, returns false
bool Text::ToBoolean( const char* value )
{
	bool output = false;
	
	if ( std::strcmp( value, "true" ) == 0 )
	{
		output = true;
	}
	else if ( std::strcmp( value, "True" ) == 0 )
	{
		output = true;
	}
	else if ( std::strcmp( value, "TRUE" ) == 0 )
	{
		output = true;
	}
	else if ( std::strcmp( value, "1" ) == 0 )
	{
		output = true;
	}

	return output;
}

// returns the bool representation of the given char array
// if the text does not convert, returns false
bool Text::ToBoolean( const Text& other )
{
	return other.ToBoolean( );
}

#pragma endregion

#pragma region FromInt

// Returns a character array representation of the given
// integer with 12 character precision.
char* Text::FromInt(int rhs)
{
	char* buffer = new char [ 12 ];// anewa( char, 12 );
	memset(buffer, 0, 12);
	_itoa_s(rhs, buffer, 12, 10);

	return buffer;
}

// Returns a character array representation of the given
// UInteger with 11 character precision.
char* Text::FromUInt(UInt rhs)
{
	return FromULong((ULong)rhs);
}

#pragma endregion

#pragma region FromLong

// Returns a character array representation of the given
// long integer with 12 character precision.
char* Text::FromLong(long rhs)
{
	char* buffer = new char [ 12 ];// anewa( char, 12 );
	memset(buffer, 0, 12);
	_ltoa_s(rhs, buffer, 12, 10);

	return buffer;
}

// Returns a character array representation of the given
// ULong integer with 11 character precision.
char* Text::FromULong(ULong rhs)
{
	char* buffer = new char [ 11 ];// anewa( char, 11 );
	memset(buffer, 0, 11);
	_ultoa_s(rhs, buffer, 11, 10);

	return buffer;
}

#pragma endregion

#pragma region FromLongLong

// Returns a character array representation of the given
// LongLong integer with 21 character precision.
char* Text::FromLongLong(LongLong rhs)
{
	char* buffer = new char [ 21 ];// anewa( char, 21 );
	memset(buffer, 0, 21);
	_i64toa_s(rhs, buffer, 21, 10);

	return buffer;
}

// Returns a character array representation of the given
// ULongLong integer with 21 character precision.
char* Text::FromULongLong(ULongLong rhs)
{
	char* buffer = new char [ 21 ];// anewa( char, 21 );
	memset(buffer, 0, 21);
	_ui64toa_s(rhs, buffer, 21, 10);

	return buffer;
}

#pragma endregion

#pragma endregion

#pragma region Combine

// Returns a new character array that represents
// a concatenated with b.
char* Text::Combine(const char* a, const char* b)
{
	UInt size = LengthOf(a) + LengthOf(b) + 1;

	char* buffer = new char [ size ];// anewa( char, size );
	memset(buffer, 0, size);

	strcat_s(buffer, size, a);
	strcat_s(buffer, size, b);

	return buffer;
}

// Returns a new character array that represents
// a concatenated with b.
char* Text::Combine(const Text& lhs, const Text& rhs)
{
	UInt size = LengthOf(lhs) + LengthOf(rhs) + 1;

	char* buffer = new char[ size ];// anewa( char, size );
	memset(buffer, 0, size);

	strcat_s(buffer, size, lhs._value);
	strcat_s(buffer, size, rhs._value);

	return buffer;
}

#pragma endregion

#pragma region Get Copy

// Returns a copy of the current text.
char* Text::GetCopy() const
{
	return GetCopy(_value);
}

// Returns a copy of the given character array.
char* Text::GetCopy(const char* rhs)
{
	UInt size = LengthOf(rhs) + 1;
	char* buffer = new char[ size ];// anewa( char, size );
	memset(buffer, 0, size);
	strcpy_s(buffer, size, rhs);
	return buffer;
}

// Returns a copy of the given text.
char* Text::GetCopy(const Text& rhs)
{
	return GetCopy(rhs._value);
}

// Returns a copy of the given character as a character array.
char* Text::GetCopy(char rhs)
{
	char* output = new char[ 2 ];// anewa( char, 2 );
	output[0] = rhs;
	output[1] = 0;
	return output;
}

#pragma endregion

#pragma region Compare

#pragma region AreEqual

// Compares the two character arrays for equality.
bool Text::AreEqual(const char* a, const char* b)
{
	if (a == nullptr || b == nullptr)
	{
		return a == b;
	}

	return (strcmp(a, b) == 0) ? true : false;
}

// Compares the two texts for equality.
bool Text::AreEqual(const Text& a, const Text& b)
{
	return AreEqual(a._value, b._value);
}

#pragma endregion

#pragma region Compare

// Compares the current text to the given text. A result of 0 indicates
// equality, while all non-zero values indicate inequality.
int Text::CompareTo(const Text& rhs) const
{
	return strcmp(_value, rhs._value);
}

// Compares the two given texts. A result of 0 indicates
// equality, while all non-zero values indicate inequality.
int Text::Compare(const Text& lhs, const Text& rhs)
{
	return lhs.CompareTo(rhs);
}

// Compares the current text to the given character array. A result of 0 indicates
// equality, while all non-zero values indicate inequality.
int Text::CompareTo(const char* rhs) const
{
	if (_value == nullptr || rhs == nullptr)
	{
		return _value == rhs;
	}

	return strcmp(_value, rhs);
}

// Compares the two given character arrays. A result of 0 indicates
// equality, while all non-zero values indicate inequality.
int Text::Compare(const char* lhs, const char* rhs)
{
	if (lhs == nullptr || rhs == nullptr)
	{
		return lhs == rhs;
	}

	return strcmp(lhs, rhs);
}

#pragma endregion

#pragma endregion

#pragma region ToLower and ToUpper

// Returns a copy of the current text with all characters in lower-casing.
char* Text::ToLower() const
{
	return ToLower(_value);
}

// Returns a copy of the given character array with all characters in lower-casing.
char* Text::ToLower(const char* rhs)
{
	auto output = GetCopy(rhs);
	_strlwr_s(output, LengthOf(rhs) + 1);
	return output;
}

// Returns a copy of the given text with all characters in lower-casing.
char* Text::ToLower(const Text& rhs)
{
	return ToLower(rhs._value);
}

// Returns a copy of the current text with all characters in upper-casing.
char* Text::ToUpper() const
{
	return ToUpper(_value);
}

// Returns a copy of the given character array with all characters in upper-casing.
char* Text::ToUpper(const char* rhs)
{
	auto output = GetCopy(rhs);
	_strupr_s(output, LengthOf(rhs) + 1);
	return output;
}

// Returns a copy of the given text with all characters in upper-casing.
char* Text::ToUpper(const Text& rhs)
{
	return ToUpper(rhs._value);
}

#pragma endregion

#pragma region Split

// Splits the given character array based on the given token. The number of
// tokens created by the split is assigned to tokenCount.
Text* Text::Split(const char* value, char token, UInt* tokenCount)
{
	UInt length = LengthOf(value);

	if (length > 0)
	{
		auto val = GetCopy(value);
		*tokenCount = 1;

		for (UInt index = 0; index < length; index++)
		{
			if (val[index] == token)
			{
				(*tokenCount)++;
			}
		}

		if (*tokenCount > 1)
		{
			Text* retval = new Text [ *tokenCount ];// anewa( Text, *tokenCount );

#pragma warning( disable:4996 )

			retval[0] = strtok(val, &token);

			for (UInt index = 1; index < *tokenCount; index++)
			{
				char* piece = strtok(nullptr, &token);

				if (piece != nullptr)
				{
					retval[index] = piece;
				}
				else
				{
					(*tokenCount)--;
				}
			}

			//delete[] val;

			return retval;
		}
		else
		{
			Text* t = new Text( val ); anew( Text )( val );

			//delete[] val;

			return t;
		}
	}

	return nullptr;
}

Text* Text::Split(const Text& value, char token, UInt* tokenCount)
{
	return Split(value._value, token, tokenCount);
}

#pragma endregion

#pragma endregion


template <typename Type>
size_t HashConfiguration<Type>::operator()(const Type& key_value) const
{
	return key_value.GetHashValue();
}

template <typename Type>
bool HashConfiguration<Type>::operator()(const Type& lhs, const Type& rhs) const
{
	return lhs < rhs;
}