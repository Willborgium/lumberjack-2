#include "..\Headers\Core.h"
#include "..\Headers\Collections.h"

using namespace HyJynxCore;
using namespace HyJynxCollections;

IConverter* Converter::GetConverter( Text fromType, Text toType, unsigned int* fromSize, unsigned int* toSize )
{
	IConverter* converter = nullptr;

	_converters.ForEach( [&]( KeyValuePair<ConverterKey, IConverter*> element )
	{
		if( element.Key.FromType.Contains(fromType) && element.Key.ToType.Contains(toType) )
		{
			converter = element.Value;
			if( fromSize != nullptr )
			{
				*fromSize = element.Key.FromTypeSize;
			}

			if( toSize != nullptr )
			{
				*toSize = element.Key.ToTypeSize;
			}
		}
	} );

	return converter;
}

void Converter::AddConverter( Text fromType, Text toType, IConverter* converter, unsigned int fromSize, unsigned int toSize )
{
	if( GetConverter( fromType, toType, nullptr, nullptr  ) == nullptr )
	{
		_converters.Append( ConverterKey( fromType, toType, fromSize, toSize ), converter );
	}
}

void* Converter::Convert(Text& fromType, Text& toType, void* data, unsigned int* outputSize)
{
	IConverter* converter = GetConverter(fromType, toType, nullptr, outputSize);

	if (converter != nullptr)
	{
		return converter->Convert(data);
	}

	return nullptr;
}

bool Converter::ConverterExists(Text& fromType, Text& toType)
{
	return GetConverter(fromType, toType, nullptr, nullptr) != nullptr;
}

Converter::ConverterKey::ConverterKey( ) { }

Converter::ConverterKey::ConverterKey( Text from, Text to, unsigned int fromSize, unsigned int toSize )
	: FromType( from ), ToType( to ), FromTypeSize( fromSize ), ToTypeSize( toSize )
{
}

bool Converter::ConverterKey::operator==( Converter::ConverterKey& rhs )
{
	return ( this->FromType == rhs.FromType &&
		this->ToType == rhs.ToType );
}

bool Converter::ConverterKey::operator!=( Converter::ConverterKey& rhs )
{
	return !( this->FromType == rhs.FromType &&
		this->ToType == rhs.ToType );
}