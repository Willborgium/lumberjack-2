#include "..\Headers\Core.h"
using namespace HyJynxCore;

bool ResourceLoader::LoadFile(Text fileName)
{
	bool result = false;

	JsonData* jsonData = JsonParser::ReadFile(fileName);

	if (jsonData != nullptr)
	{
		if (jsonData->DataType == JsonData::JsonDataType::Object)
		{
			if (jsonData->Data->ObjectData->ContainsKey("TargetType"))
			{
				JsonData* targetTypeData = jsonData->Data->ObjectData->ValueOf("TargetType");
				JsonData* nameData = jsonData->Data->ObjectData->ValueOf("Name");
					
				if (targetTypeData->DataType == JsonData::JsonDataType::Value &&
					nameData->DataType == JsonData::JsonDataType::Value)
				{
					Text* targetType = targetTypeData->Data->ValueData;
					Text* name = nameData->Data->ValueData;
					
					if (_converter->ConverterExists(Text("JsonData"), *targetType))
					{
						void* data = nullptr;
						UInt dataSize = 0;

						data = _converter->Convert(Text("JsonData"), *targetType, jsonData, &dataSize);

						if (dataSize > 0 && data != nullptr)
						{
							UInt group = 0;
							if (jsonData->Data->ObjectData->ContainsKey("Group"))
							{
								JsonData* groupData = jsonData->Data->ObjectData->ValueOf("Group");
								if (groupData->DataType == JsonData::JsonDataType::Value)
								{
									group = groupData->Data->ValueData->ToInt();
								}
							}
							result = _resources->Add(data, *name, group);
						}
					}
					else
					{
						throw new Exception(Text("No suitable converter found from 'JsonData' to '{0}'.").Replace(Text("{0}"), *targetType));
					}
				}
				else
				{
					throw new Exception("Property 'TargetType' must be a primitive value.");
				}
			}
			else
			{
				throw new Exception("JSON object does not define the 'TargetType' property.");
			}
		}
		else
		{
			throw new Exception("JSON value must be an object to be parsed at this level.");
		}
	}

	return result;
}