#include "..\Headers\Core.h"
using namespace HyJynxCore;

Exception::Exception( Text message )
	: _message( message ), _level( ExceptionLevel::Fatal ), _timestamp( Time::Now( ) )
{
}

// Initializes a new exception with the given message and level.
// - message:	The message associated with the exception.
// - level:		The level of the exception. [Use the ExceptionLevel enumeration]
Exception::Exception( Text message, USmall level )
	: _message( message ), _level( level ), _timestamp( Time::Now( ) )
{
}

// Returns the message associated with this exception.
Text Exception::GetMessageText( ) const
{
	return _message;
}

// Returns the timestamp associated with this exception.
Time Exception::GetTimeStamp( ) const
{
	return _timestamp;
}

// Returns the level associated with this exception.
USmall Exception::GetLevel( ) const
{
	return _level;
}