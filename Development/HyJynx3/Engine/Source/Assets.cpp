#include "..\Headers\Core.h"
using namespace HyJynxCore;

Asset Assets::GetConfigjson( )
{
	return Asset( "Assets\\Config.enc.json", 589 );
}

Asset Assets::Getrockjpg( )
{
	return Asset( "Assets\\rock.enc.jpg", 134943 );
}

Asset Assets::GetTestItOuttxt( )
{
	return Asset( "Assets\\TestItOut.enc.txt", 9 );
}

Asset Assets::Getmdl_mushroom_asciiFBX( )
{
	return Asset( "Assets\\Models\\mdl_mushroom_ascii.enc.FBX", 6738185 );
}

Asset Assets::Getmdl_mushroom_binaryFBX( )
{
	return Asset( "Assets\\Models\\mdl_mushroom_binary.enc.FBX", 740944 );
}

Asset Assets::Getbricks_diffusebmp( )
{
	return Asset( "Assets\\Textures\\bricks_diffuse.enc.bmp", 786486 );
}

Asset Assets::Getsubrockjpg( )
{
	return Asset( "Assets\\Textures\\subrock.enc.jpg", 134943 );
}