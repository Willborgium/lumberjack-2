#include "..\Headers\Core.h"
using namespace HyJynxCore;

DispatchingSystem::DispatchingSystem( )
: ISystem( "Dispatcher" )
{
}

bool DispatchingSystem::Initialize( )
{
	return true;
}

void DispatchingSystem::Update( )
{
	_systems.ForEach( [ &] ( ISystem* system )
	{
		if ( system->Is( SystemState::Uninitialized ) )
		{
			if ( system->Initialize( ) )
			{
				system->SetState( SystemState::Initialized );
				system->Begin( );
			}
		}

		if ( system->Is( SystemState::Running ) )
		{
			system->Update( );
		}
	} );
}

bool DispatchingSystem::Uninitialize( )
{
	return true;
}

void DispatchingSystem::AppendSystem( ISystem* system )
{
	_systems.Append( system );
}

void DispatchingSystem::InsertSystem( UInt index, ISystem* system )
{
	_systems.InsertAt( index, system );
}

ISystem* DispatchingSystem::RemoveSystem( UInt index )
{
	ISystem* system = _systems.ElementAt( index );
	_systems.RemoveAt( index );
	return system;
}

ISystem* DispatchingSystem::RemoveSystem( Text name )
{
	ISystem* system = GetSystem( name );

	if ( system != nullptr )
	{
		_systems.Remove( system );
	}

	return system;
}

void DispatchingSystem::MoveSystem( UInt source, UInt destination )
{
	ISystem* system = RemoveSystem( source );
	_systems.InsertAt( destination, system );
}

void DispatchingSystem::MoveSystem( Text name, UInt destination )
{
	ISystem* system = RemoveSystem( name );
	if ( system != nullptr )
	{
		_systems.InsertAt( destination, system );
	}
}

ISystem* DispatchingSystem::GetSystem( Text name )
{
	ISystem* system = nullptr;

	_systems.ForEach( [&] ( ISystem* s )
	{
		if ( system == nullptr && s->GetName( ) == name )
		{
			system = s;
		}
	} );

	return system;
}

ISystem* DispatchingSystem::GetSystem( UInt index )
{
	return _systems.ElementAt( index );
}

UInt DispatchingSystem::IndexOf( Text name )
{
	return _systems.IndexOf( GetSystem( name ) );
}

bool DispatchingSystem::SuspendSystem( Text name )
{
	bool result = false;

	auto system = GetSystem( name );

	if ( system != nullptr )
	{
		if ( system->Suspend( ) )
		{
			system->SetState( SystemState::Suspended );
			result = true;
		}
	}

	return result;
}

bool DispatchingSystem::ResumeSystem( Text name )
{
	bool result = false;

	auto system = GetSystem( name );

	if ( system != nullptr )
	{
		if ( system->Resume( ) )
		{
			system->SetState( SystemState::Running );
			result = true;
		}
	}

	return result;
}