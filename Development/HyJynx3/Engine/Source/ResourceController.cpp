
#include "..\Headers\Core.h"
#include "..\Headers\Helper.h"

using namespace HyJynxCore;
using namespace HyJynxUtilities;

bool ResourceController::ResourceKey::operator==(const ResourceKey& rhs)
{
	return Name == rhs.Name && Group == rhs.Group;
}

bool ResourceController::ResourceKey::operator!=(const ResourceKey& rhs)
{
	return !operator==(rhs);
}

ResourceController::ResourceKey::ResourceKey()
: Group(0)
{
}

ResourceController::ResourceKey::ResourceKey(Text name)
: Name(name), Group(0)
{
}

ResourceController::ResourceKey::ResourceKey(Text name, UInt group)
: Name(name), Group(group)
{
}

ResourceController::ResourceController()
{
}

void* ResourceController::Remove(Text name, UInt group)
{
	void* output = nullptr;
	ResourceKey key(name, group);

	if (_resources.ContainsKey(key))
	{
		output = _resources[key];
		_resources.Remove(key);
	}

	return output;
}

// modify the file path to the build directory's content output
Text ResourceController::GetPath( _In_ const Text& path )
{
	Text output;

#ifdef _WIN32
	output = "..\\..\\bin\\Win32\\";
#else
	output = "..\\..\\bin\\x64\\";
#endif

#ifdef _DEBUG
	output += "Debug\\";
#else
	output += "Release\\";
#endif

	output += path;

	output = Helper::ExpandDirectory( output );

	return output.RemoveFromEnd( 1 );
}