#include "..\Headers\Core.h"

using namespace HyJynxCore;

void Mutex::Lock ( )
{
	if ( !_isCurrentLocked )
	{
		while ( *_handle );
		*_handle = true;
		_isCurrentLocked = true;
	}
}

bool Mutex::TryLock ( )
{
	if ( !( *_handle ) && !_isCurrentLocked )
	{
		*_handle = true;
		_isCurrentLocked = true;
		return true;
	}

	return false;
}

void Mutex::Unlock ( )
{
	if ( _isCurrentLocked )
	{
		if ( _handle != nullptr )
		{
			if ( *_handle )
			{
				*_handle = false;
				_isCurrentLocked = false;
			}
		}
	}
}

Mutex::~Mutex ( ) throw ( )
{
	Unlock ( );
}