#include "..\Headers\Utilities.h"
#include "..\Headers\Collections.h"
#include "..\Headers\Core.h"

using namespace HyJynxUtilities;
using namespace HyJynxCollections;
using namespace HyJynxCore;

HyJynxUtilities::TestRunner::TestResults::TestResults()
{
	Passed = Failed = TestsRun = 0;
}

bool HyJynxUtilities::TestRunner::RegisterTest(Text name, std::function<bool(void)> test)
{
	bool output = false;

	if (!_tests.ContainsKey(name))
	{
		output = true;
		_tests.Append(name, test);
	}

	return output;
}

HyJynxUtilities::TestRunner::TestResults HyJynxUtilities::TestRunner::ExecuteTests(std::function<void(Text)> callback)
{
	Text executing = "Executing {0}...";

	TestResults results;

	_tests.ForEach([&](KeyValuePair<Text, std::function<bool(void)>> testKVP)
	{
		callback(executing.Replace(Text("{0}"), testKVP.Key));
		bool passed = testKVP.Value();
		callback(testKVP.Key + (passed ? " Passed" : " Failed"));
		results.Passed += passed ? 1 : 0;
		results.Failed += passed ? 0 : 1;
		results.TestsRun++;
	});

	Text output = "Passed: ";
	output += Text(results.Passed);
	output += "\nFailed: ";
	output += Text(results.Failed);
	output += "\nRun   : ";
	output += Text(results.TestsRun);

	callback(output);

	return results;
}