#include "..\Headers\Utilities.h"
#include "..\Headers\MemoryManagement.h"
#include "..\Headers\Core.h"

using namespace HyJynxUtilities;
using namespace HyJynxMemoryManagement;
using namespace HyJynxCore;
using namespace HyJynxCollections;

#include <Windows.h>

const char* Helper::WindowClassName = "HyJynx Default Window Class";

HWND Helper::CreateDefaultWindow(WNDPROC winproc, HINSTANCE instance, Text caption, int width, int height)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = winproc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = instance;
	wcex.hIcon = LoadIcon(instance, MAKEINTRESOURCE(IDI_APPLICATION));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = nullptr;
	wcex.lpszClassName = WindowClassName;
	wcex.hIconSm = LoadIcon(instance, MAKEINTRESOURCE(IDI_APPLICATION));

	if (!RegisterClassEx(&wcex))
	{
		return nullptr;
	}

	return CreateWindow(WindowClassName, caption.GetValue(),
		WS_OVERLAPPED | WS_BORDER | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX, CW_USEDEFAULT, CW_USEDEFAULT,
		width, height, nullptr, nullptr, instance, nullptr);
}

bool Helper::IsFirstInstance(GlobalMemoryPool& pool, bool markFirstInstance)
{
	const char* readValue = pool.Read();

	int* ptr = 0;
	memcpy(&ptr, readValue, 4);

	if (ptr == (int*)0xdeadbeef)
	{
		ExceptionHandler::RaiseException(new Exception("This application is already running."));
		pool.Close();

		return false;
	}
	else
	{
		if (markFirstInstance)
		{
			UInt w = 0xdeadbeef;
			char flag[4];
			memcpy(flag, &w, 4);

			pool.Write(flag, 4);
		}
	}

	return true;
}

void Helper::ShowMessage(Text message)
{
	MessageBox(nullptr, message.GetValue(), "Exception Raised...", MB_OK);
}

Text Helper::ExpandDirectory(Text directory)
{
	Text search = Text::Empty();

	if (directory.StartsWith('\\') || directory.StartsWith("..") || directory == Text::Empty())
	{
		char* buf = new char[255];
		GetCurrentDirectory(255, buf);

		UInt currentTokens = 0;
		Text* currentParts = Text::Split(buf, '\\', &currentTokens);

		UInt inputTokens = 0;
		Text* inputParts = directory.Split('\\', &inputTokens);

		UInt backupCount = directory.CountOf("..");
		currentTokens -= (backupCount > 0 ? backupCount - 1 : backupCount);

		for (UInt baseIndex = 0; baseIndex < currentTokens; baseIndex++)
		{
			search += currentParts[baseIndex] + '\\';
		}

		for (UInt pathIndex = backupCount; pathIndex < inputTokens; pathIndex++)
		{
			search += inputParts[pathIndex] + '\\';
		}
	}
	else
	{
		search = directory;
	}

	return search;
}

DynamicCollection<Text> Helper::FindFiles(Text directory, Text filter)
{
	DynamicCollection<Text> output;

	Text search = ExpandDirectory(directory) + filter;

	WIN32_FIND_DATA findData;
	HANDLE findHandle = INVALID_HANDLE_VALUE;

	findHandle = FindFirstFile(search, &findData);
	
	if (findHandle != INVALID_HANDLE_VALUE)
	{
		do
		{
			if (findData.dwFileAttributes & FILE_ATTRIBUTE_ARCHIVE)
			{
				output.Append(search + findData.cFileName);
			}
		}
		while (FindNextFile(findHandle, &findData) != 0);
	}

	return output;
}