#include "..\Headers\Design.h"
using namespace HyJynxDesign;

FuncTable* FuncTable::_instance = nullptr;

FuncTable::FuncTable( ) { }

FuncTable* FuncTable::Instance( )
{
	if ( _instance == nullptr )
	{
		_instance = new FuncTable( );
	}

	return _instance;
}

void FuncTable::Register( void* addr, void* func )
{
	FuncPair* ptr = _ptrs;

	while ( ptr != nullptr && ptr->Next != nullptr )
	{
		ptr = ptr->Next;
	}

	if ( ptr == nullptr )
	{
		_ptrs = new FuncPair( addr, func );
	}
}