#include "..\Headers\Core.h"
using namespace HyJynxCore;

JsonConfiguration::JsonConfiguration( HyJynxCore::Text path )
: ConfigurationPath( path )
{
}

TextJsonConfiguration::TextJsonConfiguration( HyJynxCore::Text path, HyJynxCore::Text* target )
: Value( target ), JsonConfiguration( path )
{
}
void TextJsonConfiguration::RetrieveValue( )
{
	*Value = ConfigurationManager::Instance( )->GetTextValue( ConfigurationPath );
}
IntJsonConfiguration::IntJsonConfiguration( HyJynxCore::Text path, int* target )
: Value( target ), JsonConfiguration( path )
{
}

void IntJsonConfiguration::RetrieveValue( )
{
	*Value = ConfigurationManager::Instance( )->GetIntValue( ConfigurationPath );
}
DoubleJsonConfiguration::DoubleJsonConfiguration( HyJynxCore::Text path, double* target )
: Value( target ), JsonConfiguration( path )
{
}

void DoubleJsonConfiguration::RetrieveValue( )
{
	*Value = ConfigurationManager::Instance( )->GetDoubleValue( ConfigurationPath );
}

BoolJsonConfiguration::BoolJsonConfiguration( HyJynxCore::Text path, bool* target )
: Value( target ), JsonConfiguration( path )
{
}

void BoolJsonConfiguration::RetrieveValue( )
{
	*Value = ConfigurationManager::Instance( )->GetTextValue( ConfigurationPath ).ToBoolean( );
}

void JsonConfigurationObject::AddMapping( JsonConfiguration* mapping )
{
	_mappings.Append( mapping );
}

void JsonConfigurationObject::RemoveMapping( HyJynxCore::Text path )
{
	_mappings.RemoveWhere( [&path] ( JsonConfiguration* value )
	{
		return value->ConfigurationPath == path;
	} );
}

void JsonConfigurationObject::Refresh( )
{
	_mappings.ForEach( [] ( JsonConfiguration* value )
	{
		value->RetrieveValue( );
	} );
}