#include "..\Headers\Core.h"
using namespace HyJynxCore;

const double TimeLength::OneMicrosecond = 1.0;
const double TimeLength::OneMillisecond = 1000.0;
const double TimeLength::OneSecond = 1000000.0;

HighPrecisionTimer::HighPrecisionTimer()
: _frequency(0), _startTime(0), _interval(0),
_isRunning(false), _intervalCount(0), _loopInfinitely(true),
_currentInterval(0), _useCallback(false), _nextTick(0)
{
	TimingSystem::Instance()->RegisterTimer(this);
}

void HighPrecisionTimer::SetCallback(CallbackMethod callback)
{
	_callback = callback;
}

void HighPrecisionTimer::SetUseCallback(bool useCallback)
{
	_useCallback = useCallback;
}

void HighPrecisionTimer::SetInterval(TimeSpan interval)
{
	_interval = LongLong(interval.TotalSeconds());
}

void HighPrecisionTimer::SetIntervalCount(UInt count)
{
	_intervalCount = count;
}

void HighPrecisionTimer::SetInfiniteLooping(bool infiniteLoop)
{
	_loopInfinitely = infiniteLoop;
}

void HighPrecisionTimer::Start()
{
	LARGE_INTEGER li;
	QueryPerformanceFrequency(&li);

	_frequency = double(li.QuadPart) / 1000.0;

	QueryPerformanceCounter(&li);

	_startTime = li.QuadPart;

	_isRunning = true;
	_currentInterval = 0;
	_nextTick = _startTime + _interval;
}

void HighPrecisionTimer::Stop()
{
	_startTime = _interval = _isRunning = ((_frequency = 0.0) == 0);
}

TimeSpan HighPrecisionTimer::GetElapsed()
{
	if (_isRunning)
	{
		LARGE_INTEGER li;
		QueryPerformanceCounter(&li);
		return TimeSpan((double(li.QuadPart - _startTime) * 1000.0 / _frequency) / TimeLength::OneSecond);
	}
	else
	{
		return 0;
	}
}

LongLong HighPrecisionTimer::GetElapsedTicks()
{
	if (_isRunning)
	{
		LARGE_INTEGER li;
		QueryPerformanceCounter(&li);
		return LongLong(double(li.QuadPart - _startTime) * 1000.0 / _frequency);
	}
	else
	{
		return 0;
	}
}

void  HighPrecisionTimer::Update()
{
	if (_isRunning)
	{
		if ((_loopInfinitely || _currentInterval < _intervalCount))
		{
			LongLong elapsed = GetElapsedTicks();
			if (!_interval || elapsed >= _nextTick - _startTime)
			{
				_nextTick = elapsed + _startTime + _interval;
				_currentInterval++;
				if (_useCallback)
				{
					_callback();
				}
			}
		}
		else
		{
			_isRunning = false;
		}
	}
}

bool HighPrecisionTimer::IsRunning() const
{
	return _isRunning;
}