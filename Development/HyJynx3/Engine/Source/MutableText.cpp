#include "..\Headers\Core.h"

using namespace HyJynxCore;

#include <stdlib.h>
#include <cstring>

void MutableText::initialize( const char* value = nullptr, unsigned int bufferSize = 0 )
{
	if ( _buffer != nullptr )
	{
		delete[ ]_buffer;
	}

	_textLength = value == nullptr ? 0 : strlen( value );

	if ( bufferSize < _growthRate )
	{
		bufferSize = _growthRate;
	}

	if ( bufferSize < _textLength + 1 )
	{
		bufferSize = _textLength + 1;
	}

	_bufferSize = bufferSize;
	_buffer = ( char* ) memset( new char[ _bufferSize ], 0, _bufferSize );
	if ( value != nullptr )
	{
		memcpy( _buffer, value, _textLength );
	}
}

bool MutableText::matches( unsigned int startIndex, const char* value, unsigned int length )
{
	bool result = true;

	for ( unsigned int index = 0; index < length; index++ )
	{
		if ( _buffer[ index + startIndex ] != value[ index ] )
		{
			result = false;
			break;
		}
	}

	return result;
}

unsigned int MutableText::grow( unsigned int requestedLength )
{
	unsigned int newBufferSize = _bufferSize;

	while ( requestedLength > newBufferSize )
	{
		newBufferSize += _growthRate;
	}

	if ( newBufferSize != _bufferSize )
	{
		char* temp = ( char* ) memset( new char[ newBufferSize ], 0, newBufferSize );
		memcpy( temp, _buffer, _bufferSize );
		delete[ ] _buffer;
		_bufferSize = newBufferSize;
		_buffer = temp;
	}

	return newBufferSize;
}

void MutableText::terminate( )
{
	memset( _buffer + _textLength, 0, _bufferSize - _textLength );
}

void MutableText::clear( )
{
	delete[ ] _buffer;
	_buffer = ( char* ) memset( new char[ _growthRate ], 0, _growthRate );
	_textLength = 0;
	_bufferSize = _growthRate;
}

MutableText::MutableText( )
{
	initialize( );
}

MutableText::MutableText( unsigned int bufferSize )
{
	initialize( nullptr, bufferSize );
}

MutableText::MutableText( const char* initialValue )
{
	initialize( initialValue );
}

MutableText::MutableText( const char* initialValue, unsigned int bufferSize )
{
	initialize( initialValue, bufferSize );
}

MutableText::MutableText( const Text& rhs )
{
	initialize( rhs._value );
}

MutableText::MutableText( const Text& initialValue, unsigned int bufferSize )
{
	initialize( initialValue._value, bufferSize );
}

MutableText::MutableText( const MutableText& rhs )
	: _bufferSize( rhs._bufferSize ), _textLength( rhs._textLength ), _buffer( nullptr )
{
	if ( _bufferSize > 0 )
	{
		_buffer = new char[ _bufferSize ];
		memcpy( _buffer, rhs._buffer, _bufferSize );
	}
}

void MutableText::Append( const char value )
{
	unsigned int newTextLength = _textLength + 1;

	unsigned int newBufferSize = grow( newTextLength );

	memset( _buffer + _textLength, value, 1 );
	_textLength = newTextLength;
	_bufferSize = newBufferSize;
}

void MutableText::Append( const char* value )
{
	unsigned int length = strlen( value );

	unsigned int newTextLength = _textLength + length;

	unsigned int newBufferSize = grow( newTextLength );

	memcpy( _buffer + _textLength, value, length );
	_textLength = newTextLength;
	_bufferSize = newBufferSize;
}

void MutableText::Append( const Text& value )
{
	Append( value._value );
}

MutableText& MutableText::operator+=( const char value )
{
	Append( value );
	return *this;
}

MutableText& MutableText::operator+=( const char* value )
{
	Append( value );
	return *this;
}

MutableText& MutableText::operator += ( const Text& value )
{
	Append( value );
	return *this;
}

void MutableText::Replace( const char oldValue, const char newValue )
{
	if ( _buffer != nullptr )
	{
		for ( unsigned int index = 0; index < _textLength; index++ )
		{
			if ( _buffer[ index ] == oldValue )
			{
				_buffer[ index ] = newValue;
			}
		}
	}
}

void MutableText::Replace( const char* oldValue, const char* newValue )
{
	unsigned int oldLen = strlen( oldValue );
	unsigned int newLen = strlen( newValue );
	unsigned int count = 0;
	unsigned int newTextLength = 0;

	unsigned int limit = _textLength - oldLen + 1;

	for ( unsigned int index = 0; index < limit; )
	{
		if ( matches( index, oldValue, oldLen ) )
		{
			count++;
			newTextLength += newLen;
			index += oldLen;
		}
		else
		{
			index++;
			newTextLength += index == limit ? oldLen : 1;
		}
	}

	if ( count > 0 )
	{
		unsigned int newBufferSize = grow( newTextLength );

		unsigned int originalIndex = 0;
		for ( unsigned int index = 0; index < newBufferSize; )
		{
			if ( matches( index, oldValue, oldLen ) )
			{
				char* dest = _buffer + index + newLen;
				char* src = _buffer + index + oldLen;
				unsigned int len = _textLength - originalIndex - oldLen;
				memcpy( dest, src, len );
				memcpy( _buffer + index, newValue, newLen );
				index += newLen;
				originalIndex += oldLen;
			}
			else
			{
				originalIndex++;
				index++;
			}
		}

		_textLength = newTextLength;
		_bufferSize = newBufferSize;
		terminate( );
	}
}

void MutableText::Replace( const Text& oldValue, const Text& newValue )
{
	Replace( oldValue._value, newValue._value );
}

void MutableText::PadLeft( const Text& value, unsigned int count )
{
	PadLeft( value._value, count );
}

void MutableText::PadLeft( const char* value, unsigned int count )
{
	unsigned int length = strlen( value );
	unsigned int newTextLength = _textLength + ( length * count );
	unsigned int newBufferSize = grow( newTextLength );

	memcpy( _buffer + ( length * count ), _buffer, _textLength );
	for ( unsigned int index = 0; index < count; index++ )
	{
		memcpy( _buffer + index * length, value, length );
	}
	_textLength = newTextLength;
}

void MutableText::PadLeft( const char value, unsigned int count )
{
	unsigned int newTextLength = _textLength + count;
	unsigned int newBufferSize = grow( newTextLength );

	memcpy( _buffer + count, _buffer, _textLength );
	memset( _buffer, value, count );
	_textLength = newTextLength;
}

void MutableText::PadLeft( unsigned int count )
{
	PadLeft( ' ', count );
}

void MutableText::PadRight( const Text& value, unsigned int count )
{
	PadRight( value._value, count );
}

void MutableText::PadRight( const char* value, unsigned int count )
{
	unsigned int length = strlen( value );
	unsigned int newTextLength = _textLength + ( length * count );
	unsigned int newBufferSize = grow( newTextLength );
	char* baseAddr = _buffer + _textLength;

	for ( unsigned int index = 0; index < count; index++ )
	{
		memcpy( baseAddr + index * length, value, length );
	}
	_textLength = newTextLength;
}

void MutableText::PadRight( const char value, unsigned int count )
{
	unsigned int newTextLength = _textLength + count;
	unsigned int newBufferSize = grow( newTextLength );

	memset( _buffer + _textLength, value, count );
	_textLength = newTextLength;
}

void MutableText::PadRight( unsigned int count )
{
	PadRight( ' ', count );
}

void MutableText::ToUpper( )
{
	_strupr_s( _buffer, _textLength + 1 );
}

void MutableText::ToLower( )
{
	_strlwr_s( _buffer, _textLength + 1 );
}

void MutableText::RemoveFromStart( unsigned int count )
{
	count = count > _textLength ? _textLength : count;
	memcpy( _buffer, _buffer + count, _textLength - count );
	_textLength -= count;
	terminate( );
}

void MutableText::TrimStart( const char value )
{
	unsigned int index = 0;

	while ( index < _textLength && _buffer[ index ] == value )
	{
		index++;
	}

	if ( index > 0 )
	{
		memcpy( _buffer, _buffer + index, _textLength - index );
		_textLength -= index;
		terminate( );
	}
}

void MutableText::RemoveFromEnd( unsigned int count )
{
	count = count > _textLength ? _textLength : count;
	memset( _buffer + _textLength - count, 0, count );
	_textLength -= count;
}

void MutableText::TrimEnd( const char value )
{
	unsigned int index = 0;

	while ( index < _textLength && _buffer[ _textLength - index - 1 ] == value )
	{
		index++;
	}

	if ( index > 0 )
	{
		memset( _buffer + _textLength - index, 0, index );
		_textLength -= index;
	}
}

void MutableText::Trim( const char value )
{
	TrimStart( value );
	TrimEnd( value );
}

void MutableText::TrimWhitespace( )
{
	Trim( ' ' );
}

char* MutableText::GetValue( )
{
	char* result = new char[ _textLength + 1 ];
	memcpy( result, _buffer, _textLength + 1 );
	clear( );
	return result;
}

char MutableText::CharacterAt( unsigned int index ) const
{
	return _buffer[ index ];
}

char MutableText::operator[]( unsigned int index ) const
{
	return CharacterAt( index );
}

unsigned int MutableText::TextLength( ) const
{
	return _textLength;
}

unsigned int MutableText::BufferSize( ) const 
{
	return _bufferSize;
}

char* MutableText::GetSubText( unsigned int startIndex, unsigned int endIndex ) const
{
	char* output = nullptr;

	if ( endIndex < _bufferSize )
	{
		unsigned int length = endIndex - startIndex;

		output = ( char* ) memcpy( new char[ length + 1 ], _buffer + startIndex, length );
		output[ length ] = 0;
	}

	return output;
}

bool MutableText::operator==( const char* rhs ) const
{
	return strncmp( _buffer, rhs, _textLength ) == 0;
}

bool MutableText::operator!=( const char* rhs ) const
{
	return strncmp( _buffer, rhs, _textLength ) != 0;
}

bool MutableText::operator==( const char rhs ) const
{
	return _textLength == 1 && _buffer[ 0 ] == rhs;
}

bool MutableText::operator!=( const char rhs ) const
{
	return _textLength != 1 || _buffer[ 0 ] != rhs;
}

bool MutableText::operator==( const MutableText& rhs ) const
{
	return _textLength == rhs._textLength && strncmp( _buffer, rhs._buffer, _textLength ) == 0;
}

bool MutableText::operator!=( const MutableText& rhs ) const
{
	return _textLength != rhs._textLength || strncmp( _buffer, rhs._buffer, _textLength ) != 0;
}

bool MutableText::operator==( const Text& rhs ) const
{
	return _textLength == rhs.Length( ) && strncmp( _buffer, rhs._value, _textLength ) == 0;
}

bool MutableText::operator!=( const Text& rhs ) const
{
	return _textLength != rhs.Length( ) || strncmp( _buffer, rhs._value, _textLength ) != 0;
}

MutableText& MutableText::operator=( const char rhs )
{
	initialize( nullptr );
	_buffer[ 0 ] = rhs;
	_textLength = 1;

	return *this;
}

MutableText& MutableText::operator=( const char* rhs )
{
	initialize( rhs );

	return *this;
}

MutableText& MutableText::operator=( const MutableText& rhs )
{
	initialize( rhs._buffer, rhs._bufferSize );

	return *this;
}

MutableText::operator char*( )
{
	return GetValue( );
}