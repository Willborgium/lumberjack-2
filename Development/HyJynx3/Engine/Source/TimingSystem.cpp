#include "..\Headers\Core.h"
using namespace HyJynxCore;

TimingSystem::TimingSystem( )
: ISystem( "TimingSystem" )
{
}

bool TimingSystem::Initialize( )
{
	bool result = false;

	if ( _state == SystemState::Uninitialized )
	{
		_systemTimer = anew( HighPrecisionTimer )( );
		_systemTimer->SetInfiniteLooping( true );
		_systemTimer->SetInterval( TimeLength::OneMicrosecond );
		_systemTimer->Start( );
		_state = SystemState::Initialized;
		result = true;
	}

	return result;
}

void TimingSystem::Update( )
{
	if (_state == SystemState::Running )
	{
		this->_timers.ForEach( [&] ( HighPrecisionTimer* timer )
		{
			if ( timer != nullptr )
			{
				timer->Update( );
			}
		} );
	}
}

bool TimingSystem::Uninitialize( )
{
	bool output = false;

	if (_state == SystemState::Running || _state == SystemState::Initialized )
	{
		_timers.ForEach( [&] ( HighPrecisionTimer* timer )
		{
			if ( timer != nullptr )
			{
				timer->Stop( );
			}
		} );

		output = true;
	}

	return output;
}

bool TimingSystem::RegisterTimer( HighPrecisionTimer* timer )
{
	bool result = false;

	if ( timer != nullptr && !_timers.Contains( timer ) )
	{
		_timers.Append( timer );

		result = true;
	}

	return result;
}

bool TimingSystem::UnRegisterTimer( HighPrecisionTimer* timer )
{
	bool result = false;

	if ( timer != nullptr && _timers.Contains( timer ) )
	{
		_timers.Remove( timer );
		timer->Stop( );

		result = true;
	}

	return result;
}

bool TimingSystem::IsRegistered( HighPrecisionTimer* timer )
{
	return timer != nullptr && _timers.Contains( timer );
}

TimeSpan TimingSystem::GetSystemElapsed( )
{
	return _systemTimer->GetElapsed( );
}