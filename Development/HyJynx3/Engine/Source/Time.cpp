#include "..\Headers\Core.h"
using namespace HyJynxCore;

#include <cstdlib>

Time::Time( )
	: Second( 0 ), Minute( 0 ), Hour( 0 ),
	DayOfMonth( 0 ), DayOfWeek( 0 ), DayOfYear( 0 ),
	Month( 0 ), Year( 0 )
{
}

Text Time::DayName( ) const
{
	switch( DayOfWeek )
	{
	case DaysOfWeek::Sunday:
		return "Sunday";
	case DaysOfWeek::Monday:
		return "Monday";
	case DaysOfWeek::Tuesday:
		return "Tuesday";
	case DaysOfWeek::Wednesday:
		return "Wednesday";
	case DaysOfWeek::Thursday:
		return "Thursday";
	case DaysOfWeek::Friday:
		return "Friday";
	case DaysOfWeek::Saturday:
		return "Saturday";
	default:
		return "Not A Day";
	}
}
Text Time::MonthName( ) const
{
	switch( Month )
	{
	case Months::January:
		return "January";
	case Months::February:
		return "February";
	case Months::March:
		return "March";
	case Months::April:
		return "April";
	case Months::May:
		return "May";
	case Months::June:
		return "June";
	case Months::July:
		return "July";
	case Months::August:
		return "August";
	case Months::September:
		return "September";
	case Months::October:
		return "October";
	case Months::November:
		return "November";
	case Months::December:
		return "December";
	default:
		return "Not A Month";
	}
}

Text Time::DatePart( USmall format ) const
{
	Text me = "";

	if( format == DateFormat::ShortDate )
	{
		me += Text( Month );
		me += "/";
		me += Text( DayOfMonth );
		me += "/";
		me += Text( Year );		
	}
	else
	{
		me += DayName( ) + ", ";
		me += MonthName( ) + " ";
		me += Text( DayOfMonth );

		switch( DayOfMonth % 10 )
		{
		case 1:
			me += "st";
			break;
		case 2:
			me += "nd";
			break;
		case 3:
			me += "rd";
			break;
		default:
			me += "th";
			break;
		}

		me += ", ";
		me += Text( Year );
	}

	return me;
}
Text Time::TimePart( ) const
{
	Text me = "";
	bool isPM = Hour >= 12;
	me += Text( Hour % 12 );
	me += ":";

	if( Minute < 10 )
	{
		me += "0";
	}

	me += Text( Minute );
	me += ":";

	if( Second < 10 )
	{
		me += "0";
	}
	me += Text( Second );

	me += isPM ? " PM" : " AM";

	return me;
}

Text Time::ToText( USmall format ) const
{
	Text me = DatePart( format ) + " ";
	me += TimePart( );
	return me;
}
TimeCode Time::ToTimeCode( )
{
	TimeCode code = 0;

	memcpy( ((char*)(&code)), &Second, 1 );
	memcpy( ((char*)(&code)) + 1, &Minute, 1 );
	memcpy( ((char*)(&code)) + 2, &Hour, 1 );
	memcpy( ((char*)(&code)) + 3, &DayOfYear, 2 );
	memcpy( ((char*)(&code)) + 5, &Year, 2 );

	return code;
}

Time Time::Today( )
{
	Time t = Now( );

	t.Hour = 0;
	t.Minute = 0;
	t.Second = 0;

	return t;
}
Time Time::FromTimeStamp(TimeStamp time)
{
	const time_t* ptTime = &time;
	tm _tm;
	memset(&_tm, 0, sizeof(tm));
	errno_t err = localtime_s(&_tm, ptTime);

	Time ts;

	ts.Second = _tm.tm_sec;
	ts.Minute = _tm.tm_min;
	ts.Hour = _tm.tm_hour;
	ts.DayOfMonth = _tm.tm_mday;
	ts.DayOfWeek = _tm.tm_wday + 1;
	ts.DayOfYear = _tm.tm_yday + 1;
	ts.Month = _tm.tm_mon + 1;
	ts.Year = _tm.tm_year + 1900;

	return ts;
}

Time Time::Now()
{
	return FromTimeStamp(time(nullptr));
}

Time Time::FromTimeCode( TimeCode code )
{
	Time t;

	char* pCode = (char*)(&code);

	memcpy( &t.Second, pCode,	1 );
	memcpy( &t.Minute, pCode + 1, 1 );
	memcpy( &t.Hour, pCode + 2, 1 );
	memcpy( &t.DayOfYear, pCode + 3, 2 );
	memcpy( &t.Year, pCode + 5, 2 );

	return t;
}