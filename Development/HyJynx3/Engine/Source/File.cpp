#include "..\Headers\Core.h"
#include "..\Headers\MemoryManagement.h"

using namespace HyJynxCore;
using namespace HyJynxMemoryManagement;

File::File()
: _path(""), _isOpen(false), _mode(OpenMode::Closed)
{
}

File::File(char* path)
: _path(path), _isOpen(false), _mode(OpenMode::Closed)
{
}

File::File(const char* path)
: _path(const_cast<char*>(path)), _isOpen(false), _mode(OpenMode::Closed)
{
}

File::~File()
{
	if (_isOpen)
	{
		Close();
	}
}

bool File::IsOpen() const
{
	return _isOpen;
}

int File::Mode() const
{
	return _mode;
}

void File::Open( int mode, _In_ bool isBinary )
{
	if (Text::LengthOf(_path) > 0 && !_isOpen)
	{
		_mode = mode;
		
		switch (mode)
		{
		case OpenMode::Read:
			_file.open(_path, std::fstream::in | (isBinary ? std::fstream::binary : 0));
			break;
		case OpenMode::Write:
			_file.open( _path, std::fstream::out | std::fstream::app | ( isBinary ? std::fstream::binary : 0 ) );
			break;
		case OpenMode::Overwrite:
			_file.open( _path, std::fstream::out | ( isBinary ? std::fstream::binary : 0 ) );
			break;
		case OpenMode::ReadWrite:
			_file.open( _path, std::fstream::in | std::fstream::out | std::fstream::app | ( isBinary ? std::fstream::binary : 0 ) );
			break;
		}

		_isOpen = _file.is_open( );
	}
}

void File::Open( _In_ char* path, _In_ int mode, _In_ bool isBinary )
{
	_path = path;
	Open( mode );
}

void File::Close()
{
	if (_isOpen)
	{
		_file.close();
		_isOpen = false;
		_mode = OpenMode::Closed;
	}
}

char* File::Read(LongLong count)
{
	return Read(0, count);
}

LongLong File::GetFileLength()
{
	LongLong output = -1;

	if (_isOpen && (_mode == OpenMode::Read || _mode == OpenMode::ReadWrite))
	{
		auto fileBuffer = _file.rdbuf();

		fileBuffer->pubseekpos(0, _file.in);
		output = fileBuffer->pubseekoff(0, _file.end, _file.in);
		fileBuffer->pubseekpos(0, _file.in);
	}

	return output;
}

char* File::Read(LongLong start, LongLong count)
{
	char* output = nullptr;

	if (_isOpen && (_mode == OpenMode::Read || _mode == OpenMode::ReadWrite))
	{
		LongLong fSize = GetFileLength();

		if (start < fSize)
		{
			if (count > fSize - start)
			{
				count = fSize - start;
			}

			output = anewa(char, (size_t)count);
			memset(output, 0, (size_t)count);

			auto fileBuffer = _file.rdbuf();
			fileBuffer->pubseekpos(start, _file.in);
			fileBuffer->sgetn(output, count);
		}
	}

	return output;
}

char* File::ReadToEnd( )
{
	char* output = nullptr;

	if (_isOpen && (_mode == OpenMode::Read || _mode == OpenMode::ReadWrite))
	{
		auto fileBuffer = _file.rdbuf();

		fileBuffer->pubseekpos(0, _file.in);
		LongLong fileSize = fileBuffer->pubseekoff(0, _file.end, _file.in);
		fileBuffer->pubseekpos(0, _file.in);

		//output = anewa(char, (size_t)fileSize);
		output = new char [ (UInt)fileSize ];
		memset(output, 0, (size_t)fileSize);
		fileBuffer->sgetn(output, fileSize);
	}

	return output;
}

void File::Write(char* value)
{
	if (_isOpen && (_mode == OpenMode::Write || _mode == OpenMode::ReadWrite || _mode == OpenMode::Overwrite))
	{
		_file.write(value, Text::LengthOf(value));
	}
}

void File::Append(char* value)
{
	if (_isOpen && (_mode == OpenMode::Write || _mode == OpenMode::ReadWrite))
	{
		_file << value;
	}
}

void File::AppendLine(char* value)
{
	if (_isOpen && (_mode == OpenMode::Write || _mode == OpenMode::ReadWrite))
	{
		_file << value;
		_file << "\n";
	}
}