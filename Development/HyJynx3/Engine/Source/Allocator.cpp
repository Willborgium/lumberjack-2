#include "..\Headers\MemoryManagement.h"
using namespace HyJynxMemoryManagement;

//void* operator new( size_t size )
//{
//	return malloc( size );
//}

#include <cstdlib>
#include <cstring>

#pragma warning( disable : 4482 )

Allocator* Allocator::_instance;

AllocatorRootHeader** Allocator::_roots = nullptr;

unsigned short Allocator::_rootCount = 0;

unsigned short Allocator::_activeRootCount = 0;

Allocator* Allocator::Instance( Allocator* instance )
{
	if( nullptr == _instance )
	{
		_instance = instance;
	}
			
	return _instance;
}

bool Allocator::InitializeRootSystem( unsigned int rootCount, void* pool )
{
	bool result = false;

	if ( Allocator::_roots == nullptr && Allocator::_rootCount == 0 )
	{
		Allocator::_rootCount = rootCount;
		if ( pool == nullptr )
		{
			Allocator::_roots = ( AllocatorRootHeader** ) malloc( Allocator::_rootCount * sizeof( AllocatorRootHeader* ) );
		}
		else
		{
			Allocator::_roots = ( AllocatorRootHeader** ) pool;
		}
		memset( Allocator::_roots, 0, Allocator::_rootCount * sizeof( AllocatorRootHeader* ) );
		result = true;
	}

	return result;
}

void Allocator::InitializeHeap( bool createNew = true, bool useNameSpace = false )
{
	// Initialize status info
	memset( &_info, 0, sizeof( _info ) );
	_info.FreeBytes = _info.TotalBytes = _size;

	// Grab the heap
	if( createNew )
	{
		_heap = ( char* )malloc( _size );
	}
	_nextFree = ( char* )memset( _heap, 0, _size );		
	
	// Initialize the name space
	if( useNameSpace )
	{
		_nextFreeNameSpace = ( char* )memset( _nameSpace, 0, _nameSpaceSize );	
	}

	// Initialize d-mapping and roots
	_mappingRoot = nullptr;
	for ( unsigned int index = 0; index < Allocator::_rootCount; index++ )
	{
		AllocatorRootHeader* ptr = Allocator::_roots [ index ];

		if ( ptr != nullptr && ptr->_namePtr != nullptr )
		{
			ptr->_nameIndex = RegisterName( ptr->_namePtr );
		}
	}
}

void* Allocator::End( ) const
{
	return _heap + _size;
}

void* Allocator::GetSlot( unsigned int size )
{
	auto searchPtr = _nextFree;
	auto heapEnd = ( char* ) End( );
	auto endPtr = heapEnd;
	AllocatorHeader** info = ( AllocatorHeader** )&searchPtr;

	// While the searchPtr is pointing in the heap and the size of the hole
	// is smaller than the requested size...
	while( searchPtr < heapEnd && endPtr <= heapEnd &&
		   ( unsigned int )( endPtr - searchPtr ) < size &&
		   ( unsigned int )( heapEnd - searchPtr ) >= size )
	{
		if( (*info)->_reserved[0] == ALLOCATOR_HEADER_KEY )
		{
			// jump to the next object
			endPtr = searchPtr += (*info)->_size + _headerSize;
		}
		else
		{
			// increase the measured size of the hole
			endPtr++;
		}
	}
	
	if ( searchPtr >= heapEnd || endPtr > heapEnd ||
		( unsigned int ) ( heapEnd - searchPtr ) < size )
	{
		return nullptr;
	}

	_nextFree = searchPtr + size;

	return searchPtr;
}

void Allocator::FreeHeader( AllocatorHeader* header )
{
	if( header != nullptr )
	{
		// Destroy the object
		if( header->_destructor != nullptr )
		{
			header->_destructor->CallDestructor( ( ( ( char* )header ) + _headerSize ) );
		}

		// Gather some info
		auto size = _headerSize + header->_size;
		
		_info.AllocatedBytes -= size;

		_info.HeaderBytes -= _headerSize;
		_info.DataBytes -= header->_size;

		_info.FreeBytes += size;
		_info.ObjectCount--;
		_info.ReleaseCalls++;
		_info.TotalFreedBytes += size;

		memset( header, 0, size );
	}
}

unsigned int Allocator::MarkSpot( char* ptr, bool isRoot )
{
	unsigned int output = 0;
	char* search = nullptr;
	AllocatorHeader* header = nullptr;
	char* end = nullptr;
	void* heapEnd = End( );

	if ( isRoot )
	{
		AllocatorRootHeader* rHeader = ( AllocatorRootHeader* ) ptr;
		header = rHeader;
		search = (char*)rHeader->_address;
		end = search + rHeader->_size;
	}
	else
	{
		search = ptr;
		header = ( AllocatorHeader* ) ( search - _headerSize );
		end = search + _headerSize + header->_size;
	}

	// if the header's first byte matches the header key, it is probably a header
	if( header->_reserved[ 0 ] == ALLOCATOR_HEADER_KEY )
	{
		// touch it
		header->_reserved[ 1 ] = AllocatorHeaderMark::Touched;
		output++;

		char* searchEnd = ptr + header->_size;

		// search for any pointers this object may have, and mark those
		while( search < end )
		{
			char* s = (char*)(*(int*)search);

			if ( s > _heap && s < heapEnd )
			{
				char* longHeaderPtr = s - _headerSize;

				if ( longHeaderPtr >= _heap && longHeaderPtr < heapEnd )
				{
					AllocatorHeader* longHeader = ( AllocatorHeader* ) ( ( s - _headerSize ) );

					// is this piece of the object a pointer to a managed object?
					if ( longHeader->_reserved[ 0 ] == ALLOCATOR_HEADER_KEY && longHeader->_reserved[ 1 ] == AllocatorHeaderMark::Untouched )
					{
						longHeader->_reserved[ 1 ] = AllocatorHeaderMark::Touched;

						output++;

						// recursively mark that object
						output += MarkSpot( s );
						search += 4;
					}
					else
					{
						search++;
					}
				}
				else
				{
					search++;
				}
			}
			else
			{
				search++;
			}
		}
	}

	return output;
}

Allocator::Allocator( )
	: _headerSize( sizeof( AllocatorHeader ) )
{
	_heap = nullptr;
}

Allocator::Allocator( unsigned int size )
	: _size( size ), _headerSize( sizeof( AllocatorHeader ) ), _nameSpace( nullptr ), _nameSpaceSize( 0 )
{
	InitializeHeap( );
}

Allocator::Allocator( _In_ char* buffer, _In_ unsigned int size )
	: _heap( buffer ), _size( size ), _headerSize( sizeof( AllocatorHeader ) ), _nameSpace( nullptr ), _nameSpaceSize( 0 )
{
	InitializeHeap( false );
}

Allocator::Allocator( _In_ char* buffer, _In_ unsigned int size, _In_ char* nsBuffer, _In_ unsigned int nsSize )
	: _heap( buffer ), _size( size ), _headerSize( sizeof( AllocatorHeader ) ), _nameSpace( nsBuffer ), _nameSpaceSize( nsSize )
{
	InitializeHeap( false, true );
}

bool Allocator::RemoveRoot( void* root )
{
	int index = 0;
	AllocatorRootHeader* header = nullptr;

	while( index < _rootCount )
	{
		header = _roots [ index ];

		if ( header == nullptr || header->_address != root )
		{
			index++;
		}
	}		

	if ( header != nullptr )
	{
		delete header;
		_roots [ index ] = nullptr;
		//_info.TotalRootHeadersFreed--;
		_activeRootCount--;
		return true;
	}

	return false;
}

void Allocator::Free( void* obj )
{
	if( obj > _heap && obj < End( ) )
	{
		AllocatorHeader* header = ( AllocatorHeader* )( ( char* ) obj - _headerSize );

		FreeHeader( header );
	}
}

const AllocatorInfo& Allocator::GetInfo( )
{
	_info.RootCount = GetCurrentRootCount( );

	return _info;
}

void Allocator::Clean( )
{
	_info.CycleStamp++;

	auto details = ShouldClean( );

	if ( details.ShouldClean )
	{
		_info.TotalCleanCalls++;

		// Mark
		unsigned short index = 0;

		while ( index < _rootCount && _roots[ index ] != nullptr )
		{
			auto count = MarkSpot( ( char* ) _roots[ index ], true );
			index++;
		}

		if ( index > 0 )
		{
			// Sweep
			char* search = _heap;
			void* end = End( );

			while ( search < end )
			{
				auto header = ( AllocatorHeader* ) search;

				if ( header->_reserved[ 0 ] == ALLOCATOR_HEADER_KEY )
				{
					char* next = search + header->_size + _headerSize;

					if ( header->_reserved[ 1 ] == AllocatorHeaderMark::Untouched &&
						header->_reserved[ 2 ] != AllocatorHeaderUsage::Constant &&
						header->_reserved[ 2 ] != AllocatorHeaderUsage::Permanent )
					{
						FreeHeader( header );
					}
					else
					{
						header->_reserved[ 1 ] = AllocatorHeaderMark::Untouched;
					}

					search = next;
				}
				else
				{
					search++;
				}
			}

			// Compact
			Compact( );
		}

		_lastCleanInfo = _info;
	}
}

void Allocator::Refresh( void* oldAddress, void* newAddress )
{
	char* search = _heap;
	void* end = (char*)End( ) - sizeof( int* );

	// refresh the heap
	while( search < end )
	{
		char* addr = ( char* )( *( int* )search );

		if( addr == oldAddress )
		{
			memcpy( search, &newAddress, sizeof( int* ) );
			search += sizeof( int* );
		}
		else
		{
			search++;
		}
	}

	// refresh the roots
	unsigned short index = 0;
	
	while ( index < _rootCount && _roots [ index ] != nullptr )
	{
		search = (char*)_roots [ index ]->_address;
		end = search + _roots [ index ]->_size;

		while ( search < end )
		{
			char* addr = ( char* ) ( *( int* ) search );

			if ( addr == oldAddress )
			{
				memcpy( search, &newAddress, sizeof( int* ) );
				search += sizeof( int* );
			}
			else
			{
				search++;
			}
		}

		index++;
	}
}

unsigned short Allocator::GetCurrentRootCount( )
{
	unsigned short index = 0;
	unsigned short count = 0;

	while( index < _rootCount )
	{
		count += _roots [ index++ ] != nullptr ? 1 : 0;
	}

	return count;
}

void Allocator::Compact( )
{
	char* search = _heap;
	void* end = End( );

	while( search < end )
	{
		if( *search != 0 )
		{
			auto header = ( AllocatorHeader* )search;

			_nextFree = search += _headerSize + header->_size;
		}
		else
		{
			char* next = _nextFree = search;

			while( next < end && *next == 0 )
			{
				next++;
			}

			if( next < end && *next != 0 )
			{
				AllocatorHeader* nextHeader = ( AllocatorHeader* )next;

				if ( nextHeader->_reserved[ 0 ] != ALLOCATOR_HEADER_KEY )
				{
					throw "Invalid Header";
				}

				unsigned int totalSize = _headerSize + nextHeader->_size;
								
				nextHeader = ( AllocatorHeader* )memmove( search, next, totalSize );

				Refresh( next + _headerSize, search + _headerSize );

				next = ( char* ) memset( search + totalSize, 0, next - search );
			}
				
			search = next;
		}
	}
}

Allocator::~Allocator( )
{
	memset( _heap, 0, _size );
	delete [] _heap;
}

unsigned int Allocator::RegisterName( const char* typeName )
{
	const unsigned int MAX_NAME_LENGTH = 256;

	unsigned int retVal = -1;

	if( _nameSpace != nullptr && _nameSpaceSize > 0 && typeName != nullptr )
	{
		char* shortName = nullptr;
		unsigned int countToSpace = 0;

		unsigned int length = strnlen_s( typeName, MAX_NAME_LENGTH );

		while ( countToSpace < length &&
				typeName [ countToSpace ] != 0 )
		{
			countToSpace++;

			if ( typeName [ countToSpace ] == ' ' )
			{
				countToSpace++;
				break;
			}
		}

		length -= countToSpace;
		shortName = (char*)typeName + countToSpace;

		if( length > 0 )
		{
			int offset = 0;
			char* nsPtr = _nameSpace;

			while ( nsPtr < _nextFreeNameSpace )
			{
				bool isSame = true;
				for ( unsigned int index = 0; index < length; index++ )
				{
					isSame = nsPtr [ index ] == shortName [ index ];

					if ( !isSame )
					{
						break;
					}
				}

				if ( isSame )
				{
					retVal = offset;
					break;
				}
				else
				{
					unsigned int nssLen = strnlen_s( nsPtr, MAX_NAME_LENGTH );
					nsPtr += nssLen + 1;
				}
			}

			if ( retVal == ~0 )
			{
				if ( _nextFreeNameSpace < ( _nameSpace + _nameSpaceSize - length ) )
				{
					retVal = ( char* ) memcpy( _nextFreeNameSpace, shortName, length ) - _nameSpace;
					_nextFreeNameSpace += length + 1;
				}
			}
		}
	}

	return retVal;
}

Allocator::CleanDetails Allocator::ShouldClean( )
{
	CleanDetails output;

	// Should clean if:
	// - Within the first ten cycles
	// - if a clean was requested
	// - There have been over 1,000 allocate calls since the last clean
	// - There have been 10,000 cycles between cleans
	// - There are less than 1,000 free bytes

	output.ShouldClean = _info.CycleStamp < 10 || _isCleanRequested || _info.AllocateCalls - _lastCleanInfo.AllocateCalls > 1000 ||
						 _info.CycleStamp - _lastCleanInfo.CycleStamp > 10000 ||
						 _info.FreeBytes < 1000;

	_isCleanRequested = false;

	return output;
}

void Allocator::RequestClean( )
{
	_isCleanRequested = true;
}