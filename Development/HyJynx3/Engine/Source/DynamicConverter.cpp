#include "..\Headers\Core.h"
using namespace HyJynxCore;

DynamicConverter::DynamicConverter( )
{
}

DynamicConverter::DynamicConverter( std::function<void*(void*)> method )
	: _method( method )
{
}

void DynamicConverter::SetConvertMethod( std::function<void*(void*)> method )
{
	_method = method;
}

void* DynamicConverter::Convert( void* object )
{
	return _method( object );
}