#include "..\Headers\Core.h"
using namespace HyJynxCore;

Text& JsonReader::PropertyValueText( JsonData* object, Text& propertyName )
{
	return *( object->Data->ObjectData->ValueOf( propertyName )->Data->ValueData );
}

int JsonReader::PropertyValueInt( JsonData* object, Text& propertyName )
{
	return object->Data->ObjectData->ValueOf( propertyName )->Data->ValueData->ToInt( );
}

double JsonReader::PropertyValueDouble( JsonData* object, Text& propertyName )
{
	return object->Data->ObjectData->ValueOf( propertyName )->Data->ValueData->ToDouble( );
}

bool JsonReader::PropertyValueBoolean( JsonData* object, Text& propertyName )
{
	return object->Data->ObjectData->ValueOf( propertyName )->Data->ValueData->ToBoolean( );
}

JsonObject* JsonReader::PropertyValueData( JsonData* object, Text& propertyName )
{
	return object->Data->ObjectData->ValueOf( propertyName )->Data->ObjectData;
}