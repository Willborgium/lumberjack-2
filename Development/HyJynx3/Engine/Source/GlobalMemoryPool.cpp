#include "..\Headers\MemoryManagement.h"
using namespace HyJynxMemoryManagement;

#include <Windows.h>

GlobalMemoryPool::GlobalMemoryPool( )
	: _size( 0 ), _name( nullptr ),
	_handle( nullptr ), _view( nullptr ),
	_isOpen( false )
{
}

GlobalMemoryPool::GlobalMemoryPool( char* name, unsigned long size )
	: _name( name ), _size( size )
{
}

bool GlobalMemoryPool::Initialize( char* name, unsigned long size )
{
	if( !_isOpen )
	{
		_name = name;
		_size = size;
		return true;
	}
	else
	{
		return false;
	}
}

bool GlobalMemoryPool::Open( )
{
	_handle = CreateFileMappingA( INVALID_HANDLE_VALUE, nullptr, PAGE_READWRITE, 0, _size, _name );

	if( _handle == nullptr )
	{
		_isOpen = false;
	}
	else
	{
		_view = ( char* )MapViewOfFile( _handle, FILE_MAP_ALL_ACCESS, 0, 0, _size );

		if( _view == nullptr )
		{
			_isOpen = false;
			CloseHandle( _handle );
		}
		else
		{
			_isOpen = true;
		}
	}

	return _isOpen;
}

bool GlobalMemoryPool::Close( )
{
	if( _isOpen && _handle != nullptr && _view != nullptr )
	{
		UnmapViewOfFile( _view );
		CloseHandle( _handle );
		_isOpen = false;
		return true;
	}
	else
	{
		return false;
	}
}

bool GlobalMemoryPool::Write( char* value, unsigned long size, unsigned long location )
{
	if( size + location > _size ||
		_isOpen == false )
	{
		return false;
	}
	else
	{
		memcpy( ( _view + location ), value, size );
		return true;
	}
}

const char* GlobalMemoryPool::Read( ) const
{
	if( _isOpen == false )
	{
		return nullptr;
	}
	else
	{
		return _view;
	}
}