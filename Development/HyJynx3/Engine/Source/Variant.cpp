#include "..\Headers\Core.h"
using namespace HyJynxCore;

Variant::Variant( )
	: _value( nullptr ), _type( nullptr ), _isPointer( false )
{
}

const char* Variant::TypeName( ) const
{
	return _type != nullptr ? _type->name( ) : nullptr;
}

void Variant::operator()()
{
	typedef void(*fptr)();
	reinterpret_cast<fptr>(reinterpret_cast<int>(_value))();
}