/**
 * CompletionChecker.cpp
 * (c) 2014 All Rights Reserved
 */

#include "..\Headers\CompletionChecker.h"

using namespace std;

namespace HyJynxUtilities
{
	// null ctor
	CompletionChecker::CompletionChecker( )
	{ }

	// default ctor
	CompletionChecker::CompletionChecker( _In_ UInt count, _In_ function< void( void ) > onComplete )
		: _total( count ),
		_currentCount( 0 ),
		_onCompleteCallback( onComplete )
	{ }

	// copy ctor
	CompletionChecker::CompletionChecker( _In_ const CompletionChecker& other )
		: _total( other._total ),
		_currentCount( other._currentCount ),
		_onCompleteCallback( other._onCompleteCallback )
	{ }

	// move ctor
	CompletionChecker::CompletionChecker( _In_ const CompletionChecker&& other )
		: _total( other._total ),
		_currentCount( other._currentCount ),
		_onCompleteCallback( other._onCompleteCallback )
	{ }

	// dtor
	CompletionChecker::~CompletionChecker( )
	{
		_total = 0;
		_currentCount = 0;
		_onCompleteCallback = nullptr;
	}

	// add current and check for completion
	void CompletionChecker::InformProcComplete( )
	{
		_currentCount++;
		if ( _currentCount == _total )
		{
			if ( _onCompleteCallback != nullptr )
			{
				_onCompleteCallback( );
			}
		}
	}
}