/**
 * KeyboardListener.cpp
 * (c) 2014 All Rights Reserved
 */

#include <InputEngine\Headers\Input.h>
using namespace HyJynxInput;

// null ctor
KeyboardListener::KeyboardListener( )
{ }

// default ctor
KeyboardListener::KeyboardListener( _In_ HyJynxCore::Text name, _In_opt_ const bool active )
	: _name( name ),
	_isActive( active )
{
	InputManager::Instance( )->GetKeyboardManager( )->RegisterListener( this );
}

// set the listener to an active/deactive state
void KeyboardListener::SetActive( _In_ const bool isActive )
{
	_isActive = isActive;
}

// determine if the handler is active or not
bool KeyboardListener::IsActive( ) const
{
	return _isActive;
}

// add a new handler, listening for a defined key event
void KeyboardListener::RegisterHandler( _In_ KeyEventHandler* handler )
{
	_handlers.Append( handler );
}

void KeyboardListener::RegisterHandler( _In_ Keys key, _In_ KeyState state, _In_ HyJynxCore::Function<void, KeyState> callback )
{
	RegisterHandler( anew( KeyEventHandler )( key, state, callback ) );
}

// remove a key event handler by name
void KeyboardListener::UnregisterHandler( _In_ const HyJynxCore::Text name )
{
	auto remove = _handlers.Where( [&name] ( KeyEventHandler* h )
	{
		return h->Name == name;
	} )->First( );

	if ( remove != nullptr )
	{
		_handlers.Remove( remove );
	}
}

// get the list of handlers this keyboard listener is reacting to
HyJynxCollections::DynamicCollection<KeyEventHandler*>& KeyboardListener::GetHandlers( )
{
	return _handlers;
}