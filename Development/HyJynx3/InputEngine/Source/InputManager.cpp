/**
 * InputManager.cpp
 * (c) 2014 All Rights Reserved
 */

#include <InputEngine\Headers\Input.h>
#include <Logger\Headers\Logger.h>

using namespace HyJynxInput;
using namespace HyJynxLogger;

InputManager::InputManager( )
{ }

InputManager::~InputManager( )
{
	if ( _directInput != nullptr )
	{
		_directInput->Release( );
		_directInput = nullptr;
	}
	
	if ( _keyboardManager != nullptr )
	{
		//delete _keyboardManager; // TODO
		_keyboardManager = nullptr;
	}

	_hInst = nullptr;
	_hWnd = nullptr;
}

bool InputManager::Initialize( _In_ HINSTANCE hInst, _In_ HWND hWnd )
{
	if ( hInst == nullptr )
	{
		Logger::Log( "InputManager - invalid argument, unable to Initialize." );
		return false;
	}
	if ( hWnd == nullptr )
	{
		Logger::Log( "InputManager - invalid argument, unable to Initialize." );
		return false;
	}

	_hInst = hInst;
	_hWnd = hWnd;

	if ( FAILED( DirectInput8Create(
		hInst != nullptr ? hInst : GetModuleHandle( NULL ),
		DIRECTINPUT_VERSION, 
		IID_IDirectInput8,
		( void** ) &_directInput, 
		NULL ) ) )
	{
		Logger::Log( "InputManager - unable to create direct input device." );
		return false;
	}

	_keyboardManager = anew( KeyboardManager )( );
	if ( !_keyboardManager->Initialize( _directInput, _hWnd ) )
	{
		Logger::Log( "InputManager - KeyboardManager failed to Initialize." );
		return false;
	}

	//_mouseManager = new MouseManager ( );
	//if ( _mouseManager->Initialize ( CreateMouseDevice ( ) ) == false )
	//{
	//	// log error
	//	return false;
	//}

	// NOTE: Here is where you add new Input Components. (like LEAP! and XBOX Controller!)

	return true;
}

//LPDIRECTINPUTDEVICE8 InputManager::CreateMouseDevice ( )
//{
//	LPDIRECTINPUTDEVICE8 device;

//	HRESULT result = _directInput->CreateDevice ( GUID_SysMouse, &device, NULL );
//	if ( FAILED ( result ) )
//	{
//		// log - unable to create to the keyboard, abandon ship
//		return NULL;
//	}

//	result = device->SetDataFormat ( &c_dfDIMouse );
//	if ( FAILED ( result ) )
//	{
//		// log - unable to set data format
//		return NULL;
//	}

//	result = device->SetCooperativeLevel ( *_hWnd, DISCL_FOREGROUND | DISCL_EXCLUSIVE );
//	if ( FAILED ( result ) )
//	{
//		// log - unable to set data coop level
//		return NULL;
//	}

//	return device;
//}

void InputManager::Update( )
{
	if ( _keyboardManager != nullptr )
	{
		_keyboardManager->Update( );
	}
}