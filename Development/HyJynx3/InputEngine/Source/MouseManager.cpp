/**
 * MouseManager.cpp
 * (c) 2014 All Rights Reserved
 */

#include <InputEngine\Headers\Input.h>

using namespace HyJynxCollections;
using namespace HyJynxLogger;

namespace HyJynxInput
{
	// default ctor
	MouseManager::MouseManager( )
		: ILog( "MouseManager" )
	{ }

	// dtor
	MouseManager::~MouseManager( )
	{
		if ( _mouseDevice != nullptr )
		{
			_mouseDevice->Release( );
			_mouseDevice = nullptr;
		}
	}

	// initializer
	bool MouseManager::Initialize( _In_ LPDIRECTINPUTDEVICE8 ms )
	{
		if ( ms == nullptr )
		{
			LogError( "Initialize(nullptr) - invalid argument" );
			return false;
		}

		_mouseDevice = ms;

		return true;
	}
};