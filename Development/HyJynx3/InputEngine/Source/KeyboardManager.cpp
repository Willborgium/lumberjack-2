/**
 * KeyboardManager.cpp
 * (c) 2014 All Rights Reserved
 */

#include <InputEngine\Headers\Input.h>
#include <Engine\Headers\TimingSystem.h>
#include <Logger\Headers\Logger.h>

using namespace HyJynxInput;
using namespace HyJynxCollections;
using namespace HyJynxCore;
using namespace HyJynxLogger;

// default ctor
KeyboardManager::KeyboardManager( )
{
	ClearFrontBuffer( );
	ClearBackBuffer( );
}

KeyboardManager::KeyboardManager( const KeyboardManager& rhs )
	: _device( rhs._device ), _listeners( rhs._listeners )
{
	memcpy( &_backBuffer, &rhs._backBuffer, KEYBOARDMANAGER_STATE_SIZE );
	memcpy( &_frontBuffer, &rhs._frontBuffer, KEYBOARDMANAGER_STATE_SIZE );
}

// dtor
KeyboardManager::~KeyboardManager( )
{
	Unacquire( );

	SAFE_RELEASE( _device );
}

// initialize the manager to an up-running state
bool KeyboardManager::Initialize( _In_ LPDIRECTINPUT8 input, _In_ HWND hwnd )
{
	bool output = false;

	HRESULT result = input->CreateDevice( GUID_SysKeyboard, &_device, NULL );
	if ( SUCCEEDED( result ) && _device != nullptr )
	{
		result = _device->SetCooperativeLevel( hwnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE );
		if ( SUCCEEDED( result ) )
		{
			result = _device->SetDataFormat( &c_dfDIKeyboard );
			if ( SUCCEEDED( result ) )
			{
				output = true;
				Acquire( );
			}
		}
	}

	Logger::Log( output ? "KeyboardManager Initialized." : "KeyboardManager Failed. (harmless)" );

	return output;
}

// aquire the keyboard
bool KeyboardManager::Acquire( )
{
	bool output = false;

	if ( _device != nullptr )
	{
		HRESULT hr = _device->Acquire( );

		output = SUCCEEDED( hr );
	}

	return output;
}

// release the keyboard from our use
bool KeyboardManager::Unacquire( )
{
	bool output = false;

	if ( _device != nullptr )
	{
		output = SUCCEEDED( _device->Unacquire( ) );
	}

	return output;
}

// update the keyboard buffers
bool KeyboardManager::Poll( _In_opt_ bool tryAcquire )
{
	bool output = false;

	if ( _device != nullptr )
	{
		HRESULT hr = _device->GetDeviceState( KEYBOARDMANAGER_STATE_SIZE, ( LPVOID ) &_backBuffer );
		if ( FAILED( hr ) )
		{
			if ( tryAcquire && Acquire( ) )
			{
				Poll( false );
			}
		}
		else
		{
			output = true;
		}
	}

	return output;
}

// process the keyboard states
void KeyboardManager::Update( )
{
	if ( Poll( ) == false )
	{
		return;
	}

	for ( unsigned short key = 0; key < KEYBOARDMANAGER_STATE_SIZE; key++ )
	{
		if ( _backBuffer [ key ] == 0 )
		{
			switch ( _frontBuffer [ key ].State )
			{
				case KeyState::Down:
				case KeyState::Pressed:
					_frontBuffer [ key ].TimeStamp = Time::Now( );
					_frontBuffer [ key ].State = KeyState::Released;
					break;
				case KeyState::Released:
					_frontBuffer [ key ].TimeStamp = Time::Now( );
				case KeyState::Up:
					_frontBuffer [ key ].State = KeyState::Up;
					break;
			}
		}
		else
		{
			switch ( _frontBuffer [ key ].State )
			{
				case KeyState::Pressed:
					_frontBuffer [ key ].TimeStamp = Time::Now( );
				case KeyState::Down:
					_frontBuffer [ key ].State = KeyState::Down;
					break;
				case KeyState::Released:
				case KeyState::Up:
					_frontBuffer [ key ].TimeStamp = Time::Now( );
					_frontBuffer [ key ].State = KeyState::Pressed;
					break;
			}
		}

		NotifyKeyEvent( static_cast< Keys >( key ) );
	}
}

// fire the keyevent to all who is listening
void KeyboardManager::NotifyKeyEvent( _In_ const Keys key )
{
	TimingSystem* timing = TimingSystem::Instance( );

	auto frontBuffer = _frontBuffer;

	auto innerCall = [key, timing, frontBuffer] ( KeyEventHandler* handler )
	{
		bool notify = false;
		KeyEvent* currentBufferKeyEvent = &frontBuffer [ static_cast< unsigned short >( key ) ];

		if ( handler->IsActive && handler->Handler.HasValue( ) )
		{
			if ( handler->Key == key && handler->NextNotifyTime <= timing->GetSystemElapsed( ) )
			{
				switch ( handler->State )
				{
					case KeyState::Down:

						if ( currentBufferKeyEvent->State == KeyState::Pressed )
						{
							notify = true;
							handler->NextNotifyTime = timing->GetSystemElapsed( );
							handler->NextNotifyTime.Add( handler->Delay );
						}
						else if ( currentBufferKeyEvent->State == KeyState::Down )
						{
							notify = true;
							handler->NextNotifyTime = timing->GetSystemElapsed( );
							handler->NextNotifyTime.Add( handler->RepeatInterval );
						}
						else if ( currentBufferKeyEvent->State == KeyState::Up
							|| currentBufferKeyEvent->State == KeyState::Released )
						{
							handler->NextNotifyTime = 0;
						}

						break;

					case KeyState::Up:
						if ( currentBufferKeyEvent->State == KeyState::Released
							&& handler->NextNotifyTime.TotalSeconds( ) == 0 )
						{
							notify = true;
							handler->NextNotifyTime = timing->GetSystemElapsed( );
							handler->NextNotifyTime.Add( handler->Delay );
						}
						else if ( currentBufferKeyEvent->State == KeyState::Up )
						{
							notify = true;
							handler->NextNotifyTime = timing->GetSystemElapsed( );
							handler->NextNotifyTime.Add( handler->RepeatInterval );
						}
						else if ( currentBufferKeyEvent->State == KeyState::Down
							|| currentBufferKeyEvent->State == KeyState::Pressed )
						{
							handler->NextNotifyTime = 0;
						}

						break;

					case KeyState::Pressed:
						if ( currentBufferKeyEvent->State == KeyState::Pressed )
						{
							notify = true;
							handler->NextNotifyTime = timing->GetSystemElapsed( );

							if ( handler->NextNotifyTime.TotalSeconds( ) == 0 )
							{
								handler->NextNotifyTime.Add( handler->Delay );
							}
							else
							{
								handler->NextNotifyTime.Add( handler->RepeatInterval );
							}
						}
						else
						{
							handler->NextNotifyTime = 0;
						}

						break;

					case KeyState::Released:
						if ( currentBufferKeyEvent->State == KeyState::Released )
						{
							notify = true;
							handler->NextNotifyTime = timing->GetSystemElapsed( );

							if ( handler->NextNotifyTime.TotalSeconds( ) == 0 )
							{
								handler->NextNotifyTime.Add( handler->Delay );
							}
							else
							{
								handler->NextNotifyTime.Add( handler->RepeatInterval );
							}
						}
						else
						{
							handler->NextNotifyTime = 0;
						}

						break;
				}
			}
		}

		if ( notify )
		{
			// TODO: pass more useful information along to the callback
			// eg: time-in-state, number of listeners of the key, etc...
			handler->Handler( currentBufferKeyEvent->State );
		}
	};

	auto outerCall = [key, timing, innerCall] ( KeyboardListener* listener )
	{
		if ( listener != nullptr && listener->IsActive( ) )
		{
			listener->GetHandlers( ).ForEach( innerCall );
		}
	};

	_listeners.ForEach( outerCall );
}

// clear the front buffer from any active key states
void KeyboardManager::ClearFrontBuffer( )
{
	ZeroMemory( &_frontBuffer, sizeof( _frontBuffer ) );
}

// clear the back buffer from any active key states
void KeyboardManager::ClearBackBuffer( )
{
	ZeroMemory( &_backBuffer, sizeof( _backBuffer ) );
}

// quick-n-dirty hook to determine a key's state
KeyState KeyboardManager::GetKeyState( _In_ const unsigned short key )
{
	return ( key < KEYBOARDMANAGER_STATE_SIZE ) ? ( KeyState ) _frontBuffer [ key ].State : KeyState::Unknown;
}

// add a handler to receive events
void KeyboardManager::RegisterListener( _In_ KeyboardListener* listener )
{
	_listeners.Append( listener );
}