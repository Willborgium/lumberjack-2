/**
 * KeyEventHandler.cpp
 * (c) 2014 All Rights Reserved
 */

#include <InputEngine\Headers\Input.h>
using namespace HyJynxInput;

// null ctor
KeyEventHandler::KeyEventHandler( )
{ }

// name ctor
KeyEventHandler::KeyEventHandler( HyJynxCore::Text name )
	:  Name( name )
{ }

// quick functional ctor
KeyEventHandler::KeyEventHandler( _In_ Keys key, _In_ KeyState state, _In_ HyJynxCore::Function<void, KeyState> cb )
	: Key( key ),
	State( state ),
	Handler( cb )
{ }