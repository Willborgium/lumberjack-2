/**
 * KeyboardManager.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXINPUT_KEYBOARDMANAGER_H
#define HYJYNXINPUT_KEYBOARDMANAGER_H

#include "DIKeys.h"
#include "KeyboardListener.h"
#include "KeyEvent.h"
#include "KeyEventHandler.h"
#include <Engine\Headers\Text.h>
#include <Engine\Headers\DynamicCollection.h>

#ifndef DIRECTINPUT_VERSION
#define DIRECTINPUT_VERSION 0x0800
#endif // DIRECTINPUT_VERSION

#include <dinput.h>
#include <sal.h>
#include <Engine\Headers\DestructorMapping.h>

namespace HyJynxInput
{
	const int KEYBOARDMANAGER_STATE_SIZE = 256;

	//
	// KeyboardManager is contained by the InputManager, and holds all logic pertaining to 
	// getting user input from a keyboard
	//
	class KeyboardManager sealed
	{
	private:
		friend  HyJynxMemoryManagement::DestructorMapping < KeyboardManager > ;
		LPDIRECTINPUTDEVICE8 _device = nullptr;
		BYTE _backBuffer [ KEYBOARDMANAGER_STATE_SIZE ];
		KeyEvent _frontBuffer [ KEYBOARDMANAGER_STATE_SIZE ];

		HyJynxCollections::DynamicCollection<KeyboardListener*> _listeners;

		//
		// aquire the keyboard ready for use
		// - returns bool: true for success, keyboard has been successfully aquired
		//
		bool Acquire( );

		//
		// update the front/back buffers
		// - bool: (optional) If the keyboard is not aquired, try and re-aquire it first
		//					  This defaults to true
		// - returns bool: true for a successfull keyboard poll
		//
		bool Poll( _In_opt_ bool tryAcquire = true );

		//
		// release the keyboard from our control (eg: minimized game window)
		// - returns bool: true for success
		//
		bool Unacquire( );

		//
		// clear the front buffer, front buffer being the current poll state
		//
		void ClearFrontBuffer( );

		//
		// clear the back buffer, back buffer being the previous poll state
		//
		void ClearBackBuffer( );

		//
		// when a key changes state, the notification will enter here for event dispatching
		//
		void NotifyKeyEvent( _In_ const Keys key );

		// unimplemented
		KeyboardManager( _In_ const KeyboardManager& );
		KeyboardManager( _In_ const KeyboardManager&& ) = delete;
		KeyboardManager& operator = ( _In_ const KeyboardManager& ) = delete;

	public:

		//
		// default used ctor
		//
		KeyboardManager( );

		//
		// dtor
		//
		~KeyboardManager( );

		//
		// initialize the keyboard manager to a ready-to-use state
		// - LPDIRECTINPUT8: DirectX input interface
		// - HWND: window handle the keyboard is associated to
		//
		bool Initialize( _In_ LPDIRECTINPUT8, _In_ HWND );

		//
		// per-cycle-process call
		//
		void Update( );

		//
		// A quick hook for front-end logic to determine what state the argued key is in
		// Do not abuse this function, you should be using KeyboardListeners
		// - Key: the key in question
		// - returns KeyState: the current state of the key in question
		//
		KeyState GetKeyState( _In_ const unsigned short key );

		//
		// InputHandlers will automatically register themselves here to begin receiving events
		// - KeyboardListener: the newly created listener
		//
		void RegisterListener( _In_ KeyboardListener* listener );
	};
}

#endif // HYJYNXINPUT_KEYBOARDMANAGER_H