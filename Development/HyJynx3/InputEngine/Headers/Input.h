/**
 * Input.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXINPUT_H
#define HYJYNXINPUT_H

#ifndef DIRECTINPUT_VERSION
#define DIRECTINPUT_VERSION 0x0800
#endif // DIRECTINPUT_VERSION

#ifndef SAFE_RELEASE
#define SAFE_RELEASE(c)if(nullptr != c) { c->Release(); c = nullptr; }
#endif // RELEASE_COM

#ifndef SAFE_DELETE
#define SAFE_DELETE(x)if(nullptr != x) { delete x; x = nullptr; }
#endif // SAFE_DELETE

#include <sal.h>
#include <functional>
#include <initguid.h>

#include "DIKeys.h"
#include "InputManager.h"
#include "KeyboardManager.h"
#include "MouseManager.h"
#include "KeyEvent.h"

#endif // HYJYNXINPUT_H