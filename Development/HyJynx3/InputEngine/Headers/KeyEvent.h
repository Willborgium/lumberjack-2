/**
 * KeyEvent.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXINPUT_KEY_EVENT_H
#define HYJYNXINPUT_KEY_EVENT_H

#include "DIKeys.h"
#include <Engine\Headers\Time.h>

namespace HyJynxInput
{
	//
	// structure of an event which occurred by a key changing in state
	//
	struct KeyEvent
	{
		//
		// The current state of the key.
		//
		KeyState State;

		//
		// The last time the state of the key changed.
		//
		HyJynxCore::Time TimeStamp;
	};
}

#endif // HYJYNXINPUT_KEY_EVENT_H