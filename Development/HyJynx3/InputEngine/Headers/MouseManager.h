/**
 * MouseManager.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXINPUT_MOUSEMANAGER_H
#define HYJYNXINPUT_MOUSEMANAGER_H

#ifndef DIRECTINPUT_VERSION
#define DIRECTINPUT_VERSION 0x0800
#endif

#include <Logger\Headers\ILog.h>
#include <Engine\Headers\DynamicCollection.h>
#include <dinput.h>

#include <sal.h>

namespace HyJynxInput
{
	//
	// This manager, contained inside of InputManager, is the API to interact with the mouse
	//
	class MouseManager sealed : public HyJynxLogger::ILog
	{
	private:

		LPDIRECTINPUTDEVICE8 _mouseDevice = nullptr;

		// unimplemented
		MouseManager( _In_ const MouseManager& ) = delete;
		MouseManager( _In_ const MouseManager&& ) = delete;
		MouseManager& operator = ( _In_ const MouseManager& ) = delete;

	public:

		//
		// default ctor
		//
		MouseManager( );

		//
		// dtor
		//
		~MouseManager( );

		//
		// initialize the mouse to a ready-to-use state
		// - LPDIRECTINPUTDEVICE8: directX API interface
		//
		bool Initialize( _In_ LPDIRECTINPUTDEVICE8 );
	};
}

#endif // HYJYNXINPUT_MOUSEMANAGER_H