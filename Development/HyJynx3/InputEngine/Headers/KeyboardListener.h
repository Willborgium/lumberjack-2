/**
 * KeyboardListener.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXINPUT_KEYBOARD_LISTENER_H
#define HYJYNXINPUT_KEYBOARD_LISTENER_H

#include "KeyEventHandler.h"
#include <Engine\Headers\Text.h>
#include <Engine\Headers\DynamicCollection.h>

namespace HyJynxInput
{
	//
	// This is the API front-end will use to receive events from the keyboard
	// many of these objects can be created.
	//
	class KeyboardListener
	{

	protected:

		HyJynxCore::Text _name;
		HyJynxCollections::DynamicCollection<KeyEventHandler*> _handlers;
		bool _isActive;

	public:

		//
		// null ctor - should not be used
		//
		KeyboardListener( );

		//
		// default ctor, defined name
		// - Text: name of this KeyboardListener (name is used for debugging purposes)
		// - bool: optionally create your hander in a deactive state (defaults to active)
		//
		KeyboardListener( _In_ const HyJynxCore::Text name, _In_opt_ const bool = true );

		//
		// set this handler to an active/inactive state
		//
		void SetActive( _In_ const bool isActive );

		//
		// determine if this handler s active or not
		// - returns bool: true - the handler is active, false: tis not
		//
		bool IsActive( ) const;

		//
		// register a new KeyEventHandler
		// - KeyEventHandler*: the handler used to interact with a single key
		//
		void RegisterHandler( _In_ KeyEventHandler* handler );

		void RegisterHandler( _In_ Keys, _In_ KeyState, _In_ HyJynxCore::Function<void, KeyState> );

		//
		// remove a registered KeyEventHandler
		// - Text: name of the handler you wish to remove
		//
		void UnregisterHandler( _In_ const HyJynxCore::Text name );

		//
		// get the entire list of KeyEventHandlers - modify as you please
		// - DynamicCollection<KeyEventHandler*>: list of registered KeyEventHandlers
		//
		HyJynxCollections::DynamicCollection<KeyEventHandler*>& GetHandlers( );
	};
}

#endif // HYJYNXINPUT_KEYBOARD_LISTENER_H