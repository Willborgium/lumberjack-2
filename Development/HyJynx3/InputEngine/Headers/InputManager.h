/**
 * InputManager.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXINPUT_INPUTMANAGER_H
#define HYJYNXINPUT_INPUTMANAGER_H

#include "KeyboardManager.h"
//#include "MouseManager.h"
#include <Engine\Headers\Singleton.h>

#include <dinput.h>
#include <Windows.h>
#include <sal.h>

namespace HyJynxInput
{
	//
	// Singleton to manage all aspects of user input. Although this object contains the managers
	// for different devices, theres no front-end interaction with this.
	//
	class InputManager sealed : public HyJynxDesign::Singleton<InputManager>
	{
		friend HyJynxDesign::Singleton<InputManager>;

	private:

		HINSTANCE			_hInst = nullptr;
		HWND				_hWnd = nullptr;
		LPDIRECTINPUT8		_directInput = nullptr;

		KeyboardManager*	_keyboardManager = nullptr;
		//MouseManager*		_mouseManager;

		//LPDIRECTINPUTDEVICE8 CreateMouseDevice ( );

		InputManager ( );

		// unimplemented
		InputManager( _In_ const InputManager& ) = delete;
		InputManager( _In_ const InputManager&& ) = delete;
		InputManager& operator = ( _In_ const InputManager& ) = delete;
		
	public:

		//
		// release all input managers
		//
		~InputManager( );

		//
		// initialize all possible input managers
		//
		bool Initialize ( _In_ HINSTANCE, _In_ HWND );

		//
		// release all input managers
		//
		void Release ( );

		//
		// process cycle and fire input events
		//
		void Update ( );

		inline KeyboardManager* GetKeyboardManager ( ) const
		{
			return _keyboardManager;
		}

		//inline MouseManager* GetMouseManager ( ) const
		//{
		//	return _mouseManager;
		//}
	};
}

#endif // HYJYNXINPUT_INPUTMANAGER_H