/**
 * KeyEventHandler.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXINPUT_KEY_EVENT_HANDLER_H
#define HYJYNXINPUT_KEY_EVENT_HANDLER_H

#include "DIKeys.h"
#include "InputManager.h"
#include <Engine\Headers\Text.h>
#include <Engine\Headers\TimeSpan.h>
#include <Engine\Headers\Time.h>
#include <Engine\Headers\Function.h>
#include <sal.h>

namespace HyJynxInput
{
	//
	// This class contains the configuration used to listen for keyevents,
	// public parameters will allow you to configure how and when events occur.
	//
	struct KeyEventHandler sealed
	{

	public:

		HyJynxCore::Text								Name = "";
		Keys											Key = Keys::NONE;
		KeyState										State = KeyState::Unknown;
		HyJynxCore::Function<void, KeyState>			Handler;
		HyJynxCore::TimeSpan							Delay = 0;
		HyJynxCore::TimeSpan							RepeatInterval = 0;
		HyJynxCore::TimeSpan							NextNotifyTime = 0;
		bool											IsActive = true;

		//
		// null ctor
		//
		KeyEventHandler( );

		//
		// named ctor
		//
		KeyEventHandler( _In_ HyJynxCore::Text name );

		//
		// quick-functional ctor
		//
		KeyEventHandler( _In_ Keys, _In_ KeyState, _In_ HyJynxCore::Function<void, KeyState> );
	};
};

#endif // HYJYNXINPUT_KEY_EVENT_HANDLER_H