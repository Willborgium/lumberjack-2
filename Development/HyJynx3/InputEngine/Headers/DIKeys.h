/**
 * DIKeys.h
 * (c) 2014 All Rights Reserved
 */

#ifndef HYJYNXINPUT_DIKEYS_H
#define HYJYNXINPUT_DIKEYS_H

#ifndef DIRECTINPUT_VERSION
#define DIRECTINPUT_VERSION 0x0800
#endif // DIRECTINPUT_VERSION
#include <dinput.h>

namespace HyJynxInput
{	
	//
	// describes different states a key can be in
	//
	enum class KeyState : unsigned short
	{
		Up = 0,
		Pressed = 1,
		Down = 2,
		Released = 3,
		Unknown = 10
	};

	//
	// describes the default mouse buttons we can always expect to be present
	//
	enum class MouseButtons : unsigned short
	{
		Left = 0,
		Right = 1,
		Middle = 2
	};

	//
	// Describes the default keys found on a QWERTY keyboard
	//
	enum class Keys : unsigned short
	{
		NONE = 0,

		//Alphabet
#pragma region KEYS_ALPHABET
		A = DIK_A,
		B = DIK_B,
		C = DIK_C,
		D = DIK_D,
		E = DIK_E,
		F = DIK_F,
		G = DIK_G,
		H = DIK_H,
		I = DIK_I,
		J = DIK_J,
		K = DIK_K,
		L = DIK_L,
		M = DIK_M,
		N = DIK_N,
		O = DIK_O,
		P = DIK_P,
		Q = DIK_Q,
		R = DIK_R,
		S = DIK_S,
		T = DIK_T,
		U = DIK_U,
		V = DIK_V,
		W = DIK_W,
		X = DIK_X,
		Y = DIK_Y,
		Z = DIK_Z,
#pragma endregion

#pragma region KEYS_NUMERALS
		Zero = DIK_0,
		One = DIK_1,
		Two = DIK_2,
		Three = DIK_3,
		Four = DIK_4,
		Five = DIK_5,
		Six = DIK_6,
		Seven = DIK_7,
		Eight = DIK_8,
		Nine = DIK_9,
#pragma endregion

#pragma region KEYS_NUMPAD
		NumZero = DIK_NUMPAD0,
		NumOne = DIK_NUMPAD1,
		NumTwo = DIK_NUMPAD2,
		NumThree = DIK_NUMPAD3,
		NumFour = DIK_NUMPAD4,
		NumFive = DIK_NUMPAD5,
		NumSix = DIK_NUMPAD6,
		NumSeven = DIK_NUMPAD7,
		NumEight = DIK_NUMPAD8,
		NumNine = DIK_NUMPAD9,
		NumMultiply = DIK_MULTIPLY,
		NumSubtract = DIK_SUBTRACT,
		NumAdd = DIK_ADD,
		NumDecimal = DIK_DECIMAL,
		NumEquals = DIK_NUMPADEQUALS,
		NumComma = DIK_NUMPADCOMMA,
		NumDivid = DIK_DIVIDE,
#pragma endregion

#pragma region KEYS_FUNCTION
		F1 = DIK_F1,
		F2 = DIK_F2,
		F3 = DIK_F3,
		F4 = DIK_F4,
		F5 = DIK_F5,
		F6 = DIK_F6,
		F7 = DIK_F7,
		F8 = DIK_F8,
		F9 = DIK_F9,
		F10 = DIK_F10,
		F11 = DIK_F11,
		F12 = DIK_F12,
#pragma endregion

#pragma region KEYS_COMMMON
		Enter = DIK_RETURN,
		Space = DIK_SPACE,
		Escape = DIK_ESCAPE,
		RightControl = DIK_RCONTROL,
		LeftControl = DIK_LCONTROL,
		Minus = DIK_MINUS,
		Equals = DIK_EQUALS,
		Backspace = DIK_BACKSPACE,
		Tab = DIK_TAB,
		LeftBracket = DIK_LBRACKET,
		RightBracket = DIK_RBRACKET,
		Semicolon = DIK_SEMICOLON,
		Apostrophe = DIK_APOSTROPHE,
		AccentGrave = DIK_GRAVE,
		LeftShift = DIK_LSHIFT,
		RightShift = DIK_RSHIFT,
		BackSlash = DIK_BACKSLASH,
		Comma = DIK_COMMA,
		Period = DIK_PERIOD,
		ForwardSlash = DIK_SLASH,
		LeftAlt = DIK_LMENU,
		RightAlt = DIK_RMENU,
		CapsLock = DIK_CAPITAL,
		NumLock = DIK_NUMLOCK,
		ScrollLock = DIK_SCROLL,
		LeftWindows = DIK_LWIN,
		RightWindows = DIK_RWIN,

#pragma endregion

#pragma region KEYS_DPAD
		Up = DIK_UP,
		Down = DIK_DOWN,
		Right = DIK_RIGHT,
		Left = DIK_LEFT,
		Home = DIK_HOME,
		PageUp = DIK_PRIOR,
		PageDown = DIK_NEXT,
		End = DIK_END,
		Insert = DIK_INSERT,
		Delete = DIK_DELETE
#pragma endregion
	};
}

#endif // HYJYNXINPUT_DIKEYS_H