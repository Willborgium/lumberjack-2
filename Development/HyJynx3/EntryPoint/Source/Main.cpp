/**
 * Main.cpp
 * (c) 2014 All Rights Reserved
 */

#include <EntryPoint\Headers\Main.h>

#include <Windows.h>

#include <Engine\Headers\EntryPoint.h>
#include <Engine\Headers\Core.h>
#include <Engine\Headers\MemoryManagement.h>
#include <Engine\Headers\Collections.h>
#include <Engine\Headers\Scripting.h>
#include <Engine\Headers\Utilities.h>
#include <Renderer\Headers\Renderer.h>
#include <Renderer\Headers\TextureManager.h>
#include <InputEngine\Headers\InputManager.h>
#include <Logger\Headers\Logger.h>

using namespace HyJynxCore;
using namespace HyJynxMemoryManagement;
using namespace HyJynxCollections;
using namespace HyJynxDesign;
using namespace HyJynxScripting;
using namespace HyJynxUtilities;
using namespace HyJynxRenderer;
using namespace HyJynxInput;
using namespace HyJynxLogger;

#pragma region OS Code

LRESULT CALLBACK WndProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
{
	switch ( message )
	{
		case WM_DESTROY:
			PostQuitMessage( 0 );
			break;
		default:
			return DefWindowProc( hWnd, message, wParam, lParam );
	}

	return 0;
}

int WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, char* lpCmdLine, int nCmdShow )
{
	int output = 0;

	EntryPoint* waldo = new EntryPoint( );

	if ( waldo->Initialize( hInstance, lpCmdLine, nCmdShow ) )
	{
		output = waldo->Run( );

		waldo->Uninitialize( );
	}

	return output;
}

#pragma endregion

EntryPoint::EntryPoint( )
{
}

GraphicsConfiguration _loadGraphicsConfig( _In_ ConfigurationManager* );
RendererConfiguration _loadRendererConfig( _In_ ConfigurationManager* );

bool EntryPoint::Initialize( HINSTANCE instance, char* commandLineArgs, int cmdShow )
{
	bool output = true;

	unsigned int maxRoots = 300;

	_rootHeap = new GlobalMemoryPool( "HyJynx Root Heap", maxRoots * sizeof( AllocatorRootHeader* ) );
	char* rootHeapPtr = nullptr;
	if ( _rootHeap->Open( ) )
	{
		rootHeapPtr = const_cast< char* >( _rootHeap->Read( ) );
	}

	Allocator::InitializeRootSystem( maxRoots, rootHeapPtr );

	_configManager = ConfigurationManager::Instance( );

	if ( !_configManager->LoadFile( ) )
	{
		output = false;
	}
	else
	{
		CoInitialize( nullptr );

		_pool = new GlobalMemoryPool( "HyJynx Instance", 4 );

		AllocatorConfig ac;
		ac.Refresh( );

		if ( ac.UseExposedHeap )
		{
			_exposedHeap = new GlobalMemoryPool( const_cast< char* >( ac.ExposedHeapName.GetValue( ) ), ac.HeapSize );
			_exposedHeap->Open( );
		}

		if ( ac.UseNamespaceHeap )
		{
			_namespaceHeap = new GlobalMemoryPool( const_cast< char* >( ac.NamespaceHeapName.GetValue( ) ), ac.NamespaceHeapSize );
			_namespaceHeap->Open( );
		}

		if ( ac.UseExposedHeap )
		{
			if ( ac.UseNamespaceHeap )
			{
				_allocator = new Allocator( const_cast< char* >( _exposedHeap->Read( ) ), ac.HeapSize,
					const_cast< char* >( _namespaceHeap->Read( ) ), ac.NamespaceHeapSize );
			}
			else
			{
				_allocator = new Allocator( const_cast< char* >( _exposedHeap->Read( ) ), ac.HeapSize );
			}
		}
		else
		{
			_allocator = new Allocator( ac.HeapSize );
		}

		Allocator::Instance( _allocator );

		_dispatcher = DispatchingSystem::Instance( );
		_timer = TimingSystem::Instance( );
		_input = InputManager::Instance( );

		_timer->Initialize( );
		_timer->Begin( );

		_cmdShow = cmdShow;

		DynamicCollection<Data*> data;
		data.Append( anew( BoxedData<Text> ) ( "CommandLineArguments", commandLineArgs ) );

		Function< void, Text > globalLogOutput = [ ] ( _In_ CaptureData& capture, _In_ Text msg )
		{
			OutputDebugString( msg.GetValue( ) );
		};

		Logger::Instance( )->SetGlobalOutput( globalLogOutput );

		if ( !AppMain( data ) )
		{
			Uninitialize( );
			output = false;
		}
		else
		{
			if ( _pool->Open( ) )
			{
				if ( !Helper::IsFirstInstance( *_pool, true ) )
				{
					Uninitialize( );
					output = false;
				}
				else
				{
					auto windowTitle = _configManager->GetTextValue( "General.WindowTitle" );
					auto windowWidth = _configManager->GetIntValue( "Graphics.WindowWidth" );
					auto windowHeight = _configManager->GetIntValue( "Graphics.WindowHeight" );

					_windowHandle = Helper::CreateDefaultWindow( WndProc, instance, windowTitle, windowWidth, windowHeight );

					if ( _dispatcher->Initialize( ) )
					{
						_dispatcher->SetState( SystemState::Initialized );

						GraphicsConfiguration gConfig = _loadGraphicsConfig( _configManager );
						RendererConfiguration rConfig = _loadRendererConfig( _configManager );

						if ( Renderer::Instance( )->Initialize( &_windowHandle, &gConfig, &rConfig ) == false )
						{
							Uninitialize( );
							output = false;
						}

						if ( TextureManager::Instance( )->Initialize( ) == false )
						{
							Uninitialize( );
							output = false;
						}
						
						if ( _input->Initialize( instance, _windowHandle ) == false )
						{
							Uninitialize( );
							output = false;
						}
					}
				}
			}
			else
			{
				output = false;
			}
		}
	}

	return output;
}

int EntryPoint::Run( )
{
	MSG msg;
	ZeroMemory( &msg, sizeof( MSG ) );

	ShowWindow( _windowHandle, _cmdShow );
	UpdateWindow( _windowHandle );


	if ( _dispatcher->Is( SystemState::Initialized ) )
	{
		_dispatcher->Begin( );
	}

	while ( msg.message != WM_QUIT )
	{
		_timer->Update( );

		if ( PeekMessage( &msg, nullptr, 0U, 0U, PM_REMOVE ) )
		{
			TranslateMessage( &msg );
			DispatchMessage( &msg );
		}
		else
		{
			_input->Update( );

			if ( _dispatcher->Is( SystemState::Running ) )
			{
				_dispatcher->Update( );
			}

			const RenderReport report = Renderer::Instance( )->Render( );

			// do stuff with render report
		}

		if ( _allocator != nullptr )
		{
			//auto info = _allocator->GetInfo( );
			//Text temp = "";
			//temp += Text(info.AllocateCalls);
			//temp += ",";
			//temp += Text( info.AllocatedBytes);
			//temp += ",";
			//temp += Text( info.ObjectCount );
			//temp += ",";
			//temp += Text( info.ReleaseCalls );
			//temp += "\n";

			//OutputDebugString( temp.GetValue( ) );
			_allocator->Clean( );
		}
	}

	return int( msg.wParam );
}

void EntryPoint::Uninitialize( )
{
	if ( _timer != nullptr )
	{
		_timer->End( );
		_timer->Uninitialize( );
	}

	if ( _dispatcher != nullptr )
	{
		if ( _dispatcher->Is( SystemState::Running ) )
		{
			_dispatcher->End( );
		}
		else
		{
			if ( _dispatcher->Is( SystemState::Suspended ) )
			{
				if ( _dispatcher->Resume( ) )
				{
					_dispatcher->End( );
				}
			}
		}

		if ( _dispatcher->Is( SystemState::Initialized ) )
		{
			if ( _dispatcher->Uninitialize( ) )
			{
				_dispatcher->SetState( SystemState::Uninitialized );
			}
		}
	}

	Renderer::Instance( )->Shutdown( );

	if ( _pool != nullptr )
	{
		_pool->Close( );
		delete _pool;
		_pool = nullptr;
	}

	if ( _allocator != nullptr )
	{
		// need to fix destructor to check if heap belongs to GMP
		//delete _allocator;
		//_allocator = nullptr;
	}

	if ( _exposedHeap != nullptr )
	{
		_exposedHeap->Close( );
		delete _exposedHeap;
		_exposedHeap = nullptr;
	}

	if ( _namespaceHeap != nullptr )
	{
		_namespaceHeap->Close( );
		delete _namespaceHeap;
		_namespaceHeap = nullptr;
	}
}

#pragma region ConfigLoading

// load all of the settings from the json file, to our object
// TODO revise and use IConvert
GraphicsConfiguration _loadGraphicsConfig( _In_ ConfigurationManager* config )
{
	GraphicsConfiguration output;

	output.SetDirectXDebug( config->GetBooleanValue( "Graphics.DirectXDebug" ) );
	output.SetRefreshRate( config->GetIntValue( "Graphics.RefreshRate" ) );
	output.SetFullscreen( config->GetBooleanValue( "Graphics.Fullscreen" ) );
	output.SetWidth( config->GetIntValue( "Graphics.WindowWidth" ) );
	output.SetHeight( config->GetIntValue( "Graphics.WindowHeight" ) );

	return output;
}

// load all of the settings from the json file, to our object
// TODO revise and use IConvert
RendererConfiguration _loadRendererConfig( _In_ ConfigurationManager* config )
{
	RendererConfiguration output;

	output.SetFrustumCullMethod( static_cast<CollisionMethod>( config->GetIntValue( "Renderer.FrustumCullMethod" ) ) );

	return output;
}

#pragma endregion