/**
 * Main.h
 * (c) 2014 All Rights Reserved
 */

#ifndef MAIN_H
#define MAIN_H

#include <Engine\Headers\EntryPoint.h>
#include <Engine\Headers\Core.h>
#include <Engine\Headers\MemoryManagement.h>
#include <Engine\Headers\Collections.h>
#include <Engine\Headers\Utilities.h>
#include <Renderer\Headers\Renderer.h>
#include <InputEngine\Headers\InputManager.h>

//
// Defines an object that controls basic application flow.
//
class EntryPoint
{
private:
	HyJynxCore::DispatchingSystem* _dispatcher;
	HyJynxCore::TimingSystem* _timer;
	HyJynxMemoryManagement::GlobalMemoryPool* _pool;
	HyJynxMemoryManagement::GlobalMemoryPool* _exposedHeap;
	HyJynxMemoryManagement::GlobalMemoryPool* _namespaceHeap;
	HyJynxMemoryManagement::GlobalMemoryPool* _rootHeap;
	HyJynxCore::ConfigurationManager* _configManager;
	HyJynxMemoryManagement::Allocator* _allocator;
	HyJynxInput::InputManager* _input;
	HWND _windowHandle;
	int _cmdShow;
public:
	//
	// Initializes a new instance of the EntryPoint class.
	//
	EntryPoint( );

	//
	// Initializes the basic systems necessary for the application to run.
	// - instance:			The instance passed in to the entry point.
	// - commandLineArgs:	The command line arguments provided on startup.
	// - cmdShow:			The cmdShow parameter passed into the entry point.
	// - returns:			A flag indicating whether initialization was successful.
	//
	bool Initialize( HINSTANCE instance, char* commandLineArgs, int cmdShow );

	//
	// Runs the main loop of the application.
	// - returns:	The return code specified by the application.
	//
	int Run( );

	//
	// Shuts down all systems initialized by the entry point.
	//
	void Uninitialize( );
};

#endif