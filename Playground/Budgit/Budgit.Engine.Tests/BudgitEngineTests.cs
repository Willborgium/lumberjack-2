﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Budgit.Engine.Tests
{
    [TestClass]
    public class BudgitEngineTests
    {
        public DateTime Today = new DateTime(2014, 8, 29);

        [TestMethod]
        public void SingleOccurenceTest()
        {
            var occurenceDate = Today;
            var start = Today.AddDays(-1);
            var end = Today.AddMonths(4);

            IOccurence single = new SingleOccurence(occurenceDate);

            var test1 = single.GetNextOccurence(start);

            Assert.AreEqual(test1, occurenceDate);

            var test2 = single.GetNextOccurence(end);

            Assert.IsNull(test2);

            var test3 = single.GetOccurences(start, end);

            Assert.AreEqual(test3.Count(), 1);
            Assert.AreEqual(test3.FirstOrDefault(), occurenceDate);
        }

        [TestMethod]
        public void WeeklyOccurenceTest()
        {
            var start = Today.AddDays(-1);
            var end = Today.AddMonths(4);

            IOccurence weekly = new WeeklyOccurence(start, end, DayOfWeek.Thursday, 2);

            var test1 = weekly.GetNextOccurence(start);

            Assert.AreEqual(test1, start);

            var test2 = weekly.GetNextOccurence(Today);

            Assert.AreEqual(test2, new DateTime(2014, 9, 11));

            var test3 = weekly.GetOccurences(Today, end.AddMonths(5));

            Assert.AreEqual(test3.Count(), 8);
        }

        [TestMethod]
        public void MonthlyOccurenceTest()
        {
            var start = Today.AddDays(-1);
            var end = Today.AddMonths(12);

            IOccurence monthly = new MonthlyOccurence(start, end, 30, 2, false);

            var test1 = monthly.GetNextOccurence(Today);

            Assert.AreEqual(test1, new DateTime(2014, 8, 30));

            var test2 = monthly.GetNextOccurence(Today.AddYears(2));

            Assert.IsNull(test2);

            var test3 = monthly.GetOccurences(Today, Today.AddYears(2));

            Assert.AreEqual(test3.Count(), 6);
        }

        [TestMethod]
        public void FinanceEngineTest()
        {
            FinanceEngine engine = new FinanceEngine();

            var augFirst = new DateTime(2014, 8, 1);

            engine.AddFinanceItem("Parking Passes", -550.0m, FinanceItemPriority.High,
                engine.CreateMonthlyOccurence(augFirst, augFirst.AddYears(10), 1, true));

            engine.AddFinanceItem("Jenna's Insurance", -100.0m, FinanceItemPriority.Medium,
                engine.CreateMonthlyOccurence(augFirst, augFirst.AddYears(10), 1, false));

            engine.AddFinanceItem("Will's Insurance", -100.0m, FinanceItemPriority.Medium,
                engine.CreateMonthlyOccurence(augFirst, augFirst.AddYears(10), 1, false));

            engine.AddFinanceItem("Phones", -100.0m, FinanceItemPriority.Medium,
                engine.CreateMonthlyOccurence(augFirst, augFirst.AddYears(10), 1, false));

            engine.AddFinanceItem("Rent", -600.0m, FinanceItemPriority.Low,
                engine.CreateMonthlyOccurence(augFirst, augFirst.AddYears(10), 1, false));

            engine.AddFinanceItem("DiscoverCard", -100.0m, FinanceItemPriority.High,
                engine.CreateMonthlyOccurence(augFirst, augFirst.AddYears(10), 2, false));

            engine.AddFinanceItem("My Great Lakes", -142.26m, FinanceItemPriority.High,
                engine.CreateMonthlyOccurence(augFirst, augFirst.AddYears(10), 7, false));

            engine.AddFinanceItem("Ally Auto", -305.09m, FinanceItemPriority.High,
                engine.CreateMonthlyOccurence(augFirst, augFirst.AddYears(10), 10, false));

            engine.AddFinanceItem("Santander Loan", -475.37m, FinanceItemPriority.High,
                engine.CreateMonthlyOccurence(augFirst, augFirst.AddYears(10), 12, false));

            engine.AddFinanceItem("Wills Paycheck", 1777.0m, FinanceItemPriority.First,
                engine.CreateWeeklyOccurence(augFirst, augFirst.AddYears(10), DayOfWeek.Friday, 2));

            engine.AddFinanceItem("CitiCard", -100.0m, FinanceItemPriority.High,
                engine.CreateMonthlyOccurence(augFirst, augFirst.AddYears(10), 20, false));

            engine.AddFinanceItem("Verizon", -110.0m, FinanceItemPriority.High,
                engine.CreateMonthlyOccurence(augFirst, augFirst.AddYears(10), 21, false));

            engine.AddFinanceItem("Utilities", -100.0m, FinanceItemPriority.High,
                engine.CreateMonthlyOccurence(augFirst, augFirst.AddYears(10), 24, false));

            engine.AddFinanceItem("Will's Sallie Mae", -341.18m, FinanceItemPriority.Medium,
                engine.CreateMonthlyOccurence(augFirst, augFirst.AddYears(10), 26, false));

            Assert.AreEqual(engine.FinanceItems.Count(), 14);

            var didSave = engine.SaveFile("TempBudget.xml");

            Assert.IsTrue(didSave);

            engine = new FinanceEngine();

            var didLoad = engine.LoadFile("TempBudget.xml");

            Assert.IsTrue(didLoad);

            var events = engine.GetEvents(DateTime.Today, DateTime.Today.AddMonths(1), 223.0m);

            string output = "Date\tName\tValue\tBalance\n";

            foreach(var item in events)
            {
                output += string.Format("{0}\t{1}\t{2}\t{3}\n", item.Date.ToShortDateString(), item.Name, item.Value, item.TotalBalance);
            }
        }
    }
}