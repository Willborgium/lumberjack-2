﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budgit.Engine
{
    public class FinanceItem
    {
        public string Name { get; set; }

        public decimal Value { get; set; }

        public FinanceItemPriority Priority { get; set; }

        public IOccurence Frequency { get; set; }
    }
}