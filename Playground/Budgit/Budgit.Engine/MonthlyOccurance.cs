﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budgit.Engine
{
    public class MonthlyOccurence : IOccurence
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int DayOfMonth { get; set; }

        public int MonthFrequency { get; set; }

        public bool IsLastDayOfMonth { get; set; }

        public MonthlyOccurence()
        {
        }

        public MonthlyOccurence(DateTime start, DateTime end, int dayOfMonth, int monthFrequency, bool isLastDayOfMonth)
        {
            StartDate = start;
            EndDate = end;
            DayOfMonth = dayOfMonth;
            MonthFrequency = monthFrequency;
            IsLastDayOfMonth = IsLastDayOfMonth;
        }

        public override IEnumerable<DateTime> GetOccurences(DateTime start, DateTime end)
        {
            var output = new List<DateTime>();

            var day = IsLastDayOfMonth ? DateTime.DaysInMonth(start.Year, start.Month) : DayOfMonth;
            if (day > DateTime.DaysInMonth(start.Year, start.Month))
            {
                day = DateTime.DaysInMonth(start.Year, start.Month);
            }
            DateTime temp = new DateTime(start.Year, start.Month, day);

            if (temp <= start)
            {
                temp = temp.AddMonths(MonthFrequency);
            }
            
            while(temp <= EndDate && temp <= end)
            {
                if(temp >= StartDate)
                {
                    output.Add(temp);
                }

                temp = temp.AddMonths(MonthFrequency);
                day = IsLastDayOfMonth ? DateTime.DaysInMonth(temp.Year, temp.Month) : DayOfMonth;
                if(day > DateTime.DaysInMonth(temp.Year, temp.Month))
                {
                    day = DateTime.DaysInMonth(temp.Year, temp.Month);
                }
                temp = new DateTime(temp.Year, temp.Month, day);
            }

            return output;
        }

        public override DateTime? GetNextOccurence(DateTime from)
        {
            DateTime? output = null;

            DateTime temp = new DateTime(from.Year, from.Month, IsLastDayOfMonth ? DateTime.DaysInMonth(from.Year, from.Month) : DayOfMonth);

            if(temp <= from)
            {
                temp = temp.AddMonths(MonthFrequency);
                temp = new DateTime(temp.Year, temp.Month, IsLastDayOfMonth ? DateTime.DaysInMonth(temp.Year, temp.Month) : DayOfMonth);
            }

            if(temp >= StartDate && temp <= EndDate)
            {
                output = temp;
            }

            return output;
        }
    }
}