﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Budgit.Engine
{
    public class FinanceEngine
    {
        private List<string> _internalMessages = new List<string>();

        private List<FinanceItem> _financeItems = new List<FinanceItem>();

        public string FilePath { get; set; }

        public IEnumerable<FinanceItem> FinanceItems
        {
            get
            {
                return _financeItems;
            }
        }

        public IEnumerable<string> InternalMessages
        {
            get
            {
                return _internalMessages;
            }
        }

        private void LogMessage(string format, params object[] args)
        {
            _internalMessages.Add(string.Format(format, args));
        }

        public FinanceEngine()
        {
            FilePath = string.Empty;
        }

        public bool LoadFile(string filePath)
        {
            bool result = false;

            if (File.Exists(filePath))
            {
                using (var reader = new StreamReader(filePath))
                {
                    var xml = new DataContractSerializer(_financeItems.GetType(), new[]
                        {
                            typeof(SingleOccurence),
                            typeof(MonthlyOccurence),
                            typeof(WeeklyOccurence)
                        });

                    _financeItems = xml.ReadObject(reader.BaseStream) as List<FinanceItem>;

                    result = _financeItems != null;
                }
            }
            else
            {
                LogMessage("Unable to find file '{0}'.", filePath);
            }

            return result;
        }

        public bool SaveFile(string filePath = null)
        {
            bool result = false;

            string file = filePath;

            if(string.IsNullOrEmpty(file))
            {
                file = FilePath;
            }

            if (!string.IsNullOrEmpty(file))
            {
                try
                {
                    using (var writer = new StreamWriter(file))
                    {
                        var xml = new DataContractSerializer(_financeItems.GetType(), new[]
                        {
                            typeof(SingleOccurence),
                            typeof(MonthlyOccurence),
                            typeof(WeeklyOccurence)
                        });

                        xml.WriteObject(writer.BaseStream, _financeItems);
                    }

                    if (File.Exists(file))
                    {
                        FilePath = file;
                        result = true;
                    }
                }
                catch(Exception err)
                {
                    LogMessage("Error while saving. '{0}'", err.Message);
                }
            }

            return result;
        }

        public IOccurence CreateSingleOccurence(DateTime date)
        {
            return new SingleOccurence(date);
        }

        public IOccurence CreateWeeklyOccurence(DateTime startDate, DateTime endDate, DayOfWeek dayOfWeek, int weekFrequency = 1)
        {
            return new WeeklyOccurence(startDate, endDate, dayOfWeek, weekFrequency);
        }

        public IOccurence CreateMonthlyOccurence(DateTime startDate, DateTime endDate, int dayOfMonth, bool isLastDayOfMonth, int monthFrequency = 1)
        {
            return new MonthlyOccurence(startDate, endDate, dayOfMonth, monthFrequency, isLastDayOfMonth);
        }

        public void AddFinanceItem(string name, decimal value, FinanceItemPriority priority, IOccurence frequency)
        {
            _financeItems.Add(new FinanceItem()
                {
                    Name = name,
                    Value = value,
                    Priority = priority,
                    Frequency = frequency
                });
        }

        public IEnumerable<FinanceEvent> GetEvents(DateTime start, DateTime end, decimal startingBalance)
        {
            var temp = new List<FinanceEvent>();

            foreach(var item in FinanceItems)
            {
                var occurrences = item.Frequency.GetOccurences(start, end);

                foreach(var occurence in occurrences)
                {
                    temp.Add(new FinanceEvent()
                        {
                            Name = item.Name,
                            Value = item.Value,
                            Date = occurence,
                            Priority = item.Priority
                        });
                }
            }

            var output = temp.OrderByDescending(i => i.Priority).OrderBy(i => i.Date);

            decimal balance = startingBalance;

            foreach(var item in output)
            {
                balance += item.Value;
                item.TotalBalance = balance;
            }

            return output;
        }
    }
}