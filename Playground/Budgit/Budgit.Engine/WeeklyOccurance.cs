﻿using System;
using System.Collections.Generic;

namespace Budgit.Engine
{
    public class WeeklyOccurence : IOccurence
    {
        private DateTime? _firstOccurence { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public DayOfWeek DayOfWeek { get; set; }

        public int WeekFrequency { get; set; }

        private void GenerateFirstOccurence()
        {
            if (_firstOccurence == null)
            {
                if (StartDate.DayOfWeek != DayOfWeek)
                {
                    var offset = DayOfWeek - StartDate.DayOfWeek;

                    if (offset < 0)
                    {
                        offset += 7;
                    }

                    _firstOccurence = StartDate.AddDays(offset);
                }
                else
                {
                    _firstOccurence = StartDate;
                }
            }
        }

        public WeeklyOccurence()
        {
        }

        public WeeklyOccurence(DateTime start, DateTime end, DayOfWeek day, int frequency)
        {
            StartDate = start;
            EndDate = end;
            DayOfWeek = day;
            WeekFrequency = frequency;
            GenerateFirstOccurence();
        }

        public override DateTime? GetNextOccurence(DateTime from)
        {
            DateTime? output = null;

            GenerateFirstOccurence();

            if (from < EndDate)
            {
                DateTime iterator = _firstOccurence.Value;

                while (iterator < from)
                {
                    iterator = iterator.AddDays(7 * WeekFrequency);
                }

                if (iterator <= EndDate)
                {
                    output = iterator;
                }
            }

            return output;
        }

        public override IEnumerable<DateTime> GetOccurences(DateTime start, DateTime end)
        {
            var output = new List<DateTime>();

            GenerateFirstOccurence();

            var iterator = _firstOccurence.Value;

            while(iterator < start || iterator < StartDate)
            {
                iterator = iterator.AddDays(7 * WeekFrequency);
            }

            while(iterator <= end && iterator <= EndDate)
            {
                output.Add(iterator);
                iterator = iterator.AddDays(7 * WeekFrequency);
            }

            return output;
        }
    }
}