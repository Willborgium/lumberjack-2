﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budgit.Engine
{
    public enum FinanceItemPriority
    {
        Low = 0,
        MediumLow,
        Medium,
        MediumHigh,
        High,
        First
    }
}
