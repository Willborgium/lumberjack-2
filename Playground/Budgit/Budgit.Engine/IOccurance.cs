﻿using System;
using System.Collections.Generic;

namespace Budgit.Engine
{
    public class IOccurence
    {
        public virtual IEnumerable<DateTime> GetOccurences(DateTime start, DateTime end)
        {
            throw new NotImplementedException();
        }

        public virtual DateTime? GetNextOccurence(DateTime from)
        {
            throw new NotImplementedException();
        }
    }
}