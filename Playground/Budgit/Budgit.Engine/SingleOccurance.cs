﻿using System;
using System.Collections.Generic;

namespace Budgit.Engine
{
    public class SingleOccurence : IOccurence
    {
        public DateTime OccurenceDate { get; set; }

        public SingleOccurence()
        {
        }

        public SingleOccurence(DateTime date)
        {
            OccurenceDate = date;
        }

        public override DateTime? GetNextOccurence(DateTime from)
        {
            DateTime? result = null;

            if (from < OccurenceDate)
            {
                result = OccurenceDate;
            }

            return result;
        }

        public override IEnumerable<DateTime> GetOccurences(DateTime start, DateTime end)
        {
            var output = new List<DateTime>();

            var occurence = GetNextOccurence(start);

            if (occurence.HasValue)
            {
                output.Add(occurence.Value);
            }

            return output;
        }
    }
}