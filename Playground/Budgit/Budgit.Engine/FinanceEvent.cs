﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budgit.Engine
{
    public class FinanceEvent
    {
        public string Name { get; set; }

        public decimal Value { get; set; }

        public DateTime Date { get; set; }

        public FinanceItemPriority Priority { get; set; }

        public decimal TotalBalance { get; set; }
    }
}