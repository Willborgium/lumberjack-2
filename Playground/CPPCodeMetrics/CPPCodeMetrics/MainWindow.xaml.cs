﻿using System.Windows;

namespace CPPCodeMetrics
{
    public partial class MainWindow : Window
    {
        public MainWindowViewModel ViewModel
        {
            get
            {
                return DataContext as MainWindowViewModel;
            }
        }

        public MainWindow()
        {
            InitializeComponent();

            DataContext = new MainWindowViewModel();
        }

        private void OnChangeSourceDirectoryClicked(object sender, RoutedEventArgs e)
        {
            if(ViewModel != null)
            {
                ViewModel.ChangeSourceDirectory();
            }
        }

        private void OnRunCodeMetrics(object sender, RoutedEventArgs e)
        {
            if (ViewModel != null)
            {
                ViewModel.RunCodeMetrics();
            }
        }
    }
}
