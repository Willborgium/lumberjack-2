﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CPPCodeMetrics
{
    public class MainWindowViewModel : NotifyOnPropertyChanged
    {
        private string _sourcePath;
        private int _headerFileCount;
        private int _sourceFileCount;
        private int _headerLineCount;
        private int _sourceLineCount;
        private int _headerCommentCount;
        private int _sourceCommentCount;
        private int _filteredHeaderLineCount;
        private int _filteredSourceLineCount;
        private double _headerCommentPercent;
        private double _sourceCommentPercent;
        private double _filteredHeaderCommentPercent;
        private double _filteredSourceCommentPercent;

        public string SourcePath
        {
            get
            {
                return _sourcePath;
            }
            set
            {
                if(value != _sourcePath)
                {
                    _sourcePath = value;
                    OnPropertyChanged();
                }
            }
        }

        public int HeaderFileCount
        {
            get
            {
                return _headerFileCount;
            }
            set
            {
                if(value != _headerFileCount)
                {
                    _headerFileCount = value;
                    OnPropertyChanged();
                }
            }
        }

        public int SourceFileCount
        {
            get
            {
                return _sourceFileCount;
            }
            set
            {
                if (value != _sourceFileCount)
                {
                    _sourceFileCount = value;
                    OnPropertyChanged();
                }
            }
        }

        public int HeaderLineCount
        {
            get
            {
                return _headerLineCount;
            }
            set
            {
                if (_headerLineCount == value)
                {
                    return;
                }
                _headerLineCount = value;
                OnPropertyChanged();
            }
        }

        public int SourceLineCount
        {
            get
            {
                return _sourceLineCount;
            }
            set
            {
                if (_sourceLineCount == value)
                {
                    return;
                }
                _sourceLineCount = value;
                OnPropertyChanged();
            }
        }

        public int HeaderCommentCount
        {
            get
            {
                return _headerCommentCount;
            }
            set
            {
                if (_headerCommentCount == value)
                {
                    return;
                }
                _headerCommentCount = value;
                OnPropertyChanged();
            }
        }

        public int SourceCommentCount
        {
            get
            {
                return _sourceCommentCount;
            }
            set
            {
                if (_sourceCommentCount == value)
                {
                    return;
                }
                _sourceCommentCount = value;
                OnPropertyChanged();
            }
        }

        public int FilteredHeaderLineCount
        {
            get
            {
                return _filteredHeaderLineCount;
            }
            set
            {
                if (_filteredHeaderLineCount == value)
                {
                    return;
                }
                _filteredHeaderLineCount = value;
                OnPropertyChanged();
            }
        }

        public int FilteredSourceLineCount
        {
            get
            {
                return _filteredSourceLineCount;
            }
            set
            {
                if (_filteredSourceLineCount == value)
                {
                    return;
                }
                _filteredSourceLineCount = value;
                OnPropertyChanged();
            }
        }

        public double HeaderCommentPercent
        {
            get
            {
                return _headerCommentPercent;
            }
            set
            {
                if (_headerCommentPercent == value)
                {
                    return;
                }
                _headerCommentPercent = value;
                OnPropertyChanged();
            }
        }

        public double SourceCommentPercent
        {
            get
            {
                return _sourceCommentPercent;
            }
            set
            {
                if (_sourceCommentPercent == value)
                {
                    return;
                }
                _sourceCommentPercent = value;
                OnPropertyChanged();
            }
        }

        public double FilteredHeaderCommentPercent
        {
            get
            {
                return _filteredHeaderCommentPercent;
            }
            set
            {
                if (_filteredHeaderCommentPercent == value)
                {
                    return;
                }
                _filteredHeaderCommentPercent = value;
                OnPropertyChanged();
            }
        }

        public double FilteredSourceCommentPercent
        {
            get
            {
                return _filteredSourceCommentPercent;
            }
            set
            {
                if (_filteredSourceCommentPercent == value)
                {
                    return;
                }
                _filteredSourceCommentPercent = value;
                OnPropertyChanged();
            }
        }

        public MainWindowViewModel()
        {
            SourcePath = @"C:\Users\William.Custode\Source\Workspaces\Lumberjack 2\Development\HyJynx3";
        }

        public void ChangeSourceDirectory()
        {
            var finder = new FolderBrowserDialog();

            var result = finder.ShowDialog();

            if(!string.IsNullOrWhiteSpace(finder.SelectedPath))
            {
                SourcePath = finder.SelectedPath;
            }
        }

        public void RunCodeMetrics()
        {
            var headerFiles = Directory.EnumerateFiles(SourcePath, "*.h", SearchOption.AllDirectories);
            var sourceFiles = Directory.EnumerateFiles(SourcePath, "*.cpp", SearchOption.AllDirectories);

            HeaderFileCount = headerFiles.Count();
            SourceFileCount = sourceFiles.Count();

            foreach(var hFileName in headerFiles)
            {
                using (var reader = new StreamReader(hFileName))
                {
                    while (!reader.EndOfStream)
                    {
                        string currentLine = reader.ReadLine();
                        currentLine = currentLine.Replace("\t", string.Empty).Replace("\r", string.Empty);

                        if (currentLine.StartsWith(@"//"))
                        {
                            HeaderCommentCount++;
                        }
                        else
                        {
                            if (currentLine.Length > 1)
                            {
                                FilteredHeaderLineCount++;
                            }
                            HeaderLineCount++;
                        }
                    }
                }

                HeaderCommentPercent = (double)HeaderCommentCount / (double)HeaderLineCount;
                FilteredHeaderCommentPercent = (double)HeaderCommentCount / (double)FilteredHeaderLineCount;
            }

            foreach (var sFileName in sourceFiles)
            {
                using (var reader = new StreamReader(sFileName))
                {
                    while(!reader.EndOfStream)
                    {
                        string currentLine = reader.ReadLine();
                        currentLine = currentLine.Replace("\t", string.Empty).Replace("\r", string.Empty);

                        if (currentLine.StartsWith(@"//"))
                        {
                            SourceCommentCount++;
                        }
                        else
                        {
                            if (currentLine.Length > 1)
                            {
                                FilteredSourceLineCount++;
                            }
                            SourceLineCount++;
                        }
                    }
                }

                SourceCommentPercent = (double)SourceCommentCount / (double)SourceLineCount;
                FilteredSourceCommentPercent = (double)SourceCommentCount / (double)FilteredSourceLineCount;
            }
        }
    }
}
