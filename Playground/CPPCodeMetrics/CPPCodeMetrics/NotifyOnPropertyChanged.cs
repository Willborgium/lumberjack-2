﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CPPCodeMetrics
{
    public class NotifyOnPropertyChanged : INotifyPropertyChanged
    {
        private Dictionary<string, PropertyChangedEventArgs> _args = new Dictionary<string,PropertyChangedEventArgs>();

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if(!_args.ContainsKey(propertyName))
            {
                _args.Add(propertyName, new PropertyChangedEventArgs(propertyName));
            }

            if(PropertyChanged != null)
            {
                PropertyChanged(this, _args[propertyName]);
            }
        }
    }
}
