﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CommentBuilder
{
    public class MainWindowViewModel : ViewModelBase
    {
        private string _summary;
        private ObservableCollection<ParameterDetail> _parameters;
        private string _returnValue;
        private string _remarks;
        private string _exampleCode;
        private ObservableCollection<ExceptionDetail> _exceptions;

        public string Summary
        {
            get
            {
                return _summary;
            }
            set
            {
                _summary = value;
                OnPropertyChanged("Summary");
            }
        }
        public ObservableCollection<ParameterDetail> Parameters
        {
            get
            {
                return _parameters;
            }
            set
            {
                _parameters = value;
                OnPropertyChanged("Parameters");
            }
        }
        public string ReturnValue
        {
            get
            {
                return _returnValue;
            }
            set
            {
                _returnValue = value;
                OnPropertyChanged("ReturnValue");
            }
        }
        public string Remarks
        {
            get
            {
                return _remarks;
            }
            set
            {
                _remarks = value;
                OnPropertyChanged("Remarks");
            }
        }
        public string ExampleCode
        {
            get
            {
                return _exampleCode;
            }
            set
            {
                _exampleCode = value;
                OnPropertyChanged("ExampleCode");
            }
        }
        public ObservableCollection<ExceptionDetail> Exceptions
        {
            get
            {
                return _exceptions;
            }
            set
            {
                _exceptions = value;
                OnPropertyChanged("Exceptions");
            }
        }

        public MainWindowViewModel()
        {
            Clear();
        }

        public void Clear()
        {
            Summary = string.Empty;
            Parameters = new ObservableCollection<ParameterDetail>();
            ReturnValue = string.Empty;
            Remarks = string.Empty;
            ExampleCode = string.Empty;
            Exceptions = new ObservableCollection<ExceptionDetail>();
        }

        public void GenerateAndCopyComments()
        {
            string comments = "/// <summary>\n/// " + Summary + "\n/// <summary>";

            if (Parameters.Count > 0)
            {
                foreach (ParameterDetail parameter in Parameters)
                {
                    comments += "\n/// <param name='" + parameter.Name + "'>[" +
                        (parameter.IsOptional ? "Optional " : "") + parameter.Intention.ToString() + "] " +
                        parameter.Description + "</param>";
                }
            }

            if (!string.IsNullOrWhiteSpace(ReturnValue) && ReturnValue != string.Empty)
            {
                comments += "\n/// <returns>" + ReturnValue + "</returns>";
            }

            if (!string.IsNullOrWhiteSpace(Remarks) && Remarks != string.Empty)
            {
                comments += "\n/// <remarks>" + Remarks + "</remarks>";
            }

            if (!string.IsNullOrWhiteSpace(ExampleCode) && ExampleCode != string.Empty)
            {
                comments += "\n/// <example><code>" + ExampleCode + "</code></example>";
            }

            if (Exceptions.Count > 0)
            {
                foreach (ExceptionDetail exception in Exceptions)
                {
                    comments += "\n/// <exception cref='" + exception.Type + "'>" + exception.Description + "</exception>";
                }
            }

            Clipboard.SetText(comments);
        }
    }
}
