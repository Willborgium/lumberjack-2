﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommentBuilder
{
    public class ParameterDetail : ViewModelBase
    {
        private string _name;
        private ParameterIntention _intention;
        private bool _isOptional;
        private string _description;

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                OnPropertyChanged("Name");
            }
        }
        public ParameterIntention Intention
        {
            get
            {
                return _intention;
            }
            set
            {
                _intention = value;
                OnPropertyChanged("Intention");
            }
        }
        public bool IsOptional
        {
            get
            {
                return _isOptional;
            }
            set
            {
                _isOptional = value;
                OnPropertyChanged("IsOptional");
            }
        }
        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
                OnPropertyChanged("Description");
            }
        }
    }
}
