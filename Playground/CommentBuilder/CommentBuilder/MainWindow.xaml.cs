﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CommentBuilder
{
    public partial class MainWindow : Window
    {
        public MainWindowViewModel ViewModel
        {
            get
            {
                return this.DataContext as MainWindowViewModel;
            }
            set
            {
                this.DataContext = value;
            }
        }

        public MainWindow()
        {
            InitializeComponent();

            ViewModel = new MainWindowViewModel();
        }

        public void Clear(object sender, EventArgs e)
        {
            if (ViewModel != null)
            {
                ViewModel.Clear();
            }
        }

        public void GenerateAndCopyComments(object sender, EventArgs e)
        {
            if (ViewModel != null)
            {
                ViewModel.GenerateAndCopyComments();
            }
        }
    }
}
