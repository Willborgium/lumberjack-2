#ifndef MEMORY_CONTROLLER_H
#define MEMORY_CONTROLLER_H

#include "DataHeader.h"
#include "MemorySegment.h"

struct MemoryControllerInfo
{
	unsigned int SmallPoolSize;
	unsigned int LargePoolSize;
	unsigned int OverflowPoolSize;
	unsigned int InterminableObjectsPoolSize;

	unsigned int LargeObjectMinimumSize;
};

class MemoryController
{
protected:

	static MemoryController* _controller;

	MemorySegment _smallPool;
	MemorySegment _largePool;
	MemorySegment _overflowPool;
	MemorySegment _interminablePool;

	MemoryControllerInfo _info;
public:
	MemoryController( ) { }

	static MemoryController* MC( )
	{
		return ( _controller == nullptr ) ?
				 _controller = ( MemoryController* ) memset( malloc( sizeof( MemoryController ) ), 0, sizeof( MemoryController ) ) : 
				 _controller;
	}

	void Initialize( MemoryControllerInfo& info )
	{
		_info = info;
		_smallPool.Initialize( _info.SmallPoolSize );
		_largePool.Initialize( _info.LargePoolSize );
		_overflowPool.Initialize( _info.OverflowPoolSize );
		_interminablePool.Initialize( _info.InterminableObjectsPoolSize );
	}

	void* GetSpace( unsigned int size, unsigned int reserved )
	{
		unsigned int headeredSize = size + DataHeader::HeaderSize;
		if( size >= _info.LargeObjectMinimumSize )
		{
			FreeBlock block = _largePool.GetFreeBlock( headeredSize );
			if( block._size >= headeredSize )
			{
				DataHeader* header = ( DataHeader* )block._start;
				header->_size = size;
				header->_reserved = reserved;
				return block._start + DataHeader::HeaderSize;
			}
			else
			{
				FreeBlock block = _overflowPool.GetFreeBlock( headeredSize );
				if( block._size >= headeredSize )
				{
					DataHeader* header = ( DataHeader* )block._start;
					header->_size = size;
					header->_reserved = reserved;
					return block._start + DataHeader::HeaderSize;
				}
				else
				{
					return nullptr;
				}
			}
		}
		else
		{			
			FreeBlock block = _smallPool.GetFreeBlock( headeredSize );
			if( block._size >= headeredSize )
			{
				DataHeader* header = ( DataHeader* )block._start;
				header->_size = size;
				header->_reserved = reserved;
				return block._start + DataHeader::HeaderSize;
			}
			else
			{
				FreeBlock block = _overflowPool.GetFreeBlock( headeredSize );
				if( block._size >= headeredSize )
				{
					DataHeader* header = ( DataHeader* )block._start;
					header->_size = size;
					header->_reserved = reserved;
					return block._start + DataHeader::HeaderSize;
				}
				else
				{
					return nullptr;
				}
			}
		}
	}

	bool TryRelease( void* addr )
	{
		if( _smallPool.ContainsAddress( addr ) ||
			_largePool.ContainsAddress( addr ) ||
			_overflowPool.ContainsAddress( addr ) )
		{
			DataHeader* header = ( DataHeader* )( char* )addr - 4;
			memset( header, 0,  header->_size + DataHeader::HeaderSize );
			return true;
		}

		return false;
	}

	bool IsInterminable( void* addr )
	{
		return _interminablePool.ContainsAddress( addr );
	}
};

void* operator new( size_t size );
void* operator new[] ( size_t size );

void* operator new( size_t size, unsigned int reserved );
void* operator new[]( size_t size, unsigned int reserved );


void operator delete( void* addr );
void operator delete[]( void* addr );
	
void operator delete( void* addr, unsigned int reserved );
void operator delete[]( void* addr, unsigned int reserved );

#endif