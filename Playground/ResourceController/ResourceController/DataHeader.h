#ifndef DATAHEADER_H
#define DATAHEADER_H

struct DataHeader sealed
{
	static const unsigned int HeaderSize;
	unsigned short _size;
	unsigned short _reserved;
};

#endif