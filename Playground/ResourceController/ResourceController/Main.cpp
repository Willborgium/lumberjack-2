#include <iostream>
using namespace std;

#include "MemoryController.h"

class Person
{
public:
	Person( char* n, int a, float c )
		: Name( n ), Age( a ), Cash( c )
	{
	}
	char* Name;
	int Age;
	float Cash;
};

void main( )
{
	MemoryControllerInfo mci;

	mci.SmallPoolSize = 5000;
	mci.LargePoolSize = 10000;
	mci.OverflowPoolSize = 5000;
	mci.InterminableObjectsPoolSize = 0;
	mci.LargeObjectMinimumSize = 128;

	MemoryController::MC( )->Initialize( mci );
	Person* p = new( 500 ) Person( "Jobin", 12, 52136.45 );
	Person* j = new( 1234 ) Person( "Alex", 51, 13.45 );

	system( "pause" );
}