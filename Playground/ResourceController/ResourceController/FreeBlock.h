#ifndef FREEBLOCK_H
#define FREEBLOCK_H

#include "AlignedBlock.h"

struct FreeBlock sealed
{
	byte* _start;
	unsigned int _size;

	FreeBlock( byte* start, unsigned int size )
		: _start( start ), _size( size )
	{
	}
};

#endif