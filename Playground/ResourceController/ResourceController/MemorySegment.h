#ifndef MEMORY_SEGMENT_H
#define MEMORY_SEGMENT_H

#include <memory>

#include "FreeBlock.h"

struct MemorySegmentInformation sealed
{
	MemorySegmentInformation( void* start )
		: StartAddress( start )
	{
	}
	const void* const StartAddress;
	unsigned int Size;
	unsigned int FreeBytes;
	unsigned int Headers;
	unsigned int FragmentedBytes;
};

class MemorySegment sealed
{
protected:
	byte* _start;
	AlignedBlock32* _buffer;
	unsigned int _byteCount;

	void* RawNew( size_t size )
	{
		return memset( malloc( size ), 0, size );
	}

public:
	MemorySegment( )
	{
	}

	inline void Initialize( unsigned int byteCount )
	{
		unsigned int blocks = ( byteCount / 32 ) + 1;
		_buffer = ( AlignedBlock32* )RawNew( blocks * 32 );
		_start = ( byte* ) _buffer;
		_byteCount = blocks * 32;
	}

	inline void Clear( )
	{
		if( _start != nullptr && _byteCount > 0 )
		{
			memset( _start, 0, _byteCount );
		}
	}

	inline FreeBlock GetFreeBlock( unsigned int minSize )
	{
#pragma warning( disable:4018 )

		if( _start == nullptr || _byteCount == 0 || minSize >= _byteCount )
		{
			return FreeBlock( nullptr, 0 );
		}
		else
		{
			byte* sptr = _start;
			byte* eptr = sptr;
			byte* end = _start + _byteCount;

			while( sptr >= _start && sptr <= end &&
				   eptr >= _start && eptr <= end &&
				   ( end - sptr ) > minSize )
			{
				if( *sptr == 0 )
				{
					if( eptr < sptr )
					{
						eptr = sptr;
					}
					else
					{
						if( *eptr == 0 )
						{
							eptr++;

							if( ( eptr - sptr ) >= minSize )
							{
								break;
							}
						}
						else
						{
							sptr = eptr;
						}
					}
				}
				else
				{
					sptr += ( ( DataHeader* )( sptr ) )->_size + DataHeader::HeaderSize;
				}
			}

			if( ( eptr - sptr ) >= minSize )
			{
				return FreeBlock( sptr, minSize );
			}
			else
			{
				return FreeBlock( nullptr, 0 );
			}
		}
#pragma warning( default:4018 )
	}

	inline MemorySegmentInformation GetInfo( )
	{		
#pragma warning( disable:4018 )

		MemorySegmentInformation msi( _start );
		memset( &msi, 0, sizeof( MemorySegmentInformation ) );
		msi.Size = _byteCount;

		if( _start != nullptr && _byteCount != 0 )
		{
			byte* sptr = _start;
			byte* eptr = sptr;
			byte* end = _start + _byteCount;

			while( sptr >= _start && sptr <= end &&
				   eptr >= _start && eptr <= end )
			{
				if( *sptr == 0 )
				{
					if( eptr < sptr )
					{
						eptr = sptr;
					}
					else
					{
						if( *eptr == 0 )
						{
							eptr++;
						}
						else
						{
							// Free bytes
							unsigned int bytes = eptr - sptr;
							msi.FreeBytes += bytes;

							// Fragmented bytes ( segments less than 32 bytes )
							if( bytes < 32 )
							{
								msi.FragmentedBytes += bytes;
							}

							sptr = eptr;
						}
					}
				}
				else
				{
					sptr += ( ( DataHeader* )( sptr ) )->_size + DataHeader::HeaderSize;
					msi.Headers++;
				}
			}
		}

		return msi;
#pragma warning( default:4018 )
	}

	inline bool ContainsAddress( void* addr )
	{
		return ( _byteCount > 0 && _start <= addr && addr < _start + _byteCount );
	}
};

#endif