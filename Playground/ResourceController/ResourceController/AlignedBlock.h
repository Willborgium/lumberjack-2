#ifndef ALIGNEDBLOCK_H
#define ALIGNEDBLOCK_H

typedef char byte;

__declspec( align( 32 ) ) struct AlignedBlock32 sealed
{
	byte _bytes[32];
};

#endif