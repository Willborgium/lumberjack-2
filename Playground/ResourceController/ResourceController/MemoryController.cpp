#include "MemoryController.h"

void* operator new( size_t size )
{
	return MemoryController::MC( )->GetSpace( size, -1 );
}

void* operator new[] ( size_t size )
{
	return MemoryController::MC( )->GetSpace( size, -1 );
}

void* operator new( size_t size, unsigned int reserved )
{
	return MemoryController::MC( )->GetSpace( size, reserved );
}

void* operator new[]( size_t size, unsigned int reserved )
{
	return MemoryController::MC( )->GetSpace( size, reserved );
}

void operator delete( void* addr )
{
	if( !MemoryController::MC( )->TryRelease( addr ) )
	{
		if( !MemoryController::MC( )->IsInterminable( addr ) )
		{
			free( addr );
		}
	}
}

void operator delete[]( void* addr )
{
	if( !MemoryController::MC( )->TryRelease( addr ) )
	{
		if( !MemoryController::MC( )->IsInterminable( addr ) )
		{
			free( addr );
		}
	}
}

void operator delete( void* addr, unsigned int reserved )
{
	if( !MemoryController::MC( )->TryRelease( addr ) )
	{
		if( !MemoryController::MC( )->IsInterminable( addr ) )
		{
			free( addr );
		}
	}
}

void operator delete[]( void* addr, unsigned int reserved )
{
	if( !MemoryController::MC( )->TryRelease( addr ) )
	{
		if( !MemoryController::MC( )->IsInterminable( addr ) )
		{
			free( addr );
		}
	}
}

MemoryController* MemoryController::_controller = nullptr;